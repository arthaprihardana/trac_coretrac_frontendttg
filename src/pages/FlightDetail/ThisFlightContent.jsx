// libraries
import React, { Fragment } from "react";

// local component
import { Button } from "atom";
import FlightSection from "./ThisFlightSection";


const ThisFlightContent = ({typeService}) => {
    return (
        <div className="left-content">
            <div className="personal-detail">
                <div className="title">
                    <h3>Flight Details</h3>
                    <p> Please confirm the Flight Detail for a more approximate pickup hour </p>
                </div>
                {
                    !typeService && <FlightSection labelDestination="CGK (Terminal 2) - Kempinski Hotel, Jl Jendral Sudirman" />
                }
                {
                    typeService === '1' && <FlightSection labelDestination="CGK (Terminal 2) - Kempinski Hotel, Jl Jendral Sudirman" />
                }
                {
                    typeService === '2' && (
                        <Fragment>
                            <FlightSection labelDestination="Kempinski Hotel, Jl Jendral Sudirman - CGK (Terminal 2)" />
                            <FlightSection labelDestination="CGK (Terminal 2) - Kempinski Hotel, Jl Jendral Sudirman" />
                        </Fragment>
                    )
                }
                <div className="action-bottom">
                    <Button type="button" goto="/personal-detail-login/airport-transfer" primary>Continue</Button>
                </div>
            </div>
        </div>
    )
}

export default ThisFlightContent;