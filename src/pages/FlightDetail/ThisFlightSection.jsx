// libraries
import React, { PureComponent } from "react";
import { Form, Formik } from "formik";
import moment from 'moment';

// local component
import { TracField } from "atom";

class FlightSection extends PureComponent {

    state = {
        hourPick: moment()
    }

    render() {
        const {
            props: {
                labelDestination
            },
            state: {
                hourPick
            }
        } = this;
        return (
            <div className="flight-detail">
                <p className="flight-destination">{labelDestination}</p>
                <Formik>
                    {({ isSubmitting, handleChange, handleBlur, values }) => (
                        <Form className="form-flight">
                            <div className="form-row">
                                <div className="form-col">
                                    <TracField
                                        id="airlineCode"
                                        name="airlineCode"
                                        type="text"
                                        labelName="Airline Code"
                                        onChange={handleChange}
                                        value={values.airlineCode}
                                    />
                                </div>
                                <div className="form-col">
                                    <TracField
                                        id="flightNumber"
                                        name="flightNumber"
                                        type="text"
                                        labelName="Flight Number"
                                        onChange={handleChange}
                                        value={values.flightNumber}
                                    />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-col pick-hour">
                                    <TracField
                                        id="pickHour"
                                        name="pickHour"
                                        type="time"
                                        labelName="Pickup Hour"
                                        onChange={(val) => this.setState({hourPick: val})}
                                        icon="clock"
                                        value={hourPick}
                                    />
                                    <div className="input-note">
                                        You may change the pickup hour if you wish to
                                    </div>
                                </div>
                            </div>
                            <div className="form-row">
                                <TracField
                                    id="pickupLocation"
                                    name="pickupLocation"
                                    type="text"
                                    labelName="Pickup Location"
                                    onChange={handleChange}
                                    value={values.pickupLocation}
                                />
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        );
    }
}

FlightSection.defaultProps = {
    labelDestination: 'CGK (Terminal 2) - Kempinski Hotel, Jl Jendral Sudirman'
}

export default FlightSection;