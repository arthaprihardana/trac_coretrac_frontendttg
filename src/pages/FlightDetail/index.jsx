/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-13 14:23:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-13 14:34:43
 */
// libraries
import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import { defaultDidMountSetStyle, defaultWillUnmountSetStyle } from 'helpers';

// local component
import { CustomerGlobal } from "context/RentContext";
import { OrderSteps } from "molecules";
import { SideOrderDetail } from 'organisms';
import { Main } from "templates";
import ThisFlightContent from "./ThisFlightContent";
import ThisFlightWrapper from "./ThisFlightWrapper";

class FlightDetail extends PureComponent {

    componentDidMount() {
        defaultDidMountSetStyle();
    }

    componentWillUnmount() {
        defaultWillUnmountSetStyle();
    }

    render() {
        const { 
            props: {
                rentContext: {
                    state: {
                        values,
                    }
                }
            },
        } = this;

        return (
            <Main solid hideMenu headerClose>
                <OrderSteps activeStep={2} type="airport-transfer" />
                <ThisFlightWrapper>
                    <ThisFlightContent typeService={values.type_service} />
                    <SideOrderDetail contentNotes type={"airport-transfer"}/>
                </ThisFlightWrapper>
            </Main>
        );
    }
}

export default CustomerGlobal(withRouter(FlightDetail));