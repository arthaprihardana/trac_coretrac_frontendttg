import React from 'react';

const ThisFlightWrapper = ({ children }) => {
    return (
        <div className="order-detail">
            <div className="container">
                <div className="order-detail-container">
                    {children}
                </div>
            </div>
        </div>
    )
}

export default ThisFlightWrapper;