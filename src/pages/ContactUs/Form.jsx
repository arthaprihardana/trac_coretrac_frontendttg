import React, { Fragment, Component } from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import './Form.scss';

// components
import { Button, TracField } from 'atom';

class FormContact extends Component {
    state = {
        showItem: false,
        selectedValue: '',
    }

    handleClickSelectMenu = (e) => {
        e.preventDefault();
        this.setState(prevState => ({
            ...prevState,
            showItem: !prevState.showItem,
        }));
    }

    render() {
        const { showItem, selectedValue } = this.state;

        return (
            <Fragment>
                <div className="form-container">
                    <div className="form">
                        <Formik
                            initialValues={{
                                name: '',
                                email: '',
                                nationality: '',
                                phoneNumber: '',
                                category: '',
                                subCategory: '',
                                messages: ''
                            }}
                            validationSchema={Yup.object().shape({
                                email: Yup.string()
                                    .email()
                                    .required()
                            })}
                        >
                            {({ handleChange, handleBlur, values }) => (
                                <Form>
                                    <TracField
                                        id="name"
                                        type="text"
                                        name="name"
                                        labelName="Name"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.name}
                                    />
                                    <div style={{ padding: '15px 0' }} />
                                    <TracField
                                        id="email"
                                        type="text"
                                        name="email"
                                        labelName="Email Address"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.email}
                                    />
                                    <div style={{ padding: '15px 0' }} />
                                    <TracField
                                        id="nationality"
                                        type="text"
                                        name="nationality"
                                        labelName="Nationality"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.nationality}
                                    />
                                    <div style={{ padding: '15px 0' }} />
                                    <TracField
                                        id="phoneNumber"
                                        type="text"
                                        name="phoneNumber"
                                        labelName="Phone Number"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.phoneNumber}
                                    />
                                    <div style={{ padding: '15px 0' }} />
                                    <TracField
                                        id="category"
                                        type="select"
                                        name="category"
                                        labelName="Category"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        showItem={showItem}
                                        onClick={this.handleClickSelectMenu}
                                        onItemClick={this.handleItemClick}
                                        value={selectedValue}
                                    />
                                    <div style={{ padding: '15px 0' }} />
                                    <TracField
                                        id="subCategory"
                                        type="select"
                                        name="subCategory"
                                        labelName="Sub Category"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        showItem={showItem}
                                        onClick={this.handleClickSelectMenu}
                                        onItemClick={this.handleItemClick}
                                        value={selectedValue}
                                    />
                                    <div style={{ padding: '15px 0' }} />
                                    <TracField
                                        id="messages"
                                        type="text-area"
                                        name="messages"
                                        labelName="Message"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.messages}
                                    />
                                    <Button type="button" primary>submit</Button>
                                </Form>
                            )}
                        </Formik>
                    </div>
                    <div className="info"></div>
                </div>
            </Fragment>
        )
    }
}

export default FormContact;