import React, { Component } from 'react';
import { Main, Static } from 'templates';
import bannerImage from 'assets/images/trac/ContactUs.png'
import './ContactUs.scss';
import FormContact from './Form';


class ContactUs extends Component {
  state = {
    banner: {
      image: bannerImage,
      title: "Contact TRAC-Astra Rent",
      subTitle: "We 2 want to hear from you! Your input is valuable and will allow us to better serve your needs in the future"
    }
  };
  render() {
    const { banner } = this.state;
    return (
      <Main className="p-contact-us">
        <Static title={banner.title} bannerImg={banner.image} subTitle={banner.subTitle}>
          <FormContact />
        </Static>
      </Main>
    )
  }
}

export default ContactUs;