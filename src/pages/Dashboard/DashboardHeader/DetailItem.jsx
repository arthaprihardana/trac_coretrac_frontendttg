import React from "react";

const DetailItem = ({label, value}) => {
    return (
        <div className="detail-item">
            <p className="label">{label}</p>
            <p className="value">{value}</p>
        </div>
    );
};

export default DetailItem;
