/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-21 15:57:53 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-23 22:55:50
 */
import React, { Component } from "react";
import UserProfile from "../../../assets/images/dummy/unknown.png";
import ImagesNull from "assets/images/dummy/unknown.png";
import DetailItem from './DetailItem';
import cx from 'classnames';
import { PulseLoader } from "react-spinners";
import { removeScrollOnPopup, delay } from 'helpers';
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { FiLogOut } from 'react-icons/fi';
// actions
import { getLogout } from "actions";
import { localStorageDecrypt } from "helpers";
import db from '../../../db';

import lang from '../../../assets/data-master/language'
class DashboardHeader extends Component {

    state = {
        showMenuMobile: false,
        user_login: null
    }

    componentDidMount = () => {
        let local_auth = localStorageDecrypt('_aaa', 'object');
        if(local_auth !== null) {
            this.setState({
                user_login: local_auth
            });
        }
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.logoutSuccess !== prevProps.logoutSuccess) {
            return { success: this.props.logoutSuccess }
        }
        if(this.props.logoutFailure !== prevProps.logoutFailure) {
            return { failure: this.props.failure }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            const {
                history: {
                    push
                },
            } = this.props;
            if(snapshot.success !== undefined) {
                delay(1000).then(() => push('/'));
            }
        }
    }

    handleShowMenuMobile = () => {
        this.setState({
            showMenuMobile: !this.state.showMenuMobile
        }, ()=> {
            removeScrollOnPopup(this.state.showMenuMobile);
        })
    }

    render() {
        const { user_login } = this.state;
        const { loading } = this.props;
        let classDetailProfile = cx('detail-profile', {
            'show': this.state.showMenuMobile
        })
        let classShowMobileMenu = cx('mobile-show-desc', {
            'show': this.state.showMenuMobile
        })
        return (
            <div className="dashboard-header">
                <div className="profile">
                    <div className="profile-img">
                        <img src={user_login && user_login.ImagePath !== null ? user_login.ImagePath : UserProfile} alt="profile" style={{ objectFit: "cover"}} />
                    </div>
                    <div className="profile-desc">
                        <p className="name">{user_login && user_login.FirstName} {user_login && user_login.LastName}</p>
                        <div className="address">{user_login && user_login.Address }</div>
                    </div>
                    <div className={classShowMobileMenu} onClick={this.handleShowMenuMobile}/>
                </div>
                <div className={classDetailProfile}>
                    <DetailItem label={lang.idNumber[lang.default]} value={user_login && user_login.NoKTP !== null ? user_login.NoKTP : "-"} />
                    <DetailItem label={lang.phoneNumber[lang.default]} value={user_login && user_login.NoHandphone !== null ? user_login.NoHandphone : "-" } />
                    <DetailItem label="Email" value={user_login && user_login.EmailPersonal !== null ? user_login.EmailPersonal : "-"} />
                    {/* { loading && <PulseLoader
                        sizeUnit={"px"}
                        size={7}
                        color={'#ffffff'}
                        loading={loading}
                    /> } */}
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ login }) => {
    const { loading, logoutSuccess, logoutFailure } = login;
    return { loading, logoutSuccess, logoutFailure };
}

export default withRouter(
    connect(mapStateToProps, {
        getLogout
    })(DashboardHeader)
)