/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-23 22:50:36 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-02-23 22:50:36 
 */
import React, { Component } from "react";
import { Route, withRouter } from "react-router-dom";
import DashboardRoutes from "./DashboardRoutes";
import SideMenuItem from "./SideMenuItem";
import MenuItem from "./MenuItem";

class DashboardContent extends Component {
    state = {
        menu: MenuItem
    };

    changeMenuActive = (data, url) => {
        let newMenu = [...this.state.menu];

        let menuIndex;
        let nextPath;
        if (data.id) {
            menuIndex = newMenu.findIndex(menuId => menuId.id === data.id);
            nextPath = data.path;
        } else {
            menuIndex = newMenu.findIndex(menuId => menuId.path === data);
            nextPath = url;
        }

        let activeIndex = newMenu.findIndex(menuId => menuId.isActive === true);

        if (activeIndex >= 0) newMenu[activeIndex].isActive = false;
        if (menuIndex >= 0) newMenu[menuIndex].isActive = true;

        //check if page doesn't have sidemenu active
        menuIndex = (menuIndex === -1 ? 0 : menuIndex);

        this.setState({
            menu: newMenu
        }, () => {
            this.props.history.push(nextPath);
        });
    }

    handleClick = item => {
        this.changeMenuActive(item);
    };

    componentDidMount() {
        let urlSplit = this.props.match.url.split('/');
        if(urlSplit.length > 3){
            this.changeMenuActive(`/dashboard/${urlSplit[2]}`, this.props.match.url);
        }else {
            this.changeMenuActive(this.props.match.url, this.props.match.url);
        }
    }

    render() {
        const { menu } = this.state;

        return (
            <div className="dashboard-content">
                <div className="dashboard-content-wrapper">
                    <div className="side-menu">
                        {menu.map(item => {
                            return (
                                <SideMenuItem
                                    key={item.id}
                                    label={item.label}
                                    iconName={item.iconName}
                                    badge={item.badge}
                                    isActive={item.isActive}
                                    onClick={() => this.handleClick(item)}
                                />
                            );
                        })}
                    </div>
                    <div className="content">
                        {DashboardRoutes.map(comp => {
                            return <Route key={comp.id} {...comp} />;
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(DashboardContent);
