import MyBooking from "../MyBooking";
import MyHistory from "../MyHistory";
import MyBookingDetail from "../MyBooking/Detail";
import HistoryBooking from "../MyHistory/Detail";
import AccountSettings from "../AccountSettings";
import MyVoucher from '../MyVoucher';
import Message from '../Message';

const DashboardRoutes = [
    {
        id: "my-booking",
        path: "/dashboard/my-booking",
        component: MyBooking,
        exact: true
    },
    {
        id: "my-booking-detail",
        path: "/dashboard/my-booking/:child/:id",
        component: MyBookingDetail,
        exact: true
    },
    {
        id: "my-voucher",
        path: "/dashboard/my-voucher",
        component: MyVoucher,
        exact: true
    },
    {
        id: "history-booking",
        path: "/dashboard/history-booking",
        // component: HistoryBooking,
        component: MyHistory,
        exact: true
    },
    {
        id: "history-booking-detail",
        path: "/dashboard/history-booking/:child/:id",
        component: HistoryBooking,
        exact: true
    },
    {
        id: "message",
        path: "/dashboard/message",
        component: Message,
        exact: true
    },
    {
        id: "account-settings",
        path: "/dashboard/account-settings",
        component: AccountSettings,
        exact: true
    },
];

export default DashboardRoutes;