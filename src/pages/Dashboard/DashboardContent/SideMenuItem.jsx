import React from "react";
import cx from 'classnames';

const SideMenuItem = ({label, iconName, badge, isActive, onClick}) => {
    const classItem = cx('list-item', {
        'active' : isActive
    })

    return (
        <div className={classItem} onClick={onClick}>
            <div className={`icon ${iconName}`} />
            <p className="label">{label}</p>
            {/* {
                badge > 0 && <p className="badge">{badge > 99 ? `+99` : badge}</p>
            } */}
        </div>
    );
};

export default SideMenuItem;
