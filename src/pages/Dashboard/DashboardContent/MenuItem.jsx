import lang from '../../../assets/data-master/language'
const MenuItem = [
    {
        id: 1,
        label: lang.myBooking[lang.default],
        iconName: "booking",
        path: "/dashboard/my-booking",
        badge: 3,
        isActive: true
    },
    // {
    //     id: 2,
    //     label: "My Voucher",
    //     iconName: "voucher",
    //     path: "/dashboard/my-voucher",
    //     badge: 3,
    //     isActive: false
    // },
    {
        id: 3,
        label: lang.historyBooking[lang.default],
        iconName: "history",
        path: "/dashboard/history-booking",
        badge: 100,
        isActive: false
    },
    // {
    //     id: 4,
    //     label: "Message",
    //     iconName: "message",
    //     path: "/dashboard/message",
    //     badge: 3,
    //     isActive: false
    // },
    {
        id: 5,
        label: lang.accountSettings[lang.default],
        iconName: "setting",
        path: "/dashboard/account-settings",
        badge: 0,
        isActive: false
    }
]

export default MenuItem;