/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-23 21:37:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 09:45:46
 */
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { defaultDidMountSetStyle, defaultWillUnmountSetStyle } from "helpers";
import { connect } from "react-redux";
import { Main } from "templates";
import "./Dashboard.scss";
import DashboardContent from './DashboardContent';
import DashboardHeader from "./DashboardHeader";

class Dashboard extends Component {

    componentDidMount() {
        defaultDidMountSetStyle();
        // var scr = document.getElementById("ze-snippet");
        // document.head.removeChild(scr);
    }

    componentWillUnmount() {
        defaultWillUnmountSetStyle();
    }

    render() {
        return (
            <Main solid dashboardLogin headerUserLogin>
                <div className="p-dashboard">
                    <DashboardHeader/>
                    <DashboardContent/>
                </div>
            </Main>
        );
    }
}

export default withRouter(Dashboard);
