import { Button, Modal, TracField } from "atom";
import { Form, Formik } from "formik";
import React, { Component, Fragment } from "react";
import * as Yup from "yup";
import './ModalModifyBookingSelectPayment.scss';


// validation schema 
const AuthSchema = Yup.object().shape({
  bankAccount: Yup.string()
      .min("9", "Min 9 characters!")
      .required("Bank Account is required!"),
  name: Yup.string()
      .min("6", "Min 6 characters!")
      .required("Name is required!")
});

class ModalModifyBookingSelectPayment extends Component {
  state = {
    step: this.props.stepPopup,
    data: 'BCA Bank',
    showPackage: false
  }

  onClickPackage = (e) => {
    let { showPackage } = this.state;
    this.setState({ showDatePicker: false, showPackage: !showPackage });
  }

  handleSelectPackage = (value) => {
    this.setState({ data: value, showPackage: false });
  }

  handleSubmit = () => {
    this.props.onSubmit('success');
  }

  render() {
    const { showModal, onClose } = this.props;

    let selectBank = (
      <Fragment>
        <div className="desc-item">
          <sup>Select Bank</sup>
          <div className={`form-input`}>
            <div className="form-select" onClick={this.onClickPackage}>
              <span>{this.state.data}</span>
            </div>
            <ul className={`form-select-list ${this.state.showPackage ? 'show' : ''}`}>
              <li onClick={(e) => this.handleSelectPackage('Permata Bank')}>Permata Bank</li>
              <li onClick={(e) => this.handleSelectPackage('Mandiri Bank')}>Mandiri Bank</li>
            </ul>
          </div>
        </div>
        <br/>
        <br/>

        <Formik
            initialValues={{
                bankAccount: "",
                name: ""
            }}
            validationSchema={AuthSchema}
            onSubmit={this.handleSubmit}
        >
            {({ isSubmitting, handleBlur, handleChange, values }) => (
                <Form>
                    <TracField
                        id="bankAccount"
                        name="bankAccount"
                        type="text"
                        labelName="Bank Account"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.bankAccount}
                    />
                    <br/><br/><br/>
                    <TracField
                        id="name"
                        name="name"
                        type="text"
                        labelName="Name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                    />
                </Form>
            )}
        </Formik>
      </Fragment >
    );

    return (
      <Fragment>
        <Modal
          open={showModal}
          extraClass="modal-modify-booking"
          onClick={onClose}
          size="medium"
          header={{
            withCloseButton: false,
            onClick: () => console.log("header close"),
            children: (
              <div className="dashboard-popup-header">
                <p>Modify Booking</p>
                <div className="icon-close" onClick={onClose} />
              </div>
            )
          }}
          content={{
            children: (
              <div className="dashboard-popup-content">
                <div className="booking-modify-detail">
                  <h3>Bank Account</h3>
                  <h4>Please enter your bank account to refund your order</h4>
                  <br/>
                  <div className="booking-modify-detail-list">
                    {selectBank}
                  </div>
                </div>
              </div>
            )
          }}
          footer={{
            position: "center",
            children: (
              <div className="success two-button">
                <Button primary onClick={this.handleSubmit}>modify booking</Button>
                <Button outline onClick={onClose}>don't modify</Button>
              </div>
            )
          }}
        />
      </Fragment>
    );
  }
}

export default ModalModifyBookingSelectPayment;
