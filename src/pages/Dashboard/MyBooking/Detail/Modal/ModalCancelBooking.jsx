import { Button, Modal } from "atom";
import React, { Component, Fragment } from "react";
import carsOne from "../../../../../assets/images/dummy/cars/Innova.png";

import StatusProgressBar from "../StatusProgressBar";

class ModalCancelBooking extends Component {
  state = {
    step: this.props.stepPopup,
    data: {
      date: this.props.data,
      selected: {
        isActived: false,
        total: 0
      },
      car: [{
        image: carsOne,
        title: 'Toyota Avanza #1',
        selected: false
      }, {
        image: carsOne,
        title: 'Toyota Avanza #2',
        selected: false
      }],
      reason: [{
        title: 'Change of date or destination',
        value: 1,
        selected: false
      },
      {
        title: 'Found a different rental option',
        value: 2,
        selected: false
      },
      {
        title: 'Personal reasons/Trip called off',
        value: 3,
        selected: false
      },
      {
        title: 'Made booking for the same dates',
        value: 4,
        selected: false
      },
      {
        title: 'None of the above',
        value: 5,
        selected: false
      }]
    }
  }

  handleSubmit = () => {
    let { step, data } = this.state;
    step = step === 0 ? 1 : -1;

    if (step === -1) {
      this.props.onSubmit('success');

      // reset popup data
      step = 0;
      data.selected.isActived = false;
      data.selected.total = 0;
      data.car.map((value, i) => {
        return data.car[i].selected = false
      });
    }

    this.setState({ step: step });
  }

  handleSelect = (e) => {
    const updateData = this.state.data;
    let index = e.currentTarget.dataset.index,
      totalSelected = 0;

    if (index === 'all') {
      updateData.selected.isActived = true;
      updateData.selected.total = updateData.car.length;
      updateData.car.map((value, i) => {
        return updateData.car[i].selected = true
      });
    }
    else {
      updateData.car[index].selected = true;
      updateData.car.map((value, i) => {
        return totalSelected = (value.selected === true) ? totalSelected + 1 : totalSelected;
      });

      updateData.selected.total = totalSelected;
      updateData.selected.isActived = (totalSelected === updateData.car.length) ? true : false;
    }

    this.setState({ data: updateData });
  }

  handleSelectReason = (e) => {
    let index = parseInt(e.currentTarget.dataset.index);

    const updateData = this.state.data;

    updateData.reason.map((value, i) => {
      return updateData.reason[i].selected = (index === i) ? true : false;
    });

    this.setState({ data: updateData });
  }

  render() {
    const data = this.state.data.car, { showModal, onClose } = this.props;

    let carItem = data.map((value, index) => {
      return (
        <div className="car-item" key={index}>
          <div className="car-item-image">
            <img src={value.image} alt="img" />
          </div>
          <div className="car-item-title">
            <h3>{value.title}</h3>
          </div>
          <div className="car-item-select">
            <span>Select car</span>
            <div data-index={index} onClick={this.handleSelect} className={`select-icon ${value.selected ? 'selected' : ''}`} />
          </div>
        </div>
      )
    }), buttonSubmit = null,
      reasonItem = this.state.data.reason.map((value, index) => {
        return (
          <div className="reason-item" key={index}>
            <div data-index={index} onClick={this.handleSelectReason} className={`select-icon ${value.selected ? 'selected' : ''}`} />
            <span>{value.title}</span>
          </div>
        )
      });

    if (this.state.data.selected.total > 0 && this.state.step === 0) {
      buttonSubmit = <div className="success">
        <Button primary onClick={this.handleSubmit}>select {this.state.data.selected.total} car</Button>
      </div>;
    }

    if (this.state.step === 1) {
      buttonSubmit = <div className="success two-button">
        <Button primary onClick={this.handleSubmit}>cancel this booking</Button>
        <Button outline onClick={onClose}>don't cancel</Button>
      </div>;
    }

    return (
      <Modal
        open={showModal}
        extraClass="modal-booking-cancel"
        onClick={onClose}
        size="medium"
        header={{
          withCloseButton: false,
          onClick: () => console.log("header close"),
          children: (
            <div className="dashboard-popup-header">
              <p>Cancel Booking</p>
              <div className="icon-close" onClick={onClose} />
            </div>
          )
        }}
        content={{
          children: (
            <div className="dashboard-popup-content">
              <div className="booking-cancel-detail">
                <div className="information left-content">
                  <h3>Cancellation is Free</h3>
                  <p><b>For: 10 days 7 hours 45 minutes</b></p>
                </div>
                <div className="status right-content">
                  <div className="status-detail">
                    <StatusProgressBar data={this.state.data.date} />
                  </div>
                </div>
              </div>
              <div className={`booking-cancel-detail-car-wrapper ${this.state.step === 0 ? 'show' : ''}`}>
                <div className="booking-cancel-detail-car-title">
                  <div className="col-6"><h3>Which car do you want to cancel</h3></div>
                  <div className="col-6">
                    <span>Select all car</span>
                    <div data-index="all" onClick={this.handleSelect} className={`select-icon ${this.state.data.selected.isActived ? 'selected' : ''}`} />
                  </div>
                </div>
                <div className="booking-cancel-detail-car-list">{carItem}</div>
              </div>
              <div className={`booking-cancel-detail-reason-wrapper ${this.state.step === 1 ? 'show' : ''}`}>
                <h2>Tell us your reason for cancelling</h2>
                <h3>This is needed to complete your cancellation</h3>
                {reasonItem}
              </div>
            </div>
          )
        }}
        footer={{
          position: "left",
          children: (
            <Fragment>
              {buttonSubmit}
            </Fragment>
          )
        }}
      />
    );
  }
}

export default ModalCancelBooking;
