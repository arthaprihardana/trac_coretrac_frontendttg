import { Button, Modal } from "atom";
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { Component, Fragment } from "react";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import DateFormInput from '../../../../../organisms/RentForm/FormWrapper/CarRentForm';
import './ModalModifyBookingSelectPayment.scss';



class ModalModifyBooking extends Component {
  state = {
    showDatePicker: false,
    showPackage: false,
    showPayment: false,
    clickFirstDate: false,
    monthShown: 2,
    minDate: moment(),
    startDate: moment(),
    endDate: moment(),
    step: this.props.stepPopup,
    data: {
      modify: {
        current: {
          date: {
            start: '01-12-2018',
            end: '10-12-2018'
          },
          package: 4,
          price: 1025000
        },
        new: {
          date: {
            start: '01-12-2018',
            end: '10-12-2018'
          },
          package: 4,
          price: 200000
        },
        change: {
          datepicker: false,
          package: false,
        }
      }
    }
  }

  handleSubmit = () => {
    this.props.onSubmit('success');
  }

  onClickDatePicker = (e) => {
    let { showDatePicker } = this.state;
    this.setState({ showDatePicker: !showDatePicker, showPackage: false });
  }

  onClickPackage = (e) => {
    let { showPackage } = this.state;
    this.setState({ showDatePicker: false, showPackage: !showPackage });
  }

  handleSelectPackage = (value) => {
    let { showPayment, showPackage, data } = this.state;

    data.modify.new.package = value;
    data.modify.change.package = true;

    if ((data.modify.change.package) && (data.modify.change.datepicker)) {
      showPayment = true;
    }

    this.setState({ showPayment: showPayment, showPackage: !showPackage, data: data });
  }

  handleChangeDate = (date, event) => {
    let value = {};
    // let changeStatus = '';

    let { clickFirstDate, startDate, endDate, showPayment, data } = this.state;

    // select date
    if (event !== undefined) {
      // select end date
      if (clickFirstDate) {

        // set state
        this.setState({
          minDate: moment(),
          endDate: date,
          clickFirstDate: false,
        });

        // set value
        value['selectDate'] = date;
        value['startDate'] = startDate;
        value['endDate'] = date;
        // console.log(value);
        // status
        // changeStatus = 'select-end-date';

        // select first date
      } else {

        // set state
        this.setState({
          minDate: date,
          startDate: date,
          endDate: date,
          clickFirstDate: true
        });

        // set value
        value['selectDate'] = date;
        value['startDate'] = date;
        value['endDate'] = date;

        // status
        // changeStatus = 'select-first-date';

      }

      // select time
    } else {

      // set state
      this.setState({ startDate: date });

      // set value
      value['startDate'] = startDate;
      value['selectDate'] = date;
      value['endDate'] = endDate;

      // status
      // changeStatus = 'select-time';
    }

    //modify data date
    data.modify.new.date.start = value['startDate'].format("DD-MM-YYYY");
    data.modify.new.date.end = value['endDate'].format("DD-MM-YYYY");

    data.modify.change.datepicker = true;
    if ((data.modify.change.package) && (data.modify.change.datepicker)) {
      showPayment = true;
    }

    this.setState({ showPayment: showPayment, data: data });
  }

  render() {
    let datePickerConfig = {
      monthShown: 2,
      intervalTime: 30,
      highlightDates: [
        {
          date: '2018-11-29',
          description: '29 Nov : Lorem ipsum day'
        },
        {
          date: '2018-12-25',
          description: '25 Okt: Dolor sit day'
        }
      ],
      onClickButton: this.handleSelectDate,
      textButton: 'Done',
      sendValue: this.getValueDateForm,
    };

    // highlightdate list
    let highlightDatesList = [];

    datePickerConfig.highlightDates.forEach((value) => {
      highlightDatesList.push(moment(value.date));
    });

    const data = this.state.data.modify, { showModal, onClose } = this.props;

    let bookingList = (
      <Fragment>
        <div className="booking-modify-detail-item current-booking">
          <h2>Current Booking</h2>
          <div className="desc-wrapper">
            <div className="desc-item">
              <sup>Dates</sup>
              <div className="desc-item-value">{moment(data.current.date.start, "DD-MM-YYYY").format('D MMM YYYY')} - {moment(data.current.date.end, "DD-MM-YYYY").format('D MMM YYYY')}</div>
            </div>
            <div className="desc-item">
            </div>
            <div className={`desc-item ${!this.state.showPayment ? 'disabled' : ''}`}>
              <sup>Total Payment</sup>
              <div className="desc-item-value">Rp {data.current.price.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</div>
            </div>
          </div>
        </div>
        <div className="booking-modify-detail-item new-booking">
          <h2>New Booking</h2>
          <div className="desc-wrapper">
            <div className="desc-item">
              <sup>Dates</sup>
              <div className={`form-input ${this.state.showPayment ? 'disabled' : ''}`}>
                <div className="form-date" onClick={this.onClickDatePicker}>
                  <span>{moment(data.new.date.start, "DD-MM-YYYY").format('D MMM YYYY')} - {moment(data.new.date.end, "DD-MM-YYYY").format('D MMM YYYY')}</span>
                </div>
                <div className={`trac-calendar-theme ${this.state.showDatePicker ? 'show' : ''}`}>
                  <div className="trac-calendar-title">
                      <div className="title-calendar">Calendar</div>
                      <div className="title-time">Pick Up Hour</div>
                  </div>
                  <DatePicker
                    inline
                    showTimeSelect
                    timeFormat="HH:mm"
                    monthsShown={datePickerConfig.monthShown}
                    timeIntervals={datePickerConfig.intervalTime}
                    minDate={this.state.minDate}
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    selected={this.state.startDate}
                    onSelect={(e) => this.handleSelectPackage(4)}
                    onChange={this.handleChangeDate}
                    highlightDates={highlightDatesList}
                    className="hour-picker"
                  />
              
                </div>
              </div>
              <div className={`desc-item-value ${!this.state.showPayment ? 'disabled' : ''}`}>{moment(data.new.date.start, "DD-MM-YYYY").format('D MMM YYYY')} - {moment(data.new.date.end, "DD-MM-YYYY").format('D MMM YYYY')}</div>
            </div>
            <div className="desc-item">
            </div>
            <div className={`desc-item ${!this.state.showPayment ? 'disabled' : ''}`}>
              <sup>Total Payment</sup>
              <div className="desc-item-value">Rp {(data.new.price + data.current.price).toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</div>
            </div>
          </div>
        </div>
      </Fragment >
    );

    return (
      <Fragment>
        <Modal
          open={showModal}
          extraClass="modal-modify-booking"
          onClick={onClose}
          size="medium"
          header={{
            withCloseButton: false,
            onClick: () => console.log("header close"),
            children: (
              <div className="dashboard-popup-header">
                <p>Modify Booking</p>
                <div className="icon-close" onClick={onClose} />
              </div>
            )
          }}
          content={{
            children: (
              <div className="dashboard-popup-content">
                <div className="booking-modify-detail">
                  <h4>Please enter the date you want to modify your booking to</h4>
                  <div className="booking-modify-detail-list">
                    {bookingList}
                  </div>
                </div>
              </div>
            )
          }}
          footer={{
            position: "left",
            children: (
              <div className="success two-button">
                <Button primary onClick={this.handleSubmit}>modify booking</Button>
                <Button outline onClick={onClose}>don't modify</Button>
              </div>
            )
          }}
        />
      </Fragment>
    );
  }
}

// default props
DateFormInput.defaultProps = {
  titleCalendar: 'Calendar',
  titleTime: 'Pick up hour',
  timeFormat: 'HH:mm',
  intervalTime: 30,
  indexElement: 0,
  indexForm: 0,
  highlightDates: [
      {
          date: '2018-11-29',
          description: '29 Nov : Lorem ipsum day'
      },
      {
          date: '2018-12-25',
          description: '25 Okt: Dolor sit day'
      }
  ],
  onClickButton: () => { },
  textButton: 'Done',
};

// props types
ModalModifyBooking.propTypes = {
  titleCalendar: PropTypes.string,
  titleTime: PropTypes.string,
  timeFormat: PropTypes.string,
  intervalTime: PropTypes.number,
  indexElement: PropTypes.number,
  indexForm: PropTypes.number,
  highlightDates: PropTypes.arrayOf(
      PropTypes.shape({
          date: PropTypes.string,
          description: PropTypes.string,
      }),
  ),
  onClickButton: PropTypes.func,
  textButton: PropTypes.string,
};

export default ModalModifyBooking;
