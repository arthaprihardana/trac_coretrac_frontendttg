import { Modal } from "atom";
import cx from 'classnames';
import { delay } from 'helpers';
import { Accordion } from 'organisms';
import React, { Component, Fragment } from "react";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import * as moment from 'moment';
import lang from "../../../../../assets/data-master/language";

class ModalPaymentGuide extends Component {
    state = {
        accountNumber: this.props.dataVA,
        content_atmMandiri:'',
        content_atmMandiriDetail:'',
        content_atmPermata:'',
        content_atmPermataDetail:'',
        content_internetMandiri:'',
        content_internetMandiriDetail:'',
        content_internetPermata:'',
        content_internetPermataDetail:'',
        popupPaymentGuide: false,
        copied: false
    };

    componentDidMount(){

        this.setState({
            VANumber:this.props.dataVA,
            content_atmMandiri:lang.pembayaranViaATMMandiri[lang.default],
            content_atmMandiriDetail:lang.pembayaranViaATMMandiriItem[lang.default],
            content_atmPermata: lang.pembayaranViaATMPermata[lang.default],
            content_atmPermataDetail: lang.pembayaranViaATMPermataItem[lang.default],
            content_internetMandiri:lang.pembayaranViaMandiriOnline[lang.default],
            content_internetMandiriDetail:lang.pembayaranViaMandiriOnlineItem[lang.default],
            content_internetPermata:lang.pembayaranViaMobilePermata[lang.default],
            content_internetPermataDetail:lang.pembayaranViaMobilePermataItem[lang.default]
        });

        // console.log(this.props.dataVA);
    }
    handleCopy = () => {
        this.setState(
            {
                copied: true
            },
            () => {
                delay(1000).then(() => {
                    this.setState({ copied: false });
                });
            }
        );
    };
    msToTime = (duration)=> {
        var milliseconds = parseInt((duration % 1000) / 100),
            seconds = Math.floor((duration / 1000) % 60),
            minutes = Math.floor((duration / (1000 * 60)) % 60),
            hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + " Jam " + minutes + " Menit " + seconds + " detik ";
    };
    render() {
        const { showModal, onClose, dataVA, dateExp } = this.props;
        let { accountNumber, copied, content_atmMandiri, content_atmMandiriDetail, content_atmPermata,content_atmPermataDetail,content_internetMandiri,content_internetMandiriDetail,content_internetPermata,content_internetPermataDetail} = this.state;
        const classAccountNumber = cx("account-number-wrapper", {
            copied: copied
        });
        return (
            <Modal
                open={showModal}
                extraClass="modal-payment-guide"
                onClick={onClose}
                header={{
                    withCloseButton: true,
                    onClick: onClose,
                    children: ""
                }}
                content={{
                    children: (
                        <Fragment>
                            <div className="info-header">
                                <h3>PermataBank Virtual Account</h3>
                            </div>
                            <p className="sub-title">
                                Please complete your payment in{" "}
                                <strong>{ this.msToTime(moment(dateExp).diff(moment())) }</strong>
                            </p>
                            <div className="detail-virtual-account">
                                <div className={classAccountNumber}>
                                    <p className="account-number">
                                        Virtual Account Number:{" "}
                                        <span>{dataVA}</span>
                                        {/*<span>{accountNumber}</span>*/}
                                    </p>
                                    <CopyToClipboard
                                        text={dataVA}
                                        onCopy={this.handleCopy}
                                    >
                                        {/* eslint-disable-next-line */}
                                        <a className="copy-account-number">
                                            copy
                                        </a>
                                    </CopyToClipboard>
                                </div>
                                <Accordion
                                    title={content_atmMandiri}
                                    defaultValue={true}
                                >
                                    <div style={{whiteSpace:'pre', overflowX:'auto'}}>
                                        {content_atmMandiriDetail}
                                    </div>
                                </Accordion>
                                <Accordion title={content_internetMandiri}>
                                    <div style={{whiteSpace:'pre', overflowX:'auto'}}>
                                        {content_internetMandiriDetail}
                                    </div>
                                </Accordion>
                                <Accordion title={content_atmPermata}>
                                    <div style={{whiteSpace:'pre', overflowX:'auto'}}>
                                        {content_atmPermataDetail}
                                    </div>
                                </Accordion>
                                <Accordion title={content_internetPermata}>
                                    <div style={{whiteSpace:'pre', overflowX:'auto'}}>
                                        {content_internetPermataDetail}
                                    </div>
                                </Accordion>
                            </div>
                        </Fragment>
                    )
                }}
                footer={{
                    position: "center",
                    children: ""
                }}
            />
        );
    }
}

export default ModalPaymentGuide;
