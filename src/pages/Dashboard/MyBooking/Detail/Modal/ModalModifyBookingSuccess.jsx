import { Button, Modal } from "atom";
import React, { Component, Fragment } from "react";

class ModalCancelBookingSuccess extends Component {
  state = {
    data: {
      price: 200000
    }
  }
  onSubmit = () => {
    this.props.onClose();
  }

  render() {
    const { data } = this.state, { showModal, onClose } = this.props;

    return (
      <Modal
        open={showModal}
        extraClass="modal-modify-booking confirm"
        onClick={onClose}
        size="medium"
        header={{
          withCloseButton: false,
          onClick: () => console.log("header close"),
          children: ''
        }}
        content={{
          children: (
            <div className="dashboard-popup-content">
              <div className="success-text">
                <h2>Bryan, Are you sure you want<br></br>to modify your booking? </h2>
                <h3>The amount of Rp <span>{data.price.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</span> will be charged to your<br></br>registered Payment Method. </h3>
              </div>
            </div>
          )
        }}
        footer={{
          position: "center",
          children: (
            <Fragment>
              <div className="success-block ">
                <Button primary onClick={this.onSubmit}>confirm modify booking</Button>
              </div>
              <div className="success-block ">
                <Button outline onClick={onClose}>no, don't modify</Button>
              </div>
              <br/>
              <br/>
            </Fragment>
          )
        }}
      />
    );
  }
}

export default ModalCancelBookingSuccess;
