// libraries
import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import { Button } from "atom";
import { dashboardPopupSetup } from "helpers";

import StatusProgressBar from "./StatusProgressBar";

// modal
import ModalCancelBooking from "./Modal/ModalCancelBooking";
import ModalCancelBookingSuccess from "./Modal/ModalCancelBookingSuccess";

class CancelDetail extends PureComponent {
    state = {
        data: {
            date: {
                start: "01-12-2018",
                end: "10-12-2018",
                current: "02-12-2018"
            },
            status: {
                current: 1,
                cancel: {
                    fee: {
                        start: "03-12-2018",
                        end: "06-12-2018"
                    }
                }
            }
        },
        cancelBookingPopup: false,
        cancelBookingSuccessPopup: false,
        selectedCar: [],
        showModal: false
    };

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    handleModal = () => {
        dashboardPopupSetup(true);
        this.setState({ cancelBookingPopup: !this.state.cancelBookingPopup });
    };

    handleNext = value => {
        if (value === "success") {
            this.setState({
                cancelBookingPopup: false,
                cancelBookingSuccessPopup: true
            });
        }
    };

    closeCancelBookingPopup = () => {
        dashboardPopupSetup(false);
        this.setState({
            cancelBookingPopup: false,
            cancelBookingSuccessPopup: false
        });
    };

    render() {
        const {
                data,
                cancelBookingPopup,
                cancelBookingSuccessPopup
            } = this.state,
            { handleNext, closeCancelBookingPopup } = this;

        return (
            <Fragment>
                <div className="booking-cancel-detail">
                    <div className="information left-content">
                        <h3>Cancellation is Free</h3>
                        <p>
                            <b>For: 10 days 7 hours 45 minutes</b>
                        </p>
                        <p>
                            To cancel for free, you need to cancel
                            <br />
                            before Nov 30 at 11:59 PM Jakarta time
                        </p>
                    </div>
                    <div className="status right-content">
                        <StatusProgressBar data={data} />
                        <div className="status-detail" />
                        <div className="status-button">
                            <Button
                                type="button"
                                outline
                                onClick={this.handleModal}
                            >
                                cancel booking
                            </Button>
                        </div>
                    </div>
                </div>
                <ModalCancelBooking
                    data={data}
                    showModal={cancelBookingPopup}
                    onSubmit={handleNext}
                    onClose={closeCancelBookingPopup}
                    stepPopup={0}
                />
                <ModalCancelBookingSuccess
                    showModal={cancelBookingSuccessPopup}
                    onSubmit={handleNext}
                    onClose={closeCancelBookingPopup}
                />
            </Fragment>
        );
    }
}

CancelDetail.defaultProps = {
    data: []
};

CancelDetail.propTypes = {
    data: PropTypes.array
};

export default CancelDetail;
