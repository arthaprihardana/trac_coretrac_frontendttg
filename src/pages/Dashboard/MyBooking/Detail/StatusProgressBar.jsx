// libraries
import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";

// plugin component
import moment from 'moment';

class StatusProgressBar extends PureComponent {
  state = {
    data: this.props.data
  };

  getProgressStatusBar = (data) => {
    let end = moment(data.date.end, "DD-MM-YYYY"),
      start = moment(data.date.start, "DD-MM-YYYY"),
      current = moment(data.date.current, "DD-MM-YYYY"),
      startcancel = moment(data.status.cancel.fee.start, "DD-MM-YYYY"),
      endcancel = moment(data.status.cancel.fee.end, "DD-MM-YYYY"),
      day = {
        currentDays: Math.abs(moment.duration(start.diff(current)).asDays()),
        totalDays: Math.abs(moment.duration(start.diff(end)).asDays()),
        freeDays: Math.abs(moment.duration(start.diff(startcancel)).asDays()),
        cancelFeeDays: Math.abs(moment.duration(startcancel.diff(endcancel)).asDays()),
        nonRefundable: Math.abs(moment.duration(endcancel.diff(end)).asDays()),
      },
      rangeDays = [
        { width: day.freeDays / day.totalDays * 100 },
        { width: day.cancelFeeDays / day.totalDays * 100 },
        { width: day.nonRefundable / day.totalDays * 100 },
      ],
      activeDays = 0;

    if (day.currentDays >= (day.freeDays + day.cancelFeeDays)) {
      activeDays = 2;
    }
    else if (day.currentDays >= day.freeDays) {
      activeDays = 1;
    }

    return {
      activeDays,
      rangeDays
    }
  }

  render() {
    const { data } = this.state;

    let { activeDays, rangeDays } = this.getProgressStatusBar(data);

    return (
      <Fragment>
        <div className="status-progress-wrapper">
          <div className="title-cancel">
            <p style={{ width: rangeDays[0].width + '%' }}><span>FREE</span></p>
            <p style={{ width: rangeDays[1].width + '%' }}><span>Cancellation Fees<br></br>Rp 100.000</span></p>
            <p style={{ width: rangeDays[2].width + '%' }}><span>Non-Refundable</span></p>
          </div>
          <div className="cancel-wrapper">
            <div className="cancel-section" style={{ width: rangeDays[0].width + '%' }}>
              <div className={`status ${activeDays === 0 ? 'active' : ''}`}>
                <div className={`bulet ${activeDays === 0 ? 'active' : ''}`}></div>
              </div>
            </div>
            <div className="cancel-section" style={{ width: rangeDays[1].width + '%' }}>
              <div className={`status ${activeDays === 1 ? 'active' : ''}`}>
                <div className={`bulet ${activeDays === 0 || activeDays === 1 ? 'active' : ''}`}></div>
              </div>
            </div>
            <div className="cancel-section" style={{ width: rangeDays[2].width + '%' }}>
              <div className={`status ${activeDays === 2 ? 'active' : ''}`}>
                <div className={`bulet ${activeDays === 1 || activeDays === 2 ? 'active' : ''}`}></div>
                <div className={`bulet last ${activeDays === 2 ? 'active' : ''}`}></div>
              </div>
            </div>
          </div>
          <div className="title-dates">
            <p style={{ width: rangeDays[0].width + '%' }}><span>{moment(data.date.start, "DD-MM-YYYY").format("MMM D")}</span></p>
            <p style={{ width: rangeDays[1].width + '%' }}><span>{moment(data.status.cancel.fee.start, "DD-MM-YYYY").format("MMM D")}</span></p>
            <p style={{ width: rangeDays[2].width + '%' }}><span>{moment(data.status.cancel.fee.end, "DD-MM-YYYY").format("MMM D")}</span></p>
          </div>
        </div>
      </Fragment>
    );
  }
}

StatusProgressBar.defaultProps = {
  data: []
};

StatusProgressBar.propTypes = {
  data: PropTypes.object
};

export default StatusProgressBar;