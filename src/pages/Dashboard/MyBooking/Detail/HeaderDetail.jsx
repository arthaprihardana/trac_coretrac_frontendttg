/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-24 23:09:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-25 00:35:37
 */
// libraries
import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import { dashboardPopupSetup } from "helpers";

//image
import BackButtonIcon from "../../../../assets/images/icons/arrow-left-orange.svg";
import HeaderButtonIcon from "../../../../assets/images/icons/lingkarplus-blue.svg";

//1
import ModalModifyBooking from "./Modal/ModalModifyBooking";
//2
import ModalModifyBookingSelectPayment from "./Modal/ModalModifyBookingSelectPayment";
//3
import ModalModifyBookingConfirmPayment from "./Modal/ModalModifyBookingConfirmPayment";
import lang from '../../../../assets/data-master/language'
//4
import ModalModifyBookingSuccess from "./Modal/ModalModifyBookingSuccess";
import ModalExtendedBooking from "./Modal/ModalExtendedBooking";
import { Link } from "react-router-dom";
// import "./HeaderDetail.scss";

class HeaderDetail extends PureComponent {
    state = {
        modifyBookingPopup: false,
        modifyBookingSelectPaymentPopup: false,
        modifyBookingConfirmPaymentPopup: false,
        modifyBookingSuccessPopup: false,
        extendedBookingPopup: false,
        selectedCar: [],
        activeCar: {},
        indexActive: 0,
        showModal: false
    };

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    callExtended = e => {
        dashboardPopupSetup(true);
        this.setState({ extendedBookingPopup: !this.state.extendedBookingPopup });
        e.preventDefault();
    };

    handleExtended = e => {
        this.setState({extendedBookingPopup: false});
    }

    handleNext = value => {
        if (value === "success") {
            this.setState({
                modifyBookingPopup: false,
                modifyBookingSuccessPopup: true
            });
        }
    };

    callModify = e => {
        dashboardPopupSetup(true);
        this.setState({ modifyBookingPopup: !this.state.modifyBookingPopup });
        e.preventDefault();
    };

    handleNext = value => {
        if (value === "success") {
            this.setState({
                modifyBookingPopup: false,
                modifyBookingSelectPaymentPopup: true,
                modifyBookingConfirmPaymentPopup: false,
                modifyBookingSuccessPopup: false
            });
        }
    };

    handleToSelectionPayment = value => {
        if (value === "success") {
            this.setState({
                modifyBookingPopup: false,
                modifyBookingSelectPaymentPopup: false,
                modifyBookingConfirmPaymentPopup: true,
                modifyBookingSuccessPopup: false
            });
        }
    };

    handleToConfirmPayment = value => {
        if (value === "success") {
            this.setState({
                modifyBookingPopup: false,
                modifyBookingSelectPaymentPopup: false,
                modifyBookingConfirmPaymentPopup: false,
                modifyBookingSuccessPopup: true
            });
        }
    };

    closeModifyBookingPopup = () => {
        dashboardPopupSetup(false);
        this.setState({
            modifyBookingPopup: false,
            modifyBookingSelectPaymentPopup: false,
            modifyBookingConfirmPaymentPopup: false,
            modifyBookingSuccessPopup: false
        });
    };

    closeExtendedBookingPopup = () => {
        dashboardPopupSetup(false);
        this.setState({
            extendedBookingPopup: false
        });
    };

    render() {
        const { 
            modifyBookingPopup, 
            modifyBookingSelectPaymentPopup,
            modifyBookingConfirmPaymentPopup,
            modifyBookingSuccessPopup,
            extendedBookingPopup
        } = this.state,
        {
            props: { page, data },
            handleExtended,
            handleNext,
            handleToSelectionPayment,
            handleToConfirmPayment,
            callModify,
            callExtended,
            closeModifyBookingPopup,
            closeExtendedBookingPopup
        } = this;

        let backLink = "/dashboard/my-booking";
        if (page === "history-booking") {
            backLink = "/dashboard/history-booking";
        }

        return (
            <Fragment>
                <div className="booking-header-detail">
                    <div className="detail-item back">
                        <Link className="detail-item-back" to={backLink}>
                            <img src={BackButtonIcon} alt="icon" />
                            {lang.backToAllBooking[lang.default]}
                        </Link>
                    </div>
                    <div className="detail-item booking-number">
                        <sup>{lang.reservationNumber[lang.default]}</sup>
                        <h3>{data.ReservationId}</h3>
                    </div>
                    <div className="detail-item car-total">
                        <sup>{lang.carTotal[lang.default]}</sup>
                        <h3>{data.carTotal} {data.carTotal > 1 ? lang.cars[lang.default] : lang.car[lang.default]}</h3>
                    </div>
                    {/*{page !== "history-booking" && (*/}
                        {/*<Fragment>*/}
                            {/*<div className="detail-item update-order">*/}
                                {/*<a*/}
                                    {/*className="detail-item-button"*/}
                                    {/*href="/"*/}
                                    {/*onClick={callExtended}*/}
                                {/*>*/}
                                    {/*<img src={HeaderButtonIcon} alt="icon" />*/}
                                    {/*<span>extend</span>*/}
                                {/*</a>*/}
                            {/*</div>*/}
                            {/* <div className="detail-item update-order">
                                <a
                                    className="detail-item-button"
                                    href="/"
                                    onClick={callModify}
                                >
                                    <img src={HeaderButtonIcon} alt="icon" />
                                    <span>modify</span>
                                </a>
                            </div> */}
                        {/*// </Fragment>*/}
                    {/*)}*/}
                    <ModalModifyBooking
                        showModal={modifyBookingPopup}
                        onSubmit={handleNext}
                        onClose={closeModifyBookingPopup}
                        stepPopup={0}
                    />
                    <ModalModifyBookingSelectPayment
                        showModal={modifyBookingSelectPaymentPopup}
                        onSubmit={handleToSelectionPayment}
                        onClose={closeModifyBookingPopup}
                    />
                    <ModalModifyBookingConfirmPayment
                        showModal={modifyBookingConfirmPaymentPopup}
                        onSubmit={handleToConfirmPayment}
                        onClose={closeModifyBookingPopup}
                    />
                    <ModalModifyBookingSuccess
                        showModal={modifyBookingSuccessPopup}
                        onSubmit={handleNext}
                        onClose={closeModifyBookingPopup}
                    />
                    <ModalExtendedBooking
                        showModal={extendedBookingPopup}
                        onSubmit={handleExtended}
                        onClose={closeExtendedBookingPopup}
                        stepPopup={0}
                    />
                </div>
            </Fragment>
        );
    }
}
//
// HeaderDetail.defaultProps = {
//     data: []
// };
//
// HeaderDetail.propTypes = {
//     data: PropTypes.array
// };

export default HeaderDetail;
