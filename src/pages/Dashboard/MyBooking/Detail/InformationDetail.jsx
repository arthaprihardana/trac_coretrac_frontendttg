/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-25 00:44:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-25 01:24:47
 */
// libraries
import React, { PureComponent } from "react";
import { Rupiah } from "atom";
import PropTypes from "prop-types";
import moment from "moment";
import "moment/locale/id";
import _ from "lodash";
import {AIRPORT, CAR_RENTAL, BU_BUS, BU_CAR} from "../../../../constant"

//image
import ArrowLeftBack from "../../../../assets/images/icons/arrow-left-black.svg";
import lang from '../../../../assets/data-master/language'
// style
// import "./DriverDetail.scss";


class DriverDetail extends PureComponent {
	state = {
		airport:AIRPORT,
		bus:BU_BUS,
		car_rental:CAR_RENTAL,
		selectedCar: [],
		activeCar: {},
		indexActive: 0
	};

	componentDidMount() {

	}

	componentWillReceiveProps(nextProps) {
	}

	render() {
		const { data } = this.props;
		const { airport, car_rental, bus} = this.state;
		return (
			<div className="booking-information-detail">
				<div className="detail-items">
					<sup>packages</sup>
					{
						(data.MsProductId===airport && data.businessUnitId === BU_CAR) && <div className="detail-items-value">{data.Duration} KM</div>
					}
					{
						(data.MsProductId===car_rental && data.businessUnitId === BU_CAR)  && <div className="detail-items-value">{data.Duration} Hour</div>
					}
					{
						(data.businessUnitId === BU_BUS)  && <div className="detail-items-value">{data.Duration} Day</div>
					}

				</div>
				<div className="detail-items">
					<sup>dates</sup>
					<div className="detail-items-value">
						<div className="detail-items-date">
							<span>{moment(data.StartDate).format('ll')}</span>
                            {data.EndDate ? (
                                <span> <img src={ArrowLeftBack} alt="icon" /> {moment(data.EndDate).format('ll')}</span>) : null
                            }

							<span></span>
						</div>
						<div className="detail-items-time">
							<span>Start {moment(data.StartDate).format('HH:mm')}</span>
                            {data.EndDate ? (
								<span> {" "}-{" "} End {moment(data.EndDate).format('HH:mm')}</span>) : null
                            }
						</div>
					</div>
				</div>
				<div className="detail-items">
					<sup>{lang.pickUpLocation[lang.default]}</sup>
					<div className="detail-items-value">
						{data.PickupLocation}
					</div>
				</div>
				<div className="detail-items">
					<sup>{lang.notesRequest[lang.default]}</sup>
					<div className="detail-items-value">{data.NotesReservation !== null && data.NotesReservation !== "null" ? data.NotesReservation : "-"}</div>
				</div>
				<div className="booking-information-payment">
					<div className="detail-items-sup">{lang.paymentDetails[lang.default]}</div>
					{data.details && _.map(data.details, (v, k) => {
						let extras = v.reservation_extras.length > 0 ? _.filter(v.reservation_extras[0].details, o => parseInt(o.Qty) > 0 ) : [];
						let promo = v.reservation_promos;
						let extend = v.extend_durations;
						return (
							<div className="detail-items" key={k}>
								<sup>{v.UnitTypeName !== null ? v.UnitTypeName : v.UnitTypeId}</sup>
								<div className="detail-items-info-price">
									<span className="info">
										<Rupiah value={parseInt(v.Price)} /> 
										for {(v.TotalDay)} Day{parseInt(v.TotalDay) > 1 && '(s)'}
									</span>
									<span className="price">
										<Rupiah value={parseInt(v.Price)} />
									</span>
								</div>
                                {
                                    _.map(promo, (pp, kk)=> (
                                            <div className="detail-items-info-price" key={kk}>
											<span className="info">
												Promo
											</span>
                                                <span className="price">
												- <Rupiah value={parseInt(pp.Price)} />
											</span>
                                            </div>
                                        )
                                    )
                                }
								<span></span>
								{_.map(extras, (vv, kk) => (
									<div className="detail-items-info-price" key={kk}>
										<span className="info">
											{vv.MsExtrasName}{" "}
											x {vv.Qty} Item
										</span>
										<span className="price">
											<Rupiah value={parseInt(vv.Price)} />
										</span>
									</div>
								))}
								{_.map(extend, (vv, kk) => (
									<div className="detail-items-info-price" key={kk}>
										<span className="info">
											Additional Hour
										</span>
										<span className="price">
											<Rupiah value={parseInt(vv.AddDurationPrice)} />
										</span>
									</div>
								))}

							</div>
						)
					})}
					<div className="detail-items">
						<div className="detail-items-total">
							<sup><b>{lang.totalPayment[lang.default]}</b></sup>
							<span>
								{/* <Rupiah value={_.reduce(data.details, (sum, n) => sum + parseInt(n.Price) ,0)} /> */}
								<Rupiah value={parseInt(data.TotalPrice)} />
							</span>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
//
// DriverDetail.defaultProps = {
//   data: []
// };
//
// DriverDetail.propTypes = {
//   data: PropTypes.array
// };

export default DriverDetail;