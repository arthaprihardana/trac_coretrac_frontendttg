// libraries
import React, { PureComponent } from "react";


// Local component
import ActiveCarDetail from "./ActiveCarDetail";
import DriverDetail from "./DriverDetail";
// style
import "./CarDetail.scss";
import DriverAvatar from "../../../../assets/images/dummy/unknown.png";
import lang from '../../../../assets/data-master/language'

class CarDetail extends PureComponent {
  state = {
    type:'',
    selectedCar: [],
    activeCar: {},
    indexActive: 0
  };

  componentDidMount() {
    if (this.props.data) {
      this.setState({
        selectedCar: this.props.data,
        activeCar: this.props.data[this.state.indexActive]
      });
      if(this.props.carType === '0104'){
          this.setState({
              type:'car'
          });
      }else{
          this.setState({
              type:'bus'
          });
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const { selectedCar, indexActive } = this.state;

    if (nextProps.data !== undefined && nextProps.data.length !== selectedCar.length) {
      let index = indexActive !== 0 ? indexActive - 1 : 0;
      this.setState({
        selectedCar: nextProps.data,
        activeCar: nextProps.data[index],
        indexActive: index
      });
    }
  }

  changeActive = type => {
    const { selectedCar, indexActive } = this.state;
    let index = indexActive - 1;
    if (type === "next") index = indexActive + 1;
    this.setState({
      activeCar: selectedCar[index],
      indexActive: index
    });
  };

  prevAction = () => {
    if (this.state.indexActive !== 0) {
      this.changeActive("prev");
    }
  };

  nexAction = () => {
    const { indexActive, selectedCar } = this.state;
    if (indexActive !== selectedCar.length - 1) {
      this.changeActive("next");
    }
  };

  removeHandle = id => {
    let updatedCarList = [...this.state.selectedCar];
    let filteredCarList = updatedCarList.filter(car => car.id !== id);
    this.props.onRemove(filteredCarList);
  };

  render() {
    const {
      state: {
          type,
        selectedCar,
        activeCar
      }
    } = this;
    let activeCarDetail = null;
      let classProgress = 'car-on-the-way';
    if (selectedCar.length > 0) {
      activeCarDetail = <ActiveCarDetail activeCar={activeCar} type={type} />;
      selectedCar.map(car => {
        return (
          <div
            className={`item ${activeCar.id === car.id ? "active" : null}`}
            key={car.id}
          >
            <div className="image">
              <img src={car.image_url} alt={car.vehicleAlias} />
              <span
                className="cancel"
                onClick={() => this.removeHandle(car.id)}
              />
            </div>
          </div>
        );
      });
    }

    return (
        <div>
            <div className="o-car-detail">
                {/* car detail header */}
                <div className="car-detail-header">
                    {/* Navigation */}
                    <div
                        className="car-navigation prev-car"
                        onClick={this.prevAction}
                    />
                    <div
                        className="car-navigation next-car"
                        onClick={this.nexAction}
                    />
                    {activeCarDetail}
                </div>
            </div>
            <div className="booking-driver-detail">
                <div className="rating">
                    {/*<h3>Rating Driver</h3>*/}
                </div>
                <div className="information">
                    <h3>{lang.driverUnitInformation[lang.default]}</h3>
                    <div className="information-avatar">
                        <img src={DriverAvatar} alt="avatar" />
                    </div>
                    {
                        (Object.entries(activeCar).length !== 0 && activeCar.drivers.length>0) ? (
                                <ul className="information-list">
                                    <ul className="information-list">
                                        <li>{lang.name[lang.default]} : {activeCar.drivers[0].DriverName ? activeCar.drivers[0].DriverName : "-"}</li>
                                        <li>
                                            : {activeCar.drivers[0].Phone ? activeCar.drivers[0].Phone : "-"}</li>
                                        <li>{lang.vehicleLicensePlate[lang.default]} : {activeCar.LicensePlate ? activeCar.LicensePlate : "-"}</li>
                                    </ul>
                                </ul>
                            ) :
                            (
                                activeCar.MsProductServiceId === "PSV0001"  ?
                                    <ul className="information-list">
                                        <ul className="information-list">
                                            <li>{lang.name[lang.default]} :  - </li>
                                            <li>{lang.mobileNumber[lang.default]}:  -</li>
                                            <li>{lang.vehicleLicensePlate[lang.default]}  : {activeCar.LicensePlate ? activeCar.LicensePlate : "-"}</li>
                                        </ul>
                                    </ul>
                                    :  <ul className="information-list">
                                        <ul className="information-list">
                                            <li>{lang.name[lang.default]} :  -</li>
                                            <li>{lang.mobileNumber[lang.default]}:  -</li>
                                            <li>{lang.vehicleLicensePlate[lang.default]}  : -</li>
                                        </ul>
                                    </ul>
                            )
                    }
                </div>
            </div>
        </div>

    );
  }
}
//
// CarDetail.defaultProps = {
//   data: []
// };
//
// CarDetail.propTypes = {
//   data: PropTypes.array
// };

export default CarDetail;