import React, { Fragment } from 'react';

import {AIRPORT, CAR_RENTAL, BU_BUS, BU_CAR} from "../../../../constant"
const ActiveCarDetail = ({ activeCar, type }) => {
  return (
    <Fragment>
      <div className="top-section">
        <div className="description">
            {/*isAC: 1*/}
            {/*isAirbag: 0*/}
            {/*isCharger: 1*/}
            {/*isKaraoke: 0*/}
            {/*isWiFi: 1*/}
            {/*passengers: [{…}]*/}
            {/*pickup_locations: [{…}]*/}
            {/*reservation_extras: [{…}]*/}
            {/*reservation_flight_detail: []*/}
            {/*reservation_promos: []*/}
            {/*seatArrangement: "2-2"*/}
            {/*termAndCondition: "<ol><li>The&nbsp;<strong style="color: rgb(79, 79, 79);">Intellectual Property</strong>&nbsp;disclosure will inform users that the contents, logo and other visual media you created is your property and is protected by copyright laws.</li><li>A&nbsp;<strong style="color: rgb(79, 79, 79);">Termination</strong>&nbsp;clause will inform that users’ accounts on your website and mobile app or users’ access to your website and mobile (if users can’t have an account with you) can be terminated in case of abuses or at your sole discretion.</li><li>A&nbsp;<strong style="color: rgb(79, 79, 79);">Governing Law</strong>&nbsp;will inform users which laws govern the agreement. This should the country in which your company is headquartered or the country from which you operate your website and mobile app.</li><li>A&nbsp;<strong style="color: rgb(79, 79, 79);">Links To Other Web Sites</strong>&nbsp;clause will inform users that you are not responsible for any third party websites that you link to. This kind of clause will generally inform users that they are responsible for reading and agreeing (or disagreeing) with the Terms and Conditions or Privacy Policies of these third parties.</li><li>If your website or mobile app allows users to create content and make that content public to other users, a&nbsp;<strong style="color: rgb(79, 79, 79);">Content</strong>&nbsp;section will inform users that they own the rights to the content they have created. The “Content” clause usually mentions that users must give you (the website or mobile app developer) a license so that you can share this content on your website/mobile app and to make it available to other users.</li></ol><p><br></p>"*/}
            {/*totalLugagge: 0*/}
            {/*totalSeat: 4*/}
          <h4 className="car-name">{activeCar.vehicleAlias ? activeCar.vehicleAlias : activeCar.vehicleTypeDesc}</h4>
          <div className="car-service-description">
              {(activeCar.MsProductId === AIRPORT && activeCar.businessUnitId === BU_CAR) &&
                  'Airport Transfer'
              }
              {(activeCar.MsProductId === CAR_RENTAL && activeCar.businessUnitId === BU_CAR) &&
                  'Car Rental'
              }
              {activeCar.businessUnitId === BU_BUS &&
                  'BUS'
              }
            {/*{activeCar.rentalType}*/}
          </div>
          <div className="car-spesification">
            <div className="item passenger">{activeCar.totalSeat}</div>
            {
              type !== 'bus' && (
                <Fragment>
                  <div className="item baggage">{activeCar.totalLugagge}</div>
                  <div className="item transmission">{activeCar.IsTransmissionManual==1 ? <span>MT</span> : <span>AT</span> }</div>
                  {activeCar.isAC == 1 ? <div className="item air-conditioner">Air Conditioner</div> : null}
                  {activeCar.isAirbag == 1 ? <div className="item air-bag">Air Bag</div> : null}
                </Fragment>
              )
            }
            {activeCar.isKaraoke == 1 && <div className="item karaoke"></div>}
            {activeCar.isWiFi == 1 && <div className="item wifi"></div>}
            {activeCar.isCharger ==1 && <div className="item charger"></div>}
          </div>
        </div>
      </div>
      <div className="car-image">
        <img src={activeCar.image_url} alt={activeCar.vehicleAlias} />
      </div>
    </Fragment>
  )
}

export default React.memo(ActiveCarDetail);