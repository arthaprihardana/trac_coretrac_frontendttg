import { Button, Modal, TracField } from "atom";
import { Form, Formik } from "formik";
import React, { Component } from "react";
import * as Yup from "yup";
import starActive from "../../../../../assets/images/icons/star-active.svg";
import star from "../../../../../assets/images/icons/star-inactive.svg";

// validation schema
const AuthSchema = Yup.object().shape({
    comment: Yup.string()
        .min("2", "Please Fill your Feel!")
        .required("Comment is required!")
});

class ModalRateBooking extends Component {
    state = {
        data: {
            user: {
                name: "Bryan"
            },
            rate: {
                value: 0
            }
        }
    };

    addData = e => {
        let selector = e.currentTarget;
        if (selector.classList.contains("btn-outline")) {
            selector.classList.remove("btn-outline");
            selector.classList.add("btn-primary");
        } else {
            selector.classList.remove("btn-primary");
            selector.classList.add("btn-outline");
        }
    };

    selectRate = e => {
        let { data } = this.state,
            rate = parseInt(e.currentTarget.dataset.star);

        data.rate.value = rate;
        this.setState({ data: data });
    };

    onSubmit = () => {
        this.props.onSubmit("success");
    };

    render() {
        const { data } = this.state,
            { showModal, onClose } = this.props;
        let rateLength = 5,
            rateTemp = [];

        for (var i = 1; i <= rateLength; i++) {
            rateTemp.push(
                <span
                    className="rate-star"
                    key={i}
                    data-star={i}
                    onClick={this.selectRate}
                >
                    {this.state.data.rate.value >= i ? (
                        <img src={starActive} alt="icon" />
                    ) : (
                        <img src={star} alt="icon" />
                    )}
                </span>
            );
        }

        return (
            <Modal
                open={showModal}
                extraClass="modal-rate-booking"
                onClick={onClose}
                size="medium"
                header={{
                    withCloseButton: false,
                    onClick: () => console.log("header close"),
                    children: ""
                }}
                content={{
                    children: (
                        <div className="dashboard-popup-content">
                            <div className="success-text">
                                <h2>
                                    How was your ride today, {data.user.name}?
                                </h2>
                                <h3>
                                    Please rate your experience today to make us
                                    better.
                                </h3>
                            </div>
                            <div className="rate-star-wrapper">{rateTemp}</div>
                            <div className="rate-star-wrapper style-guide-space">
                                <br />
                                <br />
                                <button
                                    type="button"
                                    className="a-btn btn-outline"
                                    onClick={e => this.addData(e)}
                                >
                                    FRIENDLY DRIVER
                                </button>
                                <button
                                    type="button"
                                    className="a-btn btn-outline"
                                    onClick={e => this.addData(e)}
                                >
                                    EXTENSIVE KNOWLEDGE
                                </button>
                                <button
                                    type="button"
                                    className="a-btn btn-outline"
                                    onClick={e => this.addData(e)}
                                >
                                    GIVE MORE HELP
                                </button>
                                <button
                                    type="button"
                                    className="a-btn btn-outline"
                                    onClick={e => this.addData(e)}
                                >
                                    FAST AND SPEED REPLY
                                </button>
                                <button
                                    type="button"
                                    className="a-btn btn-outline"
                                    onClick={e => this.addData(e)}
                                >
                                    USEFULLY RELAXED
                                </button>
                            </div>
                            <br />
                            <br />
                            <Formik
                                initialValues={{
                                    comment: ""
                                }}
                                validationSchema={AuthSchema}
                            >
                                {({
                                    isSubmitting,
                                    handleBlur,
                                    handleChange,
                                    values
                                }) => (
                                    <Form>
                                        <TracField
                                            id="comment"
                                            name="comment"
                                            type="text"
                                            labelName="add your comment"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.comment}
                                        />
                                    </Form>
                                )}
                            </Formik>
                            <br />
                            <br />
                        </div>
                    )
                }}
                footer={{
                    position: "center",
                    children: (
                        <div className="success">
                            <Button
                                className="center"
                                primary
                                onClick={this.onSubmit}
                            >
                                Submit
                            </Button>
                        </div>
                    )
                }}
            />
        );
    }
}

export default ModalRateBooking;
