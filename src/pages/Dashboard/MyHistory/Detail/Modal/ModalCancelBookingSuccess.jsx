import { Button, Modal } from "atom";
import React, { Component } from "react";
import SuccessImg from '../../../../../assets/images/icons/success-cancel-booking.svg';

class ModalCancelBookingSuccess extends Component {
  onSubmit = () => {
    this.props.onClose();
  }

  render() {
    const { showModal, onClose } = this.props;
    return (
      <Modal
        open={showModal}
        extraClass="success"
        onClick={onClose}
        size="medium"
        header={{
          withCloseButton: false,
          onClick: () => console.log("header close"),
          children: ''
        }}
        content={{
          children: (
            <div className="dashboard-popup-content">
              <div className="img-thumb">
                <img src={SuccessImg} alt="success" />
              </div>
              <div className="success-text">
                <h2>Your ride have been cancelled</h2>
                <h3>Thank you for making reservation with us. </h3>
              </div>
            </div>
          )
        }}
        footer={{
          position: "center",
          children: (
            <div className="success two-button">
              <Button primary onClick={this.onSubmit}>book another car</Button>
              <Button outline onClick={this.onSubmit}>Done</Button>
            </div>
          )
        }}
      />
    );
  }
}

export default ModalCancelBookingSuccess;
