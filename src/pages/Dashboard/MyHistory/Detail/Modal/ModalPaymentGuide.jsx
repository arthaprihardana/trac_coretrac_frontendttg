import { Modal } from "atom";
import cx from 'classnames';
import { delay } from 'helpers';
import { Accordion } from 'organisms';
import React, { Component, Fragment } from "react";
import { CopyToClipboard } from 'react-copy-to-clipboard';

class ModalPaymentGuide extends Component {
    state = {
        accountNumber: this.props.dataVA,
        copied: false
    };

    componentDidMount(){
        // console.log("oke");
        this.setState({
            VANumber:this.props.dataVA
        });
        // console.log(this.props.dataVA);
    }
    handleCopy = () => {
        this.setState(
            {
                copied: true
            },
            () => {
                delay(1000).then(() => {
                    this.setState({ copied: false });
                });
            }
        );
    };
    render() {
        const { showModal, onClose, dataVA } = this.props;
        let { accountNumber, copied} = this.state;
        const classAccountNumber = cx("account-number-wrapper", {
            copied: copied
        });
        return (
            <Modal
                open={showModal}
                extraClass="modal-payment-guide"
                onClick={onClose}
                header={{
                    withCloseButton: true,
                    onClick: onClose,
                    children: ""
                }}
                content={{
                    children: (
                        <Fragment>
                            <div className="info-header">
                                <h3>PermataBank Virtual Account</h3>
                            </div>
                            <p className="sub-title">
                                Please complete your payment in{" "}
                                <strong>58 minutes 25 seconds</strong>
                            </p>
                            <div className="detail-virtual-account">
                                <div className={classAccountNumber}>
                                    <p className="account-number">
                                        Virtual Account Number:{" "}
                                        <span>{dataVA}</span>
                                        {/*<span>{accountNumber}</span>*/}
                                    </p>
                                    <CopyToClipboard
                                        text={dataVA}
                                        onCopy={this.handleCopy}
                                    >
                                        {/* eslint-disable-next-line */}
                                        <a className="copy-account-number">
                                            copy
                                        </a>
                                    </CopyToClipboard>
                                </div>
                                <Accordion
                                    title="Payment via ATM"
                                    defaultValue={true}
                                >
                                    <ol className="desc-list">
                                        <li>Choose Other Transaction</li>
                                        <li>Choose Transfer</li>
                                        <li>Choose Permata Virtual Account</li>
                                        <li>
                                            Enter Virtual Account Number{" "}
                                            <span>{dataVA}</span>
                                        </li>
                                        <li>Choose Correct and press Yes</li>
                                        <li>Keep Payment Proof</li>
                                        <li>Pyament Done</li>
                                    </ol>
                                </Accordion>
                                <Accordion title="Payment via ATM">
                                    <ol className="desc-list">
                                        <li>Choose Other Transaction</li>
                                        <li>Choose Transfer</li>
                                        <li>Choose Permata Virtual Account</li>
                                        <li>
                                            Enter Virtual Account Number{" "}
                                            <span>{dataVA}</span>
                                        </li>
                                        <li>Choose Correct and press Yes</li>
                                        <li>Keep Payment Proof</li>
                                        <li>Pyament Done</li>
                                    </ol>
                                </Accordion>
                            </div>
                        </Fragment>
                    )
                }}
                footer={{
                    position: "center",
                    children: ""
                }}
            />
        );
    }
}

export default ModalPaymentGuide;
