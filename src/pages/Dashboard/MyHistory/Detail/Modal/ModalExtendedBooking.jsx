import { Button, Modal } from "atom";
import React, { Component, Fragment } from "react";

class ModalExtendedBooking extends Component {
  state = {
    step: this.props.stepPopup,
    showPayment: false,
    data: {
      extended: {
        current: {
          price: 1025000,
          package: 4
        },
        new: {
          price: 200000,
          package: 0
        }
      }
    }
  }

  handleSubmit = () => {
    this.props.onSubmit('success');
  }

  onClickDatePicker = (e) => {
    let { showDatePicker } = this.state;
    this.setState({ showDatePicker: !showDatePicker, showPackage: false });
  }

  onClickPackage = (e) => {
    let { showPackage } = this.state;
    this.setState({ showDatePicker: false, showPackage: !showPackage });
  }

  handleSelectPackage = (value) => {
    let { showPayment, showPackage, data } = this.state;

    data.modify.new.package = value;
    data.modify.change.package = true;

    if ((data.modify.change.package) && (data.modify.change.datepicker)) {
      showPayment = true;
    }

    this.setState({ showPayment: showPayment, showPackage: !showPackage, data: data });
  }

  handleHours = (value) => {
    let { data, showPayment } = this.state;

    switch (value) {
      case 'add':
        data.extended.new.package = data.extended.new.package + 1;
        break;
      case 'remove':
        data.extended.new.package = data.extended.new.package - 1;
        break;
      default:
        break;
    }

    showPayment = (data.extended.new.package > 0) ? true : false;

    this.setState({ data: data, showPayment: showPayment });
  }

  render() {
    const data = this.state.data.extended, { showModal, onClose } = this.props;

    let bookingList = (
      <Fragment>
        <div className="booking-modify-detail-item current-booking">
          <h2>Current Package</h2>
          <div className="desc-wrapper">
            <div className="desc-item">
              <sup>Rental Package</sup>
              <div className="desc-item-value">{data.current.package} Hours</div>
            </div>
            <div className={`desc-item ${!this.state.showPayment ? 'disabled' : ''}`}></div>
          </div>
        </div>
        <div className="booking-modify-detail-item new-booking">
          <h2>Add Hours</h2>
          <div className="desc-wrapper">
            <div className="desc-item">
              <div className="desc-item-counter">
                <div className="desc-item-counter-minus" onClick={(e) => this.handleHours('remove')}>-</div>
                <div className="desc-item-counter-value">{data.new.package}</div>
                <div className="desc-item-counter-plus" onClick={(e) => this.handleHours('add')}>+</div>
              </div>
            </div>
            <div className={`desc-item ${!this.state.showPayment ? 'disabled' : ''}`}>
              <sup>Total Payment</sup>
              <div className="desc-item-value">Rp {(data.new.price + data.current.price).toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</div>
            </div>
          </div>
        </div>
      </Fragment >
    );

    return (
      <Fragment>
        <Modal
          open={showModal}
          extraClass="modal-extended-booking modal-modify-booking"
          onClick={onClose}
          size="small"
          header={{
            withCloseButton: false,
            onClick: () => console.log("header close"),
            children: (
              <div className="dashboard-popup-header">
                <p>Extended Booking</p>
                <div className="icon-close" onClick={onClose} />
              </div>
            )
          }}
          content={{
            children: (
              <div className="dashboard-popup-content">
                <div className="booking-modify-detail">
                  <h4>Please enter the date you want to modify your booking to</h4>
                  <div className="booking-modify-detail-list">
                    {bookingList}
                  </div>
                </div>
              </div>
            )
          }}
          footer={{
            position: "left",
            children: (
              <div className="success two-button">
                <Button primary onClick={this.handleSubmit}>add hours</Button>
                <Button outline onClick={onClose}>don't add hours</Button>
              </div>
            )
          }}
        />
      </Fragment>
    );
  }
}

export default ModalExtendedBooking;
