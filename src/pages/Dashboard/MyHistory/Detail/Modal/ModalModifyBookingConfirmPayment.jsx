import visaCard from "assets/images/logo/visa.png";
import { Button, Modal } from "atom";
import React, { Component, Fragment } from "react";
import CardDesc from "../../../AccountSettings/CreditCards/CardDesc";
import './ModalModifyBookingSelectPayment.scss';


class ModalModifyBookingConfirmPayment extends Component {
  state = {
    step: this.props.stepPopup, 
    id: "visa",
    cardName: "Visa",
    cardNumber: "****2222",
    cardHolder: "Bryan Barry Borang",
    cardExpiry: "08/21",
    cardImg: visaCard,
    isSelected: true,
    isShow: false
  }

  handleSubmit = () => {
    this.props.onSubmit('success');
  }

  render() {
    const { showModal, onClose } = this.props;

    let selectBank = (
      <div className="card-item "> 
        <div className="card-info">
          <div className="card-img">
              <img src={visaCard} alt="card" />
          </div>
          <div className="card-wrapper">
            <CardDesc label="Credit Card Number" value={this.state.cardNumber} />
            <CardDesc label="Name on Card" value={this.state.cardHolder} />
          </div>
          <CardDesc label="Expiry Date" value={this.state.cardExpiry} />
        </div >
      </div>
    );

    return (
      <Fragment>
        <Modal
          open={showModal}
          extraClass="modal-modify-booking"
          onClick={onClose}
          size="medium"
          header={{
            withCloseButton: false,
            onClick: () => console.log("header close"),
            children: (
              <div className="dashboard-popup-header">
                <p>Modify Booking</p>
                <div className="icon-close" onClick={onClose} />
              </div>
            )
          }}
          content={{
            children: (
              <div className="dashboard-popup-content">
                <div className="booking-modify-detail">
                  <h3>Credit / Debit Card</h3>
                  <h4>Please enter your bank account to refund your order</h4>
                
                  <div className="booking-modify-detail-list">
                    {selectBank}
                  </div>
                </div>
              </div>
            )
          }}
          footer={{
            position: "center",
            children: (
              <div className="success two-button">
                <Button primary onClick={this.handleSubmit}>modify booking</Button>
                <Button outline onClick={onClose}>don't modify</Button>
              </div>
            )
          }}
        />
      </Fragment>
    );
  }
}

export default ModalModifyBookingConfirmPayment;