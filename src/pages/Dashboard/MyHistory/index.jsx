/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-24 09:35:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 18:12:07
 */
import React, { Component, Fragment } from 'react';
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import './MyHistory.scss';
import { Button, Text } from "atom";
import { Pagination } from "molecules";
import { dashboardPopupSetup, encrypt } from "helpers";
import { getVehicleAttribute, getMyHistory } from "actions";
import { PulseLoader } from "react-spinners";
import moment from 'moment';
import 'moment/locale/id';
import product_master from 'assets/data-master/master-product.json';

// modal
import ModalRateBooking from "../MyBooking/Detail/Modal/ModalRateBooking";
import ModalPaymentGuide from "../MyBooking/Detail/Modal/ModalPaymentGuide";
//cars
import carsOne from "assets/images/dummy/cars/Innova.png";
import carsTwo from "assets/images/dummy/cars/alphard.png";
import * as _ from "lodash";

import lang from '../../../assets/data-master/language'

class MyHistory extends Component {
    
    state = {
        rateBookingPopup: false,
        loading: true,
        data: [],
        reservation: [],
        vehicle: [],
        page: 0,
        page_route: this.props.match.url.split("/")[2],
        popupPaymentGuide: false,
        lastPage: 0
    }

    rateSubmit = (value) => {
        dashboardPopupSetup(false);
        this.setState({ rateBookingPopup: false });
    }

    showPaymentGuide = () => {
        dashboardPopupSetup(true);
        this.setState({
            popupPaymentGuide: true
        })
    }
    popupPaymentClose = () => {
        dashboardPopupSetup(false);
        this.setState({
            popupPaymentGuide: false
        })
    }

    closeRateBookingPopup = () => {
        dashboardPopupSetup(false);
        this.setState({ rateBookingPopup: false })
    }
    componentWillUnmount() {
        this.setState({
            loading: true
        })
    }
    componentDidMount(){
        dashboardPopupSetup(false);
        let route_now = this.props.match.url.split("/")[2];
        this.props.getVehicleAttribute();
        this.props.getMyHistory();
        this.setState({
            data:[],
            loading: true
        })

        // this.setState({ rateBookingPopup: true });
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.myHistory !== prevProps.myHistory) {
            return { history: this.props.myHistory };
        }
        if(this.props.vehicleattr !== prevProps.vehicleattr) {
            return { b: this.props.vehicleattr };
        }
        if(this.props.match !== prevProps.match) {
            return { route: this.props.match };
        }

        return null;
    };

    filterCarHistory(){
        var tmp_myhistory = JSON.parse(JSON.stringify(this.props.myHistory));
        var place_myhistory = [];
        let route_now = this.props.match.url.split("/")[2];
        if(typeof tmp_myhistory !== 'undefined' && tmp_myhistory!== null && typeof this.props.vehicleattr.vehicle_attr!=='undefined' && this.props.vehicleattr.vehicle_attr !== null)
        {

            // if(typeof this.props.myBooking.data instanceof Object){
            tmp_myhistory.data.forEach(t=>{
                t.active = 0;
                t.details.forEach(y =>{
                    let tmp_ = _.filter(product_master, {MsProductId:y.MsProductId});
                    if(tmp_.length>0){
                        y.ProductName = tmp_[0].MsProductName;
                        let tmp_2 = _.filter(tmp_[0].product_service, {ProductServiceId:y.MsProductServiceId});
                        if(tmp_2.length>0){
                            y.ServiceName = tmp_2[0].ProductServiceName
                        }else{
                            y.ServiceName = ''
                        }
                    }else{
                        y.ProductName = '';
                        y.ServiceName = '';
                    }
                    let obj = this.props.vehicleattr.vehicle_attr.filter(x=>{
                        return x.vehicleTypeId === y.UnitTypeId;
                    });
                    if(obj.length>0){
                        y.image_url = obj[0].vehicleImage;
                    }
                });
                place_myhistory.push(t);
            });
            // }
        }
        // if(place_mybooking !== null ) {
        if(tmp_myhistory!== null) {
            this.setState({
                data: tmp_myhistory.data,
                page: tmp_myhistory.current_page,
                lastPage: tmp_myhistory.last_page
            });
        }
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot!==null){
            if(snapshot.route){

            }
            if(snapshot.history) {

                this.setState({
                    reservation:snapshot.history
                });
                if(this.state.vehicle){
                    this.filterCarHistory();
                }
            }
            if(snapshot.b) {
                this.setState({
                    vehicle:snapshot.b
                });
                if(this.state.reservation){
                    this.filterCarHistory()
                }
            }
        }

        //
        //
        //
        const {
            history: {
                action,
                push
            },
            isLogin
        } = this.props;
        if(!isLogin) {
            push('/')
        }


    }

    // update car image avatar
    carImageActive = (e) => {
        let index = e.currentTarget.dataset.key,
            id = e.currentTarget.dataset.index,
            updateData = this.state.data;

        updateData[index].active = id;
        this.setState({ data: updateData })
    };

    render() {
        let {
            data,
            vehicle,
            page,
            page_route,
            lastPage,
            popupPaymentGuide,

            rateBookingPopup
        } = this.state,
        {
            rateSubmit,
            popupPaymentClose,
            showPaymentGuide,
            closeRateBookingPopup,
            carImageActive
        } = this,
        {

            loading,
            history: {
                push
            }
        } = this.props,
        listBooking = null;

        listBooking = data.map((value, index) => {
            let classProgress = null,
                carListActive = parseInt(value.active),
                carListImage = value.details,
                carListTemplate = null,
                carProgress = null,
                carListDotTemplate = null;

            switch (value.Status) {
                case "BOSID-010":
                    classProgress = 'car-on-the-way';
                    break;
                case "BOSID-005":
                    classProgress = 'car-with-you';
                    break;
                case "BOSID-007":
                    classProgress = 'complete';
                    break;
                default:
                    break;
            }

                    carListTemplate = carListImage.map((carImage, i) => {
                        return (
                            <img className={carListActive === i ? 'active' : ''} src={carImage.image_url} alt={`car-${i}`} key={i} />
                        );
                    });
                    carProgress = carListImage.map((carImage, i) => {
                        if(carListActive === i){
                            switch (carImage.ActivityId) {
                                case "AMID-009#ASID-015":
                                case "AMID-018#ASID-015":
                                    return (
                                        <div className={`tracker-active car-on-the-way`} key={i}>
                                            <div className="icon-tracker"></div>
                                        </div>
                                    );
                                case "AMID-002#ASID-017":
                                    return (
                                        <div className={`tracker-active car-with-you`} key={i}>
                                            <div className="icon-tracker"></div>
                                        </div>
                                    );
                                case "AMID-002#ASID-016":
                                case "AMID-006#ASID-017":
                                case "AMID-009#ASID-017":
                                    return (
                                        <div className={`tracker-active complete`} key={i}>
                                            <div className="icon-tracker"></div>
                                        </div>
                                    );
                                default:
                                    return (
                                        <div className={`tracker-active car-on-the-way`} key={i}>
                                            <div className="icon-tracker"></div>
                                        </div>
                                    );
                            }
                        }
                    });

                    carListDotTemplate = carListImage.map((carImage, i) => {
                        return (
                            <span onClick={(e) => carImageActive(e)} className={carListActive === i ? 'active' : ''} data-key={index} data-index={i} key={i} />
                        );
                    });

            return (
                <Fragment key={index}>
                    {/*{!this.state.loading ?*/}
                    <div className="myBooking-settings" >
                        <div className="wrapper-my-booking">
                            <div className="status-car">
                                <h4 className="title-cars">{value.details[0].ProductName} - {value.details[0].ServiceName}</h4>
                                {/*<h4 className="title-cars">{value.ServiceTypeName.split('-')[0]}</h4>*/}
                                {(page !== 'history-booking' && value.Status === 'BOSID-010') &&
                                <p className={`driver-status waiting-payment`}>Waiting For Payment</p>}
                                {(value.Status === 'BOSID-002' || value.Status === 'BOSID-007') && <p className={`driver-status`}>{lang.canceled[lang.default]}</p>}
                                <p className="under-line">&nbsp;</p>
                            </div>
                            <div className="my-booking-cars">
                                <div className="my-booking-cars-image">
                                    {carListTemplate}
                                    {/*<img className="active" src={carsOne} alt={`car`} />*/}
                                </div>
                                <div className="my-booking-cars-dot">
                                    {carListDotTemplate}
                                    {/*<span onClick={(e) => {}} className={'active'} />*/}
                                </div>
                            </div>
                            <div className="detail-cars">
                                <div className="car-desc" style={{marginBottom: 10}}>
                                    <p className="car-total">{lang.carTotal[lang.default]}</p>
                                    <p className="value-car-total" style={{fontSize:14}}>{value.details.length > 1 ? value.details.length + ' Cars' : value.details.length + ' Car'}</p>
                                </div>
                                <div className="car-desc" style={{marginBottom: 10}}>
                                    <p className="book-numb">{lang.reservationNumber[lang.default]}</p>
                                    <p className="value-book-numb" style={{fontSize:14}}>{value.ReservationId}</p>
                                </div>
                                <div className="car-desc" style={{marginBottom: 10}}>
                                    <p className="dates">{lang.dates[lang.default]}</p>
                                    <p className="value-dates" style={{fontSize:14}}>{
                                        moment(value.details[0].StartDate).format('ll')} {value.details[0].EndDate ? (
                                        <span> - {moment(value.details[0].EndDate).format('ll')} </span>) : null
                                    } </p>
                                </div>
                                <div className="detail-cars btn">
                                    <Button text onClick={() => {
                                        push(`/dashboard/history-booking/detail/${value.ReservationId}`)
                                    }}>
                                        Detail
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*: null}*/}
                </Fragment>
            )
        })

        // listBooking = data.map((value, index) => {
        //     let classProgress = null,
        //         carListActive = parseInt(value.car.active),
        //         carListImage = value.car.image,
        //         carListTemplate = null,
        //         carListDotTemplate = null;

        //     carListTemplate = carListImage.map((carImage, i) => {
        //         return (
        //             <img className={carListActive === i ? 'active' : ''} src={carImage} alt={`car-${i}`} key={i} />
        //         );
        //     });

        //     carListDotTemplate = carListImage.map((carImage, i) => {
        //         return (
        //             <span onClick={(e) => carImageActive(e)} className={carListActive === i ? 'active' : ''} data-key={index} data-index={i} key={i} />
        //         );
        //     });

        //     switch (value.status.progress) {
        //         case 1:
        //             classProgress = 'car-on-the-way'
        //             break;
        //         case 2:
        //             classProgress = 'car-with-you'
        //             break;
        //         case 3:
        //             classProgress = 'complete'
        //             break;
        //         default:
        //             break;
        //     }

        //     return (
        //         <div className="myBooking-settings" key={index}>
        //             <div className="wrapper-my-booking">
        //                 <div className="status-car">
        //                     <h4 className="title-cars">{value.title}</h4>
        //                     {page !== 'history-booking' && <p className="driver-status">{value.status.text}</p>}
        //                     <p className="under-line">&nbsp;</p>
        //                 </div>
        //                 <div className="my-booking-cars">
        //                     <div className="my-booking-cars-image">
        //                         {carListTemplate}
        //                     </div>
        //                     <div className="my-booking-cars-dot">
        //                         {carListDotTemplate}
        //                     </div>
        //                 </div>
        //                 <div className="detail-cars">
        //                     <div className="car-desc">
        //                         <p className="car-total">Car Total</p>
        //                         <p className="value-car-total">{value.total > 1 ? value.total + ' Cars' : value.total + ' Car'}</p>
        //                     </div>
        //                     <div className="car-desc">
        //                         <p className="book-numb">Booking Number</p>
        //                         <p className="value-book-numb">{value.booking_number}</p>
        //                     </div>
        //                     <div className="car-desc">
        //                         <p className="dates">Dates</p>
        //                         <p className="value-dates">{value.dates.start} - {value.dates.end}</p>
        //                     </div>
        //                     <div className="detail-cars btn">
        //                         {page !== 'history-booking' ? <div className="wrapper-detail">
        //                             <div className="detail-tracker">
        //                                 <div className={`tracker-active ${classProgress}`}>
        //                                     <div className="icon-tracker"></div>
        //                                 </div>
        //                             </div>
        //                             <p className="label-track detail-info-on-the-way">Car on the way</p>
        //                             <p className="label-track detail-info-with-you">Car with you</p>
        //                             <p className="label-track detail-info-complete">Complete</p>
        //                         </div> : null}
        //                         <Button text goto={page === 'history-booking' ? "/dashboard/history-booking/detail" : "/dashboard/my-booking/detail"}>
        //                             Detail
        //                         </Button>
        //                         {/* {page === 'history-booking' ? <Button text goto="/dashboard/my-booking/detail">Rate Driver</Button> : null} */}
        //                     </div>
        //                 </div>
        //             </div>
        //         </div>
        //     )
        // });

        return (
            <Fragment>
                {listBooking}
                {loading && <div className="loading-event" style={{ marginTop: 20, textAlign: 'center' }}>
                    <PulseLoader
                        sizeUnit={"px"}
                        size={14}
                        color={'#f47d00'}
                        // color={'#1f419b'}
                        loading={loading}
                    />
                    <Text centered>{lang.pleaseWaitWeProcess[lang.default]}</Text>
                </div> }
                { (data.length > 0 && page_route=='history-booking' ) && <Pagination onClick={index => this.props.getMyHistory({ page: index }) } currentActivePage={page} totalPage={lastPage} /> }
                <ModalRateBooking showModal={rateBookingPopup} onSubmit={rateSubmit} onClose={closeRateBookingPopup} />
                <ModalPaymentGuide showModal={popupPaymentGuide} onClose={popupPaymentClose} />
            </Fragment>
        )
    }
}

const mapStateToProps = ({ dashboard, login, vehicleattr }) => {
    const { loading, myHistory } = dashboard;
    const { isLogin } = login;
    return { loading, isLogin, vehicleattr, myHistory }
}

export default withRouter(
    connect(mapStateToProps, {
        getMyHistory,
        getVehicleAttribute
    })(MyHistory)
);