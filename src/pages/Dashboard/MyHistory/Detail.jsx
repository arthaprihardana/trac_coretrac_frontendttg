/**
 * @author: Artha Prihardana
 * @Date: 2019-02-24 23:06:33
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-25 01:16:22
 */
import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";

// assets & style
import selectedCarJSON from "../../../assets/data-dummy/selected-car.json";
import "./MyBookingDetail.scss";

import HeaderDetail from "./Detail/HeaderDetail";
import CarDetail from "./Detail/CarDetail";

import CancelDetail from "./Detail/CancelDetail";

import {  Text } from "atom";
import InformationDetail from "./Detail/InformationDetail";
import { getDetailMyBooking, getVehicleAttribute } from "actions"
import { PulseLoader } from "react-spinners";
import lang from '../../../assets/data-master/language'

const selectedCarDummy = selectedCarJSON.map(car => {
    return car;
});

class Detail extends Component {

    state = {
        page: this.props.match.url.split('/')[2],
        selectedCar: '',
        all_data: '',
        book_id: this.props.match.params.id,
        bu:'',
        vehicle: [],
        myReservation:'',
        showDetailMobile: false,
        loading: true,
        showConfirmMobile: false,
        headerData: {},
        informationData: {}
    };

    componentDidMount = () => {
        let book_id = this.state.book_id;
        this.props.getDetailMyBooking({IdBooking:this.state.book_id});

        // const { detailMyBooking } = this.props;

    };
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.detailMyBooking !== prevProps.detailMyBooking) {
            return { a: this.props.detailMyBooking}
        }
        if(this.props.vehicleattr !== prevProps.vehicleattr) {
            return { b: this.props.vehicleattr };
        }
        return null;
    };
    filterCar(){
        var tmp_mybooking = JSON.parse(JSON.stringify(this.state.myReservation));
        if(typeof tmp_mybooking !== 'undefined' && tmp_mybooking!== null && typeof this.props.vehicleattr.vehicle_attr!=='undefined' && this.props.vehicleattr.vehicle_attr !== null)
        {
            if(tmp_mybooking.details){
                tmp_mybooking.details.forEach((y,index) =>{
                    let obj = this.props.vehicleattr.vehicle_attr.filter(x=>{
                        return x.vehicleTypeId === y.UnitTypeId;
                    });

                    if(obj.length>0){
                        y.image_url = obj[0].vehicleImage;
                    }
                    y={ ...y, ...obj[0]};
                    tmp_mybooking.details[index]=y;
                });
                this.setState({
                    myReservation: tmp_mybooking
                });

                this.setState({
                    selectedCar:tmp_mybooking.details,
                    all_data:tmp_mybooking,
                    bu:tmp_mybooking.BusinessUnitId,
                    headerData: {
                        ReservationId: tmp_mybooking.ReservationId,
                        carTotal: tmp_mybooking.details.length
                    },
                    informationData: {
                        businessUnitId: tmp_mybooking.BusinessUnitId,
                        MsProductId: tmp_mybooking.details[0].MsProductId,
                        Duration: tmp_mybooking.details[0].Duration,
                        Packages: tmp_mybooking.details[0].MsProductServiceId,
                        StartDate: tmp_mybooking.details[0].StartDate,
                        EndDate: tmp_mybooking.details[0].EndDate,
                        PickupLocation: tmp_mybooking.details[0].pickup_locations[0].Alamat,
                        NotesReservation: tmp_mybooking.NotesReservation,
                        TotalPrice: tmp_mybooking.TotalPrice,
                        details: tmp_mybooking.details
                    },
                    loading:false
                });
            }

        }
    }
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot!==null){
            if(snapshot.a) {
                this.props.getVehicleAttribute();
                this.setState({
                    myReservation:snapshot.a
                });
                if(this.state.vehicle){
                    this.filterCar();
                }
            }
            if(snapshot.b) {
                this.setState({
                    vehicle:snapshot.b
                });
                if(this.state.myReservation){
                    this.filterCar();
                }
            }
        }
    }

    extrasChange = value => {
        this.setState({
            selectedCar: value
        }, () => {
            // if (this.state.selectedCar.length < 1) {
            //   this.props.history.push("/transport-list/car");
            // }
        });
    };

    render() {
        const {
            state: {
                all_data,
                page,
                selectedCar,
                bu,
                loading,
                headerData,
                informationData
            },
            extrasChange
        } = this;

        return (
            <Fragment>
                <HeaderDetail page={page} data={headerData} />
                <div className="booking-detail-content">
                    <div className="left-content">
                        {loading ? <div className="loading-event" style={{ marginTop: 20, textAlign: 'center' }}>
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={14}
                                    color={'#f47d00'}
                                    // color={'#1f419b'}
                                    loading={loading}
                                />
                                <Text centered>{lang.pleaseWaitWeProcess[lang.default]}</Text>
                            </div> :
                            <CarDetail data={selectedCar} carType={bu} onExtrasChange={extrasChange} onRemove={extrasChange} onSubmit={() => this.setState({ showDetailMobile: true })} />
                        }


                    </div>
                    <div className="right-content">
                        <InformationDetail data={informationData} />
                    </div>
                </div>
                {/*{page !== 'history-booking' ? <CancelDetail /> : null}*/}
            </Fragment>
        )
    }
}

const mapStateToProps = ({ dashboard, vehicleattr }) => {
    const { detailMyBooking } = dashboard;
    return { detailMyBooking, vehicleattr }
}

export default connect(mapStateToProps, {
    getDetailMyBooking,
    getVehicleAttribute
})(Detail);