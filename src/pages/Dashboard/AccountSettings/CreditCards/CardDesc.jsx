import React from 'react';

const CardDesc = ({label, value}) => {
    return(
        <div className="card-desc">
            <p className="label-card">{label}</p>
            <p className="value-card">{value}</p>
        </div>
    )
}

export default CardDesc;