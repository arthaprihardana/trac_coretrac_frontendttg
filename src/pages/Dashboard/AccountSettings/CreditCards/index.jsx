import React, { Component, Fragment } from "react";
import visaCard from "assets/images/logo/visa.png";
import master from "assets/images/logo/masterCard.png";
import { Button } from "atom";
import CardItem from "./CardItem";
import { OutsideClick } from "organisms";
import ModalRemoveConfirmation from "../Modal/ModalRemoveConfirmation";
import { dashboardPopupSetup } from "helpers";
import ModalAddCardSuccess from "../Modal/ModalAddCardSuccess";
import ModalUpdateCardSuccess from "../Modal/ModalUpdateCardSuccess";
import ModalAddCard from "../Modal/ModalAddCard";
import { getCreditCard, postCreditCard, putCreditCard, deleteCreditCard, putPrimaryCC } from 'actions'
import connect from "react-redux/es/connect/connect";

class CreditCards extends Component {
    state = {
        selectedId:'',
        creditCardsList: [],
        //     {
        //         id: "visa",
        //         cardName: "Visa",
        //         cardNumber: "****2222",
        //         cardHolder: "Bryan Barry Borang",
        //         cardExpiry: "08/21",
        //         cardImg: visaCard,
        //         isSelected: true,
        //         isShow: false
        //     },
        //     {
        //         id: "master",
        //         cardName: "Master",
        //         cardNumber: "****3333",
        //         cardHolder: "Bryan Barry Borang",
        //         cardExpiry: "08/21",
        //         cardImg: master,
        //         isSelected: false,
        //         isShow: false
        //     }
        // ],
        isOutsideActive: false,
        popupRemoveConfirmation: false,
        popupAddCard: false,
        popupUpdateCard: false,
        popupSuccessAddCard: false
    };
    componentDidMount = () => {
        this.getCreditCard();
    }
    getCreditCard = async () => {
        this.props.getCreditCard();
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.getCreditCardSuccess !== prevProps.getCreditCardSuccess) {
            return { getSuccess: this.props.getCreditCardSuccess }
        }
        if(this.props.getCreditCardFailure !== prevProps.getCreditCardFailure) {
            return { getFailure: this.props.getCreditCardFailure }
        }
        if(this.props.deleteCreditCardSuccess !== prevProps.deleteCreditCardSuccess) {
            return { deleteSuccess: this.props.deleteCreditCardSuccess }
        }
        if(this.props.deleteCreditCardFailure !== prevProps.deleteCreditCardFailure) {
            return { deleteFailure: this.props.deleteCreditCardFailure }
        }
        if(this.props.putPrimaryCCSuccess !== prevProps.putPrimaryCCSuccess) {
            return { primaryCCSUccess: this.props.putPrimaryCCSuccess }
        }
        if(this.props.putPrimaryCCFailure !== prevProps.putPrimaryCCFailure) {
            return { primaryCCFailure: this.props.putPrimaryCCFailure }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.deleteSuccess !== undefined) {
                // console.log("delete success");
                this.getCreditCard();
                dashboardPopupSetup(false);

                this.setState({
                    popupRemoveConfirmation: false
                })
            }
            if(snapshot.primaryCCSUccess !== undefined) {
                // console.log("Success primaryCCSUccess");
                this.getCreditCard();
                dashboardPopupSetup(true);
                this.setState({
                    popupUpdateCard: true
                })
            }
            if(snapshot.deleteFailure !== undefined) {
                // console.log("delete failure");
                this.getCreditCard();
            }
            if(snapshot.getSuccess !== undefined) {
                console.log("success",snapshot.getSuccess);
                let tmp = snapshot.getSuccess.Data;
                let obj;
                let data=[];
                if(tmp.length>0){
                    tmp.forEach(i=>{
                        let img;
                        let name;
                        if(i.CardType=='VISA'){
                            img= visaCard;
                            name= "Visa";
                        }else{
                            img=master;
                            name= "Master";
                        }
                        obj={
                            id: i.Id,
                            cardName:name,
                            cardNumber: i.DecryptedCreditCard,
                            cardExpiry: i.DecryptedCardExpired,
                            cardImg: i.CardImage,
                            cardLogo: img,
                            isSelected: i.IsPrimary == 0 ? false : true,
                            isShow: false
                        };
                        data.push(obj);
                    });
                    this.setState({
                        creditCardsList:data
                    })
                }else{
                    this.setState({
                        creditCardsList:[]
                    })
                }
                // this.creditCardsList = snapshot.getSuccess;
            }
            if(snapshot.getFailure !== undefined) {
                console.log("failed");
            }
        }
    }

    updateCardList = (data, key) => {
        let newList = this.state.creditCardsList.map(card => {
            card[key] = false;
            if (data) {
                if (card.id === data.id) card[key] = true;
            }
            return card;
        });
        if(data.id !== undefined){
            this.setState({
                creditCardsList: newList
            }, ()=>{
                console.log("selectedId =>", this.state.selectedId);
            });
        }
    };

    handleCardShow = data => {
        this.setState(
            {
                isOutsideActive: true
            },
            () => {
                this.updateCardList(data, "isShow");
            }
        );
    };
    handleSetPrimary = data => {
        this.props.putPrimaryCC({Id:data.id});
        this.updateCardList(data, "isSelected");
    };
    handleOutsideClick = () => {
        this.setState(
            {
                isOutsideActive: false
            },
            () => {
                this.updateCardList(false, "isShow");
            }
        );
    };
    handleRemoveCard = data => {
        this.setState({
            selectedId:data.id,
            popupRemoveConfirmation: true
        })
    };
    confirmDeleteCard = () => {
        let tmp = this.state.selectedId;
        let FormData={
            Id:tmp
        };
        console.log("FormData",FormData);
        this.props.deleteCreditCard(FormData);
    }
    cancelDeleteCard = () => {
        dashboardPopupSetup(false);
        this.setState({
            popupRemoveConfirmation: false
        })
    }

    handleAddCard = () => {
        dashboardPopupSetup(true);
        this.setState({
            popupAddCard: true,
        })
    }
    confirmAddCard = () => {
        dashboardPopupSetup(false);
        this.setState({
            popupAddCard: false,
        }, () => {
            dashboardPopupSetup(true);
            this.setState({
                popupSuccessAddCard: true
            })
        })
    }
    doneAddCard = () => {
        dashboardPopupSetup(false);
        this.getCreditCard();
        this.setState({
            popupSuccessAddCard: false
        })
    }
    doneUpdateCard = () => {
        dashboardPopupSetup(false);
        this.setState({
            popupUpdateCard: false
        })
    }
    cancelAddCard = () => {
        dashboardPopupSetup(false);
        this.setState({
            popupAddCard: false,
        })
    }
    render() {
        const {
            state: { creditCardsList, isOutsideActive, popupRemoveConfirmation, popupAddCard,popupUpdateCard, popupSuccessAddCard },
            handleCardShow,
            handleSetPrimary,
            handleOutsideClick,
            handleRemoveCard,
            confirmDeleteCard,
            cancelDeleteCard,
            handleAddCard,
            confirmAddCard,
            doneAddCard,
            doneUpdateCard,
            cancelAddCard
        } = this;
        return (
            <Fragment>
                <div className="credit-card-section section-wrapper">
                    <div className="titleIdentification2">
                        <h3>Credit Cards</h3>
                        <p className="textIdentification">
                            We’ll autofill your details and make it easier for you
                            to rent services through TRAC. Your payment details are
                            stored securely
                        </p>
                    </div>
                    <OutsideClick
                        onOutsideClick={handleOutsideClick}
                        active={isOutsideActive}
                    >
                        <div className="creditCard-list">
                            {creditCardsList.map(card => {
                                return (
                                    <CardItem
                                        key={card.id}
                                        data={card}
                                        onClick={handleCardShow}
                                        onSetPrimary={()=>handleSetPrimary(card)}
                                        onRemove={()=>handleRemoveCard(card)}
                                    />
                                );
                            })}
                        </div>
                    </OutsideClick>
                    <Button type="button" outline onClick={handleAddCard}>
                        Add Card
                    </Button>
                </div>
                <ModalRemoveConfirmation showModal={popupRemoveConfirmation} onSubmit={confirmDeleteCard} onClose={cancelDeleteCard} />
                <ModalAddCardSuccess showModal={popupSuccessAddCard} onSubmit={doneAddCard} onClose={doneAddCard} />
                <ModalAddCardSuccess showModal={popupUpdateCard} onSubmit={doneUpdateCard} onClose={doneUpdateCard} />
                <ModalAddCard showModal={popupAddCard} onSubmit={confirmAddCard} onClose={cancelAddCard} />
            </Fragment>
        );
    }
}

const mapStateToProps = ({ dashboard }) => {
    const { getCreditCardSuccess, getCreditCardFailure,
        putCreditCardSuccess, putCreditCardFailure,
        putPrimaryCCSuccess, putPrimaryCCFailure,
        postCreditCardFailure, postCreditCardSuccess,
        deleteCreditCardSuccess, deleteCreditCardFailure, } = dashboard;
    return { getCreditCardSuccess, getCreditCardFailure,
        putCreditCardSuccess, putCreditCardFailure,
        putPrimaryCCSuccess, putPrimaryCCFailure,
        postCreditCardFailure, postCreditCardSuccess,
        deleteCreditCardSuccess, deleteCreditCardFailure }
}

export default connect(mapStateToProps, {
    getCreditCard, postCreditCard, putCreditCard, deleteCreditCard, putPrimaryCC
})(CreditCards);

// export default CreditCards;
