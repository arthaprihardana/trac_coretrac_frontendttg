import React from "react";
import cx from 'classnames';
import CardDesc from "./CardDesc";
import { Button } from "atom";

const CardItem = ({data, onClick, onSetPrimary, onRemove}) => {
    const classSelected = cx('select-icon', {
        'selected': data.isSelected
    });

    const classCardItem = cx('card-item', {
        'show': data.isShow
    });

    return (
        <div className={classCardItem} onClick={() => onClick(data)}>
            <div className="card-info">
                <div className={classSelected} />
                <div className="card-img">
                    <img src={data.cardLogo} alt="card" />
                </div>
                <CardDesc label="Credit Card Number" value={data.cardNumber} />
                <CardDesc label="Expiry Date" value={data.cardExpiry} />
                <div className="card-desc">
                    <img src={data.cardImg} style={{width:150}} alt="cardImg"/>
                </div>
            </div>
            <div className="card-action">
                <Button type="button" primary onClick={() => onSetPrimary(data)}>
                    Set Primary
                </Button>
                <Button type="button" outline onClick={onRemove}>
                    Remove Card
                </Button>
            </div>
        </div>
    );
};

export default CardItem;
