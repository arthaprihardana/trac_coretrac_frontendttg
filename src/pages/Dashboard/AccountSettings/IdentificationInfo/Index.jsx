/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-22 18:17:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-23 22:52:09
 */
import { TracField } from "atom";
import { Form, Formik } from "formik";
import React, { Component } from "react";
// import { IDCard } from "organisms";
import { connect } from "react-redux";
import { setFormIdentification } from 'actions'
import * as Yup from "yup";

import lang from '../../../../assets/data-master/language'

class IdentificationInfo extends Component {
    // ImageKTP: "https://tracomni.blob.core.windows.net/profile-picture/USR-0047_KTP.png"
    // ImagePath: null
    // ImageSIM: "https://tracomni.blob.core.windows.net/profile-picture/USR-0047_SIM.png"

    state = {
        ktpChanged:false,
        simChanged:false,
        ImagePath:"",
        IsForeigner:0,
        IdUser     : "",
        ImagePathBase64:"",
        ImageSIM:"",
        ImageSIMBase64:"",
        ImageKTP:"",
        ImageKTPBase64:"",
        NoKTP: "",
        NoSIM: "",
        Address: "",
	counter: 0,
        form: {
            checkbox1: true,
            checkbox2: false
        }
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.user !== prevProps.user) {
            return this.props.user.Data;
        }
        return null;
    }
    
    componentDidUpdate = (prevProps, prevState, snapshot) => {

        if(snapshot !== null) {
            console.log(snapshot);
            let noKTP='';
            if(snapshot.NoKTP){
                noKTP=snapshot.NoKTP;
            }
            let ImagePath='';
            if(snapshot.ImagePath){
                ImagePath=snapshot.ImagePath;
            }
            let ImageSIM='';
            if(snapshot.ImageSIM){
                ImageSIM=snapshot.ImageSIM;
            }
            let ImageKTP='';
            if(snapshot.ImageKTP){
                ImageKTP=snapshot.ImageKTP;
            }
            let noSIM='';
            if(snapshot.NoSIM){
                noSIM=snapshot.NoSIM;
            }
            this.setState({
                NoKTP: noKTP,
                ImagePath: ImagePath,
                ImageKTP: ImageKTP,
                IsForeigner: snapshot.IsForeigner,
                ImageSIM: ImageSIM,
                IdUser     : snapshot.Id,
                NoSIM: noSIM,
                Address: snapshot.Address
            });
            if(snapshot.IsForeigner==0){
                this.setState({
                    form: {
                        ...this.state.form,
                        checkbox1: true,
                        checkbox2: false
                    }
                });
            }else{
                this.setState({
                    form: {
                        ...this.state.form,
                        checkbox1: false,
                        checkbox2: true
                    }
                });
            }

        }
    };

    handleCheckBox = e => {
        if(e.target.id === "checkbox1"){
            console.log("Indonesia");
            this.setState({
                IsForeigner:0
            })
        }else{
            console.log("Foreigner")
            this.setState({
                IsForeigner:1
            })
        }
        this.setState({
            form: {
                ...this.state.form,
                checkbox1: e.target.id === "checkbox1" ? true : false,
                checkbox2: e.target.id === "checkbox2" ? true : false
            }
        });
    };
    simChange =(e) => {
        var canvas=document.createElement("canvas");
        var ctx=canvas.getContext("2d");
        var cw=canvas.width;
        var ch=canvas.height;
        var maxW=640;
        var maxH=480;
        var img = new Image;
        img.onload = ()=> {
            var iw=img.width;
            var ih=img.height;
            var scale=Math.min((maxW/iw),(maxH/ih));
            var iwScaled=iw*scale;
            var ihScaled=ih*scale;
            canvas.width=iwScaled;
            canvas.height=ihScaled;
            ctx.drawImage(img,0,0,iwScaled,ihScaled);
            let tmp = canvas.toDataURL("image/jpeg",0.5);
            this.setState({
                simChanged:true,
                ImageSIMBase64: tmp,
                ImageSIM: tmp
            });
            if(this.state.ktpChanged){
                this.props.setFormIdentification({
                    ImageKTP    : this.state.ImageKTPBase64,
                    ImageSIM    : tmp,
                    NoKTP       : this.state.NoKTP,
                    IdUser     : this.state.IdUser,
                    IsForeigner : this.state.IsForeigner,
                    NoSIM       : this.state.NoSIM,
                    Address     : this.state.Address
                });
            }else{
                this.props.setFormIdentification({
                    ImageSIM    : tmp,
                    NoKTP       : this.state.NoKTP,
                    IdUser     : this.state.IdUser,
                    IsForeigner : this.state.IsForeigner,
                    NoSIM       : this.state.NoSIM,
                    Address     : this.state.Address
                });
            }
            console.log(tmp);
        };
        img.src = URL.createObjectURL(e.target.files[0]);
    };
    // simChange =(e) => {
    //     var file= e.target.files[0];
    //     var myReader = new FileReader();
    //     myReader.onloadend = (e) => {
    //         console.log(myReader.result);
    //         this.setState({
    //             simChanged:true,
    //             ImageSIMBase64: myReader.result,
    //             ImageSIM: URL.createObjectURL(file)
    //         });
    //         if(this.state.ktpChanged){
    //             this.props.setFormIdentification({
    //                 ImageKTP    : this.state.ImageKTPBase64,
    //                 ImageSIM    : myReader.result,
    //                 NoKTP       : this.state.NoKTP,
    //                 IdUser     : this.state.IdUser,
    //                 IsForeigner : this.state.IsForeigner,
    //                 NoSIM       : this.state.NoSIM,
    //                 Address     : this.state.Address
    //             });
    //         }else{
    //             this.props.setFormIdentification({
    //                 ImageSIM    : myReader.result,
    //                 NoKTP       : this.state.NoKTP,
    //                 IdUser     : this.state.IdUser,
    //                 IsForeigner : this.state.IsForeigner,
    //                 NoSIM       : this.state.NoSIM,
    //                 Address     : this.state.Address
    //             });
    //         }
    //
    //     };
    //     myReader.readAsDataURL(file);
    // };
    ktpChange = (e) => {
            var canvas=document.createElement("canvas");
            var ctx=canvas.getContext("2d");
            var cw=canvas.width;
            var ch=canvas.height;
            var maxW=640;
            var maxH=480;
            var img = new Image;
            img.onload = ()=> {
                var iw=img.width;
                var ih=img.height;
                var scale=Math.min((maxW/iw),(maxH/ih));
                var iwScaled=iw*scale;
                var ihScaled=ih*scale;
                canvas.width=iwScaled;
                canvas.height=ihScaled;
                ctx.drawImage(img,0,0,iwScaled,ihScaled);
                let tmp = canvas.toDataURL("image/jpeg",0.5);
                this.setState({
                    ktpChanged:true,
                    ImageKTPBase64: tmp,
                    ImageKTP: tmp
                });
                if(this.state.simChanged){
                    this.props.setFormIdentification({
                        ImageKTP    : tmp,
                        ImageSIM    : this.state.ImageSIMBase64,
                        NoKTP       : this.state.NoKTP,
                        NoSIM       : this.state.NoSIM,
                        IdUser     : this.state.IdUser,
                        IsForeigner : this.state.IsForeigner,
                        Address     : this.state.Address
                    });
                }else{
                    this.props.setFormIdentification({
                        ImageKTP    : tmp,
                        NoKTP       : this.state.NoKTP,
                        IdUser     : this.state.IdUser,
                        IsForeigner : this.state.IsForeigner,
                        NoSIM       : this.state.NoSIM,
                        Address     : this.state.Address
                    });
                }
                console.log(tmp);
            };
            img.src = URL.createObjectURL(e.target.files[0]);
    };

    // ktpChange2 = (e) => {
    //
    //     var file= e.target.files[0];
    //     var myReader = new FileReader();
    //     myReader.onloadend = (e) => {
    //         console.log(myReader.result);
    //         this.setState({
    //             ktpChanged:true,
    //             ImageKTPBase64: myReader.result,
    //             ImageKTP: URL.createObjectURL(file)
    //         });
    //         if(this.state.simChanged){
    //             this.props.setFormIdentification({
    //                 ImageKTP    : myReader.result,
    //                 ImageSIM    : this.state.ImageSIMBase64,
    //                 NoKTP       : this.state.NoKTP,
    //                 NoSIM       : this.state.NoSIM,
    //                 IdUser     : this.state.IdUser,
    //                 IsForeigner : this.state.IsForeigner,
    //                 Address     : this.state.Address
    //             });
    //         }else{
    //             this.props.setFormIdentification({
    //                 ImageKTP    : myReader.result,
    //                 NoKTP       : this.state.NoKTP,
    //                 IdUser     : this.state.IdUser,
    //                 IsForeigner : this.state.IsForeigner,
    //                 NoSIM       : this.state.NoSIM,
    //                 Address     : this.state.Address
    //             });
    //         }
    //
    //     };
    //     myReader.readAsDataURL(file);
    // };

    handleChange = (key) => {
        console.log(this.state.imgChanged);
        if(this.state.simChanged && this.state.ktpChanged){
            this.props.setFormIdentification({
                ImageKTP    : this.state.ImageKTPBase64,
                ImageSIM    : this.state.ImageSIMBase64,
                NoKTP       : this.state.NoKTP,
                IdUser     : this.state.IdUser,
                NoSIM       : this.state.NoSIM,
                IsForeigner : this.state.IsForeigner,
                Address     : this.state.Address
            });
        }
        if(!this.state.simChanged && this.state.ktpChanged){
            this.props.setFormIdentification({
                ImageKTP    : this.state.ImageKTPBase64,
                NoKTP       : this.state.NoKTP,
                IdUser     : this.state.IdUser,
                NoSIM       : this.state.NoSIM,
                IsForeigner : this.state.IsForeigner,
                Address     : this.state.Address
            });
        }
        if(this.state.simChanged && !this.state.ktpChanged){
            this.props.setFormIdentification({
                ImageSIM    : this.state.ImageSIMBase64,
                NoKTP       : this.state.NoKTP,
                IdUser     : this.state.IdUser,
                NoSIM       : this.state.NoSIM,
                IsForeigner : this.state.IsForeigner,
                Address     : this.state.Address
            });
        }
        if(!this.state.simChanged && !this.state.ktpChanged){
            this.props.setFormIdentification({
                NoKTP       : this.state.NoKTP,
                IdUser     : this.state.IdUser,
                NoSIM       : this.state.NoSIM,
                IsForeigner : this.state.IsForeigner,
                Address     : this.state.Address
            });
        }
    };
    setInputFilter = function(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
            textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                }
            });
        });
    };
    changeKTP = (e)=>{
        this.value = this.value.replace(/[^\d]/g, '');
    }
    render() {
        const { NoKTP, NoSIM, Address, form, ImageKTP, ImageSIM } = this.state;
        const { simChange, ktpChange } = this;
        const img_placeholder = {
            position: 'absolute',
            width: '100%',
            height: '100%',
            top: 0,
            left: 0
        };
        return (
            <div className="identification-information-section section-wrapper">
                <Formik
                    initialValues={{
                        phoneNumber: 81212341234,
                        NoKTP: NoKTP,
                        NoSIM: NoSIM,
                        Address: Address
                    }}
                >
                    {({ isSubmitting, handleChange, handleBlur, values }) => (
                        <Form>
                            <div className="title-wrapper">
                                <h3>{lang.identificationInformation[lang.default]}</h3>
                                <p className="textIdentification">
                                    {lang.weWillAutoFill[lang.default]}
                                </p>
                                <div className="select-citizen">
                                    <TracField
                                        id="checkbox1"
                                        type="checkbox"
                                        name="checkbox1"
                                        labelName={lang.indeonsiaCitizen[lang.default]}
                                        labelSize="small"
                                        onChange={this.handleCheckBox}
                                        value={values.checkbox1}
                                        checked={form.checkbox1}
                                    />
                                    <TracField
                                        id="checkbox2"
                                        type="checkbox"
                                        name="checkbox2"
                                        labelName={lang.foreignCitizen[lang.default]}
                                        labelSize="small"
                                        onChange={this.handleCheckBox}
                                        value={values.checkbox2}
                                        checked={form.checkbox2}
                                    />
                                </div>
                            </div>
                            <div className="form-col initial id-card-wrapper">
                                {
                                    form.checkbox1 && (
                                        <div className="m-id-card-photo">
                                            <div className="upload-area">
                                                <div className="drag-zone plus">
                                                    { !ImageKTP ? (
                                                        <div>
                                                            <div className="icon plus" />
                                                            <div className="description">
                                                                {lang.addKTPCard[lang.default]}
                                                            </div>
                                                            <input
                                                                type="file"
                                                                className="input-file"
                                                                ref="changeKTP"
                                                                onChange={ktpChange}
                                                            />
                                                        </div>
                                                      ) : (
                                                           <img src={ImageKTP} style={img_placeholder} alt=""/>
                                                        )
                                                    }
                                                </div>
                                                <input
                                                    type="file"
                                                    onChange={ktpChange}
                                                    className="input-file"
                                                />
                                            </div>
                                        </div>
                                    )
                                }
                                <div className="m-id-card-photo">
                                    <div className="upload-area">
                                        <div className="drag-zone plus">
                                            { !ImageSIM ? (
                                                <div>
                                                    <div className="icon plus" />
                                                    <div className="description">
                                                       {lang.addLicenseCard[lang.default]}
                                                    </div>
                                                    <input
                                                        type="file"
                                                        className="input-file"
                                                        ref="changeSIM"
                                                        onChange={simChange}
                                                    />
                                                </div>
                                            ) : (
                                                <img src={ImageSIM} style={img_placeholder} alt=""/>
                                            )
                                            }
                                        </div>
                                        <input
                                            type="file"
                                            onChange={simChange}
                                            className="input-file"
                                        />
                                    </div>
                                </div>
                            </div>
                            {
                                form.checkbox1 && (
                                    <div className="titleIdentification1">
                                        <div className="form-col">
                                            <TracField
                                                id="NoKTP"
                                                name="NoKTP"
                                                type="text"
                                                labelName="KTP Number"
                                                onChange={e => {
                                                    if(e.currentTarget.value.length<17)
                                                    this.setState({
                                                        NoKTP: e.currentTarget.value.replace(/[^\d]/g, '')
                                                    }, () => this.handleChange('NoKTP'));
                                                }}
                                                onBlur={handleBlur}
                                                value={values.NoKTP || NoKTP}
                                            />
                                        </div>
                                    </div>
                                )
                            }

                            <div className="titleIdentification1">
                                <div className="form-col">
                                    <TracField
                                        id="NoSIM"
                                        name="NoSIM"
                                        type="text"
                                        labelName="License Number"
                                        onChange={e => {
                                            if(e.currentTarget.value.length<13)
                                            this.setState({
                                                NoSIM: e.currentTarget.value.replace(/[^\d]/g, '')
                                            }, () => this.handleChange('NoSIM'));
                                        }}
                                        onBlur={handleBlur}
                                        value={values.NoSIM || NoSIM}
                                    />
                                </div>
                            </div>
                            <div className="titleIdentification1 last">
                                <div className="form-col">
                                    <TracField
                                        id="Address"
                                        name="Address"
                                        type="text"
                                        labelName="Address"
                                        onChange={e => {
                                            this.setState({
                                                Address: e.currentTarget.value
                                            }, () => this.handleChange('Address'));
                                        }}
                                        onBlur={handleBlur}
                                        value={values.Address || Address}
                                    />
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        );
    }
}

const mapStateToProps = ({ userprofile }) => {
    const { loading, user } = userprofile;
    return { loading, user };
}

export default connect(mapStateToProps, {
    setFormIdentification
})(IdentificationInfo);
