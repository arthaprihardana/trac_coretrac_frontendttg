/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-21 16:48:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-23 22:56:50
 */
// libraries
import React, { Component, Fragment } from "react";

import { Button } from "atom";
import { dashboardPopupSetup } from "helpers";

// local component
import CreditCards from "./CreditCards";
import { Text } from "atom"
import IdentificationInfo from "./IdentificationInfo/Index";
import UserAccount from "./UserAccount";
import ModalSaveConfirmation from './Modal/ModalSaveConfirmation'
import ModalSuccess from './Modal/ModalSuccess'

import { PulseLoader } from "react-spinners";

import { connect } from "react-redux";
import { getUserProfile, updateUserProfile } from "actions";
import { localStorageDecrypt } from "helpers";

import lang from '../../../assets/data-master/language'
// style
import "./AccountSettings.scss";

import db from '../../../db';

class AccountSettings extends Component {
    state = {
        popupSaveConfirmation: false,
        isLoading: false,
        loading: true,
        popupSuccess: false
    };
    componentDidMount = () => {
        this.getUserProfile();
            setTimeout(()=>{
                this.setState({
                    loading:false
                })
            },500)
    }

    getUserProfile = async () => {
        let local_auth = localStorageDecrypt('_aaa', 'object');
        this.props.getUserProfile(local_auth.UserLoginId);
    }
    saveEdit = () => {
        dashboardPopupSetup(true);
        this.setState({
            popupSaveConfirmation: true
        })
    };
    confirmSave = () => {
        dashboardPopupSetup(false);
        this.setState({
            isLoading:true,
            popupSaveConfirmation: false
        });
        let obj_comb = {...this.props.formAccount, ...this.props.formIdentification};
        let FirstName, LastName;
        if(obj_comb.displayName!== undefined){
            obj_comb.displayName = obj_comb.displayName.replace(/^\s+/g, '');
        }
        if(obj_comb.displayName){
            FirstName = obj_comb.displayName.substr(0,obj_comb.displayName.indexOf(' '));
            LastName = obj_comb.displayName.substr(obj_comb.displayName.indexOf(' ')+1);
            obj_comb.FirstName = FirstName;
            obj_comb.LastName = LastName;
        }
        if(obj_comb.NoHandphone){
            obj_comb.NoHandphone = obj_comb.phoneCode+obj_comb.NoHandphone
        }
        this.props.updateUserProfile({ FormData: obj_comb });
    };
    cancelSave = () => {
        dashboardPopupSetup(false);
        this.setState({
            popupSaveConfirmation: false
        })
    };
    discardEdit = () => {
        this.getUserProfile();
    };

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.updateUserProfileSuccess !== prevProps.updateUserProfileSuccess) {
            return { success: this.props.updateUserProfileSuccess }
        }
        if(this.props.updateUserProfileFailure !== prevProps.updateUserProfileFailure) {
            return { failure: this.props.updateUserProfileFailure }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.success !== undefined) {
                // console.log("success");
                this.getUserProfile();
                setTimeout(()=>{
                    this.setState({
                        isLoading:false,
                        popupSuccess:true
                    });
                },500)
            }
            if(snapshot.failure !== undefined) {
                // console.log("failed");
            }
        }
    }

    render() {
        const {
            saveEdit,
            confirmSave,
            discardEdit,
            cancelSave,
            state: { popupSaveConfirmation, popupSuccess, isLoading ,loading}
        } = this;
        return (
            <Fragment>
                {loading && <div className="loading-event" style={{ marginTop: 20, textAlign: 'center' }}>
                    <PulseLoader
                        sizeUnit={"px"}
                        size={14}
                        color={'#f47d00'}
                        // color={'#1f419b'}
                        loading={loading}
                    />
                    <Text centered>{lang.pleaseWaitWeProcess[lang.default]}</Text>
                </div> }
                {
                    <div className="p-account-settings">
                        <UserAccount />
                        <IdentificationInfo />
                        {/*<CreditCards />*/}
                        {/* <p className="remove-account">
                        <a href="/"> Click here</a> to remove your TRAC Account.
                        We’ll be sad to see you go
                    </p> */}
                    </div>
                }

                {
                    (this.props.formIdentification || this.props.formAccount) && (
                        <div className="btn-group action" style={{paddingTop: 15}}>
                            <Button type="button" primary onClick={saveEdit} >
                                { isLoading ?
                                    <PulseLoader
                                        sizeUnit={"px"}
                                        size={7}
                                        color={'#ffffff'}
                                        loading={isLoading}
                                    /> : "Save"
                                }

                            </Button>
                            {/*<Button type="button" outline onClick={discardEdit}>*/}
                                {/*Discard Edit*/}
                            {/*</Button>*/}
                        </div>
                    )
                }

                <ModalSaveConfirmation
                    showModal={popupSaveConfirmation}
                    onSubmit={confirmSave}
                    onClose={cancelSave}
                />
                <ModalSuccess
                    showModal={popupSuccess}
                    onClose={()=>this.setState({
                        popupSuccess:false
                    }, window.location.reload())}
                />
            </Fragment>
        );
    }
}
const mapStateToProps = ({ userprofile }) => {
    const { formAccount, formIdentification, updateUserProfileSuccess, updateUserProfileFailure } = userprofile;
    return { formAccount, formIdentification,updateUserProfileSuccess, updateUserProfileFailure  }
}

export default connect(mapStateToProps, {
    updateUserProfile,
    getUserProfile
})(AccountSettings);
