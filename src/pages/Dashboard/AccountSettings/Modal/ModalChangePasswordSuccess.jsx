import { Button, Modal } from "atom";
import React, { Component } from "react";
import SuccessImg from '../../../../assets/images/icons/success-change-password.svg';
import lang from '../../../../assets/data-master/language'

class ModalAddCardSuccess extends Component {
    render() {
        const { showModal, onSubmit, onClose } = this.props;
        return (
            <Modal
                open={showModal}
                extraClass="success"
                onClick={onClose}
                size="medium"
                header={{
                    withCloseButton: false,
                    onClick: () => console.log("header close"),
                    children: ''
                }}
                content={{
                    children: (
                        <div className="dashboard-popup-content">
                            <div className="img-thumb">
                                <img src={SuccessImg} alt="success"/>
                            </div>
                            <p className="success-text">{lang.yourPasswordHave[lang.default]} <br/>{lang.successfullyChanged[lang.default]}</p>
                        </div>
                    )
                }}
                footer={{
                    position: "center",
                    children: (
                        <div className="success">
                            <Button primary onClick={onSubmit}>
                                {lang.done[lang.default]}
                            </Button>
                        </div>
                    )
                }}
            />
        );
    }
}

export default ModalAddCardSuccess;
