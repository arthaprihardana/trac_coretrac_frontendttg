import { Button, Modal, TracField } from "atom";
import { Form, Formik } from "formik";
import React, { Component } from "react";

import lang from '../../../../assets/data-master/language'

import * as Yup from "yup";
Yup.addMethod(Yup.mixed, 'sameAs', function (ref, message) {
    return this.test('sameAs', message, function (value) {
        const other = this.resolve(ref);
        return !other || !value || value === other;
    });
});
// Validation
const AuthSchema = Yup.object().shape({
    oldPassword: Yup.string()
        .min("8", "Should be 6 characters!")
        .required(lang.loginEmptyPassword[lang.default]),
    newPassword: Yup.string()
        .min("8", "Should be 8 characters!")
        .required(lang.loginEmptyPassword[lang.default]),
    confirmPassword: Yup.string().sameAs(Yup.ref('newPassword'), lang.newPasswordNotMatch[lang.default])
        .min("8", "Should be 8 characters!")
        .required(lang.loginEmptyPassword[lang.default])
});

class ModalChangePassword extends Component {
    state = {
        oldPassword:'',
        newPassword:'',
        confirmPassword:''
    }
    handleSubmit = (val) => {
        console.log('handle submit =>', val)
    }
    render() {
        const { showModal, onSubmit, onClose } = this.props;
        const { oldPassword, newPassword, confirmPassword } = this.state;
        return (
            <Modal
                open={showModal}
                extraClass="change-password"
                onClick={onClose}
                size="medium"
                header={{
                    withCloseButton: false,
                    onClick: () => console.log("header close"),
                    children: (
                        <div className="dashboard-popup-header">
                            <p>{lang.changePassword[lang.default]}</p>
                            <div className="icon-close" onClick={onClose} />
                        </div>
                    )
                }}
                content={{
                    children: (
                        <div className="dashboard-popup-content">
                            <div className="form-wrapper">
                                <Formik
                                    initialValues={{
                                        oldPassword: "",
                                        newPassword: "",
                                        confirmPassword: "",
                                    }}
                                    validationSchema={AuthSchema}
                                    onSubmit={this.handleSubmit}
                                >
                                    {({
                                        isSubmitting,
                                        handleBlur,
                                        handleChange,
                                        values
                                    }) => (
                                        <Form>
                                            <TracField
                                                id="oldPassword"
                                                name="oldPassword"
                                                type="password"
                                                labelName={lang.oldPassword[lang.default]}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.oldPassword}
                                            />
                                            <TracField
                                                id="newPassword"
                                                name="newPassword"
                                                type="password"
                                                labelName={lang.newPassword[lang.default]}
                                               onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.newPassword}
                                            />
                                            <TracField
                                                id="confirmPassword"
                                                name="confirmPassword"
                                                type="password"
                                                labelName={lang.newPasswordConfirm[lang.default]}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.confirmPassword}
                                            />
                                            <div className="dashboard-popup-footer">
                                                <Button primary onClick={()=>onSubmit(values)} disabled={(values.confirmPassword !== values.newPassword) ||  values.oldPassword.length<6 || values.newPassword.length<6}>
                                                    {lang.changePassword[lang.default]}
                                                </Button>
                                            </div>
                                        </Form>
                                    )}
                                </Formik>
                            </div>
                        </div>
                    )
                }}
                footer={{
                    position: "center",
                    children: (
                        <div className="dashboard-popup-footer">

                        </div>
                    )
                }}
            />
        );
    }
}

export default ModalChangePassword;
