import { Button, Modal, Text, TracField } from "atom";
import { Form, Formik } from "formik";
import { Toggle } from 'organisms';
import React, { Component, Fragment } from "react";

import Images from "assets/images/dummy/placeholder.png";
import { localStorageDecrypt } from "helpers";
import {  postCreditCard } from 'actions'
import connect from "react-redux/es/connect/connect";
import "../AccountSettings.scss";

class ModalAddCard extends Component {
    state = {
        savedCard: false,
        aggrement: false,
        ImagePath: "",
        bank:'',
        cardType:'',
        creditCardValid:'',
        creditCardNumber:'',
        imgTmp:Images,
        ImagePathBase64: "",
        termsModal: false
    }
    handleSubmit = () => {
        console.log("handle submit");
    };
    readThis = (e) => {
        var canvas=document.createElement("canvas");
        var ctx=canvas.getContext("2d");
        var cw=canvas.width;
        var ch=canvas.height;
        var maxW=640;
        var maxH=480;
        var img = new Image;
        img.onload = ()=> {
            var iw=img.width;
            var ih=img.height;
            var scale=Math.min((maxW/iw),(maxH/ih));
            var iwScaled=iw*scale;
            var ihScaled=ih*scale;
            canvas.width=iwScaled;
            canvas.height=ihScaled;
            ctx.drawImage(img,0,0,iwScaled,ihScaled);
            let tmp = canvas.toDataURL("image/jpeg",0.5);
            this.setState({
                ImagePathBase64: tmp,
                ImagePath: tmp
            });
        };
        img.src = URL.createObjectURL(e.files[0]);
    };
    handleChangePhoto = () => {
        let tmp= document.getElementById("changePhoto");
        tmp.click();
        // let changePhoto = this.refs.changePhoto;
        // changePhoto.click();
    };
    confirmSave = () => {
        // this.props.updateUserProfile({ FormData: obj_comb });
    };
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.postCreditCardSuccess !== prevProps.postCreditCardSuccess) {
            return { success: this.props.postCreditCardSuccess }
        }
        if(this.props.postCreditCardSuccess !== prevProps.postCreditCardSuccess) {
            return { failure: this.props.postCreditCardSuccess }
        }
        return null;
    }

    kosongState() {
        this.setState({
            creditCardNumber:'',
            creditCardValid:'',
            bank:'',
            cardType:'',
            ImagePath:'',
            ImagePathBase64:''
        })
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.success !== undefined) {
                console.log("success");
                this.props.onSubmit();
            }
            if(snapshot.failure !== undefined) {
                console.log("failed");
            }
        }
    };
    submitAdd = () =>{
        let local_auth = localStorageDecrypt('_aaa', 'object');
        let cardNumber = this.state.creditCardNumber;
        let cardExp = this.state.creditCardValid;
        let cardPub = this.state.bank;
        let img = this.state.ImagePathBase64;
        let cardType = this.state.cardType;
        let data = {
            UserId: local_auth.UserLoginId,
            CreditCard : cardNumber,
            CardExpired : cardExp,
            CardPublisher : cardPub,
            CardImage : img,
            CardType : cardType
        }
        console.log("data", data);
        this.props.postCreditCard({ FormData: data });
    };
    photoChange = (e) => {
        this.readThis(e.target)
    };
    changeCreditCard = (e) => {
        let master = /^5[1-5][0-9]{14}$|^2(?:2(?:2[1-9]|[3-9][0-9])|[3-6][0-9][0-9]|7(?:[01][0-9]|20))[0-9]{12}$/
        let visa = /^4[0-9]{12}(?:[0-9]{3})?$/
        let master_card = new RegExp(master);
        let visa_card = new RegExp(visa);
        let text = e.currentTarget.value.replace(/[^\d]/g, '');
        if(text.length>0){
            let formated = text.match(/.{1,4}/g).join('-');
            if(text.length<17){
                if( text.match(master) !== null){
                    this.setState({
                        cardType:"MASTER",
                        creditCardNumber: formated
                    });
                }else if(text.match(visa) !== null){
                    this.setState({
                        cardType:"VISA",
                        creditCardNumber: formated
                    });
                }else{
                    this.setState({
                        cardType:"",
                        creditCardNumber: formated
                    });
                }
            }
        }else{
            this.setState({
                cardType:"",
                creditCardNumber: ""
            });
        }


    }
    handleTermsModal = (type) => (e) => {
        e.preventDefault();
        let action = false;
        type === 'open' ? action = true : action = false;
        this.setState({ termsModal: action })
    }
    render() {
        const { showModal, onSubmit, onClose } = this.props;
        const { photoChange, handleChangePhoto, submitAdd, changeCreditCard, kosongState } =this;
        const {termsModal, aggrement, ImagePath, imgTmp, creditCardNumber, creditCardValid, bank, cardType} = this.state;
        return (
            <Modal
                open={showModal}
                extraClass="add-credit-card"
                onClick={onClose}
                size="medium"
                header={{
                    withCloseButton: false,
                    onClick: () => console.log("header close"),
                    children: (
                        <div className="dashboard-popup-header">
                            <p>Add Credit Card</p>
                            <div className="icon-close" onClick={() => {
                                this.kosongState();
                                onClose()
                            }} />
                        </div>
                    )
                }}
                content={{
                    children: (
                        <div className="dashboard-popup-content">
                            <div className="form-add-card-wrapper">
                                <Fragment>
                                    <div className="info-header">
                                        <h3>Credit/Debit Card</h3>
                                        <div className="brand-wrapper">
                                            <div className="item-brand">
                                                <img
                                                    src={require("../../../../assets/images/logo/payment-logo.png")}
                                                    alt=""
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="payment-info-form" style={{maxWidth:'100%'}}>
                                        <Formik>
                                            {({
                                                handleChange,
                                                handleBlur,
                                                values
                                            }) => (
                                                <Form>
                                                    <div className="modal-settings">
                                                        <div className="left-form">
                                                            <div className="form-col">
                                                                <div className="form-input">
                                                                    <TracField
                                                                        id="creditCardNumber"
                                                                        name="creditCardNumber"
                                                                        type="text"
                                                                        labelName="Credit/Debit Card Number"
                                                                        onChange={e => {
                                                                           changeCreditCard(e)
                                                                            // this.setState({
                                                                            //     creditCardNumber: e.currentTarget.value.replace(/[^\d]/g, '')
                                                                            // });
                                                                        }}
                                                                        onBlur={handleBlur}
                                                                        value={
                                                                            creditCardNumber
                                                                        }
                                                                    />
                                                                </div>
                                                                <div className="form-input">
                                                                    <TracField
                                                                        id="creditCardValid"
                                                                        name="creditCardValid"
                                                                        type="text"
                                                                        labelName="Valid Until"
                                                                        onChange={e => {
                                                                            if(e.currentTarget.value.length<5)
                                                                            this.setState({
                                                                                creditCardValid: e.currentTarget.value.replace(/[^\d]/g, '')
                                                                            });
                                                                        }}
                                                                        onBlur={
                                                                            handleBlur
                                                                        }
                                                                        value={
                                                                            creditCardValid
                                                                        }
                                                                    />
                                                                </div>
                                                                <div className="form-input">
                                                                    <div className="col">
                                                                        <TracField
                                                                            id="cardPublish"
                                                                            name="cardPublish"
                                                                            type="text"
                                                                            labelName="Bank"
                                                                            onChange={e => {
                                                                                this.setState({
                                                                                    bank: e.currentTarget.value
                                                                                });
                                                                            }}
                                                                            onBlur={
                                                                                handleBlur
                                                                            }
                                                                            value={
                                                                                bank
                                                                            }
                                                                        />
                                                                    </div>
                                                                    <div className="col">
                                                                        <TracField
                                                                            id="cardType"
                                                                            name="cardType"
                                                                            type="text"
                                                                            readonly
                                                                            labelName="Card Type"
                                                                            onBlur={
                                                                                handleBlur
                                                                            }
                                                                            value={
                                                                                cardType
                                                                            }
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div className="form-input terms-condition">
                                                                    <div className="inline">
                                                                        <TracField
                                                                            id="aggrement"
                                                                            name="aggrement"
                                                                            type="checkbox"
                                                                            labelName="I understand the "
                                                                            labelSize="small"
                                                                            checked={aggrement}
                                                                            onChange={() =>
                                                                                this.setState({aggrement: !this.state.aggrement})
                                                                            }
                                                                        />
                                                                    </div>
                                                                    <div className="inline">
                                                                        <a href="https://developers.facebook.com" onClick={this.handleTermsModal('open')}>Terms and Conditions</a>
                                                                        <Modal
                                                                            open={termsModal}
                                                                            extraClass="modal-on-add-card"
                                                                            onClick={this.handleTermsModal('close')}
                                                                            header={{
                                                                                withCloseButton: true,
                                                                                onClick: this.handleTermsModal('close'),
                                                                                children: <Text type="h3">Terms and Condition</Text>,
                                                                            }}
                                                                            content={{
                                                                                children: (
                                                                                    <div>
                                                                                        <ol>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                                                        </ol>
                                                                                    </div>
                                                                                ),
                                                                            }}
                                                                            footer={{
                                                                                position: 'center',
                                                                                children: (
                                                                                    <div className="modal-footer-action">
                                                                                        <Button primary onClick={this.handleTermsModal('close')}>I Agree</Button>
                                                                                        <Button outline onClick={this.handleTermsModal('close')}>Decline</Button>
                                                                                    </div>
                                                                                ),
                                                                            }}
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="right-form">
                                                            <div className="form-col">
                                                                <div className="profile-photo-wrapper" style={{padding:10}}>
                                                                    <img
                                                                        src = {ImagePath !== "" ? ImagePath : imgTmp}
                                                                        alt="profile"
                                                                        style={{ objectFit: "cover", width:150, border: '2px solid #d4d2d2',borderRadius: 10}}
                                                                    />
                                                                    <div className=" changePhotoAccount style-guide-space-y style-guide-space">
                                                                        <input
                                                                            type="file"
                                                                            className="input-file change-photo"
                                                                            id="changePhoto"
                                                                            ref="changePhoto"
                                                                            onChange={photoChange}
                                                                        />
                                                                        <Button outline onClick={handleChangePhoto}>
                                                                            UPLOAD PHOTO
                                                                        </Button>
                                                                    </div>
                                                                </div>
                                                                {/*<div className="form-input save-info">*/}
                                                                    {/*<div className="desc">*/}
                                                                        {/*Set As Primary*/}
                                                                    {/*</div>*/}
                                                                    {/*/!*<div className="desc">*!/*/}
                                                                        {/*/!*Save Card Detail to*!/*/}
                                                                        {/*/!*make your booking*!/*/}
                                                                        {/*/!*faster*!/*/}
                                                                    {/*/!*</div>*!/*/}
                                                                    {/*<div className="toggle-wrapper">*/}
                                                                        {/*<div className="action-wrapper-toggle">*/}
                                                                            {/*<Toggle*/}
                                                                                {/*id="save-card"*/}
                                                                                {/*defaultValue={*/}
                                                                                    {/*this*/}
                                                                                        {/*.state*/}
                                                                                        {/*.savedCard*/}
                                                                                {/*}*/}
                                                                                {/*onValueChange={() =>*/}
                                                                                    {/*this.setState(*/}
                                                                                        {/*{*/}
                                                                                            {/*savedCard: !this*/}
                                                                                                {/*.state*/}
                                                                                                {/*.savedCard*/}
                                                                                        {/*}*/}
                                                                                    {/*)*/}
                                                                                {/*}*/}
                                                                            {/*/>*/}
                                                                        {/*</div>*/}
                                                                    {/*</div>*/}
                                                                {/*</div>*/}
                                                            </div>
                                                        </div>
                                                    </div>


                                                </Form>
                                            )}
                                        </Formik>
                                    </div>
                                </Fragment>
                            </div>
                        </div>
                    )
                }}
                footer={{
                    position: "center",
                    children: (
                        <div className="dashboard-popup-footer add-card">
                            <Button primary onClick={() => {
                                this.kosongState();
                                submitAdd()
                            }} disabled={!aggrement || creditCardNumber=='' || creditCardValid=='' || bank=='' || cardType=='' || ImagePath==''}>
                                Add Card
                            </Button>
                        </div>
                    )
                }}
            />
        );
    }
}


const mapStateToProps = ({ dashboard }) => {
    const {  postCreditCardFailure, postCreditCardSuccess } = dashboard;
    return {  postCreditCardFailure, postCreditCardSuccess }
}

export default connect(mapStateToProps, {
   postCreditCard
})(ModalAddCard);


// export default ModalAddCard;
