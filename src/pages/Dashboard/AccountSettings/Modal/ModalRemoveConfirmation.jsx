import { Button, Modal } from "atom";
import React, { Component } from "react";

class ModalRemoveConfirmation extends Component {
    render() {
        const { showModal, onSubmit, onClose } = this.props;
        return (
            <Modal
                open={showModal}
                extraClass="modal-remove-confirmation"
                onClick={onClose}
                size="medium"
                header={{
                    withCloseButton: false,
                    onClick: () => console.log("header close"),
                    children: ''
                }}
                content={{
                    children: (
                        <div className="dashboard-popup-content">
                            <p className="title-note">Are you sure you want to delete your credit card?</p>
                            <p className="sub-note">add card if you want to use payment with your credit card</p>
                        </div>
                    )
                }}
                footer={{
                    position: "center",
                    children: (
                        <div className="success">
                            <div className="button-wrapper">
                                <Button primary onClick={onSubmit}>
                                    CONFIRM DELETE CREDIT CARD
                                </Button>
                            </div>
                            <div className="button-wrapper">
                                <Button outline onClick={onClose}>
                                    NO, DON'T DELETE
                                </Button>
                            </div>
                        </div>
                    )
                }}
            />
        );
    }
}

export default ModalRemoveConfirmation;
