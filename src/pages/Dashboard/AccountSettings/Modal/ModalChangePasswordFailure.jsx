import { Button, Modal } from "atom";
import React, { Component } from "react";
import SuccessImg from '../../../../assets/images/icons/success-change-password.svg';

class ModalChangePasswordFailure extends Component {
    render() {
        const { showModal, onSubmit, onClose } = this.props;
        return (
            <Modal
                open={showModal}
                extraClass="success"
                onClick={onClose}
                size="medium"
                header={{
                    withCloseButton: false,
                    onClick: () => console.log("header close"),
                    children: ''
                }}
                content={{
                    children: (
                        <div className="dashboard-popup-content">
                            <p className="success-text">{this.props.errorMsg ? this.props.errorMsg : "The password and old password must be different"}</p>
                        </div>
                    )
                }}
                footer={{
                    position: "center",
                    children: (
                        <div className="success">
                            <Button primary onClick={onSubmit}>
                                Done
                            </Button>
                        </div>
                    )
                }}
            />
        );
    }
}

export default ModalChangePasswordFailure;
