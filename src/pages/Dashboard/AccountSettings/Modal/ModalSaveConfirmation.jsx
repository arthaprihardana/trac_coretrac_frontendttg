import { Button, Modal } from "atom";
import React, { Component } from "react";
import lang from '../../../../assets/data-master/language'

class ModalSaveConfirmation extends Component {
    render() {
        const { showModal, onSubmit, onClose } = this.props;
        return (
            <Modal
                open={showModal}
                extraClass="modal-remove-confirmation"
                onClick={onClose}
                size="medium"
                header={{
                    withCloseButton: false,
                    onClick: () => console.log("header close"),
                    children: ''
                }}
                content={{
                    children: (
                        <div className="dashboard-popup-content">
                            <p className="title-note">{lang.wouldYouLikeToSave[lang.default]}</p>
                        </div>
                    )
                }}
                footer={{
                    position: "center",
                    children: (
                        <div className="success">
                            <div className="button-wrapper">
                                <Button primary onClick={onSubmit}>
                                    Save
                                </Button>
                            </div>
                            <div className="button-wrapper">
                                <Button outline onClick={onClose}>
                                    Discard
                                </Button>
                            </div>
                        </div>
                    )
                }}
            />
        );
    }
}

export default ModalSaveConfirmation;
