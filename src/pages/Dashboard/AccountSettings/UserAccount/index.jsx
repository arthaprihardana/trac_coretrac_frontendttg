/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-22 17:34:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-23 22:51:25
 */
import React, { Component, Fragment } from "react";
import { TracField, Button } from "atom";
import { Form, Formik } from "formik";
import Images from "assets/images/dummy/unknown.png";
import { dashboardPopupSetup } from "helpers";
import { CustomerGlobal } from 'context/RentContext';
import { connect } from "react-redux";
import lang from '../../../../assets/data-master/language'
// flags
import flagId from "assets/images/flags/id.svg";
import flagEn from "assets/images/flags/en.svg";
import ModalChangePassword from "../Modal/ModalChangePassword";
import ModalChangePasswordSuccess from "../Modal/ModalChangePasswordSuccess";
import ModalChangePasswordFailure from "../Modal/ModalChangePasswordFailure";
import { setFormAccount, updatePassword } from "actions";
import { localStorageDecrypt } from "helpers";
const optionPhoneNumber = [
    {
        id: "id",
        value: "+62",
        text: "+(62) Indonesia",
        icon: flagId
    },
    {
        id: "us",
        value: "+1",
        text: "(+1) United State",
        icon: flagEn
    }
];

class UserAccount extends Component {
    state = {
        phoneNumber: {
            showItem: false,
            selectedValue: "+62",
            selectedIcon: flagId
        },
        changePasswordPopup: false,
        successPopup: false,
        failurePopup: false,
        imgChanged: false,
	    imgProfile: Images,
	    imgProfileBase64: "",
        displayName: "",
        errorMsg: "",
        IdUser      :"",
        EmailPersonal: "",
        NoHandphone: "",
        KTPNumber: "",
        licenseNumber: "",
        licenseNumberA: "",
        ImagePath: ""
    };

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        // updatePasswordSuccess, updatePasswordFailure
        if(this.props.user !== prevProps.user) {
            return {user:this.props.user.Data}
        }
        if(this.props.updatePasswordSuccess !== prevProps.updatePasswordSuccess) {
            console.log("SUCCESS SNAPSHOT");
            return {pwdSuccess:this.props.updatePasswordSuccess}
        }
        if(this.props.updatePasswordFailure !== prevProps.updatePasswordFailure) {
            console.log("FAILURE SNAPSHOT");
            return {pwdFailure:this.props.updatePasswordFailure}
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            console.log("snapshot=>", snapshot);
            if(snapshot.user){
                this.setState({
                    ImagePath: snapshot.user.ImagePath,
                    IdUser   : snapshot.user.Id,
                    displayName: `${snapshot.user.FirstName} ${snapshot.user.LastName}`,
                    EmailPersonal: snapshot.user.EmailPersonal,
                    NoHandphone: snapshot.user.NoHandphone !== null ? snapshot.user.NoHandphone.split(/^(\+62|0)/g)[2] : "",
                    KTPNumber: snapshot.user.NoKTP,
                    licenseNumber: snapshot.user.NoSIM,
                    licenseNumberA: snapshot.user.Address
                })
            }
            if(snapshot.pwdSuccess){
                console.log("success");
                this.setState({
                    changePasswordPopup: false
                }, () => {
                    dashboardPopupSetup(true);
                    this.setState({
                        successPopup: true
                    })
                })
            }
            if(snapshot.pwdFailure){
                console.log("failure");
                this.setState({
                    errorMsg:snapshot.pwdFailure.ErrorMessage,
                    failurePopup: true
                })
            }
        }
    }

    handleClickPhoneNumberMenu = e => {
        this.setState(prevState => ({
            ...prevState,
            phoneNumber: {
                ...this.state.phoneNumber,
                showItem: !prevState.phoneNumber.showItem
            }
        }));
        e.preventDefault();
    };

    handleItemPhoneNumberClick = (e, v) => {
        const icon = e.currentTarget.querySelector("img").getAttribute("src");
        this.setState(prevState => ({
            ...prevState,
            phoneNumber: {
                showItem: false,
                selectedValue: v,
                selectedIcon: icon
            }
        }));
        e.preventDefault();
    };

    handleChangePassword = () => {
        dashboardPopupSetup(true);
        this.setState({
            changePasswordPopup: true
        })
    }

    popupSubmit = (e) => {
        console.log("VALUE", e);
        dashboardPopupSetup(false);
        let local_auth = localStorageDecrypt('_aaa', 'object');

        this.props.updatePassword({
            UserProfileId           : local_auth.UserLoginId,
            password                : e.newPassword,
            password_confirmation   : e.confirmPassword,
            old_password            : e.oldPassword
        });
    }

    closeChangePassword = () => {
        dashboardPopupSetup(false);
        this.setState({
            changePasswordPopup: false
        })
    }

    successClose = () => {
        dashboardPopupSetup(false);
        this.setState({
            successPopup: false
        })
    }
    failureClose = () => {
        dashboardPopupSetup(false);
        this.setState({
            failurePopup: false
        })
    }
    readThis = (e) => {
        var canvas=document.createElement("canvas");
        var ctx=canvas.getContext("2d");
        var cw=canvas.width;
        var ch=canvas.height;
        var maxW=640;
        var maxH=480;
        var img = new Image;
        img.onload = ()=> {
            var iw=img.width;
            var ih=img.height;
            var scale=Math.min((maxW/iw),(maxH/ih));
            var iwScaled=iw*scale;
            var ihScaled=ih*scale;
            canvas.width=iwScaled;
            canvas.height=ihScaled;
            ctx.drawImage(img,0,0,iwScaled,ihScaled);
            let tmp = canvas.toDataURL("image/jpeg",0.5);
            this.setState({
                imgChanged:true,
                imgProfileBase64: tmp,
                imgProfile: tmp
            });
            this.props.setFormAccount({
                ImagePath  :tmp,
                phoneCode  : this.state.phoneNumber.selectedValue,
                IdUser     : this.state.IdUser,
                displayName: this.state.displayName,
                NoHandphone: this.state.NoHandphone
            });
            console.log(tmp);
        };
        img.src = URL.createObjectURL(e.files[0]);
    };
    // readThis(inputValue ) {
    //     var file= inputValue.files[0];
    //     var myReader = new FileReader();
    //
    //     myReader.onloadend = (e) => {
    //         console.log(myReader.result);
    //         this.setState({
    //             imgChanged:true,
    //             imgProfileBase64: myReader.result,
    //             imgProfile: URL.createObjectURL(inputValue.files[0])
    //         });
    //         this.props.setFormAccount({
    //             ImagePath  : myReader.result,
    //             phoneCode  : this.state.phoneNumber.selectedValue,
    //             IdUser     : this.state.IdUser,
    //             displayName: this.state.displayName,
    //             NoHandphone: this.state.NoHandphone
    //         });
    //         // return myReader.result;
    //     };
    //     myReader.readAsDataURL(file);
    // }

    handleChangePhoto = () => {
        let changePhoto = this.refs.changePhoto;
        changePhoto.click();
    };

    photoChange = (e) => {
        this.readThis(e.target)
        // this.setState({
        //     imgChanged:true,
        //     imgProfileBase64: ,
        //     imgProfile:
        // });
        // this.props.setFormAccount({
        //     ImagePath  : this.readThis(e.target),
        //     displayName: this.state.displayName,
        //     NoHandphone: this.state.NoHandphone
        // });
        // console.log(this.readThis(e.target));
    }

    handleChange = (key) => {
        // this.props.setFormAccount({
        //     [key]: this.state[key],
        // });
        console.log(this.state.imgChanged);
        if(this.state.imgChanged){
            this.props.setFormAccount({
                phoneCode  : this.state.phoneNumber.selectedValue,
                ImagePath  : this.state.imgProfileBase64,
                IdUser     : this.state.IdUser,
                displayName: this.state.displayName,
                NoHandphone: this.state.NoHandphone
            });
        }else{

            this.props.setFormAccount({
                phoneCode  : this.state.phoneNumber.selectedValue,
                displayName: this.state.displayName,
                IdUser     : this.state.IdUser,
                NoHandphone: this.state.NoHandphone
            });
        }

    }

    render() {
        const {
            handleItemPhoneNumberClick,
            handleClickPhoneNumberMenu,
            popupSubmit,
            handleChangePassword,
            closeChangePassword,
            successClose,
            failureClose,
            handleChangePhoto,
	    photoChange,
            state: {
                ImagePath,
                phoneNumber,
                failurePopup,
                changePasswordPopup,
                successPopup,
                displayName,
                EmailPersonal,
                NoHandphone,
                KTPNumber,
                licenseNumber,
                errorMsg,
                licenseNumberA,
		        imgProfile
            }
        } = this;
	const {state: {loginType}} = this.props.rentContext;

        return (
            <Fragment>

                <div className="profile-account-section section-wrapper">
                    <h3>{lang.tracAccount[lang.default]}</h3>
                    <div className="profile-photo-wrapper">
                        <img
                            className="profileAccount"
                            src={ImagePath !== null ? ImagePath : imgProfile}
                            alt="profile"
                            style={{ objectFit: "cover"}}
                        />
                        <div className=" changePhotoAccount style-guide-space-y style-guide-space">
                            <input
                                type="file"
                                className="input-file change-photo"
                                ref="changePhoto"
				onChange={photoChange}
                            />
                            <Button outline onClick={handleChangePhoto}>
                                {lang.changePhoto[lang.default]}
                            </Button>
                        </div>
                    </div>
                    <Formik
                        initialValues={{
                            displayName: displayName,
                            countryCode: +62,
                            EmailPersonal: EmailPersonal,
                            phoneNumber: NoHandphone,
                            KTPNumber: KTPNumber,
                            licenseNumber: licenseNumber,
                            licenseNumberA: licenseNumberA
                        }}
                    >
                        {({ isSubmitting, handleChange, handleBlur, values }) => (
                            <Form>
                                <div className="left-form">
                                    <div className="form-col">
                                        <TracField
                                            id="displayName"
                                            name="displayName"
                                            type="text"
                                            labelName={lang.displayName[lang.default]}
                                            // onChange={handleChange}
                                            onChange={e => {
                                                this.setState({
                                                    displayName: e.currentTarget.value
                                                }, () => this.handleChange('displayName'));
                                            }}
                                            onBlur={handleBlur}
                                            value={values.displayName || displayName}
                                        />
                                    </div>
                                    <div className="form-col">
					{
                        loginType === 'email' &&
                        // (
                        //     <Fragment>
                        //         <label htmlFor="">Email</label>
                        //         <input  type="text" value={values.email || email} />
                        //     </Fragment>
                        // )
                        (

				                        <TracField
				                            id="email"
				                            name="email"
				                            type="text"
				                            labelName="Email"
				                            // onChange={handleChange}
                                            // disabled={true}
				                            onBlur={handleBlur}
                                            onChange={e => {
                                                this.setState({
                                                    EmailPersonal: this.state.EmailPersonal
                                                }, () => this.handleChange('EmailPersonal'));
                                            }}
				                            value={values.EmailPersonal || EmailPersonal}
				                            readonly
				                        />
                        )
					}
					{
                                            loginType === 'facebook' && (
                                                <div className="social-icon">
                                                    <p className="label">Linked Account</p>
                                                    <div className="badge-wrapper">
                                                        <div className="badge facebook">Facebook</div>
                                                    </div>
                                                </div> 
                                            )
                                        }
                                        {
                                            loginType === 'google' && (
                                                <div className="social-icon">
                                                    <p className="label">Linked Account</p>
                                                    <div className="badge-wrapper">
                                                        <p className="text-badge">barrybryan@gmail.com</p>
                                                        <div className="badge gmail">Google</div>
                                                    </div>
                                                </div>
                                            )
                                        }
                                    </div>
                                </div>
                                <div className="right-form">
                                    <div className="form-col">
                                        <div className="input-group">
                                            <div className="input-group-col phone-number">
                                                <TracField
                                                    id="phoneNumberCountryCode"
                                                    type="select"
                                                    name="phone_number_country_code"
                                                    labelName={lang.phoneNumber[lang.default]}
                                                    showItem={phoneNumber.showItem}
                                                    onClick={
                                                        handleClickPhoneNumberMenu
                                                    }
                                                    onItemClick={
                                                        handleItemPhoneNumberClick
                                                    }

                                                    options={optionPhoneNumber}
                                                    value={
                                                        phoneNumber.selectedValue
                                                    }
                                                    icon={phoneNumber.selectedIcon}
                                                    listHasIcon
                                                    displayIcon
                                                />
                                            </div>
                                            <div className="input-group-col">
                                                <TracField
                                                    id="phoneNumber"
                                                    name="phoneNumber"
                                                    type="text"
                                                    labelName={lang.phoneNumber[lang.default]}
                                                    // onChange={handleChange}
                                                    onChange={e => {
                                                        if(e.currentTarget.value.length<14)
                                                            this.setState({
                                                                NoHandphone: e.currentTarget.value.replace(/[^\d]/g, '')
                                                            }, () => this.handleChange('NoHandphone'));
                                                    }}
                                                    onBlur={handleBlur}
                                                    value={values.phoneNumber || NoHandphone}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="change-password-wrapper">
                                    <Button outline onClick={handleChangePassword}> {lang.changePassword[lang.default]} </Button>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
                <ModalChangePassword showModal={changePasswordPopup} onSubmit={popupSubmit} onClose={closeChangePassword} />
                <ModalChangePasswordSuccess showModal={successPopup} onSubmit={successClose} onClose={successClose} />
                <ModalChangePasswordFailure showModal={failurePopup} onSubmit={failureClose} errorMsg={errorMsg} onClose={failureClose} />
            </Fragment>
        );
    }
}

const mapStateToProps = ({ userprofile }) => {
    const { loading, user, updatePasswordSuccess, updatePasswordFailure } = userprofile;
    return { loading, user, updatePasswordSuccess, updatePasswordFailure };
}

export default connect(mapStateToProps, {
    updatePassword,
    setFormAccount
})(CustomerGlobal(UserAccount));
