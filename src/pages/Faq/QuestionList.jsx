import React, { Component } from "react";

// components
import "./Faq.scss";

class QuestionList extends Component {

    render() {
        return(
            <div>
                <div className="info-header">
                    <h3>{this.props.data}</h3>
                </div>
                <div className="list-question-wrapper">
                    {this.props.questlist}
                </div>
            </div>
        );
    }
}

export default QuestionList;