import React, { PureComponent } from "react";

// components
import { Main } from "templates";
import { Accordion } from 'organisms';
import "./Faq.scss";
import "../../organisms/Footer/Footer.scss";
import "./QuestionList";
import QuestionList from "./QuestionList";
import faqDataJSON from '../../assets/data-master/faq.json';
import lang from '../../assets/data-master/language'

class Faq extends PureComponent {
    state = {
        data : "",
        all_data:"",
        btnobj : [
            {label:"PERTANYAAN UMUM", id:1},
            {label:"WEBSITE & AKUN TRAC", id:2},
            {label:"DAILY CAR RENTAL", id:3},
            {label:"AIRPORT TRANSFER", id:4},
            {label:"BUS RENTAL", id:5},
            {label:"PAKET WISATA", id:6},
            {label:"PEMBAYARAN", id:7},
            {label:"RENTAL MOBIL KORPORASI", id:8},
            {label:"ASTRA FMS", id:9},
            {label:"BANTUAN SELAMA MASA SEWA", id:10}
        ],
        queobj : [],
    };

    componentDidMount() {
        document.body.classList.add("add-padding-top");
        let complete = [
            ...faqDataJSON.question.umum, ...faqDataJSON.question.website, ...faqDataJSON.question.carRental, ...faqDataJSON.question.aiport,  ...faqDataJSON.question.busRental,  ...faqDataJSON.question.wisata,  ...faqDataJSON.question.pembayaran, ...faqDataJSON.question.fms, ...faqDataJSON.question.mobilKorporasi,  ...faqDataJSON.question.bantuanSelamaSewa
        ];

        this.setState({
            all_data: complete
        });
        this.removeActiveButton();
        // document.querySelectorAll(".badges")[0].classList.add('active');
        this.setState({
            data : "ALL QUESTION",
            queobj : complete
        });
    }

    removeActiveButton = () => {
        for(let i = 0; i <= 8;i++){
            document.querySelectorAll(".badges")[i].classList.remove('active');
        }
    }

    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
    }

    changeType = (i, j) => {
        this.removeActiveButton();
        //console.log(e)
        if(i === 1){
            this.setState({
                data : j,
                queobj : faqDataJSON.question.umum
            });
        }else 
        if(i === 2){
            this.setState({
                data : j,
                queobj : faqDataJSON.question.website
            });
        }else 
        if(i === 3){
            this.setState({
                data : j,
                queobj : faqDataJSON.question.carRental
            });
        }else 
        if(i === 4){
            this.setState({
                data : j,
                queobj : faqDataJSON.question.aiport
            });
        }else 
        if(i === 5){
            this.setState({
                data : j,
                queobj : faqDataJSON.question.busRental
            });
        }else 
        if(i === 6){
            this.setState({
                data : j,
                queobj : faqDataJSON.question.wisata
            });
        }else 
        if(i === 7){
            this.setState({
                data : j,
                queobj : faqDataJSON.question.pembayaran
            });
        }else
        if(i === 8){
            this.setState({
                data : j,
                queobj : faqDataJSON.question.mobilKorporasi
            });
        }else
        if(i === 9){
            this.setState({
                data : j,
                queobj : faqDataJSON.question.fms
            });
        }
        else{
            this.setState({
                data : j,
                queobj : faqDataJSON.question.bantuanSelamaSewa
            });
        }
        
    }

    filter_ = (e) =>{
        let all_data = this.state.all_data;
        let hasil_filter=  all_data.filter(x=>{
            let tmp =x.question.toLowerCase();
            return tmp.includes(e.toLowerCase());

        });
        if(e.length>0){
            this.setState({
                data : "Search Result of \""+e+"\"",
                queobj : hasil_filter
            });
        }else{
            this.setState({
                data : "ALL QUESTION",
                queobj : all_data
            });
        }
    };

    render() {  
        
        return (
            <Main solid >
                <div className="bg-blue">
                    <div className="header-faq title-header faq-content-wrapper container">
                        <h2 className="txt-label">{lang.whatWeCanHelpYou[lang.default]}</h2>
                        <form action='#'>
                            <input
                            type='text'
                            className='input-text'
                            onChange={e => {
                                this.filter_(e.currentTarget.value)
                            }}
                            placeholder={lang.typeHereToFind[lang.default]}
                            />
                            <button className='submit-button' >
                            {lang.sendButton[lang.default]}
                            </button>
                        </form>
                    </div>
                </div>
                <div className="faq-content-wrapper container">
                    <div className="faq-button-list">
                        {this.state.btnobj.map((item, index) => (
                            <ButtonList key={index} btnClick={this.changeType} label={item.label} id={item.id} />
                        ))}
                    </div>
                   <QuestionList data={this.state.data} questlist= {this.state.queobj.map((item, index) => (
                        <Question key={index} question={item.question} answer={item.answer} />
                    ))}/>
                </div>
            </Main>
        );
    }
}

class ButtonList extends React.Component {

    selectData = (e) => {

        let selector = e.currentTarget;
        if (selector.classList.contains("active")) {
            selector.classList.remove("active");
        } else {
            selector.classList.add("active");
        }
        this.props.btnClick(this.props.id, this.props.label)
        // console.log(this.props.label);
    }

    render() {
        return(
            <button 
                id={this.props.id}
                type="buttons" 
                className="badges" 
                onClick={e => this.selectData(e)} >
                {this.props.label}
            </button>
        );
    }
}

class Question extends React.Component {
    render() {
        return(
            <Accordion
                title={this.props.question}
                defaultValue={false}
            >
                <p>{this.props.answer}</p> 
            </Accordion>
        );
    }
}
export default Faq;

