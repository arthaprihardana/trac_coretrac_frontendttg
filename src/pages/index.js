/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-23 22:11:16 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 09:57:51
 */
export { default as Home } from './Home';
export { default as StyleGuide } from './StyleGuide';
export { default as Login } from './Auth/Login';
export { default as Register } from './Auth/Register';
export { default as NewPassword } from './Auth/NewPassword';
export { default as ForgotPassword } from './Auth/ForgotPassword';
export { default as Order } from './Order';
export { default as PersonalDetail } from './PersonalDetail';
export { default as PersonalDetailCreateAccount } from './PersonalDetail/PersonalDetailCreateAccount';
export { default as PersonalDetailLogin } from './PersonalDetail/PersonalDetailLogin';
export { default as FlightDetail } from './FlightDetail';
export { default as TransportList } from './TransportList';
export { default as Payment } from './Payment';
export { default as BookingSuccess } from './BookingSuccess';
export { default as NotFound } from './NotFound';
export { default as PickupDetails } from './PickupDetails';
export { default as Dashboard } from './Dashboard';
export { default as Faq } from './Faq';
export { default as NewsAndEvent } from './NewsAndEvent';
export { default as NewsDetail } from './NewsAndEvent/NewsContentDetail';
export { default as VerificationRegister } from './SinglePageVerification/register';
export { default as VerificationForgotPassword } from './SinglePageVerification/forgotPassword';
// export { default as LocatorMobile } from './SinglePageVerification/locator_mobile';

export { default as SiteMap } from './SiteMap';
export { default as AboutUs } from './AboutUs';
export { default as OutletLocations } from './OutletLocations';
export { default as Tnc } from './Tnc';
export { default as Link } from './Link';
export { default as ProductService } from './ProductService';
export { default as PrivacyPolicy } from './PrivacyPolicy';
export { default as PromotionPage } from './PromotionPage';
export { default as PromotionDetail } from './PromotionPage/PromoContentDetail/PromotionDetail';
export { default as DetailCarLease } from './ProductService/ProductServiceDetail/DetailCarLease';
export { default as DetailDriverLease } from './ProductService/ProductServiceDetail/DetailDriverLease';
export { default as DetailMotorLease } from './ProductService/ProductServiceDetail/DetailMotorLease';
export { default as DetailBusLease } from './ProductService/ProductServiceDetail/DetailBusLease';
export { default as DetailPersonalCar } from './ProductService/ProductServiceDetail/DetailPersonalCar';
export { default as DetailPersonalAirport } from './ProductService/ProductServiceDetail/DetailPersonalAirport';
export { default as DetailPersonalBus } from './ProductService/ProductServiceDetail/DetailPersonalBus';
export { default as DetailTracData } from './ProductService/ProductServiceDetail/DetailTracData';
export { default as DetailTracFleet } from './ProductService/ProductServiceDetail/DetailTracFleet';
export { default as DetailTracSmart } from './ProductService/ProductServiceDetail/DetailTracSmart';
export { default as DetailOperatingLease } from './ProductService/ProductServiceDetail/DetailOperatingLease';
export { default as FormContactUs } from './FormContactUs';
