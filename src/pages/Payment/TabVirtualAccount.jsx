import React, { PureComponent, Fragment } from 'react';
import { TracField } from 'atom';
import { Form, Formik } from 'formik';

import permata from 'assets/images/dummy/bank-permata.png';
import mandiri from 'assets/images/dummy/bank-mandiri.png';
import bca from 'assets/images/dummy/bank-bca.png';

import lang from '../../assets/data-master/language'

class TabVirtualAccount extends PureComponent {
    state={
        selectedBank: 'bank-permata'
    }

    render() {
        let {selectedBank} = this.state;

        return (
            <Fragment>
                <div className="info-header">
                    <h3>Virtual Account</h3>
                </div>
                <p className="sub-title">{lang.youCanTransfer[lang.default]}</p>
                <div className="virtual-account-form">
                    <Formik>
                        {({ handleChange, handleBlur, values }) => (
                            <Form>
                                <div className="form-input">
                                    <TracField
                                        id="bank-permata"
                                        name="radio"
                                        type="radio-custom"
                                        checked={selectedBank}
                                        onChange={(val)=>this.setState({selectedBank: val})}
                                        initial
                                    ><img src={permata} alt="permata"/></TracField>
                                    <TracField
                                        id="bank-mandiri"
                                        name="radio"
                                        type="radio-custom"
                                        checked={selectedBank}
                                        onChange={(val)=>this.setState({selectedBank: val})}
                                        initial
                                    ><img src={mandiri} alt="mandiri"/></TracField>
                                    <TracField
                                        id="bank-bca"
                                        name="radio"
                                        type="radio-custom"
                                        checked={selectedBank}
                                        onChange={(val)=>this.setState({selectedBank: val})}
                                        initial
                                    ><img src={bca} alt="bca"/></TracField>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </Fragment>
        )
    }
}

export default TabVirtualAccount;