import React, { PureComponent } from 'react';
import { OrderSteps } from 'molecules';
import { Main } from 'templates';
import { SideOrderDetail } from 'organisms';
import './Payment.scss';
import FormAction from './thisComponent/FormAction';
import TabCreditCard from './thisComponent/TabCreditCard';
import TabVirtualAccount from './thisComponent/TabVirtualAccount';
import DetailVirtualAccount from './thisComponent/DetailVirtualAccount';
import { delay } from 'helpers';
import { withRouter } from 'react-router-dom';

import selectedCarJSON from "../../assets/data-dummy/selected-car.json";
import bookingDetailJSON from "assets/data-dummy/booking.json";

import cx from 'classnames';
import { CustomerGlobal } from 'context/RentContext';

const selectedCarDummy = selectedCarJSON.map(car => {
    // car.img = require(`../../assets/images/dummy/cars/${car.img}`);
    return car;
});

class Payment extends PureComponent {
    state = {
        tabActive: 'creditCard',
        selectedCar: selectedCarDummy,
        bookingDetail: bookingDetailJSON,
    }
    componentDidMount() {
        document.body.classList.add('add-padding-top');
        document.body.classList.add('background-grey');
    }

    componentWillUnmount() {
        document.body.classList.remove('add-padding-top');
        document.body.classList.remove('background-grey');
    }

    tabChange = (type) => (e) => {
        this.setState({ tabActive: type })
    }

    handleFormSubmit = () => {
        const {tabActive} = this.state;
        const { history: { push } } = this.props;

        if(tabActive === 'virtualAccount'){
            this.setState({tabActive: 'detailVirtualAccount'})
        }else {
            delay(1000).then(() => push('/booking-success'));
        }
    }

    changePayment = () => {
        this.setState({tabActive: 'virtualAccount'})
    }

    render() {
        let { tabActive, selectedCar, bookingDetail } = this.state;
        let tabToShow = null;
        switch (tabActive) {
            case 'creditCard':
                tabToShow = <TabCreditCard />
                break;
            case 'virtualAccount':
                tabToShow = <TabVirtualAccount />;
                break;
            case 'detailVirtualAccount':
                tabToShow = <DetailVirtualAccount handleChange={this.changePayment} handleDone={this.handleFormSubmit} />;
                break;
            default:
                tabToShow = null;
        }

        let orderStepsData = [
            { step: 'Car Order', className: 'done' },
            { step: 'Personal Detail', className: 'done' },
            { step: 'Payment', className: 'current' },
        ];

        if(this.props.rentContext.state.values.type_service === '2'){
            orderStepsData = [
                { step: "Car Order", className: "done" },
                { step: "Personal Detail", className: "done" },
                { step: "Driver Information", className: "done" },
                { step: "Payment", className: "current" }
            ];
        }

        return (
            <Main solid headerUserLogin headerClose>
                <OrderSteps data={orderStepsData} />
                <div className="order-detail">
                    <div className="container">
                        <div className="order-detail-container">
                            <div className="left-content">
                                <div className="p-payment">
                                    <div className="payment-option">
                                        <h3 className="title">Payment</h3>
                                        <ul className="list-payment">
                                            <li className={cx({'active': tabActive === 'creditCard'})} onClick={this.tabChange('creditCard')}>Credit/Debit Card</li>
                                            <li className={cx({'active': tabActive === 'virtualAccount' || tabActive === 'detailVirtualAccount'})} onClick={this.tabChange('virtualAccount')}>Virtual Account</li>
                                        </ul>
                                    </div>
                                    <div className="payment-info">
                                        <div className="info-wrapper">
                                            {tabToShow}
                                            {tabActive !== 'detailVirtualAccount' && <FormAction onSubmit={this.handleFormSubmit} />}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <SideOrderDetail selectedCar={selectedCar} bookingDetail={bookingDetail} />

                        </div>
                    </div>
                </div>
            </Main>
        )
    }
}

export default CustomerGlobal(withRouter(Payment));