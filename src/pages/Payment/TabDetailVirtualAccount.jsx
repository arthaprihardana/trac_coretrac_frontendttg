/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-25 01:35:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-25 01:38:19
 */
import React, { PureComponent, Fragment } from 'react';
import { Button } from 'atom';
import cx from 'classnames';
import { delay } from 'helpers';
import { Accordion } from 'organisms';
import { CopyToClipboard } from 'react-copy-to-clipboard';

class DetailVirtualAccount extends PureComponent {
    state = {
        accountNumber: '5603 3028 6739 2820',
        copied: false
    }
    handleCopy = () => {
        this.setState({
            copied: true
        }, ()=>{
            delay(1000).then(()=> {
                this.setState({copied: false})
            })
        })
    }
    render() {
        let { accountNumber, copied} = this.state;
        let { handleChange, handleDone} = this.props;
        const classAccountNumber = cx('account-number-wrapper', {
            'copied': copied
        })

        return (
            <Fragment>
                <div className="info-header">
                    <h3>PermataBank Virtual Account</h3>
                </div>
                <p className="sub-title">Please complete your payment in <strong>58 minutes 25 seconds</strong></p>
                <div className="detail-virtual-account">
                    <div className={classAccountNumber}>
                        <p className="account-number">Virtual Account Number: <span>{accountNumber}</span></p>
                        <CopyToClipboard text={accountNumber} onCopy={this.handleCopy}>
                            {/* eslint-disable-next-line */}
                            <a className="copy-account-number">copy</a>
                        </CopyToClipboard>
                    </div>
                    <Accordion title="Payment via ATM" defaultValue={true}>
                        <ol className="desc-list">
                            <li>Choose Other Transaction</li>
                            <li>Choose Transfer</li>
                            <li>Choose Permata Virtual Account</li>
                            <li>Enter Virtual Account Number <span>{accountNumber}</span></li>
                            <li>Choose Correct and press Yes</li>
                            <li>Keep Payment Proof</li>
                            <li>Pyament Done</li>
                        </ol>
                    </Accordion>
                    <Accordion title="Payment via ATM">
                        <ol className="desc-list">
                            <li>Choose Other Transaction</li>
                            <li>Choose Transfer</li>
                            <li>Choose Permata Virtual Account</li>
                            <li>Enter Virtual Account Number <span>{accountNumber}</span></li>
                            <li>Choose Correct and press Yes</li>
                            <li>Keep Payment Proof</li>
                            <li>Pyament Done</li>
                        </ol>
                    </Accordion>
                </div>
                <div className="action-wrapper">
                    <Button type="button" outline onClick={handleChange}>Change Method</Button>
                    <Button type="button" primary onClick={handleDone}>Done</Button>
                </div>
            </Fragment>
        )
    }
}

export default DetailVirtualAccount;