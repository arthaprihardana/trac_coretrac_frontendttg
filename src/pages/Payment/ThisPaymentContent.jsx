import React, { Component } from 'react';
import FormAction from './ThisFormAction';
import TabCreditCard from './TabCreditCard';
import TabVirtualAccount from './TabVirtualAccount';
import DetailVirtualAccount from './TabDetailVirtualAccount';
import cx from 'classnames';
import { delay } from 'helpers';
import { withRouter } from 'react-router-dom';

class ThisPaymentContent extends Component {
    state = {
        tabActive: 'creditCard',
    }

    tabChange = (type) => (e) => {
        this.setState({ tabActive: type })
    }

    handleFormSubmit = () => {
        const {tabActive} = this.state;
        const { 
            history: { 
                push 
            },
            match: {
                params: {
                    id
                }
            }
        } = this.props;

        if(tabActive === 'virtualAccount'){
            this.setState({tabActive: 'detailVirtualAccount'})
        }else {
            delay(1000).then(() => push(`/booking-success/${id}`));
        }
    }

    render() {
        const {
            state: {
                tabActive, 
            }
        } = this;

        return (
            <div className="left-content">
                <div className="p-payment">
                    <div className="payment-option">
                        <h3 className="title">Payment</h3>
                        <ul className="list-payment">
                            <li className={cx({ 'active': tabActive === 'creditCard' })} onClick={this.tabChange('creditCard')}>Credit/Debit Card</li>
                            <li className={cx({ 'active': tabActive === 'virtualAccount' || tabActive === 'detailVirtualAccount' })} onClick={this.tabChange('virtualAccount')}>Virtual Account</li>
                        </ul>
                    </div>
                    <div className="payment-info">
                        <div className="info-wrapper">
                            {
                                tabActive === 'creditCard' && <TabCreditCard />
                            }
                            {
                                tabActive === 'virtualAccount' && <TabVirtualAccount />
                            }
                            {
                                tabActive === 'detailVirtualAccount' && <DetailVirtualAccount handleChange={this.changePayment} handleDone={this.handleFormSubmit}/>
                            }
                            {
                                tabActive !== 'detailVirtualAccount' && <FormAction onSubmit={this.handleFormSubmit} />
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(ThisPaymentContent);