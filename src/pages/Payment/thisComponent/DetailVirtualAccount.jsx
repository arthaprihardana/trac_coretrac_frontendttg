/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-15 10:21:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 01:15:08
 */
import React, { PureComponent, Fragment } from 'react';
import { withRouter } from "react-router-dom";
import { Button } from 'atom';
import cx from 'classnames';
import { delay } from 'helpers';
import { Accordion } from 'organisms';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { localStorageDecrypt } from "helpers";


import lang from '../../../assets/data-master/language'
import * as moment from "moment";
class DetailVirtualAccount extends PureComponent {

    state = {
        waitingPayment:'',
        accountNumber: '',
        dateExp:'',
        content_atm:'',
        content_atmDetail:'',
        content_internet:'',
        content_internetDetail:'',
        copied: false,
        bankName: null
    }

    componentDidMount = () => {
        let inv = localStorageDecrypt('_inv', 'object');
        let rv = localStorageDecrypt('_rv', 'object');
        // console.log("rv", rv);
        if(rv !== null){
            this.setState({
                dateExp: rv.WaitingForPaymentTime
            })
        }
        if(inv !== null) {
            let tmp;
            let tmp_atm;
            let tmp_atmDetail;
            let tmp_internet;
            let tmp_internetDetail;
            if(inv.va_bca !== undefined){
                tmp = "Bank BCA"
            }

            if(inv.va_permata !== undefined){
                tmp_atm = lang.pembayaranViaATMPermata[lang.default];
                tmp_atmDetail = lang.pembayaranViaATMPermataItem[lang.default];
                tmp_internet = lang.pembayaranViaMobilePermata[lang.default];
                tmp_internetDetail = lang.pembayaranViaMobilePermataItem[lang.default];
                tmp = "Bank Permata"
            }

            if(inv.va_mandiri !== undefined){
                tmp_internet = lang.pembayaranViaMandiriOnline[lang.default];
                tmp_internetDetail = lang.pembayaranViaMandiriOnlineItem[lang.default];
                tmp_atm = lang.pembayaranViaATMMandiri[lang.default];
                tmp_atmDetail = lang.pembayaranViaATMMandiriItem[lang.default];

                tmp = "Bank Mandiri"
            }

            this.setState({
                content_atm:tmp_atm,
                content_atmDetail:tmp_atmDetail,
                content_internet:tmp_internet,
                content_internetDetail:tmp_internetDetail,
                accountNumber: inv.va_bca || inv.va_permata || inv.va_mandiri ,
                bankName: tmp
            })
            // this.setState({
            //     accountNumber: inv.va_bca || inv.va_permata || inv.va_mandiri ,
            //     bankName: inv.va_bca !== undefined ? "Bank BCA" : "Bank Permata"
            // })
        }
    }
    msToTime = (duration)=> {
        var milliseconds = parseInt((duration % 1000) / 100),
            seconds = Math.floor((duration / 1000) % 60),
            minutes = Math.floor((duration / (1000 * 60)) % 60),
            hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + " Jam " + minutes + " Menit " + seconds + " detik ";
    };
    handleCopy = () => {
        this.setState({
            copied: true
        }, ()=>{
            delay(1000).then(()=> {
                this.setState({copied: false})
            })
        })
    }
    render() {
        let { accountNumber, copied, bankName} = this.state;
        let { handleChange, handleDone} = this.props;
        const classAccountNumber = cx('account-number-wrapper', {
            'copied': copied
        })
        let { content_atm, dateExp, content_atmDetail, content_internet, content_internetDetail } = this.state
        return (
            <Fragment>

                <div className="info-header">
                    <h3>{bankName} Virtual Account</h3>
                </div>
                <p className="sub-title">{lang.pleaseCompletePayment[lang.default]} <strong>{this.msToTime(moment(dateExp).diff(moment()))}</strong></p>
                <div className="detail-virtual-account">
                    <div className={classAccountNumber}>
                        <p className="account-number">{lang.virtualAccountNumber[lang.default]}: <span>{accountNumber}</span></p>
                        <CopyToClipboard text={accountNumber} onCopy={this.handleCopy}>
                            {/* eslint-disable-next-line */}
                            <a className="copy-account-number">copy</a>
                        </CopyToClipboard>
                    </div>
                    <Accordion title={content_atm} defaultValue={true}>
                        <div style={{whiteSpace:'pre', overflowX:'auto'}}>
                            {content_atmDetail}
                        </div>
                    </Accordion>
                    <Accordion title={content_internet}>
                        <div style={{whiteSpace:'pre', overflowX:'auto'}}>
                            {content_internetDetail}
                        </div>
                    </Accordion>
                </div>
                <div className="action-wrapper">
                    {/* <Button type="button" outline onClick={handleChange}>Change Method</Button> */}
                    <Button type="button" primary onClick={handleDone}>{lang.done[lang.default]}</Button>
                </div>
            </Fragment>
        )
    }
}

export default withRouter(DetailVirtualAccount);
