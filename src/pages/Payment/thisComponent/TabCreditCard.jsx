import React, { PureComponent, Fragment } from 'react';
import { TracField } from 'atom';
import { Form, Formik } from 'formik';
import { Toggle } from 'organisms';

class TabCreditCard extends PureComponent {
    state = {
        savedCard: false
    }

    render() {
        return (
            <Fragment>
                <div className="info-header">
                    <h3>Credit/Debit Card</h3>
                    <div className="brand-wrapper">
                        <div className="item-brand">
                            <img src={require('../../../assets/images/logo/payment-logo.png')} alt="" />
                        </div>
                    </div>
                </div>
                <div className="payment-info-form">
                    <Formik>
                        {({ handleChange, handleBlur, values }) => (
                            <Form>
                                <div className="form-input">
                                    <TracField
                                        id="creditCardNumber"
                                        name="creditCardNumber"
                                        type="text"
                                        labelName="Credit/Debit Card Number"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.creditCardNumber}
                                    />
                                </div>
                                <div className="form-input">
                                    <div className="col">
                                        <TracField
                                            id="creditCardValid"
                                            name="creditCardValid"
                                            type="text"
                                            labelName="Valid Until"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.creditCardValid}
                                        />
                                    </div>
                                    <div className="col relative">
                                        <TracField
                                            id="creditCardCVV"
                                            name="creditCardCVV"
                                            type="text"
                                            labelName="CVV"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.creditCardCVV}
                                        />
                                        <div className="info-note">
                                            <a href="https://developers.facebook.com">What is CVV?</a>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-input">
                                    <TracField
                                        id="creditCardName"
                                        name="creditCardName"
                                        type="text"
                                        labelName="Name on Card"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.creditCardName}
                                    />
                                </div>
                                <div className="form-input save-info">
                                    <div className="desc">Save Card Detail to make your booking faster</div>
                                    <div className="toggle-wrapper">
                                        <div className="action-wrapper-toggle">
                                            <Toggle id="save-card" defaultValue={this.state.savedCard} onValueChange={() => this.setState({savedCard: !this.state.savedCard})} />
                                        </div>
                                    </div>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </Fragment>
        )
    }
}

export default TabCreditCard;