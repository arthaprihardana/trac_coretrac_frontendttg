/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 00:43:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 01:45:32
 */
import React, { PureComponent, Fragment } from 'react';
import { TracField } from 'atom';
import { Form, Formik } from 'formik';
import { connect } from "react-redux";
import _ from "lodash";

// actions
import { getBank } from "actions";

// helpers
import { localStorageDecrypt } from "helpers";

// import permata from 'assets/images/dummy/bank-permata.png';
// import mandiri from 'assets/images/dummy/bank-mandiri.png';
// import bca from 'assets/images/dummy/bank-bca.png';

class TabVirtualAccount extends PureComponent {

    state={
        selectedBank: '',
        bankList: []
    }

    componentDidMount = () => {
        let BusinessUnitId = localStorageDecrypt('_bu');
        this.props.getBank({ BusinessUnitId: BusinessUnitId });
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.bank !== prevProps.bank) {
            return { bank: this.props.bank }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.bank !== undefined) {
                this.setState({
                    bankList: snapshot.bank
                });
            }
        }
    }

    render() {
        let {selectedBank, bankList} = this.state;

        return (
            <Fragment>
                <div className="info-header">
                    <h3>Virtual Account</h3>
                </div>
                <p className="sub-title">You can transfer from any banking channel (m-banking, SMS, or ATM)</p>
                <div className="virtual-account-form">
                    <Formik>
                        {({ handleChange, handleBlur, values }) => (
                            <Form>
                                <div className="form-input">
                                    { bankList.length > 0 && _.map(bankList, (v,k) => (
                                        <TracField
                                            key={k}
                                            id={v.PaymentChannel}
                                            name="radio"
                                            type="radio-custom"
                                            checked={selectedBank}
                                            onChange={ val => this.setState({ selectedBank: val }, () => this.props.onSelectBank(val)) }
                                            initial>
                                            <img src={v.Images} alt={v.MsBankName} />
                                        </TracField>
                                    )) }
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = ({ masterbank }) => {
    const { loading, bank } = masterbank;
    return { bank, loading };
}

export default connect(mapStateToProps, {
    getBank
})(TabVirtualAccount);
