/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 21:22:08 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 23:15:56
 */
import React, { PureComponent } from 'react';
import { Button, Modal, Text, TracField } from 'atom';
import { Form, Formik } from 'formik';
import { connect } from "react-redux";
import { localStorageDecrypt } from "helpers";
import _ from "lodash";

import { createInvoice, isPaymentValidate } from "actions";
import { PulseLoader } from "react-spinners";

import db from "../../../db";
import { SONotCreated } from '../../../constant';
import { localStorageEncrypt } from "helpers";

class FormAction extends PureComponent {
    state = {
        termsModal: false,
        aggrement: false,
        // selectedCar: [],
        // bookingDetail: {},
        user_login: {},
    //     BusinessUnitId: ''
    }

    componentDidMount = () => {
        let local_auth = localStorageDecrypt('_aaa', 'object');
        this.setState({ user_login: local_auth })
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.invoice !== prevProps.invoice) {
            return { invoice: this.props.invoice }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.invoice !== undefined) {
                localStorageEncrypt('_inv', JSON.stringify(snapshot.invoice.Data));
                this.props.isPaymentValidate(true);
                this.props.onSubmit();
            }
        }
    }
    
    handleTermsModal = (type) => (e) => {
        e.preventDefault();
        let action = false;
        type === 'open' ? action = true : action = false;
        this.setState({ termsModal: action })
    }

    agreeTerm = () =>{
        this.setState({
            aggrement: true,termsModal:false
        }, ()=>{
            this.props.onAggrement(this.state.aggrement)
        })

        // this.handleTermsModal('close');
    }
    disagreeTerm = () =>{
        this.setState({aggrement: false,termsModal:false})
        this.props.onAggrement(false)
        // this.handleTermsModal('close');
    }

    handleAggrement = () => {
        this.setState({aggrement: !this.state.aggrement})
    }

    handleMakePayment = () => {
        const { user_login } = this.state;
        let rv = JSON.parse(localStorageDecrypt('_rv'));
        this.props.createInvoice({
            StatusInvoice: SONotCreated,
            CustomerId: rv.CustomerId,
            CustomerName: rv.CustomerName,
            PICUserInvoice: user_login.UserLoginId,
            PICUserInvoiceEmail: user_login.EmailPersonal,
            SendMail: 0,
            PriceTotalInvoice: rv.TotalPrice,
            BusinessUnitId: rv.BusinessUnitId,
            PICUserInvoiceName: `${user_login.FirstName} ${user_login.LastName}`,
            PaymentChannel: this.props.PaymentChannel,
            detail: [{
                ReservationCode: rv.ReservationId,
                ContractSAPCode: rv.details[0].ContractId,
                MaterialSAPCode: rv.details[0].MaterialId,
                TotalDay: rv.details[0].TotalDay,
                Price: _.reduce(rv.details, (sum, n) => sum + parseInt(n.Price) ,0),
                PriceExtras: _.reduce(rv.details, (sum, n) => sum + parseInt(n.PriceExtras) ,0),
                ReservationDate: rv.created_at,
                PriceExtend: 0,
                TotalExtend: 0,
                QuantityOLC: 0,
                PriceOLC: 0,
                QuantityTrip: 0,
                PriceTrip: 0
            }]
        })
        // this.props.onSubmit()
    }

    render() {
        const { termsModal, aggrement } = this.state;
        const { loading, buttonDisable } = this.props;

        return (
            <Formik>
                {({ handleChange, handleBlur, values }) => (
                    <Form>
                        <div className="form-input">
                            <div className="inline">
                                <TracField
                                    id="aggrement"
                                    name="aggrement"
                                    type="checkbox"
                                    labelName="I understand the "
                                    labelSize="small"
                                    // onChange={this.handleAggrement}
                                    checked={aggrement}
                                    // onClick={}
                                    onChange={() =>
                                        this.setState({termsModal:true})
                                        // this.handleTermsModal('open')
                                        // this.setState({aggrement: !this.state.aggrement}, () => this.props.onAggrement(this.state.aggrement))
                                    }
                                />
                            </div>
                            <div className="inline">
                                <a href="https://developers.facebook.com" onClick={this.handleTermsModal('open')}>Terms and Conditions</a>
                                <Modal
                                    open={termsModal}
                                    onClick={this.handleTermsModal('close')}
                                    header={{
                                        withCloseButton: true,
                                        onClick: this.handleTermsModal('close'),
                                        children: <Text type="h3">Terms and Condition</Text>,
                                    }}
                                    content={{
                                        children: (
                                            <div>
                                                <ol>
                                                    <li>Anda berhak memilih metode pembayaran yang telah Kami sediakan ketika melakukan pemesanan layanan secara online, yaitu menggunakan kartu kredit atau melalui transfer rekening Virtual Account (VA) atas nama PT Serasi Autoraya. Pilihan ini akan muncul pada proses “Pembayaran” setelah Anda melengkapi detail order dan data pribadi</li>
                                                    <li>Untuk metode pembayaran melalui VA, Anda dapat memilih bank rekanan kami yang telah terdaftar di website TRAC. Lalu, Anda akan mendapatkan nomor VA. </li>
                                                    <li>Pembayaran transaksi menggunakan VA tidak dikenakan biaya pelayanan.</li>
                                                    <li>Pembayaran dengan VA hanya berlaku untuk satu tagihan terbaru dan satu nomor VA hanya berlaku untuk satu tagihan</li>
                                                    <li>Anda wajib melakukan pembayaran terlebih dulu sesuai jumlah transaksi sebelum tanggal jatuh tempo yang tertera, melalui beragam channel pembayaran seperti mobile banking, internet banking, ATM, atau teller. Setelah pembayaran terkonfirmasi, Anda akan mendapatkan nomor reservasi, detail pemesanan, dan verifikasi melalui email yang terdaftar di akun TRAC Anda</li>
                                                    <li>Selain pembayaran via Virtual Account, Anda dapat memilih metode pembayaran dengan kartu kredit. Anda bisa memasukkan kartu kredit baru atau menggunakan salah satu kartu kredit yang telah terdaftar pada akun Anda. Namun, TRAC hanya menerima pembayaran transaksi via kartu kredit berlogo Visa dan Mastercard.</li>
                                                    <li>Saat memilih metode pembayaran ini, Anda akan dibawa ke laman Token TRAC. Fasilitas ini kami sediakan agar Anda dapat melakukan proses pembayaran dengan menggunakan kartu kredit secara aman, mudah, dan cepat. Fasilitas ini juga dilengkapi dengan sistem keamanan berlapis yang terintegrasi dengan payment gateway</li>
                                                    <li>Pembayaran dengan kartu kredit ini akan terverifikasi secara otomatis sehingga Anda dapat langsung menerima nomor reservasi, detail pemesanan, dan verifikasi melalui email yang terdaftar di akun TRAC Anda</li>
                                                    <li>Jika transaksi kartu kredit tidak berhasil, pastikan kartu kredit Anda berlogo Visa atau Mastercard dan pastikan limit kartu kredit Anda mencukupi. Jika kartu kredit tetap tidak bisa digunakan, silakan hubungi bank penerbit kartu kredit tersebut</li>
                                                </ol>
                                            </div>
                                        ),
                                    }}
                                    footer={{
                                        position: 'center',
                                        children: (
                                            <div className="modal-footer-action">
                                                <Button primary onClick={this.agreeTerm}>I Agree</Button>
                                                <Button outline onClick={this.disagreeTerm}>Decline</Button>
                                            </div>
                                        ),
                                    }}
                                />
                            </div>
                        </div>
                        <div className="action-wrapper">
                            <Button type="button" disabled={buttonDisable || loading} primary onClick={() => this.handleMakePayment()}>
                                { loading ? 
                                    <PulseLoader
                                        sizeUnit={"px"}
                                        size={7}
                                        color={'#ffffff'}
                                        loading={loading}
                                    /> : "Make Payment" 
                                }
                            </Button>
                        </div>
                    </Form>
                )}
            </Formik>
        )
    }
}

const mapStateToProps = ({ billing }) => {
    const { loading, invoice } = billing;
    return { loading, invoice };
}

export default connect(mapStateToProps, {
    createInvoice,
    isPaymentValidate
})(FormAction);
