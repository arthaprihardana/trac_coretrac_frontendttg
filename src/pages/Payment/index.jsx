/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 00:25:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 16:17:09
 */
import React, { PureComponent } from 'react';
import { OrderSteps } from 'molecules';
import { Main } from 'templates';
import { SideOrderDetail } from 'organisms';
import './Payment.scss';
import FormAction from './thisComponent/FormAction';
import TabCreditCard from './thisComponent/TabCreditCard';
import TabVirtualAccount from './thisComponent/TabVirtualAccount';
import DetailVirtualAccount from './thisComponent/DetailVirtualAccount';
import { delay } from 'helpers';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import { isPromoValidate, savePromoCode, isPaymentValidate, notesRequestCarRental, setBranchFromBus} from "actions";

import selectedCarJSON from "../../assets/data-dummy/selected-car.json";
import bookingDetailJSON from "assets/data-dummy/booking.json";

import cx from 'classnames';
import { CustomerGlobal } from 'context/RentContext';

const selectedCarDummy = selectedCarJSON.map(car => {
    // car.img = require(`../../assets/images/dummy/cars/${car.img}`);
    return car;
});

class Payment extends PureComponent {

    state = {
        tabActive: 'virtualAccount',
        selectedCar: selectedCarDummy,
        bookingDetail: bookingDetailJSON,
        buttonDisable: true,
        PaymentChannel: null,
    }

    componentWillMount = () => {
        // console.log('will mount ==>')
    }

    componentDidMount() {
        // const {
        //     history: {
        //         action,
        //         push
        //     }
        // } = this.props;
        // if(action === "POP") {
        //     push("/")
        // }
        window.history.pushState(null, null, window.location.href);
        window.onpopstate = function(event) {
            window.history.go(1);
        };
        document.body.classList.add('add-padding-top');
        document.body.classList.add('background-grey');
    }

    componentWillUnmount() {
        document.body.classList.remove('add-padding-top');
        document.body.classList.remove('background-grey');
        localStorage.removeItem('persist:discount');
        localStorage.removeItem('selectedCar');
        localStorage.removeItem('listStockCar');
        localStorage.removeItem('CarRentalFormDisplay');
        localStorage.removeItem('_inv');
        localStorage.removeItem('_fi');
        this.props.isPromoValidate(false);
        this.props.savePromoCode(null);
        this.props.isPaymentValidate(false);
        this.props.notesRequestCarRental(null);
        this.props.setBranchFromBus(null);
    }

    tabChange = (type) => (e) => {
        this.setState({ tabActive: type })
    }

    handleFormSubmit = () => {
        const {tabActive} = this.state;
        const { history: { replace } } = this.props;

        if(tabActive === 'virtualAccount'){
            this.setState({tabActive: 'detailVirtualAccount'})
        }else {
            delay(1000).then(() => replace('/booking-success'));
        }
    }

    changePayment = () => {
        this.setState({tabActive: 'virtualAccount'})
    }

    handleSelectBank = (value) => {
        this.setState({ PaymentChannel: value });
    }

    handleAggrement = (value) => {
        if(value) {
            this.setState(prevState => {
                if(prevState.PaymentChannel !== null) {
                    return { buttonDisable: false }
                } else {
                    return { buttonDisable: true }
                }
            })
        } else {
            this.setState({ buttonDisable: true })
        }
    }

    render() {
        let { isPayment } = this.props;
        let { tabActive, selectedCar, bookingDetail, buttonDisable, PaymentChannel } = this.state;
        let { match: {
            params: { id }
        } } = this.props;
        let tabToShow = null;
        switch (tabActive) {
            case 'creditCard':
                tabToShow = <TabCreditCard />
                break;
            case 'virtualAccount':
                tabToShow = isPayment ? <DetailVirtualAccount handleChange={this.changePayment} handleDone={this.handleFormSubmit} /> : <TabVirtualAccount onSelectBank={this.handleSelectBank} />;
                break;
            case 'detailVirtualAccount':
                tabToShow = <DetailVirtualAccount handleChange={this.changePayment} handleDone={this.handleFormSubmit} />;
                break;
            default:
                tabToShow = null;
        }

        let orderStepsData = [
            { step: 'Car Order', className: 'done' },
            { step: 'Personal Detail', className: 'done' },
            { step: 'Payment', className: 'current' },
        ];

        if(this.props.rentContext.state.values.type_service === '2'){
            orderStepsData = [
                { step: "Car Order", className: "done" },
                { step: "Personal Detail", className: "done" },
                { step: "Driver Information", className: "done" },
                { step: "Payment", className: "current" }
            ];
        }

        return (
            <Main solid headerUserLogin headerClose>
                <OrderSteps data={orderStepsData} activeStep={3} />
                <div className="order-detail">
                    <div className="container">
                        <div className="order-detail-container">
                            <div className="left-content">
                                <div className="p-payment">
                                    <div className="payment-option">
                                        <h3 className="title">Payment</h3>
                                        <ul className="list-payment">
                                            {/* <li className={cx({'active': tabActive === 'creditCard'})} onClick={this.tabChange('creditCard')}>Credit/Debit Card</li> */}
                                            <li className={cx({'active': tabActive === 'virtualAccount' || tabActive === 'detailVirtualAccount'})} onClick={this.tabChange('virtualAccount')}>Virtual Account</li>
                                        </ul>
                                    </div>
                                    <div className="payment-info">
                                        <div className="info-wrapper">
                                            {tabToShow}
                                            {tabActive !== 'detailVirtualAccount' && !isPayment && <FormAction onSubmit={this.handleFormSubmit} buttonDisable={buttonDisable} onAggrement={this.handleAggrement} PaymentChannel={PaymentChannel} />}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <SideOrderDetail type={id} />

                        </div>
                    </div>
                </div>
            </Main>
        )
    }
}

const mapStateToProps = ({ billing }) => {
    const { isPayment } = billing;
    return { isPayment }
}

export default CustomerGlobal(
    withRouter(
        connect(mapStateToProps, {
            isPromoValidate,
            savePromoCode,
            isPaymentValidate,
            notesRequestCarRental,
            setBranchFromBus
        })(Payment)
    )
);