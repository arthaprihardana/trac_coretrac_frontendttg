import { Button, Modal } from "atom";
import React, { Component } from "react";
import SuccessImg from '../../../assets/images/icons/success-change-password.svg';

class ModalAddCardSuccess extends Component {
    render() {
        const { showModal, onSubmit, onClose } = this.props;
        return (
            <Modal
                open={showModal}
                extraClass="success add-card-success"
                onClick={onClose}
                size="medium"
                header={{
                    withCloseButton: false,
                    onClick: () => console.log("header close"),
                    children: ''
                }}
                content={{
                    children: (
                        <div className="dashboard-popup-content" style={{    width: '100%',textAlign: 'center'}}>
                            <div className="img-thumb">
                                <img src={SuccessImg} alt="success"/>
                            </div>
                            <p className="success-text">Successfully<br/> Send The Message</p>
                        </div>
                    )
                }}
                footer={{
                    position: "center",
                    children: (
                        <div className="success">
                            <Button primary onClick={onClose}>
                                Done
                            </Button>
                        </div>
                    )
                }}
            />
        );
    }
}

export default ModalAddCardSuccess;
