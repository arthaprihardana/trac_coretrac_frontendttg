import React, { Component, Fragment } from 'react';
import { TracField, Button } from "atom";
import * as Yup from 'yup';
import { Form, Formik } from "formik";
import Images from "assets/images/dummy/unknown.png";
import { createContactUs, checkIsLogin } from 'actions'
//assets
import flagId from "assets/images/flags/id.svg";
import flagEn from "assets/images/flags/en.svg";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import ModalSuccess from './ModalSuccess'
import {PulseLoader} from "react-spinners";

const optionPhoneNumber = [
    {
        id: "id",
        value: "+62",
        text: "+(62) Indonesia",
        icon: flagId
    }
];

class FormContact extends Component{
    state = {
        login_data:null,
        isLogin: false,
        popupSuccess: false,
        phoneNumber: {
            showItem: false,
            selectedValue: "+62",
            selectedIcon: flagId
        },
        changePasswordPopup: false,
        successPopup: false,
        isLoading: false,
        from : "",
        fromNames : "",
        category : "",
        subcategory:"",
        message : "",
        noTelp:"",
        attachments : [],
        imgProfile: Images,
        showItem: false,
        showItem2: false,
        selectedValue: '',
        subCategoryOption:
        [
            {
                id: "unit",
                value: "unit",
                text: "unit",
                icon: "",
            },
            {
                id: "driver",
                value: "driver",
                text: "driver",
                icon: "",
            },
            {
                id: "unitDriver",
                value: "unit & driver",
                text: "unit & driver",
                icon: "",
            }
        ],
    };

    handleClickSelectMenu = (e) => {
        e.preventDefault();
        this.setState(prevState => ({
            ...prevState,
            showItem: !prevState.showItem,
        }));
    };
    handleClickSelectMenu2 = (e) => {
        e.preventDefault();
        this.setState(prevState => ({
            ...prevState,
            showItem2: !prevState.showItem2,
        }));
    };

    componentDidMount() {
        this.props.checkIsLogin();
        if(this.props.isLogin){
            // NoHandphone LastName FirstName EmailPersonal
            this.setState({
                isLogin: true,
                login_data: this.props.localUser,
                from : this.props.localUser.EmailPersonal,
                fromNames : this.props.localUser.FirstName + ' ' + this.props.localUser.LastName,
                category : "",
                subcategory:"",
                message : "",
                noTelp:this.props.localUser.NoHandphone.split(/^(\+62|0)/g)[2]
            });
        }
    }

    handleClickPhoneNumberMenu = e => {
        this.setState(prevState => ({
            ...prevState,
            phoneNumber: {
                ...this.state.phoneNumber,
                showItem: !prevState.phoneNumber.showItem
            }
        }));
        e.preventDefault();
    };

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.isLogin !== prevProps.isLogin) {
            return { login : this.props.isLogin };
        }
        if(this.props.createContactUsSuccess !== prevProps.createContactUsSuccess) {
            return { success: this.props.createContactUsSuccess };
        }
        if(this.props.createContactUsFailure !== prevProps.createContactUsFailure) {
            return { failure: this.props.createContactUsFailure }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.login) {
                this.setState({
                    isLogin: snapshot,
                    login_data: this.props.localUser,
                    from : this.props.localUser.EmailPersonal,
                    fromNames : this.props.localUser.FirstName + ' ' + this.props.localUser.LastName,
                    category : "",
                    subcategory:"",
                    message : "",

                    noTelp:this.props.localUser.NoHandphone.split(/^(\+62|0)/g)[2]
                });
            }
            if(snapshot.success !== undefined) {
               // console.log("Success");
               this.setState({
                   popupSuccess:true,
               })
            }
            if(snapshot.failure !== undefined) {
                // console.log("Failure");
            }
        }
    }
    handleItemPhoneNumberClick = (e, v) => {
        const icon = e.currentTarget.querySelector("img").getAttribute("src");
        this.setState(prevState => ({
            ...prevState,
            phoneNumber: {
                showItem: false,
                selectedValue: v,
                selectedIcon: icon
            }
        }));
        e.preventDefault();
    };
    handleItemCategoryClick = (e, v) => {
        this.setState(prevState => ({
            ...prevState,
            showItem: false,
            category:v
        }));
        e.preventDefault();
    };
    handleItemSubCategoryClick = (e, v) => {
        this.setState(prevState => ({
            ...prevState,
            showItem2: false,
            subcategory:v
        }));
        e.preventDefault();
    };
    submitData = () =>{
        this.setState({
            isLoading:true
        })
        let from = this.state.from;
        let fromNames = this.state.fromNames;
        let category = this.state.category;
        let subcategory = this.state.subcategory;
        let message = this.state.message;
        let noTelp = this.state.phoneNumber.selectedValue + this.state.noTelp;
        this.props.createContactUs({
            from : from,
            fromNames : fromNames,
            attachments : [],
            category : category,
            subcategory:subcategory,
            message : message,
            noTelp: noTelp
        });
    };
    render(){
        const {
            handleItemPhoneNumberClick,
            handleClickPhoneNumberMenu,
            state: { phoneNumber, subCategoryOption },
            submitData
        } = this;
        const { isLoading, popupSuccess,showItem, showItem2, selectedValue,isLogin, from ,fromNames ,category , subcategory, message, noTelp } = this.state;

        return(
            <Fragment>

                <Formik
                    initialValues={{
                        fromNames: fromNames,
                        countryCode: +62,
                        from: from,
                        noTelp: noTelp,
                        nationality:"Indonesia",
                        category:category,
                        subcaegory:subcategory,
                        messages:message
                    }}
                    validationSchema={Yup.object().shape({
                        email: Yup.string()
                            .email()
                            .required()
                    })}
                >
                    {({ isSubmitting, handleChange, handleBlur, values }) => (
                        <Form>

                            <div className="left-form">
                                {!isLogin && <div className="disabled_form text-center" style={{paddingTop:200, textAlign:'center'}}>
                                    <h5>You need to Login First</h5>
                                    <Button type="button" primary goto='/login' > LOGIN </Button>

                                </div>}
                                <div className="form-col">
                                    <TracField
                                        id="displayName"
                                        name="displayName"
                                        type="text"
                                        labelName="Name"
                                        onChange={e => {
                                            this.setState({
                                                fromNames: e.currentTarget.value
                                            })
                                        }}
                                        onBlur={handleBlur}
                                        value={values.fromNames || fromNames}
                                    />
                                    <TracField
                                        id="email"
                                        type="text"
                                        name="email"
                                        labelName="Email Address"
                                        onChange={e => {
                                            this.setState({
                                                from: e.currentTarget.value
                                            })
                                        }}
                                        onBlur={handleBlur}
                                        value={values.from || from}
                                    />
                                    <div className="right-form1">
                                        <div className="form-col">
                                            <div className="input-group">
                                                <div className="input-group-col phone-number">
                                                    <TracField
                                                        id="phoneNumberCountryCode"
                                                        type="select"
                                                        name="phone_number_country_code"
                                                        labelName="Phone Number"
                                                        showItem={phoneNumber.showItem}
                                                        // onClick={
                                                        //     handleClickPhoneNumberMenu
                                                        // }
                                                        // onItemClick={
                                                        //     handleItemPhoneNumberClick
                                                        // }
                                                        options={optionPhoneNumber}
                                                        value={
                                                            phoneNumber.selectedValue
                                                        }
                                                        icon={phoneNumber.selectedIcon}
                                                        listHasIcon
                                                        displayIcon
                                                    />
                                                </div>
                                                <div className="input-group-col">
                                                    <TracField
                                                        id="phoneNumber"
                                                        name="phoneNumber"
                                                        onChange={e => {
                                                            this.setState({
                                                                noTelp: e.currentTarget.value
                                                            })
                                                        }}
                                                        type="text"
                                                        labelName="Phone Number"
                                                        onBlur={handleBlur}
                                                        value={values.noTelp || noTelp}
                                                    />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="left-form1">
                                    <div className="form-col">
                                    <TracField
                                            id="category"
                                            type="select"
                                            name="category"
                                            labelName="Category"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            showItem={showItem}
                                            onClick={this.handleClickSelectMenu}
                                            onItemClick={this.handleItemCategoryClick}
                                            value={values.category || category}
                                        />
                                    </div>
                                </div>
                                <div className="right-form1">
                                    <div className="form-col">
                                        <div className="input-group">
                                            <div className="right-form2">
                                                <div className="form-col">
                                                    <TracField
                                                        id="subCategory"
                                                        type="select"
                                                        name="subCategory"
                                                        labelName="Sub Category"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        showItem={showItem2}
                                                        onClick={this.handleClickSelectMenu2}
                                                        onItemClick={this.handleItemSubCategoryClick}
                                                        value={values.subcategory || subcategory}
                                                        options={subCategoryOption}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-col">
                                    <TracField
                                        id="messages"
                                        type="text-area"
                                        name="messages"
                                        labelName="Message"
                                        onChange={e => {
                                            this.setState({
                                                message: e.currentTarget.value
                                            })
                                        }}
                                        onBlur={handleBlur}
                                        value={values.messages || message}
                                    />
                                </div>
                                { isLoading && <Button type="button" primary  >
                                    <PulseLoader
                                        sizeUnit={"px"}
                                        size={7}
                                        color={'#ffffff'}
                                        loading={isLoading}
                                    />
                                </Button> }
                                {!isLoading && <Button type="button" primary onClick={submitData}
                                                       disabled={message === '' || subcategory === '' || category === '' || noTelp === '' || fromNames === '' || from === ''}>Submit</Button>
                                }
                            </div>
                            <div className="right-form">
                            <div className="form-col">
                                <div className="phone-customer">
                                    <div className="phone-icon">
                                        <h4>Customer Assistance Center:</h4>
                                        <p>1500-009</p>
                                    </div>
                                    <div className="contact-call">
                                        <h4>Reservation Call Center:</h4>
                                        <p>(021) 877 877 87</p>
                                    </div>
                                </div>                    
                                <div className="contact-mail">
                                    <div className="phone-mail">
                                        <h4>Email Customer Care</h4>
                                        <p>rco.nasional@trac.astra.co.id</p>
                                    </div>
                                    <div className="mail-contact">
                                        <a className="a-btn btn-text" href="mailto:rco.nasional@trac.astra.co.id?Subject=Reservasi" target="_top">EMAIL US</a>
                                    </div>
                                </div>
                                <div className="locate">
                                    <div className="locate-pin">
                                        <h4>Locate Us</h4>
                                        <a className="a-btn btn-text" href="./outlet-locations">FIND NEAREST LOCATION</a> 
                                    </div>         
                                </div>
                            </div>
                        </div>
                        </Form>
                    )}
                </Formik>
                <ModalSuccess
                    showModal={popupSuccess}
                    onClose={()=>this.setState({
                        popupSuccess:false
                    }, window.location.reload())}
                />
            </Fragment>
        )
    }
}

const mapStateToProps = ({ userprofile, login }) => {
    const { createContactUsSuccess, createContactUsFailure } = userprofile;
    const { isLogin, localUser } = login;
    return { isLogin,localUser, createContactUsSuccess, createContactUsFailure };
}

export default withRouter(
    connect(mapStateToProps, {
        checkIsLogin,
        createContactUs
    })(FormContact)
);
