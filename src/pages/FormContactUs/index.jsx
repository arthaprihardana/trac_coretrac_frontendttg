import React, { Component, Fragment } from 'react';
import { Main, Static } from 'templates';
import './FormContactUs.scss';

//thisComponent
import FormContact from './thisComponent/FormContact';

//assets
import Contactbanner from 'assets/images/trac/ContactUs.png';

class FormContactUs extends Component{
    render(){
        return(
            <Fragment>
                <Main className="contactus-form">
                    <Static title="Contact Us" bannerImg={Contactbanner} subTitle="We want to hear from you! Your input is valuable and will allow us to better serve your needs in the future"> 
                        <FormContact />
                    </Static>
                </Main>
            </Fragment>
        )
    }
}

export default FormContactUs;