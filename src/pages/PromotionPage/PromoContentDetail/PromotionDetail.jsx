import React, { Component, Fragment } from 'react';
import { Main, Static } from 'templates';
import { Button } from 'atom';
import { ListBlog} from 'molecules';
import ContentBanner from '../PromoBanner';
import promo_all from '../../../assets/data-master/promo'

//asset
import BannerPromo from 'assets/images/dummy/mobil-promo1.png';
import Promo1 from 'assets/images/dummy/promo1.png';
import Promo2 from 'assets/images/dummy/promo2.png';
import Promo3 from 'assets/images/dummy/promo3.png';

import promo1banner from "../../../assets/images/trac/promo/promo-1-big-banner.jpg";
import promo1 from "../../../assets/images/trac/promo/promo-1.jpg";

import { getListPromo } from "actions";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";

class PromotionDetail extends Component{
    state={

        listPromoAll:[],
        indexId:parseInt(this.props.match.params.id),
        breadCrumb: [
            {
                label: 'Home',
                link: '/'
            },
            {
                label: 'Promotion',
                link: '/promo'
            },
            {
                label: 'car Rental',
                link: '/'
            },
        ],
        other1:[
            {
                id: 1,
                image:promo1,
                label:"promo",
                title:"Early Bird Ayo Mudik dapatkan E-Money Gratis 1.200K",
                lead:"",
                date:"",
                linkTo:"/promo-detail",
                linkText:"Read More",
                period:"Periode Promo",
                labelDate:"1 April - 30 Mei 2019",
                btnDetail:"See Detail",
                labelGrey:"label-date"
            }
        ]
    };
    createMarkup = (html) => {
        let tmp = JSON.parse(JSON.stringify(html));
        return {__html: tmp};
    };
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.match.params.id !== prevProps.match.params.id){
            return { newParam: this.props.match.params.id }
        }
        if(this.props.listPromoSuccess !== prevProps.listPromoSuccess) {
            return { promoSuccess: this.props.listPromoSuccess }
        }
        return null;
    };
    componentDidMount () {
        this.props.getListPromo();
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': 0
        });
    }
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (snapshot !== null) {
            if (snapshot.promoSuccess !== undefined) {
                this.setState({
                    listPromoAll:snapshot.promoSuccess
                })
            }
            if (snapshot.newParam !== undefined) {
                window.scrollTo({
                    'behavior': 'smooth',
                    'left': 0,
                    'top': 0
                });
                this.setState({
                    id: snapshot.newParam
                });
                if (this.state.listPromoAll.length > 0) {
                    let tmp = this.state.listPromoAll.findIndex(x => x.Id == snapshot.newParam);
                    this.setState({
                        indexId: parseInt(tmp + 1)
                    })

                }
            }
        }
    };

    copyText= () =>{
        // let copyText = document.getElementById("myInput");
        // copyText.select();
        //
        // document.execCommand("copy");
    };
    render(){

        const { listPromoAll,indexId } = this.state;
        return(
            <Fragment>
                <Main className="detail-promosi">
                    {
                        (listPromoAll.length > 0 && indexId !== null) && (
                            <Static contentBanner={<ContentBanner />} bannerImg={listPromoAll[indexId-1].ImageBanner}
                                    breadCrumb={this.state.breadCrumb}>
                                <div className="promo-wrapper" style={{    marginTop: 50}}>
                                    <div className="left-promo">
                                        <div className="detail-content">
                                            <div className="content_article"
                                                 dangerouslySetInnerHTML={this.createMarkup(listPromoAll[indexId-1].Content)}
                                                 style={{textAlign: 'justify'}}/>
                                        </div>
                                    </div>
                                    <div className="right-promo">
                                        <div className="copy-promo">
                                            <div className="teks-copy promo">
                                                <p className="promo-period">Periode Promo<br/><span> - </span></p>
                                                <p>Kode Kupon</p>
                                                <input id="myInput" type="text" className="promo-code" value="" readOnly/>
                                                <p className="text-coupon">Masukan Kode promo ini di halaman Order</p>
                                                <Button type="button" primary onClick={this.copyText()}>
                                                    Copy Promo
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="promo-line detail container"></div>
                                <h2>Promo Kami</h2>
                                <div className="promo-section">
                                    {
                                        this.state.listPromoAll.map(other => {
                                            return (
                                                <ListBlog
                                                    key={other.Id}
                                                    image={other.ImageThumbnail}
                                                    label=""
                                                    title={other.Title}
                                                    lead=""
                                                    date=""
                                                    linkTo={'/promo-detail/'+other.Id}
                                                    linkText={'/promo-detail/'+other.Id}
                                                    period=''
                                                    labelDate=''
                                                    btnDetail=''
                                                    labelGrey=''
                                                />
                                            )
                                        })
                                    }
                                </div>


                            </Static>
                        )
                    }
                </Main>
            </Fragment>
        )
    }
}




const mapStateToProps = ({  product}) => {
    const { listPromoSuccess } = product;
    return {listPromoSuccess }
}

export default withRouter(
    connect(mapStateToProps, {
        getListPromo
    })(PromotionDetail)
);
