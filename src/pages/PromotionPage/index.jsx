import React, { Component, Fragment } from 'react';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import PropTypes from "prop-types";
import uuid from "uuid";
import { Main } from "templates";
import { isObject, isString } from "lodash";
import { Button } from 'atom';
import Slider from "react-slick";
import { Cover } from "atom";
import './PromotionPage.scss';
import { ListBlog} from 'molecules';
import promo_all from '../../assets/data-master/promo'
import promo1 from "../../assets/images/trac/promo/promo-1.jpg";
import promo1banner from "../../assets/images/trac/promo/promo-1-big-banner.jpg";


import { getListPromo } from "actions";
//assetse
import Slider1 from 'assets/images/dummy/ban-promo.png';
import Promo1 from 'assets/images/dummy/promo1.png';
import Promo2 from 'assets/images/dummy/promo2.png';
import Promo3 from 'assets/images/dummy/promo3.png';
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";

class PromotionPage extends Component{
    state= {
        listPromo:[],
        promo_all:promo_all,
        tapActive: [
            {

            },
        ],
        menuPromo: [
            {
                id: 1,
                name: 'All Promo',
                isActive: false,
                className:"active promo"
                
            },
            // {
            //     id: 2,
            //     name: 'Car Rental',
            //     isActive: false,
            //     className:"car-promo"
            // },
            // {
            //     id: 3,
            //     name: 'Airport Transport',
            //     isActive: false,
            //     className:"airport-promo"
            // },
            // {
            //     id: 4,
            //     name: 'Bus Rental',
            //     isActive: false,
            //     className:"bus-promo"
            // },
        ],
        info1:[
            {
                id: 1,
                image:promo1,
                label:"promo",
                title:"Early Bird Ayo Mudik dapatkan E-Money Gratis 1.200K",
                lead:"",
                date:"",
                linkTo:"/promo-detail",
                linkText:"Read More",
                period:"Periode Promo",
                labelDate:"1 April - 30 Mei 2019",
                btnDetail:"See Detail",
                labelGrey:"label-date"
            }
        ]

    }
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.listPromoSuccess !== prevProps.listPromoSuccess) {
            return { promoSuccess: this.props.listPromoSuccess }
        }
        return null;
    };
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (snapshot !== null) {
            if (snapshot.promoSuccess !== undefined) {
                this.setState({
                    listPromo:snapshot.promoSuccess.reverse()
                })
            }
        }
    };
    componentDidMount() {
        this.props.getListPromo();
    }

    promoClick = (e, id) => {
        console.log(id);
    }

    render() {
        const { config, images } = this.props;
        const { listPromo } = this.state;
        return (
            <Fragment>
                <Main>
                    <div className="title wraper">
                        <div className="t-main t-auth">
                            <div className="left">
                                <div className="m-slideshow">
                                    <Slider {...config}>
                                        {images.map(image => (
                                            <div key={uuid()} className="slide-item">
                                                <Cover image={image.src} />
                                                <div className="slide-text ">
                                                    <div className="container">
                                                        {isObject(image.text) && (
                                                            <React.Fragment>
                                                                <h3 
                                                                    dangerouslySetInnerHTML={{
                                                                    __html: image.text.title
                                                                    }}
                                                                />
                                                                <p className="persentase-text">{image.text.label}</p>
                                                                <p>{image.text.period}</p>
                                                                <p>{image.text.Button}</p>
                                                            </React.Fragment>
                                                        )}
                                                        {isString(image.text) && <p>{image.text}</p>}
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                    </Slider>
                                </div>
                            </div>
                        </div>
                        <ul className="promo-icon">
                            {
                                this.state.menuPromo.map( menu => {
                                    return (<li key={menu.id} className={menu.className}>
                                        <a onClick={e => this.promoClick(e, menu.id)}>{menu.name}</a>
                                    </li>)
                                })
                            }
                        </ul>
                    </div>
                    <div className="promo-line container">
                        <div className="promo-section">
                            {
                                this.state.listPromo.map( other => {
                                    return(

                                        <ListBlog
                                            key={other.Id}
                                            image={other.ImageThumbnail}
                                            label=""
                                            title={other.Title}
                                            lead=""
                                            date=""
                                            linkTo={'/promo-detail/'+other.Id}
                                            linkText={'/promo-detail/'+other.Id}
                                            period=''
                                            labelDate=''
                                            btnDetail=''
                                            labelGrey=''
                                        />
                                    )
                                })
                            }
                        </div>
                        <div className="section-btn">
                        {/*<Button type="button" outline>*/}
                        {/*Load More*/}
                        {/*</Button>*/}
                        </div>
                    </div>

                </Main>
            </Fragment>
        )
    }
}

PromotionPage.defaultProps = {
    config: {
        dots: true,
        lazyLoad: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 7000,
        arrows: false,
        speed: 350,
        slidesToScroll: 1,
        initialSlide: 0,
        fade: true,
        cssEase: "ease"
    },
    images: [
        {
            text: {
                title:"Early Bird <br/> Ayo Mudik",
                label:"1.200K",
                period:"Periode Promo 1 April - 30 Mei 2019",
                Button:
                <Button type="button" goto="/" primary>
                  Book Now
                </Button>
            },
            src: promo1banner
        }
        ]
};

PromotionPage.propTypes = {
    config: PropTypes.object, // eslint-disable-line
    images: PropTypes.arrayOf(
        PropTypes.shape({
            text: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
            path: PropTypes.string,
            src: PropTypes.string
        })
    )
};


const mapStateToProps = ({  product}) => {
    const { listPromoSuccess } = product;
    return {listPromoSuccess }
}

export default withRouter(
    connect(mapStateToProps, {
        getListPromo
    })(PromotionPage)
);
