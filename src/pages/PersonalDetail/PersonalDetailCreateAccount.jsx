/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-09 01:15:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-21 14:52:39
 */
// libraries
import React, { PureComponent } from "react";
import { connect } from "react-redux";

// actions
import { postRegister, deletePrevRegister } from "actions";

// assets & styles
import ThisPersonalDetailWrapper from "./ThisPersonalDetailWrapper";
import ThisPersonalDetailCreateContent from "./ThisPersonalDetailCreateContent";

class PersonalDetailCreateAccount extends PureComponent {

    state = {
        isModalOpen: false,
        receiveResponse: {}
    }

    handleSubmit = values => {
        this.props.postRegister({ FormData: values });
    }
    
    handleModalClose = () => {
        this.setState({
            isModalOpen: false
        })
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.registerSucces !== prevProps.registerSucces) {
            return { success: this.props.registerSucces }
        }
        if(this.props.registerError !== prevProps.registerError) {
            return { failure: this.props.registerError }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.success !== undefined) {
                this.setState({ 
                    isModalOpen: true,
                    receiveResponse: {
                        user_login: snapshot.success.user_login,
                        ErrorMessage: null
                    }
                });
            }
            if(snapshot.failure !== undefined) {
                this.setState({
                    isModalOpen: true,
                    receiveResponse: {
                        user_login: null,
                        ErrorMessage: snapshot.failure
                    }
                }, () => this.props.deletePrevRegister());
            }
        }
    }

    render() {
        const {
            props: {
                match: {
                    params: {
                        id
                    }
                },
                loading
            },
            state: {
                isModalOpen,
                receiveResponse
            },
            handleSubmit,
            handleModalClose
        } = this;
        return (
            <ThisPersonalDetailWrapper type={id}>
                <ThisPersonalDetailCreateContent onSubmit={handleSubmit} isLoading={loading} isModalOpen={isModalOpen} handleModalClose={handleModalClose} onReceiveResponse={receiveResponse} type={id} />
            </ThisPersonalDetailWrapper>
        );
    }
}


const mapStateToProps = ({ register }) => {
    const { loading, registerSucces, registerError } = register;
    return { loading, registerSucces, registerError };
}

export default connect(mapStateToProps, {
    postRegister,
    deletePrevRegister
})(PersonalDetailCreateAccount);
