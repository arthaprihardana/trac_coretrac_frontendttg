/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 20:12:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-18 14:28:41
 */
import { delay } from 'helpers';
import React, { Component } from 'react';
import FormData from "./thisComponent/FormData";
import { withRouter } from 'react-router-dom';

class ThisPersonalDetailContent extends Component {
    
    handleSubmitForm = () => {
        const { 
            history: { 
                push 
            },
            match: {
                params: {
                    id
                }
            }
        } = this.props;
        delay(1000).then(() => push(`/payment`));
    }

    render() {
        const {
            props: {
                type_service
            },
            handleSubmitForm
        } = this;
        return (
            <div className="left-content">
                <div className="personal-detail personal-account create-account">
                    <div className="title">
                        <h3>Personal Detail</h3>
                    </div>
                    <div className="form-wrapper">
                        <FormData onSubmit={handleSubmitForm} type_service={type_service} />
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(ThisPersonalDetailContent);