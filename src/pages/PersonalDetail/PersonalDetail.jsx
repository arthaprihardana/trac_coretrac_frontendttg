import React, { PureComponent } from "react";
import { delay } from 'helpers';

// components
import { Main } from "templates";
import { withRouter } from 'react-router-dom';
import { Sidebar, SidebarBookingDetail, SidebarNote, TotalPaymentMobile } from "organisms";
import {
    OrderSteps,
    SidebarCarOrder,
    SidebarPaymentDetail
} from "molecules";
import FormData from "./thisComponent/FormData";

// style
import "./PersonalDetail.scss";

//dummy data
import selectedCarJSON from "assets/data-dummy/selected-car.json";
import bookingDetailJSON from "assets/data-dummy/booking.json";
import { CustomerGlobal } from "context/RentContext";

const selectedCarDummy = selectedCarJSON.map(car => {
    // car.img = require(`../../assets/images/dummy/cars/${car.img}`);
    return car;
});

class PersonalDetail extends PureComponent {
    state = {
        showNoteDetail: false,
        email: "",
        selectedCar: selectedCarDummy,
        bookingDetail: bookingDetailJSON,
        showDetail: false,
    };

    componentDidMount() {
        document.body.classList.add("add-padding-top");
        document.body.classList.add("background-grey");
    }

    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
        document.body.classList.remove("background-grey");
    }

    // note detail
    handleNoteDetail = () => {
        this.setState({ showNoteDetail: !this.state.showNoteDetail });
    };

    handleShowPaymentDetail = (status) => {
        this.setState({ showDetail: status});
    }

    handleSubmitForm = (value) => {
        const { history: { push } } = this.props;
        delay(1000).then(() => push('/payment'));
    }

    render() {
        const { 
            handleShowPaymentDetail, 
            state: {
                selectedCar, 
                bookingDetail, 
                showDetail
            },
            props: {
                rentContext: {
                    state: {
                        values: {
                            type_service
                        }
                    }
                }
            }
        } = this;
        // order step
        let orderStepsData = [
            { step: "Car Order", className: "done" },
            { step: "Personal Detail", className: "current" },
            { step: "Payment", className: "" }
        ];

        if(type_service === '2'){
            orderStepsData = [
                { step: "Car Order", className: "done" },
                { step: "Personal Detail", className: "current" },
                { step: "Driver Information", className: "" },
                { step: "Payment", className: "" }
            ];
        }

        return (
            <Main solid headerUserLogin headerClose>
                {/* order steps */}
                <OrderSteps data={orderStepsData} />

                {/* Order Detail */}
                <div className="order-detail">
                    <div className="container">
                        <div className="order-detail-container">
                            {/* Left Content */}
                            <div className="left-content">
                                <div className="personal-detail personal-account create-account">
                                    <div className="title">
                                        <h3>Personal Detail</h3>
                                    </div>
                                    <div className="form-wrapper">
                                        <FormData onSubmit={this.handleSubmitForm} type_service={type_service} />
                                    </div>
                                </div>
                            </div>

                            {/* Sidebar */}
                            <Sidebar showDetail={ showDetail } returnShowDetail = { handleShowPaymentDetail }>
                                {/* Car Order */}
                                <SidebarCarOrder data={selectedCar} />

                                {/* Booking Detail */}
                                <SidebarBookingDetail data={bookingDetail} />

                                {/* Note/Request */}
                                <SidebarNote hasContent />

                                {/* Payment Detail */}
                                <SidebarPaymentDetail data={selectedCar} />
                            </Sidebar>
                        </div>
                    </div>
                    {/* TotalPaymentMobile */}
                    <TotalPaymentMobile showDetail={ showDetail } returnShowDetail = { handleShowPaymentDetail }/>
                </div>
            </Main>
        );
    }
}
export default CustomerGlobal(withRouter(PersonalDetail));