/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-01 10:49:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-22 10:07:11
 */
import React, { PureComponent } from "react";
import { Button, TracField } from "atom";
import { Accordion } from "organisms";
import { connect } from 'react-redux';
import Dropzone from "react-dropzone";
import * as Yup from "yup";
import image2base64 from "image-to-base64";
import _ from "lodash";

import lang from '../../../assets/data-master/language'

// actions
import { passangerCarRental } from "actions";

// flags
import flagId from "assets/images/flags/id.svg";
// import flagEn from "assets/images/flags/en.svg";

const FormSchema = Yup.object().shape({
    Passengers: Yup.array().of(Yup.object().shape(
        {
            IDCardNumber: Yup.number().integer()
                .min("6", "Should be 6 characters!")
                .required("Password is required!")
        }
        )
    )
});

const optionPhoneNumber = [
    {
        id: "id",
        value: "+62",
        text: "+(62) Indonesia",
        icon: flagId,
    },
    // {
    //     id: "us",
    //     value: "+1",
    //     text: "(+1) United State",
    //     icon: flagEn,
    // }
];

class FormBusRental extends PureComponent {
    state = {
        phoneNumberFlag: {
            showItem: false,
            selectedValue: "+62",
            selectedIcon: flagId,
        },
        ImageKTP: [],
        ImageKTPbase64: '',
        ImageSIM: [],
        ImageSIMBase64: '',
        IsForeigner: false,
        imDriver: false
    }

    handleClickPhoneNumberMenu = (e) => {
        this.setState(prevState => ({
            ...prevState,
            phoneNumberFlag: {
                showItem: !prevState.phoneNumberFlag.showItem
            }
        }));
        e.preventDefault();
    }

    handleItemPhoneNumberClick = (e, v) => {
        const icon = e.currentTarget.querySelector('img').getAttribute("src");
        this.setState(prevState => ({
            ...prevState,
            phoneNumberFlag: {
                showItem: false,
                selectedValue: v,
                selectedIcon: icon,
            }
        }));
        e.preventDefault();
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(this.state.ImageKTP !== prevState.ImageKTP) {
            if(this.state.ImageKTP.length > 0) {
                let r = /^(blob)/g;
                if(r.test(this.state.ImageKTP[0].preview)) {
                    var file = this.state.ImageKTP[0];
                    const reader = new FileReader();
                    reader.onload = (event) => {
                        this.setState({
                            ImageKTPbase64: event.target.result
                        }, () => this.props.values.Passengers[this.props.index].ImageKTP = this.state.ImageKTPbase64 )
                    };
                    reader.readAsDataURL(file);
                } else {
                    let self = this;
                    image2base64(this.state.ImageKTP[0].preview).then(response => {
                        self.setState({
                            ImageKTPbase64: `data:image/png;base64,${response}`
                        }, () => this.props.values.Passengers[this.props.index].ImageKTP = this.state.ImageKTPbase64 );
                    })
                }
            }
        }
        if(this.state.ImageSIM !== prevState.ImageSIM) {
            if(this.state.ImageSIM.length > 0) {
                var file = this.state.ImageSIM[0]
                const reader = new FileReader();
                reader.onload = (event) => {
                    this.setState({
                        ImageSIMBase64: event.target.result
                    }, () => this.props.values.Passengers[this.props.index].ImageSIM = this.state.ImageSIMBase64)
                };
                reader.readAsDataURL(file);
            }
        }
    }
    

    render() {
        const {
            props: {
                handleClickPhoneNumberMenu, handleItemPhoneNumberClick, title, index,
                handleChange, handleBlur, values
            },
            state: {
                phoneNumberFlag
            }
        } = this;

        return (
            <Accordion defaultValue={true} title={title} driverInfo>
                <p className="desc-info">{lang.pleaseScanUploadKTPCoordinator[lang.default]}</p>
                <div className="form-row">
                    <div className="form-col initial">
                        <TracField
                            id={`Passengers[${index}].IsForeigner`}
                            name={`Passengers[${index}].IsForeigner`}
                            type="checkbox"
                            labelName={lang.indeonsiaCitizen[lang.default]}
                            labelSize="small"
                            onChange={ () => {
                                this.setState({ 
                                    IsForeigner: !this.state.IsForeigner 
                                }, () => values.Passengers[index].IsForeigner = this.state.IsForeigner )
                            }}
                            checked={!this.state.IsForeigner}
                        />
                    </div>
                    <div className="form-col initial">
                        <TracField
                            id={`Passengers[${index}].IsForeigner`}
                            name={`Passengers[${index}].IsForeigner`}
                            type="checkbox"
                            labelName={lang.foreignCitizen[lang.default]}
                            labelSize="small"
                            onChange={ () => {
                                this.setState({ 
                                    IsForeigner: !this.state.IsForeigner 
                                }, () => values.Passengers[index].IsForeigner = this.state.IsForeigner )
                            }}
                            checked={this.state.IsForeigner}
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col initial">
                        <Dropzone
                            accept="image/*"
                            onDrop={files => this.setState({
                                ImageKTP: files.map(file => Object.assign(file, {
                                    preview: URL.createObjectURL(file)
                                }))
                            })}
                            onFileDialogCancel={() => this.setState({ ImageKTP: [] })}
                            >
                            {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, acceptedFiles, rejectedFiles }) => (
                                <div {...getRootProps()} className="m-id-card-photo">
                                    <div className="upload-area">
                                        { this.state.ImageKTP.length > 0 ? this.state.ImageKTP.map(file => (
                                            <div key={file.name} className="preview-zone">
                                                <img alt={file.name} src={file.preview} className="preview-image" />
                                            </div>
                                            ))  : 
                                            <div className="drag-zone">
                                                <div className="icon" />
                                                <div className="description">
                                                    {lang.youCanDragAndDrop[lang.default]}
                                                </div>
                                            </div>
                                        }
                                        <Button type="button" secondary>{lang.photoKTP[lang.default]}</Button>
                                        <input {...getInputProps()} className="input-file" />
                                    </div>
                                </div>
                            )}
                        </Dropzone>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col">
                        <TracField
                            id={`Passengers[${index}].IDCardNumber`}
                            name={`Passengers[${index}].IDCardNumber`}
                            type="number"
                            labelName={lang.ktpNumber[lang.default]}
                            onChange={handleChange}
                            value={ values.Passengers[index].IDCardNumber }
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col">
                        <TracField
                            id={`Passengers[${index}].Name`}
                            name={`Passengers[${index}].Name`}
                            type="text"
                            labelName={lang.fullName[lang.default]}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={ values.Passengers[index].Name }
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col">
                        <TracField
                            id={`Passengers[${index}].Address`}
                            name={`Passengers[${index}].Address`}
                            type="text"
                            labelName="Residential Address"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={ values.Passengers[index].Address }
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col">
                        <div className="input-group">
                            <div className="input-group-col phone-number">
                                <TracField
                                    id="phoneNumberPassengerCountryCode"
                                    type="select"
                                    name="phone_numberPassenger_country_code"
                                    labelName={lang.phoneNumber[lang.default]}
                                    showItem={phoneNumberFlag.showItem}
                                    onClick={handleClickPhoneNumberMenu}
                                    onItemClick={handleItemPhoneNumberClick}
                                    options={optionPhoneNumber}
                                    value={phoneNumberFlag.selectedValue}
                                    icon={phoneNumberFlag.selectedIcon}
                                    listHasIcon
                                    displayIcon
                                />
                            </div>
                            <div className="input-group-col">
                                <TracField
                                    id={`Passengers[${index}].PhoneNumber`}
                                    name={`Passengers[${index}].PhoneNumber`}
                                    type="number"
                                    labelName={lang.phoneNumber[lang.default]}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={ values.Passengers[index].PhoneNumber }
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </Accordion>
        )
    }
}

const mapStateToProps = ({ carrental }) => {
    const { passanger } = carrental;
    return { passanger };
}
export default connect(mapStateToProps, {
    passangerCarRental
})(FormBusRental);