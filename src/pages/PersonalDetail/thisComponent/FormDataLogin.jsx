/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-01 00:12:01 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 16:15:09
 */
import React, { PureComponent, Fragment } from "react";
import { withRouter } from "react-router-dom";
//dummy data
import successImg from 'assets/images/background/login-success.svg';
import { Link } from "react-router-dom";
import * as Yup from "yup";
import { connect } from "react-redux";

// actions
import { postLogin, postLoginSosmed, deletePrevLogin } from "actions";

// services
import auth0 from 'services/Auth';

// components
import { Form, Formik } from "formik";
import { Button, Modal, TracField } from "atom";
import { PulseLoader } from 'react-spinners';

// db
import db from '../../../db';
import window from "window-or-global";
import {debounce} from "lodash";
import lang from '../../../assets/data-master/language'

// validation schema
const AuthSchema = Yup.object().shape({
    email: Yup.string()
        .email("Email should be valid!")
        .required(lang.loginEmptyEmail[lang.default]),
    password: Yup.string()
        .min("6", "Should be 6 characters!")
        .required(lang.loginEmptyPassword[lang.default])
});

class FormDataLogin extends PureComponent {
    constructor() {
        super();
        this.state = {
            icon: "password hide",
            type_pwd:'password',
            showModal: false,
            rememberMe: false,
            ErrorMessage: null
        };

        this.handleShowPassword = this.handleShowPassword.bind(this);
    }


    componentDidMount = () => {
        this.handleAuth0();
    }

    handleLogin = () => {
        this.setState({showModal: false}, () => {
            this.props.modalAction();
        })
    }

    handleAuth0 = async () => {
        const Auth0 = new auth0(this.props.match.params.id);
        let responseAuth = await Auth0.handleAuthentication();
        if(responseAuth !== null) {
            this.props.postLoginSosmed(responseAuth);
        }
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.loginSuccess !== prevProps.loginSuccess) {
            return { success: this.props.loginSuccess };
        }
        if(this.props.loginFailure !== prevProps.loginFailure) {
            if(this.props.loginFailure !== null) {
                return { failure: this.props.loginFailure }
            }
        }
        if(this.props.loginsosmedSuccess !== prevProps.loginsosmedSuccess) {
            return { sosmedsuccess: this.props.loginsosmedSuccess }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.success !== undefined) {
                this.setState({showModal: false}, () => {
                    this.props.modalAction();
                })
            }
            if(snapshot.failure !== undefined) {
                this.setState({
                    showModal: true,
                    ErrorMessage: lang.emailPasswordNotCorrect[lang.default]
                    // ErrorMessage: snapshot.failure
                });
            }
            if(snapshot.sosmedsuccess !== undefined) {
                const {
                    history: { push }
                } = this.props;
                this.props.modalAction();
            }
        }
    }

    handleModal = () => {
        this.setState({showModal: !this.state.showModal})
    }

    handleRememberMe = () => {
        this.setState({rememberMe: !this.state.rememberMe})
    }

    handleSubmit = (values, { setSubmitting }) => {
        this.props.postLogin({FormData: values});
        setSubmitting(false);
    }
    handleShowPassword(e) {
        console.log("click")
        e.preventDefault();
        const { type_pwd, icon } = this.state;
        if (type_pwd === "text" && icon === "password show")
            this.setState({ type_pwd: "password", icon: "password hide" });
        if (type_pwd === "password" && icon === "password hide")
            this.setState({ type_pwd: "text", icon: "password show" });
    }
    handleModalClose = () => {
        this.setState({
            showModal: false
        }, () => this.props.deletePrevLogin())
    }

    render(){
        const {
            state: {
                type_pwd,
                rememberMe,
                icon,
                showModal,
                ErrorMessage
            },
            props: {
                type
            },
            handleShowPassword,
            handleRememberMe
        } = this;

        return (
            <Fragment>
                <Formik
                    initialValues={{
                        email: "",
                        password: "",
                        rememberme: ""
                    }}
                    validationSchema={AuthSchema}
                    onSubmit={this.handleSubmit}>
                    {({ isSubmitting, handleChange, handleBlur, values }) => (
                        <Form>
                            <div className="form-row">
                                <div className="form-col">
                                    <TracField
                                        id="email"
                                        name="email"
                                        type="text"
                                        labelName="Email"
                                        validate={AuthSchema.email}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.email}
                                    />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-col">
                                    <TracField
                                        id="password"
                                        name="password"
                                        type={type_pwd}
                                        labelName="Password"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        onClick={handleShowPassword}
                                        value={values.password}
                                        icon={icon}
                                    />
                                    {/*<TracField*/}
                                        {/*id="password"*/}
                                        {/*name="password"*/}
                                        {/*type="password"*/}
                                        {/*labelName="Password"*/}
                                        {/*onChange={handleChange}*/}
                                        {/*onBlur={handleBlur}*/}
                                        {/*value={values.password}*/}
                                        {/*icon="password hide"*/}
                                    {/*/>*/}
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-remember-forgot">
                                    <div>
                                        <TracField
                                            id="rememberme"
                                            name="rememberme"
                                            type="checkbox"
                                            labelName={lang.rememberMe[lang.default]}
                                            labelSize="small"
                                            onChange={handleRememberMe}
                                            checked={rememberMe}
                                            value={values.aggreement}
                                        />
                                    </div>
                                    <div>
                                        <Link to="/forgot-password">
                                            {lang.forgotPassword[lang.default]}
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-action-col">
                                    <Button type="submit" primary>
                                        {this.props.loading ? 
                                        <PulseLoader
                                            sizeUnit={"px"}
                                            size={7}
                                            color={'#ffffff'}
                                            loading={this.props.loading}
                                        />
                                        : "Login" }
                                    </Button>
                                </div>
                                <Modal
                                    open={showModal}
                                    onClick={this.handleModalClose}
                                    header={{
                                        withCloseButton: true,
                                        onClick: this.handleModalClose,
                                        children: ''
                                    }}
                                    content={{
                                        children: (
                                            <div className="success-login-wrapper">
                                                <div className="img-success">
                                                    <img src={successImg} alt=""/>
                                                </div>
                                                <p className="main-title">{lang.loginFailed[lang.default]}</p>
                                                <p className="sub-title">{ErrorMessage}</p>
                                            </div>
                                        ),
                                    }}
                                    footer={{
                                        position: 'center',
                                        children: (
                                            <div className="modal-footer-action">
                                                <Button primary onClick={this.handleModalClose}>Close</Button>
                                            </div>
                                        ),
                                    }}
                                />
                                <div className="form-action-col">
                                    {lang.orContineuWith[lang.default]}
                                </div>
                                <div className="form-action-col">
                                    <div className="social-icon">
                                        <a
                                            className="google"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                            onClick={() => {
                                                const Auth0 = new auth0(type);
                                                Auth0.openGoogleauth();
                                            }}
                                        >
                                            google
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            </Fragment>
        )
    }
}

const mapStateToProps = ({ login }) => {
    return login;
}

export default withRouter(
    connect(mapStateToProps, {
        postLogin,
        postLoginSosmed,
        deletePrevLogin
    })(FormDataLogin)
);
