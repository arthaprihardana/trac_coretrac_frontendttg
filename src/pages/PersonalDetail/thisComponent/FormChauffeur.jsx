/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-01 11:24:53 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-19 17:00:52
 */
import React, { PureComponent } from "react";
import { Button, TracField } from "atom";
// import { Form, Formik } from "formik";
import * as Yup from "yup";
import { connect } from 'react-redux';
import { passangerCarRental } from "actions";
import Dropzone from "react-dropzone";
import image2base64 from "image-to-base64";

// flags
import flagId from "assets/images/flags/id.svg";
// import flagEn from "assets/images/flags/en.svg";

// validation schema
// const AuthSchema = Yup.object().shape({
//     email: Yup.string()
//         .email("Email should be valid!")
//         .required("Email is required!"),
//     password: Yup.string()
//         .min("6", "Should be 6 characters!")
//         .required("Password is required!")
// });

const optionPhoneNumber = [
    {
        id: "id",
        value: "+62",
        text: "+(62) Indonesia",
        icon: flagId,
    },
    // {
    //     id: "us",
    //     value: "+1",
    //     text: "(+1) United State",
    //     icon: flagEn,
    // }
];

class FormChauffeur extends PureComponent {

    state = {
        phoneNumberFlag: {
            showItem: false,
            selectedValue: "+62",
            selectedIcon: flagId
        },
        carResponsible: 'cars1',
        // carResponsible: false,
        Name: null,
        PhoneNumber: null,
        Email: null,
        IDCardNumber: null,
        NPWPNumber: null,
        LicenseNumber: null,
        PassportNumber: null,
        Address: null,
        IsPIC: true,
        ImageKTP: [],
        ImageKTPbase64: '',
        ImageSIM: [],
        ImageSIMBase64: '',
        IsForeigner: false,
        countPassanger: []
    };

    handleClickPhoneNumberMenu = (e) => {
        const { phoneNumberFlag } = this.state;
        this.setState(prevState => ({
            ...prevState,
            phoneNumberFlag: {
                ...phoneNumberFlag,
                showItem: !prevState.phoneNumberFlag.showItem
            }
        }));
        e.preventDefault();
    }

    handleItemPhoneNumberClick = (e, v) => {
        const icon = e.currentTarget.querySelector('img').getAttribute("src");
        this.setState(prevState => ({
            ...prevState,
            phoneNumber: {
                showItem: false,
                selectedValue: v,
                selectedIcon: icon,
            }
        }));
        e.preventDefault();
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(this.state.ImageKTP !== prevState.ImageKTP) {
            if(this.state.ImageKTP.length > 0) {
                var file = this.state.ImageKTP[0]
                const reader = new FileReader();
                reader.onload = (event) => {
                    this.setState({
                        ImageKTPbase64: event.target.result
                    }, () => this.props.values.Passengers[this.props.index].ImageKTP = this.state.ImageKTPbase64 )
                };
                reader.readAsDataURL(file);
            }
        }
    }
    
    render() {

        const {
            handleItemPhoneNumberClick,
            handleClickPhoneNumberMenu,
            state: { phoneNumberFlag, carResponsible },
            props: { selectedCar, values, handleChange, handleBlur, index }
        } = this;
        
        return (
            <div className="form-row">
                <div className="form-row"></div>
                <div className="form-passenger-detail-box">
                    <div className="title">
                        <h3>Passenger Detail</h3>
                        <p>Lorem ipsum dolor sit amet</p>
                    </div>
                    <div className="form-row">
                        <div className="form-col">
                            <Dropzone
                                accept="image/*"
                                onDrop={files => this.setState({
                                    ImageKTP: files.map(file => Object.assign(file, {
                                        preview: URL.createObjectURL(file)
                                    }))
                                })}
                                onFileDialogCancel={() => this.setState({ ImageKTP: [] })}
                                >
                                {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, acceptedFiles, rejectedFiles }) => (
                                    <div {...getRootProps()} className="m-id-card-photo">
                                        <div className="upload-area">
                                            { this.state.ImageKTP.length > 0 ? this.state.ImageKTP.map(file => (
                                                <div key={file.name} className="preview-zone">
                                                    <img alt={file.name} src={file.preview} className="preview-image" />
                                                </div>
                                                ))  : 
                                                <div className="drag-zone">
                                                    <div className="icon" />
                                                    <div className="description">
                                                        You can drag and drop images to upload
                                                    </div>
                                                </div>
                                            }
                                            <Button type="button" secondary>Photo KTP</Button>
                                            <input {...getInputProps()} className="input-file" />
                                        </div>
                                    </div>
                                )}
                            </Dropzone>
                        </div>
                    </div>
                    <div className="form-row valign-top">
                        <div className="form-col">
                            <TracField
                                id={`Passengers[${index}].Name`}
                                name={`Passengers[${index}].Name`}
                                type="text"
                                labelName="Full Name"
                                // onChange={handleChange}
                                onChange={e => {
                                    this.setState({
                                        Name: e.currentTarget.value
                                    }, () => values.Passengers[index].Name = this.state.Name )
                                }}
                                onBlur={handleBlur}
                                value={ this.state.Name }
                            />
                            <div className="input-note">
                                As on ID Card/Passport/driving
                                license
                            </div>
                        </div>
                        <div className="form-col">
                            <div className="input-group">
                                <div className="input-group-col phone-number">
                                    <TracField
                                        id="phoneNumberPassengerCountryCode"
                                        type="select"
                                        name="phone_numberPassenger_country_code"
                                        labelName="Phone Number"
                                        showItem={phoneNumberFlag.showItem}
                                        onClick={handleClickPhoneNumberMenu}
                                        onItemClick={handleItemPhoneNumberClick}
                                        options={optionPhoneNumber}
                                        value={phoneNumberFlag.selectedValue}
                                        icon={phoneNumberFlag.selectedIcon}
                                        listHasIcon
                                        displayIcon
                                    />
                                </div>
                                <div className="input-group-col">
                                    <TracField
                                        id={`Passengers[${index}].PhoneNumber`}
                                        name={`Passengers[${index}].PhoneNumber`}
                                        type="number"
                                        labelName="Phone Number"
                                        // onChange={handleChange}
                                        onChange={e => {
                                            this.setState({
                                                PhoneNumber: e.currentTarget.value
                                            }, () => values.Passengers[index].PhoneNumber = this.state.PhoneNumber )
                                        }}
                                        onBlur={handleBlur}
                                        value={ this.state.PhoneNumber }
                                    />
                                </div>
                            </div>
                            <div className="input-note">
                                The driver will contact this
                                number(s) to coordinate pick-up.
                            </div>
                        </div>
                    </div>
                    <div className="form-row valign-top">
                        <div className="form-col">
                            <div className="input-multiple-radio">
                                <h5 className="title-input">
                                Car responsible
                                </h5>
                                {selectedCar.map((v, k) => (
                                    <TracField
                                        key={k}
                                        id={`cars${k+1}[${index}]`}
                                        name={`cars${k+1}[${index}]`}
                                        type="radio"
                                        labelName={`${v.vehicleTypeDesc} (Car #${k+1})`}
                                        labelSize="medium"
                                        onChange={ e => {
                                            this.setState({
                                                carResponsible: e.target.id
                                            }, () => values.Passengers[index].CarResponsible = { vehicleTypeId: v.vehicleTypeId, isTransmissionManual: v.isTransmissionManual } )
                                        }}
                                        checked={carResponsible === `cars${k+1}[${index}]` ? true : false}
                                    />
                                ))}
                            </div>
                        </div>
                        <div className="form-col">
                            <TracField
                                id={`Passengers[${index}].Email`}
                                name={`Passengers[${index}].Email`}
                                type="text"
                                labelName="Email"
                                // onChange={handleChange}
                                onChange={e => {
                                    this.setState({
                                        Email: e.currentTarget.value
                                    }, () => values.Passengers[index].Email = this.state.Email )
                                }}
                                onBlur={handleBlur}
                                value={ this.state.Email }
                            />
                            <div className="input-note">
                                Optional
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )

        // return (
        //     <Fragment>
        //         {/* <div className="form-row"></div> */}
        //         {/* <div className={`form-passenger-detail ${bookingForSomeone ? "active" : "else"}`}>
        //             { values.Passengers.map(( v, i ) => ( */}
        //                 <div className="form-row">
        //                     <div className="form-passenger-detail-box">
        //                         <div className="title">
        //                             <h3>Passenger Detail</h3>
        //                             <p>Lorem ipsum dolor sit amet</p>
        //                         </div>
        //                         <div className="form-row valign-top">
        //                             <div className="form-col">
        //                                 <TracField
        //                                     id={`Passengers[${0}].Name`}
        //                                     name={`Passengers[${0}].Name`}
        //                                     type="text"
        //                                     labelName="Full Name"
        //                                     onChange={handleChange}
        //                                     onBlur={handleBlur}
        //                                     value={ values.Passengers[0].Name }
        //                                 />
        //                                 <div className="input-note">
        //                                     As on ID Card/Passport/driving
        //                                     license
        //                                 </div>
        //                             </div>
        //                             <div className="form-col">
        //                                 <div className="input-group">
        //                                     <div className="input-group-col phone-number">
        //                                         <TracField
        //                                             id="phoneNumberPassengerCountryCode"
        //                                             type="select"
        //                                             name="phone_numberPassenger_country_code"
        //                                             labelName="Phone Number"
        //                                             showItem={phoneNumber.showItem}
        //                                             onClick={handleClickPhoneNumberMenu}
        //                                             onItemClick={handleItemPhoneNumberClick}
        //                                             options={optionPhoneNumber}
        //                                             value={phoneNumber.selectedValue}
        //                                             icon={phoneNumber.selectedIcon}
        //                                             listHasIcon
        //                                             displayIcon
        //                                         />
        //                                     </div>
        //                                     <div className="input-group-col">
        //                                         <TracField
        //                                             id={`Passengers[${0}].PhoneNumber`}
        //                                             name={`Passengers[${0}].PhoneNumber`}
        //                                             type="text"
        //                                             labelName="Phone Number"
        //                                             onChange={handleChange}
        //                                             onBlur={handleBlur}
        //                                             value={ values.Passengers[0].PhoneNumber }
        //                                         />
        //                                     </div>
        //                                 </div>
        //                                 <div className="input-note">
        //                                     The driver will contact this
        //                                     number(s) to coordinate pick-up.
        //                                 </div>
        //                             </div>
        //                         </div>
        //                         <div className="form-row valign-top">
        //                             <div className="form-col">
        //                                 <div className="input-multiple-radio">
        //                                     <h5 className="title-input">
        //                                     Car responsible
        //                                     </h5>
        //                                     {selectedCar.map((v, k) => (
        //                                         <TracField
        //                                             key={k}
        //                                             id={`cars${k+1}`}
        //                                             name={`cars${k+1}`}
        //                                             type="radio"
        //                                             labelName={`${v.vehicleTypeDesc} (Car #${k+1})`}
        //                                             labelSize="medium"
        //                                             onChange={(e)=>this.setState({carResponsible: e.target.id})}
        //                                             checked={carResponsible === `cars${k+1}` ? true : false}
        //                                         />
        //                                     ))}
        //                                 </div>
        //                             </div>
        //                             <div className="form-col">
        //                                 <TracField
        //                                     id={`Passengers[${0}].Email`}
        //                                     name={`Passengers[${0}].Email`}
        //                                     type="text"
        //                                     labelName="Email"
        //                                     onChange={handleChange}
        //                                     onBlur={handleBlur}
        //                                     value={values.Passengers[0].Email}
        //                                 />
        //                                 <div className="input-note">
        //                                     Optional
        //                                 </div>
        //                             </div>
        //                         </div>
        //                     </div>
        //                     {/* <div className="add-other-pasenger" onClick={() => arrayHelpers.insert(i, schema)}>
        //                         <span className="icon" />
        //                         Add Passenger
        //                     </div> */}
        //                 </div>
        //             {/* )) }
        //         </div> */}
        //     </Fragment>
        // )
    }
}

const mapStateToProps = ({ carrental}) => {
    const { passanger } = carrental;
    return { passanger }
}

export default connect(mapStateToProps, {
    passangerCarRental
})(FormChauffeur);
