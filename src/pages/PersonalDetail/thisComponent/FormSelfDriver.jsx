/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-01 10:49:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 10:46:32
 */
import React, { PureComponent } from "react";
import { Button, TracField } from "atom";
import { Accordion } from "organisms";
import { connect } from 'react-redux';
import Dropzone from "react-dropzone";
import image2base64 from "image-to-base64";
import _ from "lodash";

// actions
import { passangerCarRental } from "actions";
import lang from '../../../assets/data-master/language'
// flags
import flagId from "assets/images/flags/id.svg";
// import flagEn from "assets/images/flags/en.svg";

const optionPhoneNumber = [
    {
        id: "id",
        value: "+62",
        text: "+(62) Indonesia",
        icon: flagId,
    },
    // {
    //     id: "us",
    //     value: "+1",
    //     text: "(+1) United State",
    //     icon: flagEn,
    // }
];

class FormSelfDriver extends PureComponent {
    state = {
        phoneNumberFlag: {
            showItem: false,
            selectedValue: "+62",
            selectedIcon: flagId,
        },
        ImageKTP: [],
        ImageKTPbase64: '',
        ImageSIM: [],
        ImageSIMBase64: '',
        IsForeigner: false,
        imDriver: false
    }

    handleClickPhoneNumberMenu = (e) => {
        this.setState(prevState => ({
            ...prevState,
            phoneNumberFlag: {
                showItem: !prevState.phoneNumberFlag.showItem
            }
        }));
        e.preventDefault();
    }
    handleItemPhoneNumberClick = (e, v) => {
        const icon = e.currentTarget.querySelector('img').getAttribute("src");
        this.setState(prevState => ({
            ...prevState,
            phoneNumberFlag: {
                showItem: false,
                selectedValue: v,
                selectedIcon: icon,
            }
        }));
        e.preventDefault();
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(this.state.ImageKTP !== prevState.ImageKTP) {
            if(this.state.ImageKTP.length > 0) {
                let r = /^(blob)/g;
                if(r.test(this.state.ImageKTP[0].preview)) {
                    var file = this.state.ImageKTP[0];
                    const reader = new FileReader();
                    reader.onload = (event) => {
                        this.setState({
                            ImageKTPbase64: event.target.result
                        }, () => this.props.values.Passengers[this.props.index].ImageKTP = this.state.ImageKTPbase64 )
                    };
                    reader.readAsDataURL(file);
                } else {
                    let self = this;
                    image2base64(this.state.ImageKTP[0].preview).then(response => {
                        self.setState({
                            ImageKTPbase64: `data:image/png;base64,${response}`
                        }, () => this.props.values.Passengers[this.props.index].ImageKTP = this.state.ImageKTPbase64 );
                    })
                }
            }
        }
        if(this.state.ImageSIM !== prevState.ImageSIM) {
            if(this.state.ImageSIM.length > 0) {
                let r = /^(blob)/g;
                if(r.test(this.state.ImageSIM[0].preview)) {
                    var file2 = this.state.ImageSIM[0]
                    const reader = new FileReader();
                    reader.onload = (event) => {
                        this.setState({
                            ImageSIMBase64: event.target.result
                        }, () => this.props.values.Passengers[this.props.index].ImageSIM = this.state.ImageSIMBase64)
                    };
                    reader.readAsDataURL(file2);
                } else {
                    let self = this;
                    image2base64(this.state.ImageSIM[0].preview).then(response => {
                        self.setState({
                            ImageSIMBase64: `data:image/png;base64,${response}`
                        }, () => this.props.values.Passengers[this.props.index].ImageSIM = this.state.ImageSIMBase64 );
                    })
                }
            }
        }
    }
    

    render() {
        const {
            props: {
                handleClickPhoneNumberMenu, handleItemPhoneNumberClick, title, index,
                handleChange, handleBlur, values
            },
            state: {
                phoneNumberFlag,
                ImageKTP,
                ImageSIM
            }
        } = this;

        let ImDriver = <TracField
            id={`Passengers[${index}].imDriver`}
            name={`Passengers[${index}].imDriver`}
            type="checkbox"
            labelName="I'm Driver"
            labelSize="small"
            onChange={() => {
                this.setState({ 
                    imDriver: !this.state.imDriver 
                }, () => {
                    this.forceUpdate();
                    this.props.whenImDriver(index);
                    values.Passengers[index].imDriver = this.state.imDriver;
                    values.Passengers[index].Name = this.state.imDriver ? values.fullName : '';
                    values.Passengers[index].PhoneNumber = this.state.imDriver ? values.phoneNumber : '';
                    values.Passengers[index].Address = this.state.imDriver ? values.address : '';
                    values.Passengers[index].IDCardNumber = this.state.imDriver ? (values.ktpNumber !== null ? values.ktpNumber : '') : '';
                    values.Passengers[index].LicenseNumber = this.state.imDriver ? (values.simNumber !== null ? values.simNumber : '') : '';
                    values.Passengers[index].IsForeigner = this.state.imDriver ? values.isForeigner : false;
                    values.Passengers[index].Email = this.state.imDriver ? values.email : '';
                    this.setState({
                        ImageKTP: this.state.imDriver && values.ImageKTP.length > 0 ? values.ImageKTP : [],
                        ImageSIM: this.state.imDriver && values.ImageSIM.length > 0 ? values.ImageSIM : []
                    })
                })
            }}
            checked={this.state.imDriver}
        /> 

        return (
            <Accordion defaultValue={true} title={title} driverInfo>
                <div className="form-row position-relative">
                    {
                        (values.fullName ==='' ||  values.phoneNumber ==='' || values.ktpNumber ==='' || values.address ==='') ? <div className="disabled-input"> </div> : null
                    }
                    { _.filter(values.Passengers, 'imDriver').length === 0 ? ImDriver
                    : (this.props.hideImDriver === index) ? ImDriver : null }
                </div> 
                <p className="desc-info">{lang.pleaseScanUploadKTPSIMSelfdrive[lang.default]}</p>
                <div className="form-row">
                    <div className="form-col initial">
                        <TracField
                            id={`Passengers[${index}].IsForeigner`}
                            name={`Passengers[${index}].IsForeigner`}
                            type="checkbox"
                            labelName="Indonesia Citizen"
                            labelSize="small"
                            onChange={ () => {
                                this.setState({ 
                                    IsForeigner: !this.state.IsForeigner 
                                }, () => values.Passengers[index].IsForeigner = this.state.IsForeigner )
                            }}
                            checked={!this.state.IsForeigner}
                        />
                    </div>
                    <div className="form-col initial">
                        <TracField
                            id={`Passengers[${index}].IsForeigner`}
                            name={`Passengers[${index}].IsForeigner`}
                            type="checkbox"
                            labelName="Foreign Citizen"
                            labelSize="small"
                            onChange={ () => {
                                this.setState({ 
                                    IsForeigner: !this.state.IsForeigner 
                                }, () => values.Passengers[index].IsForeigner = this.state.IsForeigner )
                            }}
                            checked={this.state.IsForeigner}
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col initial">
                        <Dropzone
                            accept="image/*"
                            onDrop={files => this.setState({
                                ImageKTP: files.map(file => Object.assign(file, {
                                    preview: URL.createObjectURL(file)
                                }))
                            })}
                            onFileDialogCancel={() => this.setState({ ImageKTP: [] })}
                            >
                            {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, acceptedFiles, rejectedFiles }) => (
                                <div {...getRootProps()} className="m-id-card-photo">
                                    <div className="upload-area">
                                        { ImageKTP.length > 0 && ImageKTP[0].preview !== null ? ImageKTP.map(file => (
                                            <div key={file.name} className="preview-zone">
                                                <img alt={file.name} src={file.preview} className="preview-image" />
                                            </div>
                                            ))  : 
                                            <div className="drag-zone">
                                                <div className="icon" />
                                                <div className="description">
                                                    You can drag and drop images to upload
                                                </div>
                                            </div>
                                        }
                                        <Button type="button" secondary>Photo KTP</Button>
                                        <input {...getInputProps()} className="input-file" />
                                    </div>
                                </div>
                            )}
                        </Dropzone>
                    </div>
                    <div className="form-col initial">
                        <Dropzone
                            accept="image/*"
                            onDrop={files => this.setState({
                                ImageSIM: files.map(file => Object.assign(file, {
                                    preview: URL.createObjectURL(file)
                                }))
                            })}
                            onFileDialogCancel={() => this.setState({ ImageSIM: [] })}
                            >
                            {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, acceptedFiles, rejectedFiles }) => (
                                <div {...getRootProps()} className="m-id-card-photo">
                                    <div className="upload-area">
                                        { ImageSIM.length > 0 && ImageSIM[0].preview !== null ? ImageSIM.map(file => (
                                            <div key={file.name} className="preview-zone">
                                                <img alt={file.name} src={file.preview} className="preview-image" />
                                            </div>
                                            ))  : 
                                            <div className="drag-zone">
                                                <div className="icon" />
                                                <div className="description">
                                                    You can drag and drop images to upload
                                                </div>
                                            </div>
                                        }
                                        <Button type="button" secondary>Photo SIM</Button>
                                        <input {...getInputProps()} className="input-file" />
                                    </div>
                                </div>
                            )}
                        </Dropzone>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col">
                        <TracField
                            id={`Passengers[${index}].IDCardNumber`}
                            name={`Passengers[${index}].IDCardNumber`}
                            type="number"
                            labelName="KTP Number"
                            onChange={handleChange}
                            value={ values.Passengers[index].IDCardNumber }
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col">
                        <TracField
                            id={`Passengers[${index}].LicenseNumber`}
                            name={`Passengers[${index}].LicenseNumber`}
                            type="number"
                            labelName="SIM Number"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={ values.Passengers[index].LicenseNumber }
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col">
                        <TracField
                            id={`Passengers[${index}].Name`}
                            name={`Passengers[${index}].Name`}
                            type="text"
                            labelName="Full Name"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={ values.Passengers[index].Name }
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col">
                        <TracField
                            id={`Passengers[${index}].Address`}
                            name={`Passengers[${index}].Address`}
                            type="text"
                            labelName="Residential Address"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={ values.Passengers[index].Address }
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-col">
                        <div className="input-group">
                            <div className="input-group-col phone-number" style={{position:'absolute', marginTop:9}}>
                                <TracField

                                    id="phoneNumberPassengerCountryCode"
                                    type="select"
                                    name="phone_numberPassenger_country_code"
                                    labelName="Phone Number"
                                    showItem={phoneNumberFlag.showItem}
                                    onClick={handleClickPhoneNumberMenu}
                                    onItemClick={handleItemPhoneNumberClick}
                                    options={optionPhoneNumber}
                                    value={phoneNumberFlag.selectedValue}
                                    icon={phoneNumberFlag.selectedIcon}
                                    listHasIcon
                                    displayIcon
                                />
                            </div>
                            <div className="input-group-col" style={{marginLeft:75}}>
                                <TracField
                                    id={`Passengers[${index}].PhoneNumber`}
                                    name={`Passengers[${index}].PhoneNumber`}
                                    type="number"
                                    labelName="Phone Number"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={ values.Passengers[index].PhoneNumber }
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </Accordion>
        )
    }
}

const mapStateToProps = ({ carrental }) => {
    const { passanger } = carrental;
    return { passanger };
}
export default connect(mapStateToProps, {
    passangerCarRental
})(FormSelfDriver);
