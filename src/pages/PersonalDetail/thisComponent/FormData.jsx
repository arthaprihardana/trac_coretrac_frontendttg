/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-01 00:39:53 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 15:26:28
 */
import React, { PureComponent, Fragment } from "react";
import { Button, TracField, Modal } from "atom";
import { Form, Formik, FieldArray } from "formik";
import * as Yup from "yup";
import { connect } from 'react-redux';
import _ from 'lodash';
import moment from "moment";
import "moment/locale/id";
import Dropzone from "react-dropzone";
import image2base64 from "image-to-base64";
import { withRouter } from 'react-router-dom';

// flags
import flagId from "assets/images/flags/id.svg";
// import flagEn from "assets/images/flags/en.svg";
import FormSelfDriver from "./FormSelfDriver";
import FormChauffeur from "./FormChauffeur";
import FormBusRental from "./FormBusRental";

// components
import { PulseLoader } from 'react-spinners';
import lang from '../../../assets/data-master/language'

// actions
import { postReservationCarRental, passangerCarRental } from "actions";

// helpers
import { localStorageDecrypt, localStorageEncrypt } from "helpers";

// validation schema
const AuthSchema = Yup.object().shape({
    // Passengers: Yup.array().of(Yup.object().shape(
    //     {
    //         IDCardNumber: Yup.string()
    //             .required("KTP Number is required")
    //             .min(16, "ID card Number must be 16 Character")
    //             .max(16, "ID card Number must be 16 Character"),
    //         PhoneNumber: Yup.string()
    //             .required("Phone Number is required")
    //             .min(9, "Phone Number minimal 9 Character")
    //             .max(13, "Phone Number max 13 Character")
    //     }
    //     )
    // ),
    fullName: Yup.string()
        .required("Full Name is required"),
    address: Yup.string()
        .required("Address is required"),
    phoneNumber: Yup.string()
        .required("Phone Number is required")
        .min(9, "Phone Number minimal 9 Character")
        .max(13, "Phone Number max 13 Character"),
    ktpNumber: Yup.string()
        .required("KTP Number is required")
        .min(16, "ID card Number must be 16 Character")
        .max(16, "ID card Number must be 16 Character")
});

const optionPhoneNumber = [
    {
        id: "id",
        value: "+62",
        text: "+(62) Indonesia",
        icon: flagId,
    },
    // {
    //     id: "us",
    //     value: "+1",
    //     text: "(+1) United State",
    //     icon: flagEn,
    // }
];

class FormData extends PureComponent {

    state = {
        bookingForSomeone: false,
        phoneNumber: {
            showItem: false,
            selectedValue: "+62",
            selectedIcon: flagId,
        },
        user_login: {},
        modalOpen: false,
        type_service: null,
        productService: "",
        content: null,
        user:'',
        selectedCar: [],
        ImageKTP: [],
        ImageKTPbase64: [],
        ImageSIM: [],
        ImageSIMbase64: [],
        reservation: {},
        reservationError: null,
        IsForeigner: false,
        hideImDriver: null
    };

    componentDidMount = () => {
        const { type } = this.props;
        this.getUserLogin();
        let selectedCar = JSON.parse(localStorage.getItem('selectedCar'));
        let CarRentalFormInput = localStorageDecrypt('_fi', 'object');
        console.log("CarRentalFormInput", CarRentalFormInput );
        // console.log("CarRentalFormInput",JSON.parse(CarRentalFormInput.type_service));
        if(CarRentalFormInput !== null) {
            switch (type) {
                case 'airport-transfer':
                    this.setState({
                        type_service: JSON.parse(CarRentalFormInput.data[0].type_service),
                        selectedCar: selectedCar
                    });
                    break;
                case 'bus-rental':
                    this.setState({
                        type_service: { ProductServiceId: 'PSV23454' },
                        selectedCar: selectedCar
                    });
                    break;
                default:
                    this.setState({
                        type_service: JSON.parse(CarRentalFormInput.type_service),
                        selectedCar: selectedCar
                    });
                    break;
            }
        } else {
            this.props.history.push('/')
        }
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.reservationsuccess !== prevState.reservationsuccess) {
            return { reservation: this.props.reservationsuccess }
        }
        return null;
    }
    
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.reservation !== null) {
                if(Object.keys(snapshot.reservation.Data).length > 0) {
                    localStorageEncrypt('_rv', JSON.stringify(snapshot.reservation.Data))
                    this.props.onSubmit({status: true});
                } else {
                    this.props.onSubmit({status: false, msg: snapshot.reservation.ErrorMessage});
                }
            }
        }

        if(this.state.ImageKTP !== prevState.ImageKTP) {
            if(this.state.ImageKTP.length > 0) {
                let r = /^(blob)/g;
                if(r.test(this.state.ImageKTP[0].preview)) {
                    var file = this.state.ImageKTP[0];
                    const reader = new FileReader();
                    reader.onload = (event) => {
                        this.setState({
                            ImageKTPbase64: event.target.result
                        })
                    };
                    reader.readAsDataURL(file);
                } else {
                    let self = this;
                    image2base64(this.state.ImageKTP[0].preview).then(response => {
                        self.setState({
                            ImageKTPbase64: `data:image/png;base64,${response}`
                        });
                    })
                }
            }
        }

        if(this.state.ImageSIM !== prevState.ImageSIM) {
            if(this.state.ImageSIM.length > 0) {
                let r = /^(blob)/g;
                if(r.test(this.state.ImageSIM[0].preview)) {
                    var file = this.state.ImageSIM[0];
                    const reader = new FileReader();
                    reader.onload = (event) => {
                        this.setState({
                            ImageSIMbase64: event.target.result
                        })
                    };
                    reader.readAsDataURL(file);
                } else {
                    let self = this;
                    image2base64(this.state.ImageSIM[0].preview).then(response => {
                        self.setState({
                            ImageSIMbase64: `data:image/png;base64,${response}`
                        });
                    })
                }
            }
        }
    }

    getUserLogin = () => {
        let local_auth = localStorageDecrypt('_aaa', 'object');
        if(local_auth !== null) {
            this.setState({
                user_login: local_auth,
                ImageKTP: [{ name: local_auth.NoKTP, preview: local_auth.ImageKTP }],
                ImageSIM: [{ name: local_auth.NoSIM, preview: local_auth.ImageSIM }],
            })
        } else {
            this.props.history.push('/')
        }
    }

    manageSubmitCarRentalSelfDrive = value => {
        const { user_login, selectedCar, type_service } = this.state;
        let BusinessUnitId = localStorageDecrypt('_bu');
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        // console.log("CarRentalFormInput =>", CarRentalFormInput);
        let selectedPromo = JSON.parse(localStorage.getItem('selectedPromo'));

        let TotalPrice = _.reduce(selectedCar, (sum, n) => {
            return sum + n.rentInfo.total
        }, 0);
        let TotalPriceExtrasPerCar = _.map(selectedCar, v => {
            let totalPriceExtras = _.reduce(v.extraItems, (sum, n) => {
                return sum + n.total
            }, 0);
            return totalPriceExtras;
        });
        let TotalPriceExtras = _.reduce(TotalPriceExtrasPerCar, (sum, n) => sum + n );
        let rangeDay = moment(CarRentalFormInput.date.endDate).diff(moment(CarRentalFormInput.date.startDate), 'days');
        // console.log("RANGE DAY ==>", rangeDay);
        // console.log("END DAY ==>", moment(`${CarRentalFormInput.date.endDate} ${CarRentalFormInput.date.time}`).format('YYYY-MM-DD HH:mm:ss'));
        // console.log("DATE FIELD ==>", CarRentalFormInput.date);
        // let date_ranged =[];
        // for(var xx=0; xx<=rangeDay; xx++){
        //     let tmp_ = moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).add(xx, 'days').format('YYYY-MM-DD HH:mm:ss')
        //     date_ranged.push(tmp_)
        // }
        // console.log("Ranged FIELD ==>", date_ranged);
        let passengers = _.map(value.Passengers, v => {
            return {
                "Name": v.Name,
                "PhoneNumber": v.PhoneNumber,
                "Email": v.Email,
                "IDCardNumber": v.IDCardNumber,
                "NPWPNumber": v.NPWPNumber,
                "LicenseNumber": v.LicenseNumber,
                "PassportNumber": v.PassportNumber,
                "Address": v.Address,
                "IsPIC": v.imDriver ? true : false,
                "ImageKTP": v.ImageKTP,
                "ImageSIM": v.ImageSIM,
                "IsForeigner": v.IsForeigner
            }
        });
        
        let ReservationDetail = _.map(selectedCar, (v, k) => {
            let combinePassengers = [];
            let isPIC = _.filter(passengers, { IsPIC: true });
            if(_.filter(passengers, { IsPIC: true }).length > 0) {
                combinePassengers.push(passengers[k], isPIC[0]);
                combinePassengers = _.uniqBy(combinePassengers, 'IsPIC');
            } else {
                combinePassengers.push(passengers[k], isPIC);
            }
            let ReservationPromo=[];
            v.extraItems = v.extraItems.filter(f=>{
                return f.amounts > 0
            });
            if(selectedPromo !== null){
                if(selectedPromo.is_all_vehicle==1){
                    ReservationPromo = [
                        {
                            "PromoCode": selectedPromo.code,
                            "CategoryPromoId": selectedPromo.category_id,
                            "CategoryPromoName": "Redeemed Code",
                            "TypeValue": selectedPromo.type_value,
                            "Value": selectedPromo.value
                        }
                    ];
                }else{
                    let data_f = selectedPromo.vehicle_id.some(t=>{
                        return t.vehicle_id == v.vehicleTypeId
                    });
                    if(data_f){
                        ReservationPromo = [
                            {
                                "PromoCode": selectedPromo.code,
                                "CategoryPromoId": selectedPromo.category_id,
                                "CategoryPromoName": "Redeemed Code",
                                "TypeValue": selectedPromo.type_value,
                                "Value": selectedPromo.value
                            }
                        ];
                    }else{
                        ReservationPromo = [];
                    }
                }
            }else{
                ReservationPromo = []
            }
            return {
                "UserId": user_login.UserLoginId,
                "UnitTypeId": v.vehicleTypeId,
                "UnitTypeName": v.vehicleAlias,
                "CityId": CarRentalFormInput.city.cityId,
                "CityName": CarRentalFormInput.city.text,
                "BranchId": CarRentalFormInput.city.value,
                "StartDate": [
                    moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).format('YYYY-MM-DD HH:mm:ss')
                ],
                "EndDate": [
                    moment(`${CarRentalFormInput.date.endDate} ${CarRentalFormInput.date.time}`).add(CarRentalFormInput.select_package, 'hours').format('YYYY-MM-DD HH:mm:ss')
                ],
                "Duration": CarRentalFormInput.select_package,
                "QtyUnit": 1,
                "QtyPassenger": 1,  // 1 untuk self drive / untuk chaffeur gunakan v.totalSeat
                "IsStay": false,
                "OutTown": false,
                "IsPlanned": false,
                "IsExpedition": false,
                "IsWithDriver": false,
                "Fuel": "01",
                "TollAndParking": "01",
                "DriverOrRider": "01",
                "IsPaymentUpfront": true,
                "ContractId": v.unitContractList[0].contractId,
                "ContractItemId": v.unitContractList[0].contractItemId,
                "ProductId": "PD-001",
                "MaterialId": v.unitContractList[0].materialId,
                "Price": v.rentInfo.basePrice * (rangeDay + 1),
                "IsTransmissionManual": v.isTransmissionManual,
                "PriceExtras": TotalPriceExtrasPerCar[k],
                "MsProductId": type_service.MsProductId,
                "MsProductServiceId": type_service.ProductServiceId,
                "ZoneId": null,     // kecuali air transport dan bus
                "TotalDistance": null,  // kecuali air transport dan bus
                "PickupLocation": [{
                    "CityId": CarRentalFormInput.city.cityId,
                    "Long": CarRentalFormInput.address.geometry.lng,
                    "Lat": CarRentalFormInput.address.geometry.lat,
                    "Alamat": CarRentalFormInput.address.text,
                    "Time": moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).format('YYYY-MM-DD HH:mm:ss'),
                    "Notes": this.props.notes
                }],
                "DropLocation": [{
                    "CityId": CarRentalFormInput.city.cityId,
                    "Long": CarRentalFormInput.address.geometry.lng,
                    "Lat": CarRentalFormInput.address.geometry.lat,
                    "Alamat": CarRentalFormInput.address.text,
                    "Time": moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).add(CarRentalFormInput.select_package, 'hours').format('YYYY-MM-DD HH:mm:ss'),   // tambah durasi
                    "Notes": this.props.notes
                }],
                "Passengers": combinePassengers,
                "ReservationExtras": v.extraItems,
                "ReservationPromo": ReservationPromo
            }
        });
        return {
            "CompanyId": BusinessUnitId === "0104" ? "0100" : "0200",
            "CustomerName": "Customer B2C",
            "BusinessUnitId": BusinessUnitId,
            "BranchId": CarRentalFormInput.city.value,
            "ServiceTypeId": selectedCar[0].unitContractList[0].serviceTypeId,
            "ServiceTypeName": `${selectedCar[0].unitContractList[0].contractSAPCode} - ${selectedCar[0].unitContractList[0].serviceTypeId}`,
            "CreatedBy": user_login.UserLoginId,
            "PIC": user_login.UserLoginId,
            "PICName": value.fullName,
            "PICPhone": `${this.state.phoneNumber.selectedValue}${value.phoneNumber}`,
            "NotesReservation": this.props.notes,
            "TotalPrice": (TotalPrice * (rangeDay + 1)) + TotalPriceExtras,
            "ReservationDetail": ReservationDetail
        }
    }

    manageSubmitCarRentalChaffeur = value => {
        const { user_login, selectedCar, type_service } = this.state;
        let BusinessUnitId = localStorageDecrypt('_bu');
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        let TotalPrice = _.reduce(selectedCar, (sum, n) => {
            return sum + n.rentInfo.total
        }, 0);
        let TotalPriceExtrasPerCar = _.map(selectedCar, v => {
            let totalPriceExtras = _.reduce(v.extraItems, (sum, n) => {
                return sum + n.total
            }, 0);
            return totalPriceExtras;
        });
        let TotalPriceExtras = _.reduce(TotalPriceExtrasPerCar, (sum, n) => sum + n );
        let rangeDay = moment(CarRentalFormInput.date.endDate).diff(moment(CarRentalFormInput.date.startDate), 'days');
        let selectedPromo = JSON.parse(localStorage.getItem('selectedPromo'));

        let ReservationDetail = _.map(selectedCar, (v, k) => {
            let ReservationPromo=[];
            v.extraItems = v.extraItems.filter(f=>{
                return f.amounts > 0
            });
            if(selectedPromo !== null){
                if(selectedPromo.is_all_vehicle==1){
                    ReservationPromo = [
                        {
                            "PromoCode": selectedPromo.code,
                            "CategoryPromoId": selectedPromo.category_id,
                            "CategoryPromoName": "Redeemed Code",
                            "TypeValue": selectedPromo.type_value,
                            "Value": selectedPromo.value
                        }
                    ];
                }else{
                    let data_f = selectedPromo.vehicle_id.some(t=>{
                        return t.vehicle_id == v.vehicleTypeId
                    });
                    if(data_f){
                        ReservationPromo = [
                            {
                                "PromoCode": selectedPromo.code,
                                "CategoryPromoId": selectedPromo.category_id,
                                "CategoryPromoName": "Redeemed Code",
                                "TypeValue": selectedPromo.type_value,
                                "Value": selectedPromo.value
                            }
                        ];
                    }else{
                        ReservationPromo = [];
                    }
                }
            }else{
                ReservationPromo = []
            }
            return {
                "UserId": user_login.UserLoginId,
                "UnitTypeId": v.vehicleTypeId,
                "UnitTypeName": v.vehicleAlias,
                "CityId": CarRentalFormInput.city.cityId,
                "CityName": CarRentalFormInput.city.text,
                "BranchId": CarRentalFormInput.city.value,
                "StartDate": [
                    moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).format('YYYY-MM-DD HH:mm:ss')
                ],
                "EndDate": [
                    CarRentalFormInput.select_package === "4" ?
                        moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).add(CarRentalFormInput.select_package, 'hours').format('YYYY-MM-DD HH:mm:ss')
                    : 
                        rangeDay > 0 ? 
                            moment(`${CarRentalFormInput.date.endDate} ${CarRentalFormInput.date.time}`).add(CarRentalFormInput.select_package, 'hours').format('YYYY-MM-DD HH:mm:ss')
                        :
                            moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).add(CarRentalFormInput.select_package, 'hours').format('YYYY-MM-DD HH:mm:ss')
                    // type_service.ProductServiceId === "PSV0001" ?
                    //     moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).add(CarRentalFormInput.select_package, 'hours').format('YYYY-MM-DD HH:mm:ss')
                    // : 
                    //     moment(`${CarRentalFormInput.date.endDate} ${CarRentalFormInput.date.time}`).format('YYYY-MM-DD HH:mm:ss')
                ],
                "Duration": CarRentalFormInput.select_package,
                "QtyUnit": 1,
                "QtyPassenger": v.totalSeat,  // 1 untuk self drive / untuk chaffeur gunakan v.totalSeat
                "IsStay": false,
                "OutTown": false,
                "IsPlanned": false,
                "IsExpedition": false,
                "IsWithDriver": true,
                "Fuel": "01",
                "TollAndParking": "01",
                "DriverOrRider": "01",
                "IsPaymentUpfront": true,
                "ContractId": v.unitContractList[0].contractId,
                "ContractItemId": v.unitContractList[0].contractItemId,
                "ProductId": "PD-001",
                "MaterialId": v.unitContractList[0].materialId,
                "Price": v.rentInfo.basePrice * (rangeDay + 1),
                "IsTransmissionManual": v.isTransmissionManual,
                "PriceExtras": TotalPriceExtrasPerCar[k],
                "MsProductId": type_service.MsProductId,
                "MsProductServiceId": type_service.ProductServiceId,
                "ZoneId": null,     // kecuali air transport dan bus
                "TotalDistance": null,  // kecuali air transport dan bus
                "PickupLocation": [{
                    "CityId": CarRentalFormInput.city.cityId,
                    "Long": CarRentalFormInput.address.geometry.lng,
                    "Lat": CarRentalFormInput.address.geometry.lat,
                    "Alamat": CarRentalFormInput.address.text,
                    "Time": moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).format('YYYY-MM-DD HH:mm:ss'),
                    "Notes": this.props.notes
                }],
                "DropLocation": [{
                    "CityId": CarRentalFormInput.city.cityId,
                    "Long": CarRentalFormInput.address.geometry.lng,
                    "Lat": CarRentalFormInput.address.geometry.lat,
                    "Alamat": CarRentalFormInput.address.text,
                    "Time": moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).add(CarRentalFormInput.select_package, 'hours').format('YYYY-MM-DD HH:mm:ss'),   // tambah durasi
                    "Notes": this.props.notes
                }],
                "Passengers": [{
                    "Name": value.fullName,
                    "PhoneNumber": value.phoneNumber,
                    "Email": value.email,
                    "IDCardNumber": value.ktpNumber,
                    "NPWPNumber": "",
                    "LicenseNumber": value.simNumber,
                    "PassportNumber": "",
                    "Address": value.address,
                    "IsPIC": true,
                    // "IsPIC": user_login.IsPic === "1" ? true : false,
                    "ImageKTP": this.state.ImageKTPbase64,
                    "ImageSIM": "",
                    "IsForeigner": value.isForeigner
                }],
                "ReservationExtras": v.extraItems,
                "ReservationPromo": ReservationPromo
            }
        });

        return {
            "CompanyId": BusinessUnitId === "0104" ? "0100" : "0200",
            "CustomerName": "Customer B2C",
            "BusinessUnitId": BusinessUnitId,
            "BranchId": CarRentalFormInput.city.value,
            "ServiceTypeId": selectedCar[0].unitContractList[0].serviceTypeId,
            "ServiceTypeName": `${selectedCar[0].unitContractList[0].contractSAPCode} - ${selectedCar[0].unitContractList[0].serviceTypeId}`,
            "CreatedBy": user_login.UserLoginId,
            "PIC": user_login.UserLoginId,
            "PICName": value.fullName,
            "PICPhone": `${this.state.phoneNumber.selectedValue}${value.phoneNumber}`,
            "NotesReservation": this.props.notes,
            "TotalPrice": (TotalPrice * (rangeDay + 1)) + TotalPriceExtras,
            "ReservationDetail": ReservationDetail
        }
    }

    manageSubmitAirportTransfer = value => {
        const { user_login, selectedCar, type_service } = this.state;
        let BusinessUnitId = localStorageDecrypt('_bu');
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        let TotalPrice = _.reduce(selectedCar, (sum, n) => {
            return sum + n.rentInfo.total
        }, 0);
        let TotalPriceExtrasPerCar = _.map(selectedCar, v => {
            let totalPriceExtras = _.reduce(v.extraItems, (sum, n) => {
                return sum + n.total
            }, 0);
            return totalPriceExtras;
        });

        let TotalPriceExtras = _.reduce(TotalPriceExtrasPerCar, (sum, n) => sum + n );
        let rangeDay = moment(CarRentalFormInput.data[0].date.endDate).diff(moment(CarRentalFormInput.data[0].date.startDate), 'days');
        let zone = JSON.parse(localStorageDecrypt('_z'));
        let selectedPromo = JSON.parse(localStorage.getItem('selectedPromo'));

        let ReservationDetail = _.map(selectedCar, (v, k) => {
            let ReservationPromo=[];
            v.extraItems = v.extraItems.filter(f=>{
                return f.amounts > 0
            });
            if(selectedPromo !== null){
                if(selectedPromo.is_all_vehicle==1){
                    ReservationPromo = [
                        {
                            "PromoCode": selectedPromo.code,
                            "CategoryPromoId": selectedPromo.category_id,
                            "CategoryPromoName": "Redeemed Code",
                            "TypeValue": selectedPromo.type_value,
                            "Value": selectedPromo.value
                        }
                    ];
                }else{
                    let data_f = selectedPromo.vehicle_id.some(t=>{
                        return t.vehicle_id == v.vehicleTypeId
                    });
                    if(data_f){
                        ReservationPromo = [
                            {
                                "PromoCode": selectedPromo.code,
                                "CategoryPromoId": selectedPromo.category_id,
                                "CategoryPromoName": "Redeemed Code",
                                "TypeValue": selectedPromo.type_value,
                                "Value": selectedPromo.value
                            }
                        ];
                    }else{
                        ReservationPromo = [];
                    }
                }
            }else{
                ReservationPromo = []
            }
            return  {
                "UserId": user_login.UserLoginId,
                "UnitTypeId": v.vehicleTypeId,
                "UnitTypeName": v.vehicleAlias,
                "CityId": CarRentalFormInput.data[0].city.cityId,
                "MsAirportCode": CarRentalFormInput.data[0].airport.value,
                "CityName": CarRentalFormInput.data[0].city.text,
                "BranchId": CarRentalFormInput.data[0].city.value,
                "StartDate": [
                    moment(`${CarRentalFormInput.data[0].date.startDate} ${CarRentalFormInput.data[0].date.time}`).format('YYYY-MM-DD HH:mm:ss')
                ],
                "EndDate": [
                    moment(`${CarRentalFormInput.data[0].date.endDate} ${CarRentalFormInput.data[0].date.time}`).format('YYYY-MM-DD HH:mm:ss')
                ],
                "Duration": zone.KM,
                "QtyUnit": 1,
                "QtyPassenger": v.totalSeat,
                "IsWithDriver": true,
                "IsExpedition": false,
                "IsPaymentUpfront": true,
                "ContractId": v.unitContractList[0].contractId,
                "ContractItemId": v.unitContractList[0].contractItemId,
                "ProductId": "PD-001",
                "MaterialId": v.unitContractList[0].materialId,
                "LicensePlate": "B 1670 FC",
                "Price": v.rentInfo.basePrice * (rangeDay + 1),
                "IsTransmissionManual": v.isTransmissionManual,
                "PriceExtras": TotalPriceExtrasPerCar[k],
                "MsProductId": type_service.MsProductId,
                "MsProductServiceId": type_service.ProductServiceId,
                "ZoneId": zone.MsZoneId,
                "TotalDistance": localStorage.getItem('distance'),
                "PickupLocation": [{
                    "CityId": CarRentalFormInput.data[0].city.cityId,
                    "Long": CarRentalFormInput.data[0].airport.geometry.lng,
                    "Lat": CarRentalFormInput.data[0].airport.geometry.lat,
                    "Alamat": CarRentalFormInput.data[0].address.text,
                    "Time": moment(`${CarRentalFormInput.data[0].date.startDate} ${CarRentalFormInput.data[0].date.time}`).format('YYYY-MM-DD HH:mm:ss'),
                    "Notes": this.props.notes
                }],
                "DropLocation": [{
                    "CityId": CarRentalFormInput.data[0].city.cityId,
                    "Long": CarRentalFormInput.data[0].address.geometry.lng,
                    "Lat": CarRentalFormInput.data[0].address.geometry.lat,
                    "Alamat": CarRentalFormInput.data[0].address.text,
                    "Time": moment(`${CarRentalFormInput.data[0].date.endDate} ${CarRentalFormInput.data[0].date.time}`).format('YYYY-MM-DD HH:mm:ss'),
                    "Notes": this.props.notes
                }],
                // "Passengers": [value.Passengers[k]],
                "Passengers": [{
                    "Name": value.fullName,
                    "PhoneNumber": value.phoneNumber,
                    "Email": value.email,
                    "IDCardNumber": value.ktpNumber,
                    "NPWPNumber": "",
                    "LicenseNumber": value.simNumber,
                    "PassportNumber": "",
                    "Address": value.address,
                    "IsPIC": user_login.IsPic === "1" ? true : false,
                    "ImageKTP": this.state.ImageKTPbase64,
                    "ImageSIM": "",
                    "IsForeigner": value.isForeigner
                }],
                "ReservationExtras": v.extraItems,
                "ReservationPromo": ReservationPromo
            }
        });

        return {
            "CompanyId": BusinessUnitId === "0104" ? "0100" : "0200",
            "CustomerName": "Customer B2C",
            "BusinessUnitId": BusinessUnitId,
            "BranchId": CarRentalFormInput.data[0].city.value,
            "ServiceTypeId": selectedCar[0].unitContractList[0].serviceTypeId,
            "ServiceTypeName": `${selectedCar[0].unitContractList[0].contractSAPCode} - ${selectedCar[0].unitContractList[0].serviceTypeId}`,
            "CreatedBy": user_login.UserLoginId,
            "PIC": user_login.UserLoginId,
            "PICName": value.fullName,
            "PICPhone": `${this.state.phoneNumber.selectedValue}${value.phoneNumber}`,
            "NotesReservation": this.props.notes,
            "TotalPrice": (TotalPrice * (rangeDay + 1)) + TotalPriceExtras,
            "ReservationDetail": ReservationDetail
        }
    }

    manageSubmitBusRental = value => {
        const { user_login, selectedCar, type_service } = this.state;
        let BusinessUnitId = localStorageDecrypt('_bu');
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));

        let TotalPrice = _.reduce(selectedCar, (sum, n) => {
            return sum + n.rentInfo.total
        }, 0);
        let TotalPriceExtrasPerCar = _.map(selectedCar, v => {
            let totalPriceExtras = _.reduce(v.extraItems, (sum, n) => {
                return sum + n.total
            }, 0);
            return totalPriceExtras;
        });
        let TotalPriceExtras = _.reduce(TotalPriceExtrasPerCar, (sum, n) => sum + n );
        let rangeDay = moment(CarRentalFormInput.date.endDate).diff(moment(CarRentalFormInput.date.startDate), 'days');

        let selectedPromo = JSON.parse(localStorage.getItem('selectedPromo'));

        let zone = JSON.parse(localStorageDecrypt('_z'));
        let passengers = _.map(value.Passengers, v => {
            return {
                "Name": v.Name,
                "PhoneNumber": v.PhoneNumber,
                "Email": v.Email,
                "IDCardNumber": v.IDCardNumber,
                "NPWPNumber": v.NPWPNumber,
                "LicenseNumber": v.LicenseNumber,
                "PassportNumber": v.PassportNumber,
                "Address": v.Address,
                "IsPIC": v.imDriver ? true : false,
                "ImageKTP": v.ImageKTP,
                "ImageSIM": "",
                "IsForeigner": v.IsForeigner
            }
        });
        let isPIC = {
            "Name": value.fullName,
            "PhoneNumber": value.phoneNumber,
            "Email": value.email,
            "IDCardNumber": value.ktpNumber,
            "NPWPNumber": "",
            "LicenseNumber": value.simNumber,
            "PassportNumber": "",
            "Address": value.address,
            "IsPIC": true,
            "ImageKTP": this.state.ImageKTPbase64,
            "ImageSIM": "",
            "IsForeigner": value.isForeigner
        };
        let ReservationDetail = _.map(selectedCar, (v, k) => {
            let combinePassengers = [];
            if(_.filter(passengers, { IsPIC: true }).length > 0) {
                combinePassengers.push(passengers[k], isPIC);
                combinePassengers = _.uniqBy(combinePassengers, 'IsPIC');
            } else {
                combinePassengers.push(passengers[k], isPIC);
            }
            v.extraItems = v.extraItems.filter(f=>{
                return f.amounts > 0
            });
            let tmp_distance = localStorage.getItem('distance');
            let totalDistance =  Math.ceil(tmp_distance);
            if(parseInt(totalDistance)===0){
                totalDistance = 1;
            }
            let ReservationPromo=[];
            if(selectedPromo !== null){
                if(selectedPromo.is_all_vehicle==1){
                    ReservationPromo = [
                        {
                            "PromoCode": selectedPromo.code,
                            "CategoryPromoId": selectedPromo.category_id,
                            "CategoryPromoName": "Redeemed Code",
                            "TypeValue": selectedPromo.type_value,
                            "Value": selectedPromo.value
                        }
                    ];
                }else{
                    let data_f = selectedPromo.vehicle_id.some(t=>{
                        return t.vehicle_id == v.vehicleTypeId
                    });
                    if(data_f){
                        ReservationPromo = [
                            {
                                "PromoCode": selectedPromo.code,
                                "CategoryPromoId": selectedPromo.category_id,
                                "CategoryPromoName": "Redeemed Code",
                                "TypeValue": selectedPromo.type_value,
                                "Value": selectedPromo.value
                            }
                        ];
                    }else{
                        ReservationPromo = [];
                    }
                }
            }else{
                ReservationPromo = []
            }
            return {
                "UserId": user_login.UserLoginId,
                "UnitTypeId": v.vehicleTypeId,
                "UnitTypeName": v.vehicleAlias,
                "CityId": CarRentalFormInput.city.cityId,
                "CityName": CarRentalFormInput.city.text,
                // "BranchId": CarRentalFormInput.city.value,
                "BranchId": this.props.branchFromBus.BranchId,
                "StartDate": [
                    moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).format('YYYY-MM-DD HH:mm:ss')
                ],
                "EndDate": [
                    moment(`${CarRentalFormInput.date.endDate} ${CarRentalFormInput.date.time}`).format('YYYY-MM-DD HH:mm:ss')
                ],
                "Duration": zone.KM,
                "QtyUnit": 1,
                "QtyPassenger": v.totalSeat,  // 1 untuk self drive / untuk chaffeur gunakan v.totalSeat
                "IsStay": false,
                "OutTown": false,
                "IsPlanned": false,
                "IsExpedition": false,
                "IsWithDriver": false,
                "Fuel": "01",
                "TollAndParking": "01",
                "DriverOrRider": "01",
                "IsPaymentUpfront": true,
                "ContractId": v.unitContractList !== null ? v.unitContractList[0].contractId : "",
                "ContractItemId": v.unitContractList !== null ? v.unitContractList[0].contractItemId : "",
                "ProductId": "PD-001",
                "MaterialId": v.unitContractList !== null ? v.unitContractList[0].materialId : "",
                "Price": v.rentInfo.basePrice * (rangeDay + 1),
                "IsTransmissionManual": v.isTransmissionManual,
                "PriceExtras": TotalPriceExtrasPerCar[k],
                "MsProductId": 'PRD0008',
                "MsProductServiceId": '',
                "ZoneId": zone.MsZoneId,
                "TotalDistance": totalDistance,
                "PickupLocation": [{
                    "CityId": CarRentalFormInput.city.cityId,
                    "Long": CarRentalFormInput.address.geometry.lng,
                    "Lat": CarRentalFormInput.address.geometry.lat,
                    "Alamat": CarRentalFormInput.address.text,
                    "Time": moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).format('YYYY-MM-DD HH:mm:ss'),
                    "Notes": this.props.notes
                }],
                "DropLocation": [{
                    "CityId": CarRentalFormInput.city.cityId,
                    "Long": CarRentalFormInput.destination.geometry.lng,
                    "Lat": CarRentalFormInput.destination.geometry.lat,
                    "Alamat": CarRentalFormInput.destination.text,
                    "Time": moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).add(CarRentalFormInput.select_package, 'hours').format('YYYY-MM-DD HH:mm:ss'),   // tambah durasi
                    "Notes": this.props.notes
                }],
                "Passengers": combinePassengers,
                "ReservationExtras": v.extraItems,
                "ReservationPromo": ReservationPromo
            }
        });
        return {
            "CompanyId": BusinessUnitId === "0104" ? "0100" : "0200",
            "CustomerName": "Customer B2C",
            "BusinessUnitId": BusinessUnitId,
            "BranchId": CarRentalFormInput.city.value,
            "ServiceTypeId": selectedCar[0].unitContractList[0].serviceTypeId,
            "ServiceTypeName": `${selectedCar[0].unitContractList[0].contractSAPCode} - ${selectedCar[0].unitContractList[0].serviceTypeId}`,
            "CreatedBy": user_login.UserLoginId,
            "PIC": user_login.UserLoginId,
            "PICName": value.fullName,
            "PICPhone": `${this.state.phoneNumber.selectedValue}${value.phoneNumber}`,
            "NotesReservation": this.props.notes,
            "TotalPrice": (TotalPrice) + TotalPriceExtras,
            "ReservationDetail": ReservationDetail
        }
    }

    handleSubmit = value => {
        const { type_service , user_login } = this.state;
        const { type } = this.props;
        let FormDataReservation;
        switch (type) {
            case 'airport-transfer':
                FormDataReservation = this.manageSubmitAirportTransfer(value);
                break;
            case 'bus-rental':
                FormDataReservation = this.manageSubmitBusRental(value);
                break;
            default:
                let serviceType = type_service;
                if(serviceType.ProductServiceId === 'PSV0001') {    // Car Rental Self Driver
                    FormDataReservation = this.manageSubmitCarRentalSelfDrive(value);
                } else {    // Car Rental Chaffeur
                    FormDataReservation = this.manageSubmitCarRentalChaffeur(value);
                }
                break;
        }
        if(type_service.ProductServiceId=="PSV0001"){
            //----TEMPORARY
            this.props.postReservationCarRental(FormDataReservation);


            // if(user_login.HasCreditCard){
            //     this.props.postReservationCarRental(FormDataReservation);
            // }else{
            //     this.setState({
            //         modalOpen: true,
            //         disablePromo: true,
            //         content: <div className="success-login-wrapper">
            //             <p className="main-title">Alert</p>
            //             <p className="sub-title">You need to Fill your Credit Card</p>
            //         </div>
            //     })
            // }
        }else{
            //----TEMPORARY
            this.props.postReservationCarRental(FormDataReservation);

        }

    };

    handleClickPhoneNumberMenu = (e) => {
        const { phoneNumber } = this.state;
        this.setState(prevState => ({
            ...prevState,
            phoneNumber: {
                ...phoneNumber,
                showItem: !prevState.phoneNumber.showItem
            }
        }));
        e.preventDefault();
    }

    handleItemPhoneNumberClick = (e, v) => {
        const icon = e.currentTarget.querySelector('img').getAttribute("src");
        this.setState(prevState => ({
            ...prevState,
            phoneNumber: {
                showItem: false,
                selectedValue: v,
                selectedIcon: icon,
            }
        }));
        e.preventDefault();
    }

    handleModalClose = (e) => {
        e.stopPropagation();
        this.setState({ modalOpen: false });
    }

    render() {
        const {
            handleItemPhoneNumberClick,
            handleClickPhoneNumberMenu,
            handleImDriver,
            state: { phoneNumber,user, user_login, modalOpen, type_service, selectedCar, reservationError, ImageKTP },
            props: { type }
        } = this;
        let serviceType;
        switch (type) {
            case 'airport-transfer':
                serviceType = type_service;
                break;
            case 'bus-rental':
                serviceType = type_service;
                break;
            default:
                serviceType = type_service;
                break;
        }
        
        return (
            <Fragment>
                { selectedCar.length > 0 && Object.keys(user_login).length > 0 && 
                
                <Formik
                    initialValues={{
                        fullName: user_login.FirstName !== null ?`${user_login.FirstName} ${user_login.LastName}` : "",
                        fullNameDone: user_login.FirstName !== null ,
                        phoneNumber: user_login.NoHandphone !== null ? user_login.NoHandphone.split(/^(\+62|0)/g)[2] : "",
                        phoneNumberDone: user_login.NoHandphone !== null ,
                        email: user_login.EmailPersonal,
                        ktpNumber: user_login.NoKTP !== null ? user_login.NoKTP : '',
                        ktpNumberDone: user_login.NoKTP !== null,
                        simNumber: user_login.NoSIM,
                        address: user_login.Address !== null ? user_login.Address : '',
                        addressDone: user_login.Address !== null,
                        isForeigner: user_login.IsForeigner === "1" ? true : false,
                        ImageKTP: this.state.ImageKTP.length > 0 ? this.state.ImageKTP : [],
                        ImageSIM: this.state.ImageSIM.length > 0 ? this.state.ImageSIM : [],
                        ImageKTPDone: this.state.ImageKTP[0].preview !== null ,
                        ImageSIMDone: this.state.ImageSIM.length > 0 ,
                        Passengers: type === "bus-rental" ? _.map(selectedCar, v => {
                                return {
                                    "Name": '',
                                    "PhoneNumber": '',
                                    "Email": '',
                                    "IDCardNumber": '',
                                    "NPWPNumber": '',
                                    "LicenseNumber": '',
                                    "PassportNumber": '',
                                    "Address": '',
                                    "IsPIC": false,
                                    "ImageKTP": '',
                                    "ImageSIM": '',
                                    "IsForeigner": false
                                }
                            })
                            : serviceType.ProductServiceId === 'PSV0001' ? _.map(selectedCar, v => { 
                                return { 
                                    "Name": '',
                                    "PhoneNumber": '',
                                    "Email": '',
                                    "IDCardNumber": '',
                                    "NPWPNumber": '',
                                    "LicenseNumber": '',
                                    "PassportNumber": '',
                                    "Address": '',
                                    "IsPIC": false,
                                    "ImageKTP": '',
                                    "ImageSIM": '',
                                    "IsForeigner": false,
                                    "imDriver": false
                                } 
                            }) : [{
                                "Name": '',
                                "PhoneNumber": '',
                                "Email": '',
                                "IDCardNumber": '',
                                "NPWPNumber": '',
                                "LicenseNumber": '',
                                "PassportNumber": '',
                                "Address": '',
                                "IsPIC": false,
                                "ImageKTP": '',
                                "ImageSIM": '',
                                "IsForeigner": false,
                                "CarResponsible": {}
                            }]
                    }}
                    validationSchema={AuthSchema}
                    onSubmit={(values, actions) => {
                        this.handleSubmit(values);
                    }}
                    >
                    {({ isSubmitting, handleChange, handleBlur, values }) => (
                        <Form>
                            <div className="form-row valign-top">
                                <div className="form-col position-relative">
                                    {
                                        (serviceType.MsProductId !== "PRD0008" && values.fullNameDone) ? <div className="disabled-input"> </div> : null
                                    }
                                    <TracField
                                        id="fullName"
                                        name="fullName"
                                        type="text"
                                        labelName={lang.fullName[lang.default]}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.fullName}
                                    />
                                    <div className="input-note">
                                        {lang.asOnIDCard[lang.default]}
                                    </div>
                                </div>
                                <div className="form-col position-relative">
                                    {
                                        (serviceType.MsProductId !== "PRD0008" && values.phoneNumberDone) ? <div className="disabled-input"> </div> : null
                                    }
                                    <div className="input-group">
                                        <div className="input-group-col phone-number" style={{position:'absolute', marginTop:9}}>
                                            <TracField
                                                id="phoneNumberCountryCode"
                                                type="select"
                                                name="phone_number_country_code"
                                                labelName={lang.phoneNumber[lang.default]}
                                                showItem={phoneNumber.showItem}
                                                onClick={handleClickPhoneNumberMenu}
                                                onItemClick={handleItemPhoneNumberClick}
                                                options={optionPhoneNumber}
                                                value={phoneNumber.selectedValue}
                                                icon={phoneNumber.selectedIcon}
                                                listHasIcon
                                                displayIcon
                                            />
                                        </div>
                                        <div className="input-group-col" style={{marginLeft:75}}>
                                            <TracField
                                                id="phoneNumber"
                                                name="phoneNumber"
                                                type="number"
                                                labelName={lang.phoneNumber[lang.default]}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.phoneNumber}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*{ serviceType.ProductServiceId !== "PSV0001" && */}
                            <div className="form-row valign-top">
                                <div className="form-col position-relative">
                                    {
                                        (serviceType.MsProductId !== "PRD0008" && values.ktpNumberDone) ? <div className="disabled-input"> </div> : null
                                    }
                                    <TracField
                                        id="ktpNumber"
                                        name="ktpNumber"
                                        type="number"
                                        labelName={lang.ktpNumber[lang.default]}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.ktpNumber}
                                    />
                                </div>
                                <div className="form-col position-relative">
                                    {
                                        (serviceType.MsProductId !== "PRD0008" && values.addressDone) ? <div className="disabled-input"> </div> : null
                                    }
                                    <TracField
                                        id="address"
                                        name="address"
                                        type="text"
                                        labelName={lang.residentialAddress[lang.default]}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.address}
                                    />
                                </div>
                            </div>

                            {/*}*/}
                            { serviceType.ProductServiceId !== "PSV0001" && 
                            <div className="form-row valign-top">
                                <div className="form-col initial">
                                    <TracField
                                        id="isForeigner"
                                        name="isForeigner"
                                        type="checkbox"
                                        labelName={lang.indeonsiaCitizen[lang.default]}
                                        labelSize="small"
                                        onChange={ () => {
                                            this.setState({ 
                                                IsForeigner: !this.state.IsForeigner 
                                            }, () => values.IsForeigner = this.state.IsForeigner )
                                        }}
                                        checked={!this.state.IsForeigner}
                                    />
                                </div>
                                <div className="form-col initial">
                                    <TracField
                                        id="isForeigner"
                                        name="isForeigner"
                                        type="checkbox"
                                        labelName={lang.foreignCitizen[lang.default]}
                                        labelSize="small"
                                        onChange={ () => {
                                            this.setState({ 
                                                IsForeigner: !this.state.IsForeigner 
                                            }, () => values.IsForeigner = this.state.IsForeigner )
                                        }}
                                        checked={this.state.IsForeigner}
                                    />
                                </div>
                            </div> }
                            <div className="form-row">
                                <div className="form-col position-relative">
                                    {
                                        (serviceType.MsProductId !== "PRD0008" && values.ImageKTPDone) ? <div className="disabled-input"> </div> : null
                                    }
                                    <Dropzone
                                        accept="image/*"
                                        onDrop={files => this.setState({
                                            ImageKTP: files.map(file => Object.assign(file, {
                                                preview: URL.createObjectURL(file)
                                            }))
                                        }, () => values.ImageKTP = this.state.ImageKTP )}
                                        // onFileDialogCancel={() => this.setState({ ImageKTP: [{name:null, value:null}] })}
                                        >
                                        {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, acceptedFiles, rejectedFiles }) => (
                                            <div {...getRootProps()} className="m-id-card-photo">
                                                <div className="upload-area">
                                                    { ImageKTP.length > 0 && ImageKTP[0].preview !== null ? ImageKTP.map(file => (
                                                        <div key={file.name} className="preview-zone">
                                                            <img alt={file.name} src={file.preview} className="preview-image" />
                                                        </div>
                                                        ))  : 
                                                        <div className="drag-zone">
                                                            <div className="icon" />
                                                            <div className="description">
                                                                {lang.youCanDragAndDrop[lang.default]}
                                                            </div>
                                                        </div>
                                                    }
                                                    <Button type="button" secondary>{lang.photoKTP[lang.default]}</Button>
                                                    <input {...getInputProps()} className="input-file" />
                                                </div>
                                            </div>
                                        )}
                                    </Dropzone>
                                </div>
                            </div>
                            { type === "bus-rental" ? 
                                <div className="self-drive">
                                    <FieldArray 
                                        name="Passengers"
                                        render={ arrayHelpers => {
                                            return (
                                                <div>
                                                    {
                                                        selectedCar.map((v, k) => <FormBusRental key={k} title={`${lang.coordinatorBus[lang.default]} #${k+1} ${lang.informationCoordinator[lang.default]}`} index={k} handleChange={handleChange} handleBlue={handleBlur} values={values} imDriver={values} whenImDriver={v => this.setState({ hideImDriver: v })} hideImDriver={this.state.hideImDriver} />)
                                                    }
                                                </div>
                                            )
                                        }}>
                                    </FieldArray>
                                </div>
                            : 
                            serviceType.ProductServiceId === 'PSV0001' ?
                                    <div className="self-drive">
                                        <FieldArray 
                                            name="Passengers"
                                            render={ arrayHelpers => {
                                                return (
                                                    <div>
                                                        {
                                                            selectedCar.map((v, k) => <FormSelfDriver key={k} title={`Driver #${k+1} Information`} index={k} handleChange={handleChange} handleBlue={handleBlur} values={values} imDriver={values} whenImDriver={v => this.setState({ hideImDriver: v })} hideImDriver={this.state.hideImDriver} />)
                                                        }
                                                    </div>
                                                )
                                            }}>
                                        </FieldArray>
                                    </div>
                                :
                                    <div className="chauffeur">
                                        <FieldArray 
                                            name="Passengers"
                                            render={ arrayHelpers => ( 
                                                <div>
                                                { this.state.bookingForSomeone &&
                                                    <div className={`form-passenger-detail ${this.state.bookingForSomeone ? "active" : "else"}`}>
                                                        { values.Passengers.map(( v, i ) => (
                                                            <FormChauffeur key={i} selectedCar={selectedCar} handleChange={handleChange} handleBlue={handleBlur} values={values} bookingForSomeone={this.state.bookingForSomeone} index={i} />
                                                        )) }
                                                    </div> }
                                                    { this.state.bookingForSomeone &&
                                                    <div className="form-row">
                                                        <div className="add-other-pasenger" onClick={() => arrayHelpers.push({
                                                            "Name": '',
                                                            "PhoneNumber": '',
                                                            "Email": '',
                                                            "IDCardNumber": '',
                                                            "NPWPNumber": '',
                                                            "LicenseNumber": '',
                                                            "PassportNumber": '',
                                                            "Address": '',
                                                            "IsPIC": true,
                                                            "ImageKTP": '',
                                                            "ImageSIM": '',
                                                            "IsForeigner": false,
                                                            "CarResponsible": {}
                                                        }) }>
                                                            <span className="icon" />
                                                            {lang.addPassenger[lang.default]}
                                                        </div>
                                                    </div>
                                                    }
                                                </div>
                                            )}>
                                        </FieldArray>
                                    </div>
                            }
                            <div className="form-row"></div>
                            <div className="form-row">
                                <Button
                                    type="submit"
                                    primary
                                    disabled={this.props.loading}>
                                    {this.props.loading ? 
                                        <PulseLoader
                                            sizeUnit={"px"}
                                            size={7}
                                            color={'#ffffff'}
                                            loading={this.props.loading}
                                        />
                                        : lang.continueButton[lang.default] }
                                </Button>
                            </div>
                            <Modal
                                open={modalOpen}
                                onClick={this.handleModalClose}
                                size="small"
                                header={{
                                    withCloseButton: false,
                                    onClick: this.handleModalClose,
                                    children: ''
                                }}
                                content={{
                                    children: (this.state.content),
                                }}
                                footer={{
                                    position: 'center',
                                    children: (
                                        <div className="modal-footer-action">
                                            <Button primary goto="/dashboard/account-settings">Goto Profile</Button>
                                        </div>
                                    ),
                                }}
                            />
                        </Form>
                    )}
                </Formik>
                
                }
            </Fragment>
        );
    }
}

const mapStateToProps = ({ carrental, masterbranch }) => {
    const { branchFromBus } = masterbranch;
    const { loading, reservationsuccess, reservationfailure, notes, passanger } = carrental;
    return { loading, branchFromBus, reservationsuccess, reservationfailure, notes, passanger };
}

export default withRouter(
    connect(mapStateToProps, {
        passangerCarRental,
        postReservationCarRental
    })(FormData)
);
