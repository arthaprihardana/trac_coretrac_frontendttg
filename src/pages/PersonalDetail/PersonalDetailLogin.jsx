/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-09 00:39:24 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 16:10:27
 */
// libraries
import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

// assets & styles
import ThisPersonalDetailWrapper from "./ThisPersonalDetailWrapper";
import ThisPersonalDetailLoginContent from "./ThisPersonalDetailLoginContent";

class PersonalDetailLogin extends PureComponent {

    componentDidMount = () => {
        const {
            history: {
                action,
                push
            },
            match: {
                params: {
                    id
                }
            },
            isLogin
        } = this.props;
        if(action === "POP" && isLogin) {
            push(`/personal-detail/${id}`)
        }
    }

    render() {
        const {
            match: {
                params: {
                    id
                }
            }
        } = this.props;

        return (
            <ThisPersonalDetailWrapper type={id}>
                <ThisPersonalDetailLoginContent type={id} />
            </ThisPersonalDetailWrapper>
        );
    }
}

const mapStateToProps = ({ login }) => {
    const { isLogin } = login;
    return { isLogin }
}

export default withRouter(
    connect(mapStateToProps, {
        
    })(PersonalDetailLogin)
);
