/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-11 07:36:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 15:49:15
 */
import React, { PureComponent } from "react";
import { delay, localStorageDecrypt } from 'helpers';

// components
import { Button, Modal } from "atom";
import { Main } from "templates";
import { withRouter } from 'react-router-dom';
import { Sidebar, SidebarBookingDetail, SidebarNote, TotalPaymentMobile } from "organisms";
import {
    OrderSteps,
    SidebarCarOrder,
    SidebarPaymentDetail
} from "molecules";
import FormData from "./thisComponent/FormData";

// style
import "./PersonalDetail.scss";

import { CustomerGlobal } from "context/RentContext";
import lang from '../../assets/data-master/language'
import successImg from 'assets/images/background/login-success.svg';
import connect from "react-redux/es/connect/connect";

class PersonalDetail extends PureComponent {
    
    state = {
        showNoteDetail: false,
        email: "",

        selectedCar: [],
        bookingDetail: {},
        showDetail: false,
        modalOpen: false,
        reservationError: null
    };

    componentDidMount() {
        const {
            history: {
                action,
                push
            },
            match: {
                url,
                params: {
                    id
                }
            }
        } = this.props;
        // if(action === "POP") {
        //     push(`/payment/${id}`)
        // }

        document.body.classList.add("add-padding-top");
        document.body.classList.add("background-grey");
        let selectedCar = JSON.parse(localStorage.getItem('selectedCar'));
        let ucr =  localStorageDecrypt('_aaa', 'object');
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        // let tmp = JSON.parse(JSON.stringify(CarRentalFormInput.type_service));
        // let product = JSON.parse(tmp);
        // this.setState({
        //     productService: product
        // });
        // console.log(product);
        // this.backListener = browserHistory.listen(location => {
        //     if (location.action === "POP") {
        //        push()
        //     }
        // });
        // this.props.router.setRouteLeaveHook(this.props.route, this.routerWillLeave);
        this.setState({
            selectedCar: selectedCar,
            bookingDetail: CarRentalFormInput,
            modalOpen: false
        });
    }
    routerWillLeave() { // return false to block navigation, true to allow
        const {
            history: {
                action,
                push
            },
            match: {
                url,
                params: {
                    id
                }
            }
        } = this.props;
        if (action === 'POP') {
            push(`/transport-list/${id}`)
            // window.location('/transport-list/car')
        }
    }
    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
        document.body.classList.remove("background-grey");
        this.routerWillLeave();
    }

    // note detail
    handleNoteDetail = () => {
        this.setState({ showNoteDetail: !this.state.showNoteDetail });
    };

    handleShowPaymentDetail = (status) => {
        this.setState({ showDetail: status});
    }

    handleSubmitForm = (value) => {
        const { 
            history: { 
                push 
            }, 
            match: { 
                url,
                params: {
                    id
                }
            } 
        } = this.props;
        if(value.status) {
            this.setState({ modalOpen: true })
            // delay(1000).then(() => push(`/payment/${id}`));
        } else {
            this.setState({ modalOpen: true, reservationError: value.msg })
            // window.location.reload();
        }
    }

    handleModalClose = e => {
        e.stopPropagation();
        this.setState({ modalOpen: false })
    }

    render() {
        const { 
            handleShowPaymentDetail, 
            state: {
                selectedCar, 
                bookingDetail,

                productService,
                showDetail,
                modalOpen,
                reservationError
            },
            props: {
                // rentContext: {
                //     state: {
                //         values: {
                //             type_service
                //         }
                //     }
                // },
                history: { 
                    push 
                }, 
                match: {
                    params: {
                        id
                    }
                }
            }
        } = this;
        
        // order step
        let orderStepsData = [
            { step: "Car Order", className: "done" },
            { step: "Personal Detail", className: "current" },
            { step: "Payment", className: "" }
        ];

        // if(type_service === 'PSV0001'){
        //     orderStepsData = [
        //         { step: "Car Order", className: "done" },
        //         { step: "Personal Detail", className: "current" },
        //         { step: "Driver Information", className: "" },
        //         { step: "Payment", className: "" }
        //     ];
        // }
        
        return (
            <Main hideMenu solid headerUserLogin headerClose>

                <Modal
                    open={modalOpen}
                    onClick={e => {
                        if(reservationError !== null) {
                            this.handleModalClose(e)
                        } else {
                            this.handleModalClose(e);
                            delay(1000).then(() => push(`/payment/${id}`));
                        }
                    }}
                    header={{
                        withCloseButton: false,
                        onClick: (e) => this.handleModalClose(e),
                        children: ''
                    }}
                    content={{
                        children: reservationError !== null ? (
                            <div className="success-login-wrapper">
                                <div className="img-success">
                                    <img src={successImg} alt=""/>
                                </div>
                                <p className="main-title">{lang.yoreReservationFailed[lang.default]} <br/>{lang.yoreReservationCannot[lang.default]}</p>
                                <p className="sub-title">{reservationError}</p>
                            </div>
                        ) : (
                            <div className="success-login-wrapper">
                                <div className="img-success">
                                    <img src={successImg} alt=""/>
                                </div>
                                <p className="main-title">{lang.congrats[lang.default]} <br/>{lang.yoreReservationCreated[lang.default]}</p>
                                <p className="sub-title">{lang.pleaseContinueToPayment[lang.default]}</p>
                            </div>
                        ),
                    }}
                    footer={{
                        position: 'center',
                        children: reservationError !== null ? (
                            <div className="modal-footer-action">
                                <Button primary onClick={e => {
                                    this.handleModalClose(e);
                                }}>Exit</Button>
                            </div>
                        ) : (
                            <div className="modal-footer-action">
                                <Button primary onClick={e => {
                                    this.handleModalClose(e);
                                    delay(1000).then(() => {
                                        push(`/payment/${id}`)
                                    });
                                }}>Continue</Button>
                            </div>
                        )
                    }}
                />

                {/* order steps */}
                <OrderSteps activeStep={2} type={id} />

                {/* Order Detail */}
                <div className="order-detail">
                    <div className="container">
                        <div className="order-detail-container">
                            {/* Left Content */}
                            <div className="left-content">
                                <div className="personal-detail personal-account create-account">
                                    <div className="title">
                                        <h3>{lang.personalDetail[lang.default]}</h3>
                                    </div>
                                    <div className="form-wrapper">
                                        <FormData onSubmit={this.handleSubmitForm}  type={id} />
                                    </div>
                                </div>
                            </div>

                            {/* Sidebar */}
                            <Sidebar showDetail={ showDetail } returnShowDetail = { handleShowPaymentDetail }>
                                {/* Car Order */}
                                { selectedCar.length > 0 && <SidebarCarOrder data={selectedCar} type={id} /> }

                                {/* Booking Detail */}
                                { Object.keys(bookingDetail).length > 0 && <SidebarBookingDetail data={bookingDetail} type={id} /> }

                                {/* Note/Request */}
                                <SidebarNote hasContent />

                                {/* Payment Detail */}
                                { selectedCar.length > 0 && <SidebarPaymentDetail data={selectedCar} type={id} /> }
                            </Sidebar>
                        </div>
                    </div>
                    {/* TotalPaymentMobile */}
                    <TotalPaymentMobile showDetail={ showDetail } returnShowDetail = { handleShowPaymentDetail }/>
                </div>
            </Main>
        );
    }
}

const mapStateToProps = ({  product }) => {
    const { selectedproductservicetype } = product;
    return { selectedproductservicetype}
}

export default withRouter(connect(mapStateToProps, {
})(PersonalDetail));

// export default CustomerGlobal(withRouter(PersonalDetail));