/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-09 01:23:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 14:54:35
 */
import { delay } from 'helpers';
import React, { Component } from 'react';
import FormDataLogin from "./thisComponent/FormDataLogin";
import { withRouter } from 'react-router-dom';
import { Button } from "atom";
import { PulseLoader } from "react-spinners";
import lang from '../../assets/data-master/language'

class ThisPersonalDetailLoginContent extends Component {

    state = {
        isLoading: false
    }

    handleLogin = (id) => {
        const { 
            history: { push }
        } = this.props;
        delay(1000).then(() => push(`/personal-detail/${id}`));
        // delay(1000).then(() => push(`/personal-detail`));
    }

    render() {
        const {
            props: {
                type,
                history: {
                    push
                }
            },
            handleLogin,
            state: {
                isLoading
            }
        } = this;

        return (
            <div className="left-content">
                <div className="personal-detail">
                    <div className="title">
                        <h3>Login</h3>
                        <p>{lang.bookFasterAndEasier[lang.default]}</p>
                    </div>
                    <div className="form-wrapper">
                        <FormDataLogin modalAction={() => handleLogin(type)} type={type} />
                    </div>
                    <div className="action-bottom">
                        <h5>{lang.dontHaveAccount[lang.default]}</h5>
                        <Button type="button" onClick={() => {
                            this.setState({
                                isLoading: true
                            }, () => {
                                delay(1000).then(() => {
                                    this.setState({ isLoading: false })
                                    push("/personal-detail-create-account/"+type)
                                })
                            })
                        }} outline>
                        {isLoading ?
                            <PulseLoader
                                sizeUnit={"px"}
                                size={7}
                                color={'#c76703'}
                                loading={isLoading}
                            /> : lang.signUp[lang.default]}
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(ThisPersonalDetailLoginContent);