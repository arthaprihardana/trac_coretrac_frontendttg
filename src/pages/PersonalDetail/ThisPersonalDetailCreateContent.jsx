/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-09 01:31:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 16:28:53
 */
import React, { Component } from 'react';
import { Formik, Form } from "formik";
import { withRouter } from 'react-router-dom';
import { Button, TracField,Modal,  Text } from "atom";
import * as Yup from "yup";
import { delay } from 'helpers';
import { PulseLoader } from "react-spinners";
import _ from "lodash";
import { ModalSuccess } from "molecules";

// services
import auth0 from 'services/Auth';

// validation schema
const AuthSchema = Yup.object().shape({
    email: Yup.string()
        .email("Email should be valid!")
        .required("Email is required!"),
    password: Yup.string()
        .min("6", "Should be 6 characters!")
        .required("Password is required!"),
    confirmPassword: Yup.string()
        .min("6", "Should be 6 characters!")
        .required("Password is required!")
});

class ThisPersonalDetailCreateContent extends Component {
    
    state = {
        termsModal:false,
        aggrement:false,
        isLoading: false
    }

    handleSubmit = (values, { setSubmitting }) => {
        this.props.onSubmit(values);
        setSubmitting(false);
    }

    componentWillReceiveProps = (nextProps) => {
        if(nextProps.isLoading !== this.props.isLoading) return true;
        if(nextProps.isModalOpen !== this.props.isModalOpen) return true;
        if(nextProps.onReceiveResponse !== this.props.onReceiveResponse) return true;
        return false;
    }

    handleTermsModal = (type) => (e) => {
        e.preventDefault();
        let action = false;
        type === 'open' ? action = true : action = false;
        this.setState({ termsModal: action })
    }
    agreeTerm = () =>{
        this.setState({
            aggrement: true,termsModal:false
        })

        // this.handleTermsModal('close');
    }
    disagreeTerm = () =>{
        this.setState({aggrement: false,termsModal:false})
        // this.handleTermsModal('close');
    }
    render() {
        const {
            props: {
                type,
                history: {
                    push
                },
                handleModalClose,
                isModalOpen,
                onReceiveResponse
            },
            state: {
                aggrement,
                termsModal,
                isLoading
            }
        } = this;
        return (
            <div className="left-content">
                <div className="personal-detail">
                    <div className="title">
                        <h3>Create Account</h3>
                        <p>Book Faster and easier by Login or Register</p>
                    </div>
                    <div className="form-wrapper">
                        <ModalSuccess
                            open={isModalOpen}
                            size="medium"
                            goto={ onReceiveResponse.user_login !== null && "/personal-detail-login/car-rental" }
                            onCloseModal={handleModalClose}
                            content={ onReceiveResponse.user_login !== null ? <div className="content">
                                <Text>Congratulation {onReceiveResponse.user_login && _.upperFirst(_.split(onReceiveResponse.user_login.EmailPersonal, "@")[0])}!</Text>
                                <Text>You&apos;ve become a TRAC Member!</Text>
                                <Text>Enjoy our service and benefit as a TRAC Member</Text>
                            </div> : <div className="content">
                                <Text>Register Failed!</Text>
                                <Text>Email has been register!</Text>
                                <Text>Please register with other email!</Text>
                            </div>}
                        />
                        <Modal
                            open={termsModal}
                            onClick={this.handleTermsModal('close')}
                            header={{
                                withCloseButton: true,
                                onClick: this.handleTermsModal('close'),
                                children: <Text type="h3">Terms and Condition</Text>,
                            }}
                            content={{
                                children: (
                                    <div>
                                        <ol>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                        </ol>
                                    </div>
                                ),
                            }}
                            footer={{
                                position: 'center',
                                children: (
                                    <div className="modal-footer-action">
                                        <Button primary onClick={this.agreeTerm}>I Agree</Button>
                                        <Button outline onClick={this.disagreeTerm}>Decline</Button>
                                    </div>
                                ),
                            }}
                        />
                        <Formik
                            initialValues={{
                                email: "",
                                password: "",
                                confirmPassword: ""
                            }}
                            validationSchema={AuthSchema}
                            onSubmit={this.handleSubmit}>
                            {({ isSubmitting, handleChange, handleBlur, values }) => (
                                <Form>
                                    <div className="form-row">
                                        <div className="form-col">
                                            <TracField
                                                id="email"
                                                name="email"
                                                type="text"
                                                labelName="Email"
                                                validate={AuthSchema.email}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.email}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="form-col">
                                            <TracField
                                                id="password"
                                                name="password"
                                                type="password"
                                                labelName="Password"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.password}
                                                icon="password hide"
                                            />
                                        </div>
                                        <div className="form-col">
                                            <TracField
                                                id="confirmPassword"
                                                name="confirmPassword"
                                                type="password"
                                                labelName="Confirm Password"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.confirmPassword}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <TracField
                                            id="aggreement"
                                            name="aggreement"
                                            type="checkbox"
                                            labelName="I agree with the <a>Terms and Conditions</a>"
                                            labelSize="small"
                                            onChange={() =>
                                                this.setState({termsModal:true})
                                            }
                                            checked={aggrement}
                                        />
                                    </div>
                                    <div className="form-row">
                                        <div className="form-action-col">
                                            <Button type="submit" primary disabled={!aggrement}>
                                                { this.props.isLoading ?
                                                    <PulseLoader
                                                        sizeUnit={"px"}
                                                        size={7}
                                                        color={'#ffffff'}
                                                        loading={this.props.isLoading}
                                                    /> : "Sign Up" 
                                                }
                                            </Button>
                                        </div>
                                        <div className="form-action-col">
                                            Or Continue with
                                        </div>
                                        <div className="form-action-col">
                                            <div className="social-icon">
                                                <a
                                                    className="google"
                                                    target="_blank"
                                                    rel="noopener noreferrer"
                                                    onClick={() => {
                                                        const Auth0 = new auth0(this.props.match.params.id);
                                                        Auth0.openGoogleauth()
                                                    }}>
                                                    google
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                    <div className="action-bottom">
                        <h5>Already have an Account?</h5>
                        <Button type="button" onClick={() => {
                            this.setState({
                                isLoading: true
                            }, () => {
                                delay(1000).then(() => {
                                    this.setState({ isLoading: false })
                                    push("/personal-detail-login/"+type)
                                })
                            })
                        }} outline>
                            { isLoading ?
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={7}
                                    color={'#c76703'}
                                    loading={isLoading}
                                /> : "Login" 
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(ThisPersonalDetailCreateContent);