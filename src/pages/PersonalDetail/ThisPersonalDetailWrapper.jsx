/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-09 00:45:08 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 23:52:30
 */
import React, { Component } from 'react';
import { OrderSteps } from "molecules";
import { SideOrderDetail } from 'organisms';
import { Main } from "templates";
import { defaultDidMountSetStyle, defaultWillUnmountSetStyle } from 'helpers';
import "./PersonalDetail.scss";

// helpers
import { localStorageDecrypt } from "helpers";

class ThisPersonalDetailWrapper extends Component {

    state = {
        selectedCar: [],
        bookingDetail: {}
    }

    componentDidMount() {
        defaultDidMountSetStyle();
        let selectedCar = JSON.parse(localStorage.getItem('selectedCar'));
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        this.setState({
            selectedCar: selectedCar,
            bookingDetail: CarRentalFormInput
        });
    }

    componentWillUnmount() {
        defaultWillUnmountSetStyle();
    }

    render(){
        const { 
            children, 
            type
        } = this.props;
        const { selectedCar, bookingDetail } = this.state;

        return (
            <Main solid headerUserLogin headerClose>
                {/* <OrderSteps activeStep={type === 'car-rental' ? 2 : 3} type={type} /> */}
                <OrderSteps activeStep={2} type={type} /> 
                <div className="order-detail">
                    <div className="container">
                        <div className="order-detail-container">
                            {children}
                            {selectedCar.length > 0 && Object.keys(bookingDetail).length > 0 && <SideOrderDetail contentNotes selectedCar={selectedCar} bookingDetail={bookingDetail} type={type} />}
                        </div>
                    </div>
                </div>
            </Main>
        )
    }
}

export default ThisPersonalDetailWrapper;