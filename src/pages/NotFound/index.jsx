import React, { PureComponent } from "react";

// style
import "./NotFound.scss";

// components
import { Main } from "templates";
import { Button } from "atom";

class NotFound extends PureComponent {
    componentDidMount() {
        document.body.classList.add("add-padding-top");
    }

    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
    }

    render() {
        return (
            <Main className="p-not-found" solid>
                <div className="not-found-content">
                    <h1>404</h1>
                    <h3>Page not found</h3>
                    <Button type="button" primary goto="/">
                        Back to Home
                    </Button>
                </div>
            </Main>
        );
    }
}

export default NotFound;