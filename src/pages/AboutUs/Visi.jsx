import React from 'react';

const Visi = () => {
    return (
        <div className="about-visi">
            <div className="container">
                <div className="left-content">
                    <p className="title">“Menjadi Besar Bersama Anda”</p>
                    <p className="sub-title">TRAC merupakan anak perusahaan PT Serasi Autoraya dan bagian dari keluarga Astra, yang memberikan layanan solusi transportasi secara menyeluruh. Kami adalah salah satu perusahaan pertama dan terbesar yang menyediakan layanan sewa kendaraan.  Namun kami tak akan berada di sini tanpa Anda.</p>
                </div>
                <div className="right-content">
                    <p className="content">Sejak berdiri tahun 1986, TRAC berusaha mengembangkan produk dan layanan. Berawal dari menyewakan hanya lima mobil saja, kini TRAC telah berkembang menjadi market leader di bidang solusi transportasi yang mengelola hingga ribuan kendaraan, yang terdiri z mobil, sepeda motor, dan bus.</p>
                    <p className="content">Kami pun memahami bahwa kebutuhan masyarakat kian berkembang. Mobilitas yang semakin tinggi menuntut efisiensi waktu. Perusahaan membutuhkan solusi transportasi untuk mendukung kegiatan bisnis, begitupun masyarakat umum perlu transportasi yang aman dan nyaman dalam setiap aktivitas.</p>
                    <p className="content">TRAC bisa menjadi solusi praktis bagi perusahaan maupun individu. Kami tak hanya menyediakan rental kendaraan, layanan yang kami tawarkan dapat disesuaikan dengan kebutuhan Anda. Kami menyadari bahwa setiap pelanggan memiliki keunikan tersendiri sehingga perlu layanan yang berbeda-beda. Kepuasan pelanggan adalah misi utama kami karena TRAC ingin menjadi besar bersama Anda.</p>
                </div>
            </div>
        </div>
    )
}

export default Visi;