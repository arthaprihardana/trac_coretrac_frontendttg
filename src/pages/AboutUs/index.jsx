import React, { Component } from 'react';
import { Main, Static } from 'templates';
import bannerImage from 'assets/images/trac/company/CompanyBanner.jpg'
import './AboutUs.scss';
import Visi from './Visi';
import Features from './Features';
import ImgItem1 from 'assets/images/trac/company/CorporateLease.jpg'
import ImgItem2 from 'assets/images/trac/company/PersonalRental.jpg'
import ImgItem3 from 'assets/images/trac/company/AstraFMS.jpg'
import lang from '../../assets/data-master/language'

class AboutUs extends Component {
    state = {
        features: [
            {
                id: 1,
                title: 'Corporate Lease',
                detail: 'Manfaatkan solusi transportasi yang menyeluruh untuk beragam kebutuhan bisnis Anda. Kami menjamin layanan transportasi yang aman, nyaman, dan terukur dengan jangka waktu kerja sama minimal satu tahun.',
                img: ImgItem1
            },
            {
                id: 2,
                title: 'Personal Rental',
                detail: 'Kami menyediakan layanan sewa kendaraan untuk semua aktivitas harian Anda. Liburan, arisan,  reuni, atau perjalanan keluar kota, menjadi lebih menyenangkan. Semua layanan kami terjamin aman, nyaman, dengan akses reservasi yang mudah.',
                img: ImgItem2
            },
            {
                id: 3,
                title: 'ASTRA FMS',
                detail: 'Kelola transportasi perusahaan secara maksimal dengan memanfaatkan sistem transportasi yang memadukan teknologi aplikasi berbasis cloud dan internet of things. Kami merancang ASTRA FMS untuk membantu Anda mengelola dan mengawasi transportasi dengan mudah.',
                img: ImgItem3
            },
        ]
    }
    render(){
        const {features} = this.state;
        return (
            <Main className="p-about-us">
                <Static title={`${lang.aboutUs[lang.default]} TRAC-Astra Rent`} bannerImg={bannerImage}>
                    <Visi />
                    <Features data={features} />
                </Static>
            </Main>
        )
    }
}

export default AboutUs;