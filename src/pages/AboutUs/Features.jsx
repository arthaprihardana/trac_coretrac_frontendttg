import React from 'react';

const Features = ({data}) => {
    return(
        <div className="features">
            <div className="container">
                {
                    data.map(feature => {
                        return (
                            <div key={feature.id} className="feature-item">
                                <div className="img-feature">
                                    <img src={feature.img} alt={feature.id}/>
                                </div>
                                <div className="content-feature">
                                    <p className="title-feature">{feature.title}</p>
                                    <p className="sub-title-feature">{feature.detail}</p>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default Features;