// libraries
import React, { PureComponent } from "react";
import { defaultDidMountSetStyle, defaultWillUnmountSetStyle } from 'helpers';

// local components
import { Main } from "templates";
import { OrderSteps } from "molecules";
import { SideOrderDetail } from 'organisms';
import ThisPickupContent from "./ThisPickupContent";
import ThisPickupWrapper from "./ThisPickupWrapper";

// assets & styles
import "./PickupDetails.scss";


class PickupDetails extends PureComponent {

    componentDidMount() {
        defaultDidMountSetStyle();
    }

    componentWillUnmount() {
        defaultWillUnmountSetStyle();
    }

    render() {
        return (
            <Main solid hideMenu headerClose>
                <OrderSteps activeStep={2} type="bus-rental" />
                <ThisPickupWrapper>
                    <ThisPickupContent />
                    <SideOrderDetail contentNotes />
                </ThisPickupWrapper>
            </Main>
        );
    }
}
export default PickupDetails;