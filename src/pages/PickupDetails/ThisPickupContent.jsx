// libraries
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { delay } from 'helpers';
import { Form, Formik } from "formik";

// local components
import {Button, TracField} from 'atom';


class ThisPickupContent extends Component {
    continuePickup = () => {

        const {
            history: {
                push
            }
        } = this.props;

        delay(1000).then(() => push('/personal-detail-login/bus-rental'));
    }

    render() {

        const {
            continuePickup
        } = this;

        return (
            <div className="left-content">
                <div className="personal-detail">
                    <div className="title">
                        <h3>Pickup Details</h3>
                        <p>Please enter the pickup detail information for a more approximate pickup</p>
                    </div>
                    <div className="form-wrapper">
                        <Formik>
                            {({ isSubmitting, handleChange, handleBlur, values }) => (
                                <Form>
                                    <div className="form-row">
                                        <div className="form-col">
                                            <TracField
                                                id="email"
                                                name="email"
                                                type="text"
                                                labelName="eg. Lobby utama Hotel Kempinski"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.email}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="form-action-col">
                                            <div className="btn-continue">
                                                <Button type="button" primary onClick={continuePickup}>Continue</Button>
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(ThisPickupContent);