import React, { PureComponent } from "react";
import { CopyToClipboard } from 'react-copy-to-clipboard';

// components
import { Main } from "templates";
import { Button } from 'atom';
import { withRouter } from 'react-router-dom';
import cx from 'classnames';

// style
import "./BookingSuccess.scss";

// image
import bookingSuccessImage from "assets/images/background/booking-success.svg";

class BookingSuccess extends PureComponent {

    state = {
        referral: 'SHARE2015',
        copy: false
    }

    componentDidMount() {
        document.body.classList.add("add-padding-top");
        document.body.classList.add("background-grey");
    }

    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
        document.body.classList.remove("background-grey");
    }

    handleCopy = () => {
        this.setState({copy: true}, () => {
            setTimeout(()=> {
                this.setState({copy: false})
            }, 1000)
        })
    }

    render() {
        const {
            state: {
                referral,
                copy
            },
            handleCopy
        } = this;
        const classCopy = cx({
            'copied': copy
        })
        return (
            <Main solid hideMenu headerUserLogin headerClose>
                <div className="p-booking-success">
                    <div className="container">
                        <div className="booking-success-image">
                            <img src={ bookingSuccessImage } alt="Banner Booking Success"/>
                        </div>
                        <div className="booking-success-description">
                            <h3 className="title">Congratulation Bryan!<br/>You have succesfully rent a car with TRAC To Go</h3>
                            <div className="booking-detail">
                                <p>Your booking Number is <strong>A09E82093U</strong></p>
                                <p>Detail of your booking and an Email Verification is sent to <strong>barry@gmail.com</strong></p>
                                <p>Please verify your email to get more benefit from TRAC</p>
                            </div>
                            <div className="referral-code">
                                <p><strong>Referral Code:</strong></p>
                                <p>Share this code to your friend. When they use is, you’ll get 25% off for your next purchase. </p>
                                <div className="referral-code-box">
                                    <h3 className={classCopy}>{referral}</h3>
                                </div>
                                <div className="copy-code-button">
                                    <CopyToClipboard text={referral} onCopy={handleCopy}>
                                        <button type="button">Copy Code</button>
                                    </CopyToClipboard>
                                </div>
                            </div>
                            <div className="other-links">
                                <div className="other-links-item">
                                    <Button primary goto="/show-my-booking">Show My Booking</Button>
                                </div>
                                <div className="other-links-item">
                                    <Button outline goto="/">Back to Home</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Main>
        );
    }
}

export default withRouter(BookingSuccess);