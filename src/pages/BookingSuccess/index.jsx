/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-15 10:28:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 01:30:16
 */
// libraries
import React, { PureComponent } from "react";
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import { defaultDidMountSetStyle, defaultWillUnmountSetStyle, localStorageDecrypt } from 'helpers';
import { isPromoValidate, savePromoCode, isPaymentValidate } from "actions";
import { connect } from "react-redux";

// local components
import { Main } from "templates";
import ThisBookingImage from "./ThisBookingImage";
import ThisBookingDetail from "./ThisBookingDetail";
import ThisBookingWrapper from "./ThisBookingWrapper";

// assets & styles
import "./BookingSuccess.scss";

// import db from '../../db';

class BookingSuccess extends PureComponent {

    state = {
        bookingData: {
            bookingNumber: null,
            customer: {
                name: null,
                email: null
            },
            referral: {
                code: 'SHARE2015',
                percentage: 25
            }
        },
        user_login: null
    }

    componentWillUnmount = () => {
        localStorage.removeItem('persist:discount');
        localStorage.removeItem('selectedCar');
        localStorage.removeItem('listStockCar');
        localStorage.removeItem('CarRentalFormDisplay');
        localStorage.removeItem('_inv');
        localStorage.removeItem('_rv');
        localStorage.removeItem('_fi');
        this.props.isPromoValidate(false);
        this.props.savePromoCode(null);
        this.props.isPaymentValidate(false);
        window.location.reload();
    }
    
    getBookingData = async () => {
        let local_auth = localStorageDecrypt('_aaa', 'object');
        let reservation = localStorageDecrypt('_rv', 'object');
        let bookingData = { ...this.state.bookingData };
        bookingData.customer.name = `${local_auth.FirstName} ${local_auth.LastName}`;
        bookingData.customer.email = local_auth.EmailPersonal;
        console.log(reservation);
        bookingData.bookingNumber = reservation.details[0].ReservationId;
        this.setState({
            bookingData: bookingData
        })
    }

    componentDidMount() {
        const {
            props: {
                history: {
                    action,
                    replace
                }
            }
        } = this;
        if(action === "POP") {
            replace("/")
        }
        defaultDidMountSetStyle();
        this.getBookingData();
    }

    componentWillUnmount() {
        defaultWillUnmountSetStyle()
    }

    render() {
        const {
            state: {
                bookingData,
            },
            props: {
                match: {
                    params: {
                        id
                    }
                }
            }
        } = this;

        return (
            <Main solid hideMenu headerUserLogin headerClose>
                <ThisBookingWrapper>
                        <ThisBookingImage type={id} />
                        {
                            !_.isEmpty(bookingData) && <ThisBookingDetail bookingData={bookingData} />
                        }
                </ThisBookingWrapper>
            </Main>
        );
    }
}

const mapStateToProps = ({  }) => {
    return {}
} 

export default withRouter(
    connect(mapStateToProps, {
        isPromoValidate, 
        savePromoCode,
        isPaymentValidate
    })(BookingSuccess)
);
