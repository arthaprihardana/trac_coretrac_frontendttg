// libraries
import React from 'react';
import PropTypes from 'prop-types';

// assets
import carRentalSuccess from "assets/images/background/car-rental.svg";
import airportTransferSuccess from "assets/images/background/airport-transfer.svg";
import busRentalSuccess from "assets/images/background/bus-rental.svg";

const ThisBookingImage = ({ type }) => {
    let img;
    switch(type){
        case 'car-rental':
            img = carRentalSuccess
            break;
        case 'airport-transfer':
            img = airportTransferSuccess;
            break;
        case 'bus-rental':
            img = busRentalSuccess;
            break;
        default:
            img = carRentalSuccess
    }

    return (
        <div className="booking-success-image">
            <img src={img} alt="Banner Booking Success" />
        </div>
    )
}

ThisBookingImage.propTypes = {
    type: PropTypes.string.isRequired
}

export default ThisBookingImage;