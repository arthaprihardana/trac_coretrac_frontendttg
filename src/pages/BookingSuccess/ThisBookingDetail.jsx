/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-15 10:24:32 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 19:41:11
 */
// libraries
import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import cx from 'classnames';
import PropTypes from 'prop-types';
import lang from '../../assets/data-master/language'

// local component
import { Button } from 'atom';

class ThisBookingDetail extends Component {

    state = {
        copy: false,
        user_login: null
    }

    handleCopy = () => {
        this.setState({ copy: true }, () => {
            setTimeout(() => {
                this.setState({ copy: false })
            }, 1000)
        })
    }
    goHome = () =>{
        window.location = '/';
    };
    goDashboard = () =>{
        window.location = '/dashboard/my-booking';
    };
    render() {
        const {
            props: {
                bookingData,
            },
            state: {
                copy
            },
            handleCopy,
            goHome,
        } = this;

        const classCopy = cx({
            'copied': copy
        })

        return (
            <div className="booking-success-description">
                <h3 className="title">{lang.congrats[lang.default]} {bookingData.customer.name}!<br />{lang.youHaveSuccessfullyRent[lang.default]}</h3>
                <div className="booking-detail">
                    <p>{lang.yourBookingNumber[lang.default]} <strong>{bookingData.bookingNumber}</strong></p>
                    <p>Detail of your booking and an Email Verification is sent to <strong>{bookingData.customer.email}</strong></p>
                    <p>Please verify your email to get more benefit from TRAC</p>
                </div>
                {/*{*/}
                    {/*bookingData.referral && (*/}
                        {/*<div className="referral-code">*/}
                            {/*<p><strong>Referral Code:</strong></p>*/}
                            {/*<p>Share this code to your friend. When they use is, you’ll get {bookingData.referral.percentage}% off for your next purchase. </p>*/}
                            {/*<div className="referral-code-box">*/}
                                {/*<h3 className={classCopy}>{bookingData.referral.code}</h3>*/}
                            {/*</div>*/}
                            {/*<div className="copy-code-button">*/}
                                {/*<CopyToClipboard text={bookingData.referral.code} onCopy={handleCopy}>*/}
                                    {/*<button type="button">Copy Code</button>*/}
                                {/*</CopyToClipboard>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*)*/}
                {/*}*/}
                <div className="other-links">
                    <div className="other-links-item">
                        <Button primary onClick={this.goDashboard}>Show My Booking</Button>
                    </div>
                    <div className="other-links-item">
                        <Button outline onClick={goHome} >Back to Home</Button>
                    </div>
                </div>
            </div>
        )
    }
}

ThisBookingDetail.propTypes = {
    bookingData: PropTypes.shape({
        bookingNumber: PropTypes.string.isRequired,
        customer: PropTypes.shape({
            name: PropTypes.string.isRequired,
            email: PropTypes.string.isRequired
        }),
        referral: PropTypes.shape({
            code: PropTypes.string,
            percentage: PropTypes.number
        })
    })
}

export default withRouter(ThisBookingDetail);