import React from 'react';
import PropTypes from 'prop-types';

const ThisBookingWrapper = ({children}) => {
    return(
        <div className="p-booking-success">
            <div className="container">
                {children}
            </div>
        </div>
    )
}

ThisBookingWrapper.proptypes = {
    children: PropTypes.any.isRequired
}

export default ThisBookingWrapper;