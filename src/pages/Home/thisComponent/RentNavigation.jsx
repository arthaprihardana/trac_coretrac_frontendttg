/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-15 15:21:40 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 14:50:26
 */
import React, { PureComponent } from "react";
import { connect } from "react-redux";
import _ from 'lodash';
import { getListProduct, selectedProductServiceType, getMasterBranch } from "actions";
import { localStorageEncrypt } from "helpers";
import lang from "../../../assets/data-master/language";

class RentNavigation extends PureComponent {

    state = {
        mainSlideActive: 1,
        showFormMobile: false,
        listProduct: []
    }

    componentDidMount(){
        this.props.getListProduct();
        document.querySelectorAll(".rental-navigation")[0].classList.add('hide');
        setTimeout(function() {
            document.querySelectorAll(".rental-navigation")[0].classList.remove('hide');
        }, 850);
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.listproductsuccess !== prevProps.listproductsuccess) {
            return this.props.listproductsuccess;
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            let pr = snapshot;
            let fl = _.filter(pr, value => {
                let reg = /^[a-z|A-Z|0-9]+[^I]\s?(6|7|8){1}$/g;
                if(reg.test(value.MsProductId)) {
                    value.icon = '';
                    switch (parseInt(value.MsProductId.substr(value.MsProductId.length - 1))) {
                        case 6:
                            value.icon = 'car-rental';
                            break;
                        case 7:
                            value.icon = 'airport-transfer';
                            break;
                        case 8:
                            value.icon = 'bus-rental';
                            break;
                        default:
                            value.icon = '';
                            break;
                    }
                    return value;
                }
            });
            this.props.onChangeRent({
                index: 1,
                BusinessUnitId: fl[0].BusinessUnitId
            });
            this.props.selectedProductServiceType(fl[0]);
            localStorageEncrypt('_bu', fl[0].BusinessUnitId)
            this.setState({ listProduct: fl });
        }
    }

    handleRentNavigation = e => {
        const value = parseInt(e.target.value);
        this.setState({
            mainSlideActive: value,
            showFormMobile: true
        });
        let _removeScroll = "remove-scroll";
        if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
            _removeScroll = "remove-scroll-ios";
        }
        if (window.innerWidth < 768) {
            setTimeout(function() {
                document.body.classList.add(_removeScroll);
            }, 500);
        }
        // this.props.onChangeRent(value);
        localStorage.removeItem('listStockCar');
        this.props.onChangeRent({
            index: value,
            BusinessUnitId: this.state.listProduct[value-1].BusinessUnitId
        });
        this.props.selectedProductServiceType(this.state.listProduct[value-1]);
        localStorageEncrypt('_bu', this.state.listProduct[value-1].BusinessUnitId)
        if(value === 3) {
            this.props.getMasterBranch(this.state.listProduct[value-1].BusinessUnitId);
        }
    };

    handleHideBookingMobile = () => {
        let _removeScroll = "remove-scroll";
        if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
            _removeScroll = "remove-scroll-ios";
        }
        if (window.innerWidth < 768) {
            document.body.classList.remove(_removeScroll);
        }
        this.setState({ showFormMobile: false });
    };

    render () {

        const {
            handleRentNavigation,
            handleHideBookingMobile,
            state: { mainSlideActive, showFormMobile, listProduct }
        } = this;

        let className = "rental-navigation";
        if (showFormMobile) {
            className += " show";
        }

        return (
            <div className={className}>
                <div className="container">
                    <div className="mouse-scroll">
                        <span className="mouse-icon" />
                        {lang.scrollToExplore[lang.default]}
                    </div>
                    <div className="rental-booking-title">Booking</div>
                    <div
                        className="rental-navigation-close"
                        onClick={handleHideBookingMobile}
                    >
                        Close
                    </div>
                    <div className="rental-navigation-button">
                        {listProduct.map((value, index) => (
                            <button
                                key={index}
                                type="button"
                                className={`a-btn btn-icon ${value.icon} ${
                                    mainSlideActive === (index+1) ? " active" : ""
                                }`}
                                onClick={handleRentNavigation}
                                value={(index+1)}
                            >
                                {value.MsProductName}
                            </button>
                        ))}
                    </div>
                </div>
            </div>
        );
    }
};

const mapStateToProps = ({ product }) => {
    return product;
}

export default connect(mapStateToProps, {
    getListProduct,
    selectedProductServiceType,
    getMasterBranch
})(RentNavigation);
