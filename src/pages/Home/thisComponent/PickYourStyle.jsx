import React from "react";
import PropTypes from "prop-types";
import Slider from "react-slick";
import uuid from "uuid/v4";
import { Link } from "react-router-dom";

// style
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

// Component
import { Image } from "atom";
import car1 from "assets/images/dummy/car-1.jpg";
import car2 from "assets/images/dummy/car-2.jpg";
import car3 from "assets/images/dummy/car-3.jpg";

const PickYourStyle = (props) => {
    const { data, config } = props;

    return (
        <div className="pick-your-style">
            <Slider {...config}>
                {data.map(item => (
                    <div className="pick-your-style-item" key={uuid()}>
                        {/*<Link to={item.link}>*/}
                            {/*<Image src={item.imagePath} alt={item.alt} />*/}
                        {/*</Link>*/}
                        <div>
                            <Image src={item.imagePath} alt={item.alt} />
                        </div>
                        <div className="categories-car">
                            <h4>
                                <span>{item.categories}</span>
                            </h4>
                        </div>
                    </div>
                ))}
            </Slider>
        </div>
    );
}

PickYourStyle.defaultProps = {
    config: {
        dots: false,
        lazyLoad: false,
        infinite: true,
        autoplay: true,
        arrows: false,
        speed: 2500,
        slidesToShow: 1,
        centerMode: true,
        slidesToScroll: 1,
        autoplaySpeed: 5000
    },
    data: [
        {
            imagePath: car1,
            alt: "Image 1",
            link: "/deal/1",
            categories: "Luxury Car"
        },
        {
            imagePath: car2,
            alt: "Image 2",
            link: "/deal/2",
            categories: "Bus"
        },
        {
            imagePath: car3,
            alt: "Image 3",
            link: "/deal/3",
            categories: "Mini Bus"
        },
        {
            imagePath: car1,
            alt: "Image 4",
            link: "/deal/1",
            categories: "Categories A"
        },
        {
            imagePath: car2,
            alt: "Image 5",
            link: "/deal/2",
            categories: "Categories B"
        },
        {
            imagePath: car3,
            alt: "Image 6",
            link: "/deal/3",
            categories: "Categories C"
        }
    ]
};

PickYourStyle.propTypes = {
    config: PropTypes.object,
    data: PropTypes.arrayOf(
        PropTypes.shape({
            imagePath: PropTypes.string,
            link: PropTypes.string
        })
    )
};

export default React.memo(PickYourStyle);