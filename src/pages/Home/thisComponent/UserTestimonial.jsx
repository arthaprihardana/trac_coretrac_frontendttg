import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import uuid from "uuid/v4";
import { delay } from 'helpers';

import userImage1 from "assets/images/dummy/user-testimonial-1.jpg";
import userImage2 from "assets/images/dummy/user-testimonial-2.jpg";
import userImage3 from "assets/images/dummy/user-testimonial-3.jpg";
import userImage4 from "assets/images/dummy/user-testimonial-4.jpg";
import userImage5 from "assets/images/dummy/user-testimonial-5.jpg";
import userAvatar1 from "assets/images/dummy/user-testimonial-avatar-1.jpg";
import userAvatar2 from "assets/images/dummy/user-testimonial-avatar-2.jpg";
import userAvatar3 from "assets/images/dummy/user-testimonial-avatar-3.jpg";
import userAvatar4 from "assets/images/dummy/user-testimonial-avatar-4.jpg";
import userAvatar5 from "assets/images/dummy/user-testimonial-avatar-5.jpg";
import lang from "../../../assets/data-master/language";
class UserTestimonial extends PureComponent {
    state = { activeClass: 1 };

    handlePickUserTestimonials = e => {
        const id = parseInt(e.currentTarget.dataset.id);
        
        if (id === this.state.activeClass) {
            return false;
        }

        document.querySelectorAll(".user-testimonial-description-lists")[0].querySelectorAll('.active')[0].classList.add("hide");
        document.querySelectorAll(".user-testimonial-image-list")[0].querySelectorAll('.active')[0].classList.add("hide");

        delay(350).then(
            () =>  this.setState({ activeClass: id })
        );

    };
    render() {
        const {
            handlePickUserTestimonials,
            state: { activeClass },
            props: { data }
        } = this;
        return (
            <div className="user-testimonial">
                <div className="user-testimonial-col">
                    <h2 className="title">{lang.hearFromCostumer[lang.default]}</h2>
                    <div className="user-testimonial-avatar-lists">
                        {data.map(item => (
                            <div onClick={handlePickUserTestimonials} data-id={item.id} className={`avatar-item ${activeClass === item.id ? "active" : ""}`} key={uuid()}>
                                <img src={item.avatar} alt={item.name} />
                            </div>
                        ))}
                    </div>
                    <div className="user-testimonial-description-lists">
                        {data.map(item => (
                            <div className={`description-item ${activeClass === item.id ? "active" : ""}`} key={uuid()}>
                                <div className="name-occupation">
                                    <span className="name">{item.name}, </span>
                                    {item.occupation}
                                </div>
                                <div className="testimony-description">
                                    <i>"</i> {item.testimony}
                                    <i>"</i>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
                <div className="user-testimonial-col">
                    <div className="user-testimonial-image-list">
                        {data.map(item => (
                            <div className={`image-item ${activeClass === item.id ? "active" : ""}`} key={uuid()}>
                                <div className="box">
                                    <img src={item.image} alt={item.name} />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}

UserTestimonial.defaultProps = {
    data: [
        {
            id: 1,
            avatar: userAvatar1,
            image: userImage1,
            name: "Ruby Franklin",
            occupation: "Graphic Designer",
            testimony: "The app that works intuitively. It makes my booking experience super easy. It helps me to see my booking trips and cancel my trips very easily."
        },
        {
            id: 2,
            avatar: userAvatar2,
            image: userImage2,
            name: "Raul Stephens",
            occupation: "Graphic Designer",
            testimony: "Quasi, quo quaerat ipsa incidunt, assumenda tempore vero delectus non ipsam enim ipsum iure aspernatur libero tenetur."
        },
        {
            id: 3,
            avatar: userAvatar3,
            image: userImage3,
            name: "Kristina Bowman",
            occupation: "Graphic Designer",
            testimony: "Earum sed assumenda eos accusantium vel. Vel nihil asperiores et aliquam nobis sint amet iure veritatis odit dolorem ipsa dignissimos."
        },
        {
            id: 4,
            avatar: userAvatar4,
            image: userImage4,
            name: "Gilbert Obrien",
            occupation: "Graphic Designer",
            testimony: "Aliquam iure excepturi ipsa explicabo perferendis ducimus, illum iusto sunt blanditiis! Asperiores id quaerat impedit sit accusamus."
        },
        {
            id: 5,
            avatar: userAvatar5,
            image: userImage5,
            name: "Zane Klein",
            occupation: "Graphic Designer",
            testimony: "Rem repudiandae molestias quia iure nesciunt, id cupiditate totam culpa vitae sed minus maiores commodi."
        }
    ]
};

UserTestimonial.propTypes = {
    data: PropTypes.arrayOf(
        PropTypes.shape({
            imagePath: PropTypes.string,
            link: PropTypes.string
        })
    )
};

export default UserTestimonial;
