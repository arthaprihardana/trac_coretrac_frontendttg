import React from "react";
import PropTypes from "prop-types";
// import downloadAppImageDesktop from "assets/images/background/download-apps-banner-desktop.jpg";
// import downloadAppImageMobile from "assets/images/background/download-apps-banner-mobile.jpg";
import lang from '../../../assets/data-master/language'

const DownloadApp = (props) => {
    const {
        imageDesktop,
        imageMobile,
        title,
        caption,
        appStoreLink,
        googlePlayLink
    } = props;

    return (
        <div
            className="download-app"
            style={{ backgroundImage: `url(${imageDesktop})` }}
        >
            <div
                className="image-mobile"
                style={{ backgroundImage: `url(${imageMobile})` }}
            />
            <div className="container">
                <div className="caption">
                    <h2>{title}</h2>
                    <div
                        className="text"
                        dangerouslySetInnerHTML={{ __html: caption }}
                    />
                    <div className="apps-links">
                        <a href={appStoreLink} className="item apps-store">
                            App Store
                        </a>
                        <a href={lang.linkPlayStore[lang.default]} className="item google-play">
                            Google Play
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
}

DownloadApp.defaultProps = {
    imageDesktop: "https://dl.dropboxusercontent.com/s/wkkqict72uoxqsn/download-apps-banner-desktop.jpg",
    imageMobile: "https://dl.dropboxusercontent.com/s/0713ciqwgpyyrdv/download-apps-banner-mobile.jpg",
    title: "Start your Adventure with TRAC To Go App",
    caption:"<ul><li>Book online your Car or Airport Transfer</li><li>Track your Driver </li><li>Flexibility to Change and Cancel Reservation anytime</li></ul>",
    appStoreLink: "https://itunes.apple.com/",
    googlePlayLink: "https://play.google.com/store/"
};

DownloadApp.propTypes = {
    imageDesktop: PropTypes.string,
    imageMobile: PropTypes.string,
    title: PropTypes.string,
    caption: PropTypes.string,
    appStoreLink: PropTypes.string,
    googlePlayLink: PropTypes.string
};

export default React.memo(DownloadApp);