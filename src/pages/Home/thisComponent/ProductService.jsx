import React from "react";
import PropTypes from "prop-types";
import ProductServiceImage from "assets/images/dummy/product-service.jpg";
import { Button } from "atom";

const ProductService = (props) => {
    const {
        image,
        title,
        caption,
        buttonLink,
        buttonText,
    } = props;

    return (
        <div className="product-service">
            <div className="container">
                <div className="image">
                    <img src={image} alt={title} />
                </div>
                <div className="caption">
                    <h2>{title}</h2>
                    <p>{caption}</p>
                    <Button type="button" primary goto={buttonLink}>
                        {buttonText}
                    </Button>
                </div>
            </div>
        </div>
    );
}

ProductService.defaultProps = {
    image: ProductServiceImage,
    title: "Check out all of our products",
    caption:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    buttonLink: "/product-service",
    buttonText: "Product & Service"
};

ProductService.propTypes = {
    image: PropTypes.string,
    title: PropTypes.string,
    caption: PropTypes.string,
    buttonLink: PropTypes.string,
    buttonText: PropTypes.string
};

export default React.memo(ProductService);