/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-17 10:23:19 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 21:00:31
 */
// core
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import moment from 'moment';
import 'moment/locale/id';

// component
import { withRouter } from 'react-router-dom';
import { RentForm } from 'organisms';

// helpers
import { delay, localStorageDecrypt } from 'helpers';
import { CustomerGlobal } from 'context/RentContext';

// actions
import { getStockCarRental, getMasterCarType, getPriceCarRental, getExtrasCarRental, airportActiveInputForm, getBusRentalPrice } from "actions";

class MultipleRentForm extends PureComponent {

    state = {
        inputShow: false,
        values: null,
        extras: [],
        stock: [],
        listPrice: [],
        listPriceBus: [],
        count: 0,
        popupShow: false,
        type: null,
        modalOpen: false
    }

    componentDidMount(){
        document.querySelectorAll(".rent-form-wrapper")[0].classList.add('hide');
        setTimeout(function() {
            document.querySelectorAll(".rent-form-wrapper")[0].classList.remove('hide');
        }, 850);
    }

    // handle click global
    handleFormChanged = (overlayStatus, overlayEvent, inputShowStatus, popupShowStatus, overlayPopupFormEvent) => {
        this.props.showOverlay(overlayStatus, overlayEvent, popupShowStatus, overlayPopupFormEvent);
        this.setState({ inputShow: inputShowStatus, popupShow: popupShowStatus });
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.stockcarrentalsuccess !== prevProps.stockcarrentalsuccess) {
            return { stockcarrentalsuccess: this.props.stockcarrentalsuccess }
        }
        if(this.props.pricecarrentalsuccess !== prevProps.pricecarrentalsuccess) {
            return { price: this.props.pricecarrentalsuccess }
        }
        if(this.props.extrascarrentalsuccess !== prevProps.extrascarrentalsuccess) {
            return { extras: this.props.extrascarrentalsuccess }
        }
        if(this.props.priceBus !== prevProps.priceBus) {
            return { priceBus: this.props.priceBus }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.stockcarrentalsuccess !== undefined) {
                this.manageStock(snapshot.stockcarrentalsuccess);
            }
            if(snapshot.extras !== undefined) {
                this.manageExtras(snapshot.extras)
            }
            if(snapshot.price !== undefined) {
                if(snapshot.price.Data.length > 0) {
                    this.setState(prevState => {
                        let prices = prevState.listPrice;
                        prices.push(snapshot.price.Data[0]);
                        return { 
                            listPrice: prices
                        }
                    }, () => this.managePrice())
                }
            }
            if(snapshot.priceBus !== undefined) {
                if(Object.keys(snapshot.priceBus.Data).length > 0) {
                    this.setState(prevState => {
                        let prices = prevState.listPriceBus;
                        prices.push(snapshot.priceBus.Data);
                        return { 
                            listPriceBus: prices
                        }
                    }, () => this.managePrice())
                }
            }
        }
    }

    handleSubmit = (values, type) => {
        const {
            rentContext: {
                handler: {
                    changeValues,
		            changeTransportType
                }
            }
        } = this.props;
        this.setState({ type: type });
        switch(type){
            case 'airport-transfer':
                // url = '/transport-list/airport-transfer';
                this.props.airportActiveInputForm(true);
                this.setState({ values: values });
                this.props.getMasterCarType(values[0].BusinessUnitId);
                this.props.getExtrasCarRental({ 
                    BusinessUnitId: values[0].BusinessUnitId, 
                    BranchId: values[0].BranchId
                });
                break;
            case 'bus-rental':
                this.setState({ values: values });
                this.props.getMasterCarType(values.BusinessUnitId);
                this.props.getExtrasCarRental({ 
                    BusinessUnitId: values.BusinessUnitId
                });
                // url = '/transport-list/bus-rental';
                break;
            default:
                this.setState({ values: values });
                this.props.getMasterCarType(values.BusinessUnitId);
                this.props.getExtrasCarRental({ 
                    BusinessUnitId: values.BusinessUnitId, 
                    BranchId: values.BranchId
                });
        }
        changeValues(values);
        changeTransportType(type);
    };

    // submit car rent form
    handleCarRentSubmit = (values) => {
        this.handleSubmit(values, 'car-rental');
    }

    // submit airport transfer form
    handleAirportTransferSubmit = (values) => {
        // console.log(JSON.stringify(values));
        this.handleSubmit(values, 'airport-transfer');
    }

    // submit bus rental form
    handleBusRentalSubmit = (values) => {
        this.handleSubmit(values, 'bus-rental');
    }

    // manage stock
    manageStock = args => {
        if(args.status === 1) {
            const { values, type } = this.state;
            // console.log("args >>",args);
            // console.log("values >>",values);
            let type_service;
            switch (type) {
                case 'airport-transfer':
                    let AirportTransferFormInput = JSON.parse(localStorageDecrypt('_fi'));
                    type_service = AirportTransferFormInput.type_service;
                    let product_service = JSON.parse(AirportTransferFormInput.data[0].type_service)
                    let stockAirportTransfer = _.map(args.data, (val, key) => {
                        let newData = val;
                        newData.id = key + 1;
                        newData.extraItems = this.state.extras;
                        newData.rentInfo = {
                            "basePrice": 0,
                            "additionalPrice": 0,
                            "priceBeforeDiscount": 0,
                            "spesialPrice": 0,
                            "spesialAmounts": 3,
                            "pricePer": "day",
                            "amounts": 0,
                            "total": 0,
                            "notes": "Taxes and Fees included"
                        };
                        let params = { 
                            BusinessUnitId: values[0].BusinessUnitId,
                            VehicleTypeId: val.vehicleTypeId,
                            // Duration: values[0].RentalDuration,
                            MsAirportCode: AirportTransferFormInput.data[0].airport.value,
                            MsProductId: product_service.MsProductId,
                            ProductServiceId: product_service.ProductServiceId,
                            MsZoneId: this.props.duration.MsZoneId
                        };
                        this.props.getPriceCarRental(params);
                        return newData;
                    });
                    this.setState({
                        stock: stockAirportTransfer
                    });
                    break;
                case 'bus-rental':

                    let BusFormInput = JSON.parse(localStorageDecrypt('_fi'));
                    let stockBusRental = _.map(args.data, (val, key) => {
                        let newData = val;
                        newData.id = key + 1;
                        newData.extraItems = this.state.extras;
                        newData.rentInfo = {
                            "basePrice": 0,
                            "additionalPrice": 0,
                            "priceBeforeDiscount": 0,
                            "spesialPrice": 0,
                            "spesialAmounts": 3,
                            "pricePer": "day",
                            "amounts": 0,
                            "total": 0,
                            "notes": "Taxes and Fees included"
                        };
                        let rangeDay = moment(BusFormInput.date.endDate).diff(moment(BusFormInput.date.startDate), 'days');
                        let params = {
                            MsProductId: 'PRD0008',
                            BusinessUnitId: val.businessUnitId,
                            // BranchId: val.branchId,
                            BranchId: this.props.branchFromBus.BranchId,
                            VehicleTypeId: val.vehicleTypeId,
                            IsOutTown: this.props.duration.IsOutTown,
                            Distance: Math.ceil(this.props.distanceBus),
                            // Distance: this.props.duration.KM,
                            Date: BusFormInput.date.startDate,
                            ZoneId: this.props.duration.MsZoneId,
                            TotalDays: (rangeDay + 1)
                        };
                        this.props.getBusRentalPrice(params);
                        return newData;
                    });
                    this.setState({
                        stock: stockBusRental
                    });
                    break;
                default:   // Car Rental
                    let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
                    type_service = JSON.parse(CarRentalFormInput.type_service);
                    let stockCarRental = _.map(args.data, (val, key) => {
                        let newData = val;
                        newData.id = key + 1;
                        newData.extraItems = JSON.parse(JSON.stringify(this.state.extras));
                        newData.rentInfo = {
                            "basePrice": 0,
                            "additionalPrice": 0,
                            "priceBeforeDiscount": 0,
                            "spesialPrice": 0,
                            "spesialAmounts": 3,
                            "pricePer": "day",
                            "amounts": 0,
                            "total": 0,
                            "notes": "Taxes and Fees included"
                        }
                        let params = { 
                            BusinessUnitId: values.BusinessUnitId,
                            VehicleTypeId: val.vehicleTypeId,
                            Duration: values.RentalDuration,
                            CityId: values.CityId,
                            MsProductId: type_service.MsProductId,
                            ProductServiceId: type_service.ProductServiceId 
                        };
                        this.props.getPriceCarRental(params);
                        return newData;
                    });
                    this.setState({
                        stock: stockCarRental
                    });
                    break;
            }
        } else {
            const {
                history: { 
                    push 
                }
            } = this.props;
            const { type } = this.state;
            this.setState({
                stock: []
            });
            let url = '';
            switch(type){
                case 'airport-transfer':
                    url = '/transport-list/airport-transfer';
                    break;
                case 'bus-rental':
                    url = '/transport-list/bus-rental';
                    break;
                default:
                    url = '/transport-list/car';
                    break;
            }
            localStorage.setItem('listStockCar', JSON.stringify([]));
            delay(1000).then(() => push(url));
        }
    };

    // manage extras
    manageExtras = args => {
        const { values, type } = this.state;
        let filterExtrasBool;
        let filterExtrasCounter;
        let filterExtrasCounterWithoutAvail;
        let dataExtrasCounter;
        let dataExtrasCounterWithoutAvail;
        let dataExtrasBool;
        let dataExtras;
        
        // if(type === "bus-rental") {
        //     filterExtrasBool = _.filter(args.Data, v => v.extras !== null && v.extras.ValueType === "boolean" );
        //     filterExtrasCounter = _.filter(args.Data, (v) => v.extras !== null && v.Availability !== null);

        //     dataExtrasCounter = _.map(filterExtrasCounter, (extra, key) => {
        //         let ex = {
        //             // "id": key + 1,
        //             "ExtrasId": extra.ExtrasId,
        //             "Availability": extra.Availability,
        //             "name": extra.extras.Name,
        //             "price": extra.Price,
        //             "pricePer": "item",
        //             "amounts": 0,
        //             "total": 0,
        //             "ValueType": "counter"
        //         };
        //         return ex;
        //     });

        //     dataExtrasBool = _.map(filterExtrasBool, (extra, key) => {
        //         let ex = {
        //             // "id": key + 1,
        //             "ExtrasId": extra.ExtrasId,
        //             "Availability": extra.Availability,
        //             "name": extra.extras.Name,
        //             "price": extra.Price,
        //             "pricePer": "item",
        //             "amounts": 0,
        //             "total": 0,
        //             "ValueType": "boolean"
        //         };
        //         return ex;
        //     });

        //     let join = _.concat(dataExtrasCounter, dataExtrasBool);
        //     dataExtras = _.map(join, (extra, key) => {
        //         extra.id = key + 1;
        //         return extra;
        //     });

        //     // dataExtras = _.map(args.Data, (extra, key) => {
        //     //     let ex = {
        //     //         "id": key + 1,
        //     //         "ExtrasId": extra.ExtrasId,
        //     //         "Availability": extra.Availability,
        //     //         "name": extra.extras.Name,
        //     //         "price": extra.Price,
        //     //         "pricePer": "item",
        //     //         "amounts": 0,
        //     //         "total": 0
        //     //     };
        //     //     return ex;
        //     // });
        // } else {
            filterExtrasBool = _.filter(args.Data, v => v.extras !== null && v.extras.ValueType === "boolean" );
            filterExtrasCounter = _.filter(args.Data, (v) => v.extras !== null && v.Availability !== null && v.extras.ValueType === "counter" && v.extras.StockType === "1");
            filterExtrasCounterWithoutAvail = _.filter(args.Data, (v) => v.extras !== null && v.extras.ValueType === "counter" && v.extras.StockType === "0");

            dataExtrasCounter = _.map(filterExtrasCounter, (extra, key) => {
                let ex = {
                    // "id": key + 1,
                    "ExtrasId": extra.ExtrasId,
                    "Availability": extra.Availability,
                    "name": extra.extras.Name,
                    "price": extra.Price,
                    "pricePer": "item",
                    "amounts": 0,
                    "total": 0,
                    "ValueType": "counter",
                    "StockType": extra.extras.StockType
                };
                return ex;
            });

            dataExtrasCounterWithoutAvail = _.map(filterExtrasCounterWithoutAvail, (extra, key) => {
                let ex = {
                    // "id": key + 1,
                    "ExtrasId": extra.ExtrasId,
                    "Availability": extra.Availability,
                    "name": extra.extras.Name,
                    "price": extra.Price,
                    "pricePer": "item",
                    "amounts": 0,
                    "total": 0,
                    "ValueType": "counter",
                    "StockType": extra.extras.StockType
                };
                return ex;
            })

            dataExtrasBool = _.map(filterExtrasBool, (extra, key) => {
                let ex = {
                    // "id": key + 1,
                    "ExtrasId": extra.ExtrasId,
                    "Availability": extra.Availability,
                    "name": extra.extras.Name,
                    "price": extra.Price,
                    "pricePer": "item",
                    "amounts": 0,
                    "total": 0,
                    "ValueType": "boolean",
                    "StockType": extra.extras.StockType
                };
                return ex;
            });

            let join = _.concat(dataExtrasCounter, dataExtrasCounterWithoutAvail, dataExtrasBool);
            dataExtras = _.map(join, (extra, key) => {
                if(this.props.selectedproductservicetype.MsProductId === "PRD0006") {
                    let a = localStorageDecrypt('_fi', "object");
                    let tp = JSON.parse(a.type_service);
                    if(tp.ProductServiceId === "PSV0001") {
                        if(extra.ExtrasId === "1" || extra.ExtrasId === "7" ) {
                            extra.id = key + 1;
                            return extra
                        }
                    } else {
                        if( extra.ExtrasId !== "4" && extra.ExtrasId !== "7" ){
                            extra.id = key + 1;
                            return extra;
                        }

                    }    
                } else {
                    if( extra.ExtrasId !== "4" && extra.ExtrasId !== "1" && extra.ExtrasId !== "7"  ){
                        extra.id = key + 1;
                        return extra;
                    }

                }
            });
            dataExtras = _.filter(dataExtras, v => v !== undefined);
            
        // }
        
        this.setState({
            extras: dataExtras
        }, () => {
            switch (type) {
                case 'airport-transfer':
                    _.map(values, value => {
                        this.props.getStockCarRental(value);
                    });
                    break;
                case "bus-rental":
                    this.props.getStockCarRental(values);
                    break;
                default:
                    this.props.getStockCarRental(values);
                    break;
            }
        });
    }

    // manage price
    managePrice = () => {
        const {
            history: { 
                push 
            }
        } = this.props;
        const { listPrice, stock, listPriceBus, type } = this.state;
        let flattenStock;
        let url = '';

        if(type === 'bus-rental') {
            let price = _.uniqBy(listPriceBus, 'VehicleTypeId');
            let newStock = _.map(price, p => {
                let filter = _.filter(stock, { vehicleTypeId: p.VehicleTypeId })
                let newFilter = _.map(filter, f => {
                    f.rentInfo = {
                        "basePrice": parseInt(p.Price),
                        "additionalPrice": 0,
                        "priceBeforeDiscount": 0,
                        "spesialPrice": 0,
                        "spesialAmounts": 3,
                        "pricePer": "day",
                        "amounts": 0,
                        "total": 0,
                        "notes": "Taxes and Fees included"
                    }
                    return f;
                })
                return newFilter;
            });
            flattenStock = _.flatten(newStock);
        } else {
            let price = _.uniqBy(listPrice, 'VehicleTypeId');
            let newStock = _.map(price, p => {
                let filter = _.filter(stock, { vehicleTypeId: p.VehicleTypeId });
                let newFilter = _.map(filter, f => {
                    let oke = _.filter(f.extraItems,{ExtrasId:"1"});
                    if(oke.length>0){
                        oke[0].price =parseInt(p.configuration_price_product_retail_details[0].AdditionalPrice)
                    }
                    f.rentInfo = {
                        "basePrice": parseInt(p.configuration_price_product_retail_details[0].BasePrice),
                        "additionalPrice": parseInt(p.configuration_price_product_retail_details[0].AdditionalPrice),
                        "priceBeforeDiscount": 0,
                        "spesialPrice": 0,
                        "spesialAmounts": 3,
                        "pricePer": "day",
                        "amounts": 0,
                        "total": 0,
                        "notes": "Taxes and Fees included"
                    }
                    return f;
                });

                return newFilter;
            });
            flattenStock = _.flatten(newStock);
        }
        switch(type){
            case 'airport-transfer':
                url = '/transport-list/airport-transfer';
                break;
            case 'bus-rental':
                url = '/transport-list/bus-rental';
                break;
            default:
                url = '/transport-list/car';
                break;
        }
        // console.log('stock ==>', flattenStock);
        localStorage.setItem('listStockCar', JSON.stringify(flattenStock));
        delay(1000).then(() => push(url));
    };

    render() {
        const {
            handleFormChanged,
            handleCarRentSubmit,
            handleAirportTransferSubmit,
            handleBusRentalSubmit,
            closeModal,
            state: { inputShow, modalOpen },
            props: { mainSlideActive, businessUnitId },
        } = this;

        return (
            <div className={`rent-form-wrapper ${inputShow === true ? 'opened' : ''} ${mainSlideActive === 2 ? 'auto-height' : ''}`}>
                <div className="container">
                    <RentForm mainSlideActive={mainSlideActive} hasChanged={handleFormChanged} onSubmit={handleCarRentSubmit} carRental inputOnTop businessUnitId={businessUnitId} loading={this.props.loading} />
                    <RentForm mainSlideActive={mainSlideActive} hasChanged={handleFormChanged} onSubmit={handleAirportTransferSubmit} airportTransfer inputOnTop businessUnitId={businessUnitId} loading={this.props.loading} />
                    <RentForm mainSlideActive={mainSlideActive} hasChanged={handleFormChanged} onSubmit={handleBusRentalSubmit} busRental inputOnTop businessUnitId={businessUnitId} loading={this.props.loading} validateMinDay={this.props.validateMinDay} />
                </div>
            </div>
        );
    }
}

MultipleRentForm.defaultProps = {
    mainSlideActive: 1,
    history: {},
};

// props types
MultipleRentForm.propTypes = {
    mainSlideActive: PropTypes.number,
    history: PropTypes.object,
    businessUnitId: PropTypes.string
};

const mapStateToProps = ({ carrental, airporttransfer, busrental, masterbranch, product }) => {
    const { loading, stockcarrentalsuccess, stockcarrentalfailure, pricecarrentalsuccess, pricecarrentalfailure, extrascarrentalsuccess } = carrental;
    const { duration } = airporttransfer;
    const { priceBus, distanceBus } = busrental;
    const { branchFromBus } = masterbranch;
    const { selectedproductservicetype } = product;
    return { 
        loading, 
        stockcarrentalsuccess, 
        stockcarrentalfailure, 
        pricecarrentalsuccess, 
        pricecarrentalfailure, 
        extrascarrentalsuccess,
        duration,
        priceBus,
        distanceBus,
        branchFromBus,
        selectedproductservicetype
    };
};

export default connect(mapStateToProps, {
    getStockCarRental,
    getMasterCarType,
    getPriceCarRental,
    getExtrasCarRental,
    airportActiveInputForm,
    getBusRentalPrice
})(CustomerGlobal(withRouter(MultipleRentForm)));
