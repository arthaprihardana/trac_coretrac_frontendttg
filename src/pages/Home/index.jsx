/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-23 20:28:43 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 19:28:08
 */
import React, { PureComponent, Fragment } from "react";
import uuid from "uuid/v4";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

// plugins
import Fade from 'react-reveal/Fade';

// components
import { Main } from "templates";
import { Button, Modal } from "atom";
import { Section, CardInfo, ListBlog, MainSlider } from "molecules";
import { ListDeal } from "organisms";

// style
import "./Home.scss";

// local component
import PickYourStyle from "./thisComponent/PickYourStyle";
import UserTestimonial from "./thisComponent/UserTestimonial";
import ProductService from "./thisComponent/ProductService";
import DownloadApp from "./thisComponent/DownloadApp";
import RentNavigation from "./thisComponent/RentNavigation";
import MultipleRentForm from "./thisComponent/MultipleRentForm";

import busRentalImg from "assets/images/background/bus-rental.svg";
import bannerCarRental from "assets/images/trac/BannerCarRental.jpg";
import bannerAirport from "assets/images/trac/BannerAirport.jpg";
import bannerBus from "assets/images/trac/BannerBus.jpg";

import fortuner from "assets/images/trac/units/fortuner.png";
import ayla from "assets/images/trac/units/ayla.png";
import avanza from "assets/images/trac/units/avanza.png";
import bus_luxury from "assets/images/trac/units/luxurybus.png";
import innova from "assets/images/trac/units/innova.png";
import motor_revo from "assets/images/trac/units/motor_revo.png";
import alphard from "assets/images/trac/units/alphard.png";
import grand_max from "assets/images/trac/units/grandmax.png";
import hillux from "assets/images/trac/units/hillux.png";

import lang from "../../assets/data-master/language";
import promo1 from "../../assets/images/trac/promo/promo-1.jpg";
import testiBayu from "../../assets/images/trac/testimoni/bayu.png";
import testiBayuThmbnl from "../../assets/images/trac/testimoni/bayu_thmb.png";
import testiHelen from "../../assets/images/trac/testimoni/helen.png";
import testiHelenThmbnl from "../../assets/images/trac/testimoni/helen_thmb.png";
// import listBlogData from "assets/data-dummy/article.json"
import promo_all from '../../assets/data-master/promo'

//
import iconAksesMudah from '../../assets/images/trac/icon/akses_mudah_jaringan_luas.png'
import iconAndalTeruji from '../../assets/images/trac/icon/andal_dan_teruji.png'
import jaminanProteksi from '../../assets/images/trac/icon/jaminan_proteksi_prima.png'
import layananPelanggan from '../../assets/images/trac/icon/layanan_pelanggan_24.png'
import productService from '../../assets/images/trac/ProductService.jpg'

import { getAdjustmentRetail, getListArticle,getListPromo, checkToken } from "actions";
import db from "../../db";

class Home extends PureComponent {
    
    state = {
        rentFormActive: 1,
        showOverlay: false,
        listBlogData: [],
        listPromo: [],
        eventOverlay: () => console.log("clicked"),
        BusinessUnitId: null,
        modalOpen: false,
        MinimumDays: 0
    };

    runZendesk(){
        function isMyScriptLoaded(url) {
            if (!url) url = "https://static.zdassets.com/ekr/snippet.js?key=2e6a8434-01c1-42eb-a18f-1bb877de53b5";
            var scripts = document.getElementsByTagName('script');
            for (var i = scripts.length; i--;) {
                if (scripts[i].src == url) return true;
            }
            return false;
        }
        if(isMyScriptLoaded("https://static.zdassets.com/ekr/snippet.js?key=2e6a8434-01c1-42eb-a18f-1bb877de53b5")){

        }else{
            var done = false;
            var scr = document.createElement('script');
            scr.onload = handleLoad;
            scr.onreadystatechange = handleReadyStateChange;
            scr.onerror = handleError;
            scr.id = "ze-snippet";
            scr.src = "https://static.zdassets.com/ekr/snippet.js?key=2e6a8434-01c1-42eb-a18f-1bb877de53b5";
            document.head.appendChild(scr);
        }

        function handleLoad() {
            if (!done) {
                done = true;
            }
        }
        function handleReadyStateChange() {
            var state;
            if (!done) {
                state = scr.readyState;
                if (state === "complete") {
                    handleLoad();
                }
            }
        }
        function handleError() {
            if (!done) {
                done = true;
            }
        }
    }

    componentDidMount() {
        // const { 
        //     history: {
        //         action,
        //         push
        //     },
        //     location
        // } = this.props;
        // if(action === "POP") {
        //     window.addEventListener("popstate", () => push(location.pathname))
        // }

        window.addEventListener('resize', this.handleWindowResize);
        if(localStorage.getItem('_aaa') != null){
            this.props.checkToken();
        }
        this.props.getListArticle();
        this.props.getListPromo();
        this.runZendesk();
        this.handleWindowResize();
        this.props.getAdjustmentRetail();
        setTimeout(()=>{
            window.scrollTo({
                'behavior': 'smooth',
                'left': 0,
                'top': 0
            });
        },500)



    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.listArticleSuccess !== prevProps.listArticleSuccess) {
            return { success: this.props.listArticleSuccess }
        }
        if(this.props.listPromoSuccess !== prevProps.listPromoSuccess) {
            return { promoSuccess: this.props.listPromoSuccess }
        }
        if(this.props.checkTokenSuccess !== prevProps.checkTokenSuccess) {
            return { tokenSuccess: this.props.checkTokenSuccess }
        }
        if(this.props.checkTokenFailure !== prevProps.checkTokenFailure) {
            return { tokenFailure: this.props.checkTokenSuccess }
        }
        return null;
    };
    logoutMe = () => {
        db.table('local_auth').delete(1);
        localStorage.removeItem('_aaa');
        window.location = '/';
    };
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (snapshot !== null) {
            if (snapshot.success !== undefined) {
                this.setState({
                    listBlogData:snapshot.success.reverse()
                })
            }
            if (snapshot.promoSuccess !== undefined) {
                this.setState({
                    listPromo:snapshot.promoSuccess.reverse()
                })
            }
            if (snapshot.tokenSuccess !== undefined) {
               // console.log("check Token Success");
            }
            if (snapshot.tokenFailure !== undefined) {
                this.logoutMe();
            }
        }
    };
    handleChangeRent = ({ index, BusinessUnitId }) => {
        // this.setState({ rentFormActive: value });
        this.setState({ 
            rentFormActive: index,
            BusinessUnitId: BusinessUnitId
        });
    }

    handleWindowResize = () => {
        if (document.querySelectorAll(".main-slider").length) {
            if (window.innerWidth < 768) {
                document.querySelectorAll(".main-slider")[0].style.height = window.innerHeight + "px";
            } else {
                document.querySelectorAll(".main-slider")[0].style.height = window.innerHeight + "px";
                // document.querySelectorAll(".main-slider")[0].removeAttribute("style");
            }
        }
    }

    // change background overlay
    changeBackgroundOverlay = (status, eventFunction) => {
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': 0
        });
        this.setState({ showOverlay: status });
        this.setState({ eventOverlay: eventFunction });
    };

    closeModal = () => {
        this.setState({ modalOpen: false })
    }

    render() {
        // get state
        const {
            showOverlay,
            eventOverlay,
            rentFormActive,
            listBlogData,
            listPromo,
            BusinessUnitId,
            modalOpen
        } = this.state;

        // main slider data
        const mainSliderData = [
            {
                id: 1,
                // imageDesktop: "https://dl.dropboxusercontent.com/s/razcvxks2qxkhft/headline-1.jpg",
                imageDesktop: bannerCarRental,
                imageMobile: bannerCarRental,
                title: lang.dreamExploreDiscover[lang.default]
            },
            {
                id: 2,
                imageDesktop: bannerAirport,
                // imageDesktop: "https://dl.dropboxusercontent.com/s/z9ousftrx7880od/headline-2.jpg",
                imageMobile: bannerAirport,
                title:lang.dreamExploreDiscover[lang.default]
            },
            {
                id: 3,
                imageDesktop: bannerBus,
                // imageDesktop: "https://dl.dropboxusercontent.com/s/ylg25e5q1wnsias/headline-3.jpg",
                imageMobile: bannerBus,
                title:lang.dreamExploreDiscover[lang.default]
            }
        ];

        // list deal data
        const listDealData = [
            {
                imagePath: promo1,
                link: "/promo-detail"
            }
        ];

        // card info data
        const cardInfoData = [
            {
                iconName: "booking process",
                iconImage: iconAksesMudah,
                title: lang.fastBookingProcess[lang.default],
                description:lang.fastBookingProcessDetail[lang.default]
            },
            {
                iconName: "car subtitution",
                iconImage: iconAndalTeruji,
                title: lang.wideRangeNetworking[lang.default],
                description:lang.wideRangeNetworkingDetail[lang.default]
            },
            {
                iconName: "coverage insurance",
                iconImage: jaminanProteksi,
                title: lang.insuranceCoverageSafety[lang.default],
                description:lang.insuranceCoverageSafetyDetail[lang.default]
            },
            {
                iconName: "premium car",
                iconImage: layananPelanggan,
                title: lang.premiumQualityRental[lang.default],
                description:lang.premiumQualityRentalDetail[lang.default]
            }
        ];

        // pick your style data
        const pickYourStyleData = [
            {
                imagePath: fortuner,
                alt: "Image 1",
                link: "/deal/1",
                categories: "Toyota Fortuner"
            },
            {
                imagePath: avanza,
                alt: "Image 2",
                link: "/deal/2",
                categories: "Toyota Avanza"
            },
            {
                imagePath: innova,
                alt: "Image 3",
                link: "/deal/3",
                categories: "Toyota Innova"
            },
            {
                imagePath: ayla,
                alt: "Image 4",
                link: "/deal/1",
                categories: "Daihatsu Ayla"
            },
            {
                imagePath: alphard,
                alt: "Image Alphard",
                link: "/deal/1",
                categories: "Toyota Alphard"
            },
            {
                imagePath: grand_max,
                alt: "Image granmax",
                link: "/deal/1",
                categories: "Daihatsu Gran Max"
            },
            {
                imagePath: hillux,
                alt: "Image Hilux",
                link: "/deal/1",
                categories: "Toyota Hilux"
            },
            {
                imagePath: bus_luxury,
                alt: "Image 5",
                link: "/deal/2",
                categories: "Luxury Bus"
            },
            {
                imagePath: motor_revo,
                alt: "Image 6",
                link: "/deal/3",
                categories: "Honda Revo"
            }
        ];
        // user testimonial data
        const userTestimonialData = [
            {
                id: 1,
                avatar: testiBayuThmbnl,
                image: testiBayu,
                name: "Bayu",
                occupation: "Karyawan Swasta",
                testimony:
                    "Driver cekatan dan selalu ontime dalam melayani sehingga acara customer berjalan lancar tanpa kendala."
            },
            {
                id: 2,
                avatar: testiHelenThmbnl,
                image: testiHelen,
                name: "Ruth Helen",
                occupation: "Karyawan Swasta",
                testimony:
                    "Driver nya sangat baik & sopan. Tiba tepat waktu saat menjemput dan sikapnya sangat sopan. Senang sekali perjalanan saya diantar oleh driver tersebut."
            }
        ];

        // product and service data
        const productServiceData = {
            image: productService,
            title: lang.checkOutAllProduct[lang.default],
            caption: lang.checkOutAllProductDesc[lang.default],
            buttonLink: "/product-services",
            buttonText: lang.productService[lang.default]
        };

        // download app data
        const downloadAppData = {
            imageDesktop: "https://dl.dropboxusercontent.com/s/wkkqict72uoxqsn/download-apps-banner-desktop.jpg",
            imageMobile: "https://dl.dropboxusercontent.com/s/0713ciqwgpyyrdv/download-apps-banner-mobile.jpg",
            title: lang.startYourAdventure[lang.default],
            caption:
                "<ul><li>"+lang.bookOnlineYourCar[lang.default]+"</li><li>"+ lang.trackYourDriver[lang.default]+ "</li><li>"+lang.flexibilityChange[lang.default]+"</li></ul>",
            appStoreLink: "/",
            googlePlayLink: "/"
        };

        return (
            <Fragment>
                <Main
                    className="p-home"
                    showOverlay={showOverlay}
                    eventOverlay={eventOverlay}
                    headerUserLogin
                >
                    {/* modal bus validate day */}
                    <Modal
                        open={modalOpen}
                        onClick={this.closeModal}
                        size="small"
                        header={{
                            withCloseButton: false,
                            onClick: this.closeModal,
                            children: ''
                        }}
                        content={{
                            children: (
                                <div className="success-login-wrapper">
                                    <div className="img-success">
                                        <img src={busRentalImg} alt="" />
                                    </div>
                                    <p className="main-title">Your select date lower than minimum day</p>
                                    <p className="sub-title">The minimum days is {this.state.MinimumDays} days</p>
                                </div>
                            ),
                        }}
                        footer={{
                            position: 'center',
                            children: (
                                <div className="modal-footer-action">
                                    <Button outline onClick={this.closeModal}>Change Day? </Button>
                                </div>
                            ),
                        }}
                    />

                    {/* main section */}

                    <div className="main-slider">
                        {/* Main Slider */}
                        <MainSlider
                            data={mainSliderData}
                            mainSlideActive={rentFormActive}
                        />

                        {/* Rent Navigation */}
                        <RentNavigation onChangeRent={ this.handleChangeRent }/>

                        {/* Rent Form */}
                        <MultipleRentForm
                            mainSlideActive={rentFormActive}
                            businessUnitId={BusinessUnitId}
                            showOverlay={this.changeBackgroundOverlay}
                            validateMinDay={v => {
                                if(v) {
                                    this.setState({
                                        modalOpen: true,
                                        MinimumDays: v
                                    })
                                }
                            }}
                        />
                    </div>

                    <Fade>

                        {/* Deals Section */}
                        <Section
                            title={lang.getTheBestDeal[lang.default]}
                            className="deals-section"
                            container
                        >
                            <ListDeal data={listPromo} />
                            <Button type="button" goto="/promo" outline center>
                                {lang.seeAllDeals[lang.default]}
                            </Button>
                        </Section>

                        {/* Card Info Section */}
                        <Section
                            title={lang.whyWeDifferent[lang.default]}
                            flex
                            container
                            className="card-info-section"
                        >
                            {cardInfoData.map(value => (
                                <CardInfo
                                    key={uuid()}
                                    iconName={value.iconName}
                                    iconImage={value.iconImage}
                                    title={value.title}
                                    description={value.description}
                                />
                            ))}
                        </Section>

                        {/* Pick Your Style */}
                        <Section
                            title={lang.pickYourStyle[lang.default]}
                            className="pick-your-style-section"
                            container
                        >
                            <PickYourStyle data={pickYourStyleData} />
                            {/*<Button type="button" goto="/car" outline center>*/}
                                {/*{lang.viewAllVehicle[lang.default]}*/}
                            {/*</Button>*/}
                        </Section>

                        {/* Blog Section */}
                        <Section title={lang.blog[lang.default]} className="blog-section">
                            <div className="blog-list">
                                <div className="blog-list-row">
                                    <div className="container">
                                        {
                                            listBlogData.length>0 && (
                                                <ListBlog
                                                    image={listBlogData[0].image}
                                                    label={listBlogData[0].label}
                                                    title={listBlogData[0].title}
                                                    lead={listBlogData[0].lead}
                                                    date={listBlogData[0].date}
                                                    linkTo={'/news-detail/'+listBlogData[0].id}
                                                    linkText={lang.readMore[lang.default]}
                                                    fullWidth
                                                />
                                            )
                                        }

                                    </div>
                                </div>
                            </div>
                            <div className="blog-list-row">
                                <div className="container">
                                    {listBlogData.map((value, index) => {
                                        let blogList = "";
                                        if (index !== 0 && index <5) {
                                            blogList = (
                                                <Fragment key={uuid()}>
                                                    <ListBlog
                                                        image={value.thumbnail}
                                                        label={value.label}
                                                        title={value.title}
                                                        lead={value.lead}
                                                        date={value.date}
                                                        linkTo={'/news-detail/'+value.id}
                                                        linkText={lang.readMore[lang.default]}
                                                    />
                                                </Fragment>
                                            );
                                        }
                                        return blogList;
                                    })}
                                </div>
                                <Button
                                    type="button"
                                    goto="/news"
                                    outline
                                    center
                                >
                                    {lang.seeAllBlog[lang.default]}
                                </Button>
                            </div>
                        </Section>

                        {/* User Testimonial */}
                        <div className="user-testimonial-section">
                            <div className="container">
                                <UserTestimonial data={userTestimonialData} />
                            </div>
                        </div>

                        {/* Product & Service */}
                        <div className="product-service-section">
                            <ProductService
                                image={productServiceData.image}
                                title={productServiceData.title}
                                caption={productServiceData.caption}
                                buttonLink={productServiceData.buttonLink}
                                buttonText={productServiceData.buttonText}
                            />
                        </div>

                        {/* Download App */}
                        <div className="download-app-section">
                            <DownloadApp
                                imageDesktop={downloadAppData.imageDesktop}
                                imageMobile={downloadAppData.imageMobile}
                                title={downloadAppData.title}
                                caption={downloadAppData.caption}
                                appStoreLink={downloadAppData.appStoreLink}
                                googlePlayLink={downloadAppData.googlePlayLink}
                            />
                        </div>

                    </Fade>

                </Main>
            </Fragment>
        );
    }
}

const mapStateToProps = ({ login, product, userprofile }) => {
    const { isLogin } = login;
    const { listArticleSuccess, listPromoSuccess } = product;
    const { checkTokenSuccess, checkTokenFailure } = userprofile;
    return { isLogin,listPromoSuccess, listArticleSuccess, checkTokenSuccess, checkTokenFailure }
}

export default withRouter(
    connect(mapStateToProps, {
        getListArticle,
        getListPromo,
        checkToken,
        getAdjustmentRetail
    })(Home)
);
