import React, { PureComponent, Fragment } from "react";
import uuid from "uuid/v4";

// plugins
import Fade from 'react-reveal/Fade';

// components
import { Main } from "templates";
import { Button } from "atom";
import { Section, CardInfo, ListBlog, MainSlider } from "molecules";
import { ListDeal } from "organisms";

// style
import "./Home.scss";

// dummy image main slider
import imageMainSliderDesktop1 from "assets/images/dummy/headline-1.jpg";
import imageMainSliderDesktop2 from "assets/images/dummy/headline-2.jpg";
import imageMainSliderDesktop3 from "assets/images/dummy/headline-3.jpg";
import imageMainSliderMobile1 from "assets/images/dummy/headline-1m.jpg";
import imageMainSliderMobile2 from "assets/images/dummy/headline-2m.jpg";
import imageMainSliderMobile3 from "assets/images/dummy/headline-3m.jpg";

// dummy list deal
import listDealImage1 from "assets/images/dummy/deal-car-1.jpg";
import listDealImage2 from "assets/images/dummy/deal-car-2.jpg";
import listDealImage3 from "assets/images/dummy/deal-car-3.jpg";

// pick your style
import car1 from "assets/images/dummy/car-1.jpg";
import car2 from "assets/images/dummy/car-2.jpg";
import car3 from "assets/images/dummy/car-3.jpg";

// dummy image blog
import blogImage1 from "assets/images/dummy/blog-primary.jpg";
import blogImage2 from "assets/images/dummy/blog-secondary-1.jpg";
import blogImage3 from "assets/images/dummy/blog-secondary-2.jpg";
import blogImage4 from "assets/images/dummy/blog-secondary-3.jpg";
import blogImage5 from "assets/images/dummy/blog-secondary-4.jpg";

// dummy user testimonial
import userImage1 from "assets/images/dummy/user-testimonial-1.jpg";
import userImage2 from "assets/images/dummy/user-testimonial-2.jpg";
import userImage3 from "assets/images/dummy/user-testimonial-3.jpg";
import userImage4 from "assets/images/dummy/user-testimonial-4.jpg";
import userImage5 from "assets/images/dummy/user-testimonial-5.jpg";
import userAvatar1 from "assets/images/dummy/user-testimonial-avatar-1.jpg";
import userAvatar2 from "assets/images/dummy/user-testimonial-avatar-2.jpg";
import userAvatar3 from "assets/images/dummy/user-testimonial-avatar-3.jpg";
import userAvatar4 from "assets/images/dummy/user-testimonial-avatar-4.jpg";
import userAvatar5 from "assets/images/dummy/user-testimonial-avatar-5.jpg";

// dummy product and service
import ProductServiceImage from "assets/images/dummy/product-service.jpg";

// dummy download app
import downloadAppImageDesktop from "assets/images/background/download-apps-banner-desktop.jpg";
import downloadAppImageMobile from "assets/images/background/download-apps-banner-mobile.jpg";

// local component
import PickYourStyle from "./thisComponent/PickYourStyle";
import UserTestimonial from "./thisComponent/UserTestimonial";
import ProductService from "./thisComponent/ProductService";
import DownloadApp from "./thisComponent/DownloadApp";
import RentNavigation from "./thisComponent/RentNavigation";
import MultipleRentForm from "./thisComponent/MultipleRentForm";

class Home extends PureComponent {
    state = {
        rentFormActive: 1,
        showOverlay: false,
        eventOverlay: () => console.log("clicked")
    };

    componentDidMount() {
        this.handleWindowResize();
        window.addEventListener('resize', this.handleWindowResize);
    }

    handleChangeRent = (value) => {
        this.setState({ rentFormActive: value });
    }

    handleWindowResize = () => {
        if (document.querySelectorAll(".main-slider").length) {
            if (window.innerWidth < 768) {
                document.querySelectorAll(".main-slider")[0].style.height = window.innerHeight + "px";
            } else {
                document.querySelectorAll(".main-slider")[0].removeAttribute("style");
            }
        }
    }

    // change background overlay
    changeBackgroundOverlay = (status, eventFunction) => {
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': 0
        });
        this.setState({ showOverlay: status });
        this.setState({ eventOverlay: eventFunction });
    };

    render() {
        // get state
        const {
            showOverlay,
            eventOverlay,
            rentFormActive,
        } = this.state;

        // main slider data
        const mainSliderData = [
            {
                id: 1,
                imageDesktop: imageMainSliderDesktop1,
                imageMobile: imageMainSliderMobile1,
                title:
                    "Dream, Explore, Discover Making Memories with TRAC To Go"
            },
            {
                id: 2,
                imageDesktop: imageMainSliderDesktop2,
                imageMobile: imageMainSliderMobile2,
                title:
                    "Dream, Explore, Discover Making Memories with TRAC To Go"
            },
            {
                id: 3,
                imageDesktop: imageMainSliderDesktop3,
                imageMobile: imageMainSliderMobile3,
                title:
                    "Dream, Explore, Discover Making Memories with TRAC To Go"
            }
        ];

        // list deal data
        const listDealData = [
            {
                imagePath: listDealImage1,
                link: "/deal/1"
            },
            {
                imagePath: listDealImage2,
                link: "/deal/2"
            },
            {
                imagePath: listDealImage3,
                link: "/deal/3"
            },
            {
                imagePath: listDealImage1,
                link: "/deal/4"
            },
            {
                imagePath: listDealImage2,
                link: "/deal/2"
            },
            {
                imagePath: listDealImage3,
                link: "/deal/3"
            },
            {
                imagePath: listDealImage1,
                link: "/deal/4"
            }
        ];

        // card info data
        const cardInfoData = [
            {
                iconName: "booking process",
                title: "Fast booking process",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
            },
            {
                iconName: "car subtitution",
                title: "Car Subtitution",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
            },
            {
                iconName: "coverage insurance",
                title: "Coverage Insurance",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
            },
            {
                iconName: "premium car",
                title: "Premium Quality Car",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
            }
        ];

        // pick your style data
        const pickYourStyleData = [
            {
                imagePath: car1,
                alt: "Image 1",
                link: "/deal/1",
                categories: "Luxury Car"
            },
            {
                imagePath: car2,
                alt: "Image 2",
                link: "/deal/2",
                categories: "Bus"
            },
            {
                imagePath: car3,
                alt: "Image 3",
                link: "/deal/3",
                categories: "Mini Bus"
            },
            {
                imagePath: car1,
                alt: "Image 4",
                link: "/deal/1",
                categories: "Categories A"
            },
            {
                imagePath: car2,
                alt: "Image 5",
                link: "/deal/2",
                categories: "Categories B"
            },
            {
                imagePath: car3,
                alt: "Image 6",
                link: "/deal/3",
                categories: "Categories C"
            }
        ];

        // list blog data
        const listBlogData = [
            {
                image: blogImage1,
                label: "News",
                title: "Where to go in September",
                lead:
                    "Is there a better time to hit the road than September? The great shoulder season of travel offers prime conditions for exploring a vast array of big-name destinations from Bandung to Bali",
                date: "Jul 6, 2017",
                linkTo: "/detail-blog",
                linkText: "Read More"
            },
            {
                image: blogImage2,
                label: "News",
                title: "Lorem Ipsum dolor sit amet sid ut ima ",
                lead:
                    "Is there a better time to hit the road than September? The great shoulder season of travel offers prime conditions for exploring a vast array of big-name destinations from Bandung to Bali",
                date: "Jul 6, 2017",
                linkTo: "/detail-blog",
                linkText: "Read More"
            },
            {
                image: blogImage3,
                label: "News",
                title: "Lorem Ipsum dolor sit amet sid ut ima ",
                lead:
                    "Is there a better time to hit the road than September? The great shoulder season of travel offers prime conditions for exploring a vast array of big-name destinations from Bandung to Bali",
                date: "Jul 6, 2017",
                linkTo: "/detail-blog",
                linkText: "Read More"
            },
            {
                image: blogImage4,
                label: "News",
                title: "Lorem Ipsum dolor sit amet sid ut ima ",
                lead:
                    "Is there a better time to hit the road than September? The great shoulder season of travel offers prime conditions for exploring a vast array of big-name destinations from Bandung to Bali",
                date: "Jul 6, 2017",
                linkTo: "/detail-blog",
                linkText: "Read More"
            },
            {
                image: blogImage5,
                label: "News",
                title: "Lorem Ipsum dolor sit amet sid ut ima ",
                lead:
                    "Is there a better time to hit the road than September? The great shoulder season of travel offers prime conditions for exploring a vast array of big-name destinations from Bandung to Bali",
                date: "Jul 6, 2017",
                linkTo: "/detail-blog",
                linkText: "Read More"
            }
        ];

        // user testimonial data
        const userTestimonialData = [
            {
                id: 1,
                avatar: userAvatar1,
                image: userImage1,
                name: "Ruby Franklin",
                occupation: "Graphic Designer",
                testimony:
                    "The app that works intuitively. It makes my booking experience super easy. It helps me to see my booking trips and cancel my trips very easily."
            },
            {
                id: 2,
                avatar: userAvatar2,
                image: userImage2,
                name: "Raul Stephens",
                occupation: "Graphic Designer",
                testimony:
                    "Quasi, quo quaerat ipsa incidunt, assumenda tempore vero delectus non ipsam enim ipsum iure aspernatur libero tenetur."
            },
            {
                id: 3,
                avatar: userAvatar3,
                image: userImage3,
                name: "Kristina Bowman",
                occupation: "Graphic Designer",
                testimony:
                    "Earum sed assumenda eos accusantium vel. Vel nihil asperiores et aliquam nobis sint amet iure veritatis odit dolorem ipsa dignissimos."
            },
            {
                id: 4,
                avatar: userAvatar4,
                image: userImage4,
                name: "Gilbert Obrien",
                occupation: "Graphic Designer",
                testimony:
                    "Aliquam iure excepturi ipsa explicabo perferendis ducimus, illum iusto sunt blanditiis! Asperiores id quaerat impedit sit accusamus."
            },
            {
                id: 5,
                avatar: userAvatar5,
                image: userImage5,
                name: "Zane Klein",
                occupation: "Graphic Designer",
                testimony:
                    "Rem repudiandae molestias quia iure nesciunt, id cupiditate totam culpa vitae sed minus maiores commodi."
            }
        ];

        // product and service data
        const productServiceData = {
            image: ProductServiceImage,
            title: "Check out all of our products",
            caption:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            buttonLink: "/product-service",
            buttonText: "Product & Service"
        };

        // download app data
        const downloadAppData = {
            imageDesktop: downloadAppImageDesktop,
            imageMobile: downloadAppImageMobile,
            title: "Start your Adventure with TRAC To Go App",
            caption:
                "<ul><li>Book online your Car or Airport Transfer</li><li>Track your Driver </li><li>Flexibility to Change and Cancel Reservation anytime</li></ul>",
            appStoreLink: "https://itunes.apple.com/",
            googlePlayLink: "https://play.google.com/store/"
        };

        return (
            <Fragment>
                <Main
                    className="p-home"
                    showOverlay={showOverlay}
                    eventOverlay={eventOverlay}
                >
                    {/* main section */}
                    <div className="main-slider">
                        {/* Main Slider */}
                        <MainSlider
                            data={mainSliderData}
                            mainSlideActive={rentFormActive}
                        />

                        {/* Rent Navigation */}
                        <RentNavigation onChangeRent={ this.handleChangeRent }/>

                        {/* Rent Form */}
                        <MultipleRentForm
                            mainSlideActive={rentFormActive}
                            showOverlay={this.changeBackgroundOverlay}
                        />
                    </div>

                    <Fade>

                        {/* Deals Section */}
                        <Section
                            title="Get the best deal for you"
                            className="deals-section"
                            container
                        >
                            <ListDeal data={listDealData} />
                            <Button type="button" goto="/deals" outline center>
                                See All Deals
                            </Button>
                        </Section>

                        {/* Card Info Section */}
                        <Section
                            title="Why We Are Different"
                            flex
                            container
                            className="card-info-section"
                        >
                            {cardInfoData.map(value => (
                                <CardInfo
                                    key={uuid()}
                                    iconName={value.iconName}
                                    title={value.title}
                                    description={value.description}
                                />
                            ))}
                        </Section>

                        {/* Pick Your Style */}
                        <Section
                            title="Pick Your Style"
                            className="pick-your-style-section"
                            container
                        >
                            <PickYourStyle data={pickYourStyleData} />
                            <Button type="button" goto="/car" outline center>
                                View All Vehicles
                            </Button>
                        </Section>

                        {/* Blog Section */}
                        <Section title="Blog" className="blog-section">
                            <div className="blog-list">
                                <div className="blog-list-row">
                                    <div className="container">
                                        <ListBlog
                                            image={listBlogData[0].image}
                                            label={listBlogData[0].label}
                                            title={listBlogData[0].title}
                                            lead={listBlogData[0].lead}
                                            date={listBlogData[0].date}
                                            linkTo={listBlogData[0].linkTo}
                                            linkText={listBlogData[0].linkText}
                                            fullWidth
                                        />
                                    </div>
                                </div>
                                <div className="blog-list-row">
                                    <div className="container">
                                        {listBlogData.map((value, index) => {
                                            let blogList = "";
                                            if (index !== 0) {
                                                blogList = (
                                                    <Fragment key={uuid()}>
                                                        <ListBlog
                                                            image={value.image}
                                                            label={value.label}
                                                            title={value.title}
                                                            lead={value.lead}
                                                            date={value.date}
                                                            linkTo={value.linkTo}
                                                            linkText={
                                                                value.linkText
                                                            }
                                                        />
                                                    </Fragment>
                                                );
                                            }
                                            return blogList;
                                        })}
                                    </div>
                                    <Button
                                        type="button"
                                        goto="/blogs"
                                        outline
                                        center
                                    >
                                        See All Blogs
                                    </Button>
                                </div>
                            </div>
                        </Section>

                        {/* User Testimonial */}
                        <div className="user-testimonial-section">
                            <div className="container">
                                <UserTestimonial data={userTestimonialData} />
                            </div>
                        </div>

                        {/* Product & Service */}
                        <div className="product-service-section">
                            <ProductService
                                image={productServiceData.image}
                                title={productServiceData.title}
                                caption={productServiceData.caption}
                                buttonLink={productServiceData.buttonLink}
                                buttonText={productServiceData.buttonText}
                            />
                        </div>

                        {/* Download App */}
                        <div className="download-app-section">
                            <DownloadApp
                                imageDesktop={
                                    downloadAppData.downloadAppImageDesktop
                                }
                                imageMobile={downloadAppData.downloadAppImageMobile}
                                title={downloadAppData.title}
                                caption={downloadAppData.caption}
                                appStoreLink={downloadAppData.appStoreLink}
                                googlePlayLink={downloadAppData.googlePlayLink}
                            />
                        </div>
                    </Fade>
                </Main>
            </Fragment>
        );
    }
}

export default Home;