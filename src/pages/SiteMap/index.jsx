import React, { Component } from "react";
import { Main, Static } from "templates";
import "./SiteMap.scss";
import ListSite from "./ListSite";
import bannerImage from '../../assets/images/dummy/sitemap-cover.png';

class SiteMap extends Component {
    state = {
        menu : [
            {
                id: 'homepage',
                list: [
                    {
                        label: 'Homepage',
                        link: '/'
                    },
                    {
                        label: 'Login',
                        link: '/login'
                    },
                    {
                        label: 'Register',
                        link: '/register'
                    },
                    {
                        label: 'Forgot Password',
                        link: '/forgot-password'
                    },
                ]
            },
            {
                id: 'product',
                list: [
                    {
                        label: 'Product & Services',
                        link: '/product-services'
                    },
                    {
                        label: 'Car Reantal',
                        link: '/detail-product-rental'
                    },
                    {
                        label: 'Bus Rental',
                        link: '/detail-product-bus-rental'
                    },
                    {
                        label: 'Airport Transfer',
                        link: '/detail-product-airport'
                    },
                ]
            },
            {
                id: 'Support',
                list: [
                    {
                        label: 'Privacy Policy',
                        link: '/privacy-policy'
                    },
                    {
                        label: 'Terms and Condition',
                        link: '/terms-and-conditions'
                    },
                    {
                        label: 'FAQ',
                        link: '/faq'
                    },
                    {
                        label: 'Contact Us',
                        link: '/contact-us'
                    },
                    {
                        label: 'Our Location',
                        link: '/outlet-locations'
                    },
                ]
            },
            {
                id: 'Blog',
                list: [
                    {
                        label: 'Blog',
                        link: '/news'
                    },
                ]
            },
            {
                id: 'Promo',
                list: [
                    {
                        label: 'Promo',
                        link: '/promo'
                    },
                ]
            },
        ],
        breadCrumb: [
            {
                label: 'Home',
                link: '/'
            },
            {
                label: 'Syarat dan Ketentuan',
                link: '/tnc'
            },
            {
                label: 'Pendahuluan',
                link: '/tnc-pendahuluan'
            },
        ]
    }

    render() {
        const {menu} = this.state;
        return (
            <Main className="p-site-map">
                <Static title="Sitemap" bannerImg={bannerImage}>
                    {
                        menu.map(menu => {
                            return <ListSite key={menu.id} data={menu} />
                        })
                    }
                </Static>
            </Main>
        );
    }
}

export default SiteMap;
