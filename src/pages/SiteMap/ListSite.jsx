import React from "react";
import { Link } from 'react-router-dom';

const ListSite = ({data}) => {
    return (
        <ul className="site-list-group">
            {
                data.list.map((menu, i) => {
                    return (
                        <li key={i} className="site-list">
                            <Link to={menu.link}>{menu.label}</Link>
                        </li>
                    )
                })
            }
        </ul>
    );
};

export default ListSite;
