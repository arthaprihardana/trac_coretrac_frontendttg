import React, { PureComponent } from "react";
import { Formik, Form } from "formik";
import { delay } from 'helpers';
import { withRouter } from 'react-router-dom';

// components
import { Main } from "templates";
import { RentForm } from "organisms";
import { TracField, Text } from "atom";
import { SelectedCar, Pagination } from "molecules";

// flags
import flagId from "assets/images/flags/id.svg";
import flagEn from "assets/images/flags/en.svg";

// style
import "./TransportList.scss";

// dummy data
import listCarAPI from "assets/data-dummy/list-car.json";
import {Rupiah} from "atom";
import Button from "../../atom/Button/Button";
import ListCar from "./thisComponent/ListCar";

const listCarDummy = listCarAPI.map(car => {
    car.img = require(`../../assets/images/dummy/cars/${car.img}`);
    return car;
});

const optionSort = [
    {
        id: "lowestPrice",
        value: "Lowest Price",
        text: "Lowest Price"
    },
    {
        id: "highestPrice",
        value: "Highest Price",
        text: "Highest Price"
    }
];

const optionCurrency = [
    {
        id: "idr",
        value: "idr",
        text: "Indonesian Rupiah",
        icon: flagId,
    },
    {
        id: "usd",
        value: "usd",
        text: "US Dollars",
        icon: flagEn,
    }
];

class TransportList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            currentActivePage: 1,
            sort: {
                showItem: false,
                selectedValue: "Lowest Price"
            },
            currency: {
                showItem: false,
                selectedValue: "IDR"
            },
            listCar: listCarDummy,
            filterPrice: {
                default: {
                    min: 100000,
                    max: 10000000
                },
                actual: {
                    min: 100000,
                    max: 10000000
                }
            },
            showFilterMobile: false,
            suvType: true,
            cityCarType: false,
            luxuryType: false
        };
        this.handlePagination = this.handlePagination.bind(this);
        this.handleClickSortSelectMenu = this.handleClickSortSelectMenu.bind(this);
        this.handleItemSortClick = this.handleItemSortClick.bind(this);
        this.handleClickCurrencySelectMenu = this.handleClickCurrencySelectMenu.bind(this);
        this.handleItemCurrencyClick = this.handleItemCurrencyClick.bind(this);
    }

    componentDidMount() {
        document.body.classList.add("add-padding-top");
        document.body.classList.add("background-grey");
    }

    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
        document.body.classList.remove("background-grey");
    }

    handlePagination(index) {
        this.setState({ currentActivePage: index });
    }

    handleClickSortSelectMenu(e) {
        e.preventDefault();
        const { sort } = this.state;
        this.setState(prevState => ({
            ...prevState,
            sort: {
                ...sort,
                showItem: !prevState.sort.showItem
            }
        }));
    }

    handleItemSortClick(e, v) {
        e.preventDefault();
        this.setState(prevState => ({
            ...prevState,
            sort: {
                showItem: false,
                selectedValue: v
            }
        }));
    }

    handleClickCurrencySelectMenu(e) {
        e.preventDefault();
        const { currency } = this.state;
        this.setState(prevState => ({
            ...prevState,
            currency: {
                ...currency,
                showItem: !prevState.currency.showItem
            }
        }));
    }

    handleItemCurrencyClick(e, v) {
        e.preventDefault();
        this.setState(prevState => ({
            ...prevState,
            currency: {
                showItem: false,
                selectedValue: v
            }
        }));
    }

    onOrderChange = (value) => {
        this.setState({
            listCar: value
        })
    }

    onRemove = (id) => {
        let newListCar = [...this.state.listCar];
        let indexUpdate = newListCar.findIndex(car => car.id === id);
        newListCar[indexUpdate].rentInfo.amounts = 0;
        newListCar[indexUpdate].rentInfo.total = 0;
        this.setState({listCar: newListCar});
    }

    handleFilterPrice = value => {
        let newFilterPrice = { ...this.state.filterPrice };
        const pricePerDigit =
            (newFilterPrice.default.max - newFilterPrice.default.min) / 99;
        newFilterPrice.actual.min = Math.round(value[0] * pricePerDigit);
        newFilterPrice.actual.max = Math.round(value[1] * pricePerDigit);
        this.setState({
            filterPrice: newFilterPrice
        });
    }

    handleFilterMobile = (status) => {
        this.setState({showFilterMobile: status});
        let _removeScroll = 'remove-scroll';
        if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
            _removeScroll = 'remove-scroll-ios';
        }
        if (status) {
            document.body.classList.add(_removeScroll);
        } else {
            document.body.classList.remove(_removeScroll);
        }
    }

    // car rent on pick
    handleCarRentChanged = (values, event) => {
        console.log("event", event);
        console.log("values", values);
    }

    handleSelectCar = () => {
        const { history: { push } } = this.props;
        delay(1000).then(() => push('/order'));
    }

    render() {
        const {
            handleCarRentChanged,
            handleFilterMobile,
            state: { currentActivePage, sort, currency, filterPrice, listCar },
        } = this;

        return (
            <Main solid>

                {/* Rent Form */}
                <RentForm mainSlideActive={1} onSubmit={ handleCarRentChanged } carRental formFilterList/>

                <div className="container">
                    <div className="p-car-rental car-rental-list">
                        <div className="rental-list-container">
                            <div className={`rental-filter ${this.state.showFilterMobile ? "show" : ""}`}>
                                <Formik
                                    initialValues={{
                                        price: 50,
                                        suv: true,
                                        cityCar: false,
                                        luxuryCar: false
                                    }}
                                    onSubmit={this.handleSubmit}
                                >
                                    {({ handleChange, handleBlur, values }) => (
                                        <Form>
                                            {/* filter-header */}
                                            <div className="filter-header">
                                                <div className="title">
                                                    Filter
                                                </div>
                                                <div className="reload">
                                                    <button type="button" className="icon ic-reload" onClick={() =>console.log("clicked!")}
                                                    />
                                                </div>
                                            </div>
                                            <div className="filter-close" onClick={ () => handleFilterMobile(false) }/>
                                            <div className="filter-list">
                                                {/* filter-item */}
                                                <div className="filter-item">
                                                    <div className="filter-item-title">
                                                        <Text>PRICE</Text>
                                                        <button type="button" className="btn-clear" onClick={() => console.log("clicked!")}>
                                                            clear
                                                        </button>
                                                    </div>
                                                    <div className="filter-item-body">
                                                        <TracField type="range" onChange={value => this.handleFilterPrice(value)}/>
                                                    </div>
                                                    <div className="filter-price-range">
                                                        <div className="price">
                                                            <p className="price-text">min</p>
                                                            <p className="price-currency">
                                                                <Rupiah value={filterPrice.actual.min}/>
                                                            </p>
                                                        </div>
                                                        <div className="price">
                                                            <p className="price-text">max</p>
                                                            <p className="price-currency">
                                                                <Rupiah value={filterPrice.actual.max}/>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* filter-item */}
                                                <div className="filter-item">
                                                    <div className="filter-item-title">
                                                        <Text>CAR TYPE</Text>
                                                        <button type="button" className="btn-clear" onClick={() =>console.log("clicked!")}>
                                                            clear
                                                        </button>
                                                    </div>
                                                    <div className="filter-item-body filter-item-has-space">
                                                        <TracField
                                                            type="checkbox"
                                                            id="suv"
                                                            name="suv"
                                                            labelName="SUV"
                                                            value={values.suv}
                                                            checked={this.state.suvType}
                                                            onChange={() => this.setState({ suvType: !this.state.suvType})}
                                                            onBlur={handleBlur}
                                                        />
                                                        <TracField
                                                            type="checkbox"
                                                            id="cityCar"
                                                            name="cityCar"
                                                            labelName="City Car"
                                                            value={values.cityCar}
                                                            checked={this.state.cityCarType}
                                                            onChange={() => this.setState({ cityCarType: !this.state.cityCarType })}
                                                            onBlur={handleBlur}
                                                        />
                                                        <TracField
                                                            type="checkbox"
                                                            id="luxuryCar"
                                                            name="luxuryCar"
                                                            labelName="Luxury Car"
                                                            value={values.luxuryCar}
                                                            checked={this.state.luxuryType}
                                                            onChange={() => this.setState({luxuryType: !this.state.luxuryType})}
                                                            onBlur={handleBlur}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="footer-filter-action">
                                                <div className="col">
                                                    <Button outline>Reset</Button>
                                                </div>
                                                <div className="col">
                                                    <Button primary onClick={() => this.setState({showFilterMobile: false})}>
                                                        Done
                                                    </Button>
                                                </div>
                                            </div>
                                        </Form>
                                    )}
                                </Formik>
                            </div>
                            <div className="rental-content">
                                {/* Filter */}
                                <div className="rental-content-filter-wrapper">
                                    <div className="rental-content-filter">
                                        <div className="filter-title-mobile" onClick={ () => handleFilterMobile(true) }>
                                            Filter
                                        </div>
                                        <Formik>
                                            <React.Fragment>
                                                <div className="filter-item">
                                                    <TracField
                                                        id="sort"
                                                        type="select"
                                                        name="sort"
                                                        labelName="Sort"
                                                        showItem={ sort.showItem }
                                                        onClick={ this.handleClickSortSelectMenu }
                                                        onItemClick={ this.handleItemSortClick }
                                                        options={ optionSort }
                                                        value={ sort.selectedValue }
                                                    />
                                                </div>
                                                <div className="filter-item">
                                                    <TracField
                                                        id="sort"
                                                        type="select"
                                                        name="sort"
                                                        labelName="Currency"
                                                        showItem={ currency.showItem }
                                                        onClick={ this.handleClickCurrencySelectMenu }
                                                        onItemClick={ this.handleItemCurrencyClick }
                                                        options={ optionCurrency }
                                                        value={ currency.selectedValue }
                                                        listHasIcon
                                                    />
                                                </div>
                                            </React.Fragment>
                                        </Formik>
                                    </div>
                                </div>

                                {/* List */}
                                <ListCar data={listCar} onChange={(value)=>this.onOrderChange(value)}/>
                                <Pagination onClick={this.handlePagination} currentActivePage={currentActivePage} totalPage={3}/>
                            </div>
                        </div>
                    </div>
                </div>
                <SelectedCar data={listCar} onRemove={(value) => this.onRemove(value)} onSubmit={this.handleSelectCar} />
            </Main>
        );
    }
}
export default withRouter(TransportList);