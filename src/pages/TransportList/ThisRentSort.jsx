/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-06 11:28:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-07 00:50:15
 */
import React, { Component, Fragment } from 'react';
import { Formik } from "formik";
import { TracField } from "atom";
import { connect } from "react-redux";
import { sortPriceCarRental } from "actions";
import lang from "../../assets/data-master/language"

const optionSort = [
    {
        id: "asc",
        value: lang.lowestPrice[lang.default],
        text: lang.lowestPrice[lang.default]
    },
    {
        id: "desc",
        value: "Highest Price",
        text: lang.highestPrice[lang.default]
    }
];

class ThisRentSort extends Component {
    state = {
        sort: {
            showItem: false,
            selectedValue: lang.lowestPrice[lang.default]
        },
        currency: {
            showItem: false,
            selectedValue: "IDR"
        }
    }

    componentDidMount = () => {
        const { sort: { selectedValue } } = this.state;
        this.props.sortPriceCarRental(selectedValue);
    }

    handleItemSortClick = (e, v) => {
        e.preventDefault();
        this.setState({
            sort: {
                showItem: false,
                selectedValue: v
            }
        }, () => this.props.sortPriceCarRental(v));
    }

    handleClickSortSelectMenu = () => {
        const {sort} = this.state;
        this.setState({
            sort: {
                ...sort,
                showItem: !sort.showItem
            }
        });
    }

    handleClickCurrencySelectMenu = (e) => {
        const { currency } = this.state;
        this.setState({
            currency: {
                ...currency,
                showItem: !currency.showItem
            }
        });
    }

    handleItemCurrencyClick = (e, v) => {
        this.setState({
            currency: {
                showItem: false,
                selectedValue: v
            }
        });
    }

    showMobileFilter = () => {
        this.props.showMobileFilter(true)
    }

    render() {
        const {
            state: {
                sort
            },
            handleItemSortClick,
            handleClickSortSelectMenu,
            showMobileFilter
        } = this;

        return (
            <div className="rental-content-filter-wrapper">
                <div className="rental-content-filter">
                    <div className="filter-title-mobile" onClick={showMobileFilter}>Filter</div>
                    <Formik>
                        <Fragment>
                            <div className="filter-item">
                                <TracField
                                    id="sort"
                                    type="select"
                                    name="sort"
                                    labelName={lang.sort[lang.default]}
                                    showItem={sort.showItem}
                                    onClick={handleClickSortSelectMenu}
                                    onItemClick={handleItemSortClick}
                                    options={optionSort}
                                    value={sort.selectedValue}
                                />
                            </div>
                            {/* <div className="filter-item">
                                <TracField
                                    id="sort"
                                    type="select"
                                    name="sort"
                                    labelName="Currency"
                                    showItem={currency.showItem}
                                    onClick={handleClickCurrencySelectMenu}
                                    onItemClick={handleItemCurrencyClick}
                                    options={optionCurrency}
                                    value={currency.selectedValue}
                                    listHasIcon
                                />
                            </div> */}
                        </Fragment>
                    </Formik>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({ carrental }) => {
    const { sortprice } = carrental;
    return { sortprice }
}

export default connect(mapStateToProps, {
    sortPriceCarRental
})(ThisRentSort);