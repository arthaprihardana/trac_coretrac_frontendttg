/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-06 14:00:16 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 15:03:01
 */
// libraries
import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { delay } from 'helpers';
import _ from "lodash";

// local component
import { Button, Modal } from "atom";
import { SelectedCar } from "molecules";

// helpers
import { localStorageDecrypt } from "helpers";

// assets
import notEnoughImg from "assets/images/background/bus-no-enough.svg";

class ThisRentSummary extends Component {
    state = {
        modalBusNotEnough: false,
        listTransport: [],
        passangerBus: {
            amount: 70,
            maxAmount: 100
        },
        loading: false,
        totalPassanger: 0
    }

    componentDidMount = () => {
        let stockCarRental = JSON.parse(localStorage.getItem('listStockCar'));
        this.setState({
            listTransport: stockCarRental
        });
    }

    continueNotEnough = () => {
        const {
            type,
            history: {
                push
            }
        } = this.props;

        this.setState({
            modalBusNotEnough: false
        }, () => {
            delay(1000).then(() => push(`/order/${type}`));
        })
    }

    handleSelectCar = (selectedData) => {
        const {
            type,
            history: {
                push
            },
        } = this.props;
        this.setState({ loading: true });

        if (type === 'bus-rental') {
            let TotalSeat = _.reduce(selectedData, (sum, n) => sum + n.totalSeat , 0);
            let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
            if(TotalSeat < CarRentalFormInput.passengers) {
                this.setState({ modalBusNotEnough: true, loading: false, totalPassanger: CarRentalFormInput.passengers })
            } else {
                let newData = _.forEach(selectedData, (v, k) => { 
                    v.selectedId = k;
                    return v;
                });
                localStorage.setItem('selectedCar', JSON.stringify(newData));
                delay(2000).then(() => {
                    this.setState({ loading: false });
                    push(`/order/${type}`)
                });
            }
        } else {
            let newData = _.forEach(selectedData, (v, k) => { 
                v.selectedId = k;
                return v;
            });
            localStorage.setItem('selectedCar', JSON.stringify(newData));
            delay(2000).then(() => {
                this.setState({ loading: false });
                push(`/order/${type}`)
            });
        }
    }

    onRemoveCar = (id) => {
        let newListCar = [...this.props.listTransport];
        let indexUpdate = newListCar.findIndex(car => car.id === id);
        newListCar[indexUpdate].rentInfo.amounts = newListCar[indexUpdate].rentInfo.amounts - 1;
        newListCar[indexUpdate].rentInfo.total =  newListCar[indexUpdate].rentInfo.total - newListCar[indexUpdate].rentInfo.basePrice;
        
        this.props.removeCar(newListCar)
    }

    closeModal = () => {
        this.setState({ modalBusNotEnough: false })
    }

    render() {
        const {
            props: {
                type,
                listTransport
            },
            state: {
                modalBusNotEnough,
                passangerBus,
                loading,
                totalPassanger
            },
            continueNotEnough,
            handleSelectCar,
            onRemoveCar,
            closeModal
        } = this;
        
        let passangerBusObj = {};
        if (type !== 'bus-rental') {
            passangerBusObj = {}
        }
        
        return (
            <Fragment>
                <SelectedCar type={type} data={listTransport} onRemove={onRemoveCar} onSubmit={handleSelectCar} passangerBus={passangerBusObj} isLoading={loading} />
                <Modal
                    open={modalBusNotEnough}
                    onClick={closeModal}
                    size="small"
                    header={{
                        withCloseButton: false,
                        onClick: closeModal,
                        children: ''
                    }}
                    content={{
                        children: (
                            <div className="success-login-wrapper">
                                <div className="img-success">
                                    <img src={notEnoughImg} alt="" />
                                </div>
                                <p className="main-title">Seat may not be enough</p>
                                <p className="sub-title">The buses that you order have only {totalPassanger} total seats,<br />still want to continue?</p>
                            </div>
                        ),
                    }}
                    footer={{
                        position: 'center',
                        children: (
                            <div className="modal-footer-action">
                                <Button outline onClick={closeModal}>Add Bus</Button>
                                {/* <Button primary onClick={continueNotEnough}>Continue</Button> */}
                            </div>
                        ),
                    }}
                />
            </Fragment>
        )
    }
}

export default withRouter(ThisRentSummary);