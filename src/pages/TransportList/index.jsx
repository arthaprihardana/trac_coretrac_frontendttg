/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-17 11:11:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 20:15:11
 */
import React, { Component } from "react";
import { delay, removeScrollOnPopup } from 'helpers';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import moment from 'moment';
import 'moment/locale/id';

// components
import { Main } from "templates";
// local component
import ThisRentForm from "./ThisRentForm";
import ThisRentFilter from "./ThisRentFilter";
import ThisRentSummary from "./ThisRentSummary";
import ThisTransportWrapper from "./ThisTransportWrapper";
import ThisRentContent from "./ThisRentContent";

// actions
import { getMasterCarType, validateTotalCartUnitVehicle, selectedCarOrderCarRental, getPriceCarRental, getStockCarRental, getExtrasCarRental, isPromoValidate, savePromoCode } from "actions";

// helpers
import { localStorageDecrypt } from "helpers";

// style
import "./TransportList.scss";
import ThisTNC from "./ThisTNC";
import { RENTAL_TIMEBASE } from "../../constant";

class TransportList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            limit: 3,
            dataTNC:'',
            currentActivePage: 1,
            totalPage: 0,
            sort: {
                showItem: false,
                selectedValue: "Lowest Price"
            },
            currency: {
                showItem: false,
                selectedValue: "IDR"
            },
            originalStock: [],
            allStock: [],
            listStockCar: [],
            filterPrice: {
                default: {
                    min: 100000,
                    max: 10000000
                },
                actual: {
                    min: 100000,
                    max: 10000000
                }
            },
            carType: [],
            showFilterMobile: false,
            clearCarAmount: null,
            totalCar: 0,
            sliderDefaultValue: [1, 100],
            showTNC: false,
            listPrice: []
        };
    }

    componentDidMount() {
        if(this.props.history.action === "POP") {

            this.props.isPromoValidate(false);
            this.props.savePromoCode(null);
        }
        this.props.isPromoValidate(false);
        this.props.savePromoCode(null);
        document.body.classList.add("add-padding-top");
        document.body.classList.add("background-grey");
        if(this.props.cartypesuccess) {
            this.setState({
                carType: this.props.cartypesuccess
            });
        }
        let CarRentalFormInput = localStorageDecrypt('_fi', 'object');
        if(CarRentalFormInput === null) {
            this.props.history.replace('/')
        }
        let stockCarRental = JSON.parse(localStorage.getItem('listStockCar'));
        if(stockCarRental !== null) {
            let paginateStock = _.chunk(stockCarRental, this.state.limit);
            let lengthPaginate = paginateStock.length;
            this.setState({
                originalStock: stockCarRental,
                totalPage: lengthPaginate,
                allStock: paginateStock,
                listStockCar: paginateStock[this.state.currentActivePage - 1]
            });
        }
    }
    routerWillLeave() { // return false to block navigation, true to allow
        const {
            history: {
                action,
                push
            }
        } = this.props;
        if (action === 'POP') {
            console.log("pop")
            this.props.history.replace('/')
            // window.location('/transport-list/car')
        }
    }
    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
        document.body.classList.remove("background-grey");
        this.routerWillLeave()
    }

    onOrderChange = (value) => {
        this.setState({
            listStockCar: value
        })
    }

    handleTNC = (status) => {
        this.setState({
            showTNC: status
        })
    };
    changeTNC = (tmp) =>{
        this.setState({
            dataTNC:tmp
        })
    };

    onRemove = (id) => {
        let newListCar = [...this.state.originalStock];
        let indexUpdate = newListCar.findIndex(car => car.id === id);
        
        newListCar[indexUpdate].rentInfo.amounts = 0;
        newListCar[indexUpdate].rentInfo.total = 0;
        let paginateStock = _.chunk(newListCar, this.state.limit);
        let lengthPaginate = paginateStock.length;
        this.setState({
            originalStock: newListCar,
            totalPage: lengthPaginate,
            allStock: paginateStock,
            listStockCar: paginateStock[this.state.currentActivePage - 1],
            clearCarAmount: id
        }, () => this.setState({ clearCarAmount: null }));
    }

    // car rent on pick
    handleCarRentChanged = (values, event, dataChange) => {
        if(dataChange === "select-address" || dataChange === "select-package" || dataChange === "select-time") {
            let BusinessUnitId = localStorageDecrypt('_bu');
            let CarRentalFormInput = localStorageDecrypt('_fi', 'object');
            this.props.getExtrasCarRental({
                BusinessUnitId: BusinessUnitId, 
                BranchId: CarRentalFormInput.city.value
            })
        }
    }

    handleSelectCar = selectedCar => {
        const { history: { push }, selectedCarOrderCarRental } = this.props;
        // localStorage.setItem('selectedcar', JSON.stringify(selectedCar));
        selectedCarOrderCarRental(selectedCar)
        delay(1000).then(() => push('/order'));
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.state.listStockCar !== prevState.listStockCar) {
            return { listStockCar: this.state.listStockCar };
        }
        if(this.state.totalCar !== prevState.totalCar) {
            return { totalCar: this.state.totalCar }
        }
        if(this.props.extrascarrentalsuccess !== prevProps.extrascarrentalsuccess) {
            return { extras: this.props.extrascarrentalsuccess }
        }
        if(this.props.stockcarrentalsuccess !== prevProps.stockcarrentalsuccess) {
            return { stockcarrentalsuccess: this.props.stockcarrentalsuccess }
        }
        if(this.props.pricecarrentalsuccess !== prevProps.pricecarrentalsuccess) {
            return { price: this.props.pricecarrentalsuccess }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.listStockCar !== undefined) {
                this.handleTotalCart(snapshot.listStockCar);
            }
            if(snapshot.totalCar !== undefined) {
                if(snapshot.totalCar < 5) {
                    this.props.validateTotalCartUnitVehicle(false);
                } else {
                    this.props.validateTotalCartUnitVehicle(true);
                }
            }
            if(snapshot.extras !== undefined) {
                this.manageExtras(snapshot.extras)
            }
            if(snapshot.stockcarrentalsuccess !== undefined) {
                this.manageStock(snapshot.stockcarrentalsuccess);
            }
            if(snapshot.price !== undefined) {
                if(snapshot.price.Data.length > 0) {
                    this.setState(prevState => {
                        let prices = prevState.listPrice;
                        prices.push(snapshot.price.Data[0]);
                        return { 
                            listPrice: prices
                        }
                    }, () => this.managePrice())
                }
            }
        }
    }

    // manage extras
    manageExtras = args => {
        // console.log('args')
        // let filterExtras = _.filter(args.Data, (v) => v.extras !== null && v.Availability !== null);
        // let dataExtras = _.map(filterExtras, (extra, key) => {
        //     let ex = {
        //         "id": key + 1,
        //         "ExtrasId": extra.ExtrasId,
        //         "Availability": extra.Availability,
        //         "name": extra.extras.Name,
        //         "price": extra.Price,
        //         "pricePer": "item",
        //         "amounts": 0,
        //         "total": 0
        //     };
        //     return ex;
        // });
        let filterExtrasBool;
        let filterExtrasCounter;
        let filterExtrasCounterWithoutAvail;
        let dataExtrasCounter;
        let dataExtrasCounterWithoutAvail;
        let dataExtrasBool;
        let dataExtras;
        filterExtrasBool = _.filter(args.Data, v => v.extras !== null && v.extras.ValueType === "boolean" );
        filterExtrasCounter = _.filter(args.Data, (v) => v.extras !== null && v.Availability !== null && v.extras.ValueType === "counter" && v.extras.StockType === "1");
        filterExtrasCounterWithoutAvail = _.filter(args.Data, (v) => v.extras !== null && v.extras.ValueType === "counter" && v.extras.StockType === "0");
        
        dataExtrasCounter = _.map(filterExtrasCounter, (extra, key) => {
            let ex = {
                // "id": key + 1,
                "ExtrasId": extra.ExtrasId,
                "Availability": extra.Availability,
                "name": extra.extras.Name,
                "price": extra.Price,
                "pricePer": "item",
                "amounts": 0,
                "total": 0,
                "ValueType": "counter"
            };
            return ex;
        });

        dataExtrasCounterWithoutAvail = _.map(filterExtrasCounterWithoutAvail, (extra, key) => {
            let ex = {
                // "id": key + 1,
                "ExtrasId": extra.ExtrasId,
                "Availability": extra.Availability,
                "name": extra.extras.Name,
                "price": extra.Price,
                "pricePer": "item",
                "amounts": 0,
                "total": 0,
                "ValueType": "counter"
            };
            return ex;
        })

        dataExtrasBool = _.map(filterExtrasBool, (extra, key) => {
            let ex = {
                // "id": key + 1,
                "ExtrasId": extra.ExtrasId,
                "Availability": extra.Availability,
                "name": extra.extras.Name,
                "price": extra.Price,
                "pricePer": "item",
                "amounts": 0,
                "total": 0,
                "ValueType": "boolean"
            };
            return ex;
        });

        let join = _.concat(dataExtrasCounter, dataExtrasCounterWithoutAvail, dataExtrasBool);
        dataExtras = _.map(join, (extra, key) => {
            extra.id = key + 1;
            return extra;
        });
        this.setState({
            extras: dataExtras
        }, () => {
            let BusinessUnitId = localStorageDecrypt('_bu');
            let CarRentalFormInput = localStorageDecrypt('_fi', 'object');
            let v = JSON.parse(CarRentalFormInput.type_service);
            let CarRentalForm = {
                BusinessUnitId: BusinessUnitId,
                BranchId: CarRentalFormInput.city.value,
                StartDate: moment(`${CarRentalFormInput.date.startDate} ${CarRentalFormInput.date.time}`).toISOString(),
                EndDate: moment(`${CarRentalFormInput.date.endDate} ${CarRentalFormInput.date.time}`).toISOString(),
                IsWithDriver: /^[a-z|A-Z|0-9]+[^I]\s?(1){1}$/.test(v.ProductServiceId) ? 0 : 1, // 0 = self driver 1 = pakai driver
                RentalDuration: parseInt(CarRentalFormInput.select_package),
                ServiceTypeId: RENTAL_TIMEBASE,  // Rental Time Based
                ValidateAttribute: 1,
                ValidateContract: 1
            };
            this.props.getStockCarRental(CarRentalForm);
        });
    }

    // manage stock
    manageStock = args => {
        if(args.status === 1) {
            let BusinessUnitId = localStorageDecrypt('_bu');
            let CarRentalFormInput = localStorageDecrypt('_fi', 'object');
            let type_service = JSON.parse(CarRentalFormInput.type_service);
            let stockCarRental = _.map(args.data, (val, key) => {
                let newData = val;
                newData.id = key + 1;
                newData.extraItems = this.state.extras;
                newData.rentInfo = {
                    "basePrice": 0,
                    "additionalPrice": 0,
                    "priceBeforeDiscount": 0,
                    "spesialPrice": 0,
                    "spesialAmounts": 3,
                    "pricePer": "day",
                    "amounts": 0,
                    "total": 0,
                    "notes": "Taxes and Fees included"
                }
                let params = { 
                    BusinessUnitId: BusinessUnitId,
                    VehicleTypeId: val.vehicleTypeId,
                    Duration: parseInt(CarRentalFormInput.select_package),
                    MsProductId: type_service.MsProductId,
                    ProductServiceId: type_service.ProductServiceId 
                };
                this.props.getPriceCarRental(params);
                return newData;
            });
            this.setState({
                stock: stockCarRental
            });
        } else {
            alert(args.message)
        }
    }

    // manage price
    managePrice = () => {
        const { listPrice, stock } = this.state;
        let price = _.uniqBy(listPrice, 'VehicleTypeId');
        let newStock = _.map(price, p => {
            let filter = _.filter(stock, { vehicleTypeId: p.VehicleTypeId })
            let newFilter = _.map(filter, f => {
                f.rentInfo = {
                    "basePrice": parseInt(p.configuration_price_product_retail_details[0].BasePrice),
                    "additionalPrice": parseInt(p.configuration_price_product_retail_details[0].AdditionalPrice),
                    "priceBeforeDiscount": 0,
                    "spesialPrice": 0,
                    "spesialAmounts": 3,
                    "pricePer": "day",
                    "amounts": 0,
                    "total": 0,
                    "notes": "Taxes and Fees included"
                }
                return f;
            })
            return newFilter;
        });
        let flattenStock = _.flatten(newStock);
        localStorage.setItem('listStockCar', JSON.stringify(flattenStock));
        let paginateStock = _.chunk(flattenStock, this.state.limit);
        let lengthPaginate = paginateStock.length;
        this.setState({
            originalStock: flattenStock,
            totalPage: lengthPaginate,
            allStock: paginateStock,
            listStockCar: paginateStock[this.state.currentActivePage - 1]
        });
    };

    handleTotalCart = listCar => {
        // const { totalPayment, totalCar } = this.state;
        let totalPayment = 0;
        let totalCar = 0;
        // let selectedCar = [];
        _.map(listCar, (car, index) => {
            totalPayment = totalPayment + car.rentInfo.total;
            totalCar = totalCar + car.rentInfo.amounts;
            // if (car.rentInfo.amounts > 0) {
            //     selectedCar.push(car);
            // }
            this.setState({ totalCar: totalCar });
        });
    }

    onRemoveSelectedCar = (newListCar) => {
        this.props.validateTotalCartUnitVehicle(false);
        this.setState({ originalStock: newListCar });
    }

    mobileOnSubmitFilter = (status) => {
        this.setState({ showFilterMobile: status });
        removeScrollOnPopup(status);
    }

    handleTNC = (status) => {
        this.setState({
            showTNC: status
        })
    }

    render() {
        const {
            props: {
                match: {
                    params: {
                        id
                    }
                },
                loading
            },
            mobileOnSubmitFilter,
            onRemoveSelectedCar,
            onOrderChange,
            handleCarRentChanged,
            // handleFilterMobile,
            handleTNC,
            changeTNC,
            state: {
                dataTNC,
                // listStockCar, 
                showTNC,
                showFilterMobile,
                originalStock
            }
        } = this;
        // console.log('originalStock ==>', originalStock);
        return (
            <Main solid headerUserLogin>
                <ThisRentForm id={id} onSubmit={handleCarRentChanged} />
                <ThisTransportWrapper>
                    <ThisRentFilter mobileOnSubmit={mobileOnSubmitFilter} showFilterMobile={showFilterMobile} type={id} />
                    <ThisRentContent mobileOnSubmitFilter={mobileOnSubmitFilter} type={id} listCar={originalStock} onOrderChange={onOrderChange} tncChange={handleTNC} tncChangeData={changeTNC} isLoading={loading} />
                </ThisTransportWrapper>
                <ThisRentSummary listTransport={originalStock} type={id} removeCar={onRemoveSelectedCar} />
                <ThisTNC datatnc={dataTNC} show={showTNC} tncClose={handleTNC} />
            </Main>
        );
    }
}

const mapStateToProps = ({ mastercartype, carrental }) => {
    const { cartypesuccess } = mastercartype;
    const { loading, stockcarrentalsuccess, maxtotalunitcart, selectedcar, pricecarrentalsuccess, pricecarrentalfailure, extrascarrentalsuccess } = carrental;
    return { cartypesuccess, loading, stockcarrentalsuccess, maxtotalunitcart, selectedcar, pricecarrentalsuccess, pricecarrentalfailure, extrascarrentalsuccess};
}

export default withRouter(
    connect(mapStateToProps, {
        getMasterCarType,
        validateTotalCartUnitVehicle,
        selectedCarOrderCarRental,
        getPriceCarRental,
        getStockCarRental,
        getExtrasCarRental,
        isPromoValidate, 
        savePromoCode
    })(TransportList)
)
