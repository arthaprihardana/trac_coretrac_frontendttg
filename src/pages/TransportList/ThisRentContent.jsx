/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-05 23:32:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-25 17:09:31
 */
import React, { Component } from 'react';
import { Text } from "atom";
import ThisRentSort from "./ThisRentSort";
import AirportDestinationInfo from "./AirportDestinationInfo";
import ListTransport from "./ListTransport";
import { Pagination } from "molecules";
import _ from "lodash";
import { connect } from "react-redux";
import { PulseLoader } from "react-spinners";

class ThisRentContent extends Component {

    state = {
        limit: 10,
        currentActivePage: 1,
        totalPage: 0,
        originalStock: [],
        listStockCar: [],
        allStock: [],
        carType: [],
        filterPrice: {},
        airportDestinationInfo: {}
    }

    componentDidMount = () => {
        const { type } = this.props;
        if(type === 'airport-transfer') {
            let afd = JSON.parse(localStorage.getItem('AirportTransferFormDisplay'));
            this.setState({
                airportDestinationInfo: {
                    From: afd.airport,
                    To: afd.pickupCityAndAddress
                }
            })
        }
    }
    
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.listCar !== prevProps.listCar) {
            return { listCar: this.props.listCar }
        }
        if(this.props.filtercartype !== prevProps.filtercartype) {
            return { filterCarType: this.props.filtercartype }
        }
        if(this.props.filterprice !== prevProps.filterprice) {
            return { filterPrice: this.props.filterprice }
        }
        if(this.props.sortprice !== prevProps.sortprice) {
            return { sortPrice: this.props.sortprice }
        }
        return null;
    }
    
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.listCar !== undefined) {
                let stockCarRental = snapshot.listCar;
                if(stockCarRental !== null) {
                    let paginateStock = _.chunk(stockCarRental, this.state.limit);
                    let lengthPaginate = paginateStock.length;
                    this.setState({
                        originalStock: stockCarRental,
                        totalPage: lengthPaginate,
                        allStock: paginateStock,
                        listStockCar: paginateStock[this.state.currentActivePage - 1]
                    });
                }
            }
            if(snapshot.filterCarType !== undefined) {
                this.setState({ 
                    carType: snapshot.filterCarType 
                }, () => this.handleFilterByCarType())
            }
            if(snapshot.filterPrice !== undefined) {
                if(Object.keys(snapshot.filterPrice).length > 0) {
                    this.setState({
                        filterPrice: snapshot.filterPrice
                    }, () => this.handleFilterByPrice())
                }
            }
            if(snapshot.sortPrice !== undefined) {
                if(snapshot.sortPrice !== null) {
                    this.handleSortPrice(snapshot.sortPrice);
                }
            }
        }
    }

    handleSortPrice = (sort) => {
        const { limit, currentActivePage, originalStock } = this.state;
        // let stockCarRental = JSON.parse(localStorage.getItem('listStockCar'));
        let stockCarRental = originalStock;
        let newSortStock = _.orderBy(stockCarRental, ['rentInfo.basePrice'], [sort === "Lowest Price" ? 'asc' : 'desc']);
        let paginateStock = _.chunk(newSortStock, limit);
        let lengthPaginate = paginateStock.length;
        this.setState(prevState => ({
            listStockCar: [],
            totalPage: lengthPaginate,
            allStock: paginateStock,
            // currentActivePage: 1
        }), () => this.setState({ listStockCar: paginateStock[currentActivePage - 1] }));
    }

    handleFilterByCarType = () => {
        const { carType, limit, filterPrice, originalStock } = this.state;
        let filterCarType = _.filter(carType, { selected: true } );
        // let stockExisting = JSON.parse(localStorage.getItem('listStockCar'));
        let stockExisting = originalStock;
        let filterStock = [];
        // let paginateStock = _.chunk(stockExisting, limit);
        let paginateStock = [];

        if(filterCarType.length > 0) {
            if(Object.keys(filterPrice).length > 0) {
                if(filterPrice.actual.min >= filterPrice.default.min || filterPrice.actual.max <= filterPrice.default.max ) {
                    filterCarType.map(val => {
                        let f = _.filter(stockExisting, ({ rentInfo: { basePrice }, carType }) => basePrice >= filterPrice.actual.min && basePrice <= filterPrice.actual.max && carType === val.CarTypeId );
                        if(f.length > 0) {
                            f.map(v => filterStock.push(v));
                        } else {
                            filterStock = [];
                            paginateStock = [];
                        }
                    });
                }
            } else {
                filterCarType.map(val => {
                    let f = _.filter(stockExisting, { carType: val.CarTypeId });
                    if(f.length > 0) {
                        f.map(v => filterStock.push(v));
                    }
                });
            }
        } else {
            paginateStock = _.chunk(stockExisting, limit);
        }
        if(filterStock.length > 0) {
            paginateStock = _.chunk(filterStock, limit);
        }
        let lengthPaginate = paginateStock.length;
        this.setState({
            totalPage: lengthPaginate,
            allStock: paginateStock,
            currentActivePage: 1,
        }, () => this.setState({listStockCar: paginateStock.length > 0 ? paginateStock[this.state.currentActivePage - 1] : []}))
    }

    handleFilterByPrice = () => {
        const { limit, currentActivePage, carType, filterPrice, originalStock } = this.state;
        // let stockExisting = JSON.parse(localStorage.getItem('listStockCar'));
        let stockExisting = originalStock;
        let filterCarType = _.filter(carType, { selected: true } );
        let f = null;
        if(filterCarType.length > 0) {
            f = filterCarType.map(val => {
                let f = _.filter(stockExisting, ({ carType, rentInfo: { basePrice } }) => basePrice >= filterPrice.actual.min && basePrice <= filterPrice.actual.max && carType === val.CarTypeId );
                return f;
            });
            f = _.flatten(f);
        } else {
            f = _.filter(stockExisting, ({ rentInfo: { basePrice } }) => basePrice >= filterPrice.actual.min && basePrice <= filterPrice.actual.max );
        }
        let paginateStock = _.chunk(f, limit);
        let lengthPaginate = paginateStock.length;
        this.setState({
            listStockCar: [],
            totalPage: lengthPaginate,
            allStock: paginateStock,
            currentActivePage: 1,
        }, () => this.setState({listStockCar: f.length > 0 ? paginateStock[currentActivePage - 1] : []}))
    }

    handlePagination = (index) => {
        this.setState({ 
            currentActivePage: index,
            listStockCar: []
        }, () => this.setState({ listStockCar: this.state.allStock[index - 1] }));
    }

    onOrderChange = (value) => {
        this.props.onOrderChange(value)
    }

    render() {
        const {
            props: {
                mobileOnSubmitFilter,
                type,
                tncChange,
                tncChangeData,
                isLoading
            },
            state: {
                currentActivePage,
                totalPage,
                listStockCar
            },
            handlePagination
        } = this;
        
        return (
            <div className="rental-content">
                <ThisRentSort showMobileFilter={mobileOnSubmitFilter} />
                {type === 'airport-transfer' && <AirportDestinationInfo airportDestinationInfo={this.state.airportDestinationInfo} />}
                {isLoading && <div className="loading-event">
                    <PulseLoader
                        sizeUnit={"px"}
                        size={14}
                        color={'#f47d00'}
                        // color={'#1f419b'}
                        loading={isLoading}
                    />
                    <Text centered>Please wait, we are process finding car for you.</Text>
                </div> }
                { !isLoading && <ListTransport type={type} data={listStockCar} onChange={(value) => this.onOrderChange(value)} tncChange={tncChange} tncChangeData={tncChangeData} /> }
                { !isLoading && <Pagination onClick={handlePagination} currentActivePage={currentActivePage} totalPage={totalPage} /> }
            </div>
        )
    }
}

const mapStateToProps = ({ carrental }) => {
    const { filtercartype, filterprice, sortprice } = carrental;
    return { filtercartype, filterprice, sortprice }
}

export default connect(mapStateToProps, {})(ThisRentContent);