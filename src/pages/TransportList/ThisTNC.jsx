import React, { Component } from 'react';
import { Modal, Text } from "atom";

class ThisTNC extends Component {
    state = {
        termsModal: false
    }

    shouldComponentUpdate(nexProps, nextState){
        if(nexProps.show !== this.props.show){
            this.setState({
                termsModal: nexProps.show
            })
            return true;
        }
        if(nextState.termsModal !== this.state.termsModal){
            this.setState({
                termsModal: nextState.termsModal
            })
            return true;
        }
        return false;
    }
    createMarkup = (html) => {
        return {__html: html};
    };

    handleTermsModal = (type) => {
        if(type === 'close'){
            this.setState({
                termsModal: false
            }, () => {
                this.props.tncClose(false);
            })
        }
    }

    render() {
        const {
            datatnc
        } = this.props;
        const {termsModal} = this.state;
        return (
            <Modal
                open={termsModal}
                onClick={() => this.handleTermsModal('close')}
                header={{
                    withCloseButton: true,
                    onClick: () => this.handleTermsModal('close'),
                    children: <Text type="h3">Terms and Condition Insurance</Text>,
                }}
                size="small"
                content={{
                    children: (
                        <div>
                            <div dangerouslySetInnerHTML={this.createMarkup(datatnc)} />
                        </div>
                    ),
                }}
                footer={{
                    position: 'center',
                    children: false,
                }}
            />
        )
    }
}

export default ThisTNC;