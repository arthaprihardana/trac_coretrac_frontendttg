import React, { Component, Fragment } from 'react';
import {Button} from 'atom';

class BusRecomendation extends Component {
    render(){
        const {
            props: {
                recomendation
            }
        } = this;
        return(
            <Fragment>
                <p className="recomendation-label">Recommended For You</p>
                {recomendation}
                <div className="recomendation-summary">
                    <div className="sum-wrapper">
                        <div className="sum-info">
                            <p className="label">Total Payment</p>
                            <p className="value">Rp 1,025,000</p>
                        </div>
                        <div className="sum-action">
                            <Button type="button" primary onClick={()=> console.log('Book Bus')}>Book Bus</Button>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default BusRecomendation;