/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-06 01:29:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 22:33:36
 */
import React, { Component, Fragment } from 'react';
import {CarBox} from 'organisms';

// helpers
import { localStorageDecrypt, localStorageEncrypt } from "helpers";
// plugins
import Fade from 'react-reveal/Fade';
// import BusRecomendation from './BusRecomendation';

class ListTransport extends Component {
    state = {
        type_service:'',
        listCar: []
    }

    componentDidMount() {
        const { type } = this.props;

        let CarRentalFormInput = localStorageDecrypt('_fi', 'object');
        if(CarRentalFormInput !== null) {
            switch (type) {
                case 'airport-transfer':
                    this.setState({
                        type_service: JSON.parse(CarRentalFormInput.data[0].type_service)
                    });
                    break;
                case 'bus-rental':
                    this.setState({
                        type_service: { ProductServiceId: 'PSV23454' }
                    });
                    break;
                default:
                    this.setState({
                        type_service: JSON.parse(CarRentalFormInput.type_service)
                    });
                    break;
            }
        }
        if (this.props.data) {
            this.setState({
                listCar: this.props.data
            })
        }
    }

    shouldComponentUpdate(nextProps){
        if(nextProps !== this.props){
            this.setState({listCar: nextProps.data})
        }
        return true;
    }

    handleChange = (newObj) => {
        let newListCar = [...this.state.listCar];
        let indexUpdate = newListCar.findIndex(car => car.id === newObj.id);
        newListCar[indexUpdate] = newObj;
        this.props.onChange(newListCar)
    }

    render() {
        const {
            props: {
                type,
                tncChange,
                tncChangeData
            }, 
            state: {
                type_service,
                listCar
            }
        } = this;
        if (listCar !== undefined && listCar.length <= 0) return null;
        
        let carShow = listCar !== undefined && listCar.length > 0 ? listCar.map(car => {
            return (
                <Fade key={car.id}>
                    <CarBox data={car} onChange={(value) => this.handleChange(value)} type={type} type_service={type_service} tncChange={tncChange} tncChangeData={tncChangeData} />
                </Fade>
            )
        }) : <div />;

        // let busRecomendationShow = listCar.slice(0, 2).map(bus => {
        //     return (
        //         <Fade key={bus.id}>
        //             <CarBox data={bus} infoOnly type={type} />
        //         </Fade>
        //     )
        // })

        return (
            <div className="rental-content-list">
                {/* {
                    type === 'bus-rental' && (
                        <Fragment>
                            <BusRecomendation recomendation={busRecomendationShow} />
                            <p className="recomendation-label">Other Bus Options</p>
                        </Fragment>
                    )
                } */}
                {carShow}
            </div>
        )
    }
}

export default ListTransport;