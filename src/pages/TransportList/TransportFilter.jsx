/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-05 23:42:24 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 18:01:16
 */
import { Rupiah, Text, TracField, Button } from "atom";
import { Form, Formik } from "formik";
import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { filterCarTypeCarRental, filterPriceRangeCarRental } from "actions";
import lang from '../../assets/data-master/language'

class TransportFilter extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            filterPrice: {
                default: {
                    min: 100000,
                    max: 10000000
                },
                actual: {
                    min: 100000,
                    max: 10000000
                }
            },
            carType: [],
            sliderDefaultValue: [100000, 10000000]
        };
    }

    componentDidMount() {
        document.body.classList.add("add-padding-top");
        document.body.classList.add("background-grey");
        let stock = JSON.parse(localStorage.getItem('listStockCar'));
        if(stock !== null && stock.length > 0) {
            let newFilterPrice = { ...this.state.filterPrice };
            newFilterPrice.actual.min = _.minBy(stock, 'rentInfo.basePrice').rentInfo.basePrice;
            newFilterPrice.actual.max = _.maxBy(stock, 'rentInfo.basePrice').rentInfo.basePrice;
            newFilterPrice.default.min = _.minBy(stock, 'rentInfo.basePrice').rentInfo.basePrice;
            newFilterPrice.default.max = _.maxBy(stock, 'rentInfo.basePrice').rentInfo.basePrice;
            this.setState({
                filterPrice: newFilterPrice,
                sliderDefaultValue: [_.minBy(stock, 'rentInfo.basePrice').rentInfo.basePrice, _.maxBy(stock, 'rentInfo.basePrice').rentInfo.basePrice]
            })
        }
        if(this.props.cartypesuccess) {
            this.setState({
                carType: this.props.cartypesuccess
            });
        }
    }

    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
        document.body.classList.remove("background-grey");
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(this.props.cartypesuccess !== prevProps.cartypesuccess) {
            this.setState({
                carType: this.props.cartypesuccess
            });
        }
    }

    handleFilterPrice = value => {
        let stock = JSON.parse(localStorage.getItem('listStockCar'));
        if(stock !== null && stock.length > 0) {
            const { filterPriceRangeCarRental } = this.props;
            let newFilterPrice = { ...this.state.filterPrice };
            filterPriceRangeCarRental({})
            newFilterPrice.actual.min = Math.round(value[0]);
            newFilterPrice.actual.max = Math.round(value[1]);
            this.setState({
                filterPrice: newFilterPrice,
                sliderDefaultValue: [Math.round(value[0]), Math.round(value[1])]
            }, () => filterPriceRangeCarRental(this.state.filterPrice));
        }
    }

    handleClearFilterPrice = () => {
        const { filterPriceRangeCarRental } = this.props;
        let newFilterPrice = { ...this.state.filterPrice };
        filterPriceRangeCarRental([]);
        newFilterPrice.actual.min = Math.round(newFilterPrice.default.min);
        newFilterPrice.actual.max = Math.round(newFilterPrice.default.max);
        this.setState({
            filterPrice: newFilterPrice,
            sliderDefaultValue: [Math.round(newFilterPrice.default.min), Math.round(newFilterPrice.default.max)]
        }, () => filterPriceRangeCarRental(this.state.filterPrice));
    }

    handleFilterMobile = (status) => {
        this.props.mobileOnSubmit(status)
    }

    handleSelectedCarType = (index) => {
        const { carType } = this.state;
        const { filterCarTypeCarRental } = this.props;
        carType[index].selected = !carType[index].selected;
        filterCarTypeCarRental([]);
        this.setState({
            carType: carType 
        },() => {
            this.forceUpdate();
            filterCarTypeCarRental(carType);
        });
    }

    handleClearFilterByCarType = () => {
        const { filterCarTypeCarRental, filterPriceRangeCarRental, cartypesuccess } = this.props;
        const { filterPrice } = this.state;
        let carType = _.map(cartypesuccess, v => {
            v.selected = false
            return v;
        });
        filterCarTypeCarRental([])
        filterPriceRangeCarRental({})
        this.setState({ 
            carType: carType,
            filterPrice: filterPrice,
            sliderDefaultValue: [filterPrice.actual.min, filterPrice.actual.max]
        }, () => {
            this.forceUpdate();
            filterPriceRangeCarRental(filterPrice)
        })
    }

    handleClearFilterAll = () => {
        const { filterCarTypeCarRental, filterPriceRangeCarRental, cartypesuccess } = this.props;
        let carType = _.map(cartypesuccess, v => {
            v.selected = false
            return v;
        });
        let newFilterPrice = { ...this.state.filterPrice };
        filterPriceRangeCarRental([]);
        newFilterPrice.actual.min = Math.round(newFilterPrice.default.min);
        newFilterPrice.actual.max = Math.round(newFilterPrice.default.max);
        filterCarTypeCarRental([])
        filterPriceRangeCarRental({})
        this.setState({ 
            carType: carType,
            filterPrice: newFilterPrice,
            sliderDefaultValue: [Math.round(newFilterPrice.default.min), Math.round(newFilterPrice.default.max)]
        }, () => {
            this.forceUpdate()
        })
    }
    
    render() {
        const {
            props: {
                type
            },
            state: { 
                filterPrice,
                carType,
                sliderDefaultValue
            },
            handleFilterMobile,
        } = this;

        return (
            <Formik
                initialValues={{
                    price: 50,
                    suv: true,
                    cityCar: false,
                    luxuryCar: false
                }}
                onSubmit={this.handleSubmit}
            >
                {({ handleChange, handleBlur, values }) => (
                    <Form>
                        {/* filter-header */}
                        <div className="filter-header">
                            <div className="title">
                                {lang.price[lang.default]}
                            </div>
                            <div className="reload">
                                <button type="button" className="icon ic-reload" onClick={this.handleClearFilterAll} />
                            </div>
                        </div>
                        <div className="filter-close" onClick={() => handleFilterMobile(false)} />
                        <div className="filter-list">
                            <Fragment>
                                {/* filter-item */}
                                {/* { type !== 'bus-rental' &&  */}
                                <div className="filter-item">
                                    <div className="filter-item-title">
                                        <Text> {lang.price[lang.default]}</Text>
                                        <button type="button" className="btn-clear" onClick={this.handleClearFilterPrice}> {lang.clear[lang.default]}</button>
                                    </div>
                                    <div className="filter-item-body">
                                        <TracField 
                                            type="range" 
                                            min={filterPrice.default.min}
                                            max={filterPrice.default.max}
                                            onChange={value => this.handleFilterPrice(value)}
                                            defaultValue={sliderDefaultValue} />
                                    </div>
                                    <div className="filter-price-range">
                                        <div className="price">
                                            <p className="price-text">min</p>
                                            <p className="price-currency">
                                                <Rupiah value={filterPrice.actual.min} />
                                            </p>
                                        </div>
                                        <div className="price">
                                            <p className="price-text">max</p>
                                            <p className="price-currency">
                                                <Rupiah value={filterPrice.actual.max} />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                {/* } */}
                                {/* filter-item */}
                                <div className="filter-item">
                                    <div className="filter-item-title">
                                        <Text>{lang.carType[lang.default]}</Text>
                                        <button type="button" className="btn-clear" onClick={this.handleClearFilterByCarType}>
                                            clear
                                        </button>
                                    </div>
                                    <div className="filter-item-body filter-item-has-space">
                                        { carType.length > 0 && carType.map((value, index) => (
                                            <TracField
                                                key={index}
                                                type="checkbox"
                                                id={value.CarTypeId}
                                                name="suv"
                                                labelName={value.Name}
                                                // value={values.suv}
                                                checked={value.selected}
                                                onChange={() => this.handleSelectedCarType(index)}
                                                onBlur={handleBlur}
                                            />
                                        )) }
                                    </div>
                                </div>
                            </Fragment>
                        </div>
                        <div className="footer-filter-action">
                            <div className="col">
                                <Button outline onClick={this.handleClearFilterAll}>Reset</Button>
                            </div>
                            <div className="col">
                                <Button primary onClick={() => handleFilterMobile(false)}>{lang.done[lang.default]}</Button>
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        );
    }
}

const mapStateToProps = ({ mastercartype }) => {
    const { cartypesuccess } = mastercartype;
    return { cartypesuccess }
}

export default connect(mapStateToProps, {
    filterCarTypeCarRental,
    filterPriceRangeCarRental
})(TransportFilter);