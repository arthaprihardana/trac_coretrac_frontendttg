/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-13 12:25:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-13 12:30:50
 */
import React from 'react';
import { Icon } from 'atom';

const AirportDestinationInfo = ({ airportDestinationInfo }) => {
    return (
        <div className="airport-destination-info">
            <p className="content-info"><span className="label-info">From: </span>{airportDestinationInfo.From}</p>
            <div className="icon-to">
                <Icon name="arrow-right-orange" />
            </div>
            <p className="content-info"><span className="label-info">To: </span>{airportDestinationInfo.To}</p>
        </div>
    )
}

export default AirportDestinationInfo;