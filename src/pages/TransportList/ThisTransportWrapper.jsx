import React from 'react';

const ThisTransportWrapper = ({ children }) => {
    return (
        <div className="container">
            <div className="p-car-rental car-rental-list">
                <div className="rental-list-container">
                    {children}
                </div>
            </div>
        </div>
    )
}

export default ThisTransportWrapper;