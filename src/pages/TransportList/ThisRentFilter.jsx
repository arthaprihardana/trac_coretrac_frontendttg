/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-05 23:41:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-06 13:39:21
 */
import React, { Component } from 'react';
import TransportFilter from "./TransportFilter";
import cx from 'classnames';

class ThisRentFilter extends Component {
    render() {
        const {
            props: {
                showFilterMobile,
                mobileOnSubmit,
                type
            }
        } = this;
        
        const classFilterMobile = cx('rental-filter', {
            'show': showFilterMobile
        })

        return (
            <div className={classFilterMobile}>
                <TransportFilter mobileOnSubmit={mobileOnSubmit} type={type} />
            </div>
        )
    }
}

export default ThisRentFilter;