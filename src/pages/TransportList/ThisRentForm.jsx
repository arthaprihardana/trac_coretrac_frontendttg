/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-09 23:59:02 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-02-09 23:59:02 
 */
import React from 'react';
import { RentForm } from "organisms";

const ThisRentForm = ({id, onSubmit}) => {
    switch(id){
        case 'airport-transfer':
            return <RentForm mainSlideActive={2} onSubmit={onSubmit} airportTransfer formFilterList />;
        case 'bus-rental':
            return <RentForm mainSlideActive={3} onSubmit={onSubmit} busRental formFilterList />;
        default: 
            return <RentForm mainSlideActive={1} onSubmit={onSubmit} carRental formFilterList />;
    }
}

export default ThisRentForm;