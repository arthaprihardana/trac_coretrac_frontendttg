/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-24 14:15:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-31 01:03:10
 */
import React, { PureComponent, Component } from 'react';
import {CarBox} from 'organisms';
import _ from 'lodash';

// plugins
import Fade from 'react-reveal/Fade';

class ListCar extends Component {
    state = {
        listCar: []
    }

    // componentDidMount() {
    //     if (this.props.data) {
    //         this.setState({
    //             listCar: this.props.data,
    //         })
    //     }
    // }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.data !== prevProps.data) {
            return this.props.data
        }
        return null;
    }
    
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            this.setState({ listCar: snapshot });
        }
    }

    handleChange = (newObj) => {
        let newListCar = [...this.state.listCar];
        let indexUpdate = newListCar.findIndex(car => car.id === newObj.id);
        newListCar[indexUpdate] = newObj;
        this.props.onChange(newListCar)
    }

    render() {
        const { listCar } = this.state;
        const { clearCarAmount } = this.props;
        if (listCar.length <= 0) return null;

        let carShow = listCar.map((car, index) => {
            return (
                <Fade key={index}>
                    <CarBox data={car} onChange={(value) => this.handleChange(value)} clearCarAmount={clearCarAmount} />
                </Fade>
            )
        })

        return (
            <div className="rental-content-list">
                {carShow}
            </div>
        )
    }
}

export default ListCar;
