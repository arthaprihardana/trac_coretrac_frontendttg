import React, { PureComponent } from "react";
import { debounce } from "lodash";
import { Formik, Form } from "formik";
import * as Yup from "yup";

// components
import { Text, Button, TracField } from "atom";
import { Slideshow, ModalSuccess } from "molecules";
import { Auth } from "templates";

// validation schema
const AuthSchema = Yup.object().shape({
    email: Yup.string()
        .email("Email should be valid!")
        .required("Email is required!")
});

class ForgotPassword extends PureComponent {
    constructor() {
        super();
        this.state = {
            width: window.innerWidth,
            height: window.innerHeight,
            modalOpen: false
        };

        this.handleWindowResize = this.handleWindowResize.bind(this);
        this.handleWindowResize = debounce(this.handleWindowResize, 300);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleWindowResize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowResize);
    }

    handleSubmit(values) {
        console.log("form.values:", values);
        this.setState({ modalOpen: true });
    }

    handleModalClose(e) {
        e.stopPropagation();
        this.setState({ modalOpen: false });
    }

    handleWindowResize() {
        this.setState({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }

    renderForm() {
        const { modalOpen } = this.state;

        return (
            <React.Fragment>
                <div className="text">
                    <Text type="h2">Forgot Password</Text>
                </div>
                <Formik
                    initialValues={{
                        email: ""
                    }}
                    validationSchema={AuthSchema}
                    onSubmit={this.handleSubmit}
                >
                    {({ isSubmitting, handleChange, handleBlur, values }) => (
                        <React.Fragment>
                            <ModalSuccess
                                open={modalOpen}
                                size="medium"
                                goto="/"
                                onCloseModal={this.handleModalClose}
                            />
                            <Form>
                                <TracField
                                    id="email"
                                    name="email"
                                    type="text"
                                    labelName="Email"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.email}
                                />
                                <Button
                                    type="submit"
                                    disabled={isSubmitting}
                                    primary={!isSubmitting}
                                >
                                    Send
                                </Button>
                            </Form>
                        </React.Fragment>
                    )}
                </Formik>
            </React.Fragment>
        );
    }

    render() {
        const { width, height } = this.state;

        return (
            <Auth
                windowSize={{ width, height }}
                left={<Slideshow />}
                right={this.renderForm()}
            />
        );
    }
}

export default ForgotPassword;