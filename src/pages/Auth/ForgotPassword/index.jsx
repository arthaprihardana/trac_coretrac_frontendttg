/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-23 21:00:17 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-01-23 21:00:17 
 */
import React, { PureComponent } from "react";
import { debounce } from "lodash";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";

// components
import { Text, Button, TracField } from "atom";
import { Slideshow, ModalSuccess } from "molecules";
import { Auth } from "templates";
import { PulseLoader } from 'react-spinners';
import window from 'window-or-global';

// actions
import { postForgotPassword, deletePrevForgotPassword } from "actions";

// validation schema
const AuthSchema = Yup.object().shape({
    email: Yup.string()
        .email("Email should be valid!")
        .required("Email is required!")
});

class ForgotPassword extends PureComponent {
    constructor() {
        super();
        this.state = {
            width: window.innerWidth,
            height: window.innerHeight,
            modalOpen: false,
            forgot_password: null,
            ErrorMessage: null
        };

        this.handleWindowResize = this.handleWindowResize.bind(this);
        this.handleWindowResize = debounce(this.handleWindowResize, 300);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleWindowResize);
    }

    componentWillUnmount() {
        this.props.deletePrevForgotPassword();
        window.removeEventListener("resize", this.handleWindowResize);
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.forgotPasswordSuccess !== prevProps.forgotPasswordSuccess) {
            return { success: this.props.forgotPasswordSuccess };
        }
        if(this.props.forgotPasswordFailure !== prevProps.forgotPasswordFailure) {
            return { failure: this.props.forgotPasswordFailure }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.success !== undefined) {
                this.setState({ 
                    modalOpen: true,
                    forgot_password: snapshot.success,
                    ErrorMessage: null
                });
            }
            if(snapshot.failure !== undefined) {
                this.setState({ 
                    modalOpen: true,
                    forgot_password: null,
                    ErrorMessage: snapshot.failure
                }, () => this.props.deletePrevForgotPassword());
            }
        }
    }
    
    handleSubmit(values, { setSubmitting }) {
        this.props.postForgotPassword({ FormData: values });
        setSubmitting(false);
    }

    handleModalClose(e) {
        e.stopPropagation();
        this.setState({ modalOpen: false });
    }

    handleWindowResize() {
        this.setState({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }

    renderForm() {
        const { modalOpen, forgot_password } = this.state;

        return (
            <React.Fragment>
                <div className="text">
                    <Text type="h2">Forgot Password</Text>
                </div>
                <Formik
                    initialValues={{
                        email: ""
                    }}
                    validationSchema={AuthSchema}
                    onSubmit={this.handleSubmit}
                >
                    {({ isSubmitting, handleChange, handleBlur, values }) => (
                        <React.Fragment>
                            <ModalSuccess
                                open={modalOpen}
                                size="medium"
                                goto={ forgot_password !== null && "/" }
                                onCloseModal={this.handleModalClose}
                                content={ forgot_password !== null ? <div className="content">
                                    <Text>Forgot Password Accepted!</Text>
                                    <Text>Email reset password has been sent to You!</Text>
                                    <Text>Update your password and come back to our website :)</Text>
                                </div> : <div className="content">
                                    <Text>Forgot Password Failed!</Text>
                                    <Text>Email not Found!</Text>
                                    <Text>Please input your email account!</Text>
                                </div>}
                            />
                            <Form>
                                <TracField
                                    id="email"
                                    name="email"
                                    type="text"
                                    labelName="Email"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.email}
                                />
                                <Button
                                    type="submit"
                                    disabled={isSubmitting}
                                    primary={!isSubmitting}
                                >
                                    {this.props.loading ? 
                                        <PulseLoader
                                            sizeUnit={"px"}
                                            size={7}
                                            color={'#ffffff'}
                                            loading={this.props.loading}
                                        />
                                        : "Send" }
                                </Button>
                            </Form>
                        </React.Fragment>
                    )}
                </Formik>
            </React.Fragment>
        );
    }

    render() {
        const { width, height } = this.state;

        return (
            <Auth
                windowSize={{ width, height }}
                left={<Slideshow />}
                right={this.renderForm()}
            />
        );
    }
}

const mapStateToProps = ({ forgotpassword }) => {
    return forgotpassword;
};

export default connect(mapStateToProps, {
    postForgotPassword,
    deletePrevForgotPassword
})(ForgotPassword);
