import React, { PureComponent } from "react";
import { debounce } from "lodash";
import { Link } from "react-router-dom";
import { Formik, Form } from "formik";

// components
import { Text, Button } from "atom";
import { Slideshow, ModalSuccess } from "molecules";
import { Auth } from "templates";

class Verification extends PureComponent{
    constructor() {
        super();
        this.state = {
            width: window.innerWidth,
            height: window.innerHeight,
            modalOpen: false,
            errorInput: false,
            inputValue: {
                firstInput: '',
                secondInput: '',
                thirdInput: '',
                fourthInput: '',
                fifthInput: '',
                sixthInput: ''
            }
        };

        this.handleWindowResize = this.handleWindowResize.bind(this);
        this.handleWindowResize = debounce(this.handleWindowResize, 300);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleWindowResize);
        document.getElementById("firstInput").focus();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowResize);
    }

    handleSubmit() {
        this.setState({ modalOpen: true });
    }

    handleModalClose(e) {
        e.stopPropagation();
        this.setState({ modalOpen: false });
    }

    handleWindowResize() {
        this.setState({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }

    inputChange = (e) => {
        if (e.charCode >= 48 && e.charCode <= 57){
            this.setState({
                inputValue: {
                    ...this.state.inputValue,
                    [e.currentTarget.id]: e.key
                }
            })
            switch(e.currentTarget.id){
                case 'firstInput':
                    document.getElementById("secondInput").focus();
                    break;
                case 'secondInput':
                    document.getElementById("thirdInput").focus();
                    break;
                case 'thirdInput':
                    document.getElementById("fourthInput").focus();
                    break;
                case 'fourthInput':
                    document.getElementById("fifthInput").focus();
                    break;
                case 'fifthInput':
                    document.getElementById("sixthInput").focus();
                    break;
                default:
                    this.setState({errorInput: true});
                    return;
            }
        }
    }

    renderForm() {
        const { modalOpen, inputValue, errorInput } = this.state;
        return (
            <React.Fragment>
                <div className="text">
                    <Text type="h2">Verification</Text>
                    <Text className="desc-verification">We have sent the verification code to<br/> 
                    bbbryan@gmail.com<br/>
                    enter the code to verify.</Text>
                </div>
                <Formik
                    onSubmit={this.handleSubmit}
                >
                    {({ isSubmitting }) => (
                        <React.Fragment>
                            <ModalSuccess
                                open={modalOpen}
                                size="medium"
                                goto="/"
                                onCloseModal={this.handleModalClose}
                            />
                            <Form>
                                <div className="code-verify">
                                    <input id="firstInput" type="number" onChange={()=> null} value={inputValue.firstInput} onKeyPress={this.inputChange}/>
                                    <input id="secondInput" type="number" onChange={()=> null} value={inputValue.secondInput} onKeyPress={this.inputChange}/>
                                    <input id="thirdInput" type="number" onChange={()=> null} value={inputValue.thirdInput} onKeyPress={this.inputChange}/>
                                    <input id="fourthInput" type="number" onChange={()=> null} value={inputValue.fourthInput} onKeyPress={this.inputChange}/>
                                    <input id="fifthInput" type="number" onChange={()=> null} value={inputValue.fifthInput} onKeyPress={this.inputChange}/>
                                    <input id="sixthInput" type="number" onChange={()=> null} value={inputValue.sixthInput} onKeyPress={this.inputChange}/>
                                </div>
                                <p className={`message ${errorInput ? 'error' : ''}`}>Code does not match.</p>
                                <Text>
                                    <Link to="/verification">Resend verification code?</Link>
                                </Text>
                                <Button
                                    type="submit"   
                                    disabled={isSubmitting}
                                    primary={!isSubmitting}
                                >
                                    SEND
                                </Button>
                                
                            </Form>
                        </React.Fragment>
                    )}
                </Formik>
            </React.Fragment>
        );
    }
    render() {
        const { width, height } = this.state;

        return (
            <Auth
                windowSize={{ width, height }}
                left={<Slideshow />}
                right={this.renderForm()}
            />
        );
    }
}

export default Verification;