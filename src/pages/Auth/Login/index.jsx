/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-23 21:00:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 16:02:07
 */
import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import { debounce } from "lodash";
import { Link, withRouter, Redirect } from "react-router-dom";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";

// components
import { Text, Button, TracField, Modal } from "atom";
import { Slideshow } from "molecules";
import { Auth } from "templates";
import { delay } from "helpers";
import { PulseLoader } from 'react-spinners';
import window from 'window-or-global';

// auth0
import auth0 from 'services/Auth';

// actions
import { postLogin, postLoginSosmed, deletePrevLogin } from "actions";

// IndexDB
import db from "../../../db";
import lang from '../../../assets/data-master/language'
import successImg from 'assets/images/background/login-success.svg';

// validation schema
const AuthSchema = Yup.object().shape({
    email: Yup.string()
        .email("Email should be valid!")
        .required("Email Wajib diisi"),
    password: Yup.string()
        .min("6", "Should be 6 characters!")
        .required("Password wajib diisi")
});

class Login extends PureComponent {
    constructor() {
        super();
        this.state = {
            width: window.innerWidth,
            height: window.innerHeight,
            checked: false,
            type: "password",
            icon: "password hide",
            email: "user@mail.com",
            password: "123456",
            modalOpen: false,
            ErrorMessage: null
        };

        this.handleWindowResize = this.handleWindowResize.bind(this);
        this.handleWindowResize = debounce(this.handleWindowResize, 300);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleCheckedChange = this.handleCheckedChange.bind(this);
        this.handleShowPassword = this.handleShowPassword.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
    }

    componentDidMount() {
        const { 
            history: {
                push,
                action
            },
            isLogin
        } = this.props;
        window.addEventListener("resize", this.handleWindowResize);
        this.handleAuth0();
        delay(3000).then(() => {
            let sosmed = localStorage.getItem('auth_sosmed');
            if(sosmed !== null) {
                this.props.postLoginSosmed(JSON.parse(sosmed));
            }
        });
        if(isLogin && action === "POP") {
            push('/');
        }
    }

    handleAuth0 = async () => {
        const Auth0 = new auth0("login");
        let responseAuth = await Auth0.handleAuthentication();
        if(responseAuth !== null) {
            this.props.postLoginSosmed(responseAuth);
        }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowResize);
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.loginSuccess !== prevProps.loginSuccess) {
            return { success: this.props.loginSuccess };
        }
        if(this.props.loginFailure !== prevProps.loginFailure) {
            return { failure: this.props.loginFailure }
        }
        if(this.props.loginsosmedSuccess !== prevProps.loginsosmedSuccess) {
            return { sosmedsuccess: this.props.loginsosmedSuccess }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.success !== undefined) {   
                const {
                    history: {
                        push
                    }
                } = this.props;
                delay(1000).then(() => push("/"));
                // window.location = '/';
            }
            if(snapshot.failure !== undefined) {
                if(snapshot.failure !== null) {
                    this.setState({
                        modalOpen: true,
                        ErrorMessage: lang.emailPasswordNotCorrect[lang.default]
                        // ErrorMessage: snapshot.failure
                    });
                }
            }
            if(snapshot.sosmedsuccess !== undefined) {
                const {
                    history: { push }
                } = this.props;
                delay(1000).then(() => push("/"));
            }
        }
    }

    handleSubmit(values, { setSubmitting }) {
        this.props.postLogin({FormData: values});
        setSubmitting(false);
    }

    handleCheckedChange() {
        this.setState(prevState => ({
            checked: !prevState.checked
        }));
    }

    handleInputChange(e, type) {
        e.preventDefault();
        if (type === "email") this.setState({ email: e.target.value });
        if (type === "password") this.setState({ password: e.target.value });
    }

    handleWindowResize() {
        this.setState({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }

    handleModalClose(e) {
        e.stopPropagation();
        this.setState({ modalOpen: false }, () => this.props.deletePrevLogin());
    }

    handleShowPassword(e) {
        e.preventDefault();
        const { type, icon } = this.state;
        if (type === "text" && icon === "password show")
            this.setState({ type: "password", icon: "password hide" });
        if (type === "password" && icon === "password hide")
            this.setState({ type: "text", icon: "password show" });
    }

    renderForm() {
        const { checked, type, icon, modalOpen, ErrorMessage } = this.state;
        const { loading } = this.props;
        return (
            <React.Fragment>
                <div className="text">
                    <Text type="h2">Login</Text>
                    <Text>
                        {lang.dontHaveAccount[lang.default]}{" "}
                        <Link to="/register">{lang.registerHere[lang.default]}</Link>
                    </Text>
                </div>
                <Formik
                    initialValues={{
                        email: "",
                        password: "",
                        rememberme: ""
                    }}
                    validationSchema={AuthSchema}
                    onSubmit={this.handleSubmit}
                >
                    {({ isSubmitting, handleBlur, handleChange, values }) => (
                        <Fragment>
                            <Modal
                                open={modalOpen}
                                onClick={this.handleModalClose}
                                header={{
                                    withCloseButton: true,
                                    onClick: this.handleModalClose,
                                    children: ''
                                }}
                                content={{
                                    children: (
                                        <div className="success-login-wrapper">
                                            <div className="img-success">
                                                <img src={successImg} alt=""/>
                                            </div>
                                            <p className="main-title">Login Failed!</p>
                                            <p className="sub-title">{ErrorMessage}</p>
                                        </div>
                                    ),
                                }}
                                footer={{
                                    position: 'center',
                                    children: (
                                        <div className="modal-footer-action">
                                            <Button primary onClick={this.handleModalClose}>Close</Button>
                                        </div>
                                    ),
                                }}
                            />
                            <Form>
                                <TracField
                                    id="email"
                                    name="email"
                                    type="text"
                                    labelName="Email"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.email}
                                />
                                <TracField
                                    id="password"
                                    name="password"
                                    type={type}
                                    labelName="Password"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    onClick={this.handleShowPassword}
                                    value={values.password}
                                    icon={icon}
                                />
                                <div className="auth-row">
                                    <div className="auth-col">
                                        <TracField
                                            id="rememberme"
                                            name="rememberme"
                                            type="checkbox"
                                            labelName="Remember Me"
                                            labelSize="small"
                                            onChange={this.handleCheckedChange}
                                            checked={checked}
                                            value={values.rememberme}
                                        />
                                    </div>
                                    <div className="auth-col">
                                        <Link
                                            className="forgot-password"
                                            to="/forgot-password"
                                        >
                                            {lang.forgotPassword[lang.default]}
                                        </Link>
                                    </div>
                                </div>
                                <Button
                                    type="submit"
                                    disabled={isSubmitting}
                                    primary={!isSubmitting}
                                >
                                    {loading ? 
                                        <PulseLoader
                                            sizeUnit={"px"}
                                            size={7}
                                            color={'#ffffff'}
                                            loading={loading}
                                        />
                                        : isSubmitting ? "Logging You In..." : "Login" }
                                </Button>
                            </Form>
                        </Fragment>
                    )}
                </Formik>
            </React.Fragment>
        );
    }

    render() {
        const { width, height } = this.state;

        return (
            <Auth
                windowSize={{ width, height }}
                left={<Slideshow />}
                right={this.renderForm()}
                authText={lang.loginSocial[lang.default]}
                corporateLogin
            />
        );
    }
}

Login.propTypes = {
    history: PropTypes.object
};

const mapStateToProps = ({ login }) => {
    const { loading, loginSuccess, loginFailure, loginsosmedSuccess, isLogin } = login;
    return { loading, loginSuccess, loginFailure, loginsosmedSuccess, isLogin };
}

export default withRouter(
    connect(mapStateToProps, {
        postLogin,
        postLoginSosmed,
        deletePrevLogin
    })(Login)
);