import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { debounce } from "lodash";
import { Link, withRouter } from "react-router-dom";
import { Formik, Form } from "formik";
import * as Yup from "yup";

// components
import { Text, Button, TracField } from "atom";
import { Slideshow } from "molecules";
import { Auth } from "templates";
import { delay } from "helpers";

// validation schema
const AuthSchema = Yup.object().shape({
    email: Yup.string()
        .email("Email should be valid!")
        .required("Email is required!"),
    password: Yup.string()
        .min("6", "Should be 6 characters!")
        .required("Password is required!")
});

class Login extends PureComponent {
    constructor() {
        super();
        this.state = {
            width: window.innerWidth,
            height: window.innerHeight,
            checked: false,
            type: "password",
            icon: "password hide",
            email: "user@mail.com",
            password: "123456"
        };

        this.handleWindowResize = this.handleWindowResize.bind(this);
        this.handleWindowResize = debounce(this.handleWindowResize, 300);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleCheckedChange = this.handleCheckedChange.bind(this);
        this.handleShowPassword = this.handleShowPassword.bind(this);
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleWindowResize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowResize);
    }

    handleSubmit(values) {
        const {
            history: { push }
        } = this.props;
        delay(1000).then(() => push("/"));
    }

    handleCheckedChange() {
        this.setState(prevState => ({
            checked: !prevState.checked
        }));
    }

    handleInputChange(e, type) {
        e.preventDefault();
        if (type === "email") this.setState({ email: e.target.value });
        if (type === "password") this.setState({ password: e.target.value });
    }

    handleWindowResize() {
        this.setState({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }

    handleShowPassword(e) {
        e.preventDefault();
        const { type, icon } = this.state;
        if (type === "text" && icon === "password show")
            this.setState({ type: "password", icon: "password hide" });
        if (type === "password" && icon === "password hide")
            this.setState({ type: "text", icon: "password show" });
    }

    renderForm() {
        const { checked, type, icon } = this.state;

        return (
            <React.Fragment>
                <div className="text">
                    <Text type="h2">Login</Text>
                    <Text>
                        Don&#39;t have an Account?{" "}
                        <Link to="/register">Register Here!</Link>
                    </Text>
                </div>
                <Formik
                    initialValues={{
                        email: "",
                        password: "",
                        rememberme: ""
                    }}
                    validationSchema={AuthSchema}
                    onSubmit={this.handleSubmit}
                >
                    {({ isSubmitting, handleBlur, handleChange, values }) => (
                        <Form>
                            <TracField
                                id="email"
                                name="email"
                                type="text"
                                labelName="Email"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.email}
                            />
                            <TracField
                                id="password"
                                name="password"
                                type={type}
                                labelName="Password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                onClick={this.handleShowPassword}
                                value={values.password}
                                icon={icon}
                            />
                            <div className="auth-row">
                                <div className="auth-col">
                                    <TracField
                                        id="rememberme"
                                        name="rememberme"
                                        type="checkbox"
                                        labelName="Remember Me"
                                        labelSize="small"
                                        onChange={this.handleCheckedChange}
                                        checked={checked}
                                        value={values.rememberme}
                                    />
                                </div>
                                <div className="auth-col">
                                    <Link
                                        className="forgot-password"
                                        to="/forgot-password"
                                    >
                                        Forgot Password?
                                    </Link>
                                </div>
                            </div>
                            <Button
                                type="submit"
                                disabled={isSubmitting}
                                primary={!isSubmitting}
                            >
                                {isSubmitting ? "Logging You In..." : "Login"}
                            </Button>
                        </Form>
                    )}
                </Formik>
            </React.Fragment>
        );
    }

    render() {
        const { width, height } = this.state;

        return (
            <Auth
                windowSize={{ width, height }}
                left={<Slideshow />}
                right={this.renderForm()}
                authText="Use your social accounts"
                corporateLogin
            />
        );
    }
}

Login.propTypes = {
    history: PropTypes.object
};

export default withRouter(Login);