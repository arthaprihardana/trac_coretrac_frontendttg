/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-23 20:43:19 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-18 10:14:41
 */
import React, { PureComponent } from "react";
import { debounce } from "lodash";
import { Link } from "react-router-dom";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { connect } from 'react-redux';
import _ from 'lodash';

// components
import { Text, Button, TracField } from "atom";
import { Slideshow, ModalSuccess } from "molecules";
import { Auth } from "templates";
import { PulseLoader } from 'react-spinners';
import window from 'window-or-global';
import lang from '../../../assets/data-master/language'
// actions
import { postRegister, deletePrevRegister } from "actions";
import refresh from '../../../assets/images/icons/refresh.svg'
// validation schema
const AuthSchema = Yup.object().shape({
    email: Yup.string()
        .email("Email should be valid!")
        .required(lang.loginEmptyEmail[lang.default]),
    password: Yup.string()
        .min("8", "Should be 8 characters!")
        .required(lang.loginEmptyPassword[lang.default]),
    confirmPassword: Yup.string()
        .min("8", "Should be 8 characters!")
        .required(lang.loginEmptyPassword[lang.default])
});
/**
 * Page Register
 *
 * @class Register
 * @extends {PureComponent}
 */
class Register extends PureComponent {
    constructor() {
        super();
        this.state = {
            captcha:'https://ibid.astra.co.id/captcha/newCaptcha?rnd=0.955637930673298',
            width: window.innerWidth,
            height: window.innerHeight,
            password: {
                type: "password",
                icon: "password hide"
            },
            confirmPassword: {
                type: "password",
                icon: "password hide"
            },
            modalOpen: false,
            user_login: null
        };

        this.handleWindowResize = this.handleWindowResize.bind(this);
        this.handleWindowResize = debounce(this.handleWindowResize, 300);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleShowPassword = this.handleShowPassword.bind(this);
        this.handleShowConfirmPassword = this.handleShowConfirmPassword.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleWindowResize);
    }

    componentWillUnmount() {
        this.props.deletePrevRegister();
        window.removeEventListener("resize", this.handleWindowResize);
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.registerSucces !== prevProps.registerSucces) {
            return { success: this.props.registerSucces }
        }
        if(this.props.registerError !== prevProps.registerError) {
            return { failure: this.props.registerError }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.success !== undefined) {
                this.setState({ 
                    modalOpen: true,
                    user_login: snapshot.success.user_login,
                    ErrorMessage: null
                });
            }
            if(snapshot.failure !== undefined) {
                this.setState({
                    modalOpen: true,
                    user_login: null,
                    ErrorMessage: snapshot.failure
                }, () => this.props.deletePrevRegister());
            }
        }
    }

    componentDidCatch = (error, info) => {
        // console.log('info ==>', info);
    }

    handleSubmit(values, { setSubmitting }) {
        this.props.postRegister({ FormData: values });
        setSubmitting(false);
    }

    handleWindowResize() {
        this.setState({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }

    handleShowPassword(e) {
        e.preventDefault();
        const {
            password: { type, icon }
        } = this.state;
        if (type === "text" && icon === "password show")
            this.setState({
                password: { type: "password", icon: "password hide" }
            });
        if (type === "password" && icon === "password hide")
            this.setState({
                password: { type: "text", icon: "password show" }
            });
    }

    reloadCaptcha = () =>{
        let tmp = 'https://ibid.astra.co.id/captcha/newCaptcha?rnd='+Math.random()
        this.setState({
            captcha:tmp
        })
    }

    handleShowConfirmPassword(e) {
        e.preventDefault();
        const {
            confirmPassword: { type, icon }
        } = this.state;
        if (type === "text" && icon === "password show")
            this.setState({
                confirmPassword: { type: "password", icon: "password hide" }
            });
        if (type === "password" && icon === "password hide")
            this.setState({
                confirmPassword: { type: "text", icon: "password show" }
            });
    }

    handleModalClose(e) {
        e.stopPropagation();
        this.setState({ modalOpen: false });
    }

    renderForm() {
        const { password, confirmPassword, modalOpen, user_login, captcha } = this.state;

        return (
            <React.Fragment>
                <div className="text">
                    <Text type="h2">{lang.register[lang.default]}</Text>
                    <Text>
                        {lang.alreadyHaveAccount[lang.default]}{" "}
                        <Link to="/login">{lang.loginHere[lang.default]}!</Link>
                    </Text>
                </div>
                <Formik
                    initialValues={{
                        email: "",
                        password: "",
                        captcha: "",
                        confirmPassword: ""
                    }}
                    validationSchema={AuthSchema}
                    onSubmit={this.handleSubmit}
                >
                    {({ isSubmitting, handleChange, handleBlur, values }) => (
                        <React.Fragment>
                            <ModalSuccess
                                open={modalOpen}
                                size="medium"
                                goto={ user_login !== null && "/" }
                                onCloseModal={this.handleModalClose}
                                content={ user_login !== null ? <div className="content">
                                    <Text>{lang.congrats[lang.default]} {user_login && _.upperFirst(_.split(user_login.EmailPersonal, "@")[0])}!</Text>
                                    <Text>{lang.registerCongrats[lang.default]}</Text>
                                    <Text>{lang.pleaseCheckYourEmail[lang.default]}</Text>
                                    <Text>{lang.enjoyOurService[lang.default]}</Text>
                                </div> : <div className="content">
                                    <Text>{lang.registerFailed[lang.default]}</Text>
                                    <Text>{lang.emailHasRegister[lang.default]}</Text>
                                    <Text>{lang.pleaseRegisterWithOther[lang.default]}</Text>
                                </div>}
                            />
                            <Form>
                                <TracField
                                    id="email"
                                    name="email"
                                    type="text"
                                    labelName="Email"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.email}
                                />
                                <TracField
                                    id="password"
                                    name="password"
                                    type={password.type}
                                    labelName="Password"
                                    onClick={this.handleShowPassword}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password}
                                    icon={password.icon}
                                />
                                <TracField
                                    id="confirmPassword"
                                    name="confirmPassword"
                                    type={confirmPassword.type}
                                    labelName={lang.confirm_password[lang.default]}
                                    onClick={this.handleShowConfirmPassword}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.confirmPassword}
                                    icon={confirmPassword.icon}
                                />
                                {/*<div  style={{display:'flex'}}>*/}
                                    {/*<div  style={{width:'45%'}}>*/}
                                        {/*<TracField*/}
                                            {/*id="captcha"*/}
                                            {/*name="captcha"*/}
                                            {/*type="text"*/}
                                            {/*labelName="Captcha"*/}
                                            {/*onChange={handleChange}*/}
                                            {/*onBlur={handleBlur}*/}
                                            {/*value={values.captcha}*/}
                                        {/*/>*/}
                                    {/*</div>*/}
                                    {/*<div  style={{width:'40%', marginTop: 25, paddingLeft:5}}>*/}
                                        {/*<img src={captcha} alt=""/>*/}
                                    {/*</div>*/}
                                    {/*<div style={{width:'15%', marginTop: 32, cursor:'pointer', textAlign:'center'}} onClick={this.reloadCaptcha}>*/}
                                        {/*<img src={refresh} alt="" />*/}
                                    {/*</div>*/}
                                   {/**/}
                                {/*</div>*/}
                                <Button
                                    type="submit"
                                    disabled={isSubmitting}
                                    primary={!isSubmitting}
                                >
                                    {this.props.loading ? 
                                        <PulseLoader
                                            sizeUnit={"px"}
                                            size={7}
                                            color={'#ffffff'}
                                            loading={this.props.loading}
                                        />
                                        : lang.register[lang.default] }
                                </Button>
                            </Form>
                        </React.Fragment>
                    )}
                </Formik>
            </React.Fragment>
        );
    }

    render() {
        const { width, height } = this.state;

        return (
            <Auth
                windowSize={{ width, height }}
                left={<Slideshow />}
                right={this.renderForm()}
                authText={lang.orLoginWith[lang.default]}
            />
        );
    }
}

const mapStateToProps = ({ register }) => {
    return register;
}

export default connect(mapStateToProps, {
    postRegister,
    deletePrevRegister
})(Register);
