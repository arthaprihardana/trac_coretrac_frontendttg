import React, { PureComponent } from "react";
import { debounce } from "lodash";
import { Formik, Form } from "formik";
import * as Yup from "yup";

// components
import { Text, Button, TracField } from "atom";
import { Slideshow, ModalSuccess } from "molecules";
import { Auth } from "templates";

// validation schema
const AuthSchema = Yup.object().shape({
    password: Yup.string()
        .min("8", "Should be 8 characters!")
        .required("Password is required!"),
    confirmPassword: Yup.string()
        .min("8", "Should be 8 characters!")
        .required("Password is required!")
});

class NewPassword extends PureComponent {
    constructor() {
        super();
        this.state = {
            width: window.innerWidth,
            height: window.innerHeight,
            password: {
                type: "password",
                icon: "password hide"
            },
            confirmPassword: {
                type: "password",
                icon: "password hide"
            },
            modalOpen: false
        };

        this.handleWindowResize = this.handleWindowResize.bind(this);
        this.handleWindowResize = debounce(this.handleWindowResize, 300);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleShowPassword = this.handleShowPassword.bind(this);
        this.handleShowConfirmPassword = this.handleShowConfirmPassword.bind(
            this
        );
        this.handleModalClose = this.handleModalClose.bind(this);
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleWindowResize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowResize);
    }

    handleSubmit(values) {
        // console.log("form.values:", values);
        this.setState({ modalOpen: true });
    }

    handleWindowResize() {
        this.setState({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }

    handleShowPassword(e) {
        e.preventDefault();
        const {
            password: { type, icon }
        } = this.state;
        if (type === "text" && icon === "password show")
            this.setState({
                password: { type: "password", icon: "password hide" }
            });
        if (type === "password" && icon === "password hide")
            this.setState({
                password: { type: "text", icon: "password show" }
            });
    }

    handleShowConfirmPassword(e) {
        e.preventDefault();
        const {
            confirmPassword: { type, icon }
        } = this.state;
        if (type === "text" && icon === "password show")
            this.setState({
                confirmPassword: { type: "password", icon: "password hide" }
            });
        if (type === "password" && icon === "password hide")
            this.setState({
                confirmPassword: { type: "text", icon: "password show" }
            });
    }

    handleModalClose(e) {
        e.stopPropagation();
        this.setState({ modalOpen: false });
    }

    renderForm() {
        const { password, confirmPassword, modalOpen } = this.state;

        return (
            <React.Fragment>
                <div className="text">
                    <Text type="h2">New Password</Text>
                    <Text>
                        Hi <strong>barrbryan@gmail.com</strong> it's time to make your new TRAC password
                    </Text>
                </div>
                <Formik
                    initialValues={{
                        password: "",
                        confirmPassword: ""
                    }}
                    validationSchema={AuthSchema}
                    onSubmit={this.handleSubmit}
                >
                    {({ isSubmitting, handleChange, handleBlur, values }) => (
                        <React.Fragment>
                            <ModalSuccess
                                open={modalOpen}
                                size="medium"
                                goto="/"
                                onCloseModal={this.handleModalClose}
                            />
                            <Form>
                                <TracField
                                    id="password"
                                    name="password"
                                    type={password.type}
                                    labelName="New Password"
                                    onClick={this.handleShowPassword}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password}
                                    // icon={password.icon}
                                />
                                <TracField
                                    id="confirmPassword"
                                    name="confirmPassword"
                                    type={confirmPassword.type}
                                    labelName="Confirm New Password"
                                    onClick={this.handleShowConfirmPassword}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.confirmPassword}
                                    // icon={confirmPassword.icon}
                                />
                                <Button
                                    type="submit"
                                    disabled={isSubmitting}
                                    primary={!isSubmitting}
                                >
                                    SEND
                                </Button>
                            </Form>
                        </React.Fragment>
                    )}
                </Formik>
            </React.Fragment>
        );
    }

    render() {
        const { width, height } = this.state;

        return (
            <Auth
                windowSize={{ width, height }}
                left={<Slideshow />}
                right={this.renderForm()}
            />
        );
    }
}

export default NewPassword;