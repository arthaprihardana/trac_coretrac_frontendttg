/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-15 10:28:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 10:00:06
 */
// libraries
import React, { PureComponent, Fragment } from "react";
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import { defaultDidMountSetStyle, defaultWillUnmountSetStyle } from 'helpers';
import { Button } from 'atom';
import * as CryptoJS from 'crypto-js';
import { getActivateUser } from 'actions'

// local components
import { Main } from "templates";
import img from "assets/images/background/car-rental.svg";

// assets & styles
import "./SinglePageAuthentication.scss";
import connect from "react-redux/es/connect/connect";

class VerificationRegisterPage extends PureComponent {

    state = {
        bookingData: {
            bookingNumber: null,
            customer: {
                name: null,
                email: null
            },
            passphrase: "1BEFDBFD1039345EB916ED1690E4EFE1",
            referral: {
                code: 'SHARE2015',
                percentage: 25
            }
        },
        user_login: null
    };
    componentDidMount() {
        defaultDidMountSetStyle();
        // let CryptoJSAesJson = {
        //     stringify: function (cipherParams) {
        //         var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
        //         if (cipherParams.iv) j.iv = cipherParams.iv.toString();
        //         if (cipherParams.salt) j.s = cipherParams.salt.toString();
        //         return JSON.stringify(j);
        //     },
        //     parse: function (jsonStr) {
        //         var j = JSON.parse(jsonStr);
        //         var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
        //         if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
        //         if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
        //         return cipherParams;
        //     }
        // }
        let token = this.props.match.params.token;
        // let passphrase = "1BEFDBFD1039345EB916ED1690E4EFE1";
        // var encrypted = CryptoJS.enc.Utf8.stringify(CryptoJS.enc.Base64.parse(token));
        // var decrypted = JSON.parse(CryptoJS.AES.decrypt(encrypted, passphrase, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8))
        // // var decrypted = JSON.parse()
        // console.log(this.props.match.params.token);
        this.props.getActivateUser({IdUser:token});
    }
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.getActivateUserSuccess !== prevProps.getActivateUserSuccess) {
            return { success: this.props.getActivateUserSuccess }
        }
        if(this.props.getActivateUserFailure !== prevProps.getActivateUserFailure) {
            return { failure: this.props.getActivateUserFailure }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.success !== undefined) {
                // console.log("success");
            }
            if(snapshot.failure !== undefined) {
                // console.log("failed");
            }
        }
    }

    componentWillUnmount() {
        defaultWillUnmountSetStyle()
    }

    render() {

        return (
        <Main solid hideMenu hideFooter>
            <div className="p-booking-success">
                <div className="container">
                    <div className="booking-success-image">
                        <img src={img} alt="Banner Booking Success" />
                    </div>
                    <div className="booking-success-description">
                        <h3 className="title">Your Email Verified</h3>
                        <div className="booking-detail">
                            <p>Enjoy.</p>
                        </div>
                        <div className="other-links">
                            <div className="other-links-item">
                                <Button primary goto="/">Go to Home Page</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Main>
        );
    }
}

const mapStateToProps = ({ userprofile }) => {
    const { getActivateUserSuccess, getActivateUserFailure } = userprofile;
    return { getActivateUserSuccess, getActivateUserFailure }
};

export default connect(mapStateToProps, {
    getActivateUser
})(VerificationRegisterPage);

// export default withRouter(VerificationRegisterPage);
