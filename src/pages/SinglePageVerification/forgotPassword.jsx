/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-15 10:28:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 10:00:10
 */
// libraries
import React, { PureComponent, Fragment } from "react";
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import * as Yup from "yup";
import { defaultDidMountSetStyle, defaultWillUnmountSetStyle } from 'helpers';
import { Button, Modal, Text, TracField } from "atom";
import { Form, Formik } from "formik";
import ModalSuccess from './Modal/ModalSuccess'
import ModalFailure from './Modal/ModalFailure'

import {  postNewPassword } from 'actions'

// local components
import { Main } from "templates";
import img from "assets/images/background/car-rental.svg";

// assets & styles
import "./SinglePageAuthentication.scss";
import connect from "react-redux/es/connect/connect";

// validation schema
const AuthSchema = Yup.object().shape({
    newPassword: Yup.string()
        .min(8, "Should be 8 characters!"),
    confirmPassword: Yup.string()
        .min(8, "Should be 8 characters!")
});

class VerificationForgotPasswordPage extends PureComponent {

    state = {
        bookingData: {
            popupSuccess:false,
            popupFailure:false,
            newPassword: '',
            confirmPassword: '',
            customer: {
                name: null,
                email: null
            },
            referral: {
                code: 'SHARE2015',
                percentage: 25
            }
        },
        user_login: null,
        isReady: false
    }

    componentDidMount() {
        defaultDidMountSetStyle()
    }

    componentWillUnmount() {
        defaultWillUnmountSetStyle()
    }
    submitReset = ()=>{
        // console.log("submit");
        let token = this.props.match.params.token;
        // console.log("data", data);
        let password = this.state.newPassword;
        let data = {
            code: token,
            password : password,
        };
        this.props.postNewPassword({ FormData: data });
    };
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.postNewPasswordSuccess !== prevProps.postNewPasswordSuccess) {
            return { success: this.props.postNewPasswordSuccess }
        }
        if(this.props.postNewPasswordFailure !== prevProps.postNewPasswordFailure) {
            return { failure: this.props.postNewPasswordFailure }
        }
        return null;
    };

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.success !== undefined) {
                this.setState({
                    popupSuccess:true
                })
            }
            if(snapshot.failure !== undefined) {
                // console.log("failed");
                this.setState({
                    popupFailure:true
                })
            }
        }
    };
    render() {
        const { newPassword,popupSuccess, confirmPassword, popupFailure } =this.state;
        const { submitReset } = this;
        return (
        <Main solid hideMenu hideFooter>
            <div className="p-booking-success">
                <div className="container">
                    <div className="booking-success-image">
                        <img src={img} alt="Banner Booking Success" />
                    </div>
                    <div className="booking-success-description">
                        <h3 className="title">Forgot Password Verification</h3>
                        <div className="booking-detail">
                            <div>
                                <Formik
                                    initialValues={{
                                        newPassword: "",
                                        confirmPassword: ""
                                    }}
                                    validationSchema={AuthSchema}
                                >
                                    {({ isSubmitting, handleBlur, handleChange, values })=> (
                                        <Form>
                                            <div className="form-input" style={{marginBottom:20}}>
                                                <TracField
                                                    id="newPassword"
                                                    name="newPassword"
                                                    type="password"
                                                    labelName="New Password"

                                                    onChange={e => {
                                                        this.setState({
                                                            newPassword: e.currentTarget.value,
                                                            isReady:true
                                                        })
                                                    }}
                                                    onBlur={handleBlur}
                                                    value={
                                                        newPassword
                                                    }
                                                />
                                            </div>
                                            <div className="form-input"  style={{marginBottom:20}}>
                                                <TracField
                                                    id="confirmPassword"
                                                    name="confirmPassword"
                                                    type="password"
                                                    labelName="Confirm New Password"
                                                    onChange={e => {
                                                        this.setState({
                                                            confirmPassword: e.currentTarget.value,
                                                            isReady:true
                                                        });
                                                    }}
                                                    onBlur={handleBlur}
                                                    value={
                                                        confirmPassword
                                                    }
                                                />
                                            </div>
                                            { confirmPassword !=='' &&
                                                <Button primary onClick={() => {
                                                    submitReset()
                                                }} disabled={!this.state.isReady || confirmPassword!==newPassword || confirmPassword.length<8}>
                                                    Change Password
                                                </Button>
                                            }

                                        </Form>
                                    )
                                    }
                                </Formik>

                            </div>
                        </div>
                        <div className="other-links">
                            <div className="other-links-item">
                                {/*<Button primary goto="/">Verify</Button>*/}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ModalSuccess
                showModal={popupSuccess}
                onClose={()=>this.setState({
                    popupSuccess:false
                }, ()=> window.location='/')}
            />
            <ModalFailure
                showModal={popupFailure}
                onClose={()=>this.setState({
                    popupFailure:false
                }, ()=> window.location='/')}
            />
        </Main>
        );
    }
}


const mapStateToProps = ({ forgotpassword }) => {
    const {  postNewPasswordSuccess, postNewPasswordFailure } = forgotpassword;
    return {  postNewPasswordSuccess, postNewPasswordFailure }
};

export default connect(mapStateToProps, {
    postNewPassword
})(VerificationForgotPasswordPage);