import React, { PureComponent, Fragment } from "react";
import bannerImage from '../../assets/images/dummy/banner-privacy-policy.png';
import logoSelog from '../../assets/images/logo/logo-selog.png';
import logoOrenz from '../../assets/images/logo/logo-orenz.png';
import logoIbid from '../../assets/images/logo/logo-ibid.png';
import logoMobil88 from '../../assets/images/logo/logo-mobil88.png';
import logoAstra from '../../assets/images/logo/logo-astra.png';

// components
import { Main, Static} from "templates";
// import { Accordion } from 'organisms';
import "./Link.scss";
import "../../organisms/Footer/Footer.scss";
import "./ContentList";
import ContentList from "./ContentList";
//import this.state.imgDataJSON from '../../assets/data-dummy/link.json';

class Link extends PureComponent {
    state = {
        data : "",
        btnobj : [
            {label:"Group Holding", id:1},
            {label:"SERA Group Companies", id:2}
        ],
        queobj : [],
        imgDataJSON : {
            "content" : {
                "contentA" : [
                    {"image":logoAstra}
                ],
                "contentB" : [
                    {"image":logoSelog},
                    {"image":logoOrenz},
                    {"image":logoIbid},
                    {"image":logoMobil88}
                ]
            }
        }
    }


    componentDidMount() {
        document.body.classList.add("add-padding-top");
        this.removeActiveButton()
        document.querySelectorAll(".side-badges-link")[0].classList.add('.active');
        this.setState({
            data : "Syarat dan Ketentuan",
            queobj : this.state.imgDataJSON.content.contentA
        });
    }

    removeActiveButton = () => {
        for(let i = 0; i <= 1;i++){
            document.querySelectorAll(".side-badges-link")[i].classList.remove('active');
        }
    }

    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
    }

    changeType = (i, j) => {
        this.removeActiveButton();
        //console.log(e)
        if(i === 1){
            this.setState({
                data : j,
                queobj : this.state.imgDataJSON.content.contentA
            });
        }else 
        if(i === 2){
            this.setState({
                data : j,
                queobj : this.state.imgDataJSON.content.contentB
            });
        }
        
    }

    render() {  
        
        return (
            <Main solid >
                <Static title="Link"  bannerImg={bannerImage}>
                </Static>
                
                <div className="tnc-content-wrapper row">
                    <div className="tnc-button-list column-link left-link">
                        {this.state.btnobj.map((item, index) => (
                            <ButtonList key={index} btnClick={this.changeType} label={item.label} id={item.id} />
                        ))}
                    </div>
                    <div className="tnc-content-list column-link right-link">
                        <ContentList data={this.state.data} contentlist= {this.state.queobj.map((item, index) => (
                            <Fragment>
                                <img className="imgbox" src={item.image} alt=""/>
                            </Fragment>
                        ))}/>
                    </div>
                </div>
            </Main>
        );
    }
}

class ButtonList extends React.Component {

    selectData = (e) => {

        let selector = e.currentTarget;
        if (selector.classList.contains("active")) {
            selector.classList.remove("active");
        } else {
            selector.classList.add("active");
        }
        this.props.btnClick(this.props.id, this.props.label)
        // console.log(this.props.label);
    }

    render() {
        return(
            <button 
                id={this.props.id}
                type="buttons" 
                className="side-badges-link" 
                onClick={e => this.selectData(e)} >
                {this.props.label}
            </button>
        );
    }
}

// class Content extends React.Component {
//     render() {
//         return(
//             <Fragment>
//                 {/* <h4>{this.props.content}</h4> */}
//                 <p>{this.props.image}</p>
//                 <img src={this.props.image} alt=""/>
//                 {/* <img className="cover" src={bannerImg} alt="banner" /> */}
//             </Fragment>
//         );
//     }
// }

export default Link;

