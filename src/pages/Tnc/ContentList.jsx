import React, { Component } from "react";

// components
import "./Tnc.scss";

class ContentList extends Component {

    render() {
        return(
            <div>
                <div className="info-header">
                    <h3>{this.props.data}</h3>
                </div>
                <div className="list-question-wrapper">
                    {this.props.questlist}
                </div>
            </div>
        );
    }
}

export default ContentList;