import React, { PureComponent, Fragment } from "react";
import bannerImage from '../../assets/images/trac/T_C.png';
// components
import { Main, Static} from "templates";
import { Accordion } from 'organisms';
import "./Tnc.scss";
import "../../organisms/Footer/Footer.scss";
import "./ContentList";
import ContentList from "./ContentList";
import syarat from '../../assets/data-master/syarat_ketentuan'

class Tnc extends PureComponent {
    state = {
        data : "",
        btnobj : [
            {label:"Pendahuluan", id:1},
            {label:"Umum", id:2},
            {label:"Penggunaan", id:3},
            {label:"Car Rental", id:4},
            {label:"Airport Transfer", id:5},
            {label:"Bus Rental", id:6},
            {label:"Asuransi", id:7},
            {label:"Pembayaran", id:8}
        ],
        queobj : [],
        breadCrumb: [
            {
                label: 'Home',
                link: '/'
            },
            {
                label: 'Syarat dan Ketentuan',
                link: '/terms-and-conditions'
            },
            {
                label: 'Pendahuluan',
                link: '/terms-and-conditions'
            },
        ],
    }

    componentDidMount() {
        document.body.classList.add("add-padding-top");
        this.removeActiveButton()
        document.querySelectorAll(".side-badges-tnc")[0].classList.add('.active');
        this.setState({
            data : "Syarat dan Ketentuan",
            queobj : syarat.pendahuluan
        });
    }

    createMarkup = (html) => {
        return {__html: html};
    };

    removeActiveButton = () => {
        for(let i = 0; i <= 7;i++){
            document.querySelectorAll(".side-badges-tnc")[i].classList.remove('active');
        }
    }

    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
    }

    changeType = (i, j) => {
        this.removeActiveButton();
        if(i === 1){
            this.setState({
                data : j,
                queobj : syarat.pendahuluan
            });
        }else 
        if(i === 2){
            this.setState({
                data : j,
                queobj : syarat.umum
            });
        }else 
        if(i === 3){
            this.setState({
                data : j,
                queobj : syarat.pengguna
            });
        }else 
        if(i === 4){
            this.setState({
                data : j,
                queobj : syarat.personal_car
            });
        }else 
        if(i === 5){
            this.setState({
                data : j,
                queobj : syarat.personal_airport
            });
        }else 
        if(i === 6){
            this.setState({
                data : j,
                queobj : syarat.personal_bus
            });
        }else
        if(i === 7){
            this.setState({
                data : j,
                queobj : syarat.asuransi
            });
        }else
        if(i === 8){
            this.setState({
                data : j,
                queobj : syarat.pembayaran
            });
        }
        
    };

    render() {
        const { queobj } =this.state;
        return (
            <Main solid >
                <Static title="Syarat dan Ketentuan" breadCrumb={this.state.breadCrumb} bannerImg={bannerImage}>
                </Static>
                
                <div className="tnc-content-wrapper row">
                    <div className="tnc-button-list column-tnc left-tnc">
                        {this.state.btnobj.map((item, index) => (
                            <ButtonList key={index} btnClick={this.changeType} label={item.label} id={item.id} />
                        ))}
                    </div>
                    <div className="tnc-content-list column-tnc right-tnc">
                        <div dangerouslySetInnerHTML={this.createMarkup(queobj)} style={{ paddingLeft: 20,  textAlign: 'justify', paddingRight: 54 }} />
                        {/*<ContentList data={this.state.data} questlist= {this.state.queobj.map((item, index) => (*/}
                            {/*<Question key={index} question={item.question} answer={item.answer} />*/}
                        {/*))}/>*/}
                    </div>
                </div>
            </Main>
        );
    }
}

class ButtonList extends React.Component {

    selectData = (e) => {

        let selector = e.currentTarget;
        if (selector.classList.contains("active")) {
            selector.classList.remove("active");
        } else {
            selector.classList.add("active");
        }
        this.props.btnClick(this.props.id, this.props.label)
        console.log(this.props.label);
    }

    render() {
        return(
            <button 
                id={this.props.id}
                type="buttons" 
                className="side-badges-tnc" 
                onClick={e => this.selectData(e)} >
                {this.props.label}
            </button>
        );
    }
}

class Question extends React.Component {
    render() {
        return(
            <Fragment>
                {/* <h4>{this.props.question}</h4> */}
                <p>{this.props.answer}</p>
            </Fragment>
            // <Accordion
            //     title={this.props.question}
            //     defaultValue={false}
            // >
            //     <p>{this.props.answer}</p> 
            // </Accordion>
        );
    }
}
export default Tnc;

