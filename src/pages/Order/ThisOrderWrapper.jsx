/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-08 10:17:17 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-02-08 10:17:17 
 */
import React from 'react';

const ThisOrderWrapper = ({children}) => {
    return (
        <div className="order-detail">
            <div className="container">
                <div className="order-detail-container">
                    {children}
                </div>
            </div>
        </div>
    )
}

export default ThisOrderWrapper;