import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import { delay } from 'helpers';

// components
import { Main } from "templates";
import { CarDetail, Sidebar, SidebarBookingDetail, SidebarNote } from "organisms";
import {
    OrderSteps,
    SidebarPromoCode,
    SidebarPaymentDetail
} from "molecules";

// style
import "./Order.scss";

//dummy data
import selectedCarJSON from "../../assets/data-dummy/selected-car.json";
import bookingDetailJSON from "assets/data-dummy/booking.json";
import { CustomerGlobal } from "context/RentContext";

const selectedCarDummy = selectedCarJSON.map(car => {
    car.img = require(`../../assets/images/dummy/cars/${car.img}`);
    return car;
});

// Order Steps
class Order extends PureComponent {
    state = {
        selectedCar: selectedCarDummy,
        bookingDetail: bookingDetailJSON
    };

    componentDidMount() {
        document.body.classList.add("add-padding-top");
        document.body.classList.add("background-grey");
    }

    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
        document.body.classList.remove("background-grey");
    }

    extrasChange = value => {
        this.setState(
            {
                selectedCar: value
            },
            () => {
                if (this.state.selectedCar.length < 1) {
                    this.props.history.push("/transport-list/car");
                }
            }
        );
    };

    handleSubmitCar = () => {
        const { history: { push } } = this.props;
        delay(1000).then(() => push('/personal-detail-login'));
    }

    render() {
        const { selectedCar, bookingDetail } = this.state;
        let orderStepsData = [
            { step: "Car Order", className: "current" },
            { step: "Personal Detail", className: "" },
            { step: "Payment", className: "" }
        ];
        if(this.props.rentContext.state.values.type_service === '2'){
            orderStepsData = [
                { step: "Car Order", className: "current" },
                { step: "Personal Detail", className: "" },
                { step: "Driver Information", className: "" },
                { step: "Payment", className: "" }
            ];
        }
        return (
            <Main solid hideMenu headerClose>
                {/* order steps */}
                <OrderSteps data={orderStepsData} />

                {/* Order Detail */}
                <div className="order-detail">
                    <div className="container">
                        <div className="order-detail-container">
                            {/* Left Content */}
                            <div className="left-content">
                                {/* Car Detail */}
                                <CarDetail
                                    data={selectedCar}
                                    onExtrasChange={value => this.extrasChange(value)}
                                    onRemove={value => this.extrasChange(value)}
                                />
                            </div>

                            {/* Sidebar */}
                            <Sidebar>

                                {/* Booking Detail */}
                                <SidebarBookingDetail data={bookingDetail}/>

                                {/* Promo Code */}
                                <SidebarPromoCode />

                                {/* Note/Request */}
                                <SidebarNote />

                                {/* Payment Detail */}
                                <SidebarPaymentDetail data={selectedCar} onSubmit={this.handleSubmitCar} showButton/>

                            </Sidebar>
                        </div>
                    </div>
                </div>
            </Main>
        );
    }
}

export default CustomerGlobal(withRouter(Order));