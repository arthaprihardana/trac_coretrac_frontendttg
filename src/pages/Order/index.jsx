/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-23 21:17:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 00:42:46
 */
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { delay, defaultDidMountSetStyle, defaultWillUnmountSetStyle } from 'helpers';
import { CustomerGlobal } from "context/RentContext";
import { connect } from "react-redux";

// components
import { Main } from "templates";
import { CarDetail, Sidebar, SidebarBookingDetail, SidebarNote, TncList, TncListMobile } from "organisms";
import {
    OrderSteps,
    SidebarPromoCode,
    SidebarPaymentDetail
} from "molecules";
import ThisOrderWrapper from "./ThisOrderWrapper";

// helpers
import { localStorageDecrypt } from "helpers";

// style
import "./Order.scss";

// db
import db from '../../db';

// Order Steps
class Order extends Component {
    state = {
        selectedCar: [],
        bookingDetail: {},
	    showDetailMobile: false,
        showConfirmMobile: false,
        isLoading: false
    };

    componentDidMount() {
	    defaultDidMountSetStyle();
        document.body.classList.add("add-padding-top");
        document.body.classList.add("background-grey");
        
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        let selectedCar = JSON.parse(localStorage.getItem('selectedCar'));
        if(selectedCar !== null) {
            this.setState({
                selectedCar: selectedCar,
                bookingDetail: CarRentalFormInput
            });
        } else {
            this.props.history.replace('/');
        }
    }
    
    componentWillUnmount() {
        defaultWillUnmountSetStyle();
    }

    extrasChange = value => {
        this.setState(
            {
                selectedCar: value
            },
            () => {
                localStorage.setItem('selectedCar', JSON.stringify(this.state.selectedCar));
                if (this.state.selectedCar.length < 1) {
                    this.props.history.push("/transport-list/car");
                }
            }
        );
    };

    handleSubmitCar = async () => {
        const {
            history: {
                push,
                location
            },
            match: {
                params: {
                    id
                }
            },
            notes
        } = this.props;
        this.setState({ isLoading: true });
        let local_auth = localStorageDecrypt('_aaa', 'object');
        let url;
        switch (id) {
            case 'airport-transfer':
                // url = '/flight-detail';
                // delay(2000).then(() => {
                //     this.setState({ isLoading: false })
                //     push(url)
                // });
                if(local_auth !== null) {
                    url = '/personal-detail/airport-transfer';
                    delay(2000).then(() => {
                        this.setState({ isLoading: false })
                        push(url)
                    })
                } else {
                    // localStorage.setItem('last_location', JSON.stringify(location));
                    url = '/personal-detail-login/airport-transfer';
                    delay(2000).then(() => {
                        this.setState({ isLoading: false })
                        push(url)
                    });
                }
                break;
            case 'bus-rental':
                // url = '/pickup-details';
                if(local_auth !== null) {
                    url = '/personal-detail/bus-rental';
                    delay(2000).then(() => {
                        this.setState({ isLoading: false })
                        push(url)
                    })
                } else {
                    // localStorage.setItem('last_location', JSON.stringify(location));
                    url = '/personal-detail-login/bus-rental';
                    delay(2000).then(() => {
                        this.setState({ isLoading: false })
                        push(url)
                    });
                }
                break;
            default:
                if(local_auth !== null) {
                    url = '/personal-detail/car-rental';
                    delay(2000).then(() => {
                        this.setState({ isLoading: false })
                        push(url)
                    })
                } else {
                    // localStorage.setItem('last_location', JSON.stringify(location));
                    url = '/personal-detail-login/car-rental';
                    delay(2000).then(() => {
                        this.setState({ isLoading: false })
                        push(url)
                    });
                }
                break;
        }
    }

    tncMobileClose = () => {
        this.setState({
            showConfirmMobile: false,
            showDetailMobile: true
        })
    }

    render() {
	    const {
            props: {
                match: {
                    params: {
                        id
                    }
                }
            },
            state: {
                selectedCar,
                bookingDetail,
                showDetailMobile,
                showConfirmMobile,
                isLoading
            },
            tncMobileClose,
            extrasChange
        } = this;
        return (
            selectedCar.length > 0 ?
            <Main solid hideMenu headerClose headerUserLogin>
                {/* order steps */}
                <OrderSteps activeStep={1} type={id} />
                <ThisOrderWrapper>
                    <CarDetail type={id} data={this.state.selectedCar} onExtrasChange={extrasChange} onRemove={extrasChange} onSubmit={() => this.setState({ showDetailMobile: true })} />
                    <Sidebar showDetail={showDetailMobile} returnShowDetail={() => this.setState({ showDetailMobile: false })}>
                        <SidebarBookingDetail data={bookingDetail} type={id} />
                        <SidebarPromoCode data={selectedCar} />
                        <SidebarNote />
                        <SidebarPaymentDetail type={id} data={selectedCar} onSubmit={this.handleSubmitCar} showButton isLoading={isLoading} />
                        <TncList triggerTnc={() => this.setState({ showDetailMobile: false, showConfirmMobile: true })} onSubmit={this.handleSubmitCar} />
                    </Sidebar>
                    {
                        showConfirmMobile && <TncListMobile close={tncMobileClose} />
                    }
                </ThisOrderWrapper> 
            </Main>
            : <div />
        );
    }
}

const mapStateToProps = ({ carrental }) => {
    const { selectedcar, notes } = carrental;
    return { selectedcar, notes }
}

// export default connect(mapStateToProps, {})(CustomerGlobal(withRouter(Order)));
export default CustomerGlobal(
    withRouter(
        connect(mapStateToProps, {

        })(Order)
    )
)