import React from "react";
import PropTypes from "prop-types";

const StyleGuideBoxColumn = ({ children }) => (
    <div className="style-guide-box-column">{children}</div>
);

StyleGuideBoxColumn.propTypes = {
    children: PropTypes.any // eslint-disable-line
};

export default StyleGuideBoxColumn;