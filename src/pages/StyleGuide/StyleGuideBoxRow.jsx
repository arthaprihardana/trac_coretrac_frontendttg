import React from "react";
import PropTypes from "prop-types";

const StyleGuideBoxRow = ({ children }) => (
    <div className="style-guide-box-row">{children}</div>
);

StyleGuideBoxRow.propTypes = {
    children: PropTypes.any // eslint-disable-line
};

export default StyleGuideBoxRow;