import React from "react";
import PropTypes from "prop-types";

const StyleGuideBox = ({ id, title, children, ...props }) => (
    <div className="style-guide-box" id={id} {...props}>
        <div className="style-guide-box-title">{title}</div>
        <div className="style-guide-box-content">{children}</div>
    </div>
);

StyleGuideBox.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    children: PropTypes.any // eslint-disable-line
};

export default StyleGuideBox;