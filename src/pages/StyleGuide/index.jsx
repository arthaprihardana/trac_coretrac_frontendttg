import React, { PureComponent, Fragment } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";

// style
import "./StyleGuide.scss";
import logo from "assets/images/logo/trac-logo-white.svg";

// // assets
// import BlogImage1 from "assets/images/dummy/blog-primary.jpg";
// import BlogImage2 from "assets/images/dummy/blog-secondary-1.jpg";
// import BlogImage3 from "assets/images/dummy/blog-secondary-2.jpg";
// import BlogImage4 from "assets/images/dummy/blog-secondary-3.jpg";
// import BlogImage5 from "assets/images/dummy/blog-secondary-4.jpg";
// // dummy image popular destination
// import DestinationImage1 from "assets/images/dummy/destination-1.jpg";
// import DestinationImage2 from "assets/images/dummy/destination-2.jpg";
// import DestinationImage3 from "assets/images/dummy/destination-3.jpg";
// import DestinationImage4 from "assets/images/dummy/destination-4.jpg";
// import DestinationImage5 from "assets/images/dummy/destination-5.jpg";

// components
import { Button, Image, Text, TracField, Cover, Modal } from "atom";
import {
    KitchenSink,
    CardInfo,
    ListBlog,
    PopularDestinations,
    Slideshow,
    ModalSuccess
} from "molecules";
import { Counter, ListDeal } from "organisms";
import StyleGuideBoxRow from "./StyleGuideBoxRow";
import StyleGuideBoxColumn from "./StyleGuideBoxColumn";
import StyleGuideBox from "./StyleGuideBox";
import StyleGuideNav from "./StyleGuideNav";

class StyleGuide extends PureComponent {
    state = {
        counter: 0,
        modal: {
            default: false,
            success: false
        },
        form: {
            checkbox1: false,
            checkbox2: true,
            radio1: false,
            radio2: true,
            type: "password",
            icon: "password hide"
        },
        listMobileOrder: [
            {
                id: 1,
                mobileName: "xenia",
                sumOrder: 0
            },
            {
                id: 2,
                mobileName: "avanza",
                sumOrder: 0
            }
        ],
        showItem: false,
        selectedValue: "red"
    };

    handleValueChange = (value, obj) => {
        let updateListMobil = [...this.state.listMobileOrder];
        let index = updateListMobil.findIndex(mobil => mobil.id === obj.id);
        updateListMobil[index].sumOrder = value;

        this.setState({
            listMobileOrder: updateListMobil
        });
    };

    handleModalDefaultOpen = () => {
        this.setState({ modal: { default: true } });
    };

    handleModalDefaultClose = e => {
        e.stopPropagation();
        this.setState({ modal: { default: false } });
    };

    handleModalSuccessOpen = () => {
        this.setState({ modal: { success: true } });
    };

    handleModalSuccessClose = e => {
        e.stopPropagation();
        this.setState({ modal: { success: false } });
    };

    handleRadioChange = () => {
        const {
            form,
            form: { radio1, radio2 }
        } = this.state;
        this.setState(prevState => ({
            ...prevState,
            form: {
                ...form,
                radio1: !radio1,
                radio2: !radio2
            }
        }));
    };

    handleCheckbox1Change = () => {
        const {
            form,
            form: { checkbox1 }
        } = this.state;
        this.setState(prevState => ({
            ...prevState,
            form: {
                ...form,
                checkbox1: !checkbox1
            }
        }));
    };

    handleCheckbox2Change = () => {
        const {
            form,
            form: { checkbox2 }
        } = this.state;
        this.setState(prevState => ({
            ...prevState,
            form: {
                ...form,
                checkbox2: !checkbox2
            }
        }));
    };

    handleShowPassword = e => {
        e.preventDefault();
        const { form } = this.state;

        if (form.type === "password" && form.icon === "password hide") {
            this.setState(prevState => ({
                ...prevState,
                form: {
                    ...form,
                    type: "text",
                    icon: "password show"
                }
            }));
        } else {
            this.setState(prevState => ({
                ...prevState,
                form: {
                    ...form,
                    type: "password",
                    icon: "password hide"
                }
            }));
        }
    };

    handleClickSelectMenu = e => {
        e.preventDefault();
        this.setState(prevState => ({
            ...prevState,
            showItem: !prevState.showItem
        }));
    };

    handleItemClick = (e, v) => {
        e.preventDefault();
        this.setState({ selectedValue: v, showItem: false });
    };

    componentDidMount = () => {
        document.body.classList.add("style-guide");
    }

    render() {
        const { modal, form, showItem, selectedValue } = this.state;

        return (
            <Fragment>
                <div className="p-style-guide">
                    <div className="style-guide-header">
                        <img
                            src={logo}
                            alt="Logo Astras"
                            className="track-logo"
                        />
                        <div className="right-section">
                            <h4>Styleguide Template</h4>
                            <h5>Created by Aleph</h5>
                        </div>
                    </div>
                    <div className="style-guide-navigation">
                        <h4>Elements</h4>
                        <ul>
                            <StyleGuideNav
                                title="Kitchen Sink"
                                id="kitchenSink"
                            />
                            <StyleGuideNav title="Button" id="button" />
                            <StyleGuideNav title="Image" id="image" />
                            <StyleGuideNav title="Cover" id="cover" />
                            <StyleGuideNav title="Text" id="text" />
                            <StyleGuideNav title="List Deal" id="listDeal" />
                            <StyleGuideNav title="Card Info" id="cardInfo" />
                            <StyleGuideNav title="Blog List" id="ListBlog" />
                            <StyleGuideNav
                                title="Popular Destinations"
                                id="popularDestinations"
                            />
                            <StyleGuideNav title="Slide Show" id="slideShow" />
                            <StyleGuideNav title="TracField" id="tracField" />
                            <StyleGuideNav title="Counter" id="counter" />
                            <StyleGuideNav title="Modal" id="modal" />
                        </ul>
                    </div>
                </div>
                <div className="style-guide-content">
                    <div className="row">
                        <div className="col-sm-12">
                            <StyleGuideBox
                                title="Kitchen Sink"
                                id="kitchenSink"
                            >
                                <KitchenSink />
                            </StyleGuideBox>

                            <StyleGuideBox title="Button" id="button">
                                <h5>Button</h5>
                                <div className="style-guide-space-y style-guide-space">
                                    <Button type="button">Default</Button>
                                    <Button type="button" primary>
                                        Primary
                                    </Button>
                                    <Button type="button" secondary>
                                        Secondary
                                    </Button>
                                    <Button type="button" outline>
                                        Outline
                                    </Button>
                                    <Button type="button" disabled>
                                        Disabled
                                    </Button>
                                    <Button type="button" outlineDisabled>
                                        Outline Disabled
                                    </Button>
                                    <Button type="button" text>
                                        Text Button
                                    </Button>
                                </div>
                                <h5>Link</h5>
                                <div className="style-guide-space-y style-guide-space">
                                    <Button link="#to">Default</Button>
                                    <Button primary link="#to">
                                        Primary
                                    </Button>
                                    <Button secondary link="#to">
                                        Secondary
                                    </Button>
                                    <Button outline link="#to">
                                        Outline
                                    </Button>
                                    <Button disabled link="#to">
                                        Disabled
                                    </Button>
                                    <Button outlineDisabled link="#to">
                                        Outline Disabled
                                    </Button>
                                    <Button text link="#to">
                                        Text Button
                                    </Button>
                                </div>
                            </StyleGuideBox>

                            <StyleGuideBox title="Image" id="image">
                                <StyleGuideBoxColumn>
                                    <Image tiny />
                                    <Image small />
                                    <Image medium />
                                    <Image large />
                                    <Image big />
                                </StyleGuideBoxColumn>
                            </StyleGuideBox>

                            <StyleGuideBox title="Cover" id="cover">
                                <Cover />
                            </StyleGuideBox>

                            <StyleGuideBox title="Text" id="text">
                                <Text type="h1" />
                                <Text type="h2" />
                                <Text type="h3" />
                                <Text type="h4" />
                                <Text type="h5" />
                                <Text type="h6" />
                                <Text indent={1}>
                                    Indented: Lorem ipsum dolor sit amet...
                                </Text>
                                <Text indent={2}>
                                    Indented: Lorem ipsum dolor sit amet...
                                </Text>
                                <Text indent={3}>
                                    Indented: Lorem ipsum dolor sit amet...
                                </Text>
                            </StyleGuideBox>

                            <StyleGuideBox title="List Deal" id="listDeal">
                                <div className="container">
                                    <ListDeal />
                                </div>
                            </StyleGuideBox>

                            <StyleGuideBox title="CardInfo" id="cardInfo">
                                <StyleGuideBoxRow>
                                    <CardInfo
                                        iconName="booking process"
                                        title="Fast booking process"
                                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
                                    />
                                    <CardInfo
                                        iconName="car subtitution"
                                        title="Car Subtitution"
                                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
                                    />
                                    <CardInfo
                                        iconName="coverage insurance"
                                        title="Coverage Insurance"
                                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
                                    />
                                    <CardInfo
                                        iconName="premium car"
                                        title="Premium Quality Car"
                                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
                                    />
                                </StyleGuideBoxRow>
                            </StyleGuideBox>

                            <StyleGuideBox title="Blog List" id="ListBlog">
                                <div className="m-blog-list">
                                    <ListBlog
                                        image={"https://dl.dropboxusercontent.com/s/inmiak52hz1ug6e/blog-primary.jpg"}
                                        label="News"
                                        title="Where to go in September"
                                        lead="Is there a better time to hit the road than September? The great
                              shoulder season of travel offers prime conditions for exploring
                              a vast array of big-name destinations from Bandung to Bali"
                                        date="Jul 6, 2017"
                                        linkTo="/detail-blog"
                                        linkText="Read More"
                                        fullWidth
                                    />
                                    <ListBlog
                                        image={"https://dl.dropboxusercontent.com/s/oyhbzpxl4vwf6q3/blog-secondary-1.jpg"}
                                        label="News"
                                        title="Lorem Ipsum dolor sit amet sid ut ima "
                                        lead="Is there a better time to hit the road than September? The great
                              shoulder season of travel offers prime conditions for exploring
                              a vast array of big-name destinations from Bandung to Bali"
                                        date="Jul 6, 2017"
                                        linkTo="/detail-blog"
                                        linkText="Read More"
                                    />
                                    <ListBlog
                                        image={"https://dl.dropboxusercontent.com/s/epltbwphjncbmaw/blog-secondary-2.jpg"}
                                        label="News"
                                        title="Lorem Ipsum dolor sit amet sid ut ima "
                                        lead="Is there a better time to hit the road than September? The great
                              shoulder season of travel offers prime conditions for exploring
                              a vast array of big-name destinations from Bandung to Bali"
                                        date="Jul 6, 2017"
                                        linkTo="/detail-blog"
                                        linkText="Read More"
                                    />
                                    <ListBlog
                                        image={"https://dl.dropboxusercontent.com/s/tf19tf7lvrpl281/blog-secondary-3.jpg"}
                                        label="News"
                                        title="Lorem Ipsum dolor sit amet sid ut ima "
                                        lead="Is there a better time to hit the road than September? The great
                              shoulder season of travel offers prime conditions for exploring
                              a vast array of big-name destinations from Bandung to Bali"
                                        date="Jul 6, 2017"
                                        linkTo="/detail-blog"
                                        linkText="Read More"
                                    />
                                    <ListBlog
                                        image={"https://dl.dropboxusercontent.com/s/lqs19kakomdaakl/blog-secondary-4.jpg"}
                                        label="News"
                                        title="Lorem Ipsum dolor sit amet sid ut ima "
                                        lead="Is there a better time to hit the road than September? The great
                              shoulder season of travel offers prime conditions for exploring
                              a vast array of big-name destinations from Bandung to Bali"
                                        date="Jul 6, 2017"
                                        linkTo="/detail-blog"
                                        linkText="Read More"
                                    />
                                </div>
                            </StyleGuideBox>

                            <StyleGuideBox
                                title="Popular Destinations"
                                id="popularDestinations"
                            >
                                <div className="m-popular-destinations">
                                    <PopularDestinations
                                        destinationImage={"https://dl.dropboxusercontent.com/s/rmthar88nj3k4pc/destination-1.jpg"}
                                        destinationTitle="Bali"
                                        destinationLink="/detail-destination"
                                    />
                                    <PopularDestinations
                                        destinationImage={"https://dl.dropboxusercontent.com/s/2t2k556ft6s1ipr/destination-2.jpg"}
                                        destinationTitle="Belitung"
                                        destinationLink="/detail-destination"
                                    />
                                    <PopularDestinations
                                        destinationImage={"https://dl.dropboxusercontent.com/s/377zz9p0smpuwau/destination-3.jpg"}
                                        destinationTitle="Bandung"
                                        destinationLink="/detail-destination"
                                    />
                                    <PopularDestinations
                                        destinationImage={"https://dl.dropboxusercontent.com/s/yjtk4qndrwx8sao/destination-4.jpg"}
                                        destinationTitle="Yogyakarta"
                                        destinationLink="/detail-destination"
                                    />
                                    <PopularDestinations
                                        destinationImage={"https://dl.dropboxusercontent.com/s/d1itx5ami8szjft/destination-5.jpg"}
                                        destinationTitle="Surabaya"
                                        destinationLink="/detail-destination"
                                    />
                                </div>
                            </StyleGuideBox>

                            <StyleGuideBox title="Slideshow" id="slideshow">
                                <Slideshow />
                            </StyleGuideBox>

                            <StyleGuideBox
                                style={{ overflow: "visible" }}
                                title="TracField"
                                id="tracField"
                            >
                                <Formik
                                    initialValues={{
                                        email: "",
                                        password: "",
                                        radio1: "",
                                        radio2: "",
                                        checkbox1: "",
                                        checkbox2: ""
                                    }}
                                    validationSchema={Yup.object().shape({
                                        email: Yup.string()
                                            .email()
                                            .required(),
                                        password: Yup.string().required()
                                    })}
                                >
                                    {({ handleChange, handleBlur, values }) => (
                                        <Form>
                                            <TracField
                                                id="email"
                                                type="text"
                                                name="email"
                                                labelName="Email"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.email}
                                            />
                                            <div
                                                style={{ padding: "15px 0" }}
                                            />
                                            <TracField
                                                id="password"
                                                type={form.type}
                                                name="password"
                                                labelName="Password"
                                                icon={form.icon}
                                                onChange={handleChange}
                                                onClick={
                                                    this.handleShowPassword
                                                }
                                                onBlur={handleBlur}
                                                value={values.password}
                                            />
                                            <div
                                                style={{ padding: "15px 0" }}
                                            />
                                            <TracField
                                                id="radio1"
                                                type="radio"
                                                name="radio1"
                                                labelName="Radio 1"
                                                onChange={
                                                    this.handleRadioChange
                                                }
                                                onBlur={handleBlur}
                                                checked={form.radio1}
                                                value={values.radio1}
                                            />
                                            <div
                                                style={{ padding: "15px 0" }}
                                            />
                                            <TracField
                                                id="radio2"
                                                type="radio"
                                                name="radio2"
                                                labelName="Radio 2"
                                                labelSize="small"
                                                onChange={
                                                    this.handleRadioChange
                                                }
                                                onBlur={handleBlur}
                                                checked={form.radio2}
                                                value={values.radio2}
                                            />
                                            <div
                                                style={{ padding: "15px 0" }}
                                            />
                                            <TracField
                                                id="checkbox1"
                                                type="checkbox"
                                                name="checkbox1"
                                                labelName="Checkbox 1"
                                                onChange={
                                                    this.handleCheckbox1Change
                                                }
                                                onBlur={handleBlur}
                                                value={values.checkbox1}
                                                checked={form.checkbox1}
                                            />
                                            <div
                                                style={{ padding: "15px 0" }}
                                            />
                                            <TracField
                                                id="checkbox2"
                                                type="checkbox"
                                                name="checkbox2"
                                                labelName="Checkbox 2"
                                                labelSize="small"
                                                onChange={
                                                    this.handleCheckbox2Change
                                                }
                                                onBlur={handleBlur}
                                                value={values.checkbox2}
                                                checked={form.checkbox2}
                                            />
                                            <div
                                                style={{ padding: "15px 0" }}
                                            />
                                            <TracField
                                                id="range"
                                                type="range"
                                                onChange={v => console.log(v)}
                                            />
                                            <div
                                                style={{ padding: "15px 0" }}
                                            />
                                            <TracField
                                                id="select"
                                                type="select"
                                                name="color"
                                                labelName="Color"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                showItem={showItem}
                                                onClick={
                                                    this.handleClickSelectMenu
                                                }
                                                onItemClick={
                                                    this.handleItemClick
                                                }
                                                value={selectedValue}
                                            />
                                        </Form>
                                    )}
                                </Formik>
                            </StyleGuideBox>

                            <StyleGuideBox title="Counter" id="counter">
                                <h5>Default</h5>
                                <div className="style-guide-space-y">
                                    <Counter
                                        onValueChange={this.handleValueChange}
                                    />
                                </div>
                                <h5>Rounded</h5>
                                <div className="style-guide-space-y">
                                    {this.state.listMobileOrder.map(mobil => {
                                        return (
                                            <Fragment key={mobil.id}>
                                                <p>
                                                    Nama Mobil:{" "}
                                                    {mobil.mobileName}
                                                </p>
                                                <p>
                                                    Jumlah Order:{" "}
                                                    {mobil.sumOrder}
                                                </p>
                                                <Counter
                                                    rounded
                                                    onValueChange={data =>
                                                        this.handleValueChange(
                                                            data,
                                                            mobil
                                                        )
                                                    }
                                                />
                                            </Fragment>
                                        );
                                    })}
                                </div>
                            </StyleGuideBox>

                            <StyleGuideBox title="Modal" id="modal">
                                <div className="style-guide-space-y style-guide-space">
                                    <Button
                                        primary
                                        onClick={this.handleModalDefaultOpen}
                                    >
                                        Open Modal Default
                                    </Button>
                                    <Button
                                        primary
                                        onClick={this.handleModalSuccessOpen}
                                    >
                                        Open Modal Success
                                    </Button>
                                    <Modal
                                        open={modal.default}
                                        onClick={this.handleModalDefaultClose}
                                        header={{
                                            withCloseButton: true,
                                            onClick: this
                                                .handleModalDefaultClose,
                                            children: (
                                                <Text type="h3">
                                                    Modal Header
                                                </Text>
                                            )
                                        }}
                                        content={{
                                            children:
                                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                                        }}
                                        footer={{
                                            position: "right",
                                            children: (
                                                <Button
                                                    primary
                                                    onClick={
                                                        this
                                                            .handleModalDefaultClose
                                                    }
                                                >
                                                    Ok
                                                </Button>
                                            )
                                        }}
                                    />
                                    <ModalSuccess
                                        open={modal.success}
                                        size="medium"
                                        onCloseModal={
                                            this.handleModalSuccessClose
                                        }
                                    />
                                </div>
                            </StyleGuideBox>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default StyleGuide;