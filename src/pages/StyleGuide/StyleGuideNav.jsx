import React from "react";
import PropTypes from "prop-types";

const StyleGuideNav = ({ id, title }) => (
    <li>
        <a href={`#${id}`}>{title}</a>
    </li>
);

StyleGuideNav.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
};

export default StyleGuideNav;