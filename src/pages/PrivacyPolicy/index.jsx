import React, { PureComponent, Fragment } from "react";
import bannerImage from '../../assets/images/trac/PrivacyPolicy.png';
// components
import { Main, Static} from "templates";
import { Accordion } from 'organisms';
import "./PrivacyPolicy.scss";
import "../../organisms/Footer/Footer.scss";
import "./ContentList";
import ContentList from "./ContentList";
import contentDataJSON from '../../assets/data-dummy/terms-and-conditions.json';
import new_privacy from '../../assets/data-master/privacy_policy'

class PrivacyPolicy extends PureComponent {
    state = {
        data : "",
        btnobj : [
            {label:"Aturan Privasi", id:1},
            {label:"Token TRAC", id:2},
            {label:"Cookie", id:3}
        ],
        queobj : [],
        breadCrumb: [
            {
                label: 'Home',
                link: '/'
            },
            {
                label: 'Syarat dan Ketentuan',
                link: '/privacy-policy'
            },
            {
                label: 'Aturan Privasi',
                link: '/privacy-policy'
            },
        ]
    };

    componentDidMount() {
        document.body.classList.add("add-padding-top");
        this.removeActiveButton()
        document.querySelectorAll(".side-badges-pp")[0].classList.add('.active');
        this.setState({
            data : "Aturan Privasi",
            queobj : new_privacy.pengenalan
        });
    }

    createMarkup = (html) => {
        return {__html: html};
    };

    removeActiveButton = () => {
        for(let i = 0; i <= 2;i++){
            document.querySelectorAll(".side-badges-pp")[i].classList.remove('active');
        }
    }

    componentWillUnmount() {
        document.body.classList.remove("add-padding-top");
    }

    changeType = (i, j) => {
        this.removeActiveButton();
        if(i === 1){
            this.setState({
                data : j,
                queobj : new_privacy.pengenalan
            });
        }else 
        if(i === 2){
            this.setState({
                data : j,
                queobj : new_privacy.token
            });
        }else 
        if(i === 3){
            this.setState({
                data : j,
                queobj : new_privacy.cookie
            });
        }
        
    }

    render() {  
        const { queobj } =this.state;
        return (
            <Main solid >
                <Static title="Privacy Policy" breadCrumb={this.state.breadCrumb} bannerImg={bannerImage}>
                </Static>
                
                <div className="tnc-content-wrapper row">
                    <div className="tnc-button-list column-pp left-pp">
                        {this.state.btnobj.map((item, index) => (
                            <ButtonList key={index} btnClick={this.changeType} label={item.label} id={item.id} />
                        ))}
                    </div>
                    <div className="tnc-content-list column-pp right-pp">
                        <div dangerouslySetInnerHTML={this.createMarkup(queobj)} style={{ paddingLeft: 20,
                            textAlign: 'justify',
                            paddingRight: 54 }} />
                        {/*<ContentList data={this.state.data} questlist= {this.state.queobj.map((item, index) => (*/}
                            {/*<Question key={index} question={item.question} answer={item.answer} />*/}
                        {/*))}/>*/}
                    </div>
                </div>
            </Main>
        );
    }
}

class ButtonList extends React.Component {

    selectData = (e) => {

        let selector = e.currentTarget;
        if (selector.classList.contains("active")) {
            selector.classList.remove("active");
        } else {
            selector.classList.add("active");
        }
        this.props.btnClick(this.props.id, this.props.label)
        console.log(this.props.label);
    }

    render() {
        return(
            <button 
                id={this.props.id}
                type="buttons" 
                className="side-badges-pp" 
                onClick={e => this.selectData(e)} >
                {this.props.label}
            </button>
        );
    }
}

class Question extends React.Component {
    render() {
        return(
            <Fragment>
                {/* <h4>{this.props.question}</h4> */}
                <p>{this.props.answer}</p>
            </Fragment>
            // <Accordion
            //     title={this.props.question}
            //     defaultValue={false}
            // >
            //     <p>{this.props.answer}</p> 
            // </Accordion>
        );
    }
}
export default PrivacyPolicy;

