import React, { Component, Fragment } from 'react';
import { Main, Static } from 'templates';
import './ProductService.scss'

//assets
import BannerProduct from 'assets/images/trac/ProductService.png';
import PageLanding1 from 'assets/images/trac/CorporateLease.png';
import PageLanding2 from 'assets/images/trac/BusServices.png';
import ImgItem1 from 'assets/images/trac/company/CorporateLease.jpg'
import ImgItem2 from 'assets/images/trac/company/PersonalRental.jpg'
import ImgItem3 from 'assets/images/trac/company/AstraFMS.jpg'
import ImgItem4 from 'assets/images/trac/company/bus_lease.png'
import lang from '../../assets/data-master/language'

class ProductService extends Component{
    state={
        product1:[
            {
                id:1,
                image:ImgItem1,
                title:lang.corporateCarService[lang.default],
                description:"Manfaatkan solusi transportasi yang menyeluruh untuk beragam kebutuhan bisnis Anda. Kami menjamin layanan transportasi yang aman, andal, dan terukur. Anda pun dapat memfokuskan energi dan sumber daya Anda untuk mengembangkan bisnis dalam menghadapi tantangan masa depan.",
                class:"landing-product",
                link:"/corporate-car-service",
            },
            {
                id:2,
                image:PageLanding1,
                title:lang.dailyCarRental[lang.default],
                description:"Kami menyediakan layanan Rental Mobil dan Airport Transfer untuk semua aktivitas harian Anda. Perjalanan dalam dan luar kota, liburan, bisnis, atau mobilitas sehari-hari kian mudah dengan reservasi online melalui situs web dan aplikasi TRAC. Pilih rental mobil dengan pengemudi terlatih kami atau kendarai sendiri mobil Anda. Mudah, aman, dan terjamin.",
                class:"landing-product page",
                link:"/detail-product-rental",
            },
            {
                id:3,
                image:ImgItem4,
                title:lang.busService[lang.default],
                description:"Rencanakan perjalanan yang aman dan nyaman bersama sahabat, keluarga, atau kolega dengan layanan Sewa Bus dari TRAC. Liburan, arisan, reuni, trip kantor atau sekolah menjadi lebih menyenangkan. Reservasi pun mudah dengan pemesanan online via situs web dan aplikasi TRAC yang bisa diakses pengguna Android maupun iOS.",
                class:"landing-product",
                link:"/bus-services",
            },
            {
                id:4,
                image:ImgItem3,
                title:"TRAC Fleet Management Solution",
                description:"Kelola transportasi perusahaan secara optimal dengan memanfaatkan sistem transportasi yang memadukan teknologi aplikasi berbasis cloud dan internet of things. Kami merancang TRAC FMS untuk membantu Anda mengelola dan mengawasi transportasi dengan mudah, sekaligus menyediakan analisis berbasis data yang terukur dan akurat.",
                class:"landing-product page",
                link:"/detail-product-trac-data",
            }
        ]
    };

    

    render(){
        return(
            <Fragment>
                <Main className="product-service page">
                    <Static title={lang.productService[lang.default]} subTitle='' bannerImg={BannerProduct}>
                    </Static>
                    {
                        this.state.product1.map(product =>{
                            return(
                                <div key={product.id} className="page-landing product">
                                        <img className={product.class} src={product.image} alt="landing1"/>
                                    <div className="teks-landing">
                                        <div className="container-txt">
                                            <h2>{product.title}</h2>
                                            <p>{product.description}</p>
                                            <a className="a-btn btn-text" href={product.link}>READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                    
                </Main>
            </Fragment>
        )
    }
}
export default ProductService;