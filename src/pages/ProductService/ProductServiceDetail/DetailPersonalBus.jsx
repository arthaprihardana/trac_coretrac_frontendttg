import React, { Component, Fragment } from 'react';
import { Main, Static } from 'templates';

//assets
import LandingRental from 'assets/images/dummy/personal-rental.png';

//thisComponent
import PersonalAirport from './thisComponentPersonal/PersonalAirport';
import PersonalBus from './thisComponentPersonal/PersonalBus';

class DetailPersonalBus extends Component{
    state={
        breadCrumb: [
            {
                label: 'Home',
                link: '/'
            },
            {
                label: 'Product Services',
                link: '/product-services'
            },
            {
                label: 'Personal Rental',
                link: '/detail-product-rental'
            },
        ],
        carLeaseImage:[
            {
                id:1,
                cardCar:"card-detail",
                leaseCar:"car-lease detail",
                title:"DAILY CAR RENTAL",
                link:"/detail-product-rental",
            },
            {
                id:2,
                cardCar:"card-detail",
                leaseCar:"drive-lease detail",
                title:"AIRPORT TRANSFER",
                link:"/detail-product-airport",
            },
            {
                id:3,
                cardCar:"card-detail active",
                leaseCar:"bus-lease detail",
                title:"BUS RENTAL",
                link:"/detail-product-bus-rental",
            }
        ]
    }

    componentDidMount() {

        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': 0
        });
    }

    render(){
    return(
        <Fragment>
            <Main className="landing-corporate">
                <Static title="Personal Rental" bannerImg={LandingRental} breadCrumb={this.state.breadCrumb} >
                    <div className="container">
                        <h2 className="title-corporate">Aktivitas Lancar, Perjalanan Tetap Nyaman</h2>
                        <p className="desc-corporate">Semua orang, termasuk Anda, berhak untuk menikmati layanan transportasi yang aman dan nyaman, termasuk untuk keperluan perjalanan jangka pendek. Kami menyediakan layanan sewa kendaraan harian untuk semua aktivitas Anda. Liburan, perjalanan bisnis di luar kota, maupun acara bersama rombongan keluarga menjadi lebih menyenangkan. Semua layanan kami terjamin aman, nyaman, dengan akses reservasi yang mudah. Anda dapat melakukan reservasi secara online, via telpon, chat Whatsapp, maupun email. Semua serba mudah dan cepat.</p>
                    </div>
                    {/*<h2 className="lease-paragraph">Pilih Layanan yang Tepat untuk Perusahaan Anda</h2>*/}
                    {/*<div className="image-lease">*/}
                        {/*{*/}
                            {/*this.state.carLeaseImage.map( car =>{*/}
                                {/*return(*/}
                                    {/*<a style={{textDecoration:'none'}} href={car.link} key={car.id} className={car.cardCar}>*/}
                                        {/*<a href={car.link} className={car.leaseCar}></a>*/}
                                        {/*<h4 className="center-text">{car.title}</h4>*/}
                                    {/*</a>*/}
                                {/*)*/}
                            {/*})*/}
                        {/*}*/}
                    {/*</div>*/}
                    <PersonalBus/>
                </Static>
            </Main>
        </Fragment>    
    )
}

}
export default DetailPersonalBus;