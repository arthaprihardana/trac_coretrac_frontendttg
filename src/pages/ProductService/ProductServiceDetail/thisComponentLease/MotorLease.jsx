import React, { Component, Fragment } from 'react';


class MotorLease extends Component{
    render(){
        return(
            <Fragment>
                <h2 className="paragraph-lease">Motorcycle Lease Service</h2>
                <div className="content-car lease">
                    <p>Bagi banyak industri, moda transportasi roda dua menjadi faktor pendukung yang sangat vital. 
                        Oleh karena itu, TRAC juga menyediakan layanan sewa motor dengan jaminan kualitas unit yang prima. 
                        Layanan ini sangat tepat untuk bisnis Anda yang mengedepankan kecepatan dan ketepatan waktu di tengah-tengah kepadatan lalu lintas Ibu Kota. Anda pun tak perlu lagi repot mengelola dan melakukan perawatan unit, 
                        kami akan melakukannya untuk Anda. </p>            
                </div> 
            </Fragment>         
        )
    }
}
export default MotorLease;