import React, { Component, Fragment } from 'react';


class DriverLease extends Component{
    render(){
        return(
            <Fragment>
                <h2 className="paragraph-lease">Driver Service</h2>
                <div className="content-car lease">
                    <p>Untuk menunjang kebutuhan transportasi perusahaan, TRAC juga menyediakan layanan sewa kendaraan dengan pengemudi yang
                        telah terlatih secara profesional. Pengemudi TRAC direkrut dengan seleksi yang ketat serta dibekali beragam pelatihan dan
                        pembinaan yang komprehensif, salah satunya defensive driving dengan instruktur bersertifikat internasional agar para pengemudi
                        selalu mengedepankan keamanan selama berkendara. Semua pengemudi juga dibekali standar pelayanan TRAC dimana tiap
                        pengemudi tidak hanya terlatih secara skill teknis saja namun juga memiliki sikap dan penampilan yang santun untuk selalu dapat
                        melayani pelanggan dengan baik. </p>
                       <p>Pengemudi kami profesional, andal dan terlatih, siap mengantar Anda dengan aman dan nyaman sampai ke tujuan.</p>
                        <p> Hubungi Customer Assistance Center 1500009 untuk informasi untuk informasi lebih detail.</p>

                </div> 
            </Fragment>         
        )
    }
}
export default DriverLease;