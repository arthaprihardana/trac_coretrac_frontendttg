import React, { Component, Fragment } from 'react';

//assets
import LxryBus from 'assets/images/trac/content/bus_luxury.png';
import SmallBus from 'assets/images/trac/content/bus_small.png';
import MediumBus from 'assets/images/trac/content/bus_medium.png';
import BigBus from 'assets/images/trac/content/bus_big.png';

class BusLease extends Component{
    state={
        notifLease:[
            {
                id:1,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif timer",
                text:"Tepat Waktu",
            },
            {
                id:2,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif security",
                text:"Aman",
            },
            {
                id:3,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif comfort",
                text:"Nyaman",
            },
            {
                id:4,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif prodrive",
                text:"Pengemudi Profesional",
            },
            {
                id:5,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif achievement",
                text:"Layanan Berkualitas",
            },
        ],
        categoryBus1:[
            {
                id:1,
                textCategory:"LUXURY BUS",
                categoryImage:LxryBus,
                desc:"Spesifikasi",
                content:"11 kursi (dilengkapi LDC, earphones, dan reflexology system), Ruang rapat, Pantry, DVD Player, Wi-Fi, LCD TV, Soket listrik , AC, Sound system, toilet" , 
                categoryClass:"left-category", 
                titleClass:"category-luxury" , 
            },
            {
                id:2,
                textCategory:"SMALL BUS",
                categoryImage:SmallBus,
                desc:"Spesifikasi",
                content:"13, 15, 16 kursi, AC, LCD TV, DVD player, Sound system, Soket listrik.",
                categoryClass:"right-category", 
                titleClass:"category-small" , 
            },
        ],
        categoryBus2:[
            {
                id:3,
                textCategory:"MEDIUM BUS",
                categoryImage:MediumBus,
                desc:"Spesifikasi",
                content:"18, 23, 29, 31, 35 kursi, AC, LCD TV, DVD player, Sound system, Soket listrik, Bagasi, Wi-Fi (tersedia di sebagian unit bus)",
                categoryClass:"left-category",  
                titleClass:"category-medium" ,
            },
            {
                id:4,
                textCategory:"BIG BUS",
                categoryImage:BigBus,
                desc:"Spesifikasi",
                content:"59, 48, 45, 40 kursi, AC, LCD TV, DVD player, Sound system, Soket listrik Bagasi, Wi-Fi (tersedia di sebagian unit bus)",
                categoryClass:"right-category", 
                titleClass:"category-big" ,
            }

        ]
    }
    render(){
        return(
            <Fragment>
                <h2 className="paragraph-lease" style={{marginTop:20}}>SEWA BUS</h2>
                <div className="content-car lease">
                    <p> Dengan pengalaman melayani kebutuhan rental mobil seiring perkembangan waktu TRAC pun menghadirkan layanan sewa bus untuk melengkapi
                        layanan yang ditawarkan. Layanan sewa Bus TRAC dapat menjadi pilihan untuk kebutuhan transportasi group atau rombongan baik untuk
                        perjalanan di dalam ataupun luar kota, untuk kebutuhan korporasi maupun personal.</p>
                    <p>
                        <ul>
                            <li style={{color: '#000000'}}>Sewa Bus Pariwisata</li>
                            <li style={{listStyle: 'none'}}>TRAC Bus dapat melayani beragam kebutuhan perjalanan mulai dari perjalanan wisata, acara keluarga, trip kantor/sekolah, dll.</li>
                            <li style={{color: '#000000'}}>Shuttle</li>
                            <li style={{listStyle: 'none'}}>Kami juga dapat melayani kebutuhan layanan shuttle baik untuk korporasi seperti antar jemput karyawan, shuttle residence, antar
                                jemput sekolah, transportasi MICE, dll.</li>
                        </ul>
                    </p>
                    <p>
                        Setiap bus kami selalu dirawat dengan baik dan interior bus pun dibuat senyaman mungkin. Keamanan merupakan salah satu faktor yang menjadi
                        perhatian kami oleh karena itu setiap seat dilengkapi dengan sabuk pengaman dan untuk kenyamanan penumpang beberapa bus dilengkapi
                        dengan fasilitas wifi, LCD TV, toilet, perangkat karaoke dan pantry.
                    </p>
                    <p>
                        TRAC Bus menawarkan beragam variasi tipe bus mulai dari small bus, medium, big hingga luxury. Dan dengan pilihan konfigurasi jumlah kursi
                        mulai dari 11 hingga 59 seats.
                    </p>
                    <p>
                        Keselamatan dan keamanan penumpang juga menjadi prioritas kami. Setiap armada TRAC Bus juga dilengkapi dengan palu pemecah kaca, pintu
                        darurat, dan tabung pemadam kebakaran. Dan salah satu hal yang menjadi keunggulan kami adalah setiap pengemudi kami merupakan
                        pengemudi yang terlatih dan sudah berpengalaman sehingga kenyamanan dan keselamatan penumpang lebih terjamin.
                    </p>
                    <p>
                        Layanan Sewa Bus TRAC saat ini berbasis di Jakarta dan Surabaya namun kemanapun tujuan Anda dapat kami layani.
                    </p>

                </div> 
                {/*<div className="notif-lease">*/}
                    {/*{*/}
                        {/*this.state.notifLease.map( notif =>{*/}
                            {/*return(*/}
                                {/*<div key={notif.id} className={notif.leaseNotif}>*/}
                                    {/*<div className={notif.NotifImage}></div>*/}
                                    {/*<h4 className="text-color">{notif.text}</h4>*/}
                                {/*</div>*/}
                            {/*)*/}
                        {/*})*/}
                    {/*}    */}
                {/*</div>*/}
                <h4>Armada Kami</h4>
                <p>Kami memiliki beragam tipe armada bus yang dapat disesuaikan dengan kebutuhan Anda :</p>
                <div className="category-lease">
                    {
                        this.state.categoryBus1.map( category => {
                            return(
                                <div key={category.id} className={category.categoryClass}>
                                    <h2 className={category.titleClass}>{category.textCategory}</h2>
                                    <img src={category.categoryImage} alt="Luxury"/>
                                    <div className="text-spesifikasi">
                                        <p className="descrip" style={{fontWeight:700,color:'#636363'}}>{category.desc}</p>
                                        <p className="content-descrip" style={{paddingRight:20}}>{category.content}</p>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                <div className="category-lease">    
                    {
                        this.state.categoryBus2.map( category => {
                            return(
                                <div key={category.id} className={category.categoryClass}>
                                    <h2 className={category.titleClass}>{category.textCategory}</h2>
                                    <img src={category.categoryImage} alt="Luxury"/>
                                    <div className="text-spesifikasi">
                                        <p className="descrip" style={{fontWeight:700,color:'#636363'}}>{category.desc}</p>
                                        <p className="content-descrip" style={{paddingRight:20}}>{category.content}</p>
                                    </div>
                                </div>
                            )
                        })
                    }    
                </div>
                <div className="content-last">
                    <p>Kini, untuk mempermudah dalam melakukan reservasi Anda dapat memesan secara online melalui website dan aplikasi TRAC. Atau,
                        Anda bisa menghubungi Customer Assistance Center 1500009 dan mengirimkan email ke <a href="mailto:rco.nasional@trac.astra.co.id?Subject=Reservasi" target="_top">rco.nasional@trac.astra.co.id.</a>
                        Mari rencanakan perjalanan Anda sekarang dan ciptakan momen kebersamaan yang tak terlupakan!</p>
                </div>
            </Fragment>       
        )
    }
}
export default BusLease;