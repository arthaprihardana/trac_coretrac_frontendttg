import React, { Component, Fragment } from 'react';


class CarLease extends Component{
    render(){
        return(
            <Fragment>
                <h2 className="paragraph-lease">Rental Mobil Harian Korporat</h2>
                <div className="content-car lease">
                    <p>Perusahaan Anda memerlukan transportasi untuk kegiatan operasional karyawan yang berbasis pemakaian harian? Rental Mobil Harian
                        Korporat memungkinkan Anda menyewa mobil hanya ketika Anda membutuhkannya.</p>
                    <p><h4 style={{color:'black'}}>Durasi fleksibel</h4></p>
                    <p> Setiap pemesanan mobil dapat disesuaikan dengan durasi pemakaian yang Anda butuhkan. Minimal durasi pemakaian adalah 4 jam dan
                        dapat ditambah sewaktu-waktu sesuai keperluan Anda.</p>
                    <p><h4 style={{color:'black'}}>Reservasi online</h4></p>
                    <p> Didukung aplikasi reservasi korporat yang bisa diunduh di smartphone, pemesanan mobil pun dapat dilakukan secara online dari mana saja.
                        Karyawan yang sudah memiliki akses dapat melakukan login dan pemesanan secara mandiri melalui aplikasi korporat dari layar telepon
                        pintar mereka.</p>
                    <p><h4 style={{color:'black'}}>Atur pemakaian</h4></p>
                    <p> Anda bisa mengatur sendiri siapa saja di perusahaan Anda yang memiliki akses melakukan pemesanan, untuk keperluan karyawan atau
                        tamu penting. Tak hanya itu, Anda dapat memantau dan mengatur penggunaan mobil melalui dasbor khusus sehingga Anda memiliki
                        kontrol penuh.</p>
                    <p><h4 style={{color:'black'}}>Analisis berkala</h4></p>
                    <p> Kami juga akan membantu Anda menganalisis pemakaian transportasi berdasarkan hasil report berkala untuk memudahkan Anda
                        melakukan efisiensi biaya. Bahkan, Anda dapat mengatur biaya transportasi untuk setiap divisi di perusahaan Anda.
                        Hubungi Customer Assistance Center 1500009 untuk informasi untuk informasi lebih detail.</p>

                </div> 
            </Fragment>         
        )
    }
}
export default CarLease;