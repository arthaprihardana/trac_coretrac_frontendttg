import React, { Component, Fragment } from 'react';


class DriverLease extends Component{
    render(){
        return(
            <Fragment>
                <h2 className="paragraph-lease">Operating Lease</h2>
                <div className="content-car lease">
                    <p>Untuk mengakomodasi kebutuhan bisnis Anda, kami menyediakan layanan penyewaan kendaraan untuk masa sewa lebih dari 1 tahun. Layanan sewa kami
                        juga ditunjang dengan Pusat-Bantuan-Pelanggan 24 jam dan jaringan layanan yang telah tersebar di seluruh nusantara. </p>
                    <p>
                        Kami memiliki beragam pilihan moda transportasi yang dapat disesuaikan dengan kebutuhan transportasi bisnis Anda, mulai dari motor, mobil penumpang,
                        mobil niaga hingga bus. Tak hanya itu, kami juga siap melayani kebutuhan transportasi untuk perusahaan yang aktivitas bisnisnya di remote area seperti
                        industri pertambangan dan perkebunan. Apapun jenis bisnis Anda, TRAC siap melayani. Melengkapi layanan tersebut, kami juga memiliki layanan rental mobil
                        berbasis syariah melalui TRAC Syariah yang merupakan pionir layanan transportasi Syariah di Indonesia.
                    </p>

                        <h3>TRAC Syariah</h3>
                    <p>
                        Layanan berbasis sistem syariah semakin menjadi kebutuhan. Bagi Anda yang juga memiliki kebutuhan ini, TRAC menyediakan pengelolaan transportasi
                        dengan cara syariah yang mengedepankan transaksi tanpa riba. Seluruh aset dibiayai dan dijamin oleh institusi keuangan syariah dan Anda tidak akan
                        dikenakan denda pembatalan. TRAC Syariah hanya membebankan Ta’widh dan Gharamah yang nantinya disumbangkan untuk kebaikan masyarakat.
                    </p>
                    <p>
                        Selain itu, TRAC Syariah menerapkan sistem pengembalian dana berdasarkan konsep pembagian risiko. Anda hanya membayar biaya perawatan yang masuk
                        dalam deposit. Jika biaya yang digunakan tidak melebihi jumlah deposit, Anda akan mendapatkan kembali dana yang tak terpakai tersebut.
                    </p>
                    <p>
                        Melalui pengawasan dari divisi syariah yang beranggotakan Badan Penasihat Syariah (SAC), layanan TRAC Syariah konsisten menerapkan kaidah syariah yang
                        berlaku. Kami merupakan perusahaan transportasi pertama yang memberikan layanan berbasis syariah secara resmi di Indonesia. Kami telah mendapatkan
                        surat rekomendasi Nomor U-576/DSN-MUI/XI/2016 dari Dewan Syariah Nasional – Majelis Ulama Indonesia (DSN MUI).
                    </p>
                    <p>
                        Dengan layanan operating lease TRAC membantu perusahaan Anda mengelola kebutuhan transportasi secara optimal. Perusahaan bisa menghemat biaya
                        serta sumber daya pengelolaan transportasi dan mengalihkannya untuk investasi yang penting bagi perkembangan bisnis. Hubungi Customer Assistance
                        Center 1500009 untuk informasi lebih detail.</p>
                </div> 
            </Fragment>         
        )
    }
}
export default DriverLease;