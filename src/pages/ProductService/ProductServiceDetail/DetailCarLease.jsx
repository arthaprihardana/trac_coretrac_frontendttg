import React, { Component, Fragment } from 'react';
import { Main, Static } from 'templates';

//assets
import LandingCorporate from 'assets/images/dummy/corporate-product.png';
// import CarLease from 'assets/images/logo/car-lease.svg';
// import DriveLease from 'assets/images/logo/drive-lease.svg';
// import MotorLese from 'assets/images/logo/motor-lease.svg';
// import BusLease from 'assets/images/logo/bus-lease.svg';

//thisComponent
import CarLease from './thisComponentLease/CarLease';

class DetailCarLease extends Component{
    state={
        breadCrumb: [
            {
                label: 'Home',
                link: '/'
            },
            {
                label: 'Product Services',
                link: '/product-services'
            },
            {
                label: 'Corporate Lease',
                link: '/corporate-car-service'
            },
        ],
        carLeaseImage:[

            {
                id:1,
                cardCar:"card-detail",
                leaseCar:"car-lease detail",
                title:"Operating Lease",
                link:"/operating-lease",
            },
            {
                id:2,
                cardCar:"card-detail active",
                leaseCar:"car-lease detail",
                title:"Rental Mobil Harian Korporat",
                link:"/corporate-car-service",
            },
            {
                id:3,
                cardCar:"card-detail",
                leaseCar:"drive-lease detail",
                title:"Layanan Pengemudi",
                link:"/driver-services",
            }
        ]
    }

    componentDidMount() {
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': 0
        });
    }


    render(){
    return(
        <Fragment>
            <Main className="landing-corporate">
                <Static title="Rental Mobil Harian Korporat" bannerImg={LandingCorporate} breadCrumb={this.state.breadCrumb} >
                    <div className="container">
                        <h2 className="title-corporate">Kami Siap Mendukung Bisnis Anda</h2>
                        <p className="desc-corporate">Sistem transportasi yang terkelola dengan baik dan efisien menjadi salah satu pendukung kesuksesan bisnis. Meski demikian, kami mengerti bahwa kebutuhan tiap perusahaan tak selalu sama sehingga memerlukan solusi transportasi yang dirancang secara khusus. Oleh karen itu, TRAC telah menyediakan beragam solusi transportasi berbasis kontrak dengan jangka waktu kerja sama minimal satu tahun. Kami siap mendukung bisnis Anda dengan jaminan layanan yang aman, nyaman, dan terukur.</p>
                    </div>
                    <h2 className="lease-paragraph">Pilih Layanan yang Tepat untuk Perusahaan Anda</h2>
                    <div className="image-lease">
                        {
                            this.state.carLeaseImage.map( car =>{
                                return(
                                    <a style={{textDecoration:'none'}} href={car.link} key={car.id} className={car.cardCar}>
                                        <a href={car.link} className={car.leaseCar}></a>
                                        <h4 className="center-text">{car.title}</h4>
                                    </a>
                                )
                            })
                        }
                    </div>
                    <CarLease />
                </Static>
            </Main>
        </Fragment>    
    )
}

}
export default DetailCarLease;