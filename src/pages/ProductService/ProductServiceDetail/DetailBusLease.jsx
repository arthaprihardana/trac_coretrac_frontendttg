import React, { Component, Fragment } from 'react';
import { Main, Static } from 'templates';

//assets
import LandingCorporate from 'assets/images/trac/BusServicesBanner.png';

//thisComponent
import BusLease from './thisComponentLease/BusLease';

class DetailBusLease extends Component{
    state={
        breadCrumb: [
            {
                label: 'Home',
                link: '/'
            },
            {
                label: 'Product Services',
                link: '/product-services'
            },
            {
                label: 'BUS LEASE SERVICE',
                link: '/bus-services'
            },
        ],
        carLeaseImage:[
            {
                id:1,
                cardCar:"card-detail",
                leaseCar:"car-lease detail",
                title:"CAR LEASE",
                link:"/corporate-car-service",
            },
            {
                id:2,
                cardCar:"card-detail",
                leaseCar:"drive-lease detail",
                title:"DRIVER SERVICE",
                link:"/driver-services",
            },
            {
                id:3,
                cardCar:"card-detail",
                leaseCar:"motor-lease detail",
                title:"MOTORCYCLE LEASE SERVICE",
                link:"/corporate-bike-service",
            },
            {
                id:4,
                cardCar:"card-detail active",
                leaseCar:"bus-lease detail",
                title:"BUS LEASE SERVICE",
                link:"/bus-services",
            }
        ]
    }

    componentDidMount() {
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': 0
        });
    }

    render(){
    return(
        <Fragment>
            <Main className="landing-corporate">
                <Static title="SEWA BUS" bannerImg={LandingCorporate} breadCrumb={this.state.breadCrumb} >
                    {/*<h2 className="lease-paragraph">Pilih Layanan yang Tepat untuk Perusahaan Anda</h2>*/}
                    {/*<div className="image-lease">*/}
                        {/*{*/}
                            {/*this.state.carLeaseImage.map( car =>{*/}
                                {/*return(*/}
                                    {/*<a style={{textDecoration:'none'}} href={car.link} key={car.id} className={car.cardCar}>*/}
                                        {/*<a href={car.link} className={car.leaseCar}></a>*/}
                                        {/*<h4 className="center-text">{car.title}</h4>*/}
                                    {/*</a>*/}
                                {/*)*/}
                            {/*})*/}
                        {/*}*/}
                    {/*</div>*/}
                    <BusLease /> 
                </Static>
            </Main>
        </Fragment>    
    )
}

}
export default DetailBusLease;