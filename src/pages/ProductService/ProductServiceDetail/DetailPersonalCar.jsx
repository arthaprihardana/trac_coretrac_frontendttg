import React, { Component, Fragment } from 'react';
import { Main, Static } from 'templates';

//assets
import LandingRental from 'assets/images/dummy/personal-rental.png';

//thisComponent
import PersonalCar from './thisComponentPersonal/PersonalCar';

class DetailPersonalCar extends Component{
    state={
        breadCrumb: [
            {
                label: 'Home',
                link: '/'
            },
            {
                label: 'Product Services',
                link: '/product-services'
            },
            {
                label: 'Personal Rental',
                link: '/detail-product-airport'
            },
        ],
        carLeaseImage:[
            {
                id:1,
                cardCar:"card-detail active",
                leaseCar:"car-lease detail",
                title:"RENTAL MOBIL HARIAN",
                link:"/detail-product-rental",
            },
            {
                id:2,
                cardCar:"card-detail",
                leaseCar:"drive-lease detail",
                title:"AIRPORT TRANSFER",
                link:"/detail-product-airport",
            }
        ]
    }

    componentDidMount() {
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': 0
        });
    }

    render(){
    return(
        <Fragment>
            <Main className="landing-corporate">
                <Static title="Personal Rental" bannerImg={LandingRental} breadCrumb={this.state.breadCrumb} >
                    <div className="container">
                        <h2 className="title-corporate">Aktivitas Lancar, Perjalanan Tetap Nyaman</h2>
                        <p className="desc-corporate">Semua orang, termasuk Anda, berhak untuk menikmati layanan transportasi yang aman dan nyaman, termasuk untuk keperluan
                            perjalanan jangka pendek. Kami menyediakan layanan sewa kendaraan harian untuk semua aktivitas Anda. Liburan, perjalanan
                            bisnis di luar kota, maupun mobilitas sehari-hari menjadi lebih menyenangkan. Semua layanan kami terjamin aman, nyaman,
                            dengan akses reservasi online yang dapat diakses kapan pun. Anda dapat melakukan pemesanan online melalui situs web atau
                            aplikasi TRAC yang tersedia untuk pengguna Android dan iOS</p>
                    </div>
                    <h2 className="lease-paragraph">Pilih Layanan yang Tepat untuk Perusahaan Anda</h2>
                    <div className="image-lease">
                        {
                            this.state.carLeaseImage.map( car =>{
                                return(
                                    <a style={{textDecoration:'none'}} href={car.link} key={car.id} className={car.cardCar}>
                                        <a goto={car.link} className={car.leaseCar}></a>
                                        <h4 className="center-text">{car.title}</h4>
                                    </a>
                                )
                            })
                        }
                    </div>
                    <PersonalCar/>
                </Static>
            </Main>
        </Fragment>    
    )
}

}
export default DetailPersonalCar;