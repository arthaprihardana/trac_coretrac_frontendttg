import React, { Component, Fragment } from 'react';


class PersonalAirport extends Component{
    state={
        notifLease:[
            {
                id:1,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif timer",
                text:"Layanan self drive di seluruh cabang TRAC",
            },
            {
                id:2,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif security",
                text:"Promo khusus member",
            },
            {
                id:3,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif comfort",
                text:"Reservasi lebih cepat",
            }
        ]
    }
    render(){
        return(
            <Fragment>
                <h2 className="paragraph-lease">Airport Transfer</h2>
                <div className="content-car lease">
                    <p>Ketika sampai di bandara, Anda tak perlu lagi repot mencari kendaraan, apalagi harus antre untuk menuju destinasi. TRAC siap
                        menjemput Anda tepat waktu dan mengantarkan hingga tujuan. Layanan ini sudah termasuk sewa mobil, layanan pengemudi,
                        asuransi perjalanan, biaya bensin, tol, dan parkir sehingga Anda tinggal duduk santai selama perjalanan.
                        </p>
                </div> 
                {/*<h5>Keunggulan layanan Airport Transfer TRAC</h5>*/}
                {/*<div className="notif-lease">*/}
                    {/*{*/}
                        {/*this.state.notifLease.map( notif =>{*/}
                            {/*return(*/}
                                {/*<div key={notif.id} className={notif.leaseNotif}>*/}
                                    {/*<div className={notif.NotifImage}></div>*/}
                                    {/*<h4 className="text-color">{notif.text}</h4>*/}
                                {/*</div>*/}
                            {/*)*/}
                        {/*})*/}
                    {/*}    */}
                {/*</div>*/}
                <div className="content-car lease">
                    <p>Anda dapat memesan Airport Transfer secara online melalui situs web dan aplikasi TRAC atau secara offline dengan menghubungi
                        Customer Assistance Center 1500009 maupun email ke <a href="mailto:rco.nasional@trac.astra.co.id?Subject=Reservasi" target="_top">rco.nasional@trac.astra.co.id.</a>  Mulai perjalanan mudah dan aman
                        bersama TRAC, sekarang!</p>
                </div> 
            </Fragment>         
        )
    }
}
export default PersonalAirport;