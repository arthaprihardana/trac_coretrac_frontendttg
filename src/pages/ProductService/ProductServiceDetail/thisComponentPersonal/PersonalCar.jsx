import React, { Component, Fragment } from 'react';


class PersonalCar extends Component{
    state={
        notifLease1:[
            {
                id:1,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif timer",
                text:"Layanan self drive di seluruh cabang TRAC",
            },
            {
                id:2,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif security",
                text:"Promo khusus member",
            },
            {
                id:3,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif comfort",
                text:"Reservasi lebih cepat",
            },
        ],
        notifLease2:[
            {
                id:1,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif timer",
                text:"Tepat Waktu",
            },
            {
                id:2,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif security",
                text:"Tanpa Biaya Tambahan",
            },
            {
                id:3,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif comfort",
                text:"Pengemudi Handal",
            }
        ]
    }
    render(){
        return(
            <Fragment>
                <h2 className="paragraph-lease">Rental Mobil Harian</h2>
                <div className="content-car lease">
                    <p>Ada kalanya Anda perlu melakukan perjalanan dalam kota, bahkan luar kota bersama keluarga, kolega, kawan maupun untuk solo
                        travelling. Atau, mobil Anda sedang perawatan ke bengkel, sedangkan Anda tetap butuh kendaraan yang senyaman mobil sendiri untuk
                        mobilitas harian yang padat. Tenang saja, TRAC siap melayani beragam kebutuhan transportasi Anda setiap waktu dengan Rental Mobil
                        Harian. Jaringan kami yang luas dapat menjangkau Anda lebih jauh. Pilih layanan yang paling sesuai dengan kebutuhan Anda:</p>
                    <h4>Rental Mobil Lepas Kunci | Self Drive</h4>
                    <p>Bebas pergi ke mana pun dan bebas atur waktu perjalanan Anda dengan rental mobil lepas kunci. Tenang saja, keamanan Anda tetap
                        terjaga dengan jaminan kondisi mobil prima dan asuransi perjalanan. Pilih paket rental 12 jam atau 24 jam dengan opsi tambahan waktu
                        atau trip keluar kota sesuai keperluan Anda.</p>
                </div> 
                {/*<div className="notif-lease">*/}
                    {/*{*/}
                        {/*this.state.notifLease1.map( notif =>{*/}
                            {/*return(*/}
                                {/*<div key={notif.id} className={notif.leaseNotif}>*/}
                                    {/*<div className={notif.NotifImage}></div>*/}
                                    {/*<h4 className="text-color">{notif.text}</h4>*/}
                                {/*</div>*/}
                            {/*)*/}
                        {/*})*/}
                    {/*}    */}
                {/*</div>*/}
                <div className="content-car lease">
                    <h4>Rental Mobil dengan Pengemudi | Chauffeur</h4>
                    <p>Ingin duduk santai menikmati perjalanan tanpa harus menyetir mobil sendiri? Manfaatkan sewa mobil dengan layanan pengemudi dari TRAC. 
                        Layanan ini sudah termasuk sewa mobil, layanan pengemudi, asuransi perjalanan, biaya bensin, tol, dan parkir. 
                        Pengemudi andal kami akan menjemput dan mengantarkan Anda sampai ke tujuan dengan mengedepankan ketepatan waktu juga layanan yang ramah dan profesional. 
                        Pilih paket sesuai kebutuhan Anda: 4 jam atau 12 jam.</p>
                </div> 
                {/*<div className="notif-lease">*/}
                    {/*{*/}
                        {/*this.state.notifLease2.map( notif =>{*/}
                            {/*return(*/}
                                {/*<div key={notif.id} className={notif.leaseNotif}>*/}
                                    {/*<div className={notif.NotifImage}></div>*/}
                                    {/*<h4 className="text-color">{notif.text}</h4>*/}
                                {/*</div>*/}
                            {/*)*/}
                        {/*})*/}
                    {/*}    */}
                {/*</div>*/}
                <div className="content-car lease">
                    <p>Pesan Rental Mobil Harian secara online melalui situs web dan aplikasi TRAC atau secara offline dengan menghubungi Customer
                        Assistance Center 1500009 maupun email ke <a href="mailto:rco.nasional@trac.astra.co.id?Subject=Reservasi" target="_top">rco.nasional@trac.astra.co.id.</a> Rencanakan perjalanan dan temukan hal baru Bersama TRAC.</p>
                </div>
            </Fragment>         
        )
    }
}
export default PersonalCar;