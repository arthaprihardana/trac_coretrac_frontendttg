import React, { Component, Fragment } from 'react';

//assets
import LxryBus from 'assets/images/trac/content/bus_luxury.png';
import SmallBus from 'assets/images/trac/content/bus_small.png';
import MediumBus from 'assets/images/trac/content/bus_medium.png';
import BigBus from 'assets/images/trac/content/bus_big.png';

class PersonalBus extends Component{
    state={
        notifLease:[
            {
                id:1,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif timer",
                text:"Tepat Waktu",
            },
            {
                id:2,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif security",
                text:"Aman",
            },
            {
                id:3,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif comfort",
                text:"Nyaman",
            },
            {
                id:4,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif prodrive",
                text:"Pengemudi Profesional",
            },
            {
                id:5,
                leaseNotif:"card-lease",
                NotifImage:"lease-notif achievement",
                text:"Layanan Berkualitas",
            },
        ],
        categoryBus1:[
            {
                id:1,
                textCategory:"LUXURY BUS",
                categoryImage:LxryBus,
                desc:"Spesifikasi",
                content:"11 kursi (dilengkapi LDC, earphones, dan reflexology system), Ruang rapat, Pantry, DVD Player, Wi-Fi, LCD TV, Soket listrik , AC, Sound system, toilet" , 
                categoryClass:"left-category", 
                titleClass:"category-luxury" , 
            },
            {
                id:2,
                textCategory:"SMALL BUS",
                categoryImage:SmallBus,
                desc:"Spesifikasi",
                content:"13 dan 15 kursi, AC, LCD TV, DVD player, Sound system, Soket listrik.",    
                categoryClass:"right-category", 
                titleClass:"category-small" , 
            },
        ],
        categoryBus2:[
            {
                id:3,
                textCategory:"MEDIUM BUS",
                categoryImage:MediumBus,
                desc:"Spesifikasi",
                content:"29 kursi, AC, LCD TV, DVD player, Sound system, Soket listrik, Bagasi, Wi-Fi (tersedia di sebagian unit bus)",   
                categoryClass:"left-category",  
                titleClass:"category-medium" ,
            },
            {
                id:4,
                textCategory:"BIG BUS",
                categoryImage:BigBus,
                desc:"Spesifikasi",
                content:"48 dan 59 kursi, AC, LCD TV, DVD player, Sound system, Soket listrik Bagasi, Wi-Fi (tersedia di sebagian unit bus)",    
                categoryClass:"right-category", 
                titleClass:"category-big" ,
            }

        ]
    }
    render(){
        return(
            <Fragment>
                <h2 className="paragraph-lease">Bus Lease Service</h2>
                <div className="content-car lease">
                    <p>Manfaatkan layanan sewa bus untuk mendukung kebutuhan transportasi perusahaan Anda, 
                        misalnya untuk antar-jemput karyawan atau transportasi shuttle bus di mall, bandara, 
                        dan area bisnis lainnya. Apapun kebutuhan Anda, kami siap melayani.hiohivhshv</p>
                </div> 
                <div className="notif-lease">
                    {
                        this.state.notifLease.map( notif =>{
                            return(
                                <div key={notif.id} className={notif.leaseNotif}>
                                    <div className={notif.NotifImage}></div>
                                    <h4 className="text-color">{notif.text}</h4>
                                </div>
                            )
                        })
                    }    
                </div>
                <div className="category-lease">
                    {
                        this.state.categoryBus1.map( category => {
                            return(
                                <div key={category.id} className={category.categoryClass}>
                                    <h2 className={category.titleClass}>{category.textCategory}</h2>
                                    <img src={category.categoryImage} alt="Luxury"/>
                                    <div className="text-spesifikasi">
                                        <p className="descrip" style={{fontWeight:700,color:'#636363'}}>{category.desc}</p>
                                        <p className="content-descrip" style={{paddingRight:20}}>{category.content}</p>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                <div className="category-lease">    
                    {
                        this.state.categoryBus2.map( category => {
                            return(
                                <div key={category.id} className={category.categoryClass}>
                                    <h2 className={category.titleClass}>{category.textCategory}</h2>
                                    <img src={category.categoryImage} alt="Luxury"/>
                                    <div className="text-spesifikasi">
                                        <p className="descrip" style={{fontWeight:700,color:'#636363'}}>{category.desc}</p>
                                        <p className="content-descrip" style={{paddingRight:20}}>{category.content}</p>
                                    </div>
                                </div>
                            )
                        })
                    }    
                </div>
                <div className="content-last">
                    <p>Untuk informasi lebih lanjut, silahkan hubungi kami:</p>
                    <p>Call Center Bus Services</p>
                    <p>Jakarta : (021) 2983 5555 | Surabaya : (031) 871 1818 | Email : bus@trac.astra.co.id | Waktu operasional: Senin-Jumat 08.00 – 17.00</p>
                </div>
            </Fragment>       
        )
    }
}
export default PersonalBus;