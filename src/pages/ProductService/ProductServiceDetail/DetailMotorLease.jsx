import React, { Component, Fragment } from 'react';
import { Main, Static } from 'templates';

//assets
import LandingCorporate from 'assets/images/trac/BikeServicesBanner.png';
// import CarLease from 'assets/images/logo/car-lease.svg';
// import DriveLease from 'assets/images/logo/drive-lease.svg';
// import MotorLese from 'assets/images/logo/motor-lease.svg';
// import BusLease from 'assets/images/logo/bus-lease.svg';

//thisComponent
import MotorLease from './thisComponentLease/MotorLease';

class DetailMotorLease extends Component{
    state={
        breadCrumb: [
            {
                label: 'Home',
                link: '/'
            },
            {
                label: 'Product Services',
                link: '/product-services'
            },
            {
                label: 'Motorcycle Lease',
                link: '/corporate-bike-service'
            },
        ],
        carLeaseImage:[
            {
                id:1,
                cardCar:"card-detail ",
                leaseCar:"car-lease detail",
                title:"CAR LEASE",
                link:"/corporate-car-service",
            },
            {
                id:2,
                cardCar:"card-detail",
                leaseCar:"drive-lease detail",
                title:"DRIVER SERVICE",
                link:"/driver-services",
            },
            {
                id:3,
                cardCar:"card-detail active",
                leaseCar:"motor-lease detail",
                title:"MOTORCYCLE LEASE SERVICE",
                link:"/corporate-bike-service",
            },
            {
                id:4,
                cardCar:"card-detail",
                leaseCar:"bus-lease detail",
                title:"BUS LEASE SERVICE",
                link:"/bus-services",
            }
        ]
    }

render(){
    return(
        <Fragment>
            <Main className="landing-corporate">
                <Static title="MOTORCYCLE LEASE SERVICE" bannerImg={LandingCorporate} breadCrumb={this.state.breadCrumb} >
                    <div className="container">
                        <h2 className="title-corporate">Kami Siap Mendukung Bisnis Anda</h2>
                        <p className="desc-corporate">Sistem transportasi yang terkelola dengan baik dan efisien menjadi salah satu pendukung kesuksesan bisnis. Meski demikian, kami mengerti bahwa kebutuhan tiap perusahaan tak selalu sama sehingga memerlukan solusi transportasi yang dirancang secara khusus. Oleh karen itu, TRAC telah menyediakan beragam solusi transportasi berbasis kontrak dengan jangka waktu kerja sama minimal satu tahun. Kami siap mendukung bisnis Anda dengan jaminan layanan yang aman, nyaman, dan terukur.</p>
                    </div>
                    <h2 className="lease-paragraph">Pilih Layanan yang Tepat untuk Perusahaan Anda</h2>
                    <div className="image-lease">
                        {
                            this.state.carLeaseImage.map( car =>{
                                return(
                                    <a style={{textDecoration:'none'}} href={car.link} key={car.id} className={car.cardCar}>
                                        <a href={car.link} className={car.leaseCar}></a>
                                        <h4 className="center-text">{car.title}</h4>
                                    </a>
                                )
                            })
                        }
                    </div>
                    <MotorLease />
                </Static>
            </Main>
        </Fragment>    
    )
}

}
export default DetailMotorLease;