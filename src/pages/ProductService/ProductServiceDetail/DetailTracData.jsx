import React, { Component, Fragment } from 'react';
import { Main, Static } from 'templates';

//assets
import LandingRental from 'assets/images/dummy/personal-rental.png';

//thisComponent
import TracData from './thisComponentTrac/TracData';

class DetailTracData extends Component{
    state={
        breadCrumb: [
            {
                label: 'Home',
                link: '/'
            },
            {
                label: 'Product Services',
                link: '/product-services'
            },
            {
                label: 'Trac Fleet Management Solution',
                link: '/detail-product-trac-fleet'
            },
        ],
        carLeaseImage:[
            {
                id:1,
                cardCar:"card-detail active",
                leaseCar:"car-lease detail",
                title:"TRAC DATA",
                link:"/detail-product-trac-data",
            },
            {
                id:2,
                cardCar:"card-detail",
                leaseCar:"drive-lease detail",
                title:"TRAC FLEET",
                link:"/detail-product-trac-fleet",
            },
            {
                id:3,
                cardCar:"card-detail",
                leaseCar:"bus-lease detail",
                title:"TRAC SMART",
                link:"/detail-product-trac-smart",
            }
        ]
    }

    componentDidMount() {
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': 0
        });
    }

    render(){
    return(
        <Fragment>
            <Main className="landing-corporate">
                <Static title="Trac Fleet Management Solution" bannerImg={LandingRental} breadCrumb={this.state.breadCrumb} >
                    <div className="container">
                        <h2 className="title-corporate">Sistem Transportasi Berbasis Teknologi</h2>
                        <p className="desc-corporate">Anda dapat mengelola kebutuhan transportasi perusahaan secara maksimal. Kini, kami menyediakan sistem
                            transportasi yang memadukan teknologi aplikasi berbasis cloud dan internet of things. Kami merancang TRAC
                            Fleet Management Solution untuk membantu Anda mengelola dan mengawasi transportasi dengan mudah.
                            Efisiensi dari aspek transportasi pun dapat tercapai secara maksimal.</p>
                    </div>
                    <div className="image-lease" style={{    marginTop: 40}}>
                        {
                            this.state.carLeaseImage.map( car =>{
                                return(
                                    <a style={{textDecoration:'none'}} href={car.link} key={car.id} className={car.cardCar}>
                                        <a href={car.link} className={car.leaseCar}></a>
                                        <h4 className="center-text">{car.title}</h4>
                                    </a>
                                )
                            })
                        }
                    </div>
                    <TracData /> 
                </Static>
            </Main>
        </Fragment>    
    )
}

}
export default DetailTracData;