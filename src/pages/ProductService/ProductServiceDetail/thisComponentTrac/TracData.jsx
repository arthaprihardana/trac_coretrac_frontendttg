import React, { Component, Fragment } from 'react';


class TracData extends Component{
    render(){
        return(
            <Fragment>
                <h2 className="paragraph-lease">TRAC Data</h2>
                <div className="content-car lease">
                    <p>Anda memiliki aset kendaraan, tapi kesulitan mengelolanya? TRAC Data menjadi solusi tepat. 
                        Kami akan merangkum data penggunaan kendaraan Anda lalu membantu melakukan analisis data. Kemudian, kami akan mengirimkan hasil report secara berkala, yang dapat Anda gunakan sebagai dasar untuk melakukan pengelolaan aset yang lebih baik.</p>
                    <p>TRAC Data akan membantu Anda untuk:</p>
                    <ul>
                        <li>Melakukan penjadwalan perawatan kendaraan tepat waktu</li>
                        <li>Meningkatkan pengawasan terhadap aspek keamanan kendaraan</li>
                        <li>Memperoleh analisis tingkat produktivitas aset kendaraan</li>
                        <li>Memperbaiki kontrol aset kendaraan Anda</li>
                        <li>Meningkatkan pemakaian kendaraan yang lebih aman dan efisien</li>
                        <li>Mengetahui informasi kondisi kendaraan selama perjalanan</li>
                    </ul>
                </div> 
            </Fragment>         
        )
    }
}
export default TracData;