import React, { Component, Fragment } from 'react';


class TracSmart extends Component{
    render(){
        return(
            <Fragment>
                <h2 className="paragraph-lease">TRAC Smart</h2>
                <div className="content-car lease">
                    <p>Anda dapat menjalankan bisnis dengan lebih tenang. Kami akan menyiapkan kendaraan sesuai kebutuhan bisnis Anda, yang
                        sudah termasuk layanan perawatan dan perbaikan kendaraan, asuransi, pengurusan dokumen (STNK dan BPKB), bantuan darurat
                        selama perjalanan, serta kendaraan pengganti.</p>
                    <p>TRAC Fleet akan membantu Anda untuk:</p>
                    <ul>
                        <li>Memastikan kendaraan selalu dalam kondisi terbaik dengan melakukan perawaan tepat waktu dan sesuai standar keamanan</li>
                        <li>Meningkatkan residual value kendaraan</li>
                        <li>Memaksimalkan efisiensi biaya untuk perawatan kendaraan</li>
                        <li>Menyediakan layanan yang profesional dengan jangkauan luas</li>
                        <li>Memperoleh report yang menyeluruh terkait kondisi kendaraan</li>
                        <li>Meningkatkan rasio ketersediaan kendaraan yang siap digunakan</li>
                    </ul>
                </div> 
            </Fragment>         
        )
    }
}
export default TracSmart;