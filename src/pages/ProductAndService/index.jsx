import React, { Component } from 'react';
import { Main, Static } from 'templates';
import bannerImage from 'assets/images/dummy/about-cover.png'
import './ProductAndService.scss';
import Features from './Features';
import ImgItem1 from 'assets/images/dummy/about-item-1.png'
import ImgItem2 from 'assets/images/dummy/about-item-2.png'
import ImgItem3 from 'assets/images/dummy/about-item-3.png'

class ProductAndService extends Component {
  state = {
    features: [
      {
        id: 1,
        title: 'Corporate Lease',
        imagePosition: 'left',
        detail: 'Sistem transportasi yang terkelola dengan baik dan efisien menjadi salah satu pendukung kesuksesan bisnis. ',
        img: ImgItem1
      },
      {
        id: 2,
        title: 'Personal Rental',
        imagePosition: 'right',
        detail: 'Kami menyediakan layanan sewa kendaraan untuk semua aktivitas harian Anda. Liburan, arisan,  reuni, atau perjalanan keluar kota, menjadi lebih menyenangkan. Semua layanan kami terjamin aman, nyaman, dengan akses reservasi yang mudah.',
        img: ImgItem2
      },
      {
        id: 3,
        title: 'ASTRA FMS',
        imagePosition: 'left',
        detail: 'Kelola transportasi perusahaan secara maksimal dengan memanfaatkan sistem transportasi yang memadukan teknologi aplikasi berbasis cloud dan internet of things. Kami merancang ASTRA FMS untuk membantu Anda mengelola dan mengawasi transportasi dengan mudah.',
        img: ImgItem3
      },
    ]
  }
  render() {
    const { features } = this.state;
    return (
      <Main className="p-product-and-service">
        <Static title="Product and Luxury Bus" subTitle="Make sure your transportation partner has best quality and reliable. No matter what your needs and where to go TRAC has the right services for you!" bannerImg={bannerImage}>
          <Features data={features} />
        </Static>
      </Main>
    )
  }
}

export default ProductAndService;