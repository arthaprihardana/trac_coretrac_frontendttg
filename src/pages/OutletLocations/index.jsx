import React, { Component, Fragment } from "react";
import { Button, TracField } from "atom";
import Select from 'react-select';

// components
import { Main } from "templates";
import "./OutletLocations.scss";
import GoogleMapReact from 'google-map-react';
import * as _ from 'lodash'
//data JSON
import locationDataJSON from '../../assets/data-dummy/list-location.json';

const AnyReactComponent = ({ text }) => <div className="marker">{text}</div>;
const typeBranch = [
    { label: "All", value: "ALL" },
    { label: "Airport", value: "BANDARA" },
    { label: "Branch", value: "BRANCH" },
    { label: "Outlet", value: "OUTLET" }
];

class OutletLocations extends Component {
    state = {
        name: "",
        address: "",
        listobj: [],
        locationList: [],
        listBranchFilter: [],
        listProvinceFilter: [],
        listobjFilter: [],
        display: '',
        center: {
            lat: 59.95,
            lng: 30.33
        },
        zoom: 13,
        branchFilter: "ALL",
        provinceFilter: "ALL"
    }

    componentDidMount() {
        let tmp=[];
        _.map(locationDataJSON, x =>{
            if(tmp.some(y => {return x.province===y.value})){

            }else{
                tmp.push({label:x.province, value:x.province})
            }

        });

        this.setState({
            data : "",
            locationList:tmp,
            listobj : locationDataJSON,
            listobjFilter: locationDataJSON,
            name : locationDataJSON[0].name,
            address : locationDataJSON[0].address,
            display: 'block',
            center: {
                lat: locationDataJSON[0].lat,
                lng: locationDataJSON[0].lng
            },
            zoom: 13
        });

    }
    changeData = (i, j, k, l) => {
        // console.log(i, j, k, l)
        this.setState({
            name : i,
            address : j,
            display: 'block',
            center: {
                lat: k,
                lng: l
            }
        });
    }
    setFilter = () => {
        
        if(this.state.branchFilter !== "ALL"){
            if(this.state.provinceFilter !== "ALL"){
                this.setState({
                    listobjFilter: this.state.listobj.filter(item=>item.type === this.state.branchFilter).filter(item=>item.province === this.state.provinceFilter)
                });
            }else {
                this.setState({
                    listobjFilter: this.state.listobj.filter(item=>item.type === this.state.branchFilter)         
                });
            }
        }else {
            if(this.state.provinceFilter !== "ALL"){
                this.setState({
                    listobjFilter: this.state.listobj.filter(item=>item.province === this.state.provinceFilter)
                });
            }else {
                this.setState({
                    listobjFilter: locationDataJSON
                });
            }
        }

    }

    selectBranch = (value) => {
        this.setState({
            branchFilter: value
        });
    }

    selectProvince = (value) => {
        this.setState({
            provinceFilter: value
        });
    }
    
    render() {  
        return (
            <Main solid >
                <div className="bg-blue-outlet-locations">    
                    <Select onChange={e => this.selectBranch(e.value)} placeholder="Select Branch or Outlet" className="selectType" options={ typeBranch } />       
                    <Select onChange={e => this.selectProvince(e.value)} placeholder="Select Province" className="selectType" options={ this.state.locationList } />
                    <div className="btnSearch">
                    <Button type="button" primary onClick={this.setFilter} >
                        Search
                    </Button>
                    </div>
                </div>
                <div className="container-outlet-locations">
                    
                    <div className="container-content">
                        <div className="pos-map">
                        <GoogleMapReact
                                bootstrapURLKeys={{ key: "AIzaSyADp3vKmKDaYACS1NLPMFf12KGbwsaYqg4" }}
                                center={this.state.center}
                                defaultZoom={this.state.zoom}
                                >
                            {/*<Marker lat={this.state.center.lat} lng={this.state.center.lng} />*/}
                                <AnyReactComponent
                                    lat={this.state.center.lat}
                                    lng={this.state.center.lng}
                                    text={this.state.name}
                                />
                            </GoogleMapReact>                                               
                        </div>
                    </div>

                    <div className="side-list">
                        {this.state.listobjFilter.map((item, index) => (
                        //{this.state.listobj.filter(item=>item.type === "BRANCH").filter(item=>item.kode === "Jabar").map((item, index) => (
                            <OutletList 
                                type={item.type} 
                                name={item.name}
                                distance="10 KM"
                                phone={item.phone}
                                address={item.address}
                                open={item.open}
                                lat={item.lat}
                                lng={item.lng}
                                btnClick={this.changeData}
                                key={index}
                            ></OutletList>
                        //}
                        ))}
                    </div>
                </div>
               
            </Main>
        );
    }
}

export default OutletLocations;

class OutletList extends React.Component {

    selectData = (e) => {
        this.props.btnClick(this.props.name, this.props.address, this.props.lat, this.props.lng)
        // console.log(this.props.name);
    }

    render() {
        return(
            <Fragment>
                <div className="container-list">
                    <div className="title-side">
                        <div className="left-side">
                            <p className="type-outlet">{this.props.type}</p>
                            <p className="name-outlet">{this.props.name}</p>
                        </div>

                        <div className="right-side">
                            {/*<p className="ico-distance-outlet">{this.props.distance}</p>*/}
                        </div>
                        <div className="clear"></div>
                    </div>     
                    <div className="phone-address">           
                        <p className="phone-outlet">{this.props.phone}</p>
                        <p className="address-outlet">{this.props.address}</p>
                    </div>   
                    <div className="title-outlet">
                        <p className="title-open-outlet">Open Hour</p>
                        <p className="open-outlet">{this.props.open}</p>
                    </div>
                    <div className="clear"></div>
                    <div className="location-link">
                        <a onClick={e => this.selectData(e)}><p className="title-set-location-outlet">SET LOCATION</p></a>
                        <div className="clear-mob-none"></div>
                        {/*<a onClick={e => this.selectData(e)}><p className="open-link-outlet">Open in Waze</p></a>                   */}
                    </div>
                </div>          
            </Fragment>
            
        );
    }
}

class OutletPin extends React.Component {

    render() {
        return(
            <Fragment>
                <div 
                    style={{display: this.props.display ? 'block' : 'none' }}
                    className="container-pin">
                    <p className="name-outlet-pin">{this.props.name}</p>
                    <p className="address-outlet-pin">{this.props.address}</p>
                    <p className="address-outlet-pin">{this.props.lat}</p>
                    <p className="address-outlet-pin">{this.props.lng}</p>
                </div> 
            </Fragment>
            
        );
    }
}