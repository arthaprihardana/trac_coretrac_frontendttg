import React, { Component, Fragment } from 'react';
import { ListBlog } from 'molecules';
import { Button } from 'atom';
import { Main } from "templates";
import * as _ from 'lodash'

// import listBlogData from "assets/data-dummy/article.json"

import { getListArticle } from "actions";
// images
import BlogImage2 from 'assets/images/dummy/blog-secondary-1.jpg';
import BlogImage3 from 'assets/images/dummy/blog-secondary-2.jpg';
import BlogImage4 from 'assets/images/dummy/blog-secondary-3.jpg';
import BlogImage5 from 'assets/images/dummy/blog-secondary-4.jpg';
import BlogImage6 from 'assets/images/dummy/slider-1.png';
import Fb from 'assets/images/logo/ic-facebook.svg';
import Twit from 'assets/images/logo/ic-twitter.svg';
import Shares from 'assets/images/logo/ic-share.svg';
import imageNews from 'assets/images/dummy/image-news.png';
import Detail1 from 'assets/images/dummy/image-detail1.png';
import Detail2 from 'assets/images/dummy/image-detail2.png';
import uuid from "uuid/v4";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import './newsDetail.scss';

class NewsDetail extends Component{
    state = {
        listBlogData: [],
        id:this.props.match.params.id,
        indexId:null,
        all_data: []
    }
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.match.params.id !== prevProps.match.params.id){
            // console.log(this.props.match.params.id)
            return { newParam: this.props.match.params.id }
        }
        if(this.props.listArticleSuccess !== prevProps.listArticleSuccess) {
            return { success: this.props.listArticleSuccess }
        }
        return null;
    };
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (snapshot !== null) {
            if(snapshot.newParam !== undefined ){
                window.scrollTo({
                    'behavior': 'smooth',
                    'left': 0,
                    'top': 0
                });
                this.setState({
                    id:snapshot.newParam
                });
                if(this.state.listBlogData.length>0){
                   let tmp = this.state.listBlogData.findIndex(x => x.id == snapshot.newParam);
                       this.setState({
                           indexId: parseInt(tmp+1)
                       })

                }
            }
            if (snapshot.success !== undefined) {
                this.setState({
                    all_data: snapshot.success.reverse(),
                    // all_data: _.filter(snapshot.success,(o)=>{ return this.props.match.params.id !=  o.id}),
                    listBlogData:snapshot.success.reverse()
                })
                if(this.state.id!== ''){
                    let tmp = snapshot.success.findIndex(x => x.id == this.state.id);
                    this.setState({
                        indexId: parseInt(tmp+1)
                    })

                }
            }
        }
    };
    componentDidMount() {
        this.props.getListArticle();
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': 0
        });
    }
    createMarkup = (html) => {
        return {__html: html};
    };
    render(){

        const { createMarkup } = this;
        const { id, all_data, listBlogData,indexId } = this.state;
        return(
            <Fragment>
                <Main>
                    {
                        (listBlogData.length>0 && indexId !='') && (
                            <div className="detail-wrapper">
                                <img className="ban-slider" src={listBlogData[indexId-1].image} alt="banner"/>
                                <div className="container">
                                    <div className="page1 ">
                                        <ul className="desc-title">
                                            <li className="home-desc">HOME</li>
                                            <li className="blog-desc">BLOG</li>
                                            <li className="libur-desc">BERLIBUR</li>
                                        </ul>
                                        <div className="title-content">
                                            <h1>{listBlogData[indexId-1].title}</h1>
                                            <h3>WRITTEN BY TRAC - OCTOBER 24, 2018</h3>
                                        </div>
                                    </div>
                                    <div className="page2">
                                        <div className="container">
                                            {/*<div className= "detail-content">*/}
                                            {/*<div className="share">*/}
                                            {/*<div className="logo-icon">*/}
                                            {/*<div className="text-under">SHARE</div>*/}
                                            {/*<img src={Fb} alt="fb"/>*/}
                                            {/*<div className="text-icon">12345</div>*/}
                                            {/*<img src={Twit} alt="twitter"/>*/}
                                            {/*<div className="text-icon">12345</div>*/}
                                            {/*<img src={Shares} alt="share"/>*/}
                                            {/*<div className="text-icon">12345</div>*/}
                                            {/*</div>*/}
                                            {/*</div>*/}
                                            {/*</div>*/}
                                            <img className="image2" src={listBlogData[indexId-1].thumbnail} alt="image2"/>
                                            {/*<h3>THIS IS STYLING FOR HEADLINE</h3>*/}
                                            <div className= "detail-content">
                                                <div className="content_article" dangerouslySetInnerHTML={this.createMarkup(listBlogData[indexId-1].content)} style={{ textAlign: 'justify' }} />
                                            </div>
                                            {/*<div className="tag-news-detail">*/}
                                            {/*<ul className="tag-detail">*/}
                                            {/*<li>TRAVE</li>*/}
                                            {/*<li>INDONESI</li>*/}
                                            {/*<li>ASI</li>*/}
                                            {/*</ul>*/}
                                            {/*</div>*/}
                                        </div>
                                        {/*<div className="detail-line"></div>  */}
                                        <div className="container">
                                            <div className="related-artc">
                                                <h2>Related Article</h2>
                                                <div className="section1">
                                                    {all_data.map((d,index) => {
                                                            let blogs = '';
                                                            if (index !== 0 && index < 5) {
                                                                blogs = (
                                                                    <Fragment key={uuid()}>
                                                                        <ListBlog
                                                                            image={d.thumbnail}
                                                                            label={d.label}
                                                                            title={d.title}
                                                                            lead={d.lead}
                                                                            date={d.date}
                                                                            linkTo={d.linkTo}
                                                                            linkText={d.linkText}
                                                                        />
                                                                    </Fragment>
                                                                );
                                                                return blogs
                                                            }
                                                        }
                                                    )}
                                                </div>
                                            </div>
                                        </div>
                                        {/*<div className="news-comment">*/}
                                            {/*<p>Leave a comment</p>*/}
                                            {/*<div className="comment-text">*/}
                                                {/*<textarea defaultValue="Write something..." />*/}
                                            {/*</div>*/}
                                            {/*<Button type="button" primary>*/}
                                                {/*Submit*/}
                                            {/*</Button>*/}
                                        {/*</div>*/}
                                    </div>
                                </div>
                            </div>
                        )
                    }

                </Main>    
             
            </Fragment>
        )
    }
}


const mapStateToProps = ({ product }) => {
    const { listArticleSuccess } = product;
    return {  listArticleSuccess }
}

export default withRouter(
    connect(mapStateToProps, {
        getListArticle,
    })(NewsDetail)
);