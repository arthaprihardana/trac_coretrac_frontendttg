import React, { Component, Fragment } from 'react';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import PropTypes from "prop-types";
import uuid from "uuid";
import { Main } from "templates";
import { isObject, isString } from "lodash";
import './NewsAndEvent.scss';
import { ListBlog} from 'molecules';
import { Button } from 'atom';
import Slider from "react-slick";
import { Cover } from "atom";
// import listBlogData from "assets/data-dummy/article.json"


import { getListArticle } from "actions";
// assets
import BlogImage2 from 'assets/images/dummy/blog-secondary-1.jpg';
import BlogImage3 from 'assets/images/dummy/blog-secondary-2.jpg';
import BlogImage4 from 'assets/images/dummy/blog-secondary-3.jpg';
import BlogImage5 from 'assets/images/dummy/blog-secondary-4.jpg';
import BlogImage6 from 'assets/images/dummy/slider-1.png';
import ImageLogin2 from "assets/images/dummy/headline-2.jpg";
import ImageLogin3 from "assets/images/dummy/headline-3.jpg";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";

class NewsAndEvent extends Component{   
    state = {
        listBlogData: [],
        data1: []
    };
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.listArticleSuccess !== prevProps.listArticleSuccess) {
            return { success: this.props.listArticleSuccess }
        }
        return null;
    };
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (snapshot !== null) {
            if (snapshot.success !== undefined) {
                this.setState({
                    listBlogData:snapshot.success.reverse()
                })
            }
        }
    };
    componentDidMount() {
        this.props.getListArticle();
    }
    render() {
        const { config, images } = this.props;
        return (
            <Fragment>
                <Main >
                    <div className="title wraper">
                        <div className="t-main t-auth">
                            <div className="left">
                                <div className="m-slideshow">
                                    <Slider {...config}>
                                        {images.map(image => (
                                            <div key={uuid()} className="slide-item">
                                                <Cover image={image.src} />
                                                <div className="slide-text ">
                                                    <div className="container">
                                                        {isObject(image.text) && (
                                                            <React.Fragment>
                                                                <h2
                                                                    dangerouslySetInnerHTML={{
                                                                    __html: image.text.title
                                                                    }}
                                                                />
                                                                <p>{image.text.Button}</p>
                                                            </React.Fragment>
                                                        )}
                                                        {isString(image.text) && <p>{image.text}</p>}
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                    </Slider>
                                </div>
                            </div>
                        </div>
                        <ul className="feat-icon">
                            <li className="active feature-images">Tips</li>
                            <li className="guidance-images">Travel</li>
                            <li className="news-images">Berita</li>
                            <li className="event-images">Event</li>
                        </ul>
                    </div>   
                    <div className="news-line container">
                        <div className="section1">
                            {
                                this.state.listBlogData.map( data => {
                                    return (
                                        <ListBlog
                                            key={data.id}
                                            image={data.thumbnail}
                                            label={data.label}
                                            title={data.title}
                                            lead={data.lead}
                                            date={data.date}
                                            linkTo={'/news-detail/'+data.id}
                                            linkText={data.linkText}
                                        />
                                    )
                                } )
                            }
                        </div> 
                        {/*<div className="section2">*/}
                            {/*{*/}
                                {/*this.state.data2.map( data => {*/}
                                    {/*return (*/}
                                        {/*<ListBlog*/}
                                            {/*key={data.id}*/}
                                            {/*image={data.image}*/}
                                            {/*label={data.label}*/}
                                            {/*title={data.title}*/}
                                            {/*lead={data.lead}*/}
                                            {/*date={data.date}*/}
                                            {/*linkTo={data.linkTo}*/}
                                            {/*linkText={data.linkText}*/}
                                        {/*/>*/}
                                    {/*)*/}
                                {/*})*/}
                            {/*}*/}
                        {/*</div> */}
                        {/*<div className="section-btn">    */}
                            {/*<Button type="button" outline goto="/news-detail/1">*/}
                            {/*All */}
                            {/*</Button>*/}
                        {/*</div>      */}
                    </div>
                </Main>
            </Fragment >        
        );
    }  
}

NewsAndEvent.defaultProps = {
    config: {
        dots: true,
        lazyLoad: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 7000,
        arrows: false,
        speed: 350,
        slidesToScroll: 1,
        initialSlide: 0,
        fade: true,
        cssEase: "ease"
    },
    images: [
        {
            text: {
                title: "Tip Hemat Sewa Mobil Untuk Mudik",
                Button:
                <Button type="button" primary>
                  Read
                </Button>
            },
            src: "https://omnispace.blob.core.windows.net/image-article/Image%20Tip%20Hemat%20Sewa%20Mobil%20untuk%20Mudik.jpg"
        },
        {
            text: {
                title: "Tip Ikuti Perayaan Waisak di Candi Borobudur",
                Button:
                <Button type="button" primary>
                  Read
                </Button>
            },
            src: "https://omnispace.blob.core.windows.net/image-article/detail-artikel-TRAC-Tip%20Waisak%20di%20Candi%20Borobudur.png"
        },
        {
            text: {
                title: "Napak Tilas 'Aruna dan Lidahnya' di Pontianak dan Singkawang ",
                Button:
                <Button type="button" primary>
                  Read
                </Button>
            },
            src: "https://omnispace.blob.core.windows.net/image-article/pontianak_singkawang_banner.jpg"
        }
    ]
};

NewsAndEvent.propTypes = {
    config: PropTypes.object, // eslint-disable-line
    images: PropTypes.arrayOf(
        PropTypes.shape({
            text: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
            path: PropTypes.string,
            src: PropTypes.string
        })
    )
};


const mapStateToProps = ({ product }) => {
    const { listArticleSuccess } = product;
    return {  listArticleSuccess }
};

export default withRouter(
    connect(mapStateToProps, {
        getListArticle
    })(NewsAndEvent)
);
