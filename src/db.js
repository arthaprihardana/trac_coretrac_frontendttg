/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-10 14:42:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-17 17:32:06
 */
import Dexie from 'dexie';

const db = new Dexie("TracToGo");

db.version(1).stores({ auth: '++id' });
db.version(1).stores({ local_auth: 'id' });

export default db;