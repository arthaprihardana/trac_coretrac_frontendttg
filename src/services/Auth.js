/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-10 11:03:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 08:21:58
 */
import auth0 from 'auth0-js';

// constant
import { AUTH0_DOMAIN, AUTH0_CLIENT_ID, MY_BASE_URL } from '../constant';

export default class Auth {

    constructor(type) {
        this.type = type;
        this.setRedirectType();
    }

    config = {
        domain: AUTH0_DOMAIN,
        clientID: AUTH0_CLIENT_ID,
        redirectUri: `${MY_BASE_URL}/login`,
        audience: 'https://trac-rental.auth0.com/userinfo',
        // audience: 'https://ttg-development.au.auth0.com/userinfo',
        responseType: 'token id_token',
        scope: 'openid email profile'
    }
    webAuth = null;

    setRedirectType = () => {
        switch (this.type) {
            case "car-rental":
                this.config.redirectUri = `${MY_BASE_URL}/personal-detail-login/car-rental`;
                this.webAuth = new auth0.WebAuth(this.config);
                break;
            case "airport-transfer":
                this.config.redirectUri = `${MY_BASE_URL}/personal-detail-login/airport-transfer`;
                this.webAuth = new auth0.WebAuth(this.config);
                break;
            case "bus-rental":
                this.config.redirectUri = `${MY_BASE_URL}/personal-detail-login/bus-rental`;
                this.webAuth = new auth0.WebAuth(this.config);
                break;
            default:
                this.webAuth = new auth0.WebAuth(this.config);
                break;
        }
    };

    handleAuthentication = () => {
        let self = this;
        let res = new Promise((resolve, reject) => {
            self.webAuth.parseHash((err, authResult) => {
                if (authResult) {
                    resolve(authResult);
                } else if (err) {
                    resolve(null);
                }
            })
        });
        return res;
    };

    openGoogleauth = () => {
        this.webAuth.authorize({
            connection: 'google-oauth2'
        });
    }
}