import {
    Home,
    StyleGuide,
    NotFound,
    Login,
    Register,
    ForgotPassword,
    TransportList,
    Order,
    PersonalDetail,
    PersonalDetailCreateAccount,
    PersonalDetailLogin,
    Payment,
    BookingSuccess,
    FlightDetail,
    PickupDetails,
    NewPassword,
    Dashboard,
    Faq,
    NewsAndEvent,
    NewsDetail,
    VerificationRegister,
    VerificationForgotPassword,
    SiteMap,
    AboutUs,
    PrivacyPolicy,
    OutletLocations,
    Tnc,
    Link,
    ProductService,
    DetailDriverLease,
    DetailMotorLease,
    DetailBusLease,
    DetailCarLease,
    DetailTracData,
    DetailPersonalCar,
    DetailPersonalAirport,
    DetailPersonalBus,
    DetailTracFleet,
    DetailOperatingLease,
    DetailTracSmart,
    PromotionPage,
    // LocatorMobile,
    PromotionDetail,
    FormContactUs
} from "pages";
import Verification from "../pages/Auth/Verification/Verification";

const AppRoutes = [
    {
        key: 1,
        path: "/",
        component: Home,
        exact: true
    },
    {
        key: 2,
        path: "/style-guide",
        component: StyleGuide,
        exact: true
    },
    {
        key: 3,
        path: "/login",
        component: Login,
        exact: true
    },
    {
        key: 4,
        path: "/register",
        component: Register,
        exact: true
    },
    {
        key: 5,
        path: "/forgot-password",
        component: ForgotPassword,
        exact: true
    },
    {
        key: 6,
        path: "/order/:id",
        component: Order,
        exact: true
    },
    {
        key: 7,
        path: "/transport-list/:id",
        component: TransportList,
        exact: true
    },
    {
        key: 8,
        path: "/personal-detail/:id",
        component: PersonalDetail,
        exact: true
    },
    {
        key: 9,
        path: "/personal-detail-create-account/:id",
        component: PersonalDetailCreateAccount,
        exact: true
    },
    {
        key: 10,
        path: "/personal-detail-login/:id",
        component: PersonalDetailLogin,
        exact: true
    },
    {
        key: 11,
        path: "/flight-detail",
        component: FlightDetail,
        exact: true
    },
    {
        key: 12,
        path: "/payment/:id",
        component: Payment,
        exact: true
    },
    {
        key: 13,
        path: "/booking-success",
        component: BookingSuccess,
        exact: true
    },
    {
        key: 14,
        path: "/pickup-details",
        component: PickupDetails,
        exact: true
    },
    {
        key: 15,
        path: "/new-password",
        component: NewPassword,
        exact: true
    },
    {
        key: 16,
        path: "/dashboard/:child",
        component: Dashboard,
        exact: true
    },
    {
        key: 17,
        path: "/dashboard/:child/:child/:id",
        component: Dashboard,
        exact: true
    },
    {
        key: 18,
        path: "/not-found",
        component: NotFound,
        exact: true
    },
    {
        key: 19,
        path: "/authentication/register/:token",
        component: VerificationRegister,
        exact: true
    },
    {
        key: 20,
        path: "/authentication/forgotpassword/:token",
        component: VerificationForgotPassword,
        exact: true
    },
    {
        key: 21,
        path: "/verification",
        component: Verification,
        exact: true
    },

    {
        key: 22,
        path: "/faq",
        component: Faq,
        exact: true
    },
    {
        key: 23,
        path: "/news",
        component: NewsAndEvent,
        exact: true
    },
    {
        key: 24,
        path: "/news-detail/:id",
        component: NewsDetail,
        exact: true
    },
    {
        key: 23,
        path: '/site-map',
        component: SiteMap,
        exact: true,
    },
    {
        key: 24,
        path: '/about-us',
        component: AboutUs,
        exact: true,
    },
    {
        key: 25,
        path: '/outlet-locations',
        component: OutletLocations,
        exact: true,
    },
    {
        key: 26,
        path: '/terms-and-conditions',
        component: Tnc,
        exact: true,
    },
    {
        key: 27,
        path: '/link',
        component: Link,
        exact: true,
    },
    {
        key: 28,
        path: '/privacy-policy',
        component: PrivacyPolicy,
        exact: true,
    },
    {
        key: 29,
        path: '/product-services',
        component: ProductService,
        exact: true,
    },
    {
        key: 30,
        path: '/corporate-car-service',
        component: DetailCarLease,
        exact: true,
    },
    {
        key: 31,
        path: '/driver-services',
        component: DetailDriverLease,
        exact: true,
    },
    {
        key: 32,
        path: '/corporate-bike-service',
        component: DetailMotorLease,
        exact: true,
    },
    {
        key: 33,
        path: '/bus-services',
        component: DetailBusLease,
        exact: true,
    },
    {
        key: 34,
        path: '/detail-product-rental',
        component: DetailPersonalCar,
        exact: true,
    },
    {
        key: 35,
        path: '/detail-product-airport',
        component: DetailPersonalAirport,
        exact: true,
    },
    {
        key: 36,
        path: '/detail-product-bus-rental',
        component: DetailPersonalBus,
        exact: true,
    },
    {
        key: 37,
        path: '/detail-product-trac-data',
        component: DetailTracData,
        exact: true,
    },
    {
        key: 38,
        path: '/detail-product-trac-fleet',
        component: DetailTracFleet,
        exact: true,
    },
    {
        key: 39,
        path: '/detail-product-trac-smart',
        component: DetailTracSmart,
        exact: true,
    },
    {
        key: 40,
        path: '/promo',
        component: PromotionPage,
        exact: true,
    },
    {
        key: 41,
        path: '/promo-detail/:id',
        component: PromotionDetail,
        exact: true,
    },
    {
        key: 42,
        path: '/contact-us',
        component: FormContactUs,
        exact: true,
    },
    {
        key: 43,
        path: '/locator-mobile',
        component: OutletLocations,
        exact: true,
    },
    {
        key: 44,
        path: '/operating-lease',
        component: DetailOperatingLease,
        exact: true,
    }
];

export default AppRoutes;
