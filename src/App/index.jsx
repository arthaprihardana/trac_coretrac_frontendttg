/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-23 11:51:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 23:35:51
 */
import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

// import './App.scss';

import AppRoutes from './AppRoutes';
import RentContextParent from '../context/RentContext';
import ReactGA from 'react-ga';

ReactGA.initialize('UA-84215719-1');

const FadingRoute = ({ component: Component, ...rest }) => {
    // console.log("rest => ", rest);
    ReactGA.pageview(rest.location.pathname);
    return (
    <Route {...rest} render={props => (
        <Component {...props}/>
    )}/>
)}

class App extends Component {
    render() {
        return (
            <Switch>
                {AppRoutes.map(route => (
                    <FadingRoute {...route} />
                ))}
                <Redirect from="*" to="/not-found" />
            </Switch>
        );
    }
}

export default RentContextParent(App);
