/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-17 10:13:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-24 23:30:16
 */
import _ from 'lodash';
import CryptoJS from "crypto-js";
import { SECRET_KEY } from '../constant';
import lang from '../assets/data-master/language'

export function delay(duration) {
    return new Promise((resolve, reject) => {
        if (!duration) return reject("Duration is not set!");
        setTimeout(() => resolve(), duration);
    });
}

export function defaultDidMountSetStyle() {
    document.body.classList.add("add-padding-top");
    document.body.classList.add("background-grey");
    return;
}

export function defaultWillUnmountSetStyle() {
    document.body.classList.remove("add-padding-top");
    document.body.classList.remove("background-grey");
    return;
}

export function removeScrollOnPopup(status) {
    let _removeScroll = 'remove-scroll';
    if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        _removeScroll = 'remove-scroll-ios';
    }
    if (status) {
        document.body.classList.add(_removeScroll);
    } else {
        document.body.classList.remove(_removeScroll);
    }
    return;
}

export function dashboardPopupSetup(status){
    var element = document.getElementsByClassName("o-header");
    var mobileElement = document.getElementsByClassName("dashboard-header");

    element[0].classList[status ? 'add' : 'remove']("show-popup-dashboard");
    mobileElement[0].classList[status ? 'add' : 'remove']("show-popup-dashboard");
    removeScrollOnPopup(status);
}

export function objectToQueryString(obj) {
    var qs = _.reduce(obj, (result, value, key) => {
        if (!_.isNull(value) && !_.isUndefined(value)) {
            if (_.isArray(value)) {
                result += _.reduce(value, (result1, value1) => {
                    if (!_.isNull(value1) && !_.isUndefined(value1)) {
                        result1 += key + '=' + value1 + '&';
                        return result1
                    } else {
                        return result1;
                    }
                }, '')
            } else {
                result += key + '=' + value + '&';
            }
            return result;
        } else {
            return result
        }
    }, '').slice(0, -1);
    return qs;
};

export function localStorageEncrypt(key, value) {
    localStorage.setItem(key, CryptoJS.AES.encrypt(value, SECRET_KEY));
}

export function localStorageDecrypt(key, type = "string") {
    let ls = localStorage.getItem(key);
    if(ls !== null) {
        let dec;
        if(type === "object") {
            let obj = CryptoJS.AES.decrypt(ls.toString(), SECRET_KEY);
            dec = JSON.parse(obj.toString(CryptoJS.enc.Utf8));
        } else {
            let str = CryptoJS.AES.decrypt(ls.toString(), SECRET_KEY);
            dec = str.toString(CryptoJS.enc.Utf8);
        }
        return dec;
    }
    return null;
}

export function encrypt(value, type = "string") {
    let e;
    switch (type) {
        case "object":
            let obj = CryptoJS.AES.encrypt(JSON.stringify(value), SECRET_KEY);
            e = obj.toString();
            break;
        default:
            let str = CryptoJS.AES.encrypt(value, SECRET_KEY);
            e = str.toString();
            break;
    }
    return e;
}

export function decrypt(value, type = "string") {
    let d;
    switch (type) {
        case "object":
            let obj = CryptoJS.AES.decrypt(value.toString(), SECRET_KEY);
            d = JSON.parse(obj.toString(CryptoJS.enc.Utf8))
            break;
        default:
            let str = CryptoJS.AES.decrypt(value.toString(), SECRET_KEY);
            d = str.toString(CryptoJS.enc.Utf8);
            break;
    }
    return d;
}

export function greetings() {
    var today = new Date();
    var hourNow = today.getHours();
    var greeting ;
    
    if (hourNow >= 0 && hourNow < 11) {
        greeting = lang.goodMorning[lang.default];
    } else if (hourNow >= 11 && hourNow < 14) {
        greeting = lang.goodDay[lang.default];
    } else if (hourNow >= 14 && hourNow < 18) {
        greeting = lang.goodAfternoon[lang.default];
    } else if (hourNow >= 18) {
        greeting = lang.goodNight[lang.default];
    }

    return greeting;
}
