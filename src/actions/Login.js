/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-07 10:14:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-23 18:58:09
 */
import { POST_LOGIN, POST_LOGIN_SUCCESS, POST_LOGIN_FAILURE, GET_LOGOUT, GET_LOGOUT_SUCCESS, GET_LOGOUT_FAILURE, POST_LOGIN_SOSMED, POST_LOGIN_SOSMED_SUCCESS, POST_LOGIN_SOSMED_FAILURE, DELETE_PREV_LOGIN, IS_LOGIN } from "./Types";

// LOGIN
export const postLogin = FormData => ({
    type: POST_LOGIN,
    payload: FormData
});

export const postLoginSuccess = response => ({
    type: POST_LOGIN_SUCCESS,
    payload: response
});

export const postLoginFailure = error => ({
    type: POST_LOGIN_FAILURE,
    payload: error
});

export const deletePrevLogin = () => ({
    type: DELETE_PREV_LOGIN
})
// LOGIN

// LOGOUT
export const getLogout = () => ({
    type: GET_LOGOUT
});

export const getLogoutSuccess = response => ({
    type: GET_LOGOUT_SUCCESS,
    payload: response
});

export const getLogoutFailure = error => ({
    type: GET_LOGOUT_FAILURE,
    payload: error
});
// LOGOUT

// LOGIN SOSMED
export const postLoginSosmed = FormData => ({
    type: POST_LOGIN_SOSMED,
    payload: FormData
});

export const postLoginSosmedSuccess = response => ({
    type: POST_LOGIN_SOSMED_SUCCESS,
    payload: response
});

export const postLoginSosmedFailure = error => ({
    type: POST_LOGIN_SOSMED_FAILURE,
    payload: error
});
// LOGIN SOSMED

// CEK LOGIN
export const checkIsLogin = () => ({
    type: IS_LOGIN
});