/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-17 10:56:03 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-01-17 10:56:03 
 */
import { GET_MASTER_CAR_TYPE, GET_MASTER_CAR_TYPE_SUCCESS, GET_MASTER_CAR_TYPE_FAILURE } from "./Types";

export const getMasterCarType = params => ({
    type: GET_MASTER_CAR_TYPE,
    payload: params
});

export const getMasterCarTypeSuccess = response => ({
    type: GET_MASTER_CAR_TYPE_SUCCESS,
    payload: response
});

export const getMasterCarTypeFailure = error => ({
    type: GET_MASTER_CAR_TYPE_FAILURE,
    payload: error
});