/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-11 14:37:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-12 13:57:40
 */
import { AIRPORT_ACTIVE_INPUT_FORM, GET_DURATION_AIRPORT_TRANSFER, GET_DURATION_AIRPORT_TRANSFER_SUCCESS, GET_DURATION_AIRPORT_TRANSFER_FAILURE } from "./Types";

export const airportActiveInputForm = inputShow => ({
    type: AIRPORT_ACTIVE_INPUT_FORM,
    payload: inputShow
});

// GET DURATION AIRPORT TRANSFER START HERE
export const getDurationAirportTransfer = params => ({
    type: GET_DURATION_AIRPORT_TRANSFER,
    payload: params
});

export const getDurationAirportTransferSuccess = response => ({
    type: GET_DURATION_AIRPORT_TRANSFER_SUCCESS,
    payload: response
});

export const getDurationAirportTransferFailure = error => ({
    type: GET_DURATION_AIRPORT_TRANSFER_FAILURE,
    payload: error
});
// GET DURATION AIRPORT TRANSFER END HERE