/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-07 14:07:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-21 14:23:30
 */
import { GET_STOCK_CAR_RENTAL, GET_STOCK_CAR_RENTAL_SUCCESS, GET_STOCK_CAR_RENTAL_FAILURE, GET_PACKAGE_CAR_RENTAL, GET_PACKAGE_CAR_RENTAL_SUCCESS, GET_PACKAGE_CAR_RENTAL_FAILURE, GET_CITY_COVERAGE_CAR_RENTAL, GET_CITY_COVERAGE_CAR_RENTAL_SUCCESS, GET_CITY_COVERAGE_CAR_RENTAL_FAILURE, MAX_CART_UNIT_VEHICLE, SELECTED_CAR_ORDER_CAR_RENTAL, GET_EXTRAS_CAR_RENTAL, GET_EXTRAS_CAR_RENTAL_SUCCESS, GET_EXTRAS_CAR_RENTAL_FAILURE, GET_CAR_RENTAL_PRICE, GET_CAR_RENTAL_PRICE_SUCCESS, GET_CAR_RENTAL_PRICE_FAILURE, POST_RESERVATION_CAR_RENTAL, POST_RESERVATION_CAR_RENTAL_SUCCESS, POST_RESERVATION_CAR_RENTAL_FAILURE, NOTES_REQUEST_CAR_RENTAL, PASSANGER_INFO_CAR_RENTAL, CLEAR_CAR_RENTAL_PRICE, FILTER_CAR_TYPE_CAR_RENTAL, FILTER_PRICE_RANGE_CAR_RENTAL, SORT_PRICE_CAR_RENTAL, VALIDATE_STOCK_EXTRAS } from "./Types";

// PACKAGE CAR RENTAL START HERE
export const getPackageCarRental = () => ({
    type: GET_PACKAGE_CAR_RENTAL
});
export const getPackageCarRentalSuccess = response => ({
    type: GET_PACKAGE_CAR_RENTAL_SUCCESS,
    payload: response
});
export const getPackageCarRentalFailure = error => ({
    type: GET_PACKAGE_CAR_RENTAL_FAILURE,
    payload: error
});
// PACKAGE CAR RENTAL END HERE

// CITY COVERAGE CAR RENTAL START HERE
export const getCityCoverageCarRental = params => ({
    type: GET_CITY_COVERAGE_CAR_RENTAL,
    payload: params
});
export const getCityCoverageCarRentalSuccess = response => ({
    type: GET_CITY_COVERAGE_CAR_RENTAL_SUCCESS,
    payload: response
});
export const getCityCoverageCarRentalFailure = error => ({
    type: GET_CITY_COVERAGE_CAR_RENTAL_FAILURE,
    payload: error
});
// CITY COVERAGE CAR RENTAL END HERE

// STOCK CAR RENTAL START HERE
export const getStockCarRental = params => ({
    type: GET_STOCK_CAR_RENTAL,
    payload: params
});

export const getStockCarRentalSuccess = response => ({
    type: GET_STOCK_CAR_RENTAL_SUCCESS,
    payload: response
});

export const getStockCarRentalFailure = error => ({
    type: GET_STOCK_CAR_RENTAL_FAILURE,
    payload: error
});
// STOCK CAR RENTAL END HERE

// VALIDATION TOTAL CART 5 UNIT VEHICLE
export const validateTotalCartUnitVehicle = amount => ({
    type: MAX_CART_UNIT_VEHICLE,
    payload: amount
});

// SELECTED CAR ORDER (CAR RENTAL)
export const selectedCarOrderCarRental = selectedCar => ({
    type: SELECTED_CAR_ORDER_CAR_RENTAL,
    payload: selectedCar
});

// EXTRAS CAR RENTAL START HERE
export const getExtrasCarRental = params => ({
    type: GET_EXTRAS_CAR_RENTAL,
    payload: params
});

export const getExtrasCarRentalSuccess = response => ({
    type: GET_EXTRAS_CAR_RENTAL_SUCCESS,
    payload: response
});

export const getExtrasCarRentalFailure = error => ({
    type: GET_EXTRAS_CAR_RENTAL_FAILURE,
    payload: error
});

export const validateStockExtras = params => ({
    type: VALIDATE_STOCK_EXTRAS,
    payload: params
});
// EXTRAS CAR RENTAL END HERE

// PRICE CONFIGURATION START HERE
export const getPriceCarRental = params => ({
    type: GET_CAR_RENTAL_PRICE,
    payload: params
});

export const getPriceCarRentalSuccess = response => ({
    type: GET_CAR_RENTAL_PRICE_SUCCESS,
    payload: response
});

export const getPriceCarRentalFailure = error => ({
    type: GET_CAR_RENTAL_PRICE_FAILURE,
    payload: error
});

export const clearPrevPriceCarRental = () => ({
    type: CLEAR_CAR_RENTAL_PRICE
})
// PRICE CONFIGURATION END HERE

// SAVE RESERVATION START HERE
export const postReservationCarRental = FormData => ({
    type: POST_RESERVATION_CAR_RENTAL,
    payload: FormData
});

export const postReservationCarRentalSuccess = response => ({
    type: POST_RESERVATION_CAR_RENTAL_SUCCESS,
    payload: response
});

export const postReservationCarRentalFailure = error => ({
    type: POST_RESERVATION_CAR_RENTAL_FAILURE,
    payload: error
});
// SAVE RESERVATION END HERE

// NOTES / REQUEST CAR RENTAL START HERE
export const notesRequestCarRental = text => ({
    type: NOTES_REQUEST_CAR_RENTAL,
    payload: text
});
// NOTES / REQUEST CAR RENTAL END HERE

// PASSANGER CAR RENTAL START HERE
export const passangerCarRental = passanger => ({
    type: PASSANGER_INFO_CAR_RENTAL,
    payload: passanger
})
// PASSANGER CAR RENTAL END HERE

// FILTER CAR TYPE CAR RENTAL START HERE
export const filterCarTypeCarRental = filter => ({
    type: FILTER_CAR_TYPE_CAR_RENTAL,
    payload: filter
});
// FILTER CAR TYPE CAR RENTAL END HERE

// FILTER PRICE RANGE CAR RENTAL START HERE
export const filterPriceRangeCarRental = filter => ({
    type: FILTER_PRICE_RANGE_CAR_RENTAL,
    payload: filter
});
// FILTER PRICE RANGE CAR RENTAL END HERE

// SORT PRICE CAR RENTAL START HERE
export const sortPriceCarRental = value => ({
    type: SORT_PRICE_CAR_RENTAL,
    payload: value
});
// SORT PRICE CAR RENTAL END HERE