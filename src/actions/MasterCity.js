/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-07 10:52:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-07 15:04:45
 */
import { GET_MASTER_CITY, GET_MASTER_CITY_SUCCESS, GET_MASTER_CITY_FAILURE } from "./Types";

export const getMasterCity = () => ({
    type: GET_MASTER_CITY
});

export const getMasterCitySuccess = response => ({
    type: GET_MASTER_CITY_SUCCESS,
    payload: response
});

export const getMasterCityFailure = error => ({
    type: GET_MASTER_CITY_FAILURE,
    payload: error
});