/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 20:21:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 01:01:41
 */
import { POST_CREATE_INVOICE, POST_CREATE_INVOICE_SUCCESS, POST_CREATE_INVOICE_FAILURE, IS_PAYMENT } from "./Types";

export const createInvoice = FormData => ({
    type: POST_CREATE_INVOICE,
    payload: FormData
});

export const createInvoiceSuccess = response => ({
    type: POST_CREATE_INVOICE_SUCCESS,
    payload: response
});

export const createInvoiceFailure = error => ({
    type: POST_CREATE_INVOICE_FAILURE,
    payload: error
});

export const isPaymentValidate = value => ({
    type: IS_PAYMENT,
    payload : value
});