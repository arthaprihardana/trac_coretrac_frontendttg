/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-03 10:20:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-15 10:59:24
 */
import {
    POST_REGISTER,
    POST_REGISTER_SUCCESS,
    POST_REGISTER_FAILURE,
    DELETE_PREV_REGISTER, GET_CAPTCHA, GET_CAPTCHA_SUCCESS, GET_CAPTCHA_FAILURE
} from './Types';

export const postRegister = FormData => ({
    type: POST_REGISTER,
    payload: FormData
});

export const postRegisterSuccess = response => ({
    type: POST_REGISTER_SUCCESS,
    payload: response
});

export const postRegisterFailure = error => ({
    type: POST_REGISTER_FAILURE,
    payload: error
});

export const deletePrevRegister = () => ({
    type: DELETE_PREV_REGISTER
});


export const getCaptcha = params => ({
    type: GET_CAPTCHA,
    payload: params
});

export const getCaptchaSuccess = response => ({
    type: GET_CAPTCHA_SUCCESS,
    payload: response
});

export const getCaptchaFailure = error => ({
    type: GET_CAPTCHA_FAILURE,
    payload: error
});