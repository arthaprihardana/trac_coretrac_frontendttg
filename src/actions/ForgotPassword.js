/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-04 10:00:02 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-15 17:04:03
 */
import {
    POST_FORGOT_PASSWORD,
    POST_FORGOT_PASSWORD_SUCCESS,
    POST_FORGOT_PASSWORD_FAILURE,
    DELETE_PREV_FORGOT_PASSWORD,
    POST_NEW_PASSWORD, POST_NEW_PASSWORD_SUCCESS, POST_NEW_PASSWORD_FAILURE
} from "./Types";

export const postForgotPassword = FormData => ({
    type: POST_FORGOT_PASSWORD,
    payload: FormData
});

export const postForgotPasswordSuccess = response => ({
    type: POST_FORGOT_PASSWORD_SUCCESS,
    payload: response
});

export const postForgotPasswordFailure = error => ({
    type: POST_FORGOT_PASSWORD_FAILURE,
    payload: error
});
export const postNewPassword = FormData => ({
    type: POST_NEW_PASSWORD,
    payload: FormData
});

export const postNewPasswordSuccess = response => ({
    type: POST_NEW_PASSWORD_SUCCESS,
    payload: response
});

export const postNewPasswordFailure = error => ({
    type: POST_NEW_PASSWORD_FAILURE,
    payload: error
});

export const deletePrevForgotPassword = () => ({
    type: DELETE_PREV_FORGOT_PASSWORD
});