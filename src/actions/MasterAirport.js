/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-11 09:09:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-11 16:40:13
 */
import { GET_MASTER_AIRPORT, GET_MASTER_AIRPORT_SUCCESS, GET_MASTER_AIRPORT_FAILURE, GET_MASTER_AIRPORT_CITY_COVERAGE, GET_MASTER_AIRPORT_CITY_COVERAGE_SUCCESS, GET_MASTER_AIRPORT_CITY_COVERAGE_FAILURE } from "./Types";

// GET MASTER AIRPORT START HERE
export const getMasterAirport = () => ({
    type: GET_MASTER_AIRPORT
});

export const getMasterAirportSuccess = response => ({
    type: GET_MASTER_AIRPORT_SUCCESS,
    payload: response
});

export const getMasterAirportFailure = error => ({
    type: GET_MASTER_AIRPORT_FAILURE,
    payload: error
});
// GET MASTER AIRPORT END HERE

// GET MASTER AIRPORT CITY COVERAGE START HERE
export const getMasterAirportCityCoverage = params => ({
    type: GET_MASTER_AIRPORT_CITY_COVERAGE,
    payload: params
});

export const getMasterAirportCityCoverageSuccess = response => ({
    type: GET_MASTER_AIRPORT_CITY_COVERAGE_SUCCESS,
    payload: response
});

export const getMasterAirportCityCoverageFailure = error => ({
    type: GET_MASTER_AIRPORT_CITY_COVERAGE_FAILURE,
    payload: error
});
// GET MASTER AIRPORT CITY COVERAGE END HERE