import {GET_VEHICLE_ATTRIBUTE_FAILURE, GET_VEHICLE_ATTRIBUTE_SUCCESS, GET_VEHICLE_ATTRIBUTE} from "./Types";

export const getVehicleAttribute = params => ({
    type: GET_VEHICLE_ATTRIBUTE,
    payload: params
});

export const getVehicleAttributeSuccess = response => ({
    type: GET_VEHICLE_ATTRIBUTE_SUCCESS,
    payload: response
});

export const getVehicleAttributeFailure = error => ({
    type: GET_VEHICLE_ATTRIBUTE_FAILURE,
    payload: error
});