/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 16:43:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-14 17:19:52
 */
import { GET_BUS_RENTAL_PRICE, GET_BUS_RENTAL_PRICE_SUCCESS, GET_BUS_RENTAL_PRICE_FAILURE, GET_DISTANCE_BUS } from "./Types";

export const getBusRentalPrice = params => ({
    type: GET_BUS_RENTAL_PRICE,
    payload: params
});

export const getBusRentalPriceSuccess = response => ({
    type: GET_BUS_RENTAL_PRICE_SUCCESS,
    payload: response
});

export const getBusRentalPriceFailure = error => ({
    type: GET_BUS_RENTAL_PRICE_FAILURE,
    payload: error
});

export const getDistanceBus = distance => ({
    type: GET_DISTANCE_BUS,
    payload: distance
});