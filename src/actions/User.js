/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-22 16:21:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-22 16:22:05
 */
import {
    GET_USER_PROFILE,
    GET_USER_PROFILE_SUCCESS,
    GET_USER_PROFILE_FAILURE,
    FORM_ACCOUNT,
    FORM_IDENTIFICATION,
    PUT_USER_PROFILE,
    PUT_USER_PROFILE_SUCCESS,
    PUT_USER_PROFILE_FAILURE,

    CONTACT_US,
    CONTACT_US_SUCCESS,
    CONTACT_US_FAILURE,
    GET_ACTIVATE_USER,
    GET_ACTIVATE_USER_SUCCESS,
    PUT_PASSWORD,
    PUT_PASSWORD_SUCCESS,
    PUT_PASSWORD_FAILURE,
    CHECK_TOKEN,
    CHECK_TOKEN_SUCCESS, CHECK_TOKEN_FAILURE
} from "./Types";


export const getActivateUser = params => ({
    type: GET_ACTIVATE_USER,
    payload: params
});

export const getActivateUserSuccess = response => ({
    type: GET_ACTIVATE_USER_SUCCESS,
    payload: response
});

export const getActivateUserFailure = error => ({
    type: GET_ACTIVATE_USER_SUCCESS,
    payload: error
});

export const getUserProfile = params => ({
    type: GET_USER_PROFILE,
    payload: params
});

export const getUserProfileSuccess = response => ({
    type: GET_USER_PROFILE_SUCCESS,
    payload: response
});

export const getUserProfileFailure = error => ({
    type: GET_USER_PROFILE_FAILURE,
    payload: error
});

export const checkToken = params => ({
    type: CHECK_TOKEN,
    payload: params
});

export const checkTokenSuccess = response => ({
    type: CHECK_TOKEN_SUCCESS,
    payload: response
});

export const checkTokenFailure = error => ({
    type: CHECK_TOKEN_FAILURE,
    payload: error
});

export const updateUserProfile = FormData =>({
    type: PUT_USER_PROFILE,
    payload: FormData
});

export const updateUserProfileSuccess = response => ({
    type: PUT_USER_PROFILE_SUCCESS,
    payload: response
});

export const updateUserProfileFailure = error => ({
    type: PUT_USER_PROFILE_FAILURE,
    payload: error
});

export const updatePassword = FormData =>({
    type: PUT_PASSWORD,
    payload: FormData
});

export const updatePasswordSuccess = response => ({
    type: PUT_PASSWORD_SUCCESS,
    payload: response
});

export const updatePasswordFailure = error => ({
    type: PUT_PASSWORD_FAILURE,
    payload: error
});

export const setFormAccount = obj => ({
    type: FORM_ACCOUNT,
    payload: obj
});

export const setFormIdentification = obj => ({
    type: FORM_IDENTIFICATION,
    payload: obj
});

export const createContactUs = FormData =>({
    type: CONTACT_US,
    payload: FormData
});

export const createContactUsSuccess = response => ({
    type: CONTACT_US_SUCCESS,
    payload: response
});

export const createContactUsFailure = error => ({
    type: CONTACT_US_FAILURE,
    payload: error
});