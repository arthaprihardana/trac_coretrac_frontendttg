/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-01 13:38:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 16:00:23
 */
import { GET_MASTER_BRANCH, GET_MASTER_BRANCH_SUCCESS, GET_MASTER_BRANCH_FAILURE, SET_BRANCH_FROM_BUS } from "./Types";

export const getMasterBranch = params => ({
    type: GET_MASTER_BRANCH,
    payload: params
});

export const getMasterBranchSuccess = response => ({
    type: GET_MASTER_BRANCH_SUCCESS,
    payload: response
});

export const getMasterBranchFailure = error => ({
    type: GET_MASTER_BRANCH_FAILURE,
    payload: error
});

export const setBranchFromBus = value => ({
    type: SET_BRANCH_FROM_BUS,
    payload: value
});