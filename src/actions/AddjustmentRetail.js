/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-24 19:18:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 19:19:06
 */
import { GET_ADJUSTMENT_RETAIL, GET_ADJUSTMENT_RETAIL_SUCCESS, GET_ADJUSTMENT_RETAIL_FAILURE } from "./Types";

export const getAdjustmentRetail = () => ({
    type: GET_ADJUSTMENT_RETAIL
});

export const getAdjustmentRetailSuccess = response => ({
    type: GET_ADJUSTMENT_RETAIL_SUCCESS,
    payload: response
});

export const getAdjustmentRetailFailure = error => ({
    type: GET_ADJUSTMENT_RETAIL_FAILURE,
    payload: error
});