/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-15 14:41:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-16 16:49:52
 */
import {
    GET_LIST_PRODUCT,
    GET_LIST_PRODUCT_SUCCESS,
    GET_LIST_PRODUCT_FAILURE,
    SELECTED_PRODUCT_SERVICE_TYPE,
    GET_LIST_ARTICLE,
    GET_LIST_ARTICLE_SUCCESS,
    GET_LIST_ARTICLE_FAILURE,
    GET_LIST_PROMO,
    GET_LIST_PROMO_SUCCESS,
    GET_LIST_PROMO_FAILURE
} from "./Types";

export const getListProduct = () => ({
    type: GET_LIST_PRODUCT
});

export const getListProductSuccess = response => ({
    type: GET_LIST_PRODUCT_SUCCESS,
    payload: response
});

export const getListProductFailure = error => ({
    type: GET_LIST_PRODUCT_FAILURE,
    payload: error
});

export const getListArticle = () => ({
    type: GET_LIST_ARTICLE
});

export const getListArticleSuccess = response => ({
    type: GET_LIST_ARTICLE_SUCCESS,
    payload: response
});

export const getListArticleFailure = error => ({
    type: GET_LIST_ARTICLE_FAILURE,
    payload: error
});

export const getListPromo = () => ({
    type: GET_LIST_PROMO
});

export const getListPromoSuccess = response => ({
    type: GET_LIST_PROMO_SUCCESS,
    payload: response
});

export const getListPromoFailure = error => ({
    type: GET_LIST_PROMO_FAILURE,
    payload: error
});

export const selectedProductServiceType = selected => ({
    type: SELECTED_PRODUCT_SERVICE_TYPE,
    payload: selected
});