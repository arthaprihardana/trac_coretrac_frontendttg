/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 00:31:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-14 00:32:42
 */
import { GET_MASTER_BANK, GET_MASTER_BANK_SUCCESS, GET_MASTER_BANK_FAILURE } from "./Types";

export const getBank = params => ({
    type: GET_MASTER_BANK,
    payload: params
});

export const getbankSuccess = response => ({
    type: GET_MASTER_BANK_SUCCESS,
    payload: response
});

export const getBankFailure = error => ({
    type: GET_MASTER_BANK_FAILURE,
    payload: error
});