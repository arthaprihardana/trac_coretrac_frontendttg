/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-24 09:42:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-24 23:39:19
 */
import {
    GET_MY_BOOKING,
    GET_MY_BOOKING_SUCCESS,
    GET_MY_BOOKING_FAILURE,
    GET_DETAIL_MY_BOOKING,
    GET_MY_HISTORY,
    GET_MY_HISTORY_SUCCESS,
    GET_MY_HISTORY_FAILURE,
    GET_DETAIL_MY_BOOKING_SUCCESS,
    GET_DETAIL_MY_BOOKING_FAILURE,
    GET_CREDIT_CARD,
    GET_CREDIT_CARD_SUCCESS,
    GET_CREDIT_CARD_FAILURE,
    POST_CREDIT_CARD,
    POST_CREDIT_CARD_SUCCESS,
    POST_CREDIT_CARD_FAILURE,
    PUT_CREDIT_CARD,
    PUT_CREDIT_CARD_SUCCESS,
    PUT_CREDIT_CARD_FAILURE,
    DELETE_CREDIT_CARD,
    DELETE_CREDIT_CARD_SUCCESS,
    DELETE_CREDIT_CARD_FAILURE,
    PUT_PRIMARY_CC,
    PUT_PRIMARY_CC_SUCCESS, PUT_PRIMARY_CC_FAILURE
} from "./Types";

export const getMyBooking = params => ({
    type: GET_MY_BOOKING,
    payload: params
});

export const getMyBookingSuccess = response => ({
    type: GET_MY_BOOKING_SUCCESS,
    payload: response
});

export const getMyBookingFailure = error => ({
    type: GET_MY_BOOKING_FAILURE,
    payload: error
});

export const getMyHistory = params => ({
    type: GET_MY_HISTORY,
    payload: params
});

export const getMyHistorySuccess = response => ({
    type: GET_MY_HISTORY_SUCCESS,
    payload: response
});

export const getMyHistoryFailure = error => ({
    type: GET_MY_HISTORY_FAILURE,
    payload: error
});

export const getDetailMyBooking = value => ({
    type: GET_DETAIL_MY_BOOKING,
    payload: value
});

export const getDetailMyBookingSuccess = response => ({
    type: GET_DETAIL_MY_BOOKING_SUCCESS,
    payload: response
});

export const getDetailMyBookingFailure = error => ({
    type: GET_DETAIL_MY_BOOKING_FAILURE,
    payload: error
});
export const getCreditCard = value => ({
    type: GET_CREDIT_CARD,
    payload: value
});

export const getCreditCardSuccess = response => ({
    type: GET_CREDIT_CARD_SUCCESS,
    payload: response
});

export const getCreditCardFailure = error => ({
    type: GET_CREDIT_CARD_FAILURE,
    payload: error
});
export const postCreditCard = FormData => ({
    type: POST_CREDIT_CARD,
    payload: FormData
});

export const postCreditCardSuccess = response => ({
    type: POST_CREDIT_CARD_SUCCESS,
    payload: response
});

export const postCreditCardFailure = error => ({
    type: POST_CREDIT_CARD_FAILURE,
    payload: error
});
export const putCreditCard = FormData => ({
    type: PUT_CREDIT_CARD,
    payload: FormData
});

export const putCreditCardSuccess = response => ({
    type: PUT_CREDIT_CARD_SUCCESS,
    payload: response
});

export const putCreditCardFailure = error => ({
    type: PUT_CREDIT_CARD_FAILURE,
    payload: error
});
export const deleteCreditCard = FormData => ({
    type: DELETE_CREDIT_CARD,
    payload: FormData
});

export const deleteCreditCardSuccess = response => ({
    type: DELETE_CREDIT_CARD_SUCCESS,
    payload: response
});

export const deleteCreditCardFailure = error => ({
    type: DELETE_CREDIT_CARD_FAILURE,
    payload: error
});


export const putPrimaryCC = FormData => ({
    type: PUT_PRIMARY_CC,
    payload: FormData
});

export const putPrimaryCCSuccess = response => ({
    type: PUT_PRIMARY_CC_SUCCESS,
    payload: response
});

export const putPrimaryCCFailure = error => ({
    type: PUT_PRIMARY_CC_FAILURE,
    payload: error
});
