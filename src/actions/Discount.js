/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-22 13:59:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 17:42:09
 */
import { GET_VALIDATE_PROMO, GET_VALIDATE_PROMO_SUCCESS, GET_VALIDATE_PROMO_FAILURE, IS_PROMO_VALIDATE, PROMO_CODE } from "./Types";

export const getValidatePromo = params => ({
    type: GET_VALIDATE_PROMO,
    payload: params
});

export const getValidatePromoSuccess = response => ({
    type: GET_VALIDATE_PROMO_SUCCESS,
    payload: response
});

export const getValidatePromoFailure = error => ({
    type: GET_VALIDATE_PROMO_FAILURE,
    payload: error
});

export const isPromoValidate = value => ({
    type: IS_PROMO_VALIDATE,
    payload: value
});

export const savePromoCode = value => ({
    type: PROMO_CODE,
    payload: value
});