/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-22 16:10:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-14 11:47:06
 */
import { GET_ADDRESS_GM, GET_ADDRESS_GM_SUCCESS, GET_ADDRESS_GM_FAILURE, GET_PLACE_DETAIL_GM, GET_PLACE_DETAIL_GM_SUCCESS, GET_PLACE_DETAIL_GM_FAILURE, GET_GEOMETRY_GM, GET_GEOMETRY_GM_SUCCESS, GET_GEOMETRY_GM_FAIURE, GET_DISTANCE_MATRIX_GM, GET_DISTANCE_MATRIX_GM_FAILURE, GET_DISTANCE_MATRIX_GM_SUCCESS, GET_DISTANCE_MATRIX_BUS, GET_DISTANCE_MATRIX_BUS_SUCCESS, GET_DISTANCE_MATRIX_BUS_FAILURE } from "./Types";

// AUTOCOMPLETE FROM GOOGLE MAPS START HERE
export const getAddressFromGM = input => ({
    type: GET_ADDRESS_GM,
    payload: input
});

export const getAddressFromGMSuccess = response => ({
    type: GET_ADDRESS_GM_SUCCESS,
    payload: response
});

export const getAddressFromGMFailure = error => ({
    type: GET_ADDRESS_GM_FAILURE,
    payload: error
});
// AUTOCOMPLETE FROM GOOGLE MAPS END HERE

// PLACE DETAIL FROM GOOGLE MAPS START HERE
export const getPlaceDetailFromGM = input => ({
    type: GET_PLACE_DETAIL_GM,
    payload: input
});

export const getPlaceDetailFromGMSuccess = response => ({
    type: GET_PLACE_DETAIL_GM_SUCCESS,
    payload: response
});

export const getPlaceDetailFromGMFailure = error => ({
    type: GET_PLACE_DETAIL_GM_FAILURE,
    payload: error
});
// PLACE DETAIL FROM GOOGLE MAPS END HERE

// FIND PLACE FROM GOOGLE MAPS START HERE
export const getFindGeometryFromGM = input => ({
    type: GET_GEOMETRY_GM,
    payload: input
});

export const getFindGeometryFromGMSuccess = response => ({
    type: GET_GEOMETRY_GM_SUCCESS,
    payload: response
});

export const getFindGeometryFromGMFailure = error => ({
    type: GET_GEOMETRY_GM_FAIURE,
    payload: error
});
// FIND PLACE FROM GOOGLE MAPS END HERE

// GET DISTANCE MATRIX GOOGLE MAPS START HERE
export const getDistanceMatrixFromGM = params => ({
    type: GET_DISTANCE_MATRIX_GM,
    payload: params
});

export const getDistanceMatrixFromGMSuccess = response => ({
    type: GET_DISTANCE_MATRIX_GM_SUCCESS,
    payload: response
});

export const getDistanceMatrixFromGMFailure = error => ({
    type: GET_DISTANCE_MATRIX_GM_FAILURE,
    payload: error
});
// GET DISTANCE MATRIX GOOGLE MAPS END HERE

// GET DISTANCE MATRIX BUS GOOGLE MAPS START HERE
export const getDistanceMatrixBus = params => ({
    type: GET_DISTANCE_MATRIX_BUS,
    payload: params
});

export const getDistanceMatrixBusSuccess = response => ({
    type: GET_DISTANCE_MATRIX_BUS_SUCCESS,
    payload: response
});

export const getDistanceMatrixBusFailure = error => ({
    type: GET_DISTANCE_MATRIX_BUS_FAILURE,
    payload: error
});
// GET DISTANCE MATRIX BUS GOOGLE MAPS END HERE