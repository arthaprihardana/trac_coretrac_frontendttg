import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

// components
import ModalHeader from "./ModalHeader";
import ModalContent from "./ModalContent";
import ModalFooter from "./ModalFooter";

// styles
import "./Modal.scss";

const Modal = (props) => {
    const { open, size, onClick, header, content, footer } = props;
    const className = cx("a-modal", {
        open
    });

    const contentWrapperClasses = cx("content-wrapper", {
        small: size === "small",
        medium: size === "medium",
        large: size === "large"
    });

    return (
        <div
            onClick={e => onClick(e)}
            className={className}
            role="presentation"
        >
            <div
                onClick={e => e.stopPropagation()}
                className={contentWrapperClasses}
                role="presentation"
            >
                <ModalHeader {...header} />
                <ModalContent {...content} />
                <ModalFooter {...footer} />
            </div>
        </div>
    );

};

Modal.defaultProps = {
    open: false,
    size: "medium",
    onClick: e => console.log("modal element:", e.target),
};

Modal.propTypes = {
    open: PropTypes.bool,
    size: PropTypes.string,
    onClick: PropTypes.func,
    header: PropTypes.shape({
        withCloseButton: PropTypes.bool,
        children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
        onClick: PropTypes.func,
        position: PropTypes.string
    }).isRequired,
    content: PropTypes.shape({
        children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
        position: PropTypes.string
    }).isRequired,
    footer: PropTypes.shape({
        children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
        position: PropTypes.string
    }).isRequired
};

export default React.memo(Modal);