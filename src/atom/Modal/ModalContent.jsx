import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

const ModalContent = props => {
    const { children, position } = props;
    const className = cx("modal-content", {
        left: position === "left",
        center: position === "center",
        right: position === "right"
    });

    return <div className={className}>{children}</div>;
};

ModalContent.defaultProps = {
    children: "Put modal content here...",
    position: "left"
};

ModalContent.propTypes = {
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    position: PropTypes.string
};

export default React.memo(ModalContent);