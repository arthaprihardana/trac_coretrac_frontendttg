import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

const ModalHeader = props => {
    const { children, withCloseButton, onClick, position } = props;
    const className = cx("modal-header", {
        left: position === "left",
        center: position === "center",
        right: position === "right"
    });

    return (
        <React.Fragment>
            <header className={className}>{children}</header>
            {withCloseButton && (
                <button
                    onClick={e => onClick(e)}
                    type="button"
                    className="close"
                />
            )}
        </React.Fragment>
    );
};

ModalHeader.defaultProps = {
    children: "Example Modal Header",
    withCloseButton: false,
    onClick: e => console.log("header element:", e.target), // eslint-disable-line
    position: "left"
};

ModalHeader.propTypes = {
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    withCloseButton: PropTypes.bool,
    onClick: PropTypes.func,
    position: PropTypes.string
};

export default React.memo(ModalHeader);