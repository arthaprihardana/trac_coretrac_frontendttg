import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

// components
import { Button } from "atom";

const ModalFooter = (props) => {
    const { position, children } = props;
    const className = cx("modal-footer", {
        left: position === "left",
        center: position === "center",
        right: position === "right"
    });

    return <footer className={className}>{children}</footer>;
}

ModalFooter.defaultProps = {
    children: <Button primary>OK</Button>,
    position: "right"
};

ModalFooter.propTypes = {
    children: PropTypes.node,
    position: PropTypes.string
};

export default React.memo(ModalFooter);