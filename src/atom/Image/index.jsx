import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import "./Image.scss";

// image dummy
import DealImage from "assets/images/dummy/deal-car-1.jpg";

/**
 * Image Component
 * @param {Object} props src, alt, tiny, small, medium, large, big, rounded
 */
const Image = (props) => {
    const { src, alt, ...rest } = props;
    const { tiny, small, medium, large, big, rounded } = rest;

    const className = cx("a-image", {
        "image-tiny": tiny,
        "image-small": small,
        "image-medium": medium,
        "image-large": large,
        "image-big": big,
        rounded
    });

    return <img src={src} alt={alt} className={className} />;

};

Image.defaultProps = {
    alt: "Default Image",
    src: DealImage
};

Image.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string
};

export default React.memo(Image);