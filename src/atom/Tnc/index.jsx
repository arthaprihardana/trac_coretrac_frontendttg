import React from 'react';
import PropTypes from 'prop-types';
import './Tnc.scss';

const Tnc = ({data, onClick}) => {

    return (
        <div className="tnc-list">
            <ul>
                {data.map(tnc => {
                    return <li key={tnc.id} onClick={() => onClick(tnc)}>{tnc.title}</li>
                })}
            </ul>
        </div>
    )
}

Tnc.propTypes = {
    data: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            type: PropTypes.string,
            content: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
        })
    ),
    onClick: PropTypes.func
}

export default Tnc;