// core
import React from "react";
import PropTypes from "prop-types";

// styles
import "./BackgroundOverlay.scss";

// background overlay component
const BackgroundOverlay = ({ show, onClick }) => {
    let className = "bg-overlay";
    if (show) {
        className += " show";
    }

    return (
        <div className={className} onClick={onClick} />
    );

};

// default props
BackgroundOverlay.defaultProps = {
    show: false,
    onClick: () => console.log("clicked!")
};

// props types
BackgroundOverlay.propTypes = {
    show: PropTypes.bool,
    onClick: PropTypes.func
};

export default React.memo(BackgroundOverlay);