/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-05 14:01:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-06 04:28:29
 */
import React from "react";
import PropTypes from "prop-types";
import { Field, ErrorMessage } from "formik";
import cx from "classnames";
import { map } from "lodash";
import Slider from "rc-slider";
import DatePicker from 'react-datepicker';

// style
import "rc-slider/assets/index.css";
import "./TracField.scss";
import { OutsideClick } from "../../organisms";

// images
import flagId from "assets/images/flags/id.svg";
import flagEn from "assets/images/flags/en.svg";


const TracField = (props) => {
    const { id, name, disabled,  type, labelName, labelSize, validate, autoComplete, icon, onClick, onChange, onBlur, placeholder, options, showItem, onItemClick, checked, value, listHasIcon, displayIcon, initial, children, defaultValue, min, max } = props;
    
    const classIcon = cx("icon", {
        "password-hide": icon === "password hide",
        "password-show": icon === "password show",
        "clock": icon === "clock",
    });

    const classLabel = cx("label", {
        small: labelSize === "small",
        medium: labelSize === "medium"
    });

    const classSelectList = cx("select",{
        'list-has-icon': listHasIcon
    });

    const classSelectItem = cx("select-item", {
        active: showItem
    });

    const classTracField = cx("a-trac-field", {
        'initial': initial
    })
    const classSelectedSelect = cx("selected", {
        "display-icon": displayIcon
    });

    return (
        <div className={classTracField}>
            {(type === "text" || type === "password" || type === "number") && (
                <React.Fragment>
                    <Field id={id} disabled={disabled}  name={name} type={type} validate={validate} autoComplete={autoComplete} component="input" onChange={onChange} onBlur={onBlur} value={value} required />
                    <label htmlFor={id}>{labelName}</label>
                    <span className="highlight" />
                    <span className="bar" />
                    {icon !== "" && <button type="button" onClick={e => onClick(e)} className={classIcon} />}
                </React.Fragment>
            )}

            {(type === "time") && (
                <React.Fragment>
                    <DatePicker
                        selected={value}
                        onChange={onChange}
                        showTimeSelect
                        showTimeSelectOnly
                        timeIntervals={15}
                        timeFormat="HH:mm"
                        dateFormat="HH:mm"
                        timeCaption=""
                    />
                    <label htmlFor={id}>{labelName}</label>
                    <span className="highlight" />
                    <span className="bar" />
                    {icon !== "" && <button type="button" onClick={e => onClick(e)} className={classIcon} />}
                </React.Fragment>
            )}

            {type === "radio" && (
                <React.Fragment>
                    <Field id={id} name={name} type={type} validate={validate} component="input" onChange={onChange} onBlur={onBlur} checked={checked} />
                    <label className={classLabel} htmlFor={id}>
                        {labelName}
                    </label>
                </React.Fragment>
            )}

            {type === "radio-custom" && (
                <label className="container-radio">
                    <div className="bank-wrapper">
                        {children}
                    </div>
                    <input id={id} type="radio" checked={checked === id ? true : false} name={name} onChange={()=>onChange(id)}/>
                    <span className="checkmark"></span>
                </label>
            )}

            {type === "checkbox" && (
                <label className={classLabel} htmlFor={id}>
                    <Field id={id} name={name} type={type} validate={validate} component="input" onChange={onChange} onBlur={onBlur} value={value} checked={checked} />
                    <span className="checkbox-material">
                        <span className="check" />
                    </span>
                    <div className="checkbox-text" dangerouslySetInnerHTML={{ __html: labelName }} />
                </label>
            )}

            {type === "select" && (
                <OutsideClick
                    onOutsideClick={e => {
                        return onClick(e);
                    }}
                    active={showItem}
                >
                    <div className={classSelectList}>
                        <div onClick={e => onClick(e)} className={classSelectedSelect}>
                            <span className="select-label">{labelName} : </span>
                            { displayIcon && (
                                <span className="select-icon"><img src={icon} alt={value} /></span>
                            )}
                            <span className="select-value">{value}</span>
                        </div>
                        <Field id={id} name={name} validate={validate} component="input" type="hidden" placeholder={placeholder} onBlur={onBlur} onChange={onChange} value={value} />
                        <div className={classSelectItem} role="document">
                            <ul className="select-list" role="listbox">
                                { map(options, option => (
                                    <li key={option.id} onClick={e => onItemClick(e, e.target.dataset.value)} role="button" className={ option.value === value ? 'item selected-item' : 'item'} data-value={option.value}>
                                        { listHasIcon && (
                                            <span className="icon-select"><img src={option.icon} alt={option.text} /></span>
                                        )}
                                        { option.text }
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                </OutsideClick>
            )}

            {type === "text-area" && (
                <React.Fragment>
                    <Field id={id} name={name} validate={validate} component="textarea" onChange={onChange} onBlur={onBlur} value={value} />
                    <label htmlFor={id}>{labelName}</label>
                </React.Fragment>
            )}

            {type === "range" && <Slider.Range allowCross={false} min={min} max={max} defaultValue={[min, max]} value={defaultValue !== null && defaultValue } onChange={v => onChange(v)} />}
            
            <ErrorMessage name={name}>{errorMessage => <div className="error-text">{errorMessage}</div>}</ErrorMessage>
        </div>
    );
}

TracField.defaultProps = {
    id: "example",
    name: "example",
    labelName: "Label",
    labelSize: "medium",
    icon: "",
    type: "text",
    placeholder: "Placeholder",
    autoComplete: "off",
    options: [
        // {
        //     id: "red",
        //     value: "red",
        //     text: "Red",
        //     icon: flagId,
        // },
        // {
        //     id: "green",
        //     value: "green",
        //     text: "Green",
        //     icon: flagEn,
        // }
        {
            id: "pertanyaan",
            value: "pertanyaan",
            text: "Pertanyaan",
            icon: ""
        },
        {
            id: "komplain",
            value: "komplain",
            text: "Komplain",
            icon: ""
        },
        {
            id: "pemesanan",
            value: "pemesanan",
            text: "Pemesanan",
            icon: ""
        }
    ],
    listHasIcon: false,
    displayIcon: false,
    checked: false,
    value: "",
    validate: null,
    onClick: v => console.log(v), // eslint-disable-line
    onChange: () => console.log("changed!"), // eslint-disable-line
    onBlur: () => console.log("blured!"),
    onItemClick: v => console.log(v),
    showItem: false
};

TracField.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    labelName: PropTypes.string,
    labelSize: PropTypes.oneOf(["small", "medium"]),
    type: PropTypes.oneOf(["text", "password", "radio", "checkbox", "select", "range"]),
    placeholder: PropTypes.string,
    icon: PropTypes.string,
    autoComplete: PropTypes.string,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
            text: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
            value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        })
    ),
    listHasIcon: PropTypes.bool,
    displayIcon: PropTypes.bool,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    validate: PropTypes.func,
    onClick: PropTypes.func,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onItemClick: PropTypes.func,
    showItem: PropTypes.bool,
    defaultValue: PropTypes.any,
    min: PropTypes.number,
    max: PropTypes.number
};

export default React.memo(TracField);