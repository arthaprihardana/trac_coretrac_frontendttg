import React from 'react';
import './Icon.scss';
import {iconList} from 'assets/images/icons/iconList';

const Icon = ({name}) => {
    return(
        <div className="icon-wrapper">
            <img src={iconList[name]} alt=""/>
        </div>
    )
}

export default Icon;