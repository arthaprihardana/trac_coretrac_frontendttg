import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

// style
import "./Text.scss";

const Text = (props) => {
    const {
        type,
        indent,
        bold,
        centered,
        disabled,
        children,
        className: customClass
    } = props;
    const className = cx("a-text", `${customClass || ""}`, {
        "indent-1": indent === 1,
        "indent-2": indent === 2,
        "indent-3": indent === 3,
        disabled,
        bold,
        centered
    });

    switch (type) {
        case "h1":
            return <h1 className={className}>{children}</h1>;
        case "h2":
            return <h2 className={className}>{children}</h2>;
        case "h3":
            return <h3 className={className}>{children}</h3>;
        case "h4":
            return <h4 className={className}>{children}</h4>;
        case "h5":
            return <h5 className={className}>{children}</h5>;
        case "h6":
            return <h6 className={className}>{children}</h6>;
        default:
            return <p className={className}>{children}</p>;
    }

};

Text.defaultProps = {
    children: "Lorem ipsum dolor sit amet..."
};

Text.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
        PropTypes.node
    ]),
    type: PropTypes.string,
    bold: PropTypes.bool,
    disabled: PropTypes.bool,
    centered: PropTypes.bool
};

export default React.memo(Text);