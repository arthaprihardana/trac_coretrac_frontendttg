import React from 'react';
import PropTypes from 'prop-types';

// image
import DummyImage from 'assets/images/dummy/login.jpg';

// style
import './Cover.scss';

const Cover = (props) => {
    const { image } = props;

    return <div className="a-cover" style={{ backgroundImage: `url(${image})` }} />;

};

Cover.defaultProps = {
    image: DummyImage,
};

Cover.propTypes = {
    image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

export default React.memo(Cover);
