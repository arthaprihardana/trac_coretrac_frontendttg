import React, {Fragment} from 'react';
import './Input.scss';
import PropTypes from 'prop-types';

const Input = (props) => {

    return (
        <Fragment>
            <label htmlFor="">{props.label}</label>
            <input className={`a-input ${props.className}`} type={props.type} placeholder={props.placeholder} />
        </Fragment>
    );

};

Input.propTypes = {
    type: PropTypes.string.isRequired,
    label: PropTypes.string,
    className: PropTypes.string,
    placeholder: PropTypes.string
}

export default React.memo(Input);