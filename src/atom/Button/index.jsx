/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-09 15:29:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 23:13:19
 */
import cx from "classnames";
import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import "./Button.scss";

const Button = (props) => {
    const { children, type, onClick, ...rest } = props;
    const {
        primary,
        secondary,
        outline,
        disabled,
        outlineDisabled,
        text,
        goto,
        left,
        center,
        right,
        clearDate
    } = rest;

    const className = cx("a-btn", {
        "btn-primary": primary,
        "btn-secondary": secondary,
        "btn-outline": outline,
        "btn-disabled": disabled,
        "btn-disabled btn-outline": outlineDisabled,
        "btn-text": text,
        "btn-clear-date": clearDate
    });

    let buttonElement = "";
    if (goto) {
        buttonElement = (
            <Link to={goto} className={className}>
                {children}
            </Link>
        );
    } else if (type === "submit") {
        buttonElement = (
            <button
                type="submit"
                onClick={e => onClick(e)}
                className={className}
                disabled={disabled || outlineDisabled}
            >
                {children}
            </button>
        );
    } else {
        buttonElement = (
            <button
                type="button"
                onClick={e => onClick(e)}
                className={className}
                disabled={disabled || outlineDisabled}
            >
                {children}
            </button>
        );
    }

    if (left || center || right) {
        const alignButton = cx("wrapper-button", {
            "align-left": left,
            "align-center": center,
            "align-right": right
        });
        buttonElement = <div className={alignButton}>{buttonElement}</div>;
    }

    return buttonElement;

};

Button.defaultProps = {
    type: "button",
    onClick: e => console.log("button element:", e.currentTarget),
};

Button.propTypes = {
    type: PropTypes.string,
    onClick: PropTypes.func,
};

export default React.memo(Button);