import React from "react";
import NumberFormat from "react-number-format";
import PropTypes from "prop-types";

const Rupiah = ({ value }) => {

    return (
        <NumberFormat
            thousandSeparator
            prefix="Rp "
            suffix=" "
            value={value}
            displayType="text"
        />
    );

};

Rupiah.propTypes = {
    value: PropTypes.number.isRequired
};

export default React.memo(Rupiah);