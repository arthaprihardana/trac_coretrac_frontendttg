import React, { Component } from "react";
import './Static.scss';
import { Link } from 'react-router-dom';

class Static extends Component {
    render() {
        const { title, children, bannerImg, subTitle, breadCrumb, contentBanner } = this.props;
        return (
            <div className="t-static">
                <div className="static-banner">
                    <img className="cover" src={bannerImg} alt="banner" />
                    <div className="container">
                        <div className="content-banner">
                            {
                                title && <p className="title-banner">{title}</p>
                            }
                            {
                                subTitle && <p className="sub-title-banner">{subTitle}</p>
                            }
                            {
                                contentBanner && contentBanner
                            }
                        </div>
                    </div>
                </div>
                {
                    breadCrumb && (
                        <div className="bread-crumb">
                            <div className="container">
                                {
                                    breadCrumb.map((item, i) => <Link key={i} to={item.link}>{item.label}</Link>)    
                                }
                            </div>
                        </div>
                    )
                }
                <div className="static-content">
                    <div className="container">{children}</div>
                </div>
            </div>
        );
    }
}

export default Static;
