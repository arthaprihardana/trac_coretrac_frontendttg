// core
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

// component
import { Header, Footer } from 'organisms';
import { BackgroundOverlay } from 'atom';

// style
import './Main.scss';

// class main
class Main extends PureComponent {
    state = {
        setShowOverlay: false,
        setEventOverlay: () => console.log('clicked'),
    };

    componentDidMount() {
        const { showOverlay, eventOverlay } = this.props;
        this.setState({ setShowOverlay: showOverlay });
        this.setState({ setEventOverlay: eventOverlay });
    }

    componentWillReceiveProps(newProps) {
        this.changeBackgroundOverlay(newProps.showOverlay, newProps.eventOverlay);
    }

    changeBackgroundOverlay = (status, eventFunction) => {
        this.setState({ setShowOverlay: status });
        this.setState({ setEventOverlay: eventFunction });
    };

    render() {
        const {
            state: { setShowOverlay, setEventOverlay },
            props: { solid, className, children, hideMenu, headerUserLogin, headerClose },
        } = this;

        return (
            <Fragment>
                <Header solid={ solid } showOverlay={ this.changeBackgroundOverlay } hideMenu={ hideMenu } headerUserLogin={ headerUserLogin } headerClose={ headerClose }/>
                <div className={`t-main ${className}`}>{children}</div>
                <Footer />
                <BackgroundOverlay show={ setShowOverlay } onClick={ setEventOverlay } />
            </Fragment>
        );
    }
}

// default props
Main.defaultProps = {
    solid: false,
    className: '',
    showOverlay: false,
    eventOverlay: () => console.log('clicked'),
};

// props types
Main.propTypes = {
    solid: PropTypes.bool,
    className: PropTypes.string,
    showOverlay: PropTypes.bool,
    eventOverlay: PropTypes.func,
    children: PropTypes.any,
};

export default Main;
