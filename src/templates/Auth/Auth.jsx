import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

// components
import { Text, Cover, Button } from "atom";
import { Main } from "templates";

// assets
import logo from "assets/images/logo/trac-logo-color.svg";

// style
import "./Auth.scss";

const screen = {
    large: 1220,
    medium: 980,
    small: 767,
    extraSmall: 480,
    extraExtraSmall: 320
};

class Auth extends PureComponent {
    componentDidMount() {
        const {
            windowSize: { width }
        } = this.props;
        const rootEl = document.getElementById("root");
        rootEl.classList.add("full-height");
        const headerEl = rootEl.querySelector("div.o-header");
        const footerEl = rootEl.querySelector("div.o-footer");
        footerEl.classList.add("hide-content");

        if (width <= screen.small) {
            headerEl.classList.remove("hide-content");
        } else {
            headerEl.classList.add("hide-content");
        }
    }

    componentDidUpdate() {
        const {
            windowSize: { width }
        } = this.props;
        const headerEl = document.querySelector("div.o-header");

        if (width <= screen.small) {
            headerEl.classList.remove("hide-content");
        } else {
            headerEl.classList.add("hide-content");
        }
    }

    componentWillUnmount() {
        const rootEl = document.getElementById("root");
        rootEl.classList.remove("full-height");
    }

    render() {
        const { left, right, authText, corporateLogin } = this.props;
        let className = 't-auth';
        if (corporateLogin) {
            className += ' is-corporate-login';
        }

        return (
            <Main solid className={ className }>
                <div className="left">{left}</div>
                <div className="right">
                    <Link to="/" className="logo">
                        <Cover image={logo}/>
                    </Link>
                    { right }
                    {
                        authText && (
                            <div className="social-auth">
                                <Text>{authText}</Text>
                                <div className="social-icon">
                                    <a
                                        className="facebook"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        href="https://developers.facebook.com/"
                                    >
                                        facebook
                                    </a>
                                    <a
                                        className="google"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        href="https://developers.google.com/"
                                    >
                                        google
                                    </a>
                                </div>
                            </div>
                        )
                    }
                    {
                        corporateLogin && (
                            <div className="corporate-rent-account">
                                <h4>Corporate Rental Account</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                                <Button type="button" outline link="#">
                                    Corporate Rental Login
                                </Button>
                            </div>
                        )
                    }
                </div>
            </Main>
        );
    }
}

Auth.propTypes = {
    windowSize: PropTypes.shape({
        width: PropTypes.number,
        height: PropTypes.number
    }).isRequired,
    left: PropTypes.node,
    right: PropTypes.node,
    authText: PropTypes.string,
    corporateLogin: PropTypes.bool
};

export default Auth;