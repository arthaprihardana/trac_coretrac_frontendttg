import React, { PureComponent } from "react";
import PropTypes from "prop-types";

// style
import "./Sidebar.scss";

class Sidebar extends PureComponent {

    // handle on click
    handleClick = () => {
        this.props.returnShowDetail(false);
    };

    render() {
        const { handleClick } = this;
        const { showDetail } = this.props;
        const { children } = this.props;

        let className = 'o-sidebar';
        if ( showDetail ) {
            className += ' show';
        }
        return(
            <div className={ className }>
                <div className="sidebar-mobile-header">
                    <h2 className="title">Order Review</h2>
                    <div className="close-sidebar" onClick={ handleClick }>Close</div>
                </div>
                <div className="sidebar-scrollover">
                    { children }
                </div>
            </div>
        );
    };
}

Sidebar.defaultProps = {
    children: '<div></div>',
    showDetail: false,
    returnShowDetail: () => console.log(),
};

Sidebar.propTypes = {
    children: PropTypes.any,
    showDetail: PropTypes.bool,
    returnShowDetail: PropTypes.func,
};

export default Sidebar;
