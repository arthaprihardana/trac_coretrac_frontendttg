// images
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
// import './Tabs.scss';

class Tabs extends PureComponent {

    componentDidMount() {
        window.addEventListener('resize', this.handleWindowResize);
    }

    render() {
        const { config } = this.props;

        return (
            <div className="tabs-list">
                <Slider {...config}>
                    <p className="tab-item active">Mc Donald's</p>
                    <p className="tab-item">Dapur Solo</p>
                    <p className="tab-item">KFC</p>
                    <p className="tab-item">Bakmi GM</p>
                    <p className="tab-item">Restaurant</p>
                </Slider>
            </div>
        );

    }
}

Tabs.defaultProps = {
    config: {
        dots: false,
        lazyLoad: false,
        infinite: false,
        autoplay: false,
        arrows: true,
        speed: 650,
        slidesToShow: 3,
        slidesToScroll: 1,
    },
};

Tabs.propTypes = {
    config: PropTypes.object,
};

export default Tabs;
