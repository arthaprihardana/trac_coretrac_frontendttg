import React, { PureComponent, Fragment } from 'react';
import { SidebarCarOrder, SidebarPaymentDetail } from 'molecules';
import { Sidebar, SidebarBookingDetail, SidebarNote, TotalPaymentMobile } from 'organisms';


class SideOrderDetail extends PureComponent {
    state = {
        selectedCar: [],
        bookingDetail: {},
        showDetail: false,
    };

    componentDidMount(){
        this.setState({
            selectedCar: this.props.selectedCar,
            bookingDetail: this.props.bookingDetail,
        })
    }

    handleShowPaymentDetail = (status) => {
        this.setState({ showDetail: status});
    }

    render() {

        const { handleShowPaymentDetail } = this;
        const { selectedCar, bookingDetail, showDetail } = this.state;

        return (
            <Fragment>
                <Sidebar showDetail={ showDetail } returnShowDetail = { handleShowPaymentDetail }>
                    {
                        selectedCar.length > 0 ? <SidebarCarOrder data={selectedCar} /> : null
                    }

                    {
                        bookingDetail.packages ? <SidebarBookingDetail data={bookingDetail}/> : null
                    }

                    <SidebarNote />

                    {
                        selectedCar.length > 0 ? <SidebarPaymentDetail data={selectedCar} /> : null
                    }
                </Sidebar>

                {/* TotalPaymentMobile */}
                <TotalPaymentMobile showDetail={ showDetail } returnShowDetail = { handleShowPaymentDetail }/>
            </Fragment>
        )
    }
}

export default SideOrderDetail;