/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-09 01:04:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 23:46:53
 */
import React, { PureComponent, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { SidebarCarOrder, SidebarPaymentDetail } from 'molecules';
import { Sidebar, SidebarBookingDetail, SidebarNote, TotalPaymentMobile } from 'organisms';
import { localStorageDecrypt } from "helpers";

class SideOrderDetail extends PureComponent {
    state = {
        selectedCar: [],
        bookingDetail: {},
        showDetail: false,
    };

    componentDidMount(){
        let selectedCar = JSON.parse(localStorage.getItem('selectedCar'));
        let CarRentalFormInput = localStorageDecrypt('_fi', 'object');
        if(selectedCar !== null && CarRentalFormInput !== null ) {
            this.setState({
                selectedCar: selectedCar,
                bookingDetail: CarRentalFormInput,
            })
        } else {
            this.props.history.replace('/');
        }
    }

    handleShowPaymentDetail = (status) => {
        this.setState({ showDetail: status});
    }

    render() {

        const { handleShowPaymentDetail } = this;
        const { selectedCar, bookingDetail, showDetail } = this.state;
        const { type } = this.props;
        
        return (
            <Fragment>
                <Sidebar showDetail={ showDetail } returnShowDetail = { handleShowPaymentDetail }>
                    {
                        selectedCar.length > 0 && <SidebarCarOrder data={selectedCar} type={type} />
                    }

                    {
                        Object.keys(bookingDetail).length > 0 && <SidebarBookingDetail data={ bookingDetail } type={type} />
                    }

                    <SidebarNote hasContent />

                    {
                        selectedCar.length > 0 && <SidebarPaymentDetail data={selectedCar} type={type} />
                    }
                </Sidebar>

                {/* TotalPaymentMobile */}
                <TotalPaymentMobile showDetail={ showDetail } returnShowDetail = { handleShowPaymentDetail }/>
            </Fragment>
        )
    }
}

export default withRouter(SideOrderDetail);