import React from 'react';

const BusExtraItem = ({product}) => {
    return (
        <div className="content-item">
            <div className="product">
                <div className="img-product">
                    <img src={product.img} alt="dummy-product" />
                </div>
                <div className="desc-product">
                    <p className="title-product">{product.name}</p>
                    <p className="sub-product">{product.detail}</p>
                </div>
            </div>
            <div className="amount">
                <div className="price-wrapper">
                    <p className="price">Rp {product.price}<span>/pcs</span></p>
                </div>
                <div className="quantity-wrapper">
                    <p className="label">Total</p>
                    <input type="number" className="sum" />
                </div>
            </div>
        </div>
    )
}

export default BusExtraItem;