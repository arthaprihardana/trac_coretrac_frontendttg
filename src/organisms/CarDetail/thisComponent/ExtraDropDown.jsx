import React, { Component } from 'react';
import cx from 'classnames';

class ExtraDropDown extends Component {
    state = {
        show: false
    }

    toggleShow = () => {
        this.setState({
            show: !this.state.show
        }, () => {
            this.props.onValueChange(this.state.show)
        })
    }

    render() {
        const {
            state: {
                show
            },
            toggleShow
        } = this;

        const classDropdown = cx('extra-dropdown', {
            'active': show
        })

        return (
            <div className={classDropdown} onClick={toggleShow}>
                <div className="icon"></div>
            </div>
        )
    }
}

export default ExtraDropDown;