/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-28 00:20:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 00:52:49
 */
import React, { Fragment, memo, Component } from 'react';
import {Rupiah} from 'atom';
import { connect } from "react-redux";
import _ from "lodash";
import lang from '../../../assets/data-master/language'
class ActiveCarDetail extends Component {

    state = {
        validPromo: false,
        activeCar: {}
    }

    componentDidMount = () => {
        // this.setState({
        //     activeCar: this.props.activeCar
        // })
        if(this.props.validPromo) {
            this.setState({
                validPromo: this.props.validPromo
            })
        }
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(this.props.validPromo !== prevProps.validPromo) {
            if(this.props.validPromo) {
                let selectedCar = JSON.parse(localStorage.getItem('selectedCar'));
                let activeCar = this.props.activeCar;
                let f = _.filter(selectedCar, { selectedId: activeCar.selectedId })[0];
                activeCar.rentInfo.priceAfterDiscount = f.rentInfo.priceAfterDiscount;
            }
            this.setState({
                validPromo: this.props.validPromo
            })
        }
    }
    
    render() {
        const { type, indexActive, activeCar } = this.props;
        const { validPromo  } = this.state;
        let CarRentalFormDisplay;
        switch (type) {
            case "airport-transfer":
                CarRentalFormDisplay = JSON.parse(localStorage.getItem('AirportTransferFormDisplay'));
                break;
            case "bus-rental":
                CarRentalFormDisplay = JSON.parse(localStorage.getItem('BusRentalFormDisplay'));
                break
            default:
                CarRentalFormDisplay = JSON.parse(localStorage.getItem('CarRentalFormDisplay'));
                break;
        }
        return (
            <Fragment>
                <div className="top-section">
                    <div className="description">
                        {/* Special Deal */}
                        {/*<div className="car-special-deal">Special Deal!</div>*/}
                        <h4 className="car-name"> { type !== 'bus-rental' ? lang.car[lang.default] : lang.bus[lang.default]}  #{indexActive + 1} - {activeCar.vehicleAlias !== null ? activeCar.vehicleAlias : activeCar.vehicleTypeDesc}</h4>
                        <div className="car-service-description">
                            { type !== 'bus-rental' ? `Rental Car - ${CarRentalFormDisplay.typeService}` : 'Bus Rental' }
                        </div>
                        <div className="car-spesification">
                            <Fragment>
                                <div className="item passenger">{activeCar.totalSeat}</div>
                                <div className="item baggage">{activeCar.totalLugagge}</div>
                                { type !== 'bus-rental' && <div className="item transmission">{activeCar.isTransmissionManual === 1 ? "MT" : "AT"}</div> }
                                {activeCar.isAC === 1 && <div className="item air-conditioner">Air Conditioner</div>}
                                {activeCar.isAirbag === 1 && <div className="item air-bag">Air Bag</div>}
                                {activeCar.isKaraoke === 1 && <div className="item karaoke"></div>}
                                {activeCar.isWiFi === 1 && <div className="item wifi"></div>}
                                {activeCar.isCharger === 1 && <div className="item charger"></div>}
                            </Fragment>
                            {/* <div className="item passenger">{activeCar.totalSeat}</div>
                            {
                                type !== 'bus-rental' && (
                                    <Fragment>
                                        <div className="item baggage">{activeCar.totalLugagge}</div>
                                        <div className="item transmission">{activeCar.isTransmissionManual === 1 ? "MT" : "AT"}</div>
                                        {activeCar.isAC === 1 && <div className="item air-conditioner">Air Conditioner</div>}
                                        {activeCar.isAirbag === 1 && <div className="item air-bag">Air Bag</div>}
                                    </Fragment>
                                )
                            }
                            {activeCar.isKaraoke === 1 && <div className="item karaoke"></div>}
                            {activeCar.isWiFi === 1 && <div className="item wifi"></div>}
                            {activeCar.isCharger === 1 && <div className="item charger"></div>} */}
                        </div>
                    </div>
                </div>
                <div className="car-image">
                    <img src={activeCar.vehicleImage} alt={activeCar.vehicleAlias !== null ? activeCar.vehicleAlias : activeCar.vehicleTypeDesc} />
                </div>
                <div className="car-price">
                    { validPromo && activeCar.rentInfo.priceAfterDiscount > 0 && <span className="price-before"><Rupiah value={activeCar.rentInfo.basePrice} /></span> }
                    <Rupiah value={ validPromo && activeCar.rentInfo.priceAfterDiscount > 0 ? parseInt(activeCar.rentInfo.priceAfterDiscount) : parseInt(activeCar.rentInfo.basePrice)} />
                    <span className="package"> / {lang.day[lang.default]}</span>
                </div>
            </Fragment>
        )
    }

}

const mapStateToProps = ({ discount }) => {
    const { validPromo } = discount;
    return { validPromo };
}

export default connect(mapStateToProps, {})(ActiveCarDetail);

// const ActiveCarDetail = ({ activeCar, type, indexActive, validPromo }) => {
//     let CarRentalFormDisplay;
//     switch (type) {
//         case "airport-transfer":
//             CarRentalFormDisplay = JSON.parse(localStorage.getItem('AirportTransferFormDisplay'));
//             break;
//         case "bus-rental":
//             CarRentalFormDisplay = JSON.parse(localStorage.getItem('BusRentalFormDisplay'));
//             break
//         default:
//             CarRentalFormDisplay = JSON.parse(localStorage.getItem('CarRentalFormDisplay'));
//             break;
//     }
//     return (
//         <Fragment>
//             <div className="top-section">
//                 <div className="description">
//                     {/* Special Deal */}
//                     <div className="car-special-deal">Special Deal!</div>
//                     <h4 className="car-name">Car #{indexActive + 1} - {activeCar.vehicleAlias !== null ? activeCar.vehicleAlias : activeCar.vehicleTypeDesc}</h4>
//                     <div className="car-service-description">
// 			            { type !== 'bus-rental' ? `Rental Car - ${CarRentalFormDisplay.typeService}` : 'Bus Rental' }
//                     </div>
//                     <div className="car-spesification">
//                         <Fragment>
//                             <div className="item passenger">{activeCar.totalSeat}</div>
//                             <div className="item baggage">{activeCar.totalLugagge}</div>
//                             { type !== 'bus-rental' && <div className="item transmission">{activeCar.isTransmissionManual === 1 ? "MT" : "AT"}</div> }
//                             {activeCar.isAC === 1 && <div className="item air-conditioner">Air Conditioner</div>}
//                             {activeCar.isAirbag === 1 && <div className="item air-bag">Air Bag</div>}
//                             {activeCar.isKaraoke === 1 && <div className="item karaoke"></div>}
//                             {activeCar.isWiFi === 1 && <div className="item wifi"></div>}
//                             {activeCar.isCharger === 1 && <div className="item charger"></div>}
//                         </Fragment>
//                         {/* <div className="item passenger">{activeCar.totalSeat}</div>
// 			            {
//                             type !== 'bus-rental' && (
//                                 <Fragment>
//                                     <div className="item baggage">{activeCar.totalLugagge}</div>
//                                     <div className="item transmission">{activeCar.isTransmissionManual === 1 ? "MT" : "AT"}</div>
//                                     {activeCar.isAC === 1 && <div className="item air-conditioner">Air Conditioner</div>}
//                                     {activeCar.isAirbag === 1 && <div className="item air-bag">Air Bag</div>}
//                                 </Fragment>
//                             )
//                         }
//                         {activeCar.isKaraoke === 1 && <div className="item karaoke"></div>}
//                         {activeCar.isWiFi === 1 && <div className="item wifi"></div>}
//                         {activeCar.isCharger === 1 && <div className="item charger"></div>} */}
//                     </div>
//                 </div>
//             </div>
//             <div className="car-image">
//                 <img src={activeCar.vehicleImage} alt={activeCar.vehicleAlias !== null ? activeCar.vehicleAlias : activeCar.vehicleTypeDesc} />
//             </div>
//             <div className="car-price">
//                 { validPromo && activeCar.rentInfo.priceAfterDiscount > 0 && <span className="price-before"><Rupiah value={activeCar.rentInfo.basePrice} /></span> }
//                 <Rupiah value={ validPromo && activeCar.rentInfo.priceAfterDiscount > 0 ? parseInt(activeCar.rentInfo.priceAfterDiscount) : parseInt(activeCar.rentInfo.basePrice)} />
//                 <span className="package"> / Day</span>
//             </div>
//         </Fragment>
//     )
// }

// export default memo(ActiveCarDetail);
