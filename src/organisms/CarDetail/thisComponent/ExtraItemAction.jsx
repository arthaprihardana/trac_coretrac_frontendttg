/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-08 10:58:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 20:43:06
 */
import React from 'react';
import { Counter, Toggle } from "organisms";
import ExtraDropDown from './ExtraDropDown';

const ExtraItemAction = ({ dataExtra, extraItemsChane, type, maxAvailable, selectedCar, activeCar, stockType }) => {
    let action = null;
    // switch (dataExtra.type) {
    switch (type) {
        case 'counter':
            action = (
                <Counter
                    id={dataExtra.id}
                    selectedCar={selectedCar}
                    activeCar={activeCar}
                    isExtras={true}
                    maxAvailable={maxAvailable}
                    changeAmount={dataExtra.amounts}
                    defaultValue={dataExtra.amounts}
                    onValueChange={value => extraItemsChane(value, dataExtra)}
                    stockType={stockType}
                />
            )
            break;
        case 'boolean':
            action = (
                <Toggle
                    id={dataExtra.id.toString()}
                    defaultValue={dataExtra.amounts > 0 ? true : false}
                    onValueChange={value => extraItemsChane(value ? 1 : 0, dataExtra)}
                />
            )
            break;
        case 'dropdown':
            action = (
                <ExtraDropDown onValueChange={value => extraItemsChane(value ? 'show' : 'hide')} />
            )
            break;
        default:
            action = null
    }
    return (
        <div className={`action-wrapper ${dataExtra.type}`}>
            {action}
        </div>
    )
}

export default ExtraItemAction;