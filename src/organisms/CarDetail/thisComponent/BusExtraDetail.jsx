import React, {Component} from 'react';
import cx from 'classnames';
import burger from 'assets/images/dummy/burger.png';
import cocacola from 'assets/images/dummy/coca-cola.png';
import BusExtraItem from './BusExtraItem';

class BusExtraDetail extends Component {
    state = {
        tabActive: {},
        tabList: [
            {
                id: 1,
                name: 'Mc Donald\'s'
            },
            {
                id: 2,
                name: 'Dapur Solo'
            },
            {
                id: 3,
                name: 'KFC'
            },
            {
                id: 4,
                name: 'Bakmi GM'
            },
            {
                id: 5,
                name: 'Restaurant'
            }
        ],
        products: [
            {
                id: 1,
                name: 'Cheese Burger alacarte',
                detail: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                img: burger,
                price: 25000
            },
            {
                id: 2,
                name: 'Coca Cola',
                detail: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                img: cocacola,
                price: 25000
            },
        ]
    }

    componentDidMount(){
        const [tab1] = this.state.tabList;
        this.setState({
            tabActive: tab1
        })
    }

    selectTab = (obj) => {
        this.setState({tabActive: obj})
    }

    render(){
        const {
            state: {
                tabList,
                tabActive,
                products
            },
            selectTab
        } = this;

        return(
            <div className="bus-extraitems-detail">
                <div className="tabs-list">
                    {
                        tabList.map(tab => {
                            const classTabItem = cx('tab-item', {
                                'active': tabActive === tab
                            })
                            return <div className={classTabItem} key={tab.id} onClick={() => selectTab(tab)}>{tab.name}</div>
                        })
                    }
                </div>
                <div className="tab-content">
                    {
                        products.map(product => {
                            return <BusExtraItem product={product} key={product.id} />
                        })
                    }
                </div>
            </div>
        )
    }
}

export default BusExtraDetail;