/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-08 10:55:12 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 20:40:43
 */
import React, { Component } from 'react';
import { Rupiah } from "atom";
import ExtraItemAction from './ExtraItemAction';
import { connect } from "react-redux";

class ExtraItem extends Component {
    iconList = name => {
        switch (name) {
            case "Add Hour":
                return "add-hour";
            case "Out Of Town":
                return "out-of-town";
            case "Overnight":
                return "overnight";
            case "Fuel Plans":
                return "fuel-plans";
            case "Baby Car Seat":
                return "baby-car-seat";
            case "Wifi":
                return "wifi";
            case "Wedding Decoration":
                return "wedding-decoration";
            case "Food And Beverage":
                return "food";
            case "Travel Neccessities":
                return "travel";
            case "Our Partner":
                return "partner";
            default:
                return "add-hour";
        }
    };

    extraItemsChane = (value, extra) => {
        this.props.onExtraChange(value, extra)
    }

    render() {
        const {
            props: {
                dataExtra,
                maxAvailable,
                selectedCar,
                activeCar,
                stockextras
            }
        } = this;
        
        return (
            <div className="extras-card" key={dataExtra.id}>
                <div className="box">
                    <div
                        className={`icon ${this.iconList(dataExtra.name)}`}
                    />
                    <div className="title">{dataExtra.name}</div>
                    {
                        dataExtra.price && (
                            <div className="caption">
                                <Rupiah value={dataExtra.price} /> {dataExtra.ValueType === "counter" && `/${dataExtra.pricePer}`}
                            </div>
                        )
                    }
                    { stockextras !== null && dataExtra.ExtrasId === stockextras.ExtrasId && <p className="info-extras">Stok telah habis</p> }
                    <ExtraItemAction dataExtra={dataExtra} extraItemsChane={this.extraItemsChane} type={dataExtra.ValueType} stockType={dataExtra.StockType} maxAvailable={maxAvailable} selectedCar={selectedCar} activeCar={activeCar} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({ carrental }) => {
    const { stockextras } = carrental;
    return { stockextras };
}

export default connect(mapStateToProps, {})(ExtraItem);