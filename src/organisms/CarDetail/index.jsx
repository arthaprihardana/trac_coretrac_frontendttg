/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-27 23:58:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 01:45:06
 */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

// Component
import { Button, Rupiah } from "atom";
import ActiveCarDetail from "./thisComponent/ActiveCarDetail";
import ExtraItem from "./thisComponent/ExtraItem";
// import BusExtraDetail from "./thisComponent/BusExtraDetail";

// style
import "./CarDetail.scss";

// helper
import lang from '../../assets/data-master/language'

// actions
import { getExtrasCarRental, validateTotalCartUnitVehicle } from "actions";

class CarDetail extends Component {
    state = {
        selectedCar: [],
        activeCar: {},
        indexActive: 0,
        listExtras: [],
        maxAvailable: 0
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        if(nextProps.data.length > 0) return true;
        return false;
    }

    componentDidMount = () => {
        if(this.props.data.length !== this.state.selectedCar.length) {
            let index =
                this.state.indexActive !== 0
                    ? this.state.indexActive - 1
                    : 0;
            this.setState({
                selectedCar: this.props.data,
                activeCar: this.props.data[index],
                indexActive: index
            });
        }
    }
    

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.data !== prevProps.data) {
            return { data: this.props.data }
        }
        return null;
    }
    
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.data !== undefined) {
                if(snapshot.data.length !== this.state.selectedCar.length) {
                    let index =
                        this.state.indexActive !== 0
                            ? this.state.indexActive - 1
                            : 0;
                    this.setState({
                        selectedCar: snapshot.data,
                        activeCar: snapshot.data[index],
                        indexActive: index
                    });
                }
            }
        }
    }

    changeActive = type => {
        const { selectedCar, indexActive } = this.state;
        let index = indexActive - 1;
        if (type === "next") index = indexActive + 1;
        if(typeof type == "number") index = type;
        this.setState({
            activeCar: selectedCar[index],
            indexActive: index
        });
    };

    prevAction = () => {
        if (this.state.indexActive !== 0) {
            this.changeActive("prev");
        }
    };

    nexAction = () => {
        if (this.state.indexActive !== this.state.selectedCar.length - 1) {
            this.changeActive("next");
        }
    };

    iconList = name => {
        switch (name) {
            case "Add Hour":
                return "add-hour";
            case "Out Of Town":
                return "out-of-town";
            case "Overnight":
                return "overnight";
            case "Fuel Plans":
                return "fuel-plans";
            case "Baby Car Seat":
                return "baby-car-seat";
            case "Wifi":
                return "wifi";
            case "Wedding Decoration":
                return "wedding-decoration";
            default:
                return "";
        }
    };

    extraItemsChane = (value, extra) => {
        const { selectedCar } = this.state;
        let activeCar = { ...this.state.activeCar };
        let extras = activeCar.extraItems;
        let indexExtra = extras.findIndex(e => e.id === extra.id);
            
        extras[indexExtra].amounts = value;
        extras[indexExtra].total = extras[indexExtra].amounts * extras[indexExtra].price;
        
        let updatedCarList = [...selectedCar];
        let indexUpdated = updatedCarList.findIndex(
            car => car.selectedId === activeCar.selectedId
        );
        updatedCarList[indexUpdated] = activeCar;
        
        this.setState({
            selectedCar: updatedCarList
        }, () => this.props.onExtrasChange(updatedCarList))
    };

    changeTotalPayment = (data) => {
        let totalPayment = 0;
        data.map(car => {
            totalPayment = totalPayment + car.rentInfo.total;
            // car.extraItems.map(extra => {
            //     return totalPayment = totalPayment + extra.total
            // })
            return totalPayment;
        })
        return totalPayment;
    }

    removeHandle = id => {
        let updatedCarList = [...this.state.selectedCar];
        let filteredCarList = updatedCarList.filter(car => car.selectedId !== id);
        this.props.onRemove(filteredCarList);
    };

    render() {
        const {
            props: {
                onSubmit,
                type,
                validPromo
            },
            state: {
                selectedCar,
                activeCar,
                indexActive
            },
            extraItemsChane
        } = this;
        let activeCarDetail = null;
        let extraItems = [];
        let selectedCarsItems = [];

        if(selectedCar.length > 0) {
            activeCarDetail = <ActiveCarDetail activeCar={activeCar} type={type} indexActive={indexActive} validPromo={validPromo} />;
            selectedCarsItems = selectedCar.map((car, key) => {
                return (
                    <div
                        onClick={() => this.changeActive(key) }
                        style={{ cursor: 'pointer' }}
                        className={`item ${activeCar.selectedId === car.selectedId ? "active" : null}`}
                        key={car.selectedId}
                    >
                        <div className="image">
                            <img src={car.vehicleImage} alt={car.vehicleTypeDesc} />
                            <span
                                className="cancel"
                                onClick={() => this.removeHandle(car.selectedId)}
                            />
                        </div>
                    </div>
                );
            });
            extraItems = activeCar.extraItems.map((extra, key) => {
                return (
                    <ExtraItem key={key} dataExtra={extra} onExtraChange={extraItemsChane} maxAvailable={parseInt(extra.Availability)} selectedCar={selectedCar} activeCar={activeCar} />
                );
            });
        }

        return (
            <div className="o-car-detail">
                {/* car detail header */}
                <div className="car-detail-header">
                    {/* Navigation */}
                    <div
                        className="car-navigation prev-car"
                        onClick={this.prevAction}
                    >
                        {lang.previousCar[lang.default]}
                    </div>
                    <div
                        className="car-navigation next-car"
                        onClick={this.nexAction}
                    >
                        {lang.nextCar[lang.default]}
                    </div>
                    {activeCarDetail}
                </div>
                {/* car detail extras */}
                { extraItems.length > 0 &&
                <div className="car-detail-extras" style={{position:'relative'}}>
                    <h4 className="title">{lang.wouldYouExtras[lang.default]}</h4>
                    { validPromo && <div className="extras-disable"> </div> }
                    <div className="m-extras">{extraItems}</div>
                </div> }
                {/* car selected */}
                <div className="car-selected">
                    <h4 className="title">
                        {type === "bus-rental" ? lang.selectedBus[lang.default] : lang.selectedCars[lang.default]}
                    </h4>
                    <div className="car-selected-lists">
                        {selectedCarsItems}
                    </div>
                    <div className="summary-payment-mobile">
                        <div className="summary-total">
                            <div className="total-payment-label">
                                {lang.totalPayment[lang.default]}
                            </div>
                            <div className="total-payment-ammount">
                                <Rupiah value={this.changeTotalPayment(selectedCar)} /> <span className="price-per">/ 3 {lang.days[lang.default]}</span>
                            </div>
                        </div>
                        <div className="summary-button" onClick={onSubmit} >
                            <Button primary >{lang.book[lang.default]} {selectedCarsItems.length} {selectedCarsItems.length <= 1 ? lang.car[lang.default] : lang.cars[lang.default]}</Button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

CarDetail.defaultProps = {
    data: []
};

CarDetail.propTypes = {
    data: PropTypes.array
};

const mapStateToProps = ({ carrental, discount }) => {
    const { validPromo } = discount;
    const {loading, extrascarrentalsuccess, extrascarrentalfailure} = carrental;
    return {loading, extrascarrentalsuccess, extrascarrentalfailure, validPromo};
}

export default connect(mapStateToProps, {
    getExtrasCarRental,
    validateTotalCartUnitVehicle
})(CarDetail);