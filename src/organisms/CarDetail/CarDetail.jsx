import React, { PureComponent } from "react";
import PropTypes from "prop-types";

// Component
import { Button, Rupiah } from "atom";
import { Counter, Toggle } from "organisms";

// style
import "./CarDetail.scss";

// Local component
import ActiveCarDetail from "./thisComponent/ActiveCarDetail";

class CarDetail extends PureComponent {
    state = {
        selectedCar: [],
        activeCar: {},
        indexActive: 0
    };

    componentDidMount() {
        if (this.props.data) {
            this.setState({
                selectedCar: this.props.data,
                activeCar: this.props.data[this.state.indexActive]
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data !== undefined) {
            if (nextProps.data.length !== this.state.selectedCar.length) {
                let index =
                    this.state.indexActive !== 0
                        ? this.state.indexActive - 1
                        : 0;
                this.setState({
                    selectedCar: nextProps.data,
                    activeCar: nextProps.data[index],
                    indexActive: index
                });
            }
        }
    }

    changeActive = type => {
        const { selectedCar, indexActive } = this.state;
        let index = indexActive - 1;
        if (type === "next") index = indexActive + 1;
        this.setState({
            activeCar: selectedCar[index],
            indexActive: index
        });
    };

    prevAction = () => {
        if (this.state.indexActive !== 0) {
            this.changeActive("prev");
        }
    };

    nexAction = () => {
        if (this.state.indexActive !== this.state.selectedCar.length - 1) {
            this.changeActive("next");
        }
    };

    iconList = name => {
        switch (name) {
            case "Add Hour":
                return "add-hour";
            case "Out Of Town":
                return "out-of-town";
            case "Overnight":
                return "overnight";
            case "Fuel Plans":
                return "fuel-plans";
            case "Baby Car Seat":
                return "baby-car-seat";
            case "Wifi":
                return "wifi";
            case "Wedding Decoration":
                return "wedding-decoration";
            default:
                return "";
        }
    };

    extraItemsChane = (value, extra) => {
        let activeCar = { ...this.state.activeCar };
        let extras = activeCar.extraItems;
        let indexExtra = extras.findIndex(e => e.id === extra.id);
        extras[indexExtra].amounts = value;
        extras[indexExtra].total =
            extras[indexExtra].amounts * extras[indexExtra].price;

        let updatedCarList = [...this.state.selectedCar];
        let indexUpdated = updatedCarList.findIndex(
            car => car.id === activeCar.id
        );
        updatedCarList[indexUpdated] = activeCar;
        this.setState(
            {
                selectedCar: updatedCarList
            },
            () => {
                this.props.onExtrasChange(updatedCarList);
            }
        );
    };

    changeTotalPayment = (data) => {
        let totalPayment = 0;
        data.map(car => {
            totalPayment = totalPayment + car.rentInfo.total;
            car.extraItems.map(extra => {
                return totalPayment = totalPayment + extra.total
            })
            return totalPayment;
        })
        return totalPayment;
    }

    removeHandle = id => {
        let updatedCarList = [...this.state.selectedCar];
        let filteredCarList = updatedCarList.filter(car => car.id !== id);
        this.props.onRemove(filteredCarList);
    };

    render() {
        const { selectedCar, activeCar } = this.state;

        let activeCarDetail = null;
        let extraItems = [];
        let selectedCarsItems = [];

        if (selectedCar.length > 0) {
            activeCarDetail = <ActiveCarDetail activeCar={activeCar} />;
            selectedCarsItems = selectedCar.map(car => {
                return (
                    <div
                        className={`item ${
                            activeCar.id === car.id ? "active" : null
                        }`}
                        key={car.id}
                    >
                        <div className="image">
                            <img src={car.img} alt={car.name} />
                            <span
                                className="cancel"
                                onClick={() => this.removeHandle(car.id)}
                            />
                        </div>
                    </div>
                );
            });
            extraItems = activeCar.extraItems.map(extra => {
                return (
                    <div className="extras-card" key={extra.id}>
                        <div className="box">
                            <div
                                className={`icon ${this.iconList(extra.name)}`}
                            />
                            <div className="title">{extra.name}</div>
                            <div className="caption">
                                <Rupiah value={extra.price} />/{extra.pricePer}
                            </div>
                            {extra.type === "counter" ? (
                                <div className="action-wrapper counter">
                                    <Counter
                                        defaultValue={extra.amounts}
                                        onValueChange={value =>
                                            this.extraItemsChane(value, extra)
                                        }
                                    />
                                </div>
                            ) : (
                                <div className="action-wrapper toggle">
                                    <Toggle
                                        id={extra.id}
                                        defaultValue={
                                            extra.amounts > 0 ? true : false
                                        }
                                        onValueChange={value =>
                                            this.extraItemsChane(
                                                value ? 1 : 0,
                                                extra
                                            )
                                        }
                                    />
                                </div>
                            )}
                        </div>
                    </div>
                );
            });
        }

        return (
            <div className="o-car-detail">
                {/* car detail header */}
                <div className="car-detail-header">
                    {/* Navigation */}
                    <div
                        className="car-navigation prev-car"
                        onClick={this.prevAction}
                    >
                        Previous Car
                    </div>
                    <div
                        className="car-navigation next-car"
                        onClick={this.nexAction}
                    >
                        Next Car
                    </div>
                    {activeCarDetail}
                </div>
                {/* car detail extras */}
                <div className="car-detail-extras">
                    <h4 className="title">Would you like any extra items?</h4>
                    <div className="m-extras">{extraItems}</div>
                </div>
                {/* car selected */}
                <div className="car-selected">
                    <h4 className="title">Selected Cars</h4>
                    <div className="car-selected-lists">
                        {selectedCarsItems}
                    </div>
                    <div className="summary-payment-mobile">
                        <div className="summary-total">
                            <div className="total-payment-label">
                                Total Payment
                            </div>
                            <div className="total-payment-ammount">
                                <Rupiah value={this.changeTotalPayment(selectedCar)} /> <span className="price-per">/ 3 days</span>
                            </div>
                        </div>
                        <div className="summary-button">
                            <Button primary>Book 3 Cars</Button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

CarDetail.defaultProps = {
    data: []
};

CarDetail.propTypes = {
    data: PropTypes.array
};

export default CarDetail;