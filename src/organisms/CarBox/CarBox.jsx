import React, { Component } from "react";
import { Link } from "react-router-dom";
import cx from "classnames";

// components
import { Button, Rupiah } from "atom";
import { Counter } from "organisms";

// styles
import "./CarBox.scss";

class CarBox extends Component {
    state = {
        selected: null,
        showDetail: false
    }

    onChange(value) {
        let newObj = {...this.props.data};

        newObj.rentInfo.amounts = value;
        if(value === newObj.rentInfo.spesialAmounts) {
            newObj.rentInfo.total = newObj.rentInfo.spesialPrice;
        }else {
            newObj.rentInfo.total = value * newObj.rentInfo.price;
        }
        this.props.onChange(newObj);
    }

    notifyMe() {
        console.log('notify me clicked')
    }

    handleShowDetail = () => {
        this.setState({
            showDetail: !this.state.showDetail
        });
    }

    shouldComponentUpdate(nextProps, nextState){
        if(nextProps.data.rentInfo.amounts !== this.props.data.rentInfo.amounts) return true;
        if(nextState !== this.state) return true;
        return false;
    }

    render() {
        const { showDetail } = this.state;
        const { data } = this.props;
        const classDetail = cx("detail", { show: showDetail });

        return (
            <div className="o-car-box">
                <div className="car-box-wrapper">
                    <div className="box-image">
                        {data.rentInfo.priceBeforeDiscount > data.rentInfo.price && <div className="special-deal">Special Deal!</div>}
                        <img src={data.img} alt={data.name} />
                        {data.rentInfo.availability <= 0 && <div className="car-status red">CAR ON BOOKING</div>}
                        {data.rentInfo.availability === 1 && <div className="car-status blue">HURRY! 1 CAR LEFT</div>}
                    </div>
                    <div className="box-description">
                        <div className="description-item">
                            <div className="car-name">
                                <h4>{data.name}</h4>
                            </div>
                            <div className="car-price">
                                <div className="price-per-package">
                                    {data.rentInfo.priceBeforeDiscount !== data.rentInfo.price && (
                                        <React.Fragment>
                                            <div className="before-discount">
                                                <span className="price-before">
                                                    <Rupiah value={data.rentInfo.priceBeforeDiscount} />
                                                </span>
                                                <span className="package-label"> / {data.rentInfo.pricePer}</span>
                                            </div>
                                        </React.Fragment>
                                    )}
                                    <Rupiah value={data.rentInfo.price} />
                                    <span className="package-label"> / {data.rentInfo.pricePer}</span>
                                </div>
                                <div className="price-per-3days">
                                    <Rupiah value={data.rentInfo.spesialPrice} />
                                    <span className="package-label">{`/ ${data.rentInfo.spesialAmounts} days`}</span>
                                </div>
                                <div className="tax-note">{data.rentInfo.notes}</div>
                            </div>
                        </div>
                        <div className="description-item car-specification">
                            <div className="car-specification-list">
                                <div className="item passenger">{data.carInfo.passanger}</div>
                                <div className="item baggage">{data.carInfo.suitcase}</div>
                                <div className="item transmission">{data.carInfo.transmission}</div>
                                {data.carInfo.protection && <div className="item air-bag"></div>}
                                {data.carInfo.ac && <div className="item air-conditioner"></div>}
                            </div>
                        </div>
                        <div className="description-item car-description-detail">
                            <div className="description-detail">
                                <Button text left onClick={this.handleShowDetail}>
                                    {showDetail ? "Hide Detail" : "More Info"}
                                </Button>
                                <div className={classDetail}>
                                    <div className="detail-content">
                                        <h6 className="detail-content-title">Average Vehicle Age: {data.carInfo.averageVehicleAgeNotes}</h6>
                                        <h6 className="detail-content-title">Price Includes</h6>
                                        <div className="list-item">
                                            <ul>
                                                {data.carInfo.priceIncludes.map((feature, index) => {
                                                    return <li key={index}>{feature}</li>
                                                })}
                                                <li>Insurance <Link to="/terms-condition">Terms and Condition</Link></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="description-button">
                                {data.rentInfo.availability > 0 && <Counter rounded onValueChange={value => this.onChange(value)} defaultValue={data.rentInfo.amounts} />}
                                {data.rentInfo.availability <= 0 && (
                                    <Button outline onClick={this.notifyMe}>
                                        Notify Me
                                    </Button>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// CarBox.defaultProps = {
//     id: 1,
//     carImage: AvanzaImage,
//     carSpecial: false,
//     carOnBooking: false,
//     carRest: false,
//     box: {
//         title: "Toyota Avanza (SUV)",
//         info: {
//             user: 6,
//             suitcase: 3,
//             settings: "MT",
//             protection: true,
//             ac: true
//         },
//         price: {
//             package: 325000,
//             beforeDiscount: 0,
//             day: 975000,
//             totalDay: 3
//         }
//     },
//     onChange: v => console.log(v)
// };

// CarBox.propTypes = {
//     id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
//     carImage: PropTypes.oneOfType([PropTypes.node, PropTypes.object]),
//     carSpecial: PropTypes.bool,
//     carOnBooking: PropTypes.bool,
//     carRest: PropTypes.bool,
//     box: PropTypes.shape({
//         title: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
//         info: PropTypes.shape({
//             user: PropTypes.number,
//             suitcase: PropTypes.number,
//             settings: PropTypes.string,
//             protection: PropTypes.bool,
//             ac: PropTypes.bool
//         }),
//         price: PropTypes.shape({
//             package: PropTypes.number,
//             beforeDiscount: PropTypes.number,
//             day: PropTypes.number,
//             totalDay: PropTypes.number
//         })
//     }),
//     onChange: PropTypes.func
// };

export default CarBox;