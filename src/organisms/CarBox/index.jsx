/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-25 17:01:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-21 10:31:37
 */
import React, { Component } from "react";
// import { Link } from "react-router-dom";
import cx from "classnames";
import { connect } from "react-redux";
import Slider from "react-slick";
import _ from 'lodash';
import moment from 'moment';
import 'moment/locale/id';

// components
import { Button, Rupiah } from "atom";
import { Counter } from "organisms";

// styles
import "./CarBox.scss";

// actions
import { getPriceCarRental } from "actions";
// helpers
import { localStorageDecrypt } from "helpers";
import lang from '../../assets/data-master/language'

class CarBox extends Component {
    
    state = {
        modalAction:'',
        type_service:'',
        selected: null,
        showDetail: false,
        changeAmount: 0
    }

    // getSnapshotBeforeUpdate = (prevProps, prevState) => {
    //     if(this.props.data !== prevProps.data) {
    //         return this.props.data;
    //     }
    //     return null;
    // }
    
    // componentDidMount = (prevProps, prevState, snapshot) => {
    //
    // }

    componentWillReceiveProps = (nextProps) => {
        if(nextProps.data.rentInfo.amounts >= 0) {
            this.onChange(nextProps.data.rentInfo.amounts);
            this.setState({
                changeAmount: nextProps.data.rentInfo.amounts
            });
        }
    }

    onChange(value) {
        let newObj = {...this.props.data};
        newObj.rentInfo.amounts = value;
        newObj.rentInfo.total = value * newObj.rentInfo.basePrice;
        this.setState({
            changeAmount: value
        });
        this.props.onChange(newObj);
    }

    notifyMe() {
        console.log('notify me clicked')
    }

    handleShowDetail = () => {
        this.setState({
            showDetail: !this.state.showDetail
        });
    }

    shouldComponentUpdate(nextProps, nextState){
        if(nextProps.pricecarrentalsuccess !== null) return true;
        if(nextState !== this.state) return true;
        return false;
    }
    tncChange = (e,x) => {
        e.preventDefault();
        console.log(x);
        this.props.tncChange(true);
        this.props.tncChangeData(x);
    };

    createMarkup = (html) => {
        let tmp = 'tes';
        html = html + this.state.modalAction;
        console.log(html);
        return {__html: html};
    };

    render() {
        const styleBorder = {padding: '5px 20px 10px 0',borderTop: '1px solid',borderTopColor: '#cacaca'};
        const { showDetail, changeAmount } = this.state;
        const { data, type, type_service } = this.props;
        const classDetail = cx("detail", { show: showDetail });
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        let rangeDay;
        if(type === "airport-transfer") {
            rangeDay = moment(CarRentalFormInput.data[0].date.endDate).diff(moment(CarRentalFormInput.data[0].date.startDate), 'days');
        } else {
            rangeDay = moment(CarRentalFormInput.date.endDate).diff(moment(CarRentalFormInput.date.startDate), 'days');
        }

        var settings = {
            dots: false,
            infinite: false,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            initialSlide: 0,
            variableWidth: true,
            centerMode: true,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        };
        return (
            <div className="o-car-box">
                <div className="car-box-wrapper">
                    <div className="box-image">
                        {/* {data.rentInfo.priceBeforeDiscount > data.rentInfo.price && <div className="special-deal">Special Deal!</div>} */}
                        {/* <img src={data.img} alt={data.name} /> */}
                        <img src={data.vehicleImage !== null ? data.vehicleImage : "http://www.indianbluebook.com/themes/ibb/assets/images/noImageAvailable.jpg"} alt={data.vehicleTypeId} />
                        {data.availableUnit <= 0 && <div className="car-status red">CAR ON BOOKING</div>}
                        {data.availableUnit === 1 && <div className="car-status blue">{lang.hurry[lang.default]} 1 {type !== "bus-rental" ? lang.carLeft[lang.default] : lang.carLeftBus[lang.default]}</div>}
                    </div>
                    <div className="box-description">
                        <div className="description-item">
                            <div className="car-name">
                                <h4>{data.vehicleAlias !== null ? data.vehicleAlias : data.vehicleTypeDesc}</h4>
                            </div>
                            <div className="car-price">
                                <div className="price-per-package">
                                    {/* <React.Fragment>
                                        <div className="before-discount">
                                            <span className="price-before">
                                                <Rupiah value={300000} />
                                            </span>
                                            <span className="package-label"> / package</span>
                                        </div>
                                    </React.Fragment> */}
                                    <Rupiah value={data.rentInfo.basePrice} />
                                    <span className="package-label"> / {type === "bus-rental" ? "unit" : null} {type === "airport-transfer" ? "trip" : null}  {type === "car-rental" ? "trip" : null} </span>
                                </div>
                                {type !== "bus-rental" && 
                                    rangeDay > 0 &&
                                        <div className="price-per-3days">
                                            <Rupiah value={data.rentInfo.basePrice * (rangeDay + 1)} />
                                            {
                                                type === "airport-transfer" ?
                                                    <span className="package-label">{`/ ${rangeDay + 1} ${lang.trip[lang.default]}`}</span> :
                                                    <span className="package-label">{`/ ${rangeDay + 1} ${lang.days[lang.default]}`}</span>

                                            }

                                        </div> 
                                }
                                <div className="tax-note">
                                    {
                                        type === 'bus-rental' ? lang.TaxesFeesIncluded[lang.default] : lang.TaxesFeesIncludedSelfDrive[lang.default]
                                    }
                                    </div>
                            </div>
                        </div>
                        <div className="description-item car-specification">
                            <div className="car-specification-list">
                                <div className="item passenger">{data.totalSeat}</div>
                                <div className="item baggage">{data.totalLugagge}</div>
                                { type !== 'bus-rental' && <div className="item transmission">{data.isTransmissionManual === 1 ? "MT" : "AT"}</div>}
                                {data.isAirbag === 1 && <div className="item air-bag"></div>}
                                {data.isAC === 1 && <div className="item air-conditioner"></div>}
                                {/* <div className="item passenger">{data.carInfo.passanger}</div> */}
                                {/* <div className="item baggage">{data.carInfo.suitcase}</div> */}
                                {/* <div className="item transmission">{data.carInfo.transmission}</div> */}
                                {/* {data.carInfo.protection && <div className="item air-bag"></div>} */}
                                {/* {data.carInfo.ac && <div className="item air-conditioner"></div>} */}
                            </div>
                        </div>
                        <div className="description-item car-description-detail">
                            <div className="description-detail">
                                <Button text left onClick={this.handleShowDetail}>
                                    {showDetail ? lang.hideDetail[lang.default] : lang.moreInfo[lang.default]}
                                </Button>
                                <div className={classDetail}>
                                    <div className="detail-content">
                                        <h6 className="detail-content-title"> Interior</h6>
                                        { data.detailImage.length > 0 && 
                                        <Slider {...settings}>
                                            { _.map(data.detailImage, ((v, k) => (
                                                <div key={k} style={{ width: 100 }} className="thumb">
                                                    <img alt={k} src={v} />
                                                </div>
                                            ))) }
                                        </Slider>
                                        }
                                        <div style={{fontSize:13}}>
                                            <div dangerouslySetInnerHTML={this.createMarkup(data.description)} style={{ paddingRight: 20 }} />
                                                <span style={styleBorder}>
                                                    {lang.insurance[lang.default]} <a href="/" onClick={(e) => this.tncChange(e,data.termAndCondition)}>{lang.termsAndConditions[lang.default]}</a>
                                                </span>
                                            </div>

                                        {/* <h6 className="detail-content-title">Average Vehicle Age: {data.carInfo.averageVehicleAgeNotes}</h6>
                                        <h6 className="detail-content-title">Price Includes</h6>
                                        <div className="list-item">
                                            <ul>
                                                {data.carInfo.priceIncludes.map((feature, index) => {
                                                    return <li key={index}>{feature}</li>
                                                })}
                                                <li>Insurance <Link to="/terms-condition">Terms and Condition</Link></li>
                                            </ul>
                                        </div> */}
                                    </div>
                                </div>
                            </div>
                            <div className="description-button">
                                {data.availableUnit > 0 && <Counter rounded onValueChange={value => this.onChange(value)} maxAvailable={data.availableUnit} defaultValue={data.rentInfo.amounts} changeAmount={changeAmount} />}
                                {/* {data.availableUnit > 0 && <Counter rounded onValueChange={value => this.onChange(value)} isClear={clearCarAmount == data.id} maxAvailable={data.availableUnit} />} */}
                                {data.availableUnit <= 0 && (
                                    <Button outline onClick={this.notifyMe}>
                                        Notify Me
                                    </Button>
                                )}
                                {/* {data.rentInfo.availability > 0 && <Counter rounded onValueChange={value => this.onChange(value)} defaultValue={data.rentInfo.amounts} />}
                                {data.rentInfo.availability <= 0 && (
                                    <Button outline onClick={this.notifyMe}>
                                        Notify Me
                                    </Button>
                                )} */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// CarBox.defaultProps = {
//     id: 1,
//     carImage: AvanzaImage,
//     carSpecial: false,
//     carOnBooking: false,
//     carRest: false,
//     box: {
//         title: "Toyota Avanza (SUV)",
//         info: {
//             user: 6,
//             suitcase: 3,
//             settings: "MT",
//             protection: true,
//             ac: true
//         },
//         price: {
//             package: 325000,
//             beforeDiscount: 0,
//             day: 975000,
//             totalDay: 3
//         }
//     },
//     onChange: v => console.log(v)
// };

// CarBox.propTypes = {
//     id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
//     carImage: PropTypes.oneOfType([PropTypes.node, PropTypes.object]),
//     carSpecial: PropTypes.bool,
//     carOnBooking: PropTypes.bool,
//     carRest: PropTypes.bool,
//     box: PropTypes.shape({
//         title: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
//         info: PropTypes.shape({
//             user: PropTypes.number,
//             suitcase: PropTypes.number,
//             settings: PropTypes.string,
//             protection: PropTypes.bool,
//             ac: PropTypes.bool
//         }),
//         price: PropTypes.shape({
//             package: PropTypes.number,
//             beforeDiscount: PropTypes.number,
//             day: PropTypes.number,
//             totalDay: PropTypes.number
//         })
//     }),
//     onChange: PropTypes.func
// };

// const mapStateToProps = ({ carrental }) => {
//     const { maxtotalunitcart } = carrental;
//     return { maxtotalunitcart }
// };

// export default connect(mapStateToProps, {
    
// })(CarBox);
const mapStateToProps = ({ carrental }) => {
    const { loading, pricecarrentalsuccess, pricecarrentalfailure } = carrental;
    return { loading, pricecarrentalsuccess, pricecarrentalfailure }
}

export default connect(mapStateToProps, {
    getPriceCarRental
})(CarBox);
