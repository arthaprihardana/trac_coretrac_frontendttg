import { Tnc } from 'atom';
import React, { Component } from 'react';

class PopupContent extends Component {
    state = {
        tncData: [],
        tncContent: {}
    }

    handleDetail = (data) => {
        this.setState({tncContent: data})
    }

    componentDidMount(){
        this.setState({
            tncData: this.props.data,
        }, () => {
            this.setState({tncContent: this.state.tncData[0]})
        })
    }

    render() {
        const {
            state: {
                tncData,
                tncContent
            },
            handleDetail
        } = this;
        return (
            <div className="tnc-popup-wrapper">
                <div className="tnc-popup-content">
                    <h3>{tncContent.title}</h3>
                    <div className="tnc-content-body">
                        <ol>
                            {
                                tncContent.content && tncContent.content.length > 0 ? 
                                tncContent.content.map((content, i)=> <li key={i}>{content}</li>)
                                : null
                            }
                        </ol>
                    </div>
                </div>
                <div className="tnc-popup-menu">
                    <Tnc data={tncData} onClick={handleDetail} />
                </div>
            </div>
        )
    }
}

PopupContent.defaultProps = {
    data: [
        {
            id: 1,
            title: 'title',
            content: ['content1', 'content2']
        }
    ]
}

export default PopupContent;