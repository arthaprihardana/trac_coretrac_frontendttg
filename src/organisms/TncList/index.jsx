/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-08 11:10:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-18 10:11:02
 */
import React, { Component, Fragment } from 'react';
import { Modal } from 'atom';
import './TncList.scss';
import TncDummy from 'assets/data-dummy/tnc.json'
import { Tnc } from 'atom';
import PopupContent from './thisComponent/PopupContent';
import { Button } from '../../atom';

class TncList extends Component {
    state = {
        showTnc: false,
        tncData: TncDummy
    }

    closeModal = () => {
        this.setState({showTnc: false})
    }

    openModal = () => {
        const windowWidth = window.innerWidth;
        if(windowWidth < 500){
            this.props.triggerTnc();
        }else {
            this.setState({showTnc: true})
        }
    }

    onSubmit = () => {
        this.props.onSubmit()
    }

    render() {
        const {
            state: {
                showTnc,
                tncData
            },
            closeModal,
            openModal,
            // onSubmit
        } = this;
        const { onSubmit } = this.props;
        return (
            <Fragment>
                <div className="tnc-order">
                    <Tnc data={tncData} onClick={openModal} />
                    <Modal
                        open={showTnc}
                        onClick={() => console.log('outside modal click')}
                        header={{
                            withCloseButton: true,
                            onClick: closeModal,
                            children: ''
                        }}
                        content={{
                            children: (
                                <PopupContent data={tncData} />
                            ),
                        }}
                        footer={{
                            position: 'center',
                            children: (
                                <div/>
                            ),
                        }}
                    />
                    <button  className="a-btn confirm"  primaryBlock onClick={onSubmit}>
                        Confirm
                    </button>
                </div>
            </Fragment>
        )
    }
}

export default TncList;