/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-30 23:47:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 09:40:50
 */
import React, { Component, Fragment } from "react";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import _ from 'lodash';
import { connect } from "react-redux";
import { checkIsLogin } from "actions";
import cx from 'classnames';

// component
import { NavigationMenu } from "molecules";
import { greetings } from "helpers";

import { getLogout } from "actions";
import { delay } from "helpers";
import {OutsideClick} from 'organisms';
// style
import "./Header.scss";

import userAvatar from 'assets/images/dummy/unknown.png';
// import UserProfile from "../../../assets/images/dummy/unknown.png";
import db from "../../db";

class Header extends Component {
    state = {
        showNavigation: false,
        isLogin: false,
        login_data: null,
        showNotifMenu: false,
	showAvatarMenu: false
    };

    componentDidMount() {
        this.props.checkIsLogin();
        document.body.classList.remove('remove-scroll-ios');
        document.body.classList.remove('remove-scroll');
        window.addEventListener("scroll", this.handleScroll);
        setTimeout(function() {
            document.querySelectorAll(".o-header")[0].classList.remove('hide');
        }, 500);
    }

    static getDerivedStateFromProps(props, state) {
        return {
            isLogin: props.isLogin,
            login_data: props.localUser
        }
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.isLogin !== prevProps.isLogin) {
            return { login : this.props.isLogin };
        }
        if(this.props.logoutSuccess !== prevProps.logoutSuccess) {
            return { success: this.props.logoutSuccess }
        }
        if(this.props.logoutFailure !== prevProps.logoutFailure) {
            return { failure: this.props.failure }
        }
        return null;
    }
    handleNotifMenu = () => {
        this.setState({
            showNotifMenu: !this.state.showNotifMenu
        })
    }
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.login) {
                this.setState({
                    isLogin: snapshot,
                    login_data: this.props.localUser
                });
            }
            if(snapshot.success){

                if(snapshot.success !== undefined) {
                    // db.table('local_auth').delete(1);
                    // localStorage.removeItem('_aaa');
                    window.location = '/';
                }
            }
            if(snapshot.failure){
                if(snapshot.success !== undefined) {
                    window.location = '/';
                }
            }
        }
    }

    componentDidCatch = (error, info) => {
        console.log('info ==>', info);
    }

    navigationMenu = () => {

        let { showNavigation } = this.state;
        showNavigation = !showNavigation;

        this.setState({
            showNavigation: showNavigation
        });

        let _removeScroll = "remove-scroll";
        if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
            _removeScroll = "remove-scroll-ios";
        }

        if (!this.state.showNavigation) {
            this.props.showOverlay(true, this.navigationMenu);
            setTimeout(function(){
                document.body.classList.add(_removeScroll);
            }, 500);
        } else {
            document.body.classList.remove(_removeScroll);
            this.props.showOverlay(false, this.navigationMenu);
        }

    };

    handleScroll = () => {
        const scroll = window.scrollY;
        if (scroll > 24) {
            document.querySelectorAll(".o-header")[0].classList.add("scrolled");
        } else {
            document.querySelectorAll(".o-header")[0].classList.remove("scrolled");
        }
    };

    logoutMe = (e) => {
        e.preventDefault();
        console.log("LOGOUT ME");
        this.props.getLogout();
        db.table('local_auth').delete(1);
        localStorage.removeItem('_aaa');
        window.location = '/';
    };
    goHome = () =>{
        window.location = '/';
    };
    avatarMenu = () => {
        this.setState({
            showAvatarMenu: !this.state.showAvatarMenu
        })
    }

    render() {
        const { solid, hideMenu, headerUserLogin, headerClose, dashboardLogin } = this.props;
        const { isLogin, login_data, showAvatarMenu, showNotifMenu } = this.state;
	const { avatarMenu,goHome } = this;
        const classShowAvatarMenu = cx('user-menu', {
            'show': showAvatarMenu
        });
        const classShowNotifMenu = cx('notif-menu', {
            'show': showNotifMenu
        });
        return (
            <Fragment>
                {/* Add solid-background for background white */}
                <div className={`o-header ${solid ? "solid-background" : "hide"}`}>
                    <div className="container">
                        <span onClick={goHome} className="trac-logo" style={{cursor:'pointer'}}>
                            Trac Logo
                        </span>
                        { !hideMenu && 
                            <div className="header-right-section">
                                <div className="header-right-links">
                                    <Link to="/" className="link-item btn-call-center">
                                        <span className="badge">24/7</span>
                                        <span>Call Center</span>
                                        <span>1500 009</span>
                                    </Link>
                                    {
                                        dashboardLogin && (
                                            <Fragment>
                                                <div className="notif-dashboard" onClick={this.handleNotifMenu}>
                                                    <div className={classShowNotifMenu}>
                                                        <ul>
                                                            <li className="menu1"><Link to="/dashboard/account-settings">Promo 17 Agustus 50%</Link>
                                                                <p>Lorem Ipsum is simply dummy text of the printing and industry.</p>
                                                            </li>
                                                            <li className="menu2"><Link to="/dashboard/account-settings">Airport Transfer discount 30%</Link>
                                                                <p>Lorem Ipsum is simply dummy text of the printing and industry.</p>
                                                            </li>
                                                            <li className="menu3"><Link to="/dashboard/account-settings">Jalan bersama keluarga</Link>
                                                                <p>Lorem Ipsum is simply dummy text of the printing and industry.</p>
                                                            </li>
                                                            <li className="menu4"><Link to="/dashboard/account-settings">Promo 17 Agustus 50%</Link>
                                                                <p>Lorem Ipsum is simply dummy text of the printing and industry.</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </Fragment>
                                        )
                                    }
                                    {/*{ headerUserLogin && isLogin ?*/}
                                    {  isLogin ?
                                        <div className="header-user-home">
                                            <div  className="user-caption-home" style={{padding:0}}>
                                                <span className="user-description-home" style={{display:"relative"}}>{greetings()}</span>
                                                <span className="user-name-home" style={{display:'relative'}}>
                                                {" "}
                                                {
                                                    login_data && login_data.FirstName ? 
                                                        login_data.FirstName.length > 10 ?
                                                        _.startCase(login_data.FirstName.substring(0, 10)) + "..."
                                                            : _.startCase(login_data.FirstName)
                                                        : 
                                                            _.split(login_data.EmailPersonal, "@")[0].length > 10 ?
                                                                _.startCase((_.split(login_data.EmailPersonal, "@")[0]).substring(0, 10)) + "..."
                                                                : _.startCase(_.split(login_data.EmailPersonal, "@")[0]) 
                                                }
                                                
                                                </span>
                                            </div>
                                            <div style={{right: -39,top: -23}}  className={`header-right-login ${dashboardLogin ? 'dashboard' : ''}`}>
                                                <OutsideClick className="header-user"  active={showAvatarMenu} onOutsideClick={avatarMenu}>
                                                    <div className="user-avatar" onClick={avatarMenu}>
                                                        <img src={ login_data && login_data.ImagePath ? login_data.ImagePath : userAvatar } alt={login_data.FirstName} />
                                                    </div>
                                                    <div className={classShowAvatarMenu}>
                                                        <ul>
                                                            <li className="profile"><Link to="/dashboard/account-settings">My Profile</Link></li>
                                                            <li className="logout" ><Link to="#" onClick={this.logoutMe} style={{ cursor: 'pointer' }}>Log Out</Link></li>
                                                        </ul>
                                                    </div>
                                                </OutsideClick>
                                            </div>

                                            {/*<Link to="/dashboard/my-booking" className="user-avatar-home">*/}
                                                {/*<img src={ login_data && login_data.ImagePath ? login_data.ImagePath : userAvatar } alt={login_data.FirstName}  />*/}
                                            {/*</Link>*/}
                                        </div>
                                        :
                                        <Link to="/login" className="link-item btn-login">
                                            Login
                                        </Link>
                                    }
                                </div>
                                { !hideMenu && 
                                <div className="burger-menu" onClick={this.navigationMenu}>
                                    <span className="burger-menu-bar" />
                                    <span className="burger-menu-bar" />
                                    <span className="burger-menu-bar" />
                                </div> }
                            </div>
                        }
                        { hideMenu && 
                            <div className="header-right-login">
                                {  isLogin &&
                                    <div className="header-user">
                                        <div className="user-caption">
                                            <span className="user-description">{greetings()}</span> 
                                            <span className="user-name">
                                            {" "}
                                            {
                                                login_data && login_data.FirstName ? 
                                                    login_data.FirstName.length > 10 ?
                                                    _.startCase(login_data.FirstName.substring(0, 10)) + "..."
                                                        : _.startCase(login_data.FirstName)
                                                    : 
                                                        _.split(login_data.EmailPersonal, "@")[0].length > 10 ?
                                                            _.startCase((_.split(login_data.EmailPersonal, "@")[0]).substring(0, 10)) + "..."
                                                            : _.startCase(_.split(login_data.EmailPersonal, "@")[0]) 
                                            }
                                            </span>
                                        </div>
                                        <div className="user-avatar">
                                            <img src={ login_data && login_data.ImagePath ? login_data.ImagePath : userAvatar } alt={login_data.FirstName}  />
                                        </div>
                                    </div> 
                                }
                                { headerClose && 
                                <div className="header-close">
                                    <span onClick={goHome} className="close-icon" style={{cursor:'pointer'}}>
                                             Close
                                        </span>
                                </div> }
                            </div>
                        }
			 { dashboardLogin && 
                                <OutsideClick className="header-user"  active={showAvatarMenu} onOutsideClick={avatarMenu}>
                                    <div className="user-avatar" onClick={avatarMenu}>
                                        <img src={ userAvatar } alt="Bryan Barry"/>
                                    </div>
                                    <div className={classShowAvatarMenu}>
                                        <ul>
                                            <li className="profile"><Link to="/dashboard/account-settings">My Profile</Link></li>
                                            <li className="logout" onClick={this.logoutMe} style={{ cursor: 'pointer' }}>Log Out</li>
                                        </ul>
                                    </div>
                                </OutsideClick>
                            }
                        {/* { (!hideMenu && !headerUserLogin && !headerClose) &&
                            <div className="header-right-section">
                                <div className="header-right-links">
                                    <Link to="/" className="link-item btn-call-center">
                                        <span className="badge">24/7</span>
                                        <span>Call Center</span>
                                        <span>(021) 877 877 87</span>
                                    </Link>
                                    { isLogin ?
                                        <div className="header-user-home">
                                            <Link to="/dashboard/my-booking" className="user-caption-home">
                                                <span className="user-description-home">{greetings()}</span> 
                                                <span className="user-name-home">
                                                {" "}
                                                {
                                                    login_data && login_data.FirstName ? 
                                                        login_data.FirstName.length > 10 ?
                                                        _.startCase(login_data.FirstName.substring(0, 10)) + "..."
                                                            : _.startCase(login_data.FirstName)
                                                        : 
                                                            _.split(login_data.EmailPersonal, "@")[0].length > 10 ?
                                                                _.startCase((_.split(login_data.EmailPersonal, "@")[0]).substring(0, 10)) + "..."
                                                                : _.startCase(_.split(login_data.EmailPersonal, "@")[0]) 
                                                }
                                                
                                                </span>
                                            </Link>
                                            <Link to="/dashboard/my-booking" className="user-avatar-home">
                                                <img src={ login_data && login_data.ImagePath ? login_data.ImagePath : userAvatar } alt={login_data.FirstName}  />
                                            </Link>
                                        </div>
                                        :
                                        <Link to="/login" className="link-item btn-login">
                                            Login
                                        </Link>
                                    }
                                </div>
                                <div className="burger-menu" onClick={this.navigationMenu}>
                                    <span className="burger-menu-bar" />
                                    <span className="burger-menu-bar" />
                                    <span className="burger-menu-bar" />
                                </div>
                            </div>
                        }
                        <div className="header-right-login">
                            { hideMenu && isLogin && 
                                <div className="header-user">
                                    <div className="user-caption">
                                        <span className="user-description">{greetings()}</span> 
                                        <span className="user-name">
                                        {" "}
                                        {
                                            login_data && login_data.FirstName ? 
                                                login_data.FirstName.length > 10 ?
                                                _.startCase(login_data.FirstName.substring(0, 10)) + "..."
                                                    : _.startCase(login_data.FirstName)
                                                : 
                                                    _.split(login_data.EmailPersonal, "@")[0].length > 10 ?
                                                        _.startCase((_.split(login_data.EmailPersonal, "@")[0]).substring(0, 10)) + "..."
                                                        : _.startCase(_.split(login_data.EmailPersonal, "@")[0]) 
                                        }
                                        </span>
                                    </div>
                                    <div className="user-avatar">
                                        <img src={ login_data && login_data.ImagePath ? login_data.ImagePath : userAvatar } alt={login_data.FirstName}  />
                                    </div>
                                </div>
                            }
                            { headerClose && 
                                <div className="header-close">
                                    <Link to="/" className="close-icon">
                                        Close
                                    </Link>
                                </div>
                            }
                        </div> */}
                    </div>
                </div>
                { (!hideMenu) &&
                    <NavigationMenu showClass={this.state.showNavigation === true ? " show" : ""} hideNavigationEvent={this.navigationMenu} />
                }
            </Fragment>
        );
    }
}

Header.defaultProps = {
    solid: false,
    hideMenu: false,
    headerUserLogin: false,
    headerClose: false,
    dashboardLogin: false,
};

Header.propTypes = {
    history: PropTypes.object,
    solid: PropTypes.bool,
    hideMenu: PropTypes.bool,
    headerUserLogin: PropTypes.bool,
    headerClose: PropTypes.bool,
    dashboardLogin: PropTypes.bool,
};

const mapStateToProps = ({ login }) => {
    const { isLogin,logoutSuccess, logoutFailure, localUser } = login;
    return { isLogin, logoutSuccess, logoutFailure, localUser };
};

export default connect(mapStateToProps, {
    getLogout,
    checkIsLogin
})(Header);
