import React, { PureComponent, Fragment } from "react";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

// component
import { NavigationMenu } from "molecules";

// style
import "./Header.scss";

import userAvatar from 'assets/images/dummy/user-testimonial-3.jpg';

class Header extends PureComponent {
    state = {
        showNavigation: false
    };

    componentDidMount() {
        document.body.classList.remove('remove-scroll-ios');
        document.body.classList.remove('remove-scroll');
        window.addEventListener("scroll", this.handleScroll);
        setTimeout(function() {
            document.querySelectorAll(".o-header")[0].classList.remove('hide');
        }, 500);
    }

    navigationMenu = () => {

        let { showNavigation } = this.state;
        showNavigation = !showNavigation;

        this.setState({
            showNavigation: showNavigation
        });

        let _removeScroll = "remove-scroll";
        if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
            _removeScroll = "remove-scroll-ios";
        }

        if (!this.state.showNavigation) {
            this.props.showOverlay(true, this.navigationMenu);
            setTimeout(function(){
                document.body.classList.add(_removeScroll);
            }, 500);
        } else {
            document.body.classList.remove(_removeScroll);
            this.props.showOverlay(false, this.navigationMenu);
        }

    };

    handleScroll = () => {
        const scroll = window.scrollY;
        if (scroll > 24) {
            document.querySelectorAll(".o-header")[0].classList.add("scrolled");
        } else {
            document.querySelectorAll(".o-header")[0].classList.remove("scrolled");
        }
    };

    render() {
        const { solid, hideMenu, headerUserLogin, headerClose, } = this.props;
        return (
            <Fragment>
                {/* Add solid-background for background white */}
                <div className={`o-header ${solid ? "solid-background" : "hide"}`}>
                    <div className="container">
                        <Link to="/" className="trac-logo">
                            Trac Logo
                        </Link>
                        { (!hideMenu && !headerUserLogin && !headerClose) &&
                            <div className="header-right-section">
                                <div className="header-right-links">
                                    <Link to="/" className="link-item btn-call-center">
                                        <span className="badge">24/7</span>
                                        <span>Call Center</span>
                                        <span>(021) 877 877 87</span>
                                    </Link>
                                    <div className="language">
                                        <div className="text">EN</div>
                                        <div className="dropdown">
                                            <ul>
                                                <li>
                                                    <Link to="/" className="language-id active">
                                                        Bahasa
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link to="/" className="language-en">
                                                        English
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <Link to="/login" className="link-item btn-login">
                                        Login
                                    </Link>
                                </div>
                                <div className="burger-menu" onClick={this.navigationMenu}>
                                    <span className="burger-menu-bar" />
                                    <span className="burger-menu-bar" />
                                    <span className="burger-menu-bar" />
                                </div>
                            </div>
                        }
                        <div className="header-right-login">
                            { headerUserLogin && 
                                <div className="header-user">
                                    <div className="user-caption">
                                        <span className="user-description">You are now login as</span> <span className="user-name">Bryan Barry</span>
                                    </div>
                                    <div className="user-avatar">
                                        <img src={ userAvatar } alt="Bryan Barry"/>
                                    </div>
                                </div>
                            }
                            { headerClose && 
                                <div className="header-close">
                                    <Link to="/" className="close-icon">
                                        Close
                                    </Link>
                                </div>
                            }
                        </div>
                    </div>
                </div>
                { (!hideMenu && !headerUserLogin) &&
                    <NavigationMenu showClass={this.state.showNavigation === true ? " show" : ""} hideNavigationEvent={this.navigationMenu} />
                }
            </Fragment>
        );
    }
}

Header.defaultProps = {
    solid: false,
    hideMenu: false,
    headerUserLogin: false,
    headerClose: false,
};

Header.propTypes = {
    solid: PropTypes.bool,
    hideMenu: PropTypes.bool,
    headerUserLogin: PropTypes.bool,
    headerClose: PropTypes.bool,
};

export default Header;
