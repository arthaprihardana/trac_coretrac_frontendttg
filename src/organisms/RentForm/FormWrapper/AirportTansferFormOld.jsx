// core
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// plugin component
import moment from 'moment';

// form display
import FormDisplay from '../FormDisplay/FormDisplay';
import MobileTopHeader from '../FormDisplay/MobileTopHeader';

// form input
import RadioFormInput from '../FormInput/RadioFormInput';
import ListSearchFormInput from '../FormInput/ListSearchFormInput';
import FlightDetailsFormInput from '../FormInput/FlightDetailsFormInput';
import DateFormInput from '../FormInput/DateFormInput';
import ButtonForm from '../FormInput/ButtonForm';

// data city
import airportDataJSON from '../../../assets/data-dummy/city.json';
import cityDataJSON from '../../../assets/data-dummy/city.json';
import airlineDataJSON from '../../../assets/data-dummy/airline.json';
import addressDataJSON from '../../../assets/data-dummy/address.json';

class AirportTansferForm extends PureComponent {
  // state
  state = {
    startDate: moment().format('YYYY-MM-DD HH:MM'),
    endDate: moment().format('YYYY-MM-DD HH:MM'),
    inputShow: 0,
    switched: false,
    airportData: airportDataJSON,
    airlineData: airlineDataJSON,
    cityData: cityDataJSON,
    addressData: addressDataJSON,
    activeButton: false,
    formInput: {
      type_service: '',
      airport: {
        value: '',
        text: '',
      },
      city: {
        value: '',
        text: '',
      },
      address: {
        value: '',
        text: '',
      },
      airline: {
        name: '',
        number: '',
        value: '',
        text: '',
      },
      date: {
        startDate: '',
        endDate: '',
        time: '',
      },
    },
    formDisplay: {
      typeService: 'Select Type',
      airport: 'Select Airport',
      pickupCityAndAddress: 'Select Your City',
      flightDetails: 'Enter Flight Details',
      dateAndTime: 'Select Date & Time',
    },
  }

  // handle click
  handleClick = (e) => {
    const { inputShow } = this.state;
    if (inputShow !== e.currentTarget.dataset.id && e.currentTarget.dataset.id !== undefined) {
      document.querySelectorAll('.jsSelectAirport')[0].classList.add('active');
      this.setState({ inputShow: parseInt(e.currentTarget.dataset.id) });
      this.props.hasChanged(true, this.handleClick, true);
    } else {
      this.setState({ inputShow: 0 });
      this.props.hasChanged(false, this.handleClick, false);
    }
  }

  // handle switch
  handleSwitch = (e) => {
    const { switched } = this.state;
    this.setState({ switched: !switched });
  }

  // click input radio
  handleClickInputRadio = (values) => {
    console.log(values);
    let updateFormDisplay = { ...this.state.formDisplay };
    updateFormDisplay[values.target] = values.text;

    let updateFormInput = { ...this.state.formInput };
    updateFormInput[values.name] = values.value;

    this.setState({
      inputShow: values.next,
      formInput: updateFormInput,
      formDisplay: updateFormDisplay,
    }, this.checkAllValue);
  }

  // filter list
  handleFilterList = (data, e) => {
    let filterBy = 'name',
      filterData = '',
      filterState = '',
      updateFilterData = {},
      updateFormDisplay = { ...this.state.formDisplay },
      updateFormInput = { ...this.state.formInput },
      test;

    switch (data) {
      case 'airport':
        filterData = airportDataJSON;
        filterState = 'airportData';
        break;
      case 'city':
        filterData = cityDataJSON;
        filterState = 'cityData';
        break;
      case 'address':
        filterData = addressDataJSON;
        filterState = 'addressData';
        break;
      case 'airline':
        filterData = airlineDataJSON;
        filterState = 'airlineData';
        filterBy = 'id';

        //get data filter
        let input = e.target.value.toUpperCase(),
          showData = filterData.filter((value) => {
            return value[filterBy].toLowerCase().search(e.target.value.toLowerCase()) !== -1;
          });

        updateFormInput.airline.name = showData.length == 1 && showData[0].id === input ? input : '';
        updateFormInput.airline.value = showData.length == 1 && showData[0].id === input ? updateFormInput.airline.name + ' ' + updateFormInput.airline.number : ' ';
        updateFormInput.airline.text = updateFormInput.airline.value;
        updateFormDisplay.flightDetails = updateFormInput.airline.text !== ' ' ? updateFormInput.airline.text : 'Enter Flight Details';

        this.setState({
          formInput: updateFormInput,
          formDisplay: updateFormDisplay,
        }, this.checkAllValue);
        break;
      default:
        break;
    }

    filterData = filterData.filter((value) => {
      return value[filterBy].toLowerCase().search(e.target.value.toLowerCase()) !== -1;
    });

    updateFilterData[filterState] = filterData;
    this.setState(updateFilterData);
  }

  // select item
  handleSelectItem = (data, e) => {
    let updateFormDisplayValue = '',
      updateState;
    const getText = e.currentTarget.innerText,
      getId = e.currentTarget.dataset.id,
      updateFormInput = { ...this.state.formInput };

    switch (data) {
      case 'airport':
        document.querySelectorAll('.jsSelectAirport')[0].classList.remove('active');

        updateFormInput.airport.value = getId;
        updateFormInput.airport.text = getText;

        updateFormDisplayValue = { ...this.state.formDisplay };
        updateFormDisplayValue.airport = getText;

        updateState = {
          inputShow: 3,
          formInput: updateFormInput,
          formDisplay: updateFormDisplayValue
        };
        break;
      case 'city':
        document.querySelectorAll('.jsSelectCityAirportTransfer')[0].classList.remove('active');
        document.querySelectorAll('.jsFindAddressAirportTransfer')[0].classList.add('active');

        updateFormInput.city.value = getId;
        updateFormInput.city.text = getText;

        updateState = {
          formInput: updateFormInput
        };
        break;
      case 'address':
        document.querySelectorAll('.jsFindAddressAirportTransfer')[0].classList.remove('active');
        document.querySelectorAll('.jsSelectCityAirportTransfer')[0].classList.add('active');

        updateFormInput.address.value = getId;
        updateFormInput.address.text = getText;

        updateFormDisplayValue = { ...this.state.formDisplay };
        updateFormDisplayValue.pickupCityAndAddress = getText;

        updateState = {
          inputShow: 4,
          formInput: updateFormInput,
          formDisplay: updateFormDisplayValue,
        };
        break;
      case 'airline':
        //document.querySelectorAll('.jsInputFlightDetails')[0].classList.remove('active');

        updateFormInput.airline.number = e.target.value;
        updateFormInput.airline.value = updateFormInput.airline.name + ' ' + updateFormInput.airline.number;
        updateFormInput.airline.text = updateFormInput.airline.name + ' ' + updateFormInput.airline.number;

        updateFormDisplayValue = { ...this.state.formDisplay };
        updateFormDisplayValue.flightDetails = updateFormInput.airline.name !== '' ? updateFormInput.airline.text : 'Enter Flight Details';

        updateState = {
          inputShow: 5,
          formInput: updateFormInput,
          formDisplay: updateFormDisplayValue,
        };
        break;
      default:
        break;
    }

    const updateFormDisplay = updateFormDisplayValue;
    this.setState(updateState, this.checkAllValue);
  }

  // back to select city
  handleBackSelectCity = () => {
    document.querySelectorAll('.jsFindAddressAirportTransfer')[0].classList.remove('active');
    document.querySelectorAll('.jsSelectCityAirportTransfer')[0].classList.add('active');
  }

  // get value date form
  getValueDateForm = (value, status) => {

    let setDateTime = '';
    const { startDate, endDate, selectDate } = value;
    const updateFormInput = { ...this.state.formInput };
    const updateFormDisplay = { ...this.state.formDisplay };

    switch (status) {
      case 'select-first-date':
        setDateTime = moment(startDate).format('ddd, DD MMM YYYY');
        updateFormInput.date.startDate = moment(startDate).format('YYYY-MM-DD');
        updateFormInput.date.endDate = '';
        updateFormInput.date.time = '';
        break;
      case 'select-end-date':
        setDateTime = moment(startDate).format('ddd, DD MMM YYYY') + '<span class="arrow-right"></span>' + moment(endDate).format('ddd, DD MMM YYYY');
        updateFormInput.date.startDate = moment(startDate).format('YYYY-MM-DD');
        updateFormInput.date.endDate = moment(endDate).format('YYYY-MM-DD');
        updateFormInput.date.time = '';
        break;
      case 'select-time':
        setDateTime = moment(startDate).format('ddd, DD MMM YYYY') + '<span class="arrow-right"></span>' + moment(endDate).format('ddd, DD MMM YYYY') + ' <span class="hour-minutes">' + moment(selectDate).format('HH:mm') + '</span>';
        updateFormInput.date.startDate = moment(startDate).format('YYYY-MM-DD');
        updateFormInput.date.endDate = moment(endDate).format('YYYY-MM-DD');
        updateFormInput.date.time = moment(selectDate).format('HH:mm');
        break;
      default:
        break;
    }

    updateFormDisplay.dateAndTime = setDateTime;

    this.setState({
      formInput: updateFormInput,
      formDisplay: updateFormDisplay,
    }, this.checkAllValue);

  }

  // check all value
  checkAllValue = () => {
    let whiteSpace = /^ *$/;
    if (
      !whiteSpace.test(this.state.formInput.type_service) &&
      !whiteSpace.test(this.state.formInput.airport.value) &&
      !whiteSpace.test(this.state.formInput.address.value) &&
      !whiteSpace.test(this.state.formInput.select_package) &&
      !whiteSpace.test(this.state.formInput.date.startDate) &&
      !whiteSpace.test(this.state.formInput.date.endDate) &&
      !whiteSpace.test(this.state.formInput.date.time)
    ) {
      this.setState({ activeButton: true });
    } else {
      this.setState({ activeButton: false });
    }
  }

  // search button
  handleSearchButton = (values) => {

    const { activeButton } = this.state;

    if (activeButton) {
      // Values and name form input
      this.props.onSubmit(this.state.formInput);
    }
  }

  // render
  render() {

    const {
      handleClick,
      handleSwitch,
      props: { mainSlideActive, inputOnTop },
      state: { inputShow, formInput, formDisplay, activeButton, switched }
    } = this;

    let classNameSwitch = 'switch-button';
    if (switched) {
      classNameSwitch += ' switched';
    }

    // rent form
    let rentForm = 'o-rent-form';
    if (mainSlideActive === 2) {
      rentForm += ' active';
    }

    if (inputOnTop) {
      rentForm += ' input-on-top';
    }

    let inputForm1 = 'rent-form-item primary-light width-25 ';
    let inputForm2 = 'rent-form-item primary-dark width-50 ';
    let inputForm3 = 'rent-form-item primary-darkest width-50 ';
    let inputForm4 = 'rent-form-item primary-darkest width-25 ';
    let inputForm5 = 'rent-form-item primary-extremely-darkest width-85 ';
    switch (inputShow) {
      case 1:
        inputForm1 += 'show';
        break;
      case 2:
        inputForm2 += 'show';
        break;
      case 3:
        inputForm3 += 'show';
        break;
      case 4:
        inputForm4 += 'show';
        break;
      case 5:
        inputForm5 += 'show';
        break;
      default:
        break;
    }

    // Calendar Data
    let rentCarCalendarData = {
      titleCalendar: 'Calendar',
      titleTime: 'Pick up hour',
      timeFormat: 'HH:mm',
      intervalTime: 30,
      highlightDates: [
        {
          date: '2018-11-29',
          description: '29 Nov : Lorem ipsum day'
        },
        {
          date: '2018-12-25',
          description: '25 Okt: Dolor sit day'
        }
      ],
      onClickButton: handleClick,
      textButton: 'Done',
      sendValue: this.getValueDateForm,
    }

    return (
      <div className={rentForm}>

        {/* Form item */}
        <div className={inputForm1}>
          {/* Form input */}
          <div className="rent-form-input">
            <MobileTopHeader title="Select Type" onClick={handleClick} />
            <div className="input-group-radio">
              <RadioFormInput
                label="One Way"
                value="1"
                name={Object.keys(formInput)[0]}
                dataNext="2"
                target="typeService"
                onClick={this.handleClickInputRadio}
              />
              <RadioFormInput
                label="Roundtrip"
                value="2"
                name={Object.keys(formInput)[0]}
                dataNext="2"
                target="typeService"
                onClick={this.handleClickInputRadio}
              />
              <RadioFormInput
                label="Multi City"
                value="3"
                name={Object.keys(formInput)[0]}
                dataNext="2"
                target="typeService"
                onClick={this.handleClickInputRadio}
              />
            </div>
          </div>
          {/* Form Display */}
          <FormDisplay id={1} label="Type of Service" value={formDisplay.typeService} onClick={handleClick} />
        </div>

        {/* Form item */}
        <div className={inputForm2}>
          {/* form input */}
          <div className="rent-form-input search-input">
            <MobileTopHeader title="From (Airport)" onClick={handleClick} />
            {/* Airport */}
            <ListSearchFormInput
              label="Select Airport"
              className="jsSelectAirport"
              name={Object.keys(formInput)[1]}
              data={this.state.airportData}
              onChangeInput={(e) => this.handleFilterList('airport', e)}
              onClickItem={(e) => this.handleSelectItem('airport', e)}
              activeBlock
            />
          </div>
          {/* Form Display */}
          <FormDisplay id={2} label="From (Airport)" value={formDisplay.airport} onClick={handleClick} search />
          <div className={classNameSwitch} onClick={handleSwitch}>Switch</div>
        </div>

        {/* Form item */}
        <div className={inputForm3}>
          {/* Form input */}
          <div className="rent-form-input search-input">
            <MobileTopHeader title="Pickup City and Address" onClick={handleClick} />
            {/* City */}
            <ListSearchFormInput
              label="Select City"
              className="jsSelectCityAirportTransfer"
              name={Object.keys(formInput)[2]}
              data={this.state.cityData}
              onChangeInput={(e) => this.handleFilterList('city', e)}
              onClickItem={(e) => this.handleSelectItem('city', e)}
              activeBlock
            />
            {/* Address */}
            <ListSearchFormInput
              label="Find Address"
              className="jsFindAddressAirportTransfer"
              name={Object.keys(formInput)[3]}
              data={this.state.addressData}
              onChangeInput={(e) => this.handleFilterList('address', e)}
              onClickItem={(e) => this.handleSelectItem('address', e)}
              onClickBack={this.handleBackSelectCity}
              backSearch
            />
          </div>
          {/* Form Display */}
          <FormDisplay id={3} label="To (Area, Address, building)" value={formDisplay.pickupCityAndAddress} onClick={handleClick} search />
        </div>

        {/* Form item */}
        <div className={inputForm4}>
          {/* Form input */}
          <div className="rent-form-input text-input">
            <MobileTopHeader title="Pickup City and Address" onClick={handleClick} />
            {/* City */}
            <FlightDetailsFormInput
              label="Flight Details"
              subLabel="Airline Name & Number"
              className="jsInputFlightDetails"
              name={Object.keys(formInput)[4]}
              data={this.state.airlineData}
              onChangeInput={(e) => this.handleFilterList('airline', e)}
              onBlur={(e) => this.handleSelectItem('airline', e)}
              activeBlock
            />
          </div>
          {/* Form Display */}
          <FormDisplay id={4} label="Flight Details" value={formDisplay.flightDetails} onClick={handleClick} />
        </div>

        {/* Form item */}
        <div className={inputForm5}>
          {/* Form input */}
          <div className="rent-form-input calendar-input">
            <MobileTopHeader title="Date and Time" onClick={handleClick} />
            <DateFormInput
              name={Object.keys(formInput)[5]}
              titleCalendar={rentCarCalendarData.titleCalendar}
              titleTime={rentCarCalendarData.titleTime}
              timeFormat={rentCarCalendarData.timeFormat}
              intervalTime={rentCarCalendarData.intervalTime}
              highlightDates={rentCarCalendarData.highlightDates}
              onClickButton={rentCarCalendarData.onClickButton}
              sendValue={rentCarCalendarData.sendValue}
              textButton={rentCarCalendarData.textButton}
            />
          </div>
          {/* Form Display */}
          <FormDisplay id={5} label="Date and Time" value={formDisplay.dateAndTime} onClick={handleClick} />
        </div>

        <ButtonForm text="Search" onClick={this.handleSearchButton} active={activeButton} />

      </div>
    );
  }

}

AirportTansferForm.defaultProps = {
  mainSlideActive: 2,
  inputShow: 0,
  inputOnTop: false,
  onSubmit: () => console.log('submited'),
  hasChanged: () => console.log('changed'),
};

// props types
AirportTansferForm.propTypes = {
  mainSlideActive: PropTypes.number,
  inputShow: PropTypes.number,
  inputOnTop: PropTypes.bool,
  onSubmit: PropTypes.func,
  hasChanged: PropTypes.func,
};

export default AirportTansferForm;