/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-11 07:56:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 07:54:26
 */
// core
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import moment from "moment";
import "moment/locale/id";
import _ from "lodash";

// data city
// import airportDataJSON from '../../../assets/data-dummy/city.json';
// import cityDataJSON from '../../../assets/data-dummy/city.json';
// import airlineDataJSON from '../../../assets/data-dummy/airline.json';
// import addressDataJSON from '../../../assets/data-dummy/address.json';

// form input
import ButtonForm from '../FormInput/ButtonForm';

// form wrapper
import FormWrapper from './FormWrapper.jsx';

//form type service
import OneWay from './AirportTransferForm/OneWay';
import RoundTrip from './AirportTransferForm/RoundTrip';
import MultiCity from './AirportTransferForm/MultiCity';

// actions
import { airportActiveInputForm, getDurationAirportTransfer } from "actions";

// helpers
import { localStorageEncrypt } from "helpers";

// plugin
import { PulseLoader } from 'react-spinners';
import { RENTAL_KMBASE } from '../../../constant';
import lang from "../../../assets/data-master/language";

class AirportTansferForm extends PureComponent {
    // state
    state = {
        // dataJson: {
        //     airportData: airportDataJSON,
        //     airlineData: airlineDataJSON,
        //     cityData: cityDataJSON,
        //     addressData: addressDataJSON
        // },
        inputShow: 0,
        switched: false,
        activeButton: false,
        formInput: {},
        formDisplay: {},
        typeService: 'one-way',
        MsZoneId: null,
		RentalDuration: 0
    }

    handleClosePopup = (e) => {
        let updateTypeService = { ...this.state.typeService };

        updateTypeService = 'one-way';
        this.setState({ inputShow: 0 });
        this.setState({ typeService: updateTypeService }, () => {
            this.props.hasChanged(false, this.handleChange, false, false);
        });
    }

    handleTypeServiceChange = (values, e) => {
        let updateTypeService = { ...this.state.typeService };

        //not oneway
        if (this.state.typeService !== values.value) {
            this.props.hasChanged(false, this.handleChange, false, false);

            updateTypeService = values.value;

            setTimeout(() => {
                this.setState({ typeService: updateTypeService }, (id) => {
                    id = id === undefined ? 1 : id;
                    this.setState({ inputShow: id });
                    this.props.hasChanged(true, this.handleChange, true, true, this.handleClosePopup);
                })
            }, 1000);
        }
    }

    // handle click
    handleChange = (e, id) => {
        const { inputShow, typeService } = this.state;
        if (e.type !== 'mousedown') {
            if (inputShow !== id && id !== undefined) {
                this.setState({ inputShow: parseInt(id) });
                this.props.hasChanged(true, this.handleChange, true, typeService === 'one-way' ? false : true, this.handleClosePopup);
            } else {
                if (typeService === 'one-way') {
                    this.props.hasChanged(false, this.handleChange, false, false);
                    this.setState({ inputShow: 0 });
                }
            }
        } else {
            this.setState({ inputShow: 0 });
            this.props.hasChanged(false, this.handleChange, false, false);
        }

        if(id === undefined) {
            this.props.airportActiveInputForm(true)
        }

        // if (id === 0) {
        //     // var elems = document.querySelectorAll(".rent-form-item");

        //     // [].forEach.call(elems, function (el) {
        //     //     console.log('test');
        //     //     el.classList.remove("show");
        //     // });
        // }
    }

    handleClick = (value) => {
        this.setState({ activeButton: value });
    }

    handleClickOutside = (e) => {
    }

    // search button
    handleSearchButton = (values) => {
        const { activeButton, formInput, formDisplay } = this.state;

        if (activeButton) {
            // Values and name form input
            let airportForm = _.map(formInput.data, v => {
                let AirportTransferForm = {
                    BusinessUnitId: this.props.businessUnitId,
                    BranchId: v.city.value,
                    // CityId: this.state.formInput.city.cityId,
                    StartDate: moment(`${v.date.startDate} ${v.date.time}`).toISOString(),
                    EndDate: moment(`${v.date.endDate} ${v.date.time}`).toISOString(),
                    IsWithDriver: 1, // 0 = self driver 1 = pakai driver
                    RentalDuration: this.state.RentalDuration,
                    RentalPackage: this.state.RentalDuration,
                    Uom: 'km',
                    ServiceTypeId: RENTAL_KMBASE,  // Rental KM Based
                    ValidateAttribute: 1,
                    ValidateContract: 1
                };
                return AirportTransferForm;
            });
            localStorageEncrypt('_fi', JSON.stringify(formInput));
            localStorage.setItem('AirportTransferFormDisplay', JSON.stringify(formDisplay));
            this.props.onSubmit(airportForm, 'onClick', 'values-completed');
            this.setState({ activeButton: false })
        }
    };
    handleChangeTop = (values) => {
        console.log("Handle Change TOP");
        const { activeButton, formInput, formDisplay } = this.state;
        // Values and name form input
        let airportForm = _.map(formInput.data, v => {
            let AirportTransferForm = {
                BusinessUnitId: this.props.businessUnitId,
                BranchId: v.city.value,
                // CityId: this.state.formInput.city.cityId,
                StartDate: moment(`${v.date.startDate} ${v.date.time}`).toISOString(),
                EndDate: moment(`${v.date.endDate} ${v.date.time}`).toISOString(),
                IsWithDriver: 1, // 0 = self driver 1 = pakai driver
                RentalDuration: this.state.RentalDuration,
                RentalPackage: this.state.RentalDuration,
                Uom: 'km',
                ServiceTypeId: RENTAL_KMBASE,  // Rental KM Based
                ValidateAttribute: 1,
                ValidateContract: 1
            };
            return AirportTransferForm;
        });
        localStorageEncrypt('_fi', JSON.stringify(formInput));
        localStorage.setItem('AirportTransferFormDisplay', JSON.stringify(formDisplay));
        // console.log("airportForm > ",airportForm);
        // console.log("formDisplay > ",formDisplay);
        // this.props.onSubmit(airportForm, 'onClick', 'values-completed');
        // this.setState({ activeButton: false })
    };

    handleGetFormInput = values => {
        this.setState({
            formInput: values.formInput,
            formDisplay: values.formDisplay,
            MsZoneId: values.MsZoneId,
            RentalDuration: values.KM
        });
    }

    // render
    render() {

        const {
            handleClick,
            handleChange,
            handleChangeTop,
            handleTypeServiceChange,
            // handleClickOutside,
            handleGetFormInput,
            props: { inputOnTop, mainSlideActive },
            state: { activeButton, inputShow }
        } = this;

        let airportRentFormTemplate = null;
        let activeOutSide = (inputShow !== 0 ? true : false);

        // rent form
        let rentForm = 'o-rent-form';
        if (mainSlideActive === 2) {
            rentForm += ' active';
        }

        if (inputOnTop) {
            rentForm += ' input-on-top';
        }

        //set type service form
        switch (this.state.typeService) {
            case 'one-way':
                airportRentFormTemplate = <OneWay onClick={handleClick} inputTOP={inputOnTop} hasChanged={handleChange} onTypeServiceChange={handleTypeServiceChange}  changeFormInput={handleGetFormInput} />;
                break;
            case 'roundtrip':
                airportRentFormTemplate = <RoundTrip onClick={handleClick} hasChanged={handleChange} onTypeServiceChange={handleTypeServiceChange} />;
                rentForm = rentForm + ' multi';
                break;
            case 'multi-city':
                airportRentFormTemplate = <MultiCity onClick={handleClick} hasChanged={handleChange} onTypeServiceChange={handleTypeServiceChange} />;
                rentForm = rentForm + ' multi';
                break;
            default:
                break;
        }

        return (
            <Fragment>
                <FormWrapper inputOnTop={inputOnTop} className={rentForm} onOutsideClick={handleChange} active={activeOutSide}>
                    {airportRentFormTemplate}

                    <div className="o-rent-form-wrapper-button">
                        <ButtonForm 
                            text={this.props.loading ? 
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={7}
                                    color={'#ffffff'}
                                    loading={this.props.loading}
                                /> : lang.search[lang.default]}
                            onClick={this.handleSearchButton} 
                            active={activeButton} />
                    </div>

                </FormWrapper>
            </Fragment>
        );
    }

}

AirportTansferForm.defaultProps = {
    activeButton: false,
    mainSlideActive: 2,
    inputShow: false,
    inputOnTop: false,
    onSubmit: () => console.log('submited'),
    hasChanged: () => console.log('changed'),
};

// props types
AirportTansferForm.propTypes = {
    activeButton: PropTypes.bool,
    mainSlideActive: PropTypes.number,
    inputShow: PropTypes.bool,
    inputOnTop: PropTypes.bool,
    onSubmit: PropTypes.func,
    hasChanged: PropTypes.func,
};

const mapStateToProps = ({ airporttransfer }) => {
    const { inputShow, duration } = airporttransfer;
    return { inputShow, duration };
}

export default connect(mapStateToProps, {
    airportActiveInputForm,
    getDurationAirportTransfer
})(AirportTansferForm);
