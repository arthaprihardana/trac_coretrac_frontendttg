/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-16 14:04:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 01:23:22
 */
// core
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';

// plugin component
import moment from 'moment';
import "moment/locale/id";
import { PulseLoader } from 'react-spinners';

// form display
import FormDisplay from '../FormDisplay';
import MobileTopHeader from '../FormDisplay/MobileTopHeader';

// form input
import RadioFormInput from '../FormInput/RadioFormInput';
import ListSearchFormInput from '../FormInput/ListSearchFormInput';
import DateFormInput from '../FormInput/DateFormInput';
import ButtonForm from '../FormInput/ButtonForm';

// form wrapper 
import FormWrapper from './FormWrapper';

// actions
import { getPackageCarRental, getCityCoverageCarRental, getAddressFromGM, getPlaceDetailFromGM, getFindGeometryFromGM } from "actions";

import lang from "../../../assets/data-master/language";
// helpers
import { localStorageEncrypt, localStorageDecrypt } from "helpers";
import { RENTAL_TIMEBASE } from '../../../constant';

class CarRentForm extends PureComponent {
    // state
    state = {
        startDate: moment().format('YYYY-MM-DD HH:MM'),
        endDate: moment().format('YYYY-MM-DD HH:MM'),
        inputShow: 0,
        cityData: [],
        addressData: [],
        activeButton: false,
        all_packages:[],
        formInput: {
            type_service: '',
            poll: {
                pickup: {
                    value: '',
                    text: ''
                },
                return: {
                    value: '',
                    text: ''
                }
            },
            city: {
                cityId: '',
                value: '',
                text: '',
            },
            address: {
                value: '',
                text: '',
                geometry: {}
            },
            select_package: '',
            date: {
                startDate: '',
                endDate: '',
                time: '',
            },
        },
        formDisplay: {
            typeService: lang.selectType[lang.default],
            typePickup: lang.pickupMethod[lang.default],
            pickupCityAndAddress: lang.selectYourCity[lang.default],
            package: lang.selectPackage[lang.default],
            dateAndTime: lang.selectDateTime[lang.default],
        },
        formValue: {
            typeService: null,
            pickupCityAndAddress: null,
            package: null,
        }
    }

    componentDidMount = () => {
        this.props.getPackageCarRental();
        if(!this.isInputOnTop()) {
            
            let CarRentalFormInput = localStorageDecrypt('_fi');
            let CarRentalFormDisplay = localStorage.getItem('CarRentalFormDisplay');
            let CityCoverage = localStorage.getItem('cityCoverage');
            if(CityCoverage !== null) {
                this.setState({
                    cityData: JSON.parse(CityCoverage)
                });
            }
            if(CarRentalFormInput !== null && CarRentalFormDisplay !== null) {
                this.setState({
                    formInput: JSON.parse(CarRentalFormInput),
                    formDisplay: JSON.parse(CarRentalFormDisplay)
                });
            }
            if(this.props.selectedproductservicetype) {
                let formValue = {...this.state.formValue};
                formValue['typeService'] = this.props.selectedproductservicetype.product_service;
                this.setState({
                    formValue: formValue
                });
            } else {
                let typeService = localStorageDecrypt('_ts', "object");
                if(typeService !== null) {
                    let formValue = {...this.state.formValue};
                    formValue['typeService'] = typeService;
                    this.setState({
                        formValue: formValue
                    })
                }
            }
        }
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.packagecarrentalsuccess !== prevProps.packagecarrentalsuccess) {
            return { package: this.props.packagecarrentalsuccess }
        }
        if(this.props.selectedproductservicetype !== prevProps.selectedproductservicetype) {
            if(this.props.selectedproductservicetype.MsProductId === "PRD0006") {
                return { selectedServiceType: this.props.selectedproductservicetype }   
            }
        }
        if(this.props.citycoveragecarrentalsuccess !== prevProps.citycoveragecarrentalsuccess) {
            return { cityCoverage: this.props.citycoveragecarrentalsuccess }
        }
        if(this.props.address !== prevProps.address) {
            return { address: this.props.address }
        }
        if(this.props.place !== prevProps.place) {
            return { place: this.props.place }
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.package !== undefined) {
                let formValue = {...this.state.formValue};
                formValue['package'] = snapshot.package;
                this.setState({
                    all_packages:snapshot.package,
                    formValue: formValue
                });
            }
            if(snapshot.selectedServiceType !== undefined) {
                let formValue = {...this.state.formValue};
                formValue['typeService'] = snapshot.selectedServiceType.product_service;
                localStorageEncrypt('_ts', JSON.stringify(snapshot.selectedServiceType.product_service));
                this.setState({
                    formValue: formValue
                });
            }
            if(snapshot.cityCoverage !== undefined) {
                // let newCity = _.map(snapshot.cityCoverage, val => { 
                //     return {
                //         id: val.BranchId, 
                //         name: _.startCase(_.lowerCase(val.MsCityName)),
                //         cityId: val.MsCityId
                //     } 
                // });
                // localStorage.setItem('cityCoverage', JSON.stringify(newCity));
                this.setState({
                    cityData: snapshot.cityCoverage
                });
            }
            if(snapshot.address !== undefined) {
                if(snapshot.address.status === "OK") {
                    let addressData = _.map(snapshot.address.predictions, v => {
                        return { id: v.place_id, name: v.description }
                    });
                    this.setState({
                        addressData: addressData
                    })
                }
            }
            if(snapshot.place !== undefined) {
                let formInput = { ...this.state.formInput }
                formInput.address.geometry = snapshot.place;
                this.setState({
                    formInput: formInput
                });
            }
        }
        if(this.props.mainSlideActive !== prevProps.mainSlideActive) {
            if(this.props.mainSlideActive !== 1) {
                this.setState({
                    formInput: {
                        type_service: '',
                        poll: {
                            pickup: {
                                value: '',
                                text: ''
                            },
                            return: {
                                value: '',
                                text: ''
                            }
                        },
                        city: {
                            value: '',
                            text: '',
                        },
                        address: {
                            value: '',
                            text: '',
                            geometry: {}
                        },
                        select_package: '',
                        date: {
                            startDate: '',
                            endDate: '',
                            time: '',
                        },
                    },
                    formDisplay: {
                        typeService: lang.selectType[lang.default],
                        pickupCityAndAddress: lang.selectYourCity[lang.default],
                        package: lang.selectPackage[lang.default],
                        dateAndTime: lang.selectDateTime[lang.default],
                    }
                });
            }
        }
    }

    // handle click
    handleClick = (e) => {
        const { inputShow } = this.state;
        if (e.type !== 'mousedown') {
            if (inputShow !== e.currentTarget.dataset.id && e.currentTarget.dataset.id !== undefined) {
                this.setState({ inputShow: parseInt(e.currentTarget.dataset.id) });
                this.props.hasChanged(true, this.handleClick, true, false);
            } else {
                this.setState({ inputShow: 0 });
                this.props.hasChanged(false, this.handleClick, false, false);
            }
        } else {
            this.setState({ inputShow: 0 });
            this.props.hasChanged(false, this.handleClick, false, false);
        }
    }

    // click input radio
    handleClickInputRadio = (values) => {
        let updateFormDisplay = {...this.state.formDisplay};
        updateFormDisplay[values.target] = values.text;

        let updateFormInput = {...this.state.formInput};
        updateFormInput[values.name] = values.value;
        if(values.name === "type_service") {
            let v = JSON.parse(values.value);
            let businessUnitId = this.props.businessUnitId ? this.props.businessUnitId : localStorageDecrypt('_bu');
            this.props.getCityCoverageCarRental(["CID-999999", businessUnitId, v.MsProductId]);
        }

        let inputShow = 0;
        let type_service = JSON.parse(values.value);
        if((values.name === "type_service") && (type_service.ProductServiceId === 'PSV0001')) {
            updateFormInput['Duration'] ='';
            updateFormDisplay['package'] =lang.rentalPackage[lang.default];
            let package_ = this.state.all_packages;
            let filtered_pkg;
            filtered_pkg = _.filter(package_, { ServiceType: 'PSV0001' });
            let formValue = {...this.state.formValue};
            formValue['package'] = filtered_pkg;
            this.setState({
                formValue: formValue
            });
        }
        if((values.name === "type_service") && (type_service.ProductServiceId !== 'PSV0001')) {
            updateFormInput['Duration'] ='';
            updateFormDisplay['package'] =lang.rentalPackage[lang.default];
            let package_ = this.state.all_packages;
            let filtered_pkg = package_.filter(t=>{
                return t.ServiceType != 'PSV0001';
            });
            let formValue = {...this.state.formValue};
            formValue['package'] = filtered_pkg;
            this.setState({
                formValue: formValue
            });
	        updateFormInput.poll.pickup.value = 0;
            updateFormInput.poll.return.value = 0;
            inputShow = 3;
        } else {
		    if ( this.isInputOnTop() ) {
            	inputShow = values.next;
        	}
	    }

        this.setState({
            inputShow: inputShow,
            formInput: updateFormInput,
            formDisplay: updateFormDisplay,
        }, () => {
            localStorageEncrypt('_fi', JSON.stringify(this.state.formInput));
            localStorage.setItem('CarRentalFormDisplay', JSON.stringify(this.state.formDisplay));
            this.checkAllValue('select-package')
        });
    }

    // click input radio with child
    handleClickInputRadioWithChild = (values) => {
        let updateFormDisplay = { ...this.state.formDisplay };
        let updateFormInput = { ...this.state.formInput };

        updateFormInput.poll[values.name].value = values.value;
        updateFormInput.poll[values.name].text = values.text;
        if (values.display !== "append") {
            updateFormDisplay[values.target] = values.text;
            document.querySelectorAll('.jsDeliverCar')[0].classList.remove('active');
            document.querySelectorAll('.jsReturnCar')[0].classList.add('active');
        }
        else {
            updateFormDisplay[values.target] = updateFormDisplay[values.target] + ', ' + values.text;
        }

        if (this.isInputOnTop() && values.display === 'append') {
            let inputShow = values.next;
            this.setState({ inputShow: inputShow });
        }

        this.setState({
            formInput: updateFormInput,
            formDisplay: updateFormDisplay,
        }, this.checkAllValue);
    }

    // filter list
    handleFilterList = (data, e) => {
        const { citycoveragecarrentalsuccess } = this.props;
        let filterData = '';
        let filterState = '';
        let updateFilterData = {};

        if (data === 'city') {
            let cityCoverage = citycoveragecarrentalsuccess !== null ? citycoveragecarrentalsuccess : JSON.parse(localStorage.getItem('cityCoverage'));
            filterData = _.map(cityCoverage, val => {
                // console.log(val)
                return {
                    id: val.BranchId || val.id,
                    cityId: val.cityId,
                    name: _.startCase(_.lowerCase(val.MsCityName)) || _.startCase(_.lowerCase(val.name))
                }
            });
            filterState = 'cityData';

            filterData = filterData.filter(value => {
                return value.name.toLowerCase().search(e.target.value.toLowerCase()) !== -1;
            });
    
            updateFilterData[filterState] = filterData;
            this.setState(updateFilterData);
            
        } else  if (data === 'address') {
            if(e.target.value.length >= 3) {
                this.props.getAddressFromGM(`${e.target.value}+${this.state.formInput.city.text}`);
            }
        }
    }

    // select item
    handleSelectItem = (data, e) => {
        if (data === 'city') {
            let cityData = _.filter(this.state.cityData, { name: e.currentTarget.innerText });
            
            // const getText = e.currentTarget.innerText;
            // const getId = e.currentTarget.dataset.id;

            const getText = cityData[0].name;
            const getId = cityData[0].id;
            const getCityId = cityData[0].cityId;

            document.querySelectorAll('.jsSelectCityCarRent')[0].classList.remove('active');
            document.querySelectorAll('.jsFindAddressCarRent')[0].classList.add('active');

            const updateFormInput = {...this.state.formInput};
            updateFormInput.city.value = getId;
            updateFormInput.city.text = getText;
            updateFormInput.city.cityId = getCityId;

            this.props.getAddressFromGM(`+${getText}`);

            this.setState({
                formInput: updateFormInput,
            }, this.checkAllValue('select-city') );

        } else  if (data === 'address') {
            const getText = e.currentTarget.innerText;
            const getId = e.currentTarget.dataset.id;

            document.querySelectorAll('.jsFindAddressCarRent')[0].classList.remove('active');
            document.querySelectorAll('.jsSelectCityCarRent')[0].classList.add('active');

            const updateFormInput = {...this.state.formInput};
            updateFormInput.address.value = getId;
            updateFormInput.address.text = getText;

            const updateFormDisplay = {...this.state.formDisplay};
            updateFormDisplay.pickupCityAndAddress = getText;

            let inputShow = 0;
            if (this.isInputOnTop()) {
                inputShow = 4;
            }
            this.props.getPlaceDetailFromGM(getId);
            this.setState({
                inputShow: inputShow,
                formInput: updateFormInput,
                formDisplay: updateFormDisplay,
            }, () => {
                localStorageEncrypt('_fi', JSON.stringify(this.state.formInput));
                localStorage.setItem('CarRentalFormDisplay', JSON.stringify(this.state.formDisplay));
                this.checkAllValue('select-address')
            });
        }
    }

    // back to select city
    handleBackSelectCity = () => {
        document.querySelectorAll('.jsFindAddressCarRent')[0].classList.remove('active');
        document.querySelectorAll('.jsSelectCityCarRent')[0].classList.add('active');
    }

    // back to select deliver car
    handleBackSelectDeliverCar = () => {
        document.querySelectorAll('.jsReturnCar')[0].classList.remove('active');
        document.querySelectorAll('.jsDeliverCar')[0].classList.add('active');
    }

    // get value date form
    getValueDateForm = (value, status) => {

        let setDateTime = '';
        const { startDate, endDate, selectDate, tmpEndDate } = value;
        const updateFormInput = {...this.state.formInput};
        const updateFormDisplay = {...this.state.formDisplay};
        let duration = this.state.formInput.select_package;

        switch(status) {
            case 'select-first-date':
                setDateTime = moment(startDate).format('ddd, DD MMM YYYY');
                updateFormInput.date.startDate = moment(startDate).format('YYYY-MM-DD');
                updateFormInput.date.endDate = '';
                updateFormInput.date.time = '';
                break;
            case 'select-end-date':
                setDateTime = moment(startDate).format('ddd, DD MMM YYYY') + '<span class="arrow-right"></span>' + moment(tmpEndDate).format('ddd, DD MMM YYYY');
                updateFormInput.date.startDate = moment(startDate).format('YYYY-MM-DD');
                updateFormInput.date.endDate = moment(endDate).format('YYYY-MM-DD') ;
                updateFormInput.date.time = '';
                break;
            case 'select-time':
                setDateTime = moment(startDate).format('ddd, DD MMM YYYY') + ' '+ moment(selectDate).format('HH:mm') +' <span class="arrow-right"></span>' + moment(tmpEndDate).format('ddd, DD MMM YYYY HH:mm') ;
                updateFormInput.date.startDate = moment(startDate).format('YYYY-MM-DD');
                updateFormInput.date.endDate = moment(endDate).format('YYYY-MM-DD');
                updateFormInput.date.time = moment(selectDate).format('HH:mm');
                break;
            default:
                break;
        }

        updateFormDisplay.dateAndTime = setDateTime;

        if ( this.isInputOnTop() ) {
            this.setState({
                formInput: updateFormInput,
                formDisplay: updateFormDisplay,
            }, this.checkAllValue);
        } else {
            if ( status === 'select-time' && window.innerWidth > 767 ) {
                this.setState({
                    inputShow: 0,
                    formInput: updateFormInput,
                    formDisplay: updateFormDisplay,
                }, () => {
                    localStorageEncrypt('_fi', JSON.stringify(this.state.formInput));
                    localStorage.setItem('CarRentalFormDisplay', JSON.stringify(this.state.formDisplay));
                    this.checkAllValue('select-time') 
                });
            } else {
                this.setState({
                    formInput: updateFormInput,
                    formDisplay: updateFormDisplay,
                }, this.checkAllValue('select-date'));
                
            }
        }

    }

    // check all value
    checkAllValue = (dataChange) => {
        let whiteSpace = /^ *$/;
        const { inputOnTop } = this.props;
        if ( inputOnTop ) {
            if (
                !whiteSpace.test(this.state.formInput.type_service) &&
                !whiteSpace.test(this.state.formInput.poll.pickup.value) &&
                !whiteSpace.test(this.state.formInput.poll.return.value) &&
                !whiteSpace.test(this.state.formInput.city.value) &&
                !whiteSpace.test(this.state.formInput.address.value) &&
                !whiteSpace.test(this.state.formInput.select_package) &&
                !whiteSpace.test(this.state.formInput.date.startDate) &&
                !whiteSpace.test(this.state.formInput.date.endDate) &&
                !whiteSpace.test(this.state.formInput.date.time)
            ) {
                this.setState({ activeButton: true });
            } else {
                this.setState({ activeButton: false });
            }
        } else {
            this.setState({ activeButton: true });
            let v = JSON.parse(this.state.formInput.type_service);
            let duration = this.state.formInput.select_package;
            let CarRentalForm = {
                BusinessUnitId: localStorageDecrypt('_bu'),
                BranchId: this.state.formInput.city.value,
                CityId: this.state.formInput.city.cityId,
                StartDate: moment(`${this.state.formInput.date.startDate} ${this.state.formInput.date.time}`).toISOString(),
                EndDate: moment(`${this.state.formInput.date.endDate} ${this.state.formInput.date.time}`).add(duration,'hours').toISOString(),
                IsWithDriver: /^[a-z|A-Z|0-9]+[^I]\s?(1){1}$/.test(v.ProductServiceId) ? 0 : 1, // 0 = self driver 1 = pakai driver
                RentalDuration: parseInt(this.state.formInput.select_package),
                RentalPackage: parseInt(this.state.formInput.select_package),
                Uom: 'hr',
                ServiceTypeId: RENTAL_TIMEBASE,  // Rental Time Based
                ValidateAttribute: 1,
                ValidateContract: 1
            };
            this.props.onSubmit(CarRentalForm, 'onChange', dataChange);
            // localStorageEncrypt('_fi', JSON.stringify(formInput));
            // localStorage.setItem('CarRentalFormDisplay', JSON.stringify(formDisplay));
            // this.props.onSubmit(this.state.formInput, 'onChange', dataChange);
        }
    }

    // search button
    handleSearchButton = (values) => {
        const { activeButton, formInput, formDisplay } = this.state;
        if (activeButton) {
            let duration = this.state.formInput.select_package;
            // Values and name form input
            let v = JSON.parse(formInput.type_service);
            let CarRentalForm = {
                BusinessUnitId: this.props.businessUnitId,
                BranchId: formInput.city.value,
                CityId: formInput.city.cityId,
                StartDate: moment(`${formInput.date.startDate} ${formInput.date.time}`).toISOString(),
                EndDate: moment(`${formInput.date.endDate} ${formInput.date.time}`).toISOString(),
                IsWithDriver: /^[a-z|A-Z|0-9]+[^I]\s?(1){1}$/.test(v.ProductServiceId) ? 0 : 1, // 0 = self driver 1 = pakai driver
                RentalDuration: parseInt(formInput.select_package),
                RentalPackage: parseInt(formInput.select_package),
                Uom: 'hr',
                ServiceTypeId: RENTAL_TIMEBASE,  // Rental Time Based
                ValidateAttribute: 1,
                ValidateContract: 1
            };
            localStorageEncrypt('_fi', JSON.stringify(formInput));
            localStorage.setItem('CarRentalFormDisplay', JSON.stringify(formDisplay));
            this.props.onSubmit(CarRentalForm, 'onClick', 'values-completed');
            this.setState({ activeButton: false })
        }
    }

    isInputOnTop = () => {
        const { inputOnTop } = this.props;
        return inputOnTop;
    }

    // render
    render() {

        const {
            handleClick,
            handleSearchButton,
            props: { mainSlideActive, inputOnTop },
            state: { inputShow, formInput, formDisplay, activeButton, formValue }
        } = this;
        
        let type_service = formInput.type_service !== '' ? JSON.parse(formInput.type_service) : {};

        // rent form
        let rentForm = 'o-rent-form';
        if ( mainSlideActive === 1 ) {
            rentForm += ' active';
        }

        if ( inputOnTop ) {
            rentForm += ' input-on-top';
        }

        let inputForm1 = 'rent-form-item primary-light ';
        let inputForm2 = 'rent-form-item primary-light ';
        let inputForm3 = 'rent-form-item primary-dark width-50 ';
        let inputForm4 = 'rent-form-item primary-darkest width-25 ';
        let inputForm5 = 'rent-form-item primary-extremely-darkest ' + (type_service.ProductServiceId === "PSV0001" ? 'width-95 ' : 'width-85 ');
        let activeOutSide = (inputShow !== 0 ? true : false);
        switch (inputShow) {
            case 1:
                inputForm1 += 'show';
                break;
            case 2:
                inputForm2 += 'show';
                break;
            case 3:
                inputForm3 += 'show';
                break;
            case 4:
                inputForm4 += 'show';
                break;
            case 5:
                inputForm5 += 'show';
                break;
            default:
                break;
        }


        // Calendar Data
        let rentCarCalendarData = {
            titleCalendar: lang.calendar[lang.default],
            titleTime: lang.pickupTime[lang.default],
            timeFormat: 'HH:mm',
            intervalTime: 30,
            onClickButton: handleClick,
            textButton: 'Done',
            sendValue: this.getValueDateForm,
        }
        
        return (

            <Fragment>
                <FormWrapper inputOnTop={ inputOnTop } className={ rentForm } onOutsideClick={ handleClick } active={ activeOutSide }>

                    {/* Form item */}
                    <div className={ inputForm1 }>
                        {/* Form input */}
                        <div className="rent-form-input">
                            <MobileTopHeader title={lang.typeOfService[lang.default]} onClick={handleClick}/>
                            { inputOnTop && 
                            <div className="input-group-radio">
                                { formValue.typeService && formValue.typeService.map((value, index) => (
                                    <RadioFormInput
                                        key={index}
                                        label={value.ProductServiceName === 'Chaffeur' ? lang.chauffeur[lang.default] : value.ProductServiceName}
                                        // label={value.ProductServiceName}
                                        // value="1"
                                        value={JSON.stringify({ MsProductId: value.MsProductId, ProductServiceId: value.ProductServiceId })}
                                        name={Object.keys(formInput)[0]}
                                        dataNext="2"
                                        target="typeService"
                                        onClick={this.handleClickInputRadio}
                                    />
                                ))}
                            </div> }
                      </div>
                      {/* Form Display */}
                      <FormDisplay id={1} label={lang.typeOfService[lang.default]} value={formDisplay.typeService} onClick={handleClick}/>
                    </div>

		            {type_service.ProductServiceId === "PSV0001" ?
                        <div className={inputForm2}>
                            {/* Form input */}
                            <div className="rent-form-input">
                                <MobileTopHeader title={lang.typeOfPickup[lang.default]} onClick={handleClick} />
                                { inputOnTop && 
                                <div className="input-group-radio hasChild jsDeliverCar active">
                                    <div className="input-radio-label">
                                        <div className="label">{lang.howTheCarDeliver[lang.default]}</div>
                                    </div>
                                    {/* <RadioFormInput
                                        label="Deliver to location"
                                        subLabel="(Extra Charge)"
                                        value="1"
                                        name="pickup"
                                        dataNext="0"
                                        target="typePickup"
                                        onClick={this.handleClickInputRadioWithChild}
                                        display="replace"
                                    /> */}
                                    <RadioFormInput
                                        label={lang.pickupAtpool[lang.default]}
                                        value="2"
                                        name="pickup"
                                        dataNext="0"
                                        target="typePickup"
                                        onClick={this.handleClickInputRadioWithChild}
                                        display="replace"
                                    />
                                    <small>*Tersedia Add-On Expedisi</small>
                                </div> }
                                { inputOnTop && 
                                <div className="input-group-radio hasChild jsReturnCar">
                                    <div className="back-form" onClick={this.handleBackSelectDeliverCar}>Back</div>
                                    <div className="input-radio-label">
                                        <div className="label">{lang.howTheCarReturn[lang.default]}</div>
                                    </div>
                                    {/* <RadioFormInput
                                        label="Pickup at location"
                                        value="1"
                                        name="return"
                                        dataNext="3"
                                        target="typePickup"
                                        onClick={this.handleClickInputRadioWithChild}
                                        display="append"
                                    /> */}
                                    <RadioFormInput
                                        label={lang.deliverToPool[lang.default]}
                                        value="2"
                                        name="return"
                                        dataNext="3"
                                        target="typePickup"
                                        onClick={this.handleClickInputRadioWithChild}
                                        display="append"
                                    />
                                    <small>*Tersedia Add-On Expedisi</small>
                                </div>
                                }
                            </div>
                            {/* Form Display */}
                            <FormDisplay id={2} label={lang.typeOfPickup[lang.default]} value={formDisplay.typePickup} onClick={handleClick} />
                        </div>
                        : ''}

                    {/* Form item */}
                    <div className={ inputForm3 }>
                      {/* form input */}
                      <div className="rent-form-input search-input">
                        <MobileTopHeader title={lang.pickupCarRental[lang.default]} onClick={handleClick}/>
                        {/* City */}
                        <ListSearchFormInput
                            label={lang.selectYourCity[lang.default]}
                            className="jsSelectCityCarRent"
                            name={Object.keys(formInput)[1]}
                            data={this.state.cityData}
                            onChangeInput={(e) => this.handleFilterList('city', e)}
                            onClickItem={(e) => this.handleSelectItem('city', e)}
                            activeBlock
                        />
                        {/* Address */}
                        <ListSearchFormInput
                            label={lang.findYourAddress[lang.default]}
                            className="jsFindAddressCarRent"
                            name={Object.keys(formInput)[2]}
                            data={this.state.addressData}
                            onChangeInput={(e) => this.handleFilterList('address', e)}
                            onClickItem={(e) => this.handleSelectItem('address', e)}
                            onClickBack={this.handleBackSelectCity}
                            backSearch
                        />
                      </div>
                      {/* Form Display */}
                      <FormDisplay id={3} label={lang.pickupCarRental[lang.default]} value={formDisplay.pickupCityAndAddress} onClick={handleClick} search/>
                    </div>

                    {/* Form item */}
                    <div className={ inputForm4 }>
                      {/* Form input */}
                      <div className="rent-form-input">
                        <MobileTopHeader title={lang.rentalPackage[lang.default]} onClick={handleClick}/>
                        <div className="input-group-radio">
                            { formValue.package && formValue.package.map((value, index) => (
                                <RadioFormInput
                                    key={index}
                                    label={value.Name}
                                    value={value.Duration.toString()}
                                    name={Object.keys(formInput)[4]}
                                    dataNext="5"
                                    target="package"
                                    onClick={this.handleClickInputRadio}
                                />
                            ))}
                        </div>
                      </div>
                      {/* Form Display */}
                      <FormDisplay id={4} label={lang.rentalPackage[lang.default]} value={formDisplay.package} onClick={handleClick}/>
                    </div>

                    {/* Form item */}
                    <div className={ inputForm5 }>
                      {/* Form input */}
                        <div className="rent-form-input calendar-input">
                            <MobileTopHeader title={lang.dateAndTime[lang.default]} onClick={handleClick}/>
                            <DateFormInput
                                name={Object.keys(formInput)[4]}
                                titleCalendar={ rentCarCalendarData.titleCalendar }
                                titleTime={ rentCarCalendarData.titleTime }
                                timeFormat={ rentCarCalendarData.timeFormat }
                                intervalTime={ rentCarCalendarData.intervalTime }
                                onClickButton={ rentCarCalendarData.onClickButton }
                                sendValue={ rentCarCalendarData.sendValue }
                                textButton={ rentCarCalendarData.textButton }
                                packageSelected={ formInput.select_package }
                                // currentStartDate={formInput.date.startDate}
                                // currentEndDate={formInput.date.endDate}
                                // currentPickUpTime={formInput.date.time}
                            />
                        </div>
                        {/* Form Display */}
                        <FormDisplay id={5} label={lang.dateAndTime[lang.default]} value={formDisplay.dateAndTime} onClick={handleClick}/>
                    </div>
                    
                    <ButtonForm 
                        text={this.props.loading ? 
                            <PulseLoader
                                sizeUnit={"px"}
                                size={7}
                                color={'#ffffff'}
                                loading={this.props.loading}
                            /> : lang.search[lang.default]}
                        onClick={ handleSearchButton } 
                        active={ activeButton }/>

                </FormWrapper>
            </Fragment>

        );
    }
  
}

CarRentForm.defaultProps = {
    mainSlideActive: 1,
    inputShow: 0,
    inputOnTop: false,
    onSubmit: () => {},
    hasChanged: () => {},
};

// props types
CarRentForm.propTypes = {
  mainSlideActive: PropTypes.number,
  inputShow: PropTypes.number,
  inputOnTop: PropTypes.bool,
  onSubmit: PropTypes.func,
  hasChanged: PropTypes.func,
};

const mapStateToProps = ({ carrental, product, addressgm }) => {
    const { packagecarrentalsuccess, packagecarrentalfailure, citycoveragecarrentalsuccess } = carrental;
    const { selectedproductservicetype } = product;
    const { address, geometry, place } = addressgm;
    return { 
        packagecarrentalsuccess, 
        packagecarrentalfailure, 
        citycoveragecarrentalsuccess, 
        selectedproductservicetype,
        address,
        geometry, 
        place
    };
};

export default connect(mapStateToProps, {
    getPackageCarRental,
    getCityCoverageCarRental,
    getAddressFromGM,
    getPlaceDetailFromGM, 
    getFindGeometryFromGM
})(CarRentForm);
