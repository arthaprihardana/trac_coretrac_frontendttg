// core
import React from 'react';
import PropTypes from 'prop-types';

// Component
import { OutsideClick } from 'organisms';

const FormWrapper = ({ inputOnTop, className, onOutsideClick, active, children }) => {
    let elementRender = '';
    if (inputOnTop) {
        elementRender = (
            <div className={className}>
                {children}
            </div>
        )
    } else {
        elementRender = (
            <OutsideClick className={className} onOutsideClick={onOutsideClick} active={active}>
                {children}
            </OutsideClick>
        )
    }

    return elementRender;
}

FormWrapper.defaultProps = {
    inputOnTop: false,
    className: '',
    active: false,
    onOutsideClick: () => { console.log('clicked!') },
};

// props types
FormWrapper.propTypes = {
    inputOnTop: PropTypes.bool,
    className: PropTypes.string,
    active: PropTypes.bool,
    onOutsideClick: PropTypes.func,
};

export default FormWrapper;
