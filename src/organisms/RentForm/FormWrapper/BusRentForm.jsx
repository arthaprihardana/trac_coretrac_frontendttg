/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 01:10:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 16:21:25
 */
// core
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { localStorageEncrypt, localStorageDecrypt } from "helpers";

// plugin component
import moment from 'moment';
import "moment/locale/id";
import { PulseLoader } from 'react-spinners';

// actions
import { getCityCoverageCarRental, getAddressFromGM, getDistanceMatrixBus, getPlaceDetailFromGM, getDurationAirportTransfer, getDistanceBus, getDistanceMatrixFromGM, setBranchFromBus } from "actions";

// form display
import FormDisplay from '../FormDisplay';
import MobileTopHeader from '../FormDisplay/MobileTopHeader';

// form input
import ListSearchFormInput from '../FormInput/ListSearchFormInput';
import DateFormInput from '../FormInput/DateFormInput';
import ButtonForm from '../FormInput/ButtonForm';
import { UAS_TIMEBASE } from '../../../constant';

import lang from "../../../assets/data-master/language";

// data city
// import cityDataJSON from '../../../assets/data-dummy/city.json';
// import addressDataJSON from '../../../assets/data-dummy/address.json';
// import destinationDataJSON from '../../../assets/data-dummy/address.json';

class BusRentForm extends PureComponent {
    // state
    state = {
        startDate: moment().format('YYYY-MM-DD HH:MM'),
        endDate: moment().format('YYYY-MM-DD HH:MM'),
        inputShow: 0,
        cityData: [],
        addressData: [],
        destinationData: [],
        activeButton: false,
        formInput: {
            city: {
                value: '',
                text: '',
            },
            address: {
                value: '',
                text: '',
                geometry: {}
            },
            destination: {
                value: '',
                text: '',
                geometry: {}
            },
            passengers: 0,
            date: {
                startDate: '',
                endDate: '',
                time: '',
            },
        },
        formDisplay: {
            pickupCityAndAddress: lang.selectYourCity[lang.default],
            destination: lang.selectArea[lang.default],
            passengers: lang.inputAmount[lang.default],
            dateAndTime: lang.selectDateTime[lang.default],
        },
        isDestination: false,
        MsZoneId: '',
        KM: '',
        MinimumDays: 0,
        masterBranch: [],
        nearBranch: []
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.citycoveragecarrentalsuccess !== prevProps.citycoveragecarrentalsuccess) {
            return { cityCoverage: this.props.citycoveragecarrentalsuccess }
        }
        if(this.props.address !== prevProps.address) {
            return { address: this.props.address }
        }
        if(this.props.place !== prevProps.place) {
			return { place: this.props.place }
        }
        if(this.props.distanceBus !== prevProps.distanceBus) {
			return { distanceBus: this.props.distanceBus }
        }
        if(this.props.duration !== prevProps.duration) {
			return { duration: this.props.duration }
        }
        if(this.props.distance !== prevProps.distance) {
            return { distance: this.props.distance }
        }
        return null;   
    }
    componentDidMount = () => {
        if(!this.isInputOnTop()) {

            let BusRentalFormInput = localStorageDecrypt('_fi');
            let BusRentalFormDisplay = localStorage.getItem('BusRentalFormDisplay');
            let CityCoverage = localStorage.getItem('cityCoverage');
            if(CityCoverage !== null) {
                this.setState({
                    cityData: JSON.parse(CityCoverage)
                });
            }
            if(BusRentalFormInput !== null && BusRentalFormDisplay !== null) {
                this.setState({
                    formInput: JSON.parse(BusRentalFormInput),
                    formDisplay: JSON.parse(BusRentalFormDisplay)
                });
            }
            if(this.props.selectedproductservicetype) {
                let formValue = {...this.state.formValue};
                formValue['typeService'] = this.props.selectedproductservicetype.product_service;
                this.setState({
                    formValue: formValue
                });
            } else {
                let typeService = localStorageDecrypt('_ts', "object");
                if(typeService !== null) {
                    let formValue = {...this.state.formValue};
                    formValue['typeService'] = typeService;
                    this.setState({
                        formValue: formValue
                    })
                }
            }
        }
    };
    isInputOnTop = () => {
        const { inputOnTop } = this.props;
        return inputOnTop;
    }
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.cityCoverage !== undefined) {
                // let newCity = _.map(snapshot.cityCoverage, val => { 
                //     return {
                //         id: val.BranchId, 
                //         name: _.startCase(_.lowerCase(val.MsCityName)),
                //         cityId: val.MsCityId
                //     } 
                // });
                // localStorage.setItem('cityCoverage', JSON.stringify(newCity));
                this.setState({
                    cityData: snapshot.cityCoverage
                });
            }
            if(snapshot.address !== undefined) {
                if(snapshot.address.status === "OK") {
                    let addressData = _.map(snapshot.address.predictions, v => {
                        return { id: v.place_id, name: v.description }
                    });
                    this.setState({
                        addressData: addressData,
                        destinationData: addressData
                    })
                }
            }
            if(snapshot.place !== undefined) {
                let formInput = { ...this.state.formInput };
                if(this.state.isDestination) {
                    formInput.destination.geometry = snapshot.place;
                } else {
                    formInput.address.geometry = snapshot.place;
                }
                let BusinessUnitId = this.props.businessUnitId;
                if(BusinessUnitId === "0201") {
                    this.props.getDistanceMatrixFromGM({
                        origins: `${this.props.branch[0].Latitude},${this.props.branch[0].Longitude}|${this.props.branch[1].Latitude},${this.props.branch[1].Longitude}`,
                        destinations: `${snapshot.place.lat},${snapshot.place.lng}`
                    });
                }
				this.setState({
                    formInput: formInput,
                    isDestination: false,
                    masterBranch: this.props.branch
				});
            }
            if(snapshot.distanceBus !== undefined) {
                if(snapshot.distanceBus.status === "OK") {
                    let toKm = snapshot.distanceBus.distance.value / 1000;
                    let roundedDistance = Math.ceil(toKm);

                    localStorage.setItem('distance', roundedDistance.toString())
                    this.setState({
                    	distance: _.ceil(toKm)
                    }, () => {
                    	let BusinessUnitId = this.props.businessUnitId;
                        let MsProductId = this.props.selectedproductservicetype.MsProductId;
                        this.props.getDistanceBus(this.state.distance);
                    	this.props.getDurationAirportTransfer({
                    		BusinessUnitId: BusinessUnitId,
                    		MsProductId: MsProductId,
                    		Distance: this.state.distance
                    	});
                    });   
                } else {
                    alert("Perjalanan Terlalu Jauh")
                }
            }
            if(snapshot.duration !== undefined) {
				this.setState({
					MsZoneId: snapshot.duration.MsZoneId,
                    KM: snapshot.duration.KM,
                    MinimumDays: parseInt(snapshot.duration.MinimumDays)
				});
            }
            if(snapshot.distance !== undefined) {
                console.log("snapshot.distance ", snapshot.distance)
                let BusinessUnitId = localStorageDecrypt('_bu');
                if(BusinessUnitId === "0201") {
                    let joinBranchDistance = _.map(this.props.branch, (v, k) => {
                        v.distance = snapshot.distance[k].elements[0].distance;
                        return v;
                    });
                    let near = _.minBy(joinBranchDistance, o => o.distance.value);
                    this.props.setBranchFromBus(near);
                }
            }
        }
        if(this.props.selectedproductservicetype !== prevProps.selectedproductservicetype) {
            let reg = /bus/i;
            if(reg.test(this.props.selectedproductservicetype.MsProductName)) {
                this.props.getCityCoverageCarRental(["CID-999999", this.props.selectedproductservicetype.BusinessUnitId, this.props.selectedproductservicetype.MsProductId]);
            }
        }
    }

    // handle click
    handleClick = (e) => {
        const { inputShow } = this.state;
        if (inputShow !== e.currentTarget.dataset.id && e.currentTarget.dataset.id !== undefined) {
            document.querySelectorAll('.jsSelectDestination')[0].classList.add('active');
            this.setState({ inputShow: parseInt(e.currentTarget.dataset.id) });
            this.props.hasChanged(true, this.handleClick, true);
        } else {
            this.setState({ inputShow: 0 });
            this.props.hasChanged(false, this.handleClick, false);
        }
    }

    // click input radio
    handleClickInputRadio = (values) => {
        let updateFormDisplay = { ...this.state.formDisplay };
        updateFormDisplay[values.target] = values.text;

        let updateFormInput = { ...this.state.formInput };
        updateFormInput[values.name] = values.value;

        this.setState({
            inputShow: values.next,
            formInput: updateFormInput,
            formDisplay: updateFormDisplay,
        }, this.checkAllValue);
    }

    // filter list
    handleFilterList = (data, e) => {
        const { citycoveragecarrentalsuccess } = this.props;
        let filterData = '';
        let filterState = '';
        let updateFilterData = {};

        if (data === 'city') {
            let cityCoverage = citycoveragecarrentalsuccess !== null ? citycoveragecarrentalsuccess : JSON.parse(localStorage.getItem('cityCoverage'));
            filterData = _.map(cityCoverage, val => {
                return {
                    id: val.BranchId || val.id, 
                    name: _.startCase(_.lowerCase(val.MsCityName)) || _.startCase(_.lowerCase(val.name))
                }
            });
            filterState = 'cityData';

            filterData = filterData.filter(value => {
                return value.name.toLowerCase().search(e.target.value.toLowerCase()) !== -1;
            });
    
            updateFilterData[filterState] = filterData;
            this.setState(updateFilterData);

        } else if (data === 'address') {
            if(e.target.value.length >= 3) {
                this.props.getAddressFromGM(`${e.target.value}+${this.state.formInput.city.text}`);
            }

        } else if (data === 'destination') {
            if(e.target.value.length >= 3) {
                this.props.getAddressFromGM(`${e.target.value}`);
            }
        }
    }

    // select item
    handleSelectItem = (data, e) => {
        if (data === 'city') {
            let cityData = _.filter(this.state.cityData, { name: e.currentTarget.innerText });

            const getText = cityData[0].name;
            const getId = cityData[0].id;
            const getCityId = cityData[0].cityId;

            document.querySelectorAll('.jsSelectCityBusRent')[0].classList.remove('active');
            document.querySelectorAll('.jsFindAddressBusRent')[0].classList.add('active');

            const updateFormInput = { ...this.state.formInput };
            updateFormInput.city.value = getId;
            updateFormInput.city.text = getText;
            updateFormInput.city.cityId = getCityId;

            this.props.getAddressFromGM(`+${getText}`);

            this.setState({
                formInput: updateFormInput,
            }, this.checkAllValue);

        } else if (data === 'address') {
            const getText = e.currentTarget.innerText;
            const getId = e.currentTarget.dataset.id;

            document.querySelectorAll('.jsFindAddressBusRent')[0].classList.remove('active');
            document.querySelectorAll('.jsSelectCityBusRent')[0].classList.add('active');

            const updateFormInput = { ...this.state.formInput };
            updateFormInput.address.value = getId;
            updateFormInput.address.text = getText;

            const updateFormDisplay = { ...this.state.formDisplay };
            updateFormDisplay.pickupCityAndAddress = getText;

            this.props.getPlaceDetailFromGM(getId);
            
            this.setState({
                inputShow: 2,
                formInput: updateFormInput,
                formDisplay: updateFormDisplay,
            }, this.checkAllValue);

        } else if (data === 'destination') {
            const getText = e.currentTarget.innerText;
            const getId = e.currentTarget.dataset.id;

            document.querySelectorAll('.jsSelectDestination')[0].classList.remove('active');

            const updateFormInput = { ...this.state.formInput };
            updateFormInput.destination.value = getId;
            updateFormInput.destination.text = getText;

            const updateFormDisplay = { ...this.state.formDisplay };
            updateFormDisplay.destination = getText;

            this.props.getDistanceMatrixBus({
				origins: this.state.formInput.address.text,
				destinations: this.state.formInput.destination.text
            });
            
            this.setState({
                isDestination: true,
                inputShow: 3,
                formInput: updateFormInput,
                formDisplay: updateFormDisplay,
            }, () => {
                this.checkAllValue();
                this.props.getPlaceDetailFromGM(getId);
            });

        }
    }

    // back to select city
    handleBackSelectCity = () => {
        document.querySelectorAll('.jsFindAddressBusRent')[0].classList.remove('active');
        document.querySelectorAll('.jsSelectCityBusRent')[0].classList.add('active');
    }

    // get value date form
    getValueDateForm = (value, status) => {

        let setDateTime = '';
        const { startDate, endDate, selectDate } = value;
        const updateFormInput = { ...this.state.formInput };
        const updateFormDisplay = { ...this.state.formDisplay };

        switch (status) {
            case 'select-first-date':
                setDateTime = moment(startDate).format('ddd, DD MMM YYYY');
                updateFormInput.date.startDate = moment(startDate).format('YYYY-MM-DD');
                updateFormInput.date.endDate = '';
                updateFormInput.date.time = '';
                break;
            case 'select-end-date':
                setDateTime = moment(startDate).format('ddd, DD MMM YYYY') + '<span class="arrow-right"></span>' + moment(endDate).format('ddd, DD MMM YYYY');
                updateFormInput.date.startDate = moment(startDate).format('YYYY-MM-DD');
                updateFormInput.date.endDate = moment(endDate).format('YYYY-MM-DD');
                updateFormInput.date.time = '';
                break;
            case 'select-time':
                setDateTime = moment(startDate).format('ddd, DD MMM YYYY') + '<span class="arrow-right"></span>' + moment(endDate).format('ddd, DD MMM YYYY') + ' <span class="hour-minutes">' + moment(selectDate).format('HH:mm') + '</span>';
                updateFormInput.date.startDate = moment(startDate).format('YYYY-MM-DD');
                updateFormInput.date.endDate = moment(endDate).format('YYYY-MM-DD');
                updateFormInput.date.time = moment(selectDate).format('HH:mm');
                break;
            default:
                break;
        }

        updateFormDisplay.dateAndTime = setDateTime;

        this.setState({
            formInput: updateFormInput,
            formDisplay: updateFormDisplay,
        }, this.checkAllValue);

    }

    // handle counter passangers
    setValuePassengers = (value) => {
        let newFormInput = { ...this.state.formInput };
        let newFormDisplay = {...this.state.formDisplay};
        newFormInput.passengers = value;
        newFormDisplay.passengers = value.toString();
        this.setState({
            formInput: newFormInput,
            formDisplay: newFormDisplay
        })
    }

    handleCountPassangers = (value) => {
        if (value === 'plus') {
            this.setValuePassengers(parseInt(this.state.formInput.passengers + 1))
        } else {
            let val = parseInt(this.state.formInput.passengers - 1 > 0 ? this.state.formInput.passengers - 1 : 0)
            this.setValuePassengers(val)
        }
    }

    handleChangeCounterPassanger = (e) => {
        let val = e.target.value === '' ? 0 : e.target.value;
        this.setValuePassengers(parseInt(val))
    }

    // check all value
    checkAllValue = () => {
        let whiteSpace = /^ *$/;
        if (
            !whiteSpace.test(this.state.formInput.city.value) &&
            !whiteSpace.test(this.state.formInput.address.value) &&
            !whiteSpace.test(this.state.formInput.destination.value) &&
            !whiteSpace.test(this.state.formInput.passengers) &&
            !whiteSpace.test(this.state.formInput.date.startDate) &&
            !whiteSpace.test(this.state.formInput.date.endDate) &&
            !whiteSpace.test(this.state.formInput.date.time)
        ) {
            this.setState({ activeButton: true });
        } else {
            this.setState({ activeButton: false });
        }
    }

    // search button
    handleSearchButton = () => {

        const { activeButton, formInput, formDisplay } = this.state;

        if (activeButton) {
            // Values and name form input
            let totayDay = moment(this.state.formInput.date.endDate).diff(moment(this.state.formInput.date.startDate), 'days');
            if((totayDay + 1) < this.state.MinimumDays) {
                // alert("Your select date lower than minimum day");
                this.props.validateMinDay(this.state.MinimumDays)
            } else {
                let BusRentForm = {
                    BusinessUnitId: this.props.businessUnitId,
                    // BranchId: formInput.city.value,
                    StartDate: moment(`${formInput.date.startDate} ${formInput.date.time}`).toISOString(),
                    EndDate: moment(`${formInput.date.endDate} ${formInput.date.time}`).toISOString(),
                    IsWithDriver: 1,
                    BranchId: this.props.branchFromBus.BranchId,
                    RentalDuration: (totayDay + 1),
                    // RentalDuration: 1,
                    RentalPackage: 1,
                    Uom: 'days',
                    ServiceTypeId: UAS_TIMEBASE,
                    ValidateAttribute: 1,
                    ValidateContract: 1
                };
                localStorageEncrypt('_fi', JSON.stringify(formInput));
                localStorage.setItem('BusRentalFormDisplay', JSON.stringify(formDisplay));
                this.props.onSubmit(BusRentForm, 'onClick', 'values-completed');
                this.setState({ activeButton: false })
            }
        }
    }

    // render
    render() {

        const {
            handleClick,
            closeModal,
            props: { mainSlideActive, inputOnTop },
            state: { cityData, addressData, destinationData, inputShow, formInput, formDisplay, activeButton }
        } = this;

        // rent form
        let rentForm = 'o-rent-form';
        if (mainSlideActive === 3) {
            rentForm += ' active';
        }

        if (inputOnTop) {
            rentForm += ' input-on-top';
        }

        let inputForm1 = 'rent-form-item primary-light width-50 ';
        let inputForm2 = 'rent-form-item primary-dark width-50 ';
        let inputForm3 = 'rent-form-item primary-darkest width-25 ';
        let inputForm4 = 'rent-form-item primary-extremely-darkest width-85 ';
        switch (inputShow) {
            case 1:
                inputForm1 += 'show';
                break;
            case 2:
                inputForm2 += 'show';
                break;
            case 3:
                inputForm3 += 'show';
                break;
            case 4:
                inputForm4 += 'show';
                break;
            default:
                break;
        }

        // Calendar Data
        let rentCarCalendarData = {
            titleCalendar: lang.calendar[lang.default],
            titleTime: lang.pickupHour[lang.default],
            timeFormat: 'HH:mm',
            intervalTime: 30,
            highlightDates: [
                {
                    date: '2018-11-29',
                    description: '29 Nov : Lorem ipsum day'
                },
                {
                    date: '2018-12-25',
                    description: '25 Okt: Dolor sit day'
                }
            ],
            onClickButton: handleClick,
            textButton: 'Done',
            sendValue: this.getValueDateForm,
        }

        return (
            <div className={rentForm}>

                {/* Form item */}
                <div className={inputForm1}>
                    {/* Form input */}
                    <div className="rent-form-input search-input">
                        <MobileTopHeader title={lang.pickupCityAdress[lang.default]} onClick={handleClick} />
                        {/* City */}
                        <ListSearchFormInput
                            label={lang.pickupTime[lang.default]}
                            className="jsSelectCityBusRent"
                            name={Object.keys(formInput)[0]}
                            data={cityData}
                            onChangeInput={(e) => this.handleFilterList('city', e)}
                            onClickItem={(e) => this.handleSelectItem('city', e)}
                            activeBlock
                        />
                        {/* Address */}
                        <ListSearchFormInput
                            label={lang.findYourAddress[lang.default]}
                            className="jsFindAddressBusRent"
                            name={Object.keys(formInput)[1]}
                            data={addressData}
                            onChangeInput={(e) => this.handleFilterList('address', e)}
                            onClickItem={(e) => this.handleSelectItem('address', e)}
                            onClickBack={this.handleBackSelectCity}
                            backSearch
                        />
                    </div>
                    {/* Form Display */}
                    <FormDisplay id={1} label={lang.pickupCityAdress[lang.default]} value={formDisplay.pickupCityAndAddress} onClick={handleClick} search />
                </div>

                {/* Form item */}
                <div className={inputForm2}>
                    {/* form input */}
                    <div className="rent-form-input search-input">
                        <MobileTopHeader title={lang.destinationBus[lang.default]} onClick={handleClick} />
                        {/* City */}
                        <ListSearchFormInput
                            label={lang.selectYourCity[lang.default]}
                            className="jsSelectDestination"
                            name={Object.keys(formInput)[2]}
                            data={destinationData}
                            onChangeInput={(e) => this.handleFilterList('destination', e)}
                            onClickItem={(e) => this.handleSelectItem('destination', e)}
                            activeBlock
                        />
                    </div>
                    {/* Form Display */}
                    <FormDisplay id={2} label={lang.destinationBus[lang.default]} value={formDisplay.destination} onClick={handleClick} search />
                </div>

                {/* Form item */}
                <div className={inputForm3}>
                    {/* Form input */}
                    <div className="rent-form-input">
                        <MobileTopHeader title="Passengers" onClick={handleClick} />
                        <div className="input-group-radio">
                            {/* <RadioFormInput
                            label="Option 1"
                            value="1"
                            name={ Object.keys(formInput)[3] }
                            dataNext="4"
                            target="passengers"
                            onClick={this.handleClickInputRadio}
                        />
                        <RadioFormInput
                            label="Option 2"
                            value="2"
                            name={Object.keys(formInput)[3]}
                            dataNext="4"
                            target="passengers"
                            onClick={this.handleClickInputRadio}
                        /> */}
                            <input className="form-input-passanger" type="number" value={formInput.passengers.toString()} onChange={this.handleChangeCounterPassanger} />
                            <div className="count-wrapper">
                                <button className="count" onClick={() => this.handleCountPassangers('minus')}>-</button>
                                <button className="count" onClick={() => this.handleCountPassangers('plus')}>+</button>
                            </div>
                        </div>
                    </div>
                    {/* Form Display */}
                    <FormDisplay id={3} label="Passengers" value={formDisplay.passengers} onClick={handleClick} />
                </div>

                {/* Form item */}
                <div className={inputForm4}>
                    {/* Form input */}
                    <div className="rent-form-input calendar-input">
                        <MobileTopHeader title={lang.dateAndTime[lang.default]} onClick={handleClick} />
                        <DateFormInput
                            name={Object.keys(formInput)[4]}
                            titleCalendar={rentCarCalendarData.titleCalendar}
                            titleTime={rentCarCalendarData.titleTime}
                            timeFormat={rentCarCalendarData.timeFormat}
                            intervalTime={rentCarCalendarData.intervalTime}
                            highlightDates={rentCarCalendarData.highlightDates}
                            onClickButton={rentCarCalendarData.onClickButton}
                            sendValue={rentCarCalendarData.sendValue}
                            textButton={rentCarCalendarData.textButton}
                        />
                    </div>
                    {/* Form Display */}
                    <FormDisplay id={4} label={lang.dateAndTime[lang.default]} value={formDisplay.dateAndTime} onClick={handleClick} />
                </div>

                <ButtonForm 
                    text={this.props.loading ?
                        <PulseLoader
                            sizeUnit={"px"}
                            size={7}
                            color={'#ffffff'}
                            loading={this.props.loading}
                        />
                        : lang.search[lang.default]}
                    onClick={this.handleSearchButton} 
                    active={activeButton} />

            </div>
        );
    }
}

BusRentForm.defaultProps = {
    mainSlideActive: 3,
    inputShow: 0,
    inputOnTop: false,
    onSubmit: () => console.log('submited'),
    hasChanged: () => console.log('changed'),
};

// props types
BusRentForm.propTypes = {
    mainSlideActive: PropTypes.number,
    inputShow: PropTypes.number,
    inputOnTop: PropTypes.bool,
    onSubmit: PropTypes.func,
    hasChanged: PropTypes.func,
};

const mapStateToProps = ({ carrental, product, addressgm, airporttransfer, masterbranch }) => {
    const { citycoveragecarrentalsuccess } = carrental;
    const { selectedproductservicetype } = product;
    const { address, geometry, place, distanceBus, distance } = addressgm;
    const { duration } = airporttransfer;
    const { branchFromBus, branch } = masterbranch;
    return { citycoveragecarrentalsuccess, branchFromBus, selectedproductservicetype, address, geometry, place, distanceBus, duration, distance, branch };
}

export default connect(mapStateToProps, {
    getCityCoverageCarRental,
    getAddressFromGM,
    getDistanceMatrixBus,
    getPlaceDetailFromGM,
    getDurationAirportTransfer,
    getDistanceBus,
    getDistanceMatrixFromGM,
    setBranchFromBus
})(BusRentForm);
