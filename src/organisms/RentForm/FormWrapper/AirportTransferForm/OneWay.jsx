/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-11 08:01:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 16:19:50
 */
// core
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import _ from "lodash";

// plugin component
import moment from 'moment';
import "moment/locale/id";

// form display
import FormDisplay from '../../FormDisplay';
import MobileTopHeader from '../../FormDisplay/MobileTopHeader';

//form input
import RadioFormInput from '../../FormInput/RadioFormInput';
import ListSearchFormInput from '../../FormInput/ListSearchFormInput';
import FlightDetailsFormInput from '../../FormInput/FlightDetailsFormInput';
import DateFormInput from '../../FormInput/DateFormInput';

// helpers
import { localStorageEncrypt, localStorageDecrypt } from "helpers";

import lang from "../../../../assets/data-master/language";

// actions
import { getMasterAirport, getMasterAirportCityCoverage, getAddressFromGM, airportActiveInputForm, getDistanceMatrixFromGM, getDurationAirportTransfer, getPlaceDetailFromGM, getFindGeometryFromGM } from "actions";

class OneWay extends PureComponent {
	state = {
		startDate: moment().format('YYYY-MM-DD HH:MM'),
		endDate: moment().format('YYYY-MM-DD HH:MM'),
		inputShow: 0,
		airportData: [],
		airlineData: [],
		cityData: [],
		addressData: [],
		formInput: {
			type_service: 'one-way',
			data: [{
				airport: {
					value: '',
					text: '',
					geometry: ''
				},
				city: {
					cityId: '',
					value: '',
					text: '',
				},
				address: {
					value: '',
					text: '',
					geometry: ''
				},
				airline: {
					name: '',
					number: '',
					value: '',
					text: '',
				},
				date: {
					startDate: '',
					endDate: '',
					time: '',
				}
			}],
		},
		formDisplay: {
			typeService: lang.selectType[lang.default],
			airport: lang.selectAirport[lang.default],
			pickupCityAndAddress: lang.selectYourCity[lang.default],
			flightDetails: lang.enterFlightDetails[lang.default],
			dateAndTime: lang.selectDateTime[lang.default],
		},
		formValue: {
            typeService: null,
            pickupCityAndAddress: null,
            package: null,
		},
		distance: 0,
		MsZoneId: null,
		KM: 0
	}

	componentDidMount = () => {
		this.props.getMasterAirport();
        if(!this.isInputOnTop()) {
            let AirportFormInput = localStorageDecrypt('_fi');
            let AirportTransferFormDisplay = localStorage.getItem('AirportTransferFormDisplay');
            let CityCoverage = localStorage.getItem('cityCoverage');
            if(CityCoverage !== null) {
                this.setState({
                    cityData: JSON.parse(CityCoverage)
                });
            }
            if(AirportFormInput !== null && AirportTransferFormDisplay !== null) {
                this.setState({
                    formInput: JSON.parse(AirportFormInput),
                    formDisplay: JSON.parse(AirportTransferFormDisplay)
                });
            }
            if(this.props.selectedproductservicetype) {
                let formValue = {...this.state.formValue};
                formValue['typeService'] = this.props.selectedproductservicetype.product_service;
                this.setState({
                    formValue: formValue
                });
            } else {
                let typeService = localStorageDecrypt('_ts', "object");
                if(typeService !== null) {
                    let formValue = {...this.state.formValue};
                    formValue['typeService'] = typeService;
                    this.setState({
                        formValue: formValue
                    })
                }
            }
        }
        // const { inputOnTop } = this.props;
        // console.log("inputOnTop =>",inputOnTop)
		// if(!inputOnTop){
        //     let AirportTransferFormDisplay = localStorage.getItem('AirportTransferFormDisplay');
        //     if(AirportTransferFormDisplay !== null) {
        //         this.setState({
        //             formDisplay: JSON.parse(AirportTransferFormDisplay)
        //         });
        //     }
		// }

	};

	getSnapshotBeforeUpdate = (prevProps, prevState) => {
		if(this.props.selectedproductservicetype !== prevProps.selectedproductservicetype) {
			if(this.props.selectedproductservicetype.MsProductId === "PRD0007") {
				return { selectedServiceType: this.props.selectedproductservicetype }
			}
		}
		if(this.props.airportsuccess !== prevProps.airportsuccess) {
			return { airport: this.props.airportsuccess }
		}
		if(this.props.airportcoveragesuccess !== prevProps.airportcoveragesuccess) {
			return { airportCoverage: this.props.airportcoveragesuccess }
		}
		if(this.props.address !== prevProps.address) {
            return { address: this.props.address }
		}
		if(this.props.distance !== prevProps.distance) {
			return { distance: this.props.distance }
		}
		if(this.props.duration !== prevProps.duration) {
			return { duration: this.props.duration }
		}
		if(this.props.geometry !== prevProps.geometry) {
			return { geometry: this.props.geometry }
		}
		if(this.props.place !== prevProps.place) {
			return { place: this.props.place }
		}
		return null;
	}
  
	componentDidUpdate = (prevProps, prevState, snapshot) => {
		if(snapshot !== null) {
			if(snapshot.selectedServiceType !== undefined) {
				let formValue = {...this.state.formValue};
				formValue['typeService'] = snapshot.selectedServiceType.product_service;
				localStorageEncrypt('_ts', JSON.stringify(snapshot.selectedServiceType.product_service));
				this.setState({
				    formValue: formValue
				});
			}
			if(snapshot.airport !== undefined) {
                this.setState({
                    airportData: snapshot.airport
                });
			}
			if(snapshot.airportCoverage !== undefined) {
				this.setState({
					cityData: snapshot.airportCoverage
				});
			}
			if(snapshot.address !== undefined) {
                if(snapshot.address.status === "OK") {
                    let addressData = _.map(snapshot.address.predictions, v => {
                        return { id: v.place_id, name: v.description }
                    });
                    this.setState({
                        addressData: addressData
                    })
                }
			}
			if(snapshot.distance !== undefined) {
				console.log("form_input = >", this.state.formInput);
				let BusinessUnitId = localStorageDecrypt('_bu');
				if(BusinessUnitId === "0104") {
					let toKm = snapshot.distance[0].elements[0].distance.value / 1000;
					this.setState({
						distance: _.round(toKm)
					}, () => {
						// localStorage.setItem('distance', this.state.distance);

						// let typeService = JSON.parse(this.state.formInput.data[0].type_service);
						let typeService = {MsProductId:"PRD0007",ProductServiceId:"PSV0003"};
						this.props.getDurationAirportTransfer({
							BusinessUnitId: BusinessUnitId,
							MsProductId: typeService.MsProductId,
							Distance: this.state.distance
						});
					});
				}
			}
			if(snapshot.duration !== undefined) {
				localStorageEncrypt('_z', JSON.stringify(snapshot.duration));
				this.setState({
					MsZoneId: snapshot.duration.MsZoneId,
					KM: snapshot.duration.KM
				});
			}
			if(snapshot.geometry !== undefined) {
				let formInput = { ...this.state.formInput };
				formInput.data[0].airport.geometry = snapshot.geometry;
				this.setState({
					formInput: formInput
				});
			}
			if(snapshot.place !== undefined) {
				let formInput = { ...this.state.formInput };
				formInput.data[0].address.geometry = snapshot.place;
				this.setState({
					formInput: formInput
				});
			}
		}
		if(this.props.mainSlideActive !== prevProps.mainSlideActive) {
            if(this.props.mainSlideActive !== 2) {
                this.setState({
                    formInput: {
						type_service: 'one-way',
						data: [{
							airport: {
								value: '',
								text: '',
								geometry: ''
							},
							city: {
								cityId: '',
								value: '',
								text: '',
							},
							address: {
								value: '',
								text: '',
								geometry: ''
							},
							airline: {
								name: '',
								number: '',
								value: '',
								text: '',
							},
							date: {
								startDate: '',
								endDate: '',
								time: '',
							},
                            type_service: {MsProductId:"PRD0007",ProductServiceId:"PSV0003"}
						}],
					},
					formDisplay: {
						typeService: lang.selectType[lang.default],
						airport: lang.selectAirport[lang.default],
						pickupCityAndAddress: lang.pickupCityAdress[lang.default],
						flightDetails: lang.enterFlightDetails[lang.default],
						dateAndTime: lang.selectDateTime[lang.default],
					}
                });
            }
		}
		if(this.props.inputShow !== prevProps.inputShow) {
			if(this.props.inputShow) {
				this.setState({
					inputShow: 0
				}, () => this.props.airportActiveInputForm(false))	
			}
		}
	}
  
	// handle click
	handleClick = (e) => {
		const { inputShow } = this.state;
		if (e.type !== 'mousedown') {
			if (inputShow !== e.currentTarget.dataset.id && e.currentTarget.dataset.id !== undefined) {
				document.querySelectorAll('.jsSelectAirport')[0].classList.add('active');
				this.setState({ inputShow: parseInt(e.currentTarget.dataset.id) });
				this.props.hasChanged(e, parseInt(e.currentTarget.dataset.id));
			} else {
				this.setState({ inputShow: 0 });
				this.props.hasChanged(e, this.state.inputShow);
			}
		} else {
			this.setState({ inputShow: 0 });
			this.props.hasChanged(e, this.state.inputShow);
		}
	}

	// handle switch
	handleSwitch = (e) => {
		const { switched } = this.state;
		this.setState({ switched: !switched });
	}

	// click input radio
	handleClickInputRadio = (values) => {
		let v = JSON.parse(values.value);
		switch (v.ProductServiceId) {
			case 'PSV0003':
				let updateFormDisplay = { ...this.state.formDisplay };
				updateFormDisplay[values.target] = values.text;

				let updateFormInput = { ...this.state.formInput };
				updateFormInput.data[0][values.name] = values.value;

				this.setState({
					inputShow: values.next,
					formInput: updateFormInput,
					formDisplay: updateFormDisplay,
				}, this.checkAllValue);
				break;
			case 'PSV0004':

				break;
			case 'PSV0005':

				break;
			default:
				this.props.onTypeServiceChange(values);
				this.setState({ inputShow: 0 });
				break;
		}
	}

	// filter list
	handleFilterList = (data, e) => {
		const { airportsuccess, airportcoveragesuccess } = this.props;
		let filterBy = 'name',
		filterData = '',
		filterState = '',
		updateFilterData = {},
		updateFormDisplay = { ...this.state.formDisplay },
		updateFormInput = { ...this.state.formInput };

		switch (data) {
			case 'airport':
				filterState = 'airportData';
				filterData = _.filter(airportsuccess, value => value.name.toLowerCase().search(e.target.value.toLowerCase()) !== -1 );
				updateFilterData[filterState] = filterData;
				this.setState(updateFilterData);
				break;
			case 'city':
				// filterData = this.state.cityData;
				filterState = 'cityData';
				filterData = _.filter(airportcoveragesuccess, value => value.name.toLowerCase().search(e.target.value.toLowerCase()) !== -1 );
				updateFilterData[filterState] = filterData;
				this.setState(updateFilterData);
				break;
			case 'address':
				if(e.target.value.length >= 3) {
					this.props.getAddressFromGM(`${e.target.value}+${this.state.formInput.data[0].city.text}`);
				}
				break;
			case 'airline':
				filterData = this.state.airlineData;
				filterState = 'airlineData';
				filterBy = 'id';

				//get data filter
				let input = e.target.value.toUpperCase(),
				showData = filterData.filter((value) => {
					return value[filterBy].toLowerCase().search(e.target.value.toLowerCase()) !== -1;
				});
                console.log(e.target);
				updateFormInput.data[0].airline.name = showData.length === 1 && showData[0].id === input ? input : '';
				updateFormInput.data[0].airline.value = showData.length === 1 && showData[0].id === input ? updateFormInput.data[0].airline.name + ' ' + updateFormInput.data[0].airline.number : ' ';
				updateFormInput.data[0].airline.text = updateFormInput.data[0].airline.value;
				updateFormDisplay.flightDetails = updateFormInput.data[0].airline.text !== ' ' ? updateFormInput.data[0].airline.text : 'Enter Flight Details';

				this.setState({
					formInput: updateFormInput,
					formDisplay: updateFormDisplay,
				}, this.checkAllValue);
				break;
			default:
				break;
		}

		if(data !== "address") {
			filterData = filterData.filter((value) => {
				return value[filterBy].toLowerCase().search(e.target.value.toLowerCase()) !== -1;
			});
			
			updateFilterData[filterState] = filterData;
			this.setState(updateFilterData);
		}
	}

	// select item
	handleSelectItem = (data, e) => {
		let updateFormDisplayValue = '',
		updateState;
		const getText = e.currentTarget.innerText,
		getId = e.currentTarget.dataset.id,
		updateFormInput = { ...this.state.formInput };

		switch (data) {
		case 'airport':
			document.querySelectorAll('.jsSelectAirport')[0].classList.remove('active');
			updateFormInput.data[0].airport.value = getId;
			updateFormInput.data[0].airport.text = getText;

			updateFormDisplayValue = { ...this.state.formDisplay };
			updateFormDisplayValue.airport = getText;

			updateState = {
				inputShow: 3,
				formInput: updateFormInput,
				formDisplay: updateFormDisplayValue
			};

			this.props.getFindGeometryFromGM(getText)
			this.props.getMasterAirportCityCoverage({MsAirportCode: getId});
			break;
		case 'city':
			document.querySelectorAll('.jsSelectCityAirportTransfer')[0].classList.remove('active');
			document.querySelectorAll('.jsFindAddressAirportTransfer')[0].classList.add('active');

			let cityData = _.filter(this.state.cityData, { name: getText });
            let getCityId = cityData[0].cityId

			updateFormInput.data[0].city.value = getId;
			updateFormInput.data[0].city.text = getText;
			updateFormInput.data[0].city.cityId = getCityId;

			this.props.getAddressFromGM(`+${getText}`);

			updateState = {
				formInput: updateFormInput
			};
			break;
		case 'address':
			document.querySelectorAll('.jsFindAddressAirportTransfer')[0].classList.remove('active');
			document.querySelectorAll('.jsSelectCityAirportTransfer')[0].classList.add('active');

			updateFormInput.data[0].address.value = getId;
			updateFormInput.data[0].address.text = getText;

			updateFormDisplayValue = { ...this.state.formDisplay };
			updateFormDisplayValue.pickupCityAndAddress = getText;

			this.props.getPlaceDetailFromGM(getId);
			this.props.getDistanceMatrixFromGM({
				origins: this.state.formInput.data[0].airport.text,
				destinations: this.state.formInput.data[0].address.text
			});

			updateState = {
				inputShow: 4,
				formInput: updateFormInput,
				formDisplay: updateFormDisplayValue,
			};
			break;
		case 'airline':
			//document.querySelectorAll('.jsInputFlightDetails')[0].classList.remove('active');

			updateFormInput.data[0].airline.number = e.target.value;
			updateFormInput.data[0].airline.value = updateFormInput.data[0].airline.name + ' ' + updateFormInput.data[0].airline.number;
			updateFormInput.data[0].airline.text = updateFormInput.data[0].airline.name + ' ' + updateFormInput.data[0].airline.number;

			updateFormDisplayValue = { ...this.state.formDisplay };
			updateFormDisplayValue.flightDetails = updateFormInput.data[0].airline.name !== '' ? updateFormInput.data[0].airline.text : 'Enter Flight Details';

			updateState = {
				inputShow: 5,
				formInput: updateFormInput,
				formDisplay: updateFormDisplayValue,
			};
			break;
		default:
			break;
		}

		// const updateFormDisplay = updateFormDisplayValue;
		this.setState(updateState, this.checkAllValue);
	}

	// back to select city
	handleBackSelectCity = () => {
		document.querySelectorAll('.jsFindAddressAirportTransfer')[0].classList.remove('active');
		document.querySelectorAll('.jsSelectCityAirportTransfer')[0].classList.add('active');
	}

	// get value date form
	getValueDateForm = (value, status) => {

		let setDateTime = '';
		const { startDate, endDate, selectDate } = value;
		const updateFormInput = { ...this.state.formInput };
		const updateFormDisplay = { ...this.state.formDisplay };

		switch (status) {
		case 'select-first-date':
			setDateTime = moment(startDate).format('ddd, DD MMM YYYY');
			updateFormInput.data[0].date.startDate = moment(startDate).format('YYYY-MM-DD');
			updateFormInput.data[0].date.endDate = '';
			updateFormInput.data[0].date.time = '';
			break;
		case 'select-end-date':
			setDateTime = moment(startDate).format('ddd, DD MMM YYYY') + '<span class="arrow-right"></span>' + moment(endDate).format('ddd, DD MMM YYYY');
			updateFormInput.data[0].date.startDate = moment(startDate).format('YYYY-MM-DD');
			updateFormInput.data[0].date.endDate = moment(endDate).format('YYYY-MM-DD');
			updateFormInput.data[0].date.time = '';
			break;
		case 'select-time':
			setDateTime = moment(startDate).format('ddd, DD MMM YYYY') + '<span class="arrow-right"></span>' + moment(endDate).format('ddd, DD MMM YYYY') + ' <span class="hour-minutes">' + moment(selectDate).format('HH:mm') + '</span>';
			updateFormInput.data[0].date.startDate = moment(startDate).format('YYYY-MM-DD');
			updateFormInput.data[0].date.endDate = moment(endDate).format('YYYY-MM-DD');
			updateFormInput.data[0].date.time = moment(selectDate).format('HH:mm');
			this.props.changeFormInput({
				formInput: this.state.formInput,
				formDisplay: this.state.formDisplay,
				MsZoneId: this.state.MsZoneId,
				KM: this.state.KM
			})
			break;
		default:
			break;
		}

		updateFormDisplay.dateAndTime = setDateTime;

		this.setState({
			formInput: updateFormInput,
			formDisplay: updateFormDisplay,
		}, this.checkAllValue());

	}
    isInputOnTop = () => {
        const { inputTOP } = this.props;
        return inputTOP;
    }
	// check all value
	checkAllValue = () => {

		let whiteSpace = /^ *$/,
		value = false;
        const { inputOnTop } = this.props;
        if (
            !whiteSpace.test(this.state.formInput.type_service) &&
            !whiteSpace.test(this.state.formInput.data[0].airport.value) &&
            !whiteSpace.test(this.state.formInput.data[0].address.value) &&
            !whiteSpace.test(this.state.formInput.data[0].select_package) &&
            !whiteSpace.test(this.state.formInput.data[0].date.startDate) &&
            !whiteSpace.test(this.state.formInput.data[0].date.endDate) &&
            !whiteSpace.test(this.state.formInput.data[0].date.time)
        ) {
            value = true;
        }
        this.props.onClick(value);
        // if ( inputOnTop ) {
        //     if (
        //         !whiteSpace.test(this.state.formInput.type_service) &&
        //         !whiteSpace.test(this.state.formInput.data[0].airport.value) &&
        //         !whiteSpace.test(this.state.formInput.data[0].address.value) &&
        //         !whiteSpace.test(this.state.formInput.data[0].select_package) &&
        //         !whiteSpace.test(this.state.formInput.data[0].date.startDate) &&
        //         !whiteSpace.test(this.state.formInput.data[0].date.endDate) &&
        //         !whiteSpace.test(this.state.formInput.data[0].date.time)
        //     ) {
        //         value = true;
        //     }
        //     this.props.onClick(value);
        // }else{
        // 	console.log("change_top");
        // 	this.props.onChangeTop(true);
		// }

	}

	// render
	render() {
		const {
			handleClick,
			handleSwitch,
			state: { inputShow, formInput, formDisplay, switched, formValue }
		} = this;

		let classNameSwitch = 'switch-button';

		if (switched) {
			classNameSwitch += ' switched';
		}

		let inputForm1 = 'rent-form-item primary-light width-20 ',
		inputForm2 = 'rent-form-item primary-dark width-50 ',
		inputForm3 = 'rent-form-item primary-darkest width-50 ',
		inputForm4 = 'rent-form-item primary-darkest width-25 ',
		inputForm5 = 'rent-form-item primary-extremely-darkest width-95 ';

		switch (inputShow) {
			case 1:
				inputForm1 += 'show';
				break;
			case 2:
				inputForm2 += 'show';
				break;
			case 3:
				inputForm3 += 'show';
				break;
			case 4:
				inputForm4 += 'show';
				break;
			case 5:
				inputForm5 += 'show';
				break;
			default:
				break;
		}

		// Calendar Data
		let rentCarCalendarData = {
			titleCalendar: 'Calendar',
			titleTime: 'Pick up hour',
			timeFormat: 'HH:mm',
			intervalTime: 30,
			highlightDates: [
				{
				date: '2018-11-29',
				description: '29 Nov : Lorem ipsum day'
				},
				{
				date: '2018-12-25',
				description: '25 Okt: Dolor sit day'
				}
			],
			onClickButton: handleClick,
			textButton: 'Done',
			sendValue: this.getValueDateForm,
		};
		// className = 'rent-form-display';
		return (
			<div className="o-rent-form-wrapper">
			
				{/* Form item */}
				<div className={inputForm1}>
					{/* Form input */}
					<div className="rent-form-input">
						<MobileTopHeader title={lang.selectType[lang.default]} onClick={handleClick} />
						<div className="input-group-radio">
							{ formValue.typeService && _.map(_.filter(formValue.typeService, { ProductServiceId:"PSV0003" }), (value, index) => (
								<RadioFormInput
									key={index}
									label={value.ProductServiceName}
									// value="one-way"
									value={JSON.stringify({ MsProductId: value.MsProductId, ProductServiceId: value.ProductServiceId })}
									name={Object.keys(formInput)[0]}
									dataNext="2"
									target="typeService"
									onClick={this.handleClickInputRadio}
								/>
							))}
						</div>
					</div>
					{/* Form Display */}
					<FormDisplay id={1} label={lang.typeOfService[lang.default]} value={formDisplay.typeService} onClick={handleClick} />
				</div>

				{/* Form item */}
				<div className={inputForm2}>
					{/* form input */}
					<div className="rent-form-input search-input">
						<MobileTopHeader title={lang.fromAirport[lang.default]} onClick={handleClick} />
						{/* Airport */}
						<ListSearchFormInput
						label={lang.selectAirport[lang.default]}
						className="jsSelectAirport"
						name={Object.keys(formInput)[1]}
						data={this.state.airportData}
						onChangeInput={(e) => this.handleFilterList('airport', e)}
						onClickItem={(e) => this.handleSelectItem('airport', e)}
						activeBlock
						/>
					</div>
					{/* Form Display */}
					<FormDisplay id={2} label={lang.fromAirport[lang.default]} value={formDisplay.airport} onClick={handleClick} search />
					<div className={classNameSwitch} onClick={handleSwitch}>Switch</div>
				</div>

				{/* Form item */}
				<div className={inputForm3}>
					{/* Form input */}
					<div className="rent-form-input search-input">
						<MobileTopHeader title={lang.pickupCityAdress[lang.default]} onClick={handleClick} />
						{/* City */}
						<ListSearchFormInput
						label={lang.selectYourCity[lang.default]}
						className="jsSelectCityAirportTransfer"
						name={Object.keys(formInput)[2]}
						data={this.state.cityData}
						onChangeInput={(e) => this.handleFilterList('city', e)}
						onClickItem={(e) => this.handleSelectItem('city', e)}
						activeBlock
						/>
						{/* Address */}
						<ListSearchFormInput
						label={lang.findYourAddress[lang.default]}
						className="jsFindAddressAirportTransfer"
						name={Object.keys(formInput)[3]}
						data={this.state.addressData}
						onChangeInput={(e) => this.handleFilterList('address', e)}
						onClickItem={(e) => this.handleSelectItem('address', e)}
						onClickBack={this.handleBackSelectCity}
						backSearch
						/>
					</div>
					{/* Form Display */}
					<FormDisplay id={3} label={lang.toAreaAdress[lang.default]} value={formDisplay.pickupCityAndAddress} onClick={handleClick} search />
				</div>

				{/* Form item */}
				<div className={inputForm4}>
					{/* Form input */}
					<div className="rent-form-input text-input">
						<MobileTopHeader title={lang.pickupCityAdress[lang.default]} onClick={handleClick} />
						{/* City */}
						<FlightDetailsFormInput
						label={lang.flightDetails[lang.default]}
						subLabel={lang.airlineNameNumber[lang.default]}
						className="jsInputFlightDetails"
						name={Object.keys(formInput)[4]}
						data={this.state.airlineData}
						onChangeInput={(e) => this.handleFilterList('airline', e)}
						onBlur={(e) => this.handleSelectItem('airline', e)}
						activeBlock
						/>
					</div>
					{/* Form Display */}
					<FormDisplay id={4} label={lang.flightDetails[lang.default]} value={formDisplay.flightDetails} onClick={handleClick} />
				</div>

				{/* Form item */}
				<div className={inputForm5}>
					{/* Form input */}
					<div className="rent-form-input calendar-input">
						<MobileTopHeader title={lang.dateAndTime[lang.default]} onClick={handleClick} />
						<DateFormInput
							name={Object.keys(formInput)[5]}
							titleCalendar={rentCarCalendarData.titleCalendar}
							titleTime={rentCarCalendarData.titleTime}
							timeFormat={rentCarCalendarData.timeFormat}
							intervalTime={rentCarCalendarData.intervalTime}
							highlightDates={rentCarCalendarData.highlightDates}
							onClickButton={rentCarCalendarData.onClickButton}
							sendValue={rentCarCalendarData.sendValue}
							textButton={rentCarCalendarData.textButton}
							packageSelected="one-way"
							/>
					</div>
					{/* Form Display */}
					<FormDisplay id={5} label={lang.dateAndTime[lang.default]} value={formDisplay.dateAndTime} onClick={handleClick} />
				</div>
				
			</div>
		);
	}
}

// default props
OneWay.defaultProps = {
	onTypeServiceChange: () => console.log('changed'),
	mainSlideActive: 2,
	inputOnTop: false,
	dataJson: {},
	hasChanged: () => console.log('changed'),
	onClick: () => console.log('click')
};

// props types
OneWay.propTypes = {
	onTypeServiceChange: PropTypes.func,
	mainSlideActive: PropTypes.number,
	inputOnTop: PropTypes.bool,
	dataJson: PropTypes.object,
	hasChanged: PropTypes.func,
	onClick: PropTypes.func
};

const mapStateToProps = ({ product, masterairport, addressgm, airporttransfer }) => {
	const { loading, airportsuccess, airportfailure, airportcoveragesuccess, airportcoveragefailure } = masterairport;
	const { selectedproductservicetype } = product;
	const { address, distance, geometry, place } = addressgm;
	const { inputShow, duration } = airporttransfer;
	return {
		selectedproductservicetype,
		loading, 
		airportsuccess, 
		airportfailure, 
		airportcoveragesuccess, 
		airportcoveragefailure,
		address,
		distance,
		inputShow,
		duration,
		geometry, 
		place
	}
}

export default connect(mapStateToProps, {
	getMasterAirport, 
	getMasterAirportCityCoverage,
	getAddressFromGM,
	airportActiveInputForm,
	getDistanceMatrixFromGM,
	getDurationAirportTransfer,
	getPlaceDetailFromGM, 
	getFindGeometryFromGM
})(OneWay);