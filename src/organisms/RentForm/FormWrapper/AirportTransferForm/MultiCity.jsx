// core
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

// plugin component
import moment from 'moment';

// form display
import FormDisplay from '../../FormDisplay';
import MobileTopHeader from '../../FormDisplay/MobileTopHeader';

//form input
import ListSearchFormInput from '../../FormInput/ListSearchFormInput';
import FlightDetailsFormInput from '../../FormInput/FlightDetailsFormInput';
import DateFormInput from '../../FormInput/DateFormInput';

class MultiCity extends PureComponent {
  state = {
    startDate: moment().format('YYYY-MM-DD HH:MM'),
    endDate: moment().format('YYYY-MM-DD HH:MM'),
    inputShow: 1,
    airportData: this.props.dataJson.airportData,
    airlineData: this.props.dataJson.airlineData,
    cityData: this.props.dataJson.cityData,
    addressData: this.props.dataJson.addressData,
    formInput: {
      type_service: 'multicity',
      maxLength: 5,
      data: [{
        form: [
          {
            alias: 'airport',
            type: 'search',
            className: 'rent-form-item primary-dark width-50 ',
            label: 'From (Airport)',
            value: '',
            text: '',
            display: 'Select Airport',
            show: false,
            data: this.props.dataJson.airportData,
            switch: true,
          },
          {
            alias: 'city',
            type: 'search',
            label: 'To (Area, Address, building)',
            className: 'rent-form-item primary-darkest width-50 ',
            value: '',
            text: '',
            display: 'Select Your City',
            show: false,
            data: this.props.dataJson.cityData,
            child: [{
              alias: 'address',
              type: 'search',
              label: '',
              display: 'Find Address',
              className: 'rent-form-item primary-darkest width-25 ',
              data: this.props.dataJson.addressData,
              value: '',
              text: '',
            }]
          },
          {
            alias: 'address',
            type: 'search',
            label: '',
            className: 'rent-form-item primary-darkest width-25 ',
            value: '',
            text: '',
            parent: 'city',
          }, {
            alias: 'airline',
            type: 'input',
            label: 'Flight Details',
            subLabel: 'Airline Name & Number',
            className: 'rent-form-item primary-darkest width-25 ',
            name: '',
            number: '',
            value: '',
            text: '',
            display: 'Enter Flight Details',
            show: false,
            data: this.props.dataJson.airlineData,
          }, {
            alias: 'date',
            type: 'date',
            label: 'Date and Time',
            className: 'rent-form-item primary-extremely-darkest width-85 ',
            value: '',
            text: '',
            display: 'Select Date & Time',
            startDate: '',
            endDate: '',
            time: '',
            show: false
          }
        ],
      }],
    },
    // formDisplay: {
    //   typeService: 'Select Type',
    //   airport: 'Select Airport',
    //   pickupCityAndAddress: 'Select Your City',
    //   flightDetails: 'Enter Flight Details',
    //   dateAndTime: 'Select Date & Time',
    // },
  }

  // handle click
  handleClick = (e) => {
    const { inputShow } = this.state;
    if (inputShow !== e.currentTarget.dataset.id && e.currentTarget.dataset.id !== undefined) {
      document.querySelectorAll('.jsSelectAirport')[0].classList.add('active');
      this.setState({ inputShow: parseInt(e.currentTarget.dataset.id) });
      this.props.hasChanged(true, this.handleClick, true);
    } else {
      this.setState({ inputShow: 0 });
      this.props.hasChanged(false, this.handleClick, false);
    }
  }

  // handle switch
  handleSwitch = (e) => {
    const { switched } = this.state;
    this.setState({ switched: !switched });
  }

  // filter list
  handleFilterList = (data, e, id, index) => {
    let filterBy = 'name',
      filterData = '',
      // filterState = '',
      // updateFilterData = {},
      // updateFormDisplay = { ...this.state.formDisplay },
      updateFormInput = { ...this.state.formInput };

    switch (data) {
      case 'airport':
        filterData = this.state.airportData;
        // filterState = 'airportData';
        break;
      case 'city':
        filterData = this.state.cityData;
        // filterState = 'cityData';
        break;
      case 'address':
        filterData = this.state.addressData;
        // filterState = 'addressData';
        break;
      case 'airline':
        filterData = this.state.airlineData;
        // filterState = 'airlineData';
        filterBy = 'id';

        //get data filter
        let input = e.target.value.toUpperCase(),
          showData = filterData.filter((value) => {
            return value[filterBy].toLowerCase().search(e.target.value.toLowerCase()) !== -1;
          });

        updateFormInput.data[id].form[index].name = showData.length === 1 && showData[0].id === input ? input : '';
        updateFormInput.data[id].form[index].value = showData.length === 1 && showData[0].id === input ? updateFormInput.data[id].form[index].name + ' ' + updateFormInput.data[id].form[index].number : ' ';;
        updateFormInput.data[id].form[index].text = updateFormInput.data[id].form[index].value;
        updateFormInput.data[id].form[index].display = updateFormInput.data[id].form[index].text !== ' ' ? updateFormInput.data[id].form[index].text : 'Enter Flight Details';

        // this.setState({
        //   formInput: updateFormInput,
        //   formDisplay: updateFormDisplay,
        // }, this.checkAllValue);
        break;
      default:
        break;
    }

    filterData = filterData.filter((value) => {
      return value[filterBy].toLowerCase().search(e.target.value.toLowerCase()) !== -1;
    });

    // updateFilterData[filterState] = filterData;
    // this.setState(updateFilterData);

    updateFormInput.data[id].form[index].data = filterData.filter((value) => {
      return value[filterBy].toLowerCase().search(e.target.value.toLowerCase()) !== -1;
    });

    this.setState({
      formInput: updateFormInput,
      //formDisplay: updateFormDisplay,
    }, this.checkAllValue);
  }

  // select item
  handleSelectItem = (data, e, id, index) => {
    let updateFormDisplayValue = '',
      updateState,
      inputShowNext = (id * 10) + index + 2;
    const getText = e.currentTarget.innerText,
      getId = e.currentTarget.dataset.id,
      updateFormInput = { ...this.state.formInput };
    switch (data) {
      case 'airport':
        document.querySelectorAll('.jsSelectAirport')[0].classList.remove('active');

        updateFormInput.data[id].form[index].value = getId;
        updateFormInput.data[id].form[index].text = getText;
        updateFormInput.data[id].form[index].display = getText;
        // updateFormInput.data[0].airport.value = getId;
        // updateFormInput.data[0].airport.text = getText;

        // updateFormDisplayValue = { ...this.state.formDisplay };
        // updateFormDisplayValue.airport = getText;

        updateState = {
          inputShow: inputShowNext,
          formInput: updateFormInput,
          // formDisplay: updateFormDisplayValue
        };
        break;
      case 'city':
        document.querySelectorAll('.jsSelectCityAirportTransfer')[id].classList.remove('active');
        document.querySelectorAll('.jsFindAddressAirportTransfer')[id].classList.add('active');

        // updateFormInput.data[0].city.value = getId;
        // updateFormInput.data[0].city.text = getText;
        updateFormInput.data[id].form[index].value = getId;
        updateFormInput.data[id].form[index].text = getText;

        updateState = {
          formInput: updateFormInput
        };
        break;
      case 'address':
        updateFormInput.data[id].form[index].display = getText;

        index = index + 1;
        document.querySelectorAll('.jsFindAddressAirportTransfer')[id].classList.remove('active');
        document.querySelectorAll('.jsSelectCityAirportTransfer')[id].classList.add('active');

        // updateFormInput.data[0].address.value = getId;
        // updateFormInput.data[0].address.text = getText;
        updateFormInput.data[id].form[index].value = getId;
        updateFormInput.data[id].form[index].text = getText;

        inputShowNext = (id * 10) + index + 2;

        // updateFormDisplayValue = { ...this.state.formDisplay };
        // updateFormDisplayValue.pickupCityAndAddress = getText;

        updateState = {
          inputShow: inputShowNext,
          formInput: updateFormInput,
          formDisplay: updateFormDisplayValue,
        };
        break;
      case 'airline':
        //document.querySelectorAll('.jsInputFlightDetails')[0].classList.remove('active');

        updateFormInput.data[id].form[index].number = e.target.value;
        updateFormInput.data[id].form[index].value = updateFormInput.data[id].form[index].name + ' ' + updateFormInput.data[id].form[index].number;
        updateFormInput.data[id].form[index].text = updateFormInput.data[id].form[index].name + ' ' + updateFormInput.data[id].form[index].number;
        updateFormInput.data[id].form[index].display = updateFormInput.data[id].form[index].name !== '' ? updateFormInput.data[id].form[index].text : 'Enter Flight Details';

        updateState = {
          inputShow: inputShowNext,
          formInput: updateFormInput,
        };
        break;
      default:
        break;
    }

    // const updateFormDisplay = updateFormDisplayValue;
    this.setState(updateState, this.checkAllValue);
  }

  // back to select city
  handleBackSelectCity = () => {
    document.querySelectorAll('.jsFindAddressAirportTransfer')[0].classList.remove('active');
    document.querySelectorAll('.jsSelectCityAirportTransfer')[0].classList.add('active');
  }

  // get value date form
  getValueDateForm = (value, status, id, index) => {

    let setDateTime = '';
    const { startDate, endDate, selectDate } = value;
    const updateFormInput = { ...this.state.formInput };
    const updateFormDisplay = { ...this.state.formDisplay };

    switch (status) {
      case 'select-first-date':
        setDateTime = moment(startDate).format('ddd, DD MMM YYYY');
        updateFormInput.data[id].form[index].startDate = moment(startDate).format('YYYY-MM-DD');
        updateFormInput.data[id].form[index].endDate = '';
        updateFormInput.data[id].form[index].time = '';
        break;
      case 'select-end-date':
        setDateTime = moment(startDate).format('ddd, DD MMM YYYY') + '<span class="arrow-right"></span>' + moment(endDate).format('ddd, DD MMM YYYY');
        updateFormInput.data[id].form[index].startDate = moment(startDate).format('YYYY-MM-DD');
        updateFormInput.data[id].form[index].endDate = moment(endDate).format('YYYY-MM-DD');
        updateFormInput.data[id].form[index].time = '';
        break;
      case 'select-time':
        setDateTime = moment(startDate).format('ddd, DD MMM YYYY') + '<span class="arrow-right"></span>' + moment(endDate).format('ddd, DD MMM YYYY') + ' <span class="hour-minutes">' + moment(selectDate).format('HH:mm') + '</span>';
        updateFormInput.data[id].form[index].startDate = moment(startDate).format('YYYY-MM-DD');
        updateFormInput.data[id].form[index].endDate = moment(endDate).format('YYYY-MM-DD');
        updateFormInput.data[id].form[index].time = moment(selectDate).format('HH:mm');
        break;
      default:
        break;
    }

    updateFormInput.data[id].form[index].display = setDateTime;

    this.setState({
      formInput: updateFormInput,
      formDisplay: updateFormDisplay,
    }, this.checkAllValue);

  }

  // check all value
  checkAllValue = () => {
    let whiteSpace = /^ *$/,
      active = false,
      result = [];

    this.state.formInput.data.map((value, index) => {
      return value.form.map((form, indexForm) => {
        if (form.type === 'date') {
          if (!whiteSpace.test(form.time)) {
            return result.push(true);
          }
          else {
            return result.push(false);
          }
        }
        else {
          if (!whiteSpace.test(form.value)) {
            return result.push(true);
          }
          else {
            return result.push(false);
          }
        }
      });
    });


    if (result.indexOf(false) < 0) {
      active = true;
    }

    this.props.onClick(active);
  }

  handleAddNewFilter = (e) => {
    let updateFormInput = { ...this.state.formInput },
      maxLength = 5,
      dataJsonTemp = {
        form: [{
          alias: 'airport',
          type: 'search',
          className: 'rent-form-item primary-dark width-50 ',
          label: 'From (Airport)',
          value: '',
          text: '',
          display: 'Select Airport',
          show: false,
          data: this.props.dataJson.airportData,
          switch: true,
        },
        {
          alias: 'city',
          type: 'search',
          label: 'To (Area, Address, building)',
          className: 'rent-form-item primary-darkest width-50 ',
          value: '',
          text: '',
          display: 'Select Your City',
          show: false,
          data: this.props.dataJson.cityData,
          child: [{
            alias: 'address',
            type: 'search',
            label: '',
            className: 'rent-form-item primary-darkest width-25 ',
            data: this.props.dataJson.addressData,
            value: '',
            text: '',
          }]
        },
        {
          alias: 'address',
          type: 'search',
          label: '',
          className: 'rent-form-item primary-darkest width-25 ',
          value: '',
          text: '',
          parent: 'city',
        }, {
          alias: 'airline',
          type: 'input',
          label: 'Flight Details',
          subLabel: 'Airline Name & Number',
          className: 'rent-form-item primary-darkest width-25 ',
          name: '',
          number: '',
          value: '',
          text: '',
          display: 'Enter Flight Details',
          show: false,
          data: this.props.dataJson.ata,
        }, {
          alias: 'date',
          type: 'date',
          label: 'Date and Time',
          className: 'rent-form-item primary-extremely-darkest width-85 ',
          value: '',
          text: '',
          display: 'Select Date & Time',
          startDate: '',
          endDate: '',
          time: '',
          show: false
        }]
      };

    if (updateFormInput.data.length < maxLength) {
      updateFormInput.data.push(dataJsonTemp);
      this.setState({ formInput: updateFormInput }, this.checkAllValue);
    }
  }

  // render
  render() {
    const {
      handleClick,
      state: { switched }
    } = this;

    let classNameSwitch = 'switch-button';

    if (switched) {
      classNameSwitch += ' switched';
    }

    // Calendar Data
    let rentCarCalendarData = {
      titleCalendar: 'Calendar',
      titleTime: 'Pick up hour',
      timeFormat: 'HH:mm',
      intervalTime: 30,
      highlightDates: [
        {
          date: '2018-11-29',
          description: '29 Nov : Lorem ipsum day'
        },
        {
          date: '2018-12-25',
          description: '25 Okt: Dolor sit day'
        }
      ],
      onClickButton: handleClick,
      textButton: 'Done',
      sendValue: this.getValueDateForm,
    },
      // className = 'rent-form-display',
      dataForm = this.state.formInput.data;

    //form template
    let templateFormInput = (value, i) => {
      let temp = [],
        index = 0;

      value.map((valueInput, valueIndex) => {
        index = (i * 10) + valueIndex + 1;
        switch (valueInput.type) {
          case 'search':
            if (valueInput.parent === undefined) {
              temp.push(
                <div className={`${valueInput.className} ${this.state.inputShow === index ? 'show' : ''}`} key={index}>
                  {/* form input */}
                  <div className="rent-form-input search-input" >
                    <MobileTopHeader title={valueInput.label} onClick={this.handleClick} />
                    {/* Airport */}
                    <ListSearchFormInput
                      label={valueInput.display}
                      className={valueInput.alias === 'airport' ? 'jsSelectAirport' : 'jsSelectCityAirportTransfer'}
                      name={valueInput.alias}
                      data={valueInput.data}
                      onChangeInput={(e) => this.handleFilterList(valueInput.alias, e, i, valueIndex)}
                      onClickItem={(e) => this.handleSelectItem(valueInput.alias, e, i, valueIndex)}
                      activeBlock
                    />

                    {valueInput.child !== undefined &&
                      <ListSearchFormInput
                        label={valueInput.child[0].display}
                        className="jsFindAddressAirportTransfer"
                        name={valueInput.child[0].alias}
                        data={valueInput.child[0].data}
                        onChangeInput={(e) => this.handleFilterList(valueInput.alias, e, i, valueIndex)}
                        onClickItem={(e) => this.handleSelectItem(valueInput.child[0].alias, e, i, valueIndex)}
                        onClickBack={this.handleBackSelectCity} backSearch />
                    }
                  </div >
                  {/* Form Display */}
                  <FormDisplay id={index} label={valueInput.label} value={valueInput.display} onClick={this.handleClick} search />
                  {valueInput.switch ? <div className={classNameSwitch} onClick={this.handleSwitch}>Switch</div> : ''}
                </div >
              );
            }
            break;
          case 'input':
            temp.push(
              <div className={`${valueInput.className} ${this.state.inputShow === index ? 'show' : ''}`} key={index}>
                {/* Form input */}
                <div className="rent-form-input text-input">
                  <MobileTopHeader title="Pickup City and Address" onClick={this.handleClick} />
                  {/* City */}
                  <FlightDetailsFormInput
                    label={valueInput.label}
                    subLabel={valueInput.subLabel}
                    className="jsInputFlightDetails"
                    name={valueInput.alias}
                    data={valueInput.data}
                    onChangeInput={(e) => this.handleFilterList(valueInput.alias, e, i, valueIndex)}
                    onBlur={(e) => this.handleSelectItem(valueInput.alias, e, i, valueIndex)}
                    activeBlock
                  />
                </div>
                {/* Form Display */}
                <FormDisplay id={index} label="Flight Details" value={valueInput.display} onClick={this.handleClick} />
              </div>
            );
            break;
          case 'date':
            temp.push(
              <div className={`${valueInput.className} ${this.state.inputShow === index ? 'show' : ''}`} key={index}>
                {/* Form input */}
                <div className="rent-form-input calendar-input">
                  <MobileTopHeader title="Date and Time" onClick={this.handleClick} />
                  <DateFormInput
                    name={valueInput.alias}
                    indexElement={i}
                    indexForm={valueIndex}
                    titleCalendar={rentCarCalendarData.titleCalendar}
                    titleTime={rentCarCalendarData.titleTime}
                    timeFormat={rentCarCalendarData.timeFormat}
                    intervalTime={rentCarCalendarData.intervalTime}
                    highlightDates={rentCarCalendarData.highlightDates}
                    onClickButton={rentCarCalendarData.onClickButton}
                    sendValue={rentCarCalendarData.sendValue}
                    textButton={rentCarCalendarData.textButton}
                  />
                </div>
                {/* Form Display */}
                <FormDisplay id={index} label="Date and Time" value={valueInput.display} onClick={this.handleClick} />
              </div >)
            break;

          default:
            break;
        }
      })

      return temp;
    }

    let formTemplate = dataForm.map((value, i) => {
      return (
        <div className="o-rent-form-wrapper" key={i}>
          {templateFormInput(value.form, i)}
        </div>
      )
    })

    return (
      <Fragment>
        {formTemplate}
        <div className="o-rent-form-add-button" onClick={this.handleAddNewFilter}>
          <div className="o-rent-form-add-button-caption">+ Add City</div>
        </div>
      </Fragment>
    );
  }
}

// default props
MultiCity.defaultProps = {
  onTypeServiceChange: () => console.log('changed'),
  mainSlideActive: 2,
  inputShow: 0,
  inputOnTop: false,
  dataJson: {},
  hasChanged: () => console.log('changed'),
  onClick: () => console.log('click')
};

// props types
MultiCity.propTypes = {
  onTypeServiceChange: PropTypes.func,
  mainSlideActive: PropTypes.number,
  inputShow: PropTypes.number,
  inputOnTop: PropTypes.bool,
  dataJson: PropTypes.object,
  hasChanged: PropTypes.func,
  onClick: PropTypes.func
};

export default MultiCity;