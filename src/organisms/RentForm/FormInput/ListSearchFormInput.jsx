/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-04 14:53:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-11 16:54:18
 */
// core
import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid/v4';
import { connect } from 'react-redux';

import lang from '../../../assets/data-master/language'

// component
import { PulseLoader } from 'react-spinners';

const ListSearchFormInput = ({ label, className, activeBlock, name, backSearch, data, onChangeInput, onClickItem, onClickBack, loadingAddress, loadingCityCoverage, loadingAirport }) => {
    let elementBackForm = '';
    let classNameBlock = 'input-search-filter ' + className;
    if (backSearch) {
        elementBackForm = <div className="back-form" onClick={ onClickBack }>Back</div>;
    }
    if (activeBlock) {
        classNameBlock += ' active';
    }
    return (
        <div className={classNameBlock}>
            { elementBackForm }
            <div className="input-search">
                <div className="label">{ label }</div>
                <div className="input-text">
                    <input
                        type="text"
                        name={ name }
                        autoFocus
                        autoComplete="off"
                        onChange={ onChangeInput }
                    />
                </div>
            </div>

            <div className="filter-list">
                {data.length > 0 ?
                    data.map(value => (
                        <div
                            key={ uuid() }
                            className="item"
                            data-id={ value.id }
                            onClick={ onClickItem }
                        >
                            { value.name }
                        </div>
                    ))
                    :
                    loadingAddress || loadingCityCoverage || loadingAirport ? 
                    <div className="filter-empty-list a-text centered">
                        <PulseLoader
                            sizeUnit={"px"}
                            size={7}
                            color={'#ffffff'}
                            loading={loadingAddress || loadingCityCoverage}
                        />
                    </div>
                    :
                    <div className="filter-empty-list a-text centered">
                        {/* <img src={researchImage} alt="research icon"></img> */}
                        <p>{lang.sorryAreaOutCoverage[lang.default]}</p>
                        <button className="a-btn btn-outline-white">{lang.searchCarRental[lang.default]}</button>
                    </div>
                }
            </div>
        </div>
    );
}

// default props
ListSearchFormInput.defaultProps = {
    label: 'Select City',
    className: 'jsSelectCity',
    name: 'city',
    activeBlock: false,
    backSearch: false,
    data: [],
    onChangeInput: () => console.log('changed!'),
    onClickItem: () => console.log('clicked!'),
    onClickBack: () => console.log('clicked!'),
};

// props types
ListSearchFormInput.propTypes = {
    label: PropTypes.string,
    className: PropTypes.string,
    name: PropTypes.string,
    activeBlock: PropTypes.bool,
    backSearch: PropTypes.bool,
    data: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.oneOfType([
                    PropTypes.string,
                    PropTypes.number
                ]),
            name: PropTypes.string,
            onClick: PropTypes.bool,
        }),
      ),
    onChangeInput: PropTypes.func,
    onClickItem: PropTypes.func,
    onClickBack: PropTypes.func,
};

const mapStateToProps = ({ addressgm, carrental, masterairport }) => {
    const loadingCityCoverage = carrental.loading;
    const loadingAddress = addressgm.loading;
    const loadingAirport = masterairport.loading;
    const { address } = addressgm;
    return { loadingAddress, loadingCityCoverage, loadingAirport, address }
}

export default connect(mapStateToProps, {})(ListSearchFormInput);
// export default ListSearchFormInput;
