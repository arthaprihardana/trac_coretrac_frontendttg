// core
import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid/v4';

const FlightDetailsFormInput = ({ label, subLabel, className, activeBlock, name, backSearch, data, onChangeInput, onBlur, onClickItem, onClickBack }) => {
	let classNameBlock = 'input-flight-detail ' + className,
		// empty data item
		dataItem = '';

	if (activeBlock) {
		classNameBlock += ' active';
	}
	if (data.length === 1) {
		dataItem = data.map(value => (
			<div
				key={uuid()}
				className="item"
				data-id={value.id}
			>
				{value.name}
			</div>
		))
	}

	return (
		<div className={classNameBlock}>
			<div className="input-title">
				<h4>{label}</h4>
			</div>
			<div className="input-text"></div>
			<div className="label">{subLabel}</div>
			<div className="row">
				<div className="input-flight-detail-item">
					<input
						className="tt-uppercase"
						type="text"
						name="name"
						autoComplete="off"
						onChange={onChangeInput}
					/>
				</div>
				<div className="input-flight-detail-item">
					<input
						type="text"
						name="number"
						autoComplete="off"
						onBlur={onBlur}
					/>
				</div>
			</div>

			<div className="flight-detail-airline-name">
				{dataItem}
			</div>
		</div>
	);
}

// default props
FlightDetailsFormInput.defaultProps = {
	label: 'Select City',
	className: 'jsSelectCity',
	name: 'name',
	activeBlock: false,
	backSearch: false,
	data: [],
	onChangeInput: () => console.log('changed!'),
	onClickItem: () => console.log('clicked!'),
	onClickBack: () => console.log('clicked!'),
};

// props types
FlightDetailsFormInput.propTypes = {
	label: PropTypes.string,
	className: PropTypes.string,
	name: PropTypes.string,
	number: PropTypes.string,
	activeBlock: PropTypes.bool,
	backSearch: PropTypes.bool,
	data: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.oneOfType([
				PropTypes.string,
				PropTypes.number
			]),
			name: PropTypes.string,
			onClick: PropTypes.bool,
		}),
	),
	onChangeInput: PropTypes.func,
	onChangeInputName: PropTypes.func,
	onChangeInputNumber: PropTypes.func,
	onClickItem: PropTypes.func,
	onClickBack: PropTypes.func,
};

export default FlightDetailsFormInput;