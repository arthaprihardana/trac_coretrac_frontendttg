// core
import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid/v4';

// form input
import RadioFormInput from '../FormInput/RadioFormInput';

const RadioFormInputList = ({ label, className, caption, activeBlock, name, backSearch, data, onChangeInput, onClickItem, onClickBack }) => {
  let elementBackForm = '';
  let classNameBlock = 'input-group-radio ' + className;
  if (backSearch) {
    elementBackForm = <div className="back-form" onClick={onClickBack}>Back</div>;
  }
  if (activeBlock) {
    classNameBlock += ' active';
  }
  return (
    <div className={classNameBlock}>
      <div class="input-radio-label">
        <div class="label">{caption}</div>
      </div>
      <RadioFormInput
        label="Deliver to location"
        subLabel="(Extra Charge)"
        value="1"
        name={data.name}
        dataNext="pickup"
        target="typePickup"
        onClick={this.handleClickInputRadioWithChild}
      />
      <RadioFormInput
        label="Pick up at pool"
        value="2"
        name={data.name}
        dataNext="pickup"
        target="typePickup"
        onClick={this.handleClickInputRadioWithChild}
      />
    </div>
  );
}

// default props
RadioFormInputList.defaultProps = {
  label: 'Select City',
  className: 'jsSelectCity',
  name: 'city',
  activeBlock: false,
  backSearch: false,
  data: [],
  onChangeInput: () => console.log('changed!'),
  onClickItem: () => console.log('clicked!'),
  onClickBack: () => console.log('clicked!'),
};

// props types
RadioFormInputList.propTypes = {
  label: PropTypes.string,
  className: PropTypes.string,
  name: PropTypes.string,
  activeBlock: PropTypes.bool,
  backSearch: PropTypes.bool,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
      ]),
      name: PropTypes.string,
      onClick: PropTypes.bool,
    }),
  ),
  onChangeInput: PropTypes.func,
  onClickItem: PropTypes.func,
  onClickBack: PropTypes.func,
};

export default RadioFormInputList;