// core
import React from 'react';
import PropTypes from 'prop-types';

const RadioFormInput = ({ label, subLabel, value, name, dataNext, target, onClick, display }) => {

    const toCamelCase = (str) => {
        return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
            if (+match === 0) return "";
            return index === 0 ? match.toLowerCase() : match.toUpperCase();
        });
    }

    const handleClick = (e) => {
        const getText = e.currentTarget.nextSibling.innerText;
        const getNext = parseInt(e.currentTarget.dataset.next);
        const getTarget = e.currentTarget.dataset.target;
        const getValue = e.currentTarget.value;
        const name = e.currentTarget.name;
        const display = e.currentTarget.dataset.display;

        let sendData = {
            text: getText,
            next: getNext,
            target: getTarget,
            name: name,
            value: getValue,
            display: display
        };

        onClick(sendData);
    }

    let inputId = toCamelCase(label);

    return (
        <div className="item">
            <input
                type="radio"
                className="input-radio"
                value={value}
                name={name}
                id={inputId}
                data-next={dataNext}
                data-target={target}
                data-display={display}
                onClick={handleClick}
            />
            <label htmlFor={inputId}>
                {label}
            </label>
            {subLabel != null ? <span className="sub-label">{subLabel}</span> : ''}
        </div>
    );
}

// default props
RadioFormInput.defaultProps = {
    label: 'Chauffeur Services',
    subLabel: null,
    inputId: 'typeService',
    value: '1',
    name: 'type_service',
    dataNext: '2',
    target: 'typeService',
    onClick: () => console.log('clicked!'),
};

// props types
RadioFormInput.propTypes = {
    label: PropTypes.string,
    subLabel: PropTypes.string,
    inputId: PropTypes.string,
    value: PropTypes.string,
    name: PropTypes.string,
    dataNext: PropTypes.string,
    target: PropTypes.string,
    onClick: PropTypes.func,
};

export default RadioFormInput;
