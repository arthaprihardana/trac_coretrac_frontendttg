/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-10 00:59:31 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-02-10 00:59:31 
 */
// core
import React from 'react';
import PropTypes from 'prop-types';

const ButtonForm = ({ text, onClick, active }) => {
    let className = 'rent-form-item rent-form-search-button';
    if (active) {
        className += ' active';
    }
    return (
        <div className={className} onClick={ onClick }>
            <div className="search-text">{ text }</div>
        </div>
    );
}

// default props
ButtonForm.defaultProps = {
    text: 'Search',
    active: false,
    onClick: () => console.log('clicked!'),
};

// props types
ButtonForm.propTypes = {
    text: PropTypes.any,
    active: PropTypes.bool,
    onClick: PropTypes.func,
};

export default ButtonForm;
