/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-18 10:31:12 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 23:28:23
 */
// core
import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
// import uuid from 'uuid/v4';

// component
import { Button } from 'atom';

// plugin component
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/id';

import _ from "lodash";

import lang from "../../../assets/data-master/language";
// style
import 'react-datepicker/dist/react-datepicker.css';

class DateFormInput extends Component {
    // state
    state = {
        clickFirstDate: false,
        monthShown: 2,
        minDate: moment(),
        maxDate: moment().add(5, 'y'),
        startDate: moment(),
        endDate: moment(),
        minTime: moment().hours(0).minutes(0),
        maxTime: moment().hours(23).minutes(30)
    }

    // did mount
    componentDidMount() {
        // window resize
        this.handleWindowResize();

        window.addEventListener('resize', this.handleWindowResize);
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.selectedproductservicetype !== prevProps.selectedproductservicetype) {
            return { selected: this.props.selectedproductservicetype }
        }
        if(this.props.packageSelected !== prevProps.packageSelected) {
            return { package: this.props.packageSelected };
        }
        if(this.props.adjustment !== prevProps.adjustment) {
            return { adjustment: this.props.adjustment }
        }
        return null;
    }
    
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(snapshot.selected !== undefined) {
                let f = _.filter(this.props.adjustment, v => snapshot.selected.MsProductId === v.MsProductId);
                if(f.length>0){
                    if(snapshot.selected.MsProductId==='PRD0008'){
                        this.setState({
                            maxTime:moment().hours(21).minutes(30)
                        })
                    }


                    var toMin = moment.duration(f[0].MaxOrderTime).asMinutes();
                    var setHour = moment().add(toMin, "minutes").format('HH:mm');
                    this.setState({
                        minDate: moment().add(toMin, "minutes"),
                        startDate: moment().add(toMin, "minutes"),
                        minTime: moment().hours(setHour.split(":")[0]).minutes(setHour.split(":")[1]),
                    })
                }

            }
            if(snapshot.package !== undefined) {
                let f = _.filter(this.props.adjustment, v => this.props.selectedproductservicetype.MsProductId === v.MsProductId);
                if(snapshot.package === "4" || snapshot.package === "12") {
                    var toMin = moment.duration(f[0].MaxOrderTime).asMinutes();
                    var setHour = moment().add(toMin, "minutes").format('HH:mm');
                    this.setState({
                        minDate: moment().add(toMin, "minutes"),
                        startDate: moment().add(toMin, "minutes"),
                        minTime: moment().hours(setHour.split(":")[0]).minutes(setHour.split(":")[1]),
                        endDate: moment(),
                    })
                } else {
                    var toMin = moment.duration(f[0].MaxOrderTime).asMinutes()
                    var setHour = moment().add(toMin, "minutes").format('HH:mm');
                    this.setState({
                        minDate: moment().add(toMin, "minutes"),
                        maxDate: moment().add(5, 'y'),
                        startDate: moment().add(toMin, "minutes"),
                        minTime: moment().hours(setHour.split(":")[0]).minutes(setHour.split(":")[1]),
                        endDate: moment(),
                        clickFirstDate: false
                    })
                }
            }
            if(snapshot.adjustment !== undefined) {
                if(this.props.selectedproductservicetype!==null){
                    let f = _.filter(snapshot.adjustment, v => this.props.selectedproductservicetype.MsProductId === v.MsProductId);
                    var toMin = moment.duration(f[0].MaxOrderTime).asMinutes();
                    var setHour = moment().add(toMin, "minutes").format('HH:mm');
                    this.setState({
                        minDate: moment().add(toMin, "minutes"),
                        startDate: moment().add(toMin, "minutes"),
                        minTime: moment().hours(setHour.split(":")[0]).minutes(setHour.split(":")[1]),
                    })
                }

            }
        }
    }

    // window resize
    handleWindowResize = () => {
        if (window.innerWidth < 768) {
            this.setState({ monthShown: 1 });
        } else {
            this.setState({ monthShown: 2 });
        }
    }

    // change date
    handleChangeDate = (date, event) => {
        let value = {};
        let changeStatus = '';
        let id = this.props.indexElement;
        let index = this.props.indexForm;

        const { packageSelected } = this.props;
        const { clickFirstDate, startDate, endDate } = this.state;
        let f = _.filter(this.props.adjustment, v => this.props.selectedproductservicetype.MsProductId === v.MsProductId);

        var toMin = moment.duration(f[0].MaxOrderTime).asMinutes();
        var after = moment().add(toMin, "minutes").format('YYYY-MM-DD HH:mm');
        let differ = moment(date).diff(after);


        if(differ>0){
            this.setState({
                minTime: moment().hours("00").minutes("00")
            });
        }

        // select date
        if (event !== undefined) {

            // select end date
            if (clickFirstDate) {

                // set state
                this.setState({
                    minDate: moment(),
                    endDate: date,
                    clickFirstDate: false,
                });
                let time_picked = moment(date).format("HH:mm");

                let date_correct = moment(endDate).format("YYYY-MM-DD ");

                let date_combine = date_correct + time_picked;

                let end_date= moment(date_combine);
                // set value
                value['selectDate'] = date;
                value['startDate'] = startDate;
                // value['endDate'] = date;
                value['endDate'] = end_date;

                // status
                changeStatus = 'select-end-date';

                // select first date
            } else {

                // set state
                this.setState({
                    minDate: date,
                    maxDate: packageSelected === "4" || packageSelected === "12" || packageSelected === "one-way" ? date : moment().add(5, 'y'),
                    startDate: date,
                    endDate: date,
                    clickFirstDate: true
                });
                let time_picked = moment(date).format("HH:mm");

                let date_correct = moment(endDate).format("YYYY-MM-DD ");

                let date_combine = date_correct + time_picked;

                let end_date= moment(date_combine);
                // set value
                value['selectDate'] = date;
                value['startDate'] = date;
                value['endDate'] = end_date;

                // status
                changeStatus = 'select-first-date';

            }
            // select time
        } else {
            let time_picked = moment(date).format("HH:mm");

            let date_correct = moment(endDate).format("YYYY-MM-DD ");

            let date_combine = date_correct + time_picked;

            let end_date= moment(date_combine);
            value['tmpEndDate'] = moment(date_combine).add(packageSelected, 'hours');
            // console.log("end_date setelah tambah", moment(end_date).format("YYYY-MM-DD HH:mm"))
            // console.log("endDate after addition addition >>", moment(end_date).format("YYYY MM DD HH:mm"));
            // console.log("package selected >>", packageSelected);
            if(this.props.selectedproductservicetype.MsProductId !== 'PRD0008'){
                value['endDate'] = moment(end_date);
            }else{
                // console.log("ini");
                value['endDate'] = endDate;
            }
            // set state
            this.setState({ startDate: date });

            // set value
            value['startDate'] = startDate;
            value['selectDate'] = date;


            // status
            changeStatus = 'select-time';

        }

        // return value to parent
        this.props.sendValue(value, changeStatus, id, index);

    }

    resetDate = () => {
        let f = _.filter(this.props.adjustment, v => this.props.selectedproductservicetype.MsProductId === v.MsProductId);
        var toMin = moment.duration(f[0].MaxOrderTime).asMinutes();
        var setHour = moment().add(toMin, "minutes").format('HH:mm');
        this.setState({
            clickFirstDate: false,
            minDate:moment().add(toMin, "minutes"),
            maxDate: moment().add(5, 'y'),
            startDate: moment().add(toMin, "minutes"),
            endDate: moment(),
            minTime: moment().hours(setHour.split(":")[0]).minutes(setHour.split(":")[1])
        })
    }

    // render
    render() {
        // props
        const {
            titleCalendar,
            titleTime,
            timeFormat,
            selectedproductservicetype,
            intervalTime,
            // highlightDates,
            onClickButton,
            textButton
        } = this.props;

        // state
        const {
            monthShown,
            minDate,
            maxDate,
            startDate,
            endDate,
        } = this.state;

        // highlightdate list
        // let highlightDatesList = [];
        // highlightDates.forEach((value) => {
        //     highlightDatesList.push(moment(value.date));
        // });
        

        return (
            <Fragment>
                <div className="trac-calendar-theme">
                    <div className="trac-calendar-title">
                        <div className="title-calendar">{ titleCalendar }</div>
                        <div className="title-time">{ titleTime }</div>
                    </div>
                    <DatePicker
                        inline
                        showTimeSelect
                        timeFormat={ timeFormat }
                        monthsShown={ monthShown }
                        timeIntervals={ intervalTime }
                        minDate={ minDate }
                        maxDate={ maxDate }
                        startDate={ startDate }
                        endDate={ endDate }
                        selected={ startDate }
                        onSelect={ this.handleChangeDate }
                        onChange={ this.handleChangeDate }
                        minTime={ this.state.minTime }
                        maxTime={ this.state.maxTime }
                        // highlightDates={ highlightDatesList }
                    />

                    {/* <div className="date-hightlight-description">
                        { highlightDates.map(value => (
                        <div className="item" key={ uuid() }>{ value.description }</div>
                        )) }
                    </div> */}
                    <div className="trac-calendar-button-mobile">
                        <Button type="button" primary onClick={ onClickButton }>{ textButton }</Button>
                    </div>
                    <button onClick={this.resetDate} className="a-btn btn-clear-date">{lang.resetDate[lang.default]}</button>
                </div>
            </Fragment>
        );
    }

}

// default props
DateFormInput.defaultProps = {
    titleCalendar: lang.calendar[lang.default],
    titleTime: lang.pickupHour[lang.default],
    timeFormat: 'HH:mm',
    intervalTime: 30,
    // highlightDates: [{
    //     date: '2018-11-29',
    //     description: '29 Nov : Lorem ipsum day'
    // },{
    //     date: '2018-12-25',
    //     description: '25 Okt: Dolor sit day'
    // }],
    onClickButton: () => {},
    textButton: 'Done',
    currentStartDate: '',
    currentEndDate: '',
    currentPickUpTime: ''
};

// props types
DateFormInput.propTypes = {
    titleCalendar: PropTypes.string,
    titleTime: PropTypes.string,
    timeFormat: PropTypes.string,
    intervalTime: PropTypes.number,
    indexElement: PropTypes.number,
    indexForm: PropTypes.number,
    highlightDates: PropTypes.arrayOf(
                        PropTypes.shape({
                          date: PropTypes.string,
                          description: PropTypes.string,
                        }),
                    ),
    onClickButton: PropTypes.func,
    textButton: PropTypes.string,
    packageSelected: PropTypes.string,
    // currentStartDate: PropTypes.any,
    // currentEndDate: PropTypes.any,
    // currentPickUpTime: PropTypes.any
};

const mapStateToProps = ({ product, adjustmentRetail }) => {
    const { selectedproductservicetype } = product;
    const { adjustment } = adjustmentRetail;
    return { selectedproductservicetype, adjustment };
}

export default connect(mapStateToProps, {})(DateFormInput);
