// core
import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

// form wrapper
import CarRentForm from './FormWrapper/CarRentForm';
import AirportTansferForm from './FormWrapper/AirportTansferForm';
import BusRentForm from './FormWrapper/BusRentForm';

// style
import './RentForm.scss';

class RentForm extends PureComponent {
    state = {
        showBookingDetail: false,
        showInput: false,
    }

    // handle click global
    handleFormChanged = (overlayStatus, overlayEvent, inputShowStatus) => {
        this.props.hasChanged(overlayStatus, overlayEvent, inputShowStatus);
        this.setState({ showInput: inputShowStatus });
    }

    // submit car rent form
    handleSubmit = (values, event, dataChange) => {
        if ( !this.isInputOnTop() && event === 'onClick') {
            this.handleClickBookingDetail();
        } else {
            if ( dataChange !== 'select-date' && dataChange !== 'select-city' ) {
                this.setState({ showInput: false });
            }
        }
        this.props.onSubmit(values, event);
    }

    // handle booking detail
    handleClickBookingDetail = () => {
        let { showBookingDetail } = this.state;
        showBookingDetail = !showBookingDetail;
        this.setState({ showBookingDetail: showBookingDetail });

        let _removeScroll = 'remove-scroll';
        if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
            _removeScroll = 'remove-scroll-ios';
        }

        if ( showBookingDetail ) {
            if (window.innerWidth < 768) {
                document.body.classList.add(_removeScroll);
            }
        } else {
            if (window.innerWidth < 768) {
                document.body.classList.remove(_removeScroll);
            }
        }

    }

    isInputOnTop = () => {
        const { inputOnTop } = this.props;
        return inputOnTop;
    }

    // render
    render() {
        const {
            handleFormChanged,
            handleSubmit,
            handleClickBookingDetail,
            state: { showBookingDetail, showInput },
            props: { mainSlideActive, carRental, airportTransfer, busRental, inputOnTop, formFilterList },
        } = this;

        let rentFormContainer = '';
        let className = 'rent-top-form';

        if ( showBookingDetail ) {
            className += ' show';
        }

        if ( showInput ) {
            className += ' showed-input';
        }

        if (formFilterList) {
            rentFormContainer = (
                <Fragment>
                    <div className={ className }>
                        <div className="rent-booking-detail">
                            Booking Detail
                            <div className="toggle-booking-detail" onClick={ handleClickBookingDetail }>Close</div>
                        </div>
                        <div className="container">
                            { carRental && (
                                <CarRentForm mainSlideActive={ mainSlideActive } hasChanged={ handleFormChanged } onSubmit={handleSubmit} inputOnTop={inputOnTop}/>
                            )}
                            { airportTransfer && (
                                <AirportTansferForm mainSlideActive={ mainSlideActive } hasChanged={ handleFormChanged } onSubmit={handleSubmit} inputOnTop={inputOnTop}/>
                            )}
                            { busRental && (
                                <BusRentForm mainSlideActive={ mainSlideActive } hasChanged={ handleFormChanged } onSubmit={handleSubmit} inputOnTop={inputOnTop}/>
                            )}
                        </div>
                    </div>
                    <div className="rent-top-background"></div>
                </Fragment>
            )
        } else {
            rentFormContainer = (
                <Fragment>
                    { carRental && (
                        <CarRentForm mainSlideActive={ mainSlideActive } hasChanged={ handleFormChanged } onSubmit={handleSubmit} inputOnTop={inputOnTop}/>
                    )}
                    { airportTransfer && (
                        <AirportTansferForm mainSlideActive={ mainSlideActive } hasChanged={ handleFormChanged } onSubmit={handleSubmit} inputOnTop={inputOnTop}/>
                    )}
                    { busRental && (
                        <BusRentForm mainSlideActive={ mainSlideActive } hasChanged={ handleFormChanged } onSubmit={handleSubmit} inputOnTop={inputOnTop}/>
                    )}
                </Fragment>
            );
        }

        return rentFormContainer;
    }
}

RentForm.defaultProps = {
  mainSlideActive: 1,
  carRental: false,
  airportTransfer: false,
  busRental: false,
  onSubmit: () => {},
  hasChanged: () => {},
};

// props types
RentForm.propTypes = {
  mainSlideActive: PropTypes.number,
  carRental: PropTypes.bool,
  airportTransfer: PropTypes.bool,
  busRental: PropTypes.bool,
  onSubmit: PropTypes.func,
  hasChanged: PropTypes.func,
};

export default RentForm;
