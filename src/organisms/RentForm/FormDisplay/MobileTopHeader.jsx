// core
import React from 'react';
import PropTypes from 'prop-types';

const MobileTopHeader = ({ title, onClick }) => {
    return (
        <div className="mobile-top-header">
            <h4>{ title }</h4>
            <div className="close-button" onClick={ onClick }>Close</div>
        </div>
    );
}

// default props
MobileTopHeader.defaultProps = {
  title: 'Type of Service',
  onClick: () => console.log('clicked!'),
};

// props types
MobileTopHeader.propTypes = {
  title: PropTypes.string,
  onClick: PropTypes.func,
};

export default MobileTopHeader;