// core
import React from 'react';
import PropTypes from 'prop-types';

const FormDisplay = ({ onClick, label, value, id, search }) => {
    let className = 'rent-form-display';
    if (search) {
        className += ' search-icon';
    }
    return (
        <div className={ className } data-id={ id } onClick={ onClick }>
            <div className="label">{ label }</div>
            <div className="value" dangerouslySetInnerHTML={{ __html: value }}></div>
        </div>
    );
}

// default props
FormDisplay.defaultProps = {
  id: 1,
  label: 'Type of Service',
  value: 'Select Type',
  onClick: () => console.log('clicked'),
};

// props types
FormDisplay.propTypes = {
  id: PropTypes.number,
  label: PropTypes.string,
  value: PropTypes.string,
  onClick: PropTypes.func,
};

export default FormDisplay;