import React, { Component } from "react";
import { Button } from 'atom';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import cx from 'classnames';
import {delay} from 'helpers';
import './Promo.scss';

class Promo extends Component {
    state = {
        copied: false,
        textCopy: 'Copy Promo'
    }

    copyPromoCode = () => {
        this.setState({
            copied: true,
            textCopy: 'Copied to Clipboard'
        }, () => {
            delay(1000).then(() => {
                this.setState({ 
                    copied: false, 
                    textCopy: 'Copy Promo' 
                });
            });
        })
    }

    render() {
        const {copied, textCopy} = this.state;
        const {data} = this.props;
        const {copyPromoCode} = this;

        const classCopied = cx('promo-code', {
            'copied': copied
        })

        return (
            <div className="o-promo">
                <p className="label">Priod Promo</p>
                <p className="value-small">{data.period}</p>
                <p className="label">Kode Kupon</p>
                <input
                    type="text"
                    onChange={() => null}
                    className={classCopied}
                    value={data.kupon}
                />
                <CopyToClipboard text={data.kupon} onCopy={copyPromoCode}>
                    <Button primary>{textCopy}</Button>
                </CopyToClipboard>
            </div>
        );
    }
}

export default Promo;
