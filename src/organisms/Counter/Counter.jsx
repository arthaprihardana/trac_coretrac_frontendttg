import React, { Component } from 'react';
import './Counter.scss';
import cx from 'classnames';
import PropTypes from 'prop-types';

class Counter extends Component {
  state = {
    value: 0,
  }

  increment = () => {
    this.setState({
      value: this.state.value + 1
    }, () => this.onChange())
  }

  decrement = () => {
    if(this.state.value !== 0) {
      this.setState({
        value: this.state.value - 1
      }, () => this.onChange())
    }
  }

  componentDidMount(){
    if(this.props.defaultValue) this.setState({value: this.props.defaultValue})
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.defaultValue !== undefined){
      this.setState({ value: nextProps.defaultValue });
    }
  }

  onChange = () => {
    this.props.onValueChange(this.state.value)
  }

  shouldComponentUpdate(nextProps, nextState){
    if(this.state.value !== nextState.value) return true;
    return false;
  }

  render() {
    const { rounded } = this.props;
    const className = cx('o-counter', {
      rounded,
    });
    let {value} = this.state;
    return (
      <div className={className}>
        <div className="counter-col">
          <button type="button" className="counter-decrement" onClick={this.decrement}>Dec</button>
        </div>
        <div className="counter-col">
          <div className="value">{value}</div>
        </div>
        <div className="counter-col">
          <button type="button" className="counter-increment" onClick={this.increment}>Inc</button>
        </div>
      </div>
    );
  }
};

Counter.propTypes = {
  onValueChange: PropTypes.func,
  defaultValue: PropTypes.number
}

export default Counter;
