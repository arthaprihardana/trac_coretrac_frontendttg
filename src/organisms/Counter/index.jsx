/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-27 22:14:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 20:44:55
 */
import React, { Component } from 'react';
import './Counter.scss';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from "lodash";
import { validateStockExtras } from "actions";

// helpers
import { localStorageDecrypt } from "helpers";

class Counter extends Component {
  state = {
    value: 0,
      bu:'',
    changeAmount: 0,
    maxStock: false
  }

  componentDidMount = () => {
    const {defaultValue} = this.props;
      let bu = localStorageDecrypt('_bu', 'string');
      this.setState({
          bu:bu
      })
      // let tmp = JSON.parse(JSON.stringify(type_service));
      console.log("Type Service =>>", bu);
    if(defaultValue !== 0) {
      this.setState({
        value: defaultValue
      })
    }
  }
  

  handleValidateAvailable = (id) => {
    const { selectedCar } = this.props;
    var extra = _.map(selectedCar, v => {
        let extras = v.extraItems;
        let extraById = _.filter(extras, { id: id });
        return extraById[0];
    })
    var totalAmount = _.reduce(extra, function(sum, n) {
        return sum + n.amounts;
    }, 0)
    if(parseInt(extra[0].Availability) > totalAmount) {
        return true;
    }
    this.props.validateStockExtras(extra[0]);
    return false;
}

  increment = () => {
    const { id, activeCar, isExtras, stockType } = this.props;
    if(isExtras) {
      let extras = activeCar.extraItems;
      let indexExtra = extras.findIndex(e => e.id === id);
      if(stockType === 1) {
        let available = this.handleValidateAvailable(extras[indexExtra].id);
        if(available) {
          this.setState(prevState => {
            return { value: this.state.value + 1 }
          }, () => this.onChange());
        }
      } else {
        this.setState(prevState => {
          return { value: this.state.value + 1 }
        }, () => this.onChange());
      }
    } else {
      this.setState(prevState => {
        if(prevState.value >= this.props.maxAvailable) {
          return { value: prevState.value, maxStock: true }
        } else {
          return { value: this.state.value + 1 }
        }
      }, () => this.onChange());
    }
  }

  decrement = () => {
    if(this.state.value !== 0) {
      this.props.validateStockExtras(null);
      this.setState({
        value: this.state.value - 1,
        maxStock: false
      }, () => this.onChange())
    }
  }

  // componentDidMount(){
  //   if(this.props.defaultValue) this.setState({value: this.props.defaultValue})
  // }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.changeAmount !== prevProps.changeAmount) {
      return { changeAmount: this.props.changeAmount }
    }
    return null;
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.changeAmount !== undefined) {
        this.setState({
          value: snapshot.changeAmount
        })
      }
    }  
  }

  // componentWillReceiveProps = (nextProps) => {
  //   console.log('nextProps.changeAmount ==>', nextProps.changeAmount);
  //   if(nextProps.changeAmount > 0) {
  //     this.setState({
  //       value: nextProps.changeAmount
  //     })
  //   }
  // }

  onChange = () => {
    this.props.onValueChange(this.state.value)
  }

  shouldComponentUpdate(nextProps, nextState){
    if(this.props !== nextProps) return true;
    if(this.state.value !== nextState.value) return true;
    return false;
  }

  render() {
    const { rounded, maxtotalunitcart, isExtras } = this.props;
    const { maxStock, bu } = this.state;
    const className = cx('o-counter', {
      rounded,
    });
    let {value} = this.state;
    return (
      <div>
        <div className={className}>
          <div className="counter-col">
            <button type="button" className="counter-decrement" onClick={this.decrement}>Dec</button>
          </div>
          <div className="counter-col">
            <div className="value">{value}</div>
          </div>
          <div className="counter-col">
            {(isExtras || bu==='0201' ) ?
            <button type="button" className="counter-increment" onClick={this.increment}>Inc</button> :
                <button type="button" disabled={maxtotalunitcart ? true : false } className="counter-increment" onClick={this.increment}>Inc</button>
            }
          </div>
        </div>
        { maxStock && <div className="stock-note">Available max stock {value} unit</div> }
      </div>
    );
  }
};

Counter.propTypes = {
  onValueChange: PropTypes.func,
  defaultValue: PropTypes.number,
  changeAmount: PropTypes.number,
  maxAvailable: PropTypes.number
}

const mapStateToProps = ({ carrental }) => {
    const { maxtotalunitcart } = carrental;
    return { maxtotalunitcart }
}

export default connect(mapStateToProps, {
  validateStockExtras
})(Counter);
