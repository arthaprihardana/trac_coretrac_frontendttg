import React, { Component } from "react";
import "./IDCard.scss";

class IDCard extends Component {
    state = {
        imgUpload: "",
        showEmpty: true
    };
    uploadPhotoChange = (e) => {
        this.setState({
            imgUpload: URL.createObjectURL(e.target.files[0]),
            showEmpty: false
        })
    }
    render() {
        const { isVerified, label } = this.props;
        const { imgUpload, showEmpty } = this.state;
        const { uploadPhotoChange } = this;
        return (
            <div className="m-id-card-photo dashboard-id">
                <div className="upload-area">
                    <div className="drag-zone plus">
                        {showEmpty && (
                            <div className="no-image-wrapper">
                                <div className="icon plus" />
                                <div className="description">{label}</div>
                            </div>
                        )}
                        {isVerified && (
                            <div className="verified">
                                <p className="verified-icon">Verified</p>
                            </div>
                        )}
                    </div>
                    {!showEmpty && (
                        <div className="img-uploaded">
                            <img src={imgUpload} alt="id-uploaded" />
                        </div>
                    )}
                    <input
                        type="file"
                        className="input-file"
                        onChange={uploadPhotoChange}
                    />
                </div>
            </div>
        );
    }
}

export default IDCard;
