import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import './Footer.scss'

class Footer extends PureComponent {
  state = {
    accordionOpen: '0'
  }

  handleAccordion = e => {
    if (window.outerWidth < 768) {
      if (this.state.accordionOpen !== e.currentTarget.dataset.id) {
        this.setState({ accordionOpen: e.currentTarget.dataset.id })
      } else {
        this.setState({ accordionOpen: '0' })
      }
      e.preventDefault()
    }
  }

  render () {
    return (
      <div className='o-footer'>
        <div className='container'>
          {/* Top Section */}
          <div className='top-section'>
            <Link to='/' className='trac-logo'>Trac</Link>
            <div className='footer-row'>
              <div className='footer-navigation-section'>
                <div className='footer-navigation-section-row'>
                  <div
                    className={`footer-col ${this.state.accordionOpen === '1' ? 'show' : ''}`}
                  >
                    <h5 className='footer-title has-submenu'>
                      <Link
                        to='/about-us'
                        data-id='1'
                        onClick={e => this.handleAccordion(e)}
                      >
                        Company
                      </Link>
                    </h5>
                    <ul className='footer-navigation'>
                      <li><Link to='/about-us'>About Us</Link></li>
                      <li><Link to='/career'>Career</Link></li>
                      <li><Link to='/blog'>Blog</Link></li>
                    </ul>
                  </div>
                  <div
                    className={`footer-col ${this.state.accordionOpen === '2' ? 'show' : ''}`}
                  >
                    <h5 className='footer-title has-submenu'>
                      <Link
                        to='/product-services'
                        data-id='2'
                        onClick={e => this.handleAccordion(e)}
                      >
                        Product & Services
                      </Link>
                    </h5>
                    <ul className='footer-navigation'>
                      <li>
                        <Link to='/corporate-car-service'>
                          Corporate Car Service
                        </Link>
                      </li>
                      <li>
                        <Link to='/driver-services'>Driver Services</Link>
                      </li>
                      <li>
                        <Link to='/corporate-bike-service'>
                          Corporate Bike Services
                        </Link>
                      </li>
                      <li><Link to='/bus-services'>Bus Services</Link></li>
                      <li><Link to='/trac-to-go'>Trac to Go</Link></li>
                    </ul>
                  </div>
                </div>
                <div className='footer-navigation-section-row'>
                  <div
                    className={`footer-col ${this.state.accordionOpen === '3' ? 'show' : ''}`}
                  >
                    <h5 className='footer-title has-submenu'>
                      <Link
                        to='/support'
                        data-id='3'
                        onClick={e => this.handleAccordion(e)}
                      >
                        Support
                      </Link>
                    </h5>
                    <ul className='footer-navigation'>
                      <li><Link to='/help-center'>Help Center</Link></li>
                      <li><Link to='/branch-locator'>Branch Locator</Link></li>
                      <li><Link to='/contact-us'>Contact Us</Link></li>
                    </ul>
                  </div>
                  <div className='footer-col'>
                    <h5 className='footer-title'>
                      <Link to='/promo'>Promo</Link>
                    </h5>
                  </div>
                </div>
              </div>
              <div className='footer-right-section'>
                <div className='form-section'>
                  <h5 className='footer-title'>Subscribe to our newsletter</h5>
                  <form action='#'>
                    <input
                      type='text'
                      className='input-text'
                      placeholder='Your email'
                    />
                    <button type='submit' className='submit-button'>
                      Send
                    </button>
                  </form>
                </div>
                <div className='social-media-section'>
                  <h6>Follow us on social media</h6>
                  <ul className='social-media-list'>
                    <li>
                      <Link to='/#instagram' className='icon-instagram'>
                        Instagram
                      </Link>
                    </li>
                    <li>
                      <Link to='/#facebook' className='icon-facebook'>
                        Facebook
                      </Link>
                    </li>
                    <li>
                      <Link to='/#twitter' className='icon-twitter'>
                        Twitter
                      </Link>
                    </li>
                    <li>
                      <Link to='/#youtube' className='icon-youtube'>
                        Youtube
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className='footer-row'>
              <div className='footer-navigation-inline'>
                <Link to='/terms-and-conditions'>Terms and Conditions</Link>
                <Link to='/privacy-policy'>Privacy Policy</Link>
              </div>
            </div>
          </div>
          {/* Bottom Section */}
          <div className='bottom-section'>
            <p className='copyright'>© 2018 Astra Trac. All rights reserved.</p>
          </div>
        </div>
      </div>
    )
  }
}

export default Footer
