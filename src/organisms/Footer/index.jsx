import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import './Footer.scss'
import lang from "../../assets/data-master/language";

class Footer extends PureComponent {
  state = {
    accordionOpen: '0'
  }

  handleAccordion = e => {
    if (window.outerWidth < 768) {
      if (this.state.accordionOpen !== e.currentTarget.dataset.id) {
        this.setState({ accordionOpen: e.currentTarget.dataset.id })
      } else {
        this.setState({ accordionOpen: '0' })
      }
      e.preventDefault()
    }
  }

  render () {
    return (
      <div className='o-footer'>
        <div className='container'>
          {/* Top Section */}
          <div className='top-section'>
            <Link to='/' className='trac-logo'>Trac</Link>
            <div className='footer-row'>
              <div className='footer-navigation-section'>
                <div className='footer-navigation-section-row'>
                  <div
                    className={`footer-col ${this.state.accordionOpen === '1' ? 'show' : ''}`}
                  >
                    <h5 className='footer-title has-submenu'>
                      <Link
                        to='/about-us'
                        data-id='1'
                        onClick={e => this.handleAccordion(e)}
                      >
                          {lang.company[lang.default]}
                      </Link>
                    </h5>
                    <ul className='footer-navigation'>
                      <li><Link to='/about-us'>{lang.aboutUs[lang.default]}</Link></li>
                      {/*<li><Link to='/career'>{lang.career[lang.default]}</Link></li>*/}
                      <li><Link to='/news'>{lang.blog[lang.default]}</Link></li>
                    </ul>
                  </div>
                  <div
                    className={`footer-col ${this.state.accordionOpen === '2' ? 'show' : ''}`}
                  >
                    <h5 className='footer-title has-submenu'>
                      <Link
                        to='/product-services'
                        data-id='2'
                        onClick={e => this.handleAccordion(e)}
                      >
                          {lang.productService[lang.default]}
                      </Link>
                    </h5>
                    <ul className='footer-navigation'>
                      <li>
                        <Link to='/corporate-car-service'>
                            {lang.corporateCarService[lang.default]}
                        </Link>
                      </li>
                      <li>
                        <Link to='/detail-product-rental'>{lang.dailyCarRental[lang.default]}</Link>
                      </li>
                      <li><Link to='/bus-services'>
                          {lang.busService[lang.default]}
                      </Link></li>
                        <li>
                            <Link to='/detail-product-trac-data'>
                                {lang.tracFMS[lang.default]}
                            </Link>
                        </li>
                      {/*<li><Link to='/trac-to-go'>*/}
                          {/*{lang.tracToGo[lang.default]}*/}
                      {/*</Link></li>*/}
                    </ul>
                  </div>
                </div>
                <div className='footer-navigation-section-row'>
                  <div
                    className={`footer-col ${this.state.accordionOpen === '3' ? 'show' : ''}`}
                  >
                    <h5 className='footer-title has-submenu'>
                      <Link
                        to='/support'
                        data-id='3'
                        onClick={e => this.handleAccordion(e)}
                      >
                          {lang.support[lang.default]}
                      </Link>
                    </h5>
                    <ul className='footer-navigation'>
                      <li><Link to='/faq'>FAQ</Link></li>
                        <li><Link to='/outlet-locations'>{lang.branchLocation[lang.default]}</Link></li>
                      <li><Link to='/contact-us'>{lang.contactUs[lang.default]}</Link></li>
                    </ul>
                  </div>
                  <div className='footer-col'>
                    <h5 className='footer-title'>
                      <Link to='/promo'>{lang.promo[lang.default]}</Link>
                    </h5>
                  </div>
                </div>
              </div>
              <div className='footer-right-section'>
                <div className='form-section'>
                  <h5 className='footer-title'>{lang.subcribeToNews[lang.default]}</h5>
                  <form action='#'>
                    <input
                      type='text'
                      className='input-text'
                      placeholder={lang.insertYourEmail[lang.default]}
                    />
                    <button className='submit-button' disabled>
                        {lang.sendButton[lang.default]}
                    </button>
                  </form>
                </div>
                <div className='social-media-section'>
                  <h6>{lang.followUs[lang.default]}</h6>
                  <ul className='social-media-list'>
                    <li>
                        <a href={lang.instagramLink[lang.default]} className='icon-instagram'> </a>
                      {/*<Link to={lang.instagramLink[lang.default]} className='icon-instagram'>*/}
                        {/*Instagram*/}
                      {/*</Link>*/}
                    </li>
                    <li>
                        <a href={lang.facebookLink[lang.default]} className='icon-facebook'> </a>
                      {/*<Link to={lang.facebookLink[lang.default]} className='icon-facebook'>*/}
                        {/*Facebook*/}
                      {/*</Link>*/}
                    </li>
                    <li>
                        <a href={lang.twitterLink[lang.default]} className='icon-twitter'> </a>
                      {/*<Link to={lang.twitterLink[lang.default]} className='icon-twitter'>*/}
                        {/*Twitter*/}
                      {/*</Link>*/}
                    </li>
                    <li>
                        <a href={lang.youtubeLink[lang.default]} className='icon-youtube'> </a>
                      {/*<Link to={lang.youtubeLink[lang.default]} className='icon-youtube'>*/}
                        {/*Youtube*/}
                      {/*</Link>*/}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className='footer-row'>
              <div className='footer-navigation-inline'>
                <Link to='/terms-and-conditions'>{lang.termsAndConditions[lang.default]}</Link>
                <Link to='/privacy-policy'>{lang.privacyPolicy[lang.default]}</Link>
                  <Link to='/link'>Link</Link>
              </div>
            </div>
          </div>
          {/* Bottom Section */}
          <div className='bottom-section'>
            <p className='copyright'>© 2019 Astra Trac. All rights reserved.</p>
          </div>
        </div>
      </div>
    )
  }
}

export default Footer
