import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import uuid from 'uuid/v4';

// components
import { Image } from 'atom';

// images
import listDealImage1 from 'assets/images/dummy/deal-car-1.jpg';
import listDealImage2 from 'assets/images/dummy/deal-car-2.jpg';
import listDealImage3 from 'assets/images/dummy/deal-car-3.jpg';

// style
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './ListDeal.scss';

class ListDeal extends PureComponent {

  state = {
    runSlick: true,
  }

  componentDidMount() {
    this.handleWindowResize();
    window.addEventListener('resize', this.handleWindowResize);
  }

  handleWindowResize = () => {
    if (window.innerWidth < 768) {
      this.setState({ runSlick: false });
    } else {
      this.setState({ runSlick: true });
    }
  }

  render() {
    const { data, config } = this.props;
    const { runSlick } = this.state;

    let listDealContent = '';

    if (runSlick) {
      listDealContent = (
        <div className="m-list-deal">
          <Slider {...config}>
            {data.map(item => (
              <div className="list-deal-item" key={uuid()}>
                <Link to={item.link}>
                  <Image src={item.imagePath} alt={item.alt} />
                </Link>
              </div>
            ))}
          </Slider>
        </div>
      );
    } else {
      listDealContent = (
        <div className="m-list-deal">
          {data.map(item => (
            <div className="list-deal-item" key={uuid()}>
              <Link to={item.link}>
                <Image src={item.imagePath} alt={item.alt} />
              </Link>
            </div>
          ))}
        </div>
      );
    }

    return (
      listDealContent
    );

  }
}

ListDeal.defaultProps = {
  config: {
    dots: false,
    lazyLoad: false,
    infinite: false,
    autoplay: false,
    arrows: true,
    speed: 650,
    slidesToShow: 3,
    slidesToScroll: 1,
  },
  data: [
    {
      imagePath: listDealImage1,
      alt: 'Image List Deal 1',
      link: '/deal/1',
    },
    {
      imagePath: listDealImage2,
      alt: 'Image List Deal 2',
      link: '/deal/2',
    },
    {
      imagePath: listDealImage3,
      alt: 'Image List Deal 3',
      link: '/deal/3',
    },
    {
      imagePath: listDealImage1,
      alt: 'Image List Deal 4',
      link: '/deal/4',
    },
  ],
};

ListDeal.propTypes = {
  config: PropTypes.object,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      imagePath: PropTypes.string,
      link: PropTypes.string,
    }),
  ),
};

export default ListDeal;
