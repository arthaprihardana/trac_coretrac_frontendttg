import React, { PureComponent } from "react";
import PropTypes from "prop-types";

// style
import "./SidebarBookingDetail.scss";

class SidebarBookingDetail extends PureComponent {
    // initialize state
    state = { showBookingDetail: false };

    // handle on click
    handleClick = () => {
        this.setState({ showBookingDetail: !this.state.showBookingDetail });
    };

    render() {
        // props
        const { data } = this.props;

        return (
            <div
                className={`m-sidebar-booking-detail ${
                    this.state.showBookingDetail ? "show" : ""
                }`}
            >
                <div
                    className="booking-detail-title"
                    onClick={this.handleClick}
                >
                    Booking Detail
                    <span />
                </div>
                <div className="booking-detail-content">
                    <div className="item">
                        <div className="label">Packages</div>
                        <div className="description">{`${data.packages.hours} ${
                            data.packages.hours > 1 ? "Hours" : "Hour"
                        } / Day`}</div>
                    </div>
                    <div className="item">
                        <div className="label">Dates</div>
                        <div className="description">
                            {data.dates.startDateRent}
                            <div className="arrow-right" />
                            {data.dates.endDateRent}
                            <br />
                            {`Start ${data.dates.startHourRent} - End ${
                                data.dates.endHourRent
                            }`}
                        </div>
                    </div>
                    <div className="item">
                        <div className="label">Pick up Location</div>
                        <div className="description">{data.pickUpLocation}</div>
                    </div>
                </div>
            </div>
        );
    }
}

SidebarBookingDetail.propTypes = {
    data: {}
};

SidebarBookingDetail.propTypes = {
    data: PropTypes.object
};
export default SidebarBookingDetail;