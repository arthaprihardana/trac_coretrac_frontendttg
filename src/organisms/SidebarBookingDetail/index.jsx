/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-28 00:39:19 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-22 11:16:40
 */
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import moment from 'moment';
import 'moment/locale/id';
import _ from "lodash";

// style
import "./SidebarBookingDetail.scss";

// helpers
import { localStorageDecrypt } from "helpers";
import lang from '../../assets/data-master/language'

class SidebarBookingDetail extends PureComponent {

    // initialize state
    state = { showBookingDetail: false };

    // handle on click
    handleClick = () => {
        this.setState({ showBookingDetail: !this.state.showBookingDetail });
    };

    bookingDetailCarRental = () => {
        const { data } = this.props;
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        let rangeDay = moment(CarRentalFormInput.date.endDate).diff(moment(CarRentalFormInput.date.startDate), 'days');
        return (
            <div className="booking-detail-content">
                <div className="item">
                    <div className="label">Packages</div>
                    <div className="description">{`${data.select_package} ${
                        data.select_package > 1 ? "Hours" : "Hour"
                    } / Day`}</div>
                </div>
                <div className="item">
                    <div className="label">Dates</div>
                    <div className="description">
                        {data.date !== undefined && moment(data.date.startDate).format('LL')}
                        {data.select_package > 4 && <div className="arrow-right" /> }
                        {data.select_package > 4 && data.date !== undefined && moment(data.date.endDate).format('LL')}
                        {" "}{rangeDay > 0 && `(${(rangeDay + 1)} days)`}
                        <br />
                        {data.date !== undefined && `Pick Up Time ${data.date.time} `}
                        {/* {data.select_package > 4 && `- End ${data.date.time}`} */}
                    </div>
                </div>
                <div className="item">
                    <div className="label">Pick up Location</div>
                    <div className="description">{data.address !== undefined && data.address.text}</div>
                </div>
            </div>
        );
    }

    bookingDetailAirportTransfer = () => {
        const { data } = this.props;
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        let rangeDay = moment(CarRentalFormInput.data[0].date.endDate).diff(moment(CarRentalFormInput.data[0].date.startDate), 'days');
        return (
            <div className="booking-detail-content">
                <div className="item">
                    <div className="label">Packages</div>
                    <div className="description">{`Airport Transfer (${_.upperFirst(_.split(data.type_service, "-").join(" "))}) / Day`}</div>
                </div>
                <div className="item">
                    <div className="label">Dates</div>
                    <div className="description">
                        {Object.keys(data).length > 0 && data.data[0].date !== undefined && moment(data.data[0].date.startDate).format('LL')}
                        {Object.keys(data).length > 0 && data.data[0].select_package > 4 && <div className="arrow-right" /> }
                        {Object.keys(data).length > 0 && data.data[0].select_package > 4 && data.data[0].date !== undefined && moment(data.data[0].date.endDate).format('LL')}
                        {" "}{rangeDay > 0 && `(${(rangeDay + 1)} days)`}
                        <br />
                        {Object.keys(data).length > 0 && data.data[0].date !== undefined && `Pick Up Time ${data.data[0].date.time} `}
                    </div>
                </div>
                <div className="item">
                    <div className="label">Pick up Location</div>
                    <div className="description">{Object.keys(data).length > 0 && data.data[0].airport !== undefined && data.data[0].airport.text}</div>
                </div>
            </div>
        )
    }

    bookingDetailBusRental = () => {
        const { data } = this.props;
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        let rangeDay = moment(CarRentalFormInput.date.endDate).diff(moment(CarRentalFormInput.date.startDate), 'days');
        return (
            <div className="booking-detail-content">
                <div className="item">
                    <div className="label">
                        {lang.dates[lang.default]}
                    </div>
                    <div className="description">
                        {data.date !== undefined && moment(data.date.startDate).format('LL')}
                        {data.select_package > 4 && <div className="arrow-right" /> }
                        {data.select_package > 4 && data.date !== undefined && moment(data.date.endDate).format('LL')}
                        {" "}{rangeDay > 0 && `(${(rangeDay + 1)} days)`}
                        <br />
                        {data.date !== undefined && `${lang.pickupTime[lang.default]}  ${data.date.time} `}
                        {/* {data.select_package > 4 && `- End ${data.date.time}`} */}
                    </div>
                </div>
                <div className="item">
                    <div className="label">{lang.pickUpLocation[lang.default]}</div>
                    <div className="description">{data.address !== undefined && data.address.text}</div>
                </div>
            </div>
        );
    }

    render() {
        // props
        const { type } = this.props;
        let detailContent;

        switch (type) {
            case "airport-transfer":
                detailContent = this.bookingDetailAirportTransfer();
                break;
            case "bus-rental": 
                detailContent = this.bookingDetailBusRental();
                break;
            default:
                detailContent = this.bookingDetailCarRental();
                break;
        }

        return (
            <div
                className={`m-sidebar-booking-detail ${
                    this.state.showBookingDetail ? "show" : ""
                }`}>
                <div
                    className="booking-detail-title"
                    onClick={this.handleClick}>
                    {lang.bookingDetail[lang.default]} <span />
                </div>
                { detailContent }
            </div>
        )
    }
}

SidebarBookingDetail.propTypes = {
    data: {}
};

SidebarBookingDetail.propTypes = {
    data: PropTypes.object
};
export default SidebarBookingDetail;