import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import { Rupiah } from "atom";

// style
import "./TotalPaymentMobile.scss";

class TotalPaymentMobile extends PureComponent {

    // handle on click
    handleClick = () => {
        this.props.returnShowDetail(true);
    };

    render() {
        const { handleClick } = this;
        const { showDetail } = this.props;

        let className = 'o-total-payment-mobile';
        if ( showDetail ) {
            className += ' hide';
        }

        return(
            <div className={ className }>
                <div className="summary-total">
                    <div className="total-payment-label">
                        Total Payment
                    </div>
                    <div className="total-payment-ammount">
                        <Rupiah value={1950000} /> <span className="price-per">/ 3 days</span>
                    </div>
                </div>
                <div className="summary-button">
                    <div className="button-detail" onClick={ handleClick }>
                        Detail
                    </div>
                </div>
            </div>
        );
    };
}

TotalPaymentMobile.defaultProps = {
    showDetail: false,
    returnShowDetail: () => console.log(),
};

TotalPaymentMobile.propTypes = {
    showDetail: PropTypes.bool,
    returnShowDetail: PropTypes.func,
};

export default TotalPaymentMobile;
