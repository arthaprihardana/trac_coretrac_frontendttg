import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Toggle.scss';

class Toggle extends Component {
    state = {
        value: false
    }

    componentDidMount() {
        if (this.props.defaultValue) {
            this.setState({
                value: this.props.defaultValue
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.defaultValue !== undefined) {
            this.setState({ value: nextProps.defaultValue });
        }
    }

    onToggleChange = () => {
        this.setState({
            value: !this.state.value
        }, () => {
            this.props.onValueChange(this.state.value);
        })
    }

    shouldComponentUpdate(nextProps){
        if(this.props.defaultValue !== nextProps.defaultValue) return true;
        return false;
    }

    render() {
        let { id } = this.props;
        let { value } = this.state;
        return (
            <div className="o-toggle-input">
                <input
                    type="checkbox"
                    className="input-checkbox"
                    // checked={value}
                    checked={this.props.defaultValue}
                    id={id}
                    onChange={this.onToggleChange}
                />
                <label htmlFor={id} />
            </div>
        )
    }
}

Toggle.propTypes = {
    id: PropTypes.string.isRequired,
    onToggleChange: PropTypes.func,
    defaultValue: PropTypes.bool
}

export default Toggle;