/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-01 10:01:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 00:49:57
 */
import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import "./SidebarNote.scss";
import { connect } from 'react-redux';
import { notesRequestCarRental } from "actions";
import lang from '../../assets/data-master/language'

class SidebarNote extends PureComponent {

    state = {
        showNoteDetail: false,
        note: null
    };

    componentDidMount = () => {
        this.setState({ note: this.props.notes })
    }

    handleClick = () => {
        this.setState({ showNoteDetail: !this.state.showNoteDetail });
    };

    onChangeValue = (e) => {
        this.props.notesRequestCarRental(e.currentTarget.value)
    }

    render(){

        const {
            handleClick,
            state: { showNoteDetail, note },
            props: { hasContent },
        } = this;

        let className = 'o-sidebar-note';
        if ( hasContent ) {
            className = 'o-sidebar-note has-content';
        }
        if ( showNoteDetail && hasContent ) {
            className = 'o-sidebar-note has-content show';
        } else if ( !showNoteDetail && hasContent ) {
            className = 'o-sidebar-note has-content';
        }

        return (
            <div className={ className }>
                { !hasContent &&
                    <div className="note-title">
                        {lang.notesRequest[lang.default]} <span />
                    </div>
                }
                { hasContent &&
                    <div className="note-title" onClick={ handleClick }>
                        {lang.notesRequest[lang.default]} <span />
                    </div>
                }
                <div className="note-content">
                    { !hasContent &&
                        <Fragment>
                            <div className="input">
                                <input
                                    type="text"
                                    className="input-text"
                                    placeholder={lang.pleaseEnterLocation[lang.default]}
                                    onChange={this.onChangeValue}
                                />
                                <div className="edit">{lang.edit[lang.default]}</div>
                            </div>
                            <div className="note">
                                {lang.notesNote[lang.default]}
                            </div>
                        </Fragment>
                    }
                    { hasContent &&
                        <div className="text">
                            { note !== null && note !== "null" ? note : "-" }
                        </div>
                    }
                </div>
            </div>
        );
    }
}

SidebarNote.defaultProps = {
    hasContent: false,
};

SidebarNote.propTypes = {
    hasContent: PropTypes.bool,
};

const mapStateToProps = ({ carrental }) => {
    const { notes } = carrental;
    return { notes }
}

export default connect(mapStateToProps, {
    notesRequestCarRental
})(SidebarNote);