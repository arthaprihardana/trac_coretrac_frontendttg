import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import "./SidebarNote.scss";

class SidebarNote extends PureComponent {

    state = {
        showNoteDetail: false,
    };

    handleClick = () => {
        this.setState({ showNoteDetail: !this.state.showNoteDetail });
    };

    render(){

        const {
            handleClick,
            state: { showNoteDetail },
            props: { hasContent },
        } = this;

        let className = 'o-sidebar-note';
        if ( hasContent ) {
            className = 'o-sidebar-note has-content';
        }
        if ( showNoteDetail && hasContent ) {
            className = 'o-sidebar-note has-content show';
        } else if ( !showNoteDetail && hasContent ) {
            className = 'o-sidebar-note has-content';
        }

        return (
            <div className={ className }>
                { !hasContent &&
                    <div className="note-title">
                        Notes/Request <span />
                    </div>
                }
                { hasContent &&
                    <div className="note-title" onClick={ handleClick }>
                        Notes/Request <span />
                    </div>
                }
                <div className="note-content">
                    { !hasContent &&
                        <Fragment>
                            <div className="input">
                                <input
                                    type="text"
                                    className="input-text"
                                    placeholder="Please enter Location Detail or other Request"
                                />
                                <div className="edit">Edit</div>
                            </div>
                            <div className="note">
                                *Lorem ipsum dolor sit amet sid ut parnum sadum par et um
                            </div>
                        </Fragment>
                    }
                    { hasContent &&
                        <div className="text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum quia recusandae
                            accusamus! Sapiente nisi esse facilis saepe autem quod doloribus?
                          </div>
                    }
                </div>
            </div>
        );
    }
}

SidebarNote.defaultProps = {
    hasContent: false,
};

SidebarNote.propTypes = {
    hasContent: PropTypes.bool,
};

export default SidebarNote;