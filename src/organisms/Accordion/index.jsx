import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './Accordion.scss';

class Accordion extends PureComponent {
    state = {
        show: false
    }
    componentDidMount(){
        if(this.props.defaultValue){
            this.setState({show: this.props.defaultValue})
        }
    }
    handleToggle = () => {
        this.setState({show: !this.state.show})
    }
    render() {
        const {show} = this.state;
        const {children, title, driverInfo} = this.props;
        const classTitle = cx('accordion-title', {
            'show': show
        })
        const classContent = cx('accordion-content', {
            'show': show
        })
        return (
            <div className="accordion-wrapper">
                {driverInfo && <p className="driver-info-title" onClick={this.handleToggle}>{title}</p>}
                {!driverInfo && <p className={classTitle} onClick={this.handleToggle}>{title}</p>}
                <div className={classContent}>
                    {children}
                </div>
            </div>
        )
    }
}

Accordion.defaultProps = {
    defaultValue: false
}

Accordion.propTypes = {
    title: PropTypes.string.isRequired,
    driverInfo: PropTypes.bool,
    defaultValue: PropTypes.bool,
    children: PropTypes.any
}

export default Accordion;