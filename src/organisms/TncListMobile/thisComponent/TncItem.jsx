import React, { Component, Fragment } from 'react';
import cx from 'classnames';

class TncItem extends Component {
    state = {
        showContent: false
    }

    toggleShow = () => {
        this.setState({
            showContent: !this.state.showContent
        })
    }

    render(){
        const {
            props: {
                data
            },
            state: {
                showContent
            },
            toggleShow
        } = this;

        const classContent = cx('content-list', {
            'show' : showContent
        })

        return (
            <Fragment>
                <p className="list-title" onClick={toggleShow}>{data.title}</p>
                <div className={classContent}>
                    <ol>
                        {
                            data && data.content.length > 0 ?
                            data.content.map((content, i) => {
                                return <li key={i}>{content}</li>
                            })
                            : <li></li>
                        }
                    </ol>
                </div>
            </Fragment>
        )
    }
}

export default TncItem;