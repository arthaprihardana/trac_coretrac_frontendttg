import React, { Component } from 'react';
import './TncListMobile.scss';
import TncItem from './thisComponent/TncItem';
import TncDummy from 'assets/data-dummy/tnc.json';
import { Icon } from '../../atom';

class TncListMobile extends Component {

    state={
        tncData : TncDummy
    }

    handleClose = () => {
        this.props.close();
    }

    render() {
        const {
            state: {
                tncData
            },
            handleClose
        } = this;

        return (
            <div className="confirm-order-mobile">
                <div className="header">
                    <div className="icon-close" onClick={handleClose}>
                        <Icon name="close-blue" />
                    </div>
                </div>
                <div className="list-tnc">
                    {
                        tncData.map(tnc => {
                            return <TncItem key={tnc.id} data={tnc} />
                        })
                    }
                </div>
            </div>
        )
    }
}

export default TncListMobile;