/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-23 21:57:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 23:38:21
 */
import React from 'react';
import { hydrate } from 'react-dom';
import Loadable from 'react-loadable';
import { Provider as ReduxProvider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';

import 'styles/index.scss';
import App from 'App';
import configureStore from 'store/configureStore';
import registerServiceWorker from './registerServiceWorker';

const store = configureStore( window.__REDUX_STATE__ || {} );
const supportsHistory = 'pushState' in window.history;

const AppBundle = (
    <ReduxProvider store={store.store}>
        <PersistGate loading={null} persistor={store.persistor}>
            <BrowserRouter forceRefresh={!supportsHistory}>
                <App />
            </BrowserRouter>
        </PersistGate>
    </ReduxProvider>
);

window.onload = () => {
    Loadable.preloadReady().then(() => {
        hydrate(
            AppBundle,
            document.getElementById('root')
        );
    });
};

registerServiceWorker();
