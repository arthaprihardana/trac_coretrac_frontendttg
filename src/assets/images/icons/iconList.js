import arrowRightOrange from './arrow-right-orange.svg';
import closeBlue from './close-blue.svg';

export const iconList = {
    'arrow-right-orange': arrowRightOrange,
    'close-blue': closeBlue
}