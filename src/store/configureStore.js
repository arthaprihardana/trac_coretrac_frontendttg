/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-23 22:05:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 17:23:50
 */
import {
    createStore,
    compose,
    applyMiddleware
} from 'redux';
import { persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import reducers from 'reducers';
import rootSaga from 'sagas';
import { composeWithDevTools } from 'redux-devtools-extension';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

export default function configureStore(initialState) {

    const store = createStore(
        reducers,
        initialState,
        composeWithDevTools(
            compose(applyMiddleware(...middlewares))
        )
    );

    let persistor = persistStore(store)

    sagaMiddleware.run(rootSaga);
    
    return { store, persistor };
    // return store;
}