/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-03 11:09:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-15 10:37:25
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { postRegisterSuccess, postRegisterFailure } from "actions";
import { POST_REGISTER } from "actions/Types";
import Axios from "axios";
import { REGISTER } from "../constant";

const postRegisterRequest = async (FormData) => {
    return await Axios.post(REGISTER, {
        EmailPersonal: FormData.email,
        Password: FormData.password
    }, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postRegisterToServer({ payload }) {
    const { FormData } = payload;
    try {
        const create = yield call(postRegisterRequest, FormData);
        if(create.data.Status === 201) {
            yield put(postRegisterSuccess(create.data.Data));
        } else {
            yield put(postRegisterFailure(create.data.ErrorMessage));
        }
    } catch (error) {
        yield put(postRegisterFailure('Oops! something went wrong!'));
    }
}

export function* postRegisterSaga() {
    yield takeEvery(POST_REGISTER, postRegisterToServer);
}

export default function* rootSaga() {
    yield all([
        fork(postRegisterSaga)
    ]);
}