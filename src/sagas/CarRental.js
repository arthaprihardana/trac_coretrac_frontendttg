/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-07 15:07:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-23 23:00:59
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { getStockCarRentalSuccess, getStockCarRentalFailure, getPackageCarRentalSuccess, getPackageCarRentalFailure, getCityCoverageCarRentalSuccess, getCityCoverageCarRentalFailure, getExtrasCarRentalSuccess, getExtrasCarRentalFailure, getPriceCarRentalSuccess, getPriceCarRentalFailure, postReservationCarRentalSuccess, postReservationCarRentalFailure } from 'actions';
import { GET_STOCK_CAR_RENTAL, GET_PACKAGE_CAR_RENTAL, GET_CITY_COVERAGE_CAR_RENTAL, GET_EXTRAS_CAR_RENTAL, GET_CAR_RENTAL_PRICE, POST_RESERVATION_CAR_RENTAL } from 'actions/Types';
import { objectToQueryString, localStorageDecrypt } from "helpers";
import Axios from 'axios';
import {
    RENTAL_DURATION_CAR_RENTAL,
    CITY_COVERAGE_CAR_RENTAL,
    LIST_STOCK_CAR_RENTAL,
    SM_APP_KEY,
    EXTRAS_CAR_RENTAL,
    PRICE_PRODUCT_CAR_RENTAL,
    RESERVATION_CAR_RENTAL,
    TMP_TOKEN
} from '../constant';
import db from '../db';

// GET PACKAGE CAR RENTAL START HERE
const getPackageCarRental = async () => {
    return await Axios.get(RENTAL_DURATION_CAR_RENTAL, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${TMP_TOKEN}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getPackageCarRentalFromServer() {
    try {
        let pcr = yield call(getPackageCarRental);
        yield put(getPackageCarRentalSuccess(pcr.data.Data));
    } catch (error) {
        yield put(getPackageCarRentalFailure(error));
    }
}

export function* getPackageCarRentalSaga() {
    yield takeEvery(GET_PACKAGE_CAR_RENTAL, getPackageCarRentalFromServer);
}
// GET PACKAGE CAR RENTAL END HERE

// CITY COVERAGE CAR RENTAL START HERE
const getCityCoverageCarRental = async (params) => {
    //  CID-999999
    let API = CITY_COVERAGE_CAR_RENTAL(params);
    return await Axios.get(API, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${TMP_TOKEN}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCityCoverageCarRentalFromServer({ payload }) {
    try {
        let pcr = yield call(getCityCoverageCarRental, payload);
        yield put(getCityCoverageCarRentalSuccess(pcr.data.Data));
    } catch (error) {
        yield put(getCityCoverageCarRentalFailure(error));
    }
}

export function* getCityCoverageCarRentalSaga() {
    yield takeEvery(GET_CITY_COVERAGE_CAR_RENTAL, getCityCoverageCarRentalFromServer);
}
// CITY COVERAGE CAR RENTAL END HERE

// GET STOCK CAR RENTAL START HERE
const getStockCarRentalRequest = async (params) => {
    // ?BusinessUnitId=0102&BranchId=SL44&StartDate=2019-01-16T07:00:00.000Z&EndDate=2019-01-16T12:00:00.000Z&IsWithDriver =1
    return await Axios.get(`${LIST_STOCK_CAR_RENTAL}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json',
            'App-Key': SM_APP_KEY
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getStockCarRentalFromServer({ payload }) {
    try {
        let response = yield call(getStockCarRentalRequest, payload);
        yield put(getStockCarRentalSuccess(response.data));
    } catch (error) {
        yield put(getStockCarRentalFailure(error));
    }
}

export function* getStockCarRentalSaga() {
    yield takeEvery(GET_STOCK_CAR_RENTAL, getStockCarRentalFromServer);
}
// GET STOCK CAR RENTAL END HERE

// GET EXTRAS CAR RENTAL START HERE
const getExtrasCarRentalRequest = async (params) => {
    return await Axios.get(`${EXTRAS_CAR_RENTAL}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${TMP_TOKEN}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getExtrasCarRentalFromServer({ payload }) {
    try {
        let response = yield call(getExtrasCarRentalRequest, payload);
        yield put(getExtrasCarRentalSuccess(response.data));
    } catch (error) {
        yield put(getExtrasCarRentalFailure(error));
    }
}

export function* getExtrasCarRentalSaga() {
    yield takeEvery(GET_EXTRAS_CAR_RENTAL, getExtrasCarRentalFromServer);
}
// GET EXTRAS CAR RENTAL END HERE

// GET PRICE START HERE
const getPriceCarRentalRequest = async params => {
    return await Axios.get(`${PRICE_PRODUCT_CAR_RENTAL}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${TMP_TOKEN}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getPriceCarRentalFromServer({ payload }) {
    try {
        let response = yield call(getPriceCarRentalRequest, payload);
        yield put(getPriceCarRentalSuccess(response.data));
    } catch (error) {
        yield put(getPriceCarRentalFailure(error));
    }
}

export function* getPriceCarRentalSaga() {
    yield takeEvery(GET_CAR_RENTAL_PRICE, getPriceCarRentalFromServer);
}
// GET PRICE END HERE

// SAVE RESERVATION START HERE
const postReservationCarRentalRequest = async FormData => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.post(RESERVATION_CAR_RENTAL, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postReservationCarRentalToServer({ payload }) {
    try {
        let response = yield call(postReservationCarRentalRequest, payload);
        yield put(postReservationCarRentalSuccess(response.data));
    } catch (error) {
        yield put(postReservationCarRentalFailure(error));
    }
}

export function* postReservationCarRentalSaga() {
    yield takeEvery(POST_RESERVATION_CAR_RENTAL, postReservationCarRentalToServer);
}
// SAVE RESERVATION END HERE

export default function* rootSaga() {
    yield all([
        fork(getPackageCarRentalSaga),
        fork(getStockCarRentalSaga),
        fork(getCityCoverageCarRentalSaga),
        fork(getExtrasCarRentalSaga),
        fork(getPriceCarRentalSaga),
        fork(postReservationCarRentalSaga)
    ]);
}