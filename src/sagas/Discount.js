/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-22 14:03:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-22 14:07:07
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { objectToQueryString } from "helpers";
import _ from "lodash";
import { getValidatePromoSuccess, getValidatePromoFailure } from "../actions";
import { GET_VALIDATE_PROMO } from "../actions/Types";
import { VALIDATE_PROMO } from "../constant";

const getValidatePromoRequest = async params => {
    let prm = _.omit(params, ['PromoCode'])
    return await Axios.get(`${VALIDATE_PROMO(params.PromoCode)}?${objectToQueryString(prm)}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getValidatePromoFromServer({ payload }) {
    try {
        let response = yield call(getValidatePromoRequest, payload);
        yield put(getValidatePromoSuccess(response.data));
    } catch (error) {
        yield put(getValidatePromoFailure(error));
    }
}

export function* getValidatePromoSaga() {
    yield takeEvery(GET_VALIDATE_PROMO, getValidatePromoFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getValidatePromoSaga)
    ]);
}