/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-24 19:24:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 19:25:01
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from "axios";
import { ADJUSTMENT_RETAIL } from "../constant";
import { getAdjustmentRetailSuccess, getAdjustmentRetailFailure } from "../actions";
import { GET_ADJUSTMENT_RETAIL } from "../actions/Types";

const getAdjustmentRetailRequest = async params => {
    return await Axios.get(`${ADJUSTMENT_RETAIL}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getAdjustmentRetailFromServer({ payload }) {
    try {
        let response = yield call(getAdjustmentRetailRequest, payload);
        yield put(getAdjustmentRetailSuccess(response.data.Data));
    } catch (error) {
        yield put(getAdjustmentRetailFailure(error));
    }
}

export function* getAdjustmentRetailSaga() {
    yield takeEvery(GET_ADJUSTMENT_RETAIL, getAdjustmentRetailFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getAdjustmentRetailSaga)
    ]);
}