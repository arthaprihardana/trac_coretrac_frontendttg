/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-12 14:00:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-12 17:31:58
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { DURATION_AIRPORT, TMP_TOKEN } from "../constant";
import { getDurationAirportTransferSuccess, getDurationAirportTransferFailure } from "../actions/AirportTransfer";
import { GET_DURATION_AIRPORT_TRANSFER } from "../actions/Types";

// GET DURATION AIRPORT TRANSFER START HERE
const getDurationAirportTransfer = async params => {
    return await Axios.get(`${DURATION_AIRPORT([params.BusinessUnitId, params.MsProductId, params.Distance])}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${TMP_TOKEN}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDurationAirportTransferFromServer({ payload }) {
    try {
        let response = yield call(getDurationAirportTransfer, payload);
        yield put(getDurationAirportTransferSuccess(response.data.Data));
    } catch (error) {
        yield put(getDurationAirportTransferFailure(error));
    }
}

export function* getDurationAirportTransferSaga() {
    yield takeEvery(GET_DURATION_AIRPORT_TRANSFER, getDurationAirportTransferFromServer);
}
// GET DURATION AIRPORT TRANSFER END HERE

export default function* rootSaga() {
    yield all([
        fork(getDurationAirportTransferSaga)
    ]);
}