/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-04 10:04:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-15 16:49:36
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import { postForgotPasswordSuccess, postForgotPasswordFailure, postNewPasswordSuccess, postNewPasswordFailure } from "actions";
import { POST_FORGOT_PASSWORD, POST_NEW_PASSWORD } from "actions/Types";
import Axios from "axios";
import { FORGOT_PASSWORD, UPDATE_NEW_PASSWORD } from "../constant";

const postForgotPasswordRequest = async (FormData) => {
    return await Axios.post(FORGOT_PASSWORD, {
        email: FormData.email
    }, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postForgotPasswordToServer({ payload }) {
    const { FormData } = payload;
    try {
        const fp = yield call(postForgotPasswordRequest, FormData);
        if(fp.data.Data !== "") {
            yield put(postForgotPasswordSuccess(true));
        } else {
            yield put(postForgotPasswordFailure(false));
        }
    } catch (error) {
        yield put(postForgotPasswordFailure('Oops! something went wrong!'))
    }
}

export function* postForgotPasswordSaga() {
    yield takeEvery(POST_FORGOT_PASSWORD, postForgotPasswordToServer)
}

const postNewPasswordRequest = async (FormData) => {
    return await Axios.post(UPDATE_NEW_PASSWORD, {
        code: FormData.code,
        password: FormData.password
    }, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
};

function* postNewPasswordToServer({ payload }) {
    const { FormData } = payload;
    try {
        const fp = yield call(postNewPasswordRequest, FormData);
        if(fp.data.Data !== "") {
            yield put(postNewPasswordSuccess(true));
        } else {
            yield put(postNewPasswordFailure(false));
        }
    } catch (error) {
        yield put(postNewPasswordFailure('Oops! something went wrong!'))
    }
}

export function* postNewPasswordSaga() {
    yield takeEvery(POST_NEW_PASSWORD, postNewPasswordToServer)
}

export default function* rootSaga() {
    yield all([
        fork(postForgotPasswordSaga),
        fork(postNewPasswordSaga)
    ]);
}