/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-07 10:57:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-08 21:37:18
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import CityDummy from "../assets/data-dummy/city.json";
import { getMasterCitySuccess, getMasterCityFailure } from 'actions';
import { GET_MASTER_CITY } from 'actions/Types';

const getMasterCityRequest = async () => {
    return CityDummy;
}

function* getMasterCityFromServer({ payload }) {
    try {
        const mc = yield call(getMasterCityRequest);
        yield put(getMasterCitySuccess(mc));
    } catch (error) {
        yield put(getMasterCityFailure(error));
    }
}

export function* getMasterCitySaga() {
    yield takeEvery(GET_MASTER_CITY, getMasterCityFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getMasterCitySaga)
    ]);
}