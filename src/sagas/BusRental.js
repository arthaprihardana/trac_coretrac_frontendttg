/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 16:47:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-14 16:49:35
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import {PRICE_PRODUCT_BUS, TMP_TOKEN} from "../constant";
import { objectToQueryString } from "helpers";
import { getBusRentalPriceSuccess, getBusRentalPriceFailure } from "../actions";
import { GET_BUS_RENTAL_PRICE } from "../actions/Types";

const getPriceBusRentalRequest = async params => {
    return await Axios.get(`${PRICE_PRODUCT_BUS}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${TMP_TOKEN}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getPriceBusRentalFromServer({ payload }) {
    try {
        let response = yield call(getPriceBusRentalRequest, payload);
        yield put(getBusRentalPriceSuccess(response.data));
    } catch (error) {
        yield put(getBusRentalPriceFailure(error));
    }
}

export function* getPriceBusRentalSaga() {
    yield takeEvery(GET_BUS_RENTAL_PRICE, getPriceBusRentalFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getPriceBusRentalSaga)
    ]);
}