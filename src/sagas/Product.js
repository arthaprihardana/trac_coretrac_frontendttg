/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-15 14:51:08 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-15 14:57:00
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { getListProductSuccess, getListProductFailure, getListArticleSuccess, getListArticleFailure, getListPromoSuccess, getListPromoFailure } from 'actions';
import { GET_LIST_PRODUCT, GET_LIST_PROMO } from 'actions/Types';
import Axios from 'axios';
import { LIST_PRODUCT, LIST_ARTICLE, LIST_PROMO } from '../constant';
import {GET_LIST_ARTICLE} from "../actions/Types";

const getListProductRequest = async () => {
    return await Axios.get(LIST_PRODUCT, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getListProductFromServer() {
    try {
        const product = yield call(getListProductRequest, FormData);
        if(product.data.Status === 200) {
            yield put(getListProductSuccess(product.data.Data));
        } else {
            yield put(getListProductFailure(product.data.ErrorMessage));
        }
    } catch (error) {
        yield put(getListProductFailure('Oops! something went wrong!'));
    }
}

export function* getListProductSaga() {
    yield takeEvery(GET_LIST_PRODUCT, getListProductFromServer);
}

const getListArticleRequest = async () => {
    return await Axios.get(LIST_ARTICLE, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getListArticleFromServer() {
    // yield put(getListArticleSuccess([]));
    try {
        const product = yield call(getListArticleRequest, FormData);
        if(product.data.Status === 200) {
            yield put(getListArticleSuccess(product.data.Data));
        } else {
            yield put(getListArticleFailure(product.data.ErrorMessage));
        }
    } catch (error) {
        yield put(getListArticleFailure('Oops! something went wrong!'));
    }
}

export function* getListArticleSaga() {
    yield takeEvery(GET_LIST_ARTICLE, getListArticleFromServer);
}
const getListPromoRequest = async () => {
    return await Axios.get(LIST_PROMO, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getListPromoFromServer() {
    // yield put(getListArticleSuccess([]));
    try {
        const product = yield call(getListPromoRequest, FormData);
        if(product.data.Status === 200) {
            yield put(getListPromoSuccess(product.data.Data));
        } else {
            yield put(getListPromoFailure(product.data.ErrorMessage));
        }
    } catch (error) {
        yield put(getListPromoFailure('Oops! something went wrong!'));
    }
}

export function* getListPromoSaga() {
    yield takeEvery(GET_LIST_PROMO, getListPromoFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getListArticleSaga),
        fork(getListPromoSaga),
        fork(getListProductSaga)
    ]);
}