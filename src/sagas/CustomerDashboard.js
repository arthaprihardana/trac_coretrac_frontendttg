/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-24 09:47:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 17:56:28
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import _ from "lodash";
import Axios from 'axios';
import { objectToQueryString, localStorageDecrypt } from "helpers";
import {
    MY_BOOKING,
    MY_HISTORY,
    MY_BOOKING_DETAIL,
    USER,
    USER_PROFILE_UPDATE,
    GET_CREDITCARD,
    CREATE_CREDITCARD,
    UPDATE_CREDITCARD,
    DELETE_CREDITCARD,
    UPDATE_PRIMARY_CC
} from "../constant";
import {
    getMyBookingSuccess,
    getMyBookingFailure,
    getMyHistorySuccess,
    getMyHistoryFailure,
    getDetailMyBookingFailure,
    getDetailMyBookingSuccess,
    getCreditCardSuccess, getCreditCardFailure,
    putCreditCardSuccess, putCreditCardFailure,
    postCreditCardFailure, postCreditCardSuccess,
    putPrimaryCCSuccess, putPrimaryCCFailure,
    deleteCreditCardSuccess, deleteCreditCardFailure
} from "../actions";
import {
    GET_MY_BOOKING,
    GET_MY_HISTORY,
    GET_DETAIL_MY_BOOKING,
    GET_CREDIT_CARD,
    PUT_CREDIT_CARD,
    POST_CREDIT_CARD,
    DELETE_CREDIT_CARD,
    GET_USER_PROFILE,
    PUT_USER_PROFILE, PUT_PRIMARY_CC
} from "../actions/Types";

const getMyBookingRequest = async params => {
    let usr = localStorageDecrypt('_aaa', 'object');
    let prm = objectToQueryString(_.merge({
        PIC: usr.UserLoginId
    }, params));
    return await Axios.get(`${MY_BOOKING}?${prm}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${usr.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMyBookingFromServer({ payload }) {
    try {
        let response = yield call(getMyBookingRequest, payload);
        yield put(getMyBookingSuccess(response.data.Data));
    } catch (error) {
        yield put(getMyBookingFailure(error));
    }
}

export function* getMyBookingSaga() {
    yield takeEvery(GET_MY_BOOKING, getMyBookingFromServer);
}

const getMyHistoryRequest = async params => {
    let usr = localStorageDecrypt('_aaa', 'object');
    let prm = objectToQueryString(_.merge({
        PIC: usr.UserLoginId
    }, params));
    return await Axios.get(`${MY_HISTORY}?${prm}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${usr.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMyHistoryFromServer({ payload }) {
    try {
        let response = yield call(getMyHistoryRequest, payload);
        yield put(getMyHistorySuccess(response.data.Data));
    } catch (error) {
        yield put(getMyHistoryFailure(error));
    }
}

export function* getMyHistorySaga() {
    yield takeEvery(GET_MY_HISTORY, getMyHistoryFromServer);
}

const getDetailMyBookingRequest = async params => {
    let usr = localStorageDecrypt('_aaa', 'object');
    return await Axios.get(`${MY_BOOKING_DETAIL}/${params.IdBooking}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${usr.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDetailMyBookingFromServer({ payload }) {
    try {
        let response = yield call(getDetailMyBookingRequest, payload);
        yield put(getDetailMyBookingSuccess(response.data.Data));
    } catch (error) {
        yield put(getDetailMyBookingFailure(error));
    }
}

export function* getDetailMyBookingSaga() {
    yield takeEvery(GET_DETAIL_MY_BOOKING, getDetailMyBookingFromServer);
}

//Credit Card

const getCreditCardRequest = async params => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.get(`${GET_CREDITCARD}${local_auth.UserLoginId}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
        .then(response => response)
        .catch(error => error);
}

function* getCreditCardFromServer({ payload }) {
    try {
        let response = yield call(getCreditCardRequest, payload);
        if(response.data.Data.length>0){
            yield put(getCreditCardSuccess(response.data));
        }else{
            response.data.kosong=1;
            yield put(getCreditCardSuccess(response.data));
        }
    } catch (error) {
        yield put(getCreditCardFailure(error));
    }
}

export function* getCreditCardSaga() {
    yield takeEvery(GET_CREDIT_CARD, getCreditCardFromServer);
}


const putCreditCardRequest = async (FormData) => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.put(`${UPDATE_CREDITCARD}/${FormData.Id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
        .then(response => response)
        .catch(error => error);
};

function* putCreditCardToServer({ payload }) {
    const { FormData } = payload;
    try {
        const create = yield call(putCreditCardRequest, FormData);
        if(create.data.Status === 201 || create.data.Status === 200) {
            yield put(putCreditCardSuccess(create.data.Data));
        } else {
            yield put(putCreditCardFailure(create.data.ErrorMessage));
        }
    } catch (error) {
        yield put(putCreditCardFailure('Oops! something went wrong!'));
    }
}

export function* updateCreditCardSagas() {
    yield takeEvery(PUT_CREDIT_CARD, putCreditCardToServer);
}

const postCreditCardRequest = async (FormData) => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.post(`${CREATE_CREDITCARD}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
        .then(response => response)
        .catch(error => error);
};
//
function* postCreditCardToServer({ payload }) {
    const { FormData } = payload;
    try {
        const create = yield call(postCreditCardRequest, FormData);
        if(create.data.Status === 201 || create.data.Status === 200) {
            yield put(postCreditCardSuccess(create.data.Data));
        } else {
            yield put(postCreditCardFailure(create.data.ErrorMessage));
        }
    } catch (error) {
        yield put(postCreditCardFailure('Oops! something went wrong!'));
    }
}

export function* postCreditCardSagas() {
    yield takeEvery(POST_CREDIT_CARD, postCreditCardToServer);
}
const deleteCreditCardRequest = async (FormData) => {
    // console.log("SAGAS DELETE FORMS= ",FormData);
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.delete(`${DELETE_CREDITCARD}/${FormData.Id}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
        .then(response => response)
        .catch(error => error);
};
// //
function* deleteCreditCardToServer({ payload }) {
    yield put(deleteCreditCardSuccess(-1));
    try {
        const create = yield call(deleteCreditCardRequest, payload);
        if(create.data.Status === 201 || create.data.Status === 200) {
            yield put(deleteCreditCardSuccess(create.data.Data));
        } else {
            yield put(deleteCreditCardFailure(create.data.ErrorMessage));
        }
    } catch (error) {
        yield put(deleteCreditCardFailure('Oops! something went wrong!'));
    }
}
//
export function* deleteCreditCardSagas() {
    yield takeEvery(DELETE_CREDIT_CARD, deleteCreditCardToServer);
}


const putPrimaryCCRequest = async (FormData) => {

    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.put(`${UPDATE_PRIMARY_CC}${local_auth.UserLoginId}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
        .then(response => response)
        .catch(error => error);
};

function* putPrimaryCCToServer({ payload }) {
    yield put(putPrimaryCCSuccess(0));
    const { FormData } = payload;
    try {
        const create = yield call(putPrimaryCCRequest, payload);
        if(create.data.Status === 201 || create.data.Status === 200) {
            yield put(putPrimaryCCSuccess(create.data.Data));
        } else {
            yield put(putPrimaryCCFailure(create.data.ErrorMessage));
        }
    } catch (error) {
        yield put(putPrimaryCCFailure('Oops! something went wrong!'));
    }
}

export function* updatePrimaryCCSagas() {
    yield takeEvery(PUT_PRIMARY_CC, putPrimaryCCToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getMyBookingSaga),
        fork(getCreditCardSaga),
        fork(updatePrimaryCCSagas),
        fork(postCreditCardSagas),
        fork(updateCreditCardSagas),
        fork(deleteCreditCardSagas),
        fork(getDetailMyBookingSaga),
        fork(getMyHistorySaga)
    ]);
}