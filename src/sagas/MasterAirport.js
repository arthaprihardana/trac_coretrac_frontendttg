/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-11 09:17:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-11 16:41:55
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from "axios";
import { objectToQueryString } from "helpers";
import { getMasterAirportSuccess, getMasterAirportFailure, getMasterAirportCityCoverageSuccess, getMasterAirportCityCoverageFailure } from "actions";
import { GET_MASTER_AIRPORT, GET_MASTER_AIRPORT_CITY_COVERAGE } from "actions/Types";
import {MASTER_AIRPORT, AIRPORT_COVERAGE, TMP_TOKEN} from "../constant";

// MASTER AIRPORT START HERE
const getMasterAirport = async () => {
    return await Axios.get(MASTER_AIRPORT, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${TMP_TOKEN}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMasterAirportFromServer({ payload }) {
    try {
        let response = yield call(getMasterAirport, payload);
        yield put(getMasterAirportSuccess(response.data.Data));
    } catch (error) {
        yield put(getMasterAirportFailure(error));
    }
}

export function* getMasterAirportSaga() {
    yield takeEvery(GET_MASTER_AIRPORT, getMasterAirportFromServer);
}
// MASTER AIRPORT END HERE

// MASTER AIRPORT CITY COVERAGE START HERE
const getMasterAirportCityCoverage = async params => {
    return await Axios.get(`${AIRPORT_COVERAGE}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMasterAirportCityCoverageFromServer({ payload }) {
    try {
        let response = yield call(getMasterAirportCityCoverage, payload);
        yield put(getMasterAirportCityCoverageSuccess(response.data.Data));
    } catch (error) {
        yield put(getMasterAirportCityCoverageFailure(error));
    }
}

export function* getMasterAirportCityCoverageSaga() {
    yield takeEvery(GET_MASTER_AIRPORT_CITY_COVERAGE, getMasterAirportCityCoverageFromServer);
}
// MASTER AIRPORT CITY COVERAGE END HERE

export default function* rootSaga() {
    yield all([
        fork(getMasterAirportSaga),
        fork(getMasterAirportCityCoverageSaga)
    ]);
}