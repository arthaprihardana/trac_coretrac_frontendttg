/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-22 16:25:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-23 23:02:10
 */
import { all, put, call, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import {
    LIST_CAR_TYPE,
    REGISTER,
    USER,
    CHECK_TOKEN_URL,
    USER_PROFILE_UPDATE,
    VERIFY_ACCOUNT,
    CHANGE_PASSWORD_INTERNAL,
    POST_CONTACT_US
} from "../constant";
import { updatePasswordSuccess,createContactUsSuccess, checkTokenSuccess, checkTokenFailure, createContactUsFailure, updatePasswordFailure,getUserProfileSuccess, getUserProfileFailure, updateUserProfileSuccess, updateUserProfileFailure, getActivateUserFailure, getActivateUserSuccess } from "../actions";
import {
    CHECK_TOKEN,
    CONTACT_US,
    GET_ACTIVATE_USER,
    GET_USER_PROFILE,
    PUT_PASSWORD,
    PUT_USER_PROFILE
} from "../actions/Types";
import { localStorageDecrypt } from "helpers";
import db from '../db';

const getActivateUserRequest = async params => {
    // console.log("trying to activate")
    // let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.get(`${VERIFY_ACCOUNT}/${params.IdUser}`, {
        headers: {
            'content-type': 'application/json'
            // 'Authorization': `Bearer ${local_auth.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
};

function* getActivateUserFromServer({ payload }) {
    try {
        let response = yield call(getActivateUserRequest, payload);
        yield put(getActivateUserSuccess(response.data));
    } catch (error) {
        yield put(getActivateUserFailure(error));
    }
}

export function* getActivateUserSaga() {
    yield takeEvery(GET_ACTIVATE_USER, getActivateUserFromServer);
}

const getuserProfileRequest = async params => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.get(`${USER}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getuserProfileFromServer({ payload }) {
    try {
        let response = yield call(getuserProfileRequest, payload);
        yield put(getUserProfileSuccess(response.data));
    } catch (error) {
        yield put(getUserProfileFailure(error));
    }
}

export function* getuserProfileSaga() {
    yield takeEvery(GET_USER_PROFILE, getuserProfileFromServer);
}


const checkTokenRequest = async params => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.get(`${CHECK_TOKEN_URL}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
    .then(response => response)
    .catch(error => console.log(""));
}

function* checkTokenFromServer({ payload }) {
    try {
        let response = yield call(checkTokenRequest, payload);
        yield put(checkTokenSuccess(response.data));
    } catch (error) {
        yield put(checkTokenFailure(error));
    }
}

export function* checkTokenSaga() {
    yield takeEvery(CHECK_TOKEN, checkTokenFromServer);
}


const putUserProfileRequest = async (FormData) => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.put(`${USER_PROFILE_UPDATE}/${FormData.IdUser}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
        .then(response => response)
        .catch(error => error);
};

function* putUserProfileToServer({ payload }) {
    const { FormData } = payload;
    try {
        const create = yield call(putUserProfileRequest, FormData);
        if(create.data.Status === 201 || create.data.Status === 200) {
            yield put(updateUserProfileSuccess(create.data.Data));
        } else {
            yield put(updateUserProfileFailure(create.data.ErrorMessage));
        }
    } catch (error) {
        yield put(updateUserProfileFailure('Oops! something went wrong!'));
    }
}

export function* updateUserProfileSaga() {
    yield takeEvery(PUT_USER_PROFILE, putUserProfileToServer);
}

const putPasswordRequest = async (FormData) => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.put(`${CHANGE_PASSWORD_INTERNAL}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
        .then(response => response)
        .catch(error => error);
};

function* putPasswordToServer({ payload }) {
    try {
        const create = yield call(putPasswordRequest, payload);
        // console.log(create);
        if((create.data.Status === 201 || create.data.Status === 200) && create.data.Data!=='') {
            yield put(updatePasswordSuccess(create.data));
        } else {
            yield put(updatePasswordFailure(create.data));
        }
    } catch (error) {
        yield put(updatePasswordFailure('Oops! something went wrong!'));
    }
}

export function* putPasswordSaga() {
    yield takeEvery(PUT_PASSWORD, putPasswordToServer);
}

const postContactUsRequest = async (FormData) => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.post(`${POST_CONTACT_US}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
        .then(response => response)
        .catch(error => error);
};

function* postContactUsServer({ payload }) {
    try {
        const create = yield call(postContactUsRequest, payload);
        // console.log(create);
        if((create.data.Status === 201 || create.data.Status === 200) && create.data.Data!=='') {
            yield put(createContactUsSuccess(create.data));
        } else {
            yield put(createContactUsFailure(create.data));
        }
    } catch (error) {
        yield put(createContactUsFailure('Oops! something went wrong!'));
    }
}

export function* postContactUsSagas() {
    yield takeEvery(CONTACT_US, postContactUsServer);
}

export default function* rootSaga() {
    yield all([
        fork(getuserProfileSaga),
        fork(getActivateUserSaga),
        fork(putPasswordSaga),
        fork(checkTokenSaga),
        fork(postContactUsSagas),
        fork(putPasswordSaga),
        fork(updateUserProfileSaga)
    ]);
}