/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-17 11:01:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-17 11:06:16
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import {LIST_CAR_TYPE, TMP_TOKEN} from '../constant';
import { getMasterCarTypeSuccess, getMasterCarTypeFailure } from 'actions';
import { GET_MASTER_CAR_TYPE } from 'actions/Types';
import Axios from 'axios';

const getCarTypeRequest = async param => {
    return await Axios.get(`${LIST_CAR_TYPE}/${param}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${TMP_TOKEN}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCarTypeFromServer({ payload }) {
    try {
        let response = yield call(getCarTypeRequest, payload);
        yield put(getMasterCarTypeSuccess(response.data.Data));
    } catch (error) {
        yield put(getMasterCarTypeFailure(error));
    }
}

export function* getCarTypeSaga() {
    yield takeEvery(GET_MASTER_CAR_TYPE, getCarTypeFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getCarTypeSaga)
    ]);
}