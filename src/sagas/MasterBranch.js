/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-01 13:43:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 14:42:47
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { GET_MASTER_BRANCH } from "../actions/Types";
import { getMasterBranchSuccess, getMasterBranchFailure } from "../actions";
import { BRANCH } from "../constant";

const getBranchRequest = async params => {
    return await Axios.get(`${BRANCH}/${params}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getBranchFromServer({ payload }) {
    try {
        let response = yield call(getBranchRequest, payload);
        yield put(getMasterBranchSuccess(response.data.Data))
    } catch (error) {
        yield put(getMasterBranchFailure(error));
    }
}

export function* getBranchSaga() {
    yield takeEvery(GET_MASTER_BRANCH, getBranchFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getBranchSaga)
    ])
}