/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-22 16:19:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 15:36:19
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import Axios from 'axios';
import { GOOGLE_PLACE_AUTOCOMPLETE, GOOGLE_PLACE_DETAIL, GOOGLE_FIND_PLACE, GOOGLE_GET_DISTANCE_MATRIX } from '../constant';
import { getAddressFromGMSuccess, getAddressFromGMFailure, getPlaceDetailFromGMSuccess, getPlaceDetailFromGMFailure, getFindGeometryFromGMSuccess, getFindGeometryFromGMFailure } from 'actions';
import { GET_ADDRESS_GM, GET_PLACE_DETAIL_GM, GET_GEOMETRY_GM } from 'actions/Types';
import { objectToQueryString } from "helpers";
import { getDistanceMatrixFromGMSuccess, getDistanceMatrixFromGMFailure, getDistanceMatrixBusSuccess, getDistanceMatrixBusFailure } from '../actions/AddressGoogleMaps';
import { GET_DISTANCE_MATRIX_GM, GET_DISTANCE_MATRIX_BUS } from '../actions/Types';

// AUTOCOMPLETE START HERE
const getAddressRequest = async input => {
    return await Axios.get(`${GOOGLE_PLACE_AUTOCOMPLETE}?input=${input}`, {
       crossdomain: true
    })
    .then(response => response.data)
    .catch(error => error);
}

function* getAddressFromGoogle({ payload }) {
    try {
        let response = yield call(getAddressRequest, payload);
        yield put(getAddressFromGMSuccess(response.data))
    } catch (error) {
        yield put(getAddressFromGMFailure(error));
    }
}

export function* getAddressSaga() {
    yield takeEvery(GET_ADDRESS_GM, getAddressFromGoogle)
}
// AUTOCOMPLETE END HERE

// PLACE DETAIL START HERE
const getPlaceDetailRequest = async input => {
    return await Axios.get(`${GOOGLE_PLACE_DETAIL}?placeid=${input}`, {
        crossdomain: true
    })
    .then(response => response.data)
    .catch(error => error);
}

function* getPlaceDetailFromGoogle({ payload }) {
    try {
        let response = yield call(getPlaceDetailRequest, payload);
        if(response.data.status === "OK") {
            yield put(getPlaceDetailFromGMSuccess(response.data.result.geometry.location))
        } else {
            yield put(getPlaceDetailFromGMSuccess(null))
        }
    } catch (error) {
        yield put(getPlaceDetailFromGMFailure(error));
    }
}

export function* getPlaceDetailSaga() {
    yield takeEvery(GET_PLACE_DETAIL_GM, getPlaceDetailFromGoogle)
}
// PLACE DETAIL END HERE

// FIND GEOMETRY START HERE
const getGeometryRequest = async input => {
    return await Axios.get(`${GOOGLE_FIND_PLACE}?input=${input}`, {
        crossdomain: true
    })
    .then(response => response.data)
    .catch(error => error);
}

function* getGeometryFromGoogle({ payload }) {
    try {
        let response = yield call(getGeometryRequest, payload);
        if(response.data.status === "OK") {
            yield put(getFindGeometryFromGMSuccess(response.data.candidates[0].geometry.location))
        } else {
            yield put(getFindGeometryFromGMSuccess(null));
        }
    } catch (error) {
        yield put(getFindGeometryFromGMFailure(error));
    }
}

export function* getGeometrySaga() {
    yield takeEvery(GET_GEOMETRY_GM, getGeometryFromGoogle)
}
// FIND GEOMETRY END HERE

// GET DISTANCE START HERE
const getDistanceRequest = async params => {
    return await Axios.get(`${GOOGLE_GET_DISTANCE_MATRIX}?${objectToQueryString(params)}`, {
        crossdomain: true
    })
    .then(response => response.data)
    .catch(error => error);
}

function* getDistanceFromGoogle({ payload }) {
    try {
        let response = yield call(getDistanceRequest, payload);
        if(response.data.status === "OK") {
            // yield put(getDistanceMatrixFromGMSuccess(response.data.rows[0].elements[0]))
            yield put(getDistanceMatrixFromGMSuccess(response.data.rows));
        } else {
            yield put(getDistanceMatrixFromGMSuccess(null));
        }
    } catch (error) {
        yield put(getDistanceMatrixFromGMFailure(error));
    }
}

export function* getDistanceSaga() {
    yield takeEvery(GET_DISTANCE_MATRIX_GM, getDistanceFromGoogle)
}
// GET DISTANCE END HERE

// GET DISTANCE BUS START HERE
const getDistanceBusRequest = async params => {
    return await Axios.get(`${GOOGLE_GET_DISTANCE_MATRIX}?${objectToQueryString(params)}`, {
        crossdomain: true
    })
    .then(response => response.data)
    .catch(error => error);
}

function* getDistanceBusFromGoogle({ payload }) {
    try {
        let response = yield call(getDistanceBusRequest, payload);
        if(response.data.status === "OK") {
            yield put(getDistanceMatrixBusSuccess(response.data.rows[0].elements[0]))
        } else {
            yield put(getDistanceMatrixBusSuccess(null));
        }
    } catch (error) {
        yield put(getDistanceMatrixBusFailure(error));
    }
}

export function* getDistanceBusSaga() {
    yield takeEvery(GET_DISTANCE_MATRIX_BUS, getDistanceBusFromGoogle)
}
// GET DISTANCE BUS END HERE

export default function* rootSaga() {
    yield all([
        fork(getAddressSaga),
        fork(getPlaceDetailSaga),
        fork(getGeometrySaga),
        fork(getDistanceSaga),
        fork(getDistanceBusSaga)
    ])
}