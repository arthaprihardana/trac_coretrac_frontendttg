/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-07 10:11:24 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 11:34:01
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { postLoginSuccess, postLoginFailure, getLogoutSuccess, getLogoutFailure, postLoginSosmedSuccess, postLoginSosmedFailure } from 'actions';
import { POST_LOGIN, GET_LOGOUT } from 'actions/Types';
import Axios from 'axios';
import { LOGIN, LOGOUT, LOGIN_SOSMED } from '../constant';
import { localStorageDecrypt } from "helpers";
import db from '../db';
import { POST_LOGIN_SOSMED } from '../actions/Types';

// LOGIN START HERE
const postLoginRequest = async FormData => {
    return await Axios.post(LOGIN, {
        EmailPersonal: FormData.email,
        Password: FormData.password
    }, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postLoginToServer({ payload }) {
    const { FormData } = payload;
    try {
        const login = yield call(postLoginRequest, FormData);
        if(login.data.Data !== "") {
            yield put(postLoginSuccess({
                login_data: {
                    Data: login.data.Data,
                    token: login.data.token
                }
            }));
        } else {
            yield put(postLoginFailure(login.data.ErrorMessage));
        }
    } catch (error) {
        yield put(postLoginFailure('Oops! something went wrong!'));
    }
}

export function* postLoginSaga() {
    yield takeEvery(POST_LOGIN, postLoginToServer);
}
// LOGIN END HERE

// LOGOUT START HERE
const getLogoutRequest = async () => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.get(LOGOUT, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getLogoutFromServer() {
    try {
        const logout = yield call(getLogoutRequest);
        if(logout.data.Data !== "") {
            yield put(getLogoutSuccess(logout.data.Data));
        } else {
            yield put(getLogoutFailure(false));
        }
    } catch (error) {
        yield put(getLogoutFailure(false));
    }
}

export function* getLogoutSaga() {
    yield takeEvery(GET_LOGOUT, getLogoutFromServer);
}
// LOGOUT END HERE

// LOGIN SOSMED START HERE
const postLoginSosmedRequest = async FormData => {
    return await Axios.post(LOGIN_SOSMED, FormData, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postLoginSosmedToServer({ payload }) {
    try {
        const login = yield call(postLoginSosmedRequest, payload);
        if(login.data.Data !== "") {
            yield put(postLoginSosmedSuccess({
                login_data: {
                    Data: login.data.Data,
                    token: login.data.token
                }
            }));
        } else {
            yield put(postLoginSosmedFailure(login.data.ErrorMessage));
        }
    } catch (error) {
        yield put(postLoginSosmedFailure('Oops! something went wrong!'));
    }
}

export function* postLoginSosmedSaga() {
    yield takeEvery(POST_LOGIN_SOSMED, postLoginSosmedToServer);
}
// LOGIN SOSMED END HERE

export default function* rootSaga() {
    yield all([
        fork(postLoginSaga),
        fork(getLogoutSaga),
        fork(postLoginSosmedSaga)
    ])
}