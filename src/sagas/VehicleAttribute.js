
import { all, put, call, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { VEHICLE_ATTR } from "../constant";
import { getVehicleAttributeFailure, getVehicleAttributeSuccess } from "../actions";
import { GET_VEHICLE_ATTRIBUTE } from "../actions/Types";
import { localStorageDecrypt } from "helpers";
import db from '../db';

const getVehcileAttributeRequest = async params => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.get(`${VEHICLE_ATTR}`, {
        headers: {
            // 'Access-Control-Allow-Origin': '*',
            'content-type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getVehicleAttributeFromServer({ payload }) {
    try {
        let response = yield call(getVehcileAttributeRequest, payload);
        yield put(getVehicleAttributeSuccess(response.data.Data));
    } catch (error) {
        yield put(getVehicleAttributeFailure(error));
    }
}

export function* getVehicleAttributeSagas() {
    yield takeEvery(GET_VEHICLE_ATTRIBUTE, getVehicleAttributeFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getVehicleAttributeSagas)
    ]);
}