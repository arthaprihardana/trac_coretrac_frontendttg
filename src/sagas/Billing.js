/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 20:25:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-23 23:00:09
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { CREATE_INVOICE } from "../constant";
import { createInvoiceSuccess, createInvoiceFailure } from "../actions";
import { POST_CREATE_INVOICE } from "../actions/Types";
import { localStorageDecrypt } from "helpers";
import db from '../db';

const postCreateInvoiceRequest = async FormData => {
    let local_auth = localStorageDecrypt('_aaa', 'object');
    return await Axios.post(CREATE_INVOICE, FormData, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${local_auth.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postCreateInvoiceFromServer({ payload }) {
    try {
        let response = yield call(postCreateInvoiceRequest, payload);
        yield put(createInvoiceSuccess(response.data));
    } catch (error) {
        yield put(createInvoiceFailure(error));
    }
}

export function* postCreateInvoiceSaga() {
    yield takeEvery(POST_CREATE_INVOICE, postCreateInvoiceFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(postCreateInvoiceSaga)
    ]);
}