/**
 * @author: Artha Prihardana 
 * @Date: 2018-12-31 19:23:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 19:25:25
 */
import 'babel-polyfill';
import { all } from 'redux-saga/effects';

import RegisterSaga from './Register';
import ForgotPasswordSaga from './ForgotPassword';
import LoginSaga from './Login';
import MasterCitySaga from './MasterCity';
import MasterCarType from './MasterCarType';
import CarRentalSaga from './CarRental';
import ProductSaga from './Product';
import AddressSaga from './AddressGoogleMaps';
import MasterAirportSaga from './MasterAirport';
import AirportTransferSaga from './AirportTransfer';
import MasterBankSaga from './MasterBank';
import BusRentalSaga from './BusRental';
import BillingSaga from './Billing';
import DiscountSaga from './Discount';
import UserProfile from './User';
import Dashboard from './CustomerDashboard';
import MasterBranchSaga from './MasterBranch';
import VehicleAttributeSaga from './VehicleAttribute';
import AdjustmentRetailSaga from './AdjustmentRetail';

export default function* rootSaga() {
    yield all([
        RegisterSaga(),
        ForgotPasswordSaga(),
        LoginSaga(),
        MasterCitySaga(),
        MasterCarType(),
        CarRentalSaga(),
        ProductSaga(),
        AddressSaga(),
        MasterAirportSaga(),
        AirportTransferSaga(),
        MasterBankSaga(),
        BusRentalSaga(),
        VehicleAttributeSaga(),
        BillingSaga(),
        DiscountSaga(),
        UserProfile(),
        Dashboard(),
        MasterBranchSaga(),
        AdjustmentRetailSaga()
    ]);
}