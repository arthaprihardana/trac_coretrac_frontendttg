/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 00:37:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-14 00:41:59
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from "axios";
import { objectToQueryString } from "helpers";
import { getbankSuccess, getBankFailure } from "../actions";
import { GET_MASTER_BANK } from "../actions/Types";
import { BANK } from "../constant";

const getMasterBankRequest = async params => {
    return await Axios.get(`${BANK}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMasterBankFromServer({ payload }) {
    try {
        let response = yield call(getMasterBankRequest, payload);
        yield put(getbankSuccess(response.data.Data));
    } catch (error) {
        yield put(getBankFailure(error));
    }
}

export function* getMasterBankSaga() {
    yield takeEvery(GET_MASTER_BANK, getMasterBankFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getMasterBankSaga)
    ]);
}