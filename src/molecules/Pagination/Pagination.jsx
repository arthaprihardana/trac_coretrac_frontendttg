import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import uuid from "uuid/v4";

// styles
import "./Pagination.scss";

const Pagination = (props) => {
    const { currentActivePage, totalPage, onClick } = props;

    const pages = [];

    for (let i = 1; i <= totalPage; i += 1) {
        const classPage = cx("page-item", { active: currentActivePage === i });
        pages.push(
            <li key={uuid()} className={classPage}>
                <button
                    type="button"
                    className="page-link"
                    onClick={e => {
                        e.preventDefault();
                        onClick(i);
                    }}
                >
                    {i}
                </button>
            </li>
        );
    }

    return (
        <div className="m-pagination">
            <nav aria-label="Page navigation">
                <ul className="pagination">{pages}</ul>
            </nav>
        </div>
    );

};

Pagination.defaultProps = {
    currentActivePage: 1,
    totalPage: 9,
    onClick: v => console.log(v)
};

Pagination.propTypes = {
    currentActivePage: PropTypes.number,
    totalPage: PropTypes.number,
    onClick: PropTypes.func
};

export default React.memo(Pagination);