import React from "react";
import Slider from "react-slick";
import PropTypes from "prop-types";
import uuid from "uuid";
import { isObject, isString } from "lodash";

// components
import { Cover } from "atom";

// assets
import ImageLogin1 from "assets/images/dummy/login.jpg";
import ImageLogin2 from "assets/images/dummy/headline-2.jpg";
import ImageLogin3 from "assets/images/dummy/headline-3.jpg";

import loginBus from 'assets/images/trac/login_bus.jpg';
import lang from '../../assets/data-master/language'
// style
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./Slideshow.scss";

const Slideshow = props => {
    const { config, images } = props;

    return (
        <div className="m-slideshow">
            <Slider {...config}>
                {images.map(image => (
                    <div key={uuid()} className="slide-item">
                        <Cover image={image.src} />
                        <div className="slide-text">
                            {isObject(image.text) && (
                                <React.Fragment>
                                    <h2
                                        dangerouslySetInnerHTML={{
                                            __html: image.text.title
                                        }}
                                    />
                                    <p>{image.text.description}</p>
                                </React.Fragment>
                            )}
                            {isString(image.text) && <p>{image.text}</p>}
                        </div>
                    </div>
                ))}
            </Slider>
        </div>
    );

};

Slideshow.defaultProps = {
    config: {
        dots: true,
        lazyLoad: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 7000,
        arrows: false,
        speed: 350,
        slidesToScroll: 1,
        initialSlide: 0,
        fade: true,
        cssEase: "ease"
    },
    images: [
        {
            text: {
                title: lang.loginSliderTitle[lang.default],
                description: lang.loginSliderDesc[lang.default]
            },
            src: ImageLogin1
        },
        {
            text: {
                title: lang.loginSliderTitle[lang.default],
                description: lang.loginSliderDesc[lang.default]
            },
            src: ImageLogin2
        },
        {
            text: {
                title: lang.loginSliderTitle[lang.default],
                description: lang.loginSliderDesc[lang.default]
            },
            src: loginBus
        }
    ]
};

Slideshow.propTypes = {
    config: PropTypes.object, // eslint-disable-line
    images: PropTypes.arrayOf(
        PropTypes.shape({
            text: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
            path: PropTypes.string,
            src: PropTypes.string
        })
    )
};

export default React.memo(Slideshow);