import React from "react";
import { Button } from 'atom';

const BookingCarItem = ({data, page, carImageActive, index, showPaymentGuide}) => {
    let classProgress = null;

    let carListActive = parseInt(data.car.active),
        carListImage = data.car.image,
        carListTemplate = null,
        carListDotTemplate = null;

        carListTemplate = carListImage.map((carImage, i) => {
            return (
                <img className={carListActive === i ? 'active' : ''} src={carImage} alt={`car-${i}`} key={i} />
            );
        });

        carListDotTemplate = carListImage.map((carImage, i) => {
            return (
                <span onClick={(e) => carImageActive(e)} className={carListActive === i ? 'active' : ''} data-key={index} data-index={i} key={i} />
            );
        });

    switch (data.status.progress) {
        case 1:
            classProgress = 'car-on-the-way'
            break;
        case 2:
            classProgress = 'car-with-you'
            break;
        case 3:
            classProgress = 'complete'
            break;
        default:
            break;
    }

    return (
        <div className="myBooking-settings">
            <div className="wrapper-my-booking">
                <div className="status-car">
                    <h4 className="title-cars">{data.title}</h4>
                    {page !== "history-booking" && (
                        <p className={`driver-status ${data.status.progress === -1 ? 'waiting-payment' : null}`}>{data.status.text}</p>
                    )}
                    <p className="under-line">&nbsp;</p>
                </div>
                <div className="my-booking-cars">
                    <div className="my-booking-cars-image">
                        {carListTemplate}
                    </div>
                    <div className="my-booking-cars-dot">
                        {carListDotTemplate}
                    </div>
                </div>
                <div className="detail-cars">
                    <div className="car-desc">
                        <p className="car-total">Car Total</p>
                        <p className="value-car-total">
                            {data.total > 1
                                ? data.total + " Cars"
                                : data.total + " Car"}
                        </p>
                    </div>
                    <div className="car-desc">
                        <p className="book-numb">Booking Number</p>
                        <p className="value-book-numb">
                            {data.booking_number}
                        </p>
                    </div>
                    <div className="car-desc">
                        <p className="dates">Dates</p>
                        <p className="value-dates">
                            {data.dates.start} - {data.dates.end}
                        </p>
                    </div>
                    <div className="detail-cars btn">
                        {page !== "history-booking" && data.status.progress !== -1 ? (
                            <div className="wrapper-detail">
                                <div className="detail-tracker">
                                    <div
                                        className={`tracker-active ${classProgress}`}
                                    >
                                        <div className="icon-tracker" />
                                    </div>
                                </div>
                                <p className="label-track detail-info-on-the-way">
                                    Car on the way
                                </p>
                                <p className="label-track detail-info-with-you">
                                    Car with you
                                </p>
                                <p className="label-track detail-info-complete">
                                    Complete
                                </p>
                            </div>
                        ) : null}
                        <Button
                            text
                            goto={
                                page === "history-booking"
                                    ? "/dashboard/history-booking/detail"
                                    : "/dashboard/my-booking/detail"
                            }
                        >
                            Detail
                        </Button>
                        {
                            data.status.progress === -1 && page !== 'history-booking' && (
                                <Button outline onClick={showPaymentGuide}>Payment Guide</Button>
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BookingCarItem;
