import React from "react";

// components
import { Text, Button, Rupiah } from "atom";

// styles
import "./SelectedCar.scss";

const SelectedCar = (props) => {
    const { data, onRemove, onSubmit } = props;
    if (data.length < 1) return null;
    let totalPayment = 0;
    let totalCar = 0;
    let selectedCar = [];
    data.map(car => {
        totalPayment = totalPayment + car.rentInfo.total;
        totalCar = totalCar + car.rentInfo.amounts;
        if (car.rentInfo.amounts > 0) {
            selectedCar.push(
                <div key={car.id} className="item">
                    <div className="image">
                        <img src={car.img} alt={car.name} />
                        <span
                            className="cancel"
                            onClick={() => onRemove(car.id)}
                        />
                    </div>
                </div>
            );
        }
        car.extraItems.map(extra => {
            totalPayment = totalPayment + extra.total;
            return totalPayment;
        });
        return totalPayment;
    });

    if (totalPayment === 0) return null;

    return (
        <div className="m-selected-car">
            <div className="container">
                <div className="selected-lists">{selectedCar}</div>
                <div className="selected-price">
                    <div className="selected-price-wrapper">
                        <div className="total-payment">
                            <Text className="label">Total Payment</Text>
                            <Text className="price">
                                <Rupiah value={totalPayment} />
                                <span className="period"> / 3 days</span>
                            </Text>
                        </div>
                        <div className="selected-action">
                            <Button primary onClick={onSubmit}>
                                Select {totalCar} {totalCar > 1 ? "cars" : "car"}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

};

export default React.memo(SelectedCar);