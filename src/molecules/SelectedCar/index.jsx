/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-25 10:59:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-21 11:07:46
 */
import React, { memo } from "react";
import moment from 'moment';
import 'moment/locale/id';
import _ from "lodash";

// components
import { Text, Button, Rupiah } from "atom";
import { PulseLoader } from "react-spinners";

// styles
import "./SelectedCar.scss";

// helpers
import { localStorageDecrypt } from "helpers";
import lang from '../../assets/data-master/language'

const SelectedCar = (props) => {
    const { data, onRemove, onSubmit, isLoading, type, passangerBus } = props; 
    if (data.length < 1) return null;
    let totalPayment = 0;
    let totalCar = 0;
    let componentSelectedCar = [];
    let valueSelectedCar = [];
    let totalPassanger = 0;
    let tmp;
    
    data.map((car, index) => {
        totalPayment = totalPayment + car.rentInfo.total;
        totalCar = totalCar + car.rentInfo.amounts;
        if (car.rentInfo.amounts > 0) {
            for(let i = 0; i < car.rentInfo.amounts; i++) {
                tmp = JSON.parse(JSON.stringify(car));
                totalPassanger = totalPassanger + car.totalSeat;
                valueSelectedCar.push(tmp);
                componentSelectedCar.push(
                    <div key={Math.random(10)} className="item">
                        <div className="image">
                            <img src={car.vehicleImage !== null ? car.vehicleImage : "http://www.indianbluebook.com/themes/ibb/assets/images/noImageAvailable.jpg"} alt={car.vehicleTypeDesc} />
                            <span
                                className="cancel"
                                onClick={() => onRemove(car.id)}
                            />
                        </div>
                    </div>
                );
            }
        }
        // car.extraItems.map(extra => {
        //     totalPayment = totalPayment + extra.total;
        //     return totalPayment;
        // });
        return totalPayment;
    });

    if (totalPayment === 0) return null;

    let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
    let rangeDay;
    if(type === "airport-transfer") {
        rangeDay = moment(CarRentalFormInput.data[0].date.endDate).diff(moment(CarRentalFormInput.data[0].date.startDate), 'days');
    } else {
        rangeDay = moment(CarRentalFormInput.date.endDate).diff(moment(CarRentalFormInput.date.startDate), 'days');
    }

    let vehicle;
    let vehicle_multi;

    if(type === "bus-rental"){
        vehicle = lang.bus[lang.default];
        vehicle_multi = lang.bus[lang.default];
    }else{
        vehicle = lang.car[lang.default];
        vehicle_multi = lang.cars[lang.default];
    }

    return (
        <div className="m-selected-car">
            <div className="container">
                <div className="selected-lists">
                    <div style={{ width: 'max-content' }}>
                        {componentSelectedCar}
                    </div>
                </div>
                {
                    type === "bus-rental" && (
                        <div className="selected-passanger">
                            <p className="label">{lang.passengers[lang.default]}</p>
                            <p className="value">{totalPassanger}/{CarRentalFormInput.passengers}</p>
                        </div>
                    )
                }
                <div className="selected-price">
                    <div className="selected-price-wrapper">
                        <div className="total-payment">
                            <Text className="label">{lang.totalPayment[lang.default]}</Text>
                            <Text className="price">
                                { type !== "bus-rental" ? <Rupiah value={totalPayment * (rangeDay + 1)} /> : <Rupiah value={totalPayment} /> }
                                <span className="period"> / {rangeDay + 1} { (rangeDay + 1) > 1 ? lang.days[lang.default] : lang.day[lang.default] }</span>
                            </Text>
                        </div>
                        <div className="selected-action">
                            <Button primary onClick={() => onSubmit(valueSelectedCar)}>
                                {isLoading ? <PulseLoader
                                    sizeUnit={"px"}
                                    size={7}
                                    color={'#ffffff'}
                                    loading={isLoading}
                                /> : 
                                `${lang.select[lang.default]} ${totalCar} ${totalCar > 1 ? vehicle_multi : vehicle}`
                                }
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

};

export default memo(SelectedCar);