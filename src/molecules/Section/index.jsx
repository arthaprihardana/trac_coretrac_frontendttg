import React, { Fragment } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

// components
import { Text } from "atom";

// style
import "./Section.scss";

const Section = (props) => {
    const { title, className, flex, children, container } = props;
    const classNames = cx("m-section ", className);
    const classNameContent = cx("content", { flex });

    let sectionContent = "";
    if (container) {
        sectionContent = (
            <Fragment>
                <div className="container">
                    <div className="title">
                        <Text type="h2" centered>
                            {title}
                        </Text>
                        <div className="line" />
                    </div>
                    <div className={classNameContent}>{children}</div>
                </div>
            </Fragment>
        );
    } else {
        sectionContent = (
            <Fragment>
                <div className="title">
                    <Text type="h2" centered>
                        {title}
                    </Text>
                    <div className="line" />
                </div>
                <div className={classNameContent}>{children}</div>
            </Fragment>
        );
    }

    return <div className={classNames}>{sectionContent}</div>;

};

Section.defaultProps = {
    title: "Lorem Ipsum Dolor",
    flex: false,
    children: "Drop content here..."
};

Section.propTypes = {
    title: PropTypes.string,
    flex: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node])
};

export default React.memo(Section);