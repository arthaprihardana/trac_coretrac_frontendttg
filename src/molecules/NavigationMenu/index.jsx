/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-12 12:10:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-23 23:04:01
 */
import React from "react";
import { Link } from "react-router-dom";
import "./NavigationMenu.scss";

import lang from "../../assets/data-master/language";

const NavigationMenu = (props) => {
    const { showClass, hideNavigationEvent } = props;

    return (
        <div className={`m-navigation-menu${showClass}`}>
            <div className="top-menu">
                <div className="close-button" onClick={hideNavigationEvent}>
                    Close
                </div>
                {/* <ul className="language">
                    <li>
                        <Link to="/" className="language-en active">
                            EN
                        </Link>
                    </li>
                    <li>
                        <Link to="/" className="language-id">
                            ID
                        </Link>
                    </li>
                </ul> */}
                <Link
                    to="/"
                    className="a-btn btn-outline-white btn-call-center"
                >
                    Call Center
                </Link>
            </div>
            <ul className="navigation-menu-list">
                <li className="active">
                    <Link to="/about-us">{lang.aboutUs[lang.default]}</Link>
                </li>
                <li>
                    <Link to="/product-services">{lang.productService[lang.default]}</Link>
                </li>
                <li>
                    <Link to="/promo">{lang.promo[lang.default]}</Link>
                </li>
                <li>
                    <Link to="/news">{lang.blog[lang.default]}</Link>
                </li>
                <li className="has-sub-menu">
                    {/* <Link to="/support">{lang.support[lang.default]}</Link> */}
                    <Link to="/">{lang.support[lang.default]}</Link>
                    <ul className="sub-menu-list">
                        <li>
                            <Link to="/faq">{lang.faq[lang.default]}</Link>
                        </li>
                        <li>
                            <Link to="/outlet-locations">{lang.branchLocation[lang.default]}</Link>
                        </li>
                        <li>
                            <Link to="/contact-us">{lang.contactUs[lang.default]}</Link>
                        </li>
                    </ul>
                </li>
                {/* { isLogin && <li>
                    <a onClick={onLogout}>Logout</a>
                </li>} */}
            </ul>
        </div>
    );

};

export default React.memo(NavigationMenu);
