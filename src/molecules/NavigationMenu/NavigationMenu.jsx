import React from "react";
import { Link } from "react-router-dom";
import "./NavigationMenu.scss";

const NavigationMenu = (props) => {
    const { showClass, hideNavigationEvent } = props;

    return (
        <div className={`m-navigation-menu${showClass}`}>
            <div className="top-menu">
                <div className="close-button" onClick={hideNavigationEvent}>
                    Close
                </div>
                <ul className="language">
                    <li>
                        <Link to="/" className="language-en active">
                            EN
                        </Link>
                    </li>
                    <li>
                        <Link to="/" className="language-id">
                            ID
                        </Link>
                    </li>
                </ul>
                <Link
                    to="/"
                    className="a-btn btn-outline-white btn-call-center"
                >
                    Call Center
                </Link>
            </div>
            <ul className="navigation-menu-list">
                <li className="active">
                    <Link to="/about">About</Link>
                </li>
                <li>
                    <Link to="/product-service">Product & Service</Link>
                </li>
                <li>
                    <Link to="/promo">Promo</Link>
                </li>
                <li>
                    <Link to="/blog">Blog</Link>
                </li>
                <li className="has-sub-menu">
                    {/* <Link to="/support">Support</Link> */}
                    <Link to="/">Support</Link>
                    <ul className="sub-menu-list">
                        <li>
                            <Link to="/faq">Help Center (F.A.Q)</Link>
                        </li>
                        <li>
                            <Link to="/branch">Branch Location</Link>
                        </li>
                        <li>
                            <Link to="/contact-us">Contact Us</Link>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    );

};

export default React.memo(NavigationMenu);
