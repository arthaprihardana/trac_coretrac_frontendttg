import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

// components
import { Text } from "atom";

// style
import "./CardInfo.scss";

const CardInfo = (props) => {
    const { iconName, title, description, iconImage } = props;
    const classNames = cx("card-icon", {
        "booking-process": iconName === "booking process",
        "coverage-insurance": iconName === "coverage insurance",
        "car-subtitution": iconName === "car subtitution",
        "premium-car": iconName === "premium car"
    });

    return (
        <div className="m-card-info">
            {/*<div className={classNames} />*/}
            <div style={{textAlign:'center'}}>
                <img src={iconImage} alt="" className="card-info" style={{ height: 64,margin: '2.8rem 0'}}/>
            </div>
            <Text type="h4" centered>
                {title}
            </Text>
            <Text disabled centered>
                {description}
            </Text>
        </div>
    );

};

CardInfo.defaultProps = {
    iconName: "booking process",
    title: "Lorem ipsum dolor",
    description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
};

CardInfo.propTypes = {
    iconName: PropTypes.string,
    iconImage: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string
};

export default React.memo(CardInfo);