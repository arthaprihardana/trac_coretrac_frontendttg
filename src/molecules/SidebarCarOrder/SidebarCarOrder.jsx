import React from "react";
import PropTypes from "prop-types";

// Style
import "./SidebarCarOrder.scss";

const SidebarCarOrder = props => {
    const { data } = props;

    return (
        <div className="m-sidebar-car-order">
            {data.map((value, index) => (
                <div className="item" key={value.id}>
                    <div className="image">
                        <img src={value.img} alt={value.name} />
                    </div>
                    <div className="description">
                        <div className="label">Car #{index + 1}</div>
                        <h4 className="name">
                            <div>{value.name}</div>
                        </h4>
                        <div className="services-type">{value.rentalType}</div>
                    </div>
                </div>
            ))}
        </div>
    );

};

SidebarCarOrder.propTypes = {
    data: PropTypes.array
};

export default React.memo(SidebarCarOrder);