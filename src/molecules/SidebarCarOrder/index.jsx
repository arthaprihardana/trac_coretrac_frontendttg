/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-09 01:05:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-25 15:32:45
 */
import React from "react";
import PropTypes from "prop-types";

// Style
import "./SidebarCarOrder.scss";

const SidebarCarOrder = props => {
    const { data, type } = props;
    let CarRentalFormDisplay;
    if(type === "airport-transfer") {
        CarRentalFormDisplay = JSON.parse(localStorage.getItem('AirportTransferFormDisplay'));
    } else {
        CarRentalFormDisplay = JSON.parse(localStorage.getItem('CarRentalFormDisplay'));
    }
    return (
        <div className="m-sidebar-car-order">
            {data.map((value, index) => (
                <div className="item" key={index}>
                    <div className="image">
                        <img src={value.vehicleImage} alt={value.vehicleTypeDesc} />
                    </div>
                    <div className="description">
                        <div className="label">{ type !== "bus-rental" ? "Car" : "Bus" } #{index + 1}</div>
                        <h4 className="name">
                            <div>{value.vehicleTypeDesc}</div>
                        </h4>
                        <div className="services-type">
                            { type !== "bus-rental" ?  
                                `Rental Car - ${ type === "airport-transfer" ? `Airport Transfer(${CarRentalFormDisplay.typeService})` : CarRentalFormDisplay.typeService }` : 
                                `Bus Rental` }
                        </div>
                    </div>
                </div>
            ))}
        </div>
    );

};

SidebarCarOrder.propTypes = {
    data: PropTypes.array
};

export default React.memo(SidebarCarOrder);