import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import React from "react";
import "./PopularDestinations.scss";
import { Image, Text } from "atom";
import Image1 from "assets/images/dummy/destination-1.jpg";

const PopularDestinations = (props) => {
    const { destinationImage, destinationTitle, destinationLink } = props;

    return (
        <Link to={destinationLink} className="a-popular-destinations">
            <div className="box-item">
                <Image src={destinationImage} alt={destinationTitle} />
                <Text type="h5">{destinationTitle}</Text>
            </div>
        </Link>
    );

};

PopularDestinations.defaultProps = {
    destinationImage: Image1,
    destinationTitle: "Bali",
    destinationLink: "#"
};

PopularDestinations.propTypes = {
    destinationImage: PropTypes.string,
    destinationTitle: PropTypes.string,
    destinationLink: PropTypes.string
};

export default React.memo(PopularDestinations);