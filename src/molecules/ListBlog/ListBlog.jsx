import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import React from "react";
import "./ListBlog.scss";
import { Button, Image, Text } from "atom";
import Image1 from "assets/images/dummy/blog-primary.jpg";

const ListBlog = (props) => {
    const {
        image,
        label,
        title,
        lead,
        date,
        linkTo,
        linkText,
        itemType,
        ...rest
    } = props;

    const { fullWidth } = rest;

    let itemBlog = "";
    if (fullWidth) {
        itemBlog = (
            <div className="m-list-blog full-width">
                <Link to={linkTo} className="blog-image">
                    <Image src={image} alt={title} />
                </Link>
                <div className="blog-description">
                    <div className="blog-label">{label}</div>
                    <h2>
                        <Link to={linkTo}>{title}</Link>
                    </h2>
                    <div className="blog-date">{date}</div>
                    <div className="blog-lead">
                        <Text>{lead}</Text>
                    </div>
                    <Button text goto={linkTo}>
                        {linkText}
                    </Button>
                </div>
            </div>
        );
    } else {
        itemBlog = (
            <div className="m-list-blog">
                <Link to={linkTo} className="blog-image">
                    <Image src={image} alt="Image 1" />
                </Link>
                <div className="blog-description">
                    <h4>
                        <Link to={linkTo}>{title}</Link>
                    </h4>
                    <div className="blog-date">{date}</div>
                </div>
            </div>
        );
    }

    return itemBlog;

};

ListBlog.defaultProps = {
    blogImage: Image1,
    blogLabel: "News",
    blogTitle: "Where to go in September",
    blogLead:
        "Is there a better time to hit the road than September? The great shoulder season of travel offers prime conditions for exploring a vast array of big-name destinations from Bandung to Bali",
    blogDate: "Jul 6, 2017",
    blogLinkTo: "#",
    blogLinkText: "Read More",
    blogItemType: ""
};

ListBlog.propTypes = {
    blogImage: PropTypes.string,
    blogLabel: PropTypes.string,
    blogTitle: PropTypes.string,
    blogLead: PropTypes.string,
    blogDate: PropTypes.string,
    blogLinkTo: PropTypes.string,
    blogLinkText: PropTypes.string,
    blogItemType: PropTypes.string
};

export default React.memo(ListBlog);