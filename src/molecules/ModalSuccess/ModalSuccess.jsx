import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

// components
import { Modal, Image, Text, Button } from "atom";

// assets
import CongratsImage from "assets/images/icons/congrats.svg";

// styles
import "./ModalSuccess.scss";

class ModalSuccess extends PureComponent {
    constructor(props) {
        super(props);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    handleCloseModal(e) {
        const { onCloseModal, goto, history } = this.props;
        if (goto) history.push(goto);
        onCloseModal(e);
    }

    renderHeader() {
        return (
            <div className="header">
                <Image src={CongratsImage} alt="congrats" big />
            </div>
        );
    }

    renderContent() {
        return (
            <div className="content">
                <Text>Congratulation Stephanie!</Text>
                <Text>You&apos;ve become a TRAC Member!</Text>
                <Text>Enjoy our service and benefit as a TRAC Member</Text>
            </div>
        );
    }

    renderFooter() {
        return (
            <Button primary onClick={e => this.handleCloseModal(e)}>
                Continue
            </Button>
        );
    }

    render() {
        const { open, size } = this.props;

        const headerConfig = {
            withCloseButton: false,
            position: "center",
            children: this.renderHeader()
        };

        const contentConfig = {
            position: "center",
            children: this.renderContent()
        };

        const footerConfig = {
            position: "center",
            children: this.renderFooter()
        };

        return (
            <div className="m-modal-success">
                <Modal
                    open={open}
                    size={size}
                    onClick={this.handleCloseModal}
                    header={headerConfig}
                    content={contentConfig}
                    footer={footerConfig}
                />
            </div>
        );
    }
}

ModalSuccess.defaultProps = {
    open: false,
    size: "medium",
    onCloseModal: e => console.log(e.currentTarget)
};

ModalSuccess.propTypes = {
    open: PropTypes.bool,
    size: PropTypes.string,
    onCloseModal: PropTypes.func,
    goto: PropTypes.string,
    history: PropTypes.object
};

export default withRouter(ModalSuccess);