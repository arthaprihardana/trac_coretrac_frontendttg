/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-31 11:14:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 16:12:38
 */
import React, { memo, Component } from "react";
import PropTypes from "prop-types";
import { Button, Rupiah } from "atom";
import moment from 'moment';
import 'moment/locale/id';
import "./SidebarPaymentDetail.scss";
import { PulseLoader } from "react-spinners";
import { connect } from "react-redux";
import _ from "lodash";

// helpers
import { localStorageDecrypt } from "helpers";
import lang from '../../assets/data-master/language'

class SidebarPaymentDetail extends Component {

    state = {
        vehicle: []
    }

    componentDidMount = () => {
        if(this.props.data) {
            this.setState({
                vehicle: this.props.data
            });
        }
    }

    componentDidUpdate = (prevProps, prevState) => {
        console.log(this.props.validPromo);
        if(this.props.data !== prevProps.data) {
            this.setState({
                vehicle: this.props.data
            });
        }
        if(this.props.validPromo !== prevProps.validPromo) {

                this.setState({
                    vehicle: JSON.parse(localStorage.getItem('selectedCar'))
                })

        }
    }
    
    render() {
        const { showButton, onSubmit, isLoading, type, validPromo } = this.props;
        const { vehicle } = this.state;

        // let vehicle = validPromo !== null ? JSON.parse(localStorage.getItem('selectedCar')) : data
    
        let items = [];
        let totalRent = 0;
        let totalExtras = 0;
        
        let CarRentalFormInput = JSON.parse(localStorageDecrypt('_fi'));
        let rangeDay;
        if(type === "airport-transfer") {
            rangeDay = moment(CarRentalFormInput.data[0].date.endDate).diff(moment(CarRentalFormInput.data[0].date.startDate), 'days');
        } else {
            rangeDay = moment(CarRentalFormInput.date.endDate).diff(moment(CarRentalFormInput.date.startDate), 'days');
        }
        
        if (vehicle.length > 0) {
            items = vehicle.map((car, index) => {
                totalRent = car.rentInfo.priceAfterDiscount > 0 ? totalRent + car.rentInfo.priceAfterDiscount : totalRent + car.rentInfo.basePrice;
                return (
                    <div className="item" key={index}>
                        <div className="label">{type !== "bus-rental" ? lang.car[lang.default] : lang.bus[lang.default] } #{index+1} {car.vehicleAlias !== null ? car.vehicleAlias : car.vehicleTypeDesc}</div>
                        <div className="description">
                            { car.rentInfo.priceAfterDiscount > 0 && 
                            <div className="row">
                                <span className="price-before"><Rupiah value={car.rentInfo.basePrice} /></span>
                            </div> }
                            <div className="row">
                                <div className="col">
                                    { car.rentInfo.priceAfterDiscount && car.rentInfo.priceAfterDiscount > 0 ? <Rupiah value={car.rentInfo.priceAfterDiscount} /> : <Rupiah value={car.rentInfo.basePrice} /> }
                                    { 
                                        type !== "bus-rental" ?
                                            rangeDay > 0 && `x${" "}
                                                ${rangeDay + 1}${" "}
                                                ${rangeDay + 1 > 1
                                                    ? 'days'
                                                    : 'day'}` 
                                        : 
                                            rangeDay > 0 && `${lang.for[lang.default]} ${" "}
                                                ${rangeDay + 1}${" "}
                                                ${rangeDay + 1 > 1
                                                    ? lang.days[lang.default]
                                                    : lang.day[lang.default]}`
                                    }
                                </div>
                                <div className="col">
                                    { type !== "bus-rental" ? 
                                        (car.rentInfo.priceAfterDiscount && car.rentInfo.priceAfterDiscount > 0 ?
                                            <Rupiah value={ rangeDay > 0 ? car.rentInfo.priceAfterDiscount * (rangeDay + 1) : car.rentInfo.priceAfterDiscount} />
                                            : 
                                            <Rupiah value={ rangeDay > 0 ? car.rentInfo.basePrice * (rangeDay + 1) : car.rentInfo.basePrice} />) : 
                                        <Rupiah value={ car.rentInfo.priceAfterDiscount && car.rentInfo.priceAfterDiscount > 0 ? car.rentInfo.priceAfterDiscount : car.rentInfo.basePrice } /> }
                                </div>
                            </div>
                            {car.extraItems !== undefined && car.extraItems.map(extra => {
                                if (extra.amounts > 0) {
                                    totalExtras = totalExtras + extra.total;
                                    return (
                                        <div className="row" key={extra.id}>
                                            <div className="col">
                                                {extra.name}:{" "}
                                                <Rupiah value={parseInt(extra.price)} /> x{" "}
                                                {extra.amounts}{" "}
                                                {extra.amounts > 1
                                                    ? `${extra.pricePer}(s)`
                                                    : extra.pricePer}
                                            </div>
                                            <div className="col">
                                                <Rupiah value={extra.total} />
                                            </div>
                                        </div>
                                    );
                                } else {
                                    return null;
                                }
                            })}
                        </div>
                    </div>
                );
            });
        }
        
        return (
            <div className="m-sidebar-payment-detail">
                <div className="payment-detail-title">{lang.paymentDetails[lang.default]}</div>
                <div className="payment-detail-content">{items}</div>
                <div className="payment-detail-total">
                    <div className="row">
                        <div className="col">{lang.totalPayment[lang.default]}</div>
                        <div className="col">
                        { type !== "bus-rental" ? 
                            <Rupiah value={rangeDay > 0 ? ((totalRent * (rangeDay + 1)) + totalExtras) : (totalRent + totalExtras)} />
                            : <Rupiah value={(totalRent + totalExtras)} /> }
                        </div>
                    </div>
                    { showButton && (
                        <div className="button">
                            <Button type="button" primary onClick={onSubmit} disabled={isLoading ? true : false} >
                                {isLoading ?
                                    <PulseLoader
                                        sizeUnit={"px"}
                                        size={7}
                                        color={'#ffffff'}
                                        loading={isLoading}
                                    /> : type !== "bus-rental" ? `${lang.order[lang.default]} ${vehicle.length} ${vehicle.length > 1 ? lang.cars[lang.default] : lang.car[lang.default]}` : `${lang.order[lang.default]} ${vehicle.length} Bus` }
                            </Button>
                        </div>
                    ) }
                </div>
            </div>
        );
    }

}

SidebarPaymentDetail.defaultProps = {
    data: [],
    showButton: false,
};

SidebarPaymentDetail.propTypes = {
    data: PropTypes.array,
    showButton: PropTypes.bool,
};

const mapStateToProps = ({ discount }) => {
    const { validPromo } = discount
    return { validPromo };
}

export default connect(mapStateToProps, {})(SidebarPaymentDetail);