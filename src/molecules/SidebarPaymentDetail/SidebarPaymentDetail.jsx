import React from "react";
import PropTypes from "prop-types";
import { Button, Rupiah } from "atom";
import "./SidebarPaymentDetail.scss";

const SidebarPaymentDetail = (props) => {
    const { data, showButton, onSubmit } = props;
    let items = [];
    let totalRent = 0;

    if (data.length > 0) {
        items = data.map(car => {
            totalRent = totalRent + car.rentInfo.total;
            return (
                <div className="item" key={car.id}>
                    <div className="label">{car.name}</div>
                    <div className="description">
                        <div className="row">
                            <div className="col">
                                <Rupiah value={car.rentInfo.price} /> x{" "}
                                {car.rentInfo.amounts}{" "}
                                {car.rentInfo.amounts > 1
                                    ? `${car.rentInfo.pricePer}(s)`
                                    : car.rentInfo.pricePer}
                            </div>
                            <div className="col">
                                <Rupiah value={car.rentInfo.total} />
                            </div>
                        </div>
                        {car.extraItems.map(extra => {
                            if (extra.amounts > 0) {
                                totalRent = totalRent + extra.total;
                                return (
                                    <div className="row" key={extra.id}>
                                        <div className="col">
                                            {extra.name}:{" "}
                                            <Rupiah value={extra.price} /> x{" "}
                                            {extra.amounts}{" "}
                                            {extra.amounts > 1
                                                ? `${extra.pricePer}(s)`
                                                : extra.pricePer}
                                        </div>
                                        <div className="col">
                                            <Rupiah value={extra.total} />
                                        </div>
                                    </div>
                                );
                            }
                            return null;
                        })}
                    </div>
                </div>
            );
        });
    }

    return (
        <div className="m-sidebar-payment-detail">
            <div className="payment-detail-title">Payment Details</div>
            <div className="payment-detail-content">{items}</div>
            <div className="payment-detail-total">
                <div className="row">
                    <div className="col">Total Payment</div>
                    <div className="col">
                        <Rupiah value={totalRent} />
                    </div>
                </div>
                { showButton && (
                    <div className="button">
                        <Button type="button" primary onClick={onSubmit}>
                            Order Car
                        </Button>
                    </div>
                ) }
            </div>
        </div>
    );

};

SidebarPaymentDetail.defaultProps = {
    data: [],
    showButton: false,
};

SidebarPaymentDetail.propTypes = {
    data: PropTypes.array,
    showButton: PropTypes.bool,
};

export default React.memo(SidebarPaymentDetail);