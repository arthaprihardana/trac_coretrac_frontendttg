import React from "react";
import PropTypes from "prop-types";
import { Button } from "atom";
import "./SidebarPromoCode.scss";

const SidebarPromoCode = props => {

    return (
        <div className="m-sidebar-promo-code">
            <div className="promo-code-title">
                Do You have Promo/Referral Code?
            </div>
            <div className="promo-code-content">
                <div className="input">
                    <input
                        type="text"
                        className="input-text"
                        placeholder="Enter Promo/Referral Code"
                    />
                </div>
                <div className="button">
                    <Button type="button" outline>
                        Apply
                    </Button>
                </div>
            </div>
        </div>
    );

};

SidebarPromoCode.defaultProps = {
    data: ""
};

SidebarPromoCode.propTypes = {
    data: PropTypes.string
};

export default React.memo(SidebarPromoCode);
