/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-22 14:08:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 07:43:48
 */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, Modal } from "atom";
import { connect } from "react-redux";
import { getValidatePromo, isPromoValidate, savePromoCode } from "actions";
import { PulseLoader } from "react-spinners";
import _ from "lodash";
import "./SidebarPromoCode.scss";
import { localStorageDecrypt } from "helpers";
//language
import lang from '../../assets/data-master/language'

class SidebarPromoCode extends Component {

    state = {
        PromoCode: "",
        modalOpen: false,
        content: null,
        disablePromo: false
    }

    componentWillUnmount = () =>{
        // this.props.isPromoValidate(false);  //jadi false kalau reset
        // this.props.savePromoCode(null); //jadi null
        // console.log(this.props.validPromo);
    };

    componentDidMount = () => {
        this.handleResetPromo();
        // if(this.props.validPromo) {
        //     this.setState({
        //         disablePromo: true
        //     })
        // }
        if(this.props.promoCode !== null) {
            this.setState({
                PromoCode: this.props.promoCode
            })
        }
    }
    

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.promo !== prevProps.promo) {
            return this.props.promo;
        }
        return null;
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if(snapshot !== null) {
            if(_.isArray(snapshot.data)) {
                this.setState({
                    modalOpen: true,
                    content: <div className="success-login-wrapper">
                        <p className="main-title">{lang.weApologize[lang.default]}</p>
                        <p className="sub-title">{lang.yourPromoNotValid[lang.default]}</p>
                    </div>
                })
            } else {
                let selectedCar = JSON.parse(localStorage.getItem('selectedCar'));
                let promo = snapshot.data;
                localStorage.setItem('selectedPromo', JSON.stringify(promo));
                if(promo.is_all_vehicle === 1) {
                    var validVehiclePromo = [];
                    this.props.isPromoValidate(true);  //jadi false kalau reset
                    this.props.savePromoCode(this.state.PromoCode); //jadi null
                    switch (promo.type_value.replace(/\s/g,'')) {
                        case "percentage":
                            _.map(selectedCar, vv => {
                                vv.rentInfo.priceAfterDiscount = vv.rentInfo.basePrice - ((vv.rentInfo.basePrice * parseInt(promo.value)) / 100);
                                vv.rentInfo.total = (vv.rentInfo.basePrice - ((vv.rentInfo.basePrice * parseInt(promo.value)) / 100)) * vv.rentInfo.amounts;
                                validVehiclePromo.push(vv);
                            })
                            break;
                        case "nominal":
                            _.map(selectedCar, vv => {
                                vv.rentInfo.priceAfterDiscount = vv.rentInfo.basePrice - parseInt(promo.value);
                                vv.rentInfo.total = (vv.rentInfo.basePrice - parseInt(promo.value)) * vv.rentInfo.amounts;
                                validVehiclePromo.push(vv);
                            });
                            break;
                        default:
                            break;
                    }
                    this.setState({
                        modalOpen: true,
                        disablePromo: true,
                        content: <div className="success-login-wrapper">
                            <p className="main-title">{lang.congrats[lang.default]}</p>
                            <p className="sub-title">{lang.yourPromoValidAll[lang.default]}</p>
                        </div>
                    }, () => localStorage.setItem('selectedCar', JSON.stringify(validVehiclePromo)))
                } else {
                    var validVehiclePromo = [];
                    var invalidVehiclePromo = [];
                    // _.map(promo.vehicle_id, v => {
                    _.map(promo.vehicle_id, v => {
                        let valid = _.filter(selectedCar, { vehicleTypeId: v.vehicle_id });

                        // let invalid=[];

                        // let invalid = _.filter(selectedCar, o => o.vehicleTypeId !== v.vehicle_id);
                        // let valid = _.filter(selectedCar, { vehicleTypeId: v.vehicle_id });
                        if(valid.length > 0) {

                            // invalid = obj_;
                            this.props.isPromoValidate(true);
                            this.props.savePromoCode(this.state.PromoCode);
                            switch (promo.type_value.replace(/\s/g,'')) {
                                case "percentage":
                                    _.map(valid, vv => {
                                        // console.log('vv ==>', vv);
                                        vv.rentInfo.priceAfterDiscount = vv.rentInfo.basePrice - ((vv.rentInfo.basePrice * parseInt(promo.value)) / 100);
                                        vv.rentInfo.total = (vv.rentInfo.basePrice - ((vv.rentInfo.basePrice * parseInt(promo.value)) / 100)) * vv.rentInfo.amounts;
                                        validVehiclePromo.push(vv);
                                    });
                                    break;
                                case "nominal":
                                    _.map(valid, vv => {
                                        vv.rentInfo.priceAfterDiscount = vv.rentInfo.basePrice - parseInt(promo.value);
                                        vv.rentInfo.total = (vv.rentInfo.basePrice - parseInt(promo.value)) * vv.rentInfo.amounts;
                                        validVehiclePromo.push(vv);
                                    });
                                    break;
                                default:
                                    break;
                            }
                        } else {

                        }

                    });

                    if(validVehiclePromo.length > 0) {
                        let obj_ = selectedCar.filter(sel=>{
                            let tmp = validVehiclePromo.some(t=>{
                                return t.vehicleTypeId === sel.vehicleTypeId
                            });
                            if(!tmp){
                                return sel;
                            }
                        });
                        _.map(obj_, vv => {
                            vv.rentInfo.priceAfterDiscount = 0;
                            vv.rentInfo.total = vv.rentInfo.basePrice * vv.rentInfo.amounts;
                            invalidVehiclePromo.push(vv);
                        });
                        let vehicleName = _.map(_.uniqBy(validVehiclePromo, 'vehicleTypeId'), v => v.vehicleAlias !== null ? v.vehicleAlias : v.vehicleTypeDesc);
                        this.setState({
                            modalOpen: true,
                            disablePromo: true,
                            content: <div className="success-login-wrapper">
                                <p className="main-title">{lang.congrats[lang.default]}</p>
                                <p className="sub-title">{lang.yourPrmoCodeValid[lang.default]} {vehicleName.toString()}. Enjoy!</p>
                            </div>
                        })
                    }else{
                        _.map(selectedCar, vv => {
                            vv.rentInfo.priceAfterDiscount = 0;
                            vv.rentInfo.total = vv.rentInfo.basePrice * vv.rentInfo.amounts;
                            invalidVehiclePromo.push(vv);
                        });
                        this.setState({
                            modalOpen: true,
                            content: <div className="success-login-wrapper">
                                <p className="main-title">{lang.weApologize[lang.default]}</p>
                                <p className="sub-title">{lang.yourPromoNotValid[lang.default]}</p>
                            </div>
                        })
                    }
                    var concatVehicle = _.concat(validVehiclePromo, invalidVehiclePromo);
                    localStorage.setItem('selectedCar', JSON.stringify(concatVehicle));
                }
            }
        }
    }

    handleSubmitPromo = () => {
        const { PromoCode } = this.state;
        const { data } = this.props;
        let local_auth = localStorageDecrypt('_aaa', 'object');
        if(local_auth !== null){
            this.props.getValidatePromo({
                PromoCode: PromoCode,
                user_id: local_auth.UserLoginId,
                business_unit_id: data[0].businessUnitId,
                branch_id: data[0].branchId
            });
        }else{
            this.props.getValidatePromo({
                PromoCode: PromoCode,
                business_unit_id: data[0].businessUnitId,
                branch_id: data[0].branchId
            });
        }

    }
    handleResetPromo = ()=>{
        this.props.savePromoCode(null);
        // this.props.validPromo = false;
        let selectedCar = JSON.parse(localStorage.getItem('selectedCar'));
        var validVehiclePromo = [];
        _.map(selectedCar, vv => {
            vv.rentInfo.priceAfterDiscount = null;
            vv.rentInfo.total = vv.rentInfo.basePrice * vv.rentInfo.amounts;
            validVehiclePromo.push(vv);
        });
        localStorage.removeItem('selectedPromo');
        this.setState({
            PromoCode : "",
            disablePromo: false,
        }, () => { localStorage.setItem('selectedCar', JSON.stringify(validVehiclePromo));
            this.props.isPromoValidate(false);
        })
    };

    handleModalClose = () => {
        this.setState({
            modalOpen: !this.state.modalOpen
        })
    }

    render() {
        const { loading } = this.props;
        const { modalOpen, disablePromo } = this.state;
        return (
            <div className="m-sidebar-promo-code">
                <Modal
                    open={modalOpen}
                    onClick={this.handleModalClose}
                    size="small"
                    header={{
                        withCloseButton: false,
                        onClick: this.handleModalClose,
                        children: ''
                    }}
                    content={{
                        children: (this.state.content),
                    }}
                    footer={{
                        position: 'center',
                        children: (
                            <div className="modal-footer-action">
                                <Button primary onClick={this.handleModalClose}>Close</Button>
                            </div>
                        ),
                    }}
                />
                <div className="promo-code-title">
                    {lang.doYouHavePromo[lang.default]}
                </div>
                <div className="promo-code-content">
                    <div className="input">
                        <input
                            type="text"
                            className="input-text"
                            placeholder={lang.enterPromoReferral[lang.default]}
                            value={this.state.PromoCode}
                            onChange={e => this.setState({ PromoCode: e.currentTarget.value.toUpperCase() })}
                            readOnly={disablePromo}
                        />
                    </div>
                    <div className="button">
                        { !disablePromo ?
                        <Button disabled={this.state.PromoCode.length===0} type="button" outline onClick={() => this.handleSubmitPromo() } outlineDisabled={loading ? true : false}>
                            { loading ? <PulseLoader
                                sizeUnit={"px"}
                                size={7}
                                color={'#f47d00'}
                                loading={loading}
                            /> : lang.apply[lang.default] }
                        </Button> : <Button type="button" outline onClick={() => {this.handleResetPromo();
                                this.setState({
                                    modalOpen: true,
                                    content: <div className="success-login-wrapper">
                                        <p className="main-title">{lang.promoCodeReset[lang.default]}</p>
                                        <p className="sub-title">{lang.yourOrderHasNoDiscount[lang.default]}</p>
                                    </div>
                                })
                        } } >
                                X
                            </Button> }
                    </div>
                </div>
            </div>
        );
    }

}

SidebarPromoCode.defaultProps = {
    data: []
};

SidebarPromoCode.propTypes = {
    data: PropTypes.array
};

const mapStateToProps = ({ discount }) => {
    const { loading, promo, validPromo, promoCode } = discount
    return { loading, promo, validPromo, promoCode }
}

export default connect(mapStateToProps, {
    getValidatePromo,
    isPromoValidate,
    savePromoCode
})(SidebarPromoCode);
