import React from "react";
import uuid from "uuid/v4";
import PropTypes from "prop-types";
import "./OrderSteps.scss";

const OrderSteps = (props) => {
    const { data } = props;

    return (
        <div className="m-order-steps">
            <div className="container">
                <div className="steps-wrapper">
                    {data.map(value => (
                        <div key={uuid()} className={`item ${value.className}`}>
                            {value.step}
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );

};

OrderSteps.defaultProps = {
    data: []
};

OrderSteps.propTypes = {
    data: PropTypes.arrayOf(
        PropTypes.shape({
            step: PropTypes.string,
            className: PropTypes.string
        })
    )
};

export default React.memo(OrderSteps);