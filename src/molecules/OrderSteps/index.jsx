/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-08 10:21:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-21 14:43:57
 */
import React, { memo } from "react";
import uuid from "uuid/v4";
import PropTypes from "prop-types";
import cx from 'classnames';

// assets & styles
import carRentalStep from './car-rental-step.json';
import airportTransferStep from './airport-transfer-step.json';
import busRentalStep from './bus-rental-step.json';
import "./OrderSteps.scss";

const OrderSteps = ({ activeStep, type }) => {
    let data = carRentalStep;
    // switch(type){
    //     case 'car-rental':
    //         data = carRentalStep;
    //         break;
    //     case 'airport-transfer':
    //         data = airportTransferStep;
    //         break;
    //     case 'bus-rental':
    //         data = busRentalStep;
    //         break;
    //     default:
    //         data = carRentalStep;
    // }

    return (
        <div className="m-order-steps">
            <div className="container">
                <div className="steps-wrapper">
                    {data.map((value, index) => {

                        let classActiveState = cx('item', {
                            'done': index + 1 < activeStep,
                            'current': index + 1 === activeStep,
                        })
                        
                        return (
                            <div key={uuid()} className={classActiveState}>
                                {value.step}
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    );

};

OrderSteps.propTypes = {
    activeStep: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
};

export default memo(OrderSteps);