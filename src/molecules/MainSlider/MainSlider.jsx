import React from "react";
import PropTypes from "prop-types";

// style
import "./MainSlider.scss";

const MainSlider = (props) => {
    const { data, mainSlideActive } = props;

    setTimeout(function() {
        document.querySelectorAll(".m-main-slider")[0].classList.remove('hide');
    }, 850);

    return (
        <div className="m-main-slider hide">
            { data.map(val => (
                <div
                    key={val.id}
                    className={`item${
                        mainSlideActive === val.id ? " active" : ""
                    }`}
                >
                    <div className="image">
                        <img src={val.imageDesktop} alt={val.title} className="desktop"/>
                        <img src={val.imageMobile} alt={val.title} className="mobile"/>
                    </div>
                    <div className="container">
                        <div className="caption">
                            <h1>{val.title}</h1>
                        </div>
                    </div>
                </div>
            )) }
        </div>
    );

};

MainSlider.defaultProps = {
    data: []
};

MainSlider.propTypes = {
    data: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            imageDesktop: PropTypes.string,
            imageMobile: PropTypes.string
        })
    )
};

export default React.memo(MainSlider);