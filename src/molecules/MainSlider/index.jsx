/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-23 21:55:35 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-01-23 21:55:35 
 */
import React, { Component } from "react";
import PropTypes from "prop-types";

// style
import "./MainSlider.scss";

class MainSlider extends Component {

    componentDidMount = () => {
        setTimeout(function() {
            document.querySelectorAll(".m-main-slider")[0].classList.remove('hide');
        }, 850);
    }

    componentDidCatch(error, info) {
        console.log('info.componentStack ==>', info.componentStack);
    }

    render() {
        const { data, mainSlideActive } = this.props;
    
        return (
            <div className="m-main-slider hide">
                { data.map(val => (
                    <div
                        key={val.id}
                        className={`item${
                            mainSlideActive === val.id ? " active" : ""
                        }`}
                    >
                        <div className="image">
                            <img src={val.imageDesktop} alt={val.title} className="desktop"/>
                            <img src={val.imageMobile} alt={val.title} className="mobile"/>
                        </div>
                        <div className="container">
                            <div className="caption">
                                <h1>{val.title}</h1>
                            </div>
                        </div>
                    </div>
                )) }
            </div>
        );
    }

}

// const MainSlider = (props) => {
//     const { data, mainSlideActive } = props;

//     setTimeout(function() {
//         console.log('window ==>', window.document.querySelectorAll(".m-main-slider"))
//         document.querySelectorAll(".m-main-slider")[0].classList.remove('hide');
//     }, 850);

//     return (
//         <div className="m-main-slider hide">
//             { data.map(val => (
//                 <div
//                     key={val.id}
//                     className={`item${
//                         mainSlideActive === val.id ? " active" : ""
//                     }`}
//                 >
//                     <div className="image">
//                         <img src={val.imageDesktop} alt={val.title} className="desktop"/>
//                         <img src={val.imageMobile} alt={val.title} className="mobile"/>
//                     </div>
//                     <div className="container">
//                         <div className="caption">
//                             <h1>{val.title}</h1>
//                         </div>
//                     </div>
//                 </div>
//             )) }
//         </div>
//     );

// };

MainSlider.defaultProps = {
    data: []
};

MainSlider.propTypes = {
    data: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            imageDesktop: PropTypes.string,
            imageMobile: PropTypes.string
        })
    )
};

export default React.memo(MainSlider);