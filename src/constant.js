/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-10 10:37:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 19:22:59
 */

// 'STID-003' //Rental TimeBase
// 'STID-004' //Rental KM Base
// 'STID-007' //Rental Hourly
// 'STID-083' //UAS TimeBase
// 'STID-084' //UAS KM Base

export const SECRET_KEY = '153fa3e1352cd10281496006eb41f9b6';
const PROTOCOL = 'https:';
const PROTOCOL2 = 'http:';
const HOST_CONFIG = 'https://omni-service-configuration-production.azurewebsites.net/api/b2c';
export const MY_BASE_URL = process.env.NODE_ENV === "development" ? `${PROTOCOL2}//localhost:3000` : `${PROTOCOL2}//www.trac.astra.co.id`;

// PRODUCT SERVICE TYPE
export const RENTAL_TIMEBASE = 'STID-003';
export const RENTAL_KMBASE = 'STID-004';
export const RENTAL_HOURLY = 'STID-007';
export const UAS_TIMEBASE = 'STID-083';
export const UAS_KMBASE = 'STID-084';

//PRODUCT ID
export const CAR_RENTAL = "PRD0006";
export const AIRPORT = "PRD0007";
export const BU_CAR = "0104";
export const BU_BUS = "0201";


//TOKEN TEMPORARY
export const TMP_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vb21uaS1zZXJ2aWNlLWF1dGhvcml6YXRpb24uYXp1cmV3ZWJzaXRlcy5uZXQvYXBpL2IyYy9hdXRob3JpemF0aW9uL2ludGVybmFsL2xvZ2luIiwiaWF0IjoxNTUxNzYxOTk4LCJleHAiOjE1NTE3OTA3OTgsIm5iZiI6MTU1MTc2MTk5OCwianRpIjoieW5YVDMxaUpkSnU2WWZ4dSIsInN1YiI6IlUtMDA2NSIsInBydiI6IjRlMzEwMTVmOGRiZTEzZjExMDNmYTYwNjcyMDgwZmQ5OGZjYjgzZjUifQ.20RnWn7vvQpNMlxyqDzpos5U8pvr07DBfTPRBEAtv64';

// BILLING
export const SONotCreated = 'INV-001';
export const Paid = 'INV-003';

// AUTH0
export const AUTH0_DOMAIN = 'trac-rental.auth0.com';
// export const AUTH0_DOMAIN = 'ttg-development.au.auth0.com';
export const AUTH0_CLIENT_ID = 'QReE5HvZtJO7x9Hl2yWwOhLSnaf2nhvX';
// export const AUTH0_CLIENT_ID = 'gK3b9YSXa0TBuhZTVY_t-Dj7A3Hvq7o8';

// GOOGLE MAPS
export const GOOGLE_MAP_API = 'AIzaSyCZJJZSHLnLHsNEcAB8uw1nFAqoqP52oUs';
// export const GOOGLE_MAP_API = 'AIzaSyCiUtZ8k6qn4ZFR0l6ukT0Uck5rdu_LPPo';
export const GOOGLE_PLACE_AUTOCOMPLETE = `${HOST_CONFIG}/configuration/gmaps/auto-complete`;
// export const GOOGLE_PLACE_AUTOCOMPLETE = `${PROTOCOL2}//188.166.174.221:9090/api/place/autocomplete/json`;
export const GOOGLE_PLACE_DETAIL = `${HOST_CONFIG}/configuration/gmaps/place-detail`;
// export const GOOGLE_PLACE_DETAIL = `${PROTOCOL2}//188.166.174.221:9090/api/place/details/json`;
export const GOOGLE_FIND_PLACE = `${HOST_CONFIG}/configuration/gmaps/place-coordinate`;
// export const GOOGLE_FIND_PLACE = `${PROTOCOL2}//188.166.174.221:9090/api/place/findplacefromtext/json`;
export const GOOGLE_GET_DISTANCE_MATRIX = `${HOST_CONFIG}/configuration/gmaps/distance-matrix`;
// export const GOOGLE_GET_DISTANCE_MATRIX = `${PROTOCOL2}//188.166.174.221:9090/api/distancematrix/json`;

// BASE API URL
const BASE_URL_BTC = prm => `${PROTOCOL}//omni-service-${prm}-production.azurewebsites.net/api/b2c`;
const BASE_URL = prm => `${PROTOCOL}//omni-service-${prm}-production.azurewebsites.net/api/`;
const SM_BASE_URL = `https://sm-prd.azurewebsites.net`;
export const SM_APP_KEY = '758E1DBF-9D2C-48FC-BD75-C1E26F1D2E4D';

// CONTACT US
// https://omni-service-notification.azurewebsites.net/api/email/contact-us/send
export const POST_CONTACT_US = `${BASE_URL('notification')}email/contact-us/send`;

// AUTHORIZATION
export const LOGIN = `${BASE_URL_BTC('authorization')}/authorization/login/`;
export const LOGIN_SOSMED = `${BASE_URL_BTC('authorization')}/authorization/loginSocialite/`;
export const REGISTER = `${BASE_URL_BTC('authorization')}/authorization/register/`;
export const LOGOUT = `${BASE_URL_BTC('authorization')}/authorization/logout/`;
export const REFRESH_TOKEN = `${BASE_URL_BTC('authorization')}/authorization/refresh-token/`;
export const CHECK_TOKEN_URL = `${BASE_URL_BTC('authorization')}/authorization/check-token/`;
export const USER = `${BASE_URL_BTC('authorization')}/authorization/get-user/`;
export const FORGOT_PASSWORD = `${BASE_URL_BTC('authorization')}/authorization/forgot-password/`;
export const USER_PROFILE = `${BASE_URL_BTC('authorization')}/authorization/user-profile`;
export const USER_PROFILE_UPDATE = `${BASE_URL_BTC('authorization')}/authorization/user-profile/update`;
export const UPDATE_NEW_PASSWORD = `${BASE_URL_BTC('authorization')}/authorization/update-password/`;
export const CHANGE_PASSWORD_INTERNAL = `${BASE_URL_BTC('authorization')}/authorization/change-password/`;
export const VERIFY_ACCOUNT = `${BASE_URL_BTC('authorization')}/authorization/activate`;
export const GET_CREDITCARD = `${BASE_URL_BTC('authorization')}/authorization/credit-card-account/get-by-user/`;
export const CREATE_CREDITCARD = `${BASE_URL_BTC('authorization')}/authorization/credit-card-account/save`;
export const UPDATE_CREDITCARD = `${BASE_URL_BTC('authorization')}/authorization/credit-card-account/update`;
export const DELETE_CREDITCARD = `${BASE_URL_BTC('authorization')}/authorization/credit-card-account/delete`;
export const UPDATE_PRIMARY_CC = `${BASE_URL_BTC('authorization')}/authorization/credit-card-account/set-primary/`;

// MASTER
export const LIST_CAR_TYPE = `${BASE_URL_BTC('configuration')}/configuration/car-type/get-by-param`;
export const BANK = `${BASE_URL_BTC('configuration')}/configuration/bank/get-by-param`;
export const BRANCH = `${BASE_URL_BTC('configuration')}/configuration/branch/get-branch/business-unit`;    // {business_unit_id}
export const VEHICLE_ATTR = `${BASE_URL_BTC('configuration')}/configuration/vehicle-attribute`;
export const STATUS_ORDER = `${BASE_URL_BTC('order')}/order/status`;

// PRODUCT
export const LIST_PRODUCT = `${BASE_URL_BTC('product')}/product/product/`;
export const LIST_ARTICLE = `${BASE_URL_BTC('product')}/product/content-article`;
export const LIST_PROMO = `${BASE_URL_BTC('product')}/product/content-promo`;

/** CAR RENTAL */
// CITY COVERAGE
export const CITY_COVERAGE_CAR_RENTAL = args => `${BASE_URL_BTC('configuration')}/configuration/city-coverage/get-by-param/${args[0]}/${args[1]}/${args[2]}`; // {CustomerId}/{BusinessUnitId}/{MsProductId}
// RENTAL DURATION
export const RENTAL_DURATION_CAR_RENTAL = `${BASE_URL_BTC('configuration')}/configuration/rental-duration`;
// LIST STOCK CAR RENTAL
export const LIST_STOCK_CAR_RENTAL = `${SM_BASE_URL}/stock/list`;
// EXTRAS CAR RENTAL
export const EXTRAS_CAR_RENTAL = `${BASE_URL_BTC('configuration')}/configuration/extras-detail/get-by-param`;
// PRICE
export const PRICE_PRODUCT_CAR_RENTAL = `${BASE_URL_BTC('price')}/price/price-product-retail/get-by-param`;
// RESERVATION
export const RESERVATION_CAR_RENTAL = `${BASE_URL_BTC('order')}/order/reservation/save`;

/** AIRPORT TRANSFER */
// MASTER AIRPORT
export const MASTER_AIRPORT = `${BASE_URL_BTC('configuration')}/configuration/airport`;
// AIRPORT COVERAGE
export const AIRPORT_COVERAGE = `${BASE_URL_BTC('configuration')}/configuration/airport-coverage/get-by-param`;
// DURATION AIRPORT
export const DURATION_AIRPORT = args => `${BASE_URL_BTC('price')}/price/zone/get-by-param/${args[0]}/${args[1]}/${args[2]}`;  // BusinessUnitId/MsProductId/Distance


/** BUS RENTAL */
// PRICE BUS
export const PRICE_PRODUCT_BUS = `${BASE_URL_BTC('price')}/price/price-bus-retail/get-price`;

/** BILLING */
export const CREATE_INVOICE = `${BASE_URL_BTC('billing')}/billing/invoice-header/save`;

/** DISCOUNT */
export const VALIDATE_PROMO = args => `${BASE_URL('discount')}/discount/promo/validate/${args}/`;    // ?user_id=USR-001&business_unit_id=0104&branch_id=SR01

/** CUSTOMER DASHBOARD */
export const MY_BOOKING = `${BASE_URL_BTC('order')}/order/reservation/get-my-booking`;
export const MY_HISTORY = `${BASE_URL_BTC('order')}/order/reservation/get-history`;
export const MY_BOOKING_DETAIL = `${BASE_URL_BTC('order')}/order/reservation/detail`;

export const ADJUSTMENT_RETAIL = `${BASE_URL_BTC('price')}/price/adjustment-retail`;