/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-15 14:38:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 20:21:42
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import {
    GET_LIST_PRODUCT,
    GET_LIST_PRODUCT_SUCCESS,
    GET_LIST_PRODUCT_FAILURE,
    SELECTED_PRODUCT_SERVICE_TYPE,
    GET_LIST_ARTICLE, GET_LIST_ARTICLE_SUCCESS, GET_LIST_ARTICLE_FAILURE, GET_LIST_PROMO, GET_LIST_PROMO_SUCCESS, GET_LIST_PROMO_FAILURE
} from "../actions/Types";

const INIT_STATE = {
    loading: false,
    listproductsuccess: null,
    listArticleSuccess: null,
    listArticleFailure: null,
    selectedproductservicetype: null
}

const persistConfig = {
    key: 'product',
    storage: storage,
    whitelist: ['selectedproductservicetype']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_LIST_PRODUCT:
            return { ...state, loading: true }
        case GET_LIST_PRODUCT_SUCCESS:
            return { ...state, loading: false, listproductsuccess: action.payload }
        case GET_LIST_PRODUCT_FAILURE:
            return { ...state, loading: false, listproductsuccess: null }
        case GET_LIST_ARTICLE:
            return { ...state, loading: true }
        case GET_LIST_ARTICLE_SUCCESS:
            return { ...state, loading: false, listArticleSuccess: action.payload, listArticleFailure:null }
        case GET_LIST_ARTICLE_FAILURE:
            return { ...state, loading: false,listArticleSuccess: null, listArticleFailure:action.payload }
        case GET_LIST_PROMO:
            return { ...state, loading: true }
        case GET_LIST_PROMO_SUCCESS:
            return { ...state, loading: false, listPromoSuccess: action.payload, listPromoFailure:null }
        case GET_LIST_PROMO_FAILURE:
            return { ...state, loading: false,listPromoSuccess: null, listPromoFailure:action.payload }
        case SELECTED_PRODUCT_SERVICE_TYPE:
            return { ...state, selectedproductservicetype: action.payload }
        default:
            return { ...state }
    }
})