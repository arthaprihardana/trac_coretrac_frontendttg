/**
 * @author: Artha Prihardana 
 * @Date: 2018-12-31 19:34:02 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 19:21:42
 */
import { combineReducers } from 'redux';
import RegisterReducer from './Register';
import ForgotPasswordReducer from './ForgotPassword';
import LoginReducer from './Login';
import MasterCityReducer from './MasterCity';
import MasterCarTypeReducer from './MasterCarType';
import CarRentalReducer from './CarRental';
import ProductReducer from './Product';
import AddressGoogleMapReducer from './AddressGoogleMaps';
import MasterAirportReducer from './MasterAirport';
import AirportTransferReducer from './AirportTransfer';
import MasterBankReducer from './MasterBank';
import BusRentalReducer from './BusRental';
import BillingReducer from './Billing';
import DiscountReducer from './Discount';
import UserReducer from './User';
import CustomerDashboardReducer from './CustomerDashboard';
import MasterBranchReducer from './MasterBranch';
import VehicleAttributeReducer from './VehicleAttribute';
import AdjustmentRetailReducer from './AdjustmentRetail';

const reducers = combineReducers({
    register: RegisterReducer,
    forgotpassword: ForgotPasswordReducer,
    login: LoginReducer,
    mastercity: MasterCityReducer,
    mastercartype: MasterCarTypeReducer,
    carrental: CarRentalReducer,
    product: ProductReducer,
    addressgm: AddressGoogleMapReducer,
    masterairport: MasterAirportReducer,
    airporttransfer: AirportTransferReducer,
    masterbank: MasterBankReducer,
    busrental: BusRentalReducer,
    billing: BillingReducer,
    discount: DiscountReducer,
    userprofile: UserReducer,
    dashboard: CustomerDashboardReducer,
    masterbranch: MasterBranchReducer,
    vehicleattr: VehicleAttributeReducer,
    adjustmentRetail: AdjustmentRetailReducer
});

export default reducers;