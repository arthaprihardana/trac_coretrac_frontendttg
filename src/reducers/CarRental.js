/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-07 14:10:10 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 17:27:51
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import _ from "lodash";
import { GET_STOCK_CAR_RENTAL, GET_STOCK_CAR_RENTAL_SUCCESS, GET_STOCK_CAR_RENTAL_FAILURE, GET_PACKAGE_CAR_RENTAL, GET_PACKAGE_CAR_RENTAL_SUCCESS, GET_PACKAGE_CAR_RENTAL_FAILURE, GET_CITY_COVERAGE_CAR_RENTAL, GET_CITY_COVERAGE_CAR_RENTAL_SUCCESS, GET_CITY_COVERAGE_CAR_RENTAL_FAILURE, MAX_CART_UNIT_VEHICLE, SELECTED_CAR_ORDER_CAR_RENTAL, GET_EXTRAS_CAR_RENTAL, GET_EXTRAS_CAR_RENTAL_SUCCESS, GET_EXTRAS_CAR_RENTAL_FAILURE, GET_CAR_RENTAL_PRICE, GET_CAR_RENTAL_PRICE_SUCCESS, GET_CAR_RENTAL_PRICE_FAILURE, POST_RESERVATION_CAR_RENTAL, POST_RESERVATION_CAR_RENTAL_SUCCESS, POST_RESERVATION_CAR_RENTAL_FAILURE, NOTES_REQUEST_CAR_RENTAL, PASSANGER_INFO_CAR_RENTAL, CLEAR_CAR_RENTAL_PRICE, FILTER_CAR_TYPE_CAR_RENTAL, FILTER_PRICE_RANGE_CAR_RENTAL, SORT_PRICE_CAR_RENTAL, VALIDATE_STOCK_EXTRAS } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    packagecarrentalsuccess: null,
    packagecarrentalfailure: null,
    citycoveragecarrentalsuccess: null,
    citycoveragecarrentalfailure: null,
    stockcarrentalsuccess: null,
    stockcarrentalfailure: null,
    extrascarrentalsuccess: null,
    extrascarrentalfailure: null,
    pricecarrentalsuccess: null,
    pricecarrentalfailure: null,
    maxtotalunitcart: false,
    selectedcar: null,
    reservationsuccess: null,
    reservationfailure: null,
    notes: null,
    passanger: [],
    filtercartype: [],
    filterprice: {},
    sortprice: null,
    stockextras: null
};

const persistConfig = {
    key: 'carrental',
    storage: storage,
    whitelist: ['citycoveragecarrentalsuccess', 'notes']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_PACKAGE_CAR_RENTAL:
            return { ...state, loading: true }
        case GET_PACKAGE_CAR_RENTAL_SUCCESS:
            return { ...state, loading: false, packagecarrentalsuccess: action.payload, packagecarrentalfailure: null }
        case GET_PACKAGE_CAR_RENTAL_FAILURE:
            return { ...state, loading: false, packagecarrentalsuccess: null, packagecarrentalfailure: action.payload }

        case GET_CITY_COVERAGE_CAR_RENTAL:
            return { ...state, loading: true }
        case GET_CITY_COVERAGE_CAR_RENTAL_SUCCESS:
            let newCity = _.map(action.payload, val => { 
                return {
                    id: val.BranchId, 
                    name: _.startCase(_.lowerCase(val.MsCityName)),
                    cityId: val.MsCityId
                } 
            });
            return { ...state, loading: false, citycoveragecarrentalsuccess: newCity, citycoveragecarrentalfailure: null }
        case GET_CITY_COVERAGE_CAR_RENTAL_FAILURE:
            return { ...state, loading: false, citycoveragecarrentalsuccess: null, citycoveragecarrentalfailure: action.payload }

        case GET_STOCK_CAR_RENTAL:
            return { ...state, loading: true }
        case GET_STOCK_CAR_RENTAL_SUCCESS:
            return { ...state, loading: false, stockcarrentalsuccess: action.payload, stockcarrentalfailure: null }
        case GET_STOCK_CAR_RENTAL_FAILURE:
            return { ...state, loading: false, stockcarrentalsuccess: null, stockcarrentalfailure: action.payload }
        case CLEAR_CAR_RENTAL_PRICE:
            return { ...state, stockcarrentalsuccess: null, stockcarrentalfailure: null }

        case POST_RESERVATION_CAR_RENTAL:
            return { ...state, loading: true }
        case POST_RESERVATION_CAR_RENTAL_SUCCESS:
            return { ...state, loading: false, reservationsuccess: action.payload, reservationfailure: null }
        case POST_RESERVATION_CAR_RENTAL_FAILURE:
            return { ...state, loading: false, reservationsuccess: null, reservationfailure: action.payload }
        
        case GET_EXTRAS_CAR_RENTAL:
            return { ...state, loading: true }
        case GET_EXTRAS_CAR_RENTAL_SUCCESS:
            return { ...state, loading: false, extrascarrentalsuccess: action.payload, extrascarrentalfailure: null }
        case GET_EXTRAS_CAR_RENTAL_FAILURE:
            return { ...state, loading: false, extrascarrentalsuccess: null, extrascarrentalfailure: action.payload }
        case VALIDATE_STOCK_EXTRAS:
            return { ...state, stockextras: action.payload }

        case GET_CAR_RENTAL_PRICE:
            return { ...state, loading: true }
        case GET_CAR_RENTAL_PRICE_SUCCESS:
            return { ...state, loading: false, pricecarrentalsuccess: action.payload, pricecarrentalfailure: null }
        case GET_CAR_RENTAL_PRICE_FAILURE:
            return { ...state, loading: false, pricecarrentalsuccess: null, pricecarrentalfailure: action.payload }

        case MAX_CART_UNIT_VEHICLE:
            return { ...state, maxtotalunitcart: action.payload }

        case SELECTED_CAR_ORDER_CAR_RENTAL:
            return { ...state, selectedcar: action.payload }

        case NOTES_REQUEST_CAR_RENTAL:
            return { ...state, notes: action.payload }

        case PASSANGER_INFO_CAR_RENTAL:
            return { ...state, passanger: action.payload }

        case FILTER_CAR_TYPE_CAR_RENTAL:
            return { ...state, filtercartype: action.payload }
        
        case FILTER_PRICE_RANGE_CAR_RENTAL:
            return { ...state, filterprice: action.payload }

        case SORT_PRICE_CAR_RENTAL:
            return { ...state, sortprice: action.payload }
        
        default:
            return { ...state }
    }
})