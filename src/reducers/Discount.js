/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-22 14:01:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-27 17:37:41
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { GET_VALIDATE_PROMO, GET_VALIDATE_PROMO_SUCCESS, GET_VALIDATE_PROMO_FAILURE, IS_PROMO_VALIDATE, PROMO_CODE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    promo: null,
    validPromo: false,
    promoCode: null
}

const persistConfig = {
    key: 'discount',
    storage: storage,
    whitelist: ['validPromo', 'promoCode']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_VALIDATE_PROMO:
            return { ...state, loading: true }
        case GET_VALIDATE_PROMO_SUCCESS:
            return { ...state, loading: false, promo: action.payload }
        case GET_VALIDATE_PROMO_FAILURE:
            return { ...state, loading: false, promo: null }
        case PROMO_CODE:
            return { ...state, promoCode: action.payload }
        case IS_PROMO_VALIDATE:
            return { ...state, validPromo: action.payload }
        default:
            return { ...state }
    }
})