/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-04 10:02:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-15 17:03:27
 */
import {
    POST_FORGOT_PASSWORD,
    POST_FORGOT_PASSWORD_SUCCESS,
    POST_FORGOT_PASSWORD_FAILURE,
    DELETE_PREV_FORGOT_PASSWORD,
    POST_NEW_PASSWORD, POST_NEW_PASSWORD_SUCCESS, POST_NEW_PASSWORD_FAILURE
} from "../actions/Types";

const INIT_STATE = {
    loading: false,
    forgotPasswordSuccess: null,
    postNewPasswordSuccess: null,
    postNewPasswordFailure: null,
    forgotPasswordFailure: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case POST_FORGOT_PASSWORD:
            return { ...state, loading: true };
        case POST_FORGOT_PASSWORD_SUCCESS:
            return { ...state, loading: false, forgotPasswordSuccess: action.payload };
        case POST_FORGOT_PASSWORD_FAILURE:
            return { ...state, loading: false, forgotPasswordFailure: action.payload };
        case POST_NEW_PASSWORD:
            return { ...state, loading: true };
        case POST_NEW_PASSWORD_SUCCESS:
            return { ...state, loading: false, postNewPasswordSuccess: action.payload };
        case POST_NEW_PASSWORD_FAILURE:
            return { ...state, loading: false, postNewPasswordFailure: action.payload };
        case DELETE_PREV_FORGOT_PASSWORD:
            return { ...state, loading: false, forgotPasswordSuccess: null, forgotPasswordFailure: null }
        default:
            return { ...state };
    }
}