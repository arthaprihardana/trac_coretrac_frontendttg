/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-07 10:09:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 11:57:54
 */
import { localStorageEncrypt, localStorageDecrypt } from "helpers";
import { POST_LOGIN, POST_LOGIN_SUCCESS, POST_LOGIN_FAILURE, GET_LOGOUT, GET_LOGOUT_SUCCESS, GET_LOGOUT_FAILURE, POST_LOGIN_SOSMED, POST_LOGIN_SOSMED_SUCCESS, POST_LOGIN_SOSMED_FAILURE, DELETE_PREV_LOGIN, IS_LOGIN } from "../actions/Types";
import db from "../db";

const INIT_STATE = {
    loading: false,
    loginSuccess: null,
    loginFailure: null,
    logoutSuccess: null,
    logoutFailure: null,
    loginsosmedSuccess: null,
    loginSosmedFailure: null,
    isLogin: false,
    localUser: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case POST_LOGIN:
            return { ...state, loading: true }
        case POST_LOGIN_SUCCESS:
            var data = {
                UserLoginId: action.payload.login_data.Data.Id,
                FirstName: action.payload.login_data.Data.FirstName,
                LastName: action.payload.login_data.Data.LastName,
                EmailPersonal: action.payload.login_data.Data.EmailPersonal,
                Address: action.payload.login_data.Data.Address,
                NoTelp: action.payload.login_data.Data.NoTelp,
                NoHandphone: action.payload.login_data.Data.NoHandphone,
                RoleId: action.payload.login_data.Data.RoleId,
                ImagePath: action.payload.login_data.Data.ImagePath,
                IsActive: action.payload.login_data.Data.IsActive,
                IsPicCostCenter: action.payload.login_data.Data.IsPicCostCenter,
                IsInternal: action.payload.login_data.Data.IsInternal,
                IsPic: action.payload.login_data.Data.IsPic,
                HasCreditCard: action.payload.login_data.Data.HasCreditCard,
                Gender: action.payload.login_data.Data.Gender,
                CreditCard: action.payload.login_data.Data.CreditCard,
                CardExpired: action.payload.login_data.Data.CardExpired,
                CardPublisher: action.payload.login_data.Data.CardPublisher,
                CardType: action.payload.login_data.Data.CardType,
                NRPExternal: action.payload.login_data.Data.NRPExternal,
                NRPInternal: action.payload.login_data.Data.NRPInternal,
                NoKTP: action.payload.login_data.Data.NoKTP,
                NoSIM: action.payload.login_data.Data.NoSIM,
                ImageKTP: action.payload.login_data.Data.ImageKTP,
                ImageSIM: action.payload.login_data.Data.ImageSIM,
                IsForeigner: action.payload.login_data.Data.IsForeigner,
                Username: action.payload.login_data.Data.Username,
                EmailCorp: action.payload.login_data.Data.EmailCorp,
                token: action.payload.login_data.token
            }
            localStorageEncrypt('_aaa', JSON.stringify(data));
            return { ...state, loading: false, loginSuccess: action.payload, loginFailure: null, isLogin: true, localUser: data }
        case POST_LOGIN_FAILURE:
            return { ...state, loading: false, loginSuccess: null, loginFailure: action.payload }
        case DELETE_PREV_LOGIN:
            return { ...state, loginFailure: null }
        case GET_LOGOUT:
            return { ...state, loading: true }
        case GET_LOGOUT_SUCCESS:
            db.table('local_auth').delete(1);
            localStorage.removeItem('_aaa');
            return { ...state, loading: false, logoutSuccess: action.payload, logoutFailure: null }
        case GET_LOGOUT_FAILURE:
            return { ...state, loading: false, logoutSuccess: null, logoutFailure: action.payload }
        case POST_LOGIN_SOSMED:
            return { ...state, loading: true }
        case POST_LOGIN_SOSMED_SUCCESS:
            var data = {
                UserLoginId: action.payload.login_data.Data.Id,
                FirstName: action.payload.login_data.Data.FirstName,
                LastName: action.payload.login_data.Data.LastName,
                EmailPersonal: action.payload.login_data.Data.EmailPersonal,
                Address: action.payload.login_data.Data.Address,
                NoTelp: action.payload.login_data.Data.NoTelp,
                NoHandphone: action.payload.login_data.Data.NoHandphone,
                RoleId: action.payload.login_data.Data.RoleId,
                ImagePath: action.payload.login_data.Data.ImagePath,
                IsActive: action.payload.login_data.Data.IsActive,
                IsPicCostCenter: action.payload.login_data.Data.IsPicCostCenter,
                IsInternal: action.payload.login_data.Data.IsInternal,
                IsPic: action.payload.login_data.Data.IsPic,
                HasCreditCard: action.payload.login_data.Data.HasCreditCard,
                Gender: action.payload.login_data.Data.Gender,
                CreditCard: action.payload.login_data.Data.CreditCard,
                CardExpired: action.payload.login_data.Data.CardExpired,
                CardPublisher: action.payload.login_data.Data.CardPublisher,
                CardType: action.payload.login_data.Data.CardType,
                NRPExternal: action.payload.login_data.Data.NRPExternal,
                NRPInternal: action.payload.login_data.Data.NRPInternal,
                NoKTP: action.payload.login_data.Data.NoKTP,
                NoSIM: action.payload.login_data.Data.NoSIM,
                ImageKTP: action.payload.login_data.Data.ImageKTP,
                ImageSIM: action.payload.login_data.Data.ImageSIM,
                IsForeigner: action.payload.login_data.Data.IsForeigner,
                Username: action.payload.login_data.Data.Username,
                EmailCorp: action.payload.login_data.Data.EmailCorp,
                token: action.payload.login_data.token
            }
            localStorageEncrypt('_aaa', JSON.stringify(data));
            return { ...state, loading: false, loginsosmedSuccess: action.payload, loginSosmedFailure: null }
        case POST_LOGIN_SOSMED_FAILURE:
            return { ...state, loading: false, loginsosmedSuccess: null, loginSosmedFailure: action.payload }
        case IS_LOGIN:
            let loginCheck = localStorageDecrypt('_aaa', 'object');
            let il = false;
            if(loginCheck !== null) {
                il = true;
            }
            return { ...state, isLogin: il, localUser: il ? loginCheck : null }
        default:
            return { ...state }
    }
}