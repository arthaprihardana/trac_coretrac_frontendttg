/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-22 16:23:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-22 16:24:24
 */
import {
    GET_USER_PROFILE,
    GET_USER_PROFILE_SUCCESS,
    GET_USER_PROFILE_FAILURE,
    FORM_ACCOUNT,
    FORM_IDENTIFICATION,
    PUT_USER_PROFILE,
    PUT_USER_PROFILE_SUCCESS,
    PUT_USER_PROFILE_FAILURE,
    GET_ACTIVATE_USER,
    GET_ACTIVATE_USER_SUCCESS,
    GET_ACTIVATE_USER_FAILURE,
    PUT_PASSWORD_SUCCESS,
    PUT_PASSWORD_FAILURE,
    PUT_PASSWORD,
    CONTACT_US, CONTACT_US_SUCCESS, CONTACT_US_FAILURE, CHECK_TOKEN, CHECK_TOKEN_SUCCESS, CHECK_TOKEN_FAILURE
} from "../actions/Types";
import { localStorageEncrypt, localStorageDecrypt } from "helpers";

const INIT_STATE = {
    loading: false,
    user: null,
    user_activate: null,
    formAccount: null,
    updateUserProfileSuccess:null,
    getActivateUserSuccess:null,
    getActivateUserFailure:null,
    updatePasswordSuccess:null,
    updatePasswordFailure:null,
    checkTokenSuccess:null,
    checkTokenFailure:null,
    updateUserError:null,
    formIdentification: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ACTIVATE_USER:
            return { ...state, loading: true };
        case GET_ACTIVATE_USER_SUCCESS:
            return { ...state, loading: false, getActivateUserSuccess: action.payload, getActivateUserFailure: null };
        case GET_ACTIVATE_USER_FAILURE:
            return { ...state, loading: false, getActivateUserSuccess: null, getActivateUserFailure: action.payload };
        case GET_USER_PROFILE:
            return { ...state, loading: true };
        case GET_USER_PROFILE_SUCCESS:
        var data = localStorageDecrypt('_aaa', 'object');
        // console.log("action.payload",action.payload);
        // console.log("data", data);
            data.FirstName = action.payload.Data.FirstName;
            data.LastName= action.payload.Data.LastName;
            data.EmailPersonal= action.payload.Data.EmailPersonal;
            data.Address= action.payload.Data.Address;
            data.NoTelp= action.payload.Data.NoTelp;
            data.NoHandphone= action.payload.Data.NoHandphone;
            data.ImagePath= action.payload.Data.ImagePath;
            data.HasCreditCard= action.payload.Data.HasCreditCard;
            data.NoKTP= action.payload.Data.NoKTP;
            data.NoSIM= action.payload.Data.NoSIM;
            data.ImageKTP= action.payload.Data.ImageKTP;
            data.ImageSIM= action.payload.Data.ImageSIM;
            data.IsForeigner= action.payload.Data.IsForeigner;
            localStorageEncrypt('_aaa', JSON.stringify(data));
            console.log(data);
            return { ...state, loading: false, user: action.payload };
        case GET_USER_PROFILE_FAILURE:
            return { ...state, loading: false, user: null };
        case CHECK_TOKEN:
            return { ...state, loading: true };
        case CHECK_TOKEN_SUCCESS:
            return { ...state, loading: false, checkTokenSuccess: action.payload, checkTokenFailure: null };
        case CHECK_TOKEN_FAILURE:
            return { ...state, loading: false, checkTokenSuccess: null, checkTokenFailure: action.payload };
        case PUT_USER_PROFILE:
            return { ...state, loading: true };
        case PUT_USER_PROFILE_SUCCESS:
            return { ...state, loading: false, updateUserProfileSuccess: action.payload, updateUserProfileFailure: null };
        case PUT_USER_PROFILE_FAILURE:
            return { ...state, loading: false, updateUserProfileSuccess: null, updateUserProfileFailure: action.payload };
        case PUT_PASSWORD:
            return { ...state, loading: true };
        case PUT_PASSWORD_SUCCESS:
            return { ...state, loading: false, updatePasswordSuccess: action.payload, updatePasswordFailure: null };
        case PUT_PASSWORD_FAILURE:
            return { ...state, loading: false, updatePasswordSuccess: null, updatePasswordFailure: action.payload };
        case FORM_ACCOUNT:
            return { ...state, formAccount: action.payload };
        case FORM_IDENTIFICATION:
            return { ...state, formIdentification: action.payload };
        case CONTACT_US:
            return { ...state, loading: true };
        case CONTACT_US_SUCCESS:
            return { ...state, loading: false, createContactUsSuccess: action.payload, createContactUsFailure: null };
        case CONTACT_US_FAILURE:
            return { ...state, loading: false, createContactUsSuccess: null, createContactUsFailure: action.payload };
        default:
            return { ...state }
    }
}