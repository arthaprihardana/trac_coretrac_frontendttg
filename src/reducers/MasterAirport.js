/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-11 09:13:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 17:27:29
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import _ from "lodash";
import { GET_MASTER_AIRPORT, GET_MASTER_AIRPORT_SUCCESS, GET_MASTER_AIRPORT_FAILURE, GET_MASTER_AIRPORT_CITY_COVERAGE, GET_MASTER_AIRPORT_CITY_COVERAGE_SUCCESS, GET_MASTER_AIRPORT_CITY_COVERAGE_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    airportsuccess: null,
    airportfailure: null,
    airportcoveragesuccess: null,
    airportcoveragefailure: null
}

const persistConfig = {
    key: 'masterairport',
    storage: storage,
    blacklist: ['airportfailure', 'airportcoveragefailure']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_AIRPORT:
            return { ...state, loading: true }
        case GET_MASTER_AIRPORT_SUCCESS:
            let newAirport = _.map(action.payload, val => { 
                return {
                    id: val.Code, 
                    name: `${_.startCase(_.lowerCase(val.CityName))} (${val.Airport})`
                } 
            });
            return { ...state, loading: false, airportsuccess: newAirport, airportfailure: null }
        case GET_MASTER_AIRPORT_FAILURE:
            return { ...state, loading: false, airportsuccess: null, airportfailure: action.payload }
        case GET_MASTER_AIRPORT_CITY_COVERAGE:
            return { ...state, loading: true }
        case GET_MASTER_AIRPORT_CITY_COVERAGE_SUCCESS:
            let f = _.filter(action.payload, v => v.city !== null);
            let newAirportCoverage = _.map(f, val => {
                return {
                    id: val.city.BranchId,  // branch id 
                    name: _.startCase(_.lowerCase(val.CityName)),
                    cityId: val.CityId
                }
            });
            return { ...state, loading: false, airportcoveragesuccess: newAirportCoverage, airportcoveragefailure: null }
        case GET_MASTER_AIRPORT_CITY_COVERAGE_FAILURE:
            return { ...state, loading: false, airportcoveragesuccess: null, airportcoveragefailure: action.payload }
        default:
            return { ...state }
    }
})