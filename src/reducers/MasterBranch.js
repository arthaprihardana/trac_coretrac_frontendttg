/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-01 13:41:16 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-01 16:04:18
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { GET_MASTER_BRANCH, GET_MASTER_BRANCH_SUCCESS, GET_MASTER_BRANCH_FAILURE, SET_BRANCH_FROM_BUS } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    branch: null,
    branchFromBus: null
}

const persistConfig = {
    key: 'masterbranch',
    storage: storage,
    whitelist: ['branchFromBus']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_BRANCH:
            return { ...state, loading: true }
        case GET_MASTER_BRANCH_SUCCESS:
            return { ...state, loading: false, branch: action.payload }
        case GET_MASTER_BRANCH_FAILURE:
            return { ...state, loading: false, branch: null }
        case SET_BRANCH_FROM_BUS:
            return { ...state, loading: false, branchFromBus: action.payload }
        default:
            return { ...state }
    }
})