/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-11 14:39:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-12 13:59:36
 */
import { AIRPORT_ACTIVE_INPUT_FORM, GET_DURATION_AIRPORT_TRANSFER, GET_DURATION_AIRPORT_TRANSFER_SUCCESS, GET_DURATION_AIRPORT_TRANSFER_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    inputShow: false,
    duration: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case AIRPORT_ACTIVE_INPUT_FORM:
            return { ...state, inputShow: action.payload }
        case GET_DURATION_AIRPORT_TRANSFER:
            return { ...state, loading: true }
        case GET_DURATION_AIRPORT_TRANSFER_SUCCESS:
            return { ...state, loading: false, duration: action.payload }
        case GET_DURATION_AIRPORT_TRANSFER_FAILURE:
            return { ...state, loading: false, duration: null }
        default:
            return { ...state }
    }
}