/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-07 10:55:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-07 15:15:05
 */
import { GET_MASTER_CITY, GET_MASTER_CITY_SUCCESS, GET_MASTER_CITY_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    mastercitysuccess: null,
    mastercityfailure: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_CITY:
            return { ...state, loading: true }
        case GET_MASTER_CITY_SUCCESS:
            return { ...state, loading: false, mastercitysuccess: action.payload, mastercityfailure: null }
        case GET_MASTER_CITY_FAILURE:
            return { ...state, loading: false, mastercitysuccess: null, mastercityfailure: action.payload }
        default:
            return { ...state };
    }
}