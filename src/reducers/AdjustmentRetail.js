/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-24 19:21:16 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 19:26:48
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { GET_ADJUSTMENT_RETAIL, GET_ADJUSTMENT_RETAIL_SUCCESS, GET_ADJUSTMENT_RETAIL_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    adjustment: null
}

const persistConfig = {
    key: 'adjustmentRetail',
    storage: storage,
    whitelist: ['adjustment']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ADJUSTMENT_RETAIL:
            return { ...state, loading: true }
        case GET_ADJUSTMENT_RETAIL_SUCCESS:
            return { ...state, loading: false, adjustment: action.payload }
        case GET_ADJUSTMENT_RETAIL_FAILURE:
            return { ...state, loading: false, adjustment: null }
        default:
            return { ...state }
    }
})