/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-22 16:17:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 17:26:53
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { GET_ADDRESS_GM, GET_ADDRESS_GM_SUCCESS, GET_ADDRESS_GM_FAILURE, GET_PLACE_DETAIL_GM, GET_PLACE_DETAIL_GM_SUCCESS, GET_PLACE_DETAIL_GM_FAILURE, GET_GEOMETRY_GM, GET_GEOMETRY_GM_SUCCESS, GET_GEOMETRY_GM_FAIURE, GET_DISTANCE_MATRIX_GM, GET_DISTANCE_MATRIX_GM_SUCCESS, GET_DISTANCE_MATRIX_GM_FAILURE, GET_DISTANCE_MATRIX_BUS, GET_DISTANCE_MATRIX_BUS_SUCCESS, GET_DISTANCE_MATRIX_BUS_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    address: null,
    geometry: null,
    place: null,
    distance: null,
    distanceBus: null
};

const persistConfig = {
    key: 'addressgm',
    storage: storage,
    blacklist: ['address', 'geometry', 'place', 'distanceBus']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ADDRESS_GM:
            return { ...state, loading: true }
        case GET_ADDRESS_GM_SUCCESS:
            return { ...state, loading: false, address: action.payload }
        case GET_ADDRESS_GM_FAILURE: 
            return { ...state, loading: false, address: null }

        case GET_PLACE_DETAIL_GM:
            return { ...state, loading: true }
        case GET_PLACE_DETAIL_GM_SUCCESS:
            return { ...state, loading: false, place: action.payload }
        case GET_PLACE_DETAIL_GM_FAILURE:
            return { ...state, loading: false, place: null }
        
        case GET_GEOMETRY_GM:
            return { ...state, loading: true }
        case GET_GEOMETRY_GM_SUCCESS:
            return { ...state, loading: false, geometry: action.payload }
        case GET_GEOMETRY_GM_FAIURE:
            return { ...state, loading: false, geometry: null }

        case GET_DISTANCE_MATRIX_GM:
            return { ...state, loading: true }
        case GET_DISTANCE_MATRIX_GM_SUCCESS:
            return { ...state, loading: false, distance: action.payload }
        case GET_DISTANCE_MATRIX_GM_FAILURE:
            return { ...state, loading: false, distance: null }

        case GET_DISTANCE_MATRIX_BUS:
            return { ...state, loading: true }
        case GET_DISTANCE_MATRIX_BUS_SUCCESS:
            return { ...state, loading: false, distanceBus: action.payload }
        case GET_DISTANCE_MATRIX_BUS_FAILURE:
            return { ...state, loading: false, distanceBus: null }

        default:
            return { ...state }
    }
})