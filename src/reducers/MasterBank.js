/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 00:36:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-14 00:37:09
 */
import { GET_MASTER_BANK, GET_MASTER_BANK_SUCCESS, GET_MASTER_BANK_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    bank: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_BANK:
            return { ...state, loading: true }
        case GET_MASTER_BANK_SUCCESS:
            return { ...state, loading: false, bank: action.payload }
        case GET_MASTER_BANK_FAILURE:
            return { ...state, loading: false, bank: null }
        default:
            return { ...state }
    }
}