/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 20:23:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 01:03:46
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { POST_CREATE_INVOICE, POST_CREATE_INVOICE_SUCCESS, POST_CREATE_INVOICE_FAILURE, IS_PAYMENT } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    invoice: null,
    isPayment: false
};

const persistConfig = {
    key: 'billing',
    storage: storage,
    whitelist: ['isPayment']
};

export default persistReducer(persistConfig, ( state = INIT_STATE, action ) => {
    switch (action.type) {
        case POST_CREATE_INVOICE:
            return { ...state, loading: true }
        case POST_CREATE_INVOICE_SUCCESS:
            return { ...state, loading: false, invoice: action.payload }
        case POST_CREATE_INVOICE_FAILURE:
            return { ...state, loading: false, invoice: null }
        case IS_PAYMENT:
            return { ...state, isPayment: action.payload }
        default:
            return { ...state }
    }
})