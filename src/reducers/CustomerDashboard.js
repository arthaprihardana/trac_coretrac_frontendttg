/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-24 09:44:08 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-26 17:27:48
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import {
    GET_MY_BOOKING,
    GET_MY_BOOKING_SUCCESS,
    GET_MY_BOOKING_FAILURE,
    GET_DETAIL_MY_BOOKING,
    GET_MY_HISTORY,
    GET_MY_HISTORY_SUCCESS,
    GET_MY_HISTORY_FAILURE,
    GET_DETAIL_MY_BOOKING_SUCCESS,
    GET_DETAIL_MY_BOOKING_FAILURE,
    GET_CREDIT_CARD,
    GET_CREDIT_CARD_SUCCESS,
    GET_CREDIT_CARD_FAILURE,
    PUT_CREDIT_CARD,
    PUT_CREDIT_CARD_SUCCESS,
    PUT_CREDIT_CARD_FAILURE,
    POST_CREDIT_CARD,
    POST_CREDIT_CARD_SUCCESS,
    POST_CREDIT_CARD_FAILURE,
    DELETE_CREDIT_CARD,
    DELETE_CREDIT_CARD_SUCCESS,
    DELETE_CREDIT_CARD_FAILURE, PUT_PRIMARY_CC, PUT_PRIMARY_CC_SUCCESS, PUT_PRIMARY_CC_FAILURE
} from "../actions/Types";
import {putPrimaryCCFailure} from "../actions";

const INIT_STATE = {
    loading: false,
    myBooking: null,
    myHistory: null,
    creditCard:null,
    getCreditCardFailure:null,
    getCreditCardSuccess:null,
    putPrimaryCCSuccess:null,
    putPrimaryCCFailure:null,
    updateCreditCardSuccess:null,
    updateCreditCardFailure:null,
    deleteCreditCardSuccess:null,
    deleteCreditCardFailure:null,
    detailMyBooking: null
}

const persistConfig = {
    key: 'dashboard',
    storage: storage,
    blacklist: ['myBooking', 'myHistory']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MY_BOOKING:
            return { ...state, loading: true }
        case GET_MY_BOOKING_SUCCESS:
            return { ...state, loading: false, myBooking: action.payload }
        case GET_MY_BOOKING_FAILURE:
            return { ...state, loading: false, myBooking: null }
        case GET_MY_HISTORY:
            return { ...state, loading: true }
        case GET_MY_HISTORY_SUCCESS:
            return { ...state, loading: false, myHistory: action.payload }
        case GET_MY_HISTORY_FAILURE:
            return { ...state, loading: false, myHistory: null }
        case GET_DETAIL_MY_BOOKING:
            return { ...state, loading: true }
        case GET_DETAIL_MY_BOOKING_SUCCESS:
            return { ...state, loading: false, detailMyBooking: action.payload }
        case GET_DETAIL_MY_BOOKING_FAILURE:
            return { ...state, loading: false, detailMyBooking: null }
        case GET_CREDIT_CARD:
            return { ...state, loading: true };
        case GET_CREDIT_CARD_SUCCESS:
            return { ...state, loading: false, getCreditCardSuccess: action.payload, getCreditCardFailure:null};
        case GET_CREDIT_CARD_FAILURE:
            return { ...state, loading: false, getCreditCardSuccess: null, getCreditCardFailure:action.payload };
        case PUT_CREDIT_CARD:
            return { ...state, loading: true };
        case PUT_CREDIT_CARD_SUCCESS:
            return { ...state, loading: false, updateCreditCardSuccess: action.payload, updateCreditCardFailure: null };
        case PUT_CREDIT_CARD_FAILURE:
            return { ...state, loading: false, updateCreditCardSuccess: null, updateCreditCardFailure: action.payload };
        case POST_CREDIT_CARD:
            return { ...state, loading: true };
        case POST_CREDIT_CARD_SUCCESS:
            return { ...state, loading: false, postCreditCardSuccess: action.payload, postCreditCardFailure: null };
        case POST_CREDIT_CARD_FAILURE:
            return { ...state, loading: false, postCreditCardSuccess: null, postCreditCardFailure: action.payload };
        case DELETE_CREDIT_CARD:
            return { ...state, loading: true };
        case DELETE_CREDIT_CARD_SUCCESS:
            return { ...state, loading: false, deleteCreditCardSuccess: action.payload, postCreditCardFailure: null };
        case DELETE_CREDIT_CARD_FAILURE:
            return { ...state, loading: false, postCreditCardSuccess: null, postCreditCardFailure: action.payload };
        case PUT_PRIMARY_CC:
            return { ...state, loading: true };
        case PUT_PRIMARY_CC_SUCCESS:
            return { ...state, loading: false, putPrimaryCCSuccess: action.payload, putPrimaryCCFailure: null };
        case PUT_PRIMARY_CC_FAILURE:
            return { ...state, loading: false, putPrimaryCCSuccess: null, putPrimaryCCFailure: action.payload };
        default:
            return { ...state }
    }
})