/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-03 10:11:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-15 11:00:04
 */
import { POST_REGISTER, POST_REGISTER_SUCCESS, POST_REGISTER_FAILURE, DELETE_PREV_REGISTER } from '../actions/Types';

const INIT_STATE = {
    loading: false,
    registerSucces: null,
    registerError: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case POST_REGISTER:
            return { ...state, loading: true };
        case POST_REGISTER_SUCCESS:
            return { ...state, loading: false, registerSucces: action.payload, registerError: null };
        case POST_REGISTER_FAILURE:
            return { ...state, loading: false, registerSucces: null, registerError: action.payload };
        case DELETE_PREV_REGISTER:
            return { ...state, registerSucces: null, registerError: null }
        default:
            return { ...state }
    }
}