/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-22 16:23:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-22 16:24:24
 */
import { GET_VEHICLE_ATTRIBUTE, GET_VEHICLE_ATTRIBUTE_SUCCESS, GET_VEHICLE_ATTRIBUTE_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    vehicle_attr: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_VEHICLE_ATTRIBUTE:
            return { ...state, loading: true }
        case GET_VEHICLE_ATTRIBUTE_SUCCESS:
            return { ...state, loading: false, vehicle_attr: action.payload }
        case GET_VEHICLE_ATTRIBUTE_FAILURE:
            return { ...state, loading: false, vehicle_attr: null }
        default:
            return { ...state }
    }
}