/**
 * @author: Artha Prihardana 
 * @Date: 2019-01-17 10:57:40 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-28 11:32:37
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import _ from "lodash";
import { GET_MASTER_CAR_TYPE, GET_MASTER_CAR_TYPE_SUCCESS, GET_MASTER_CAR_TYPE_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    cartypesuccess: null,
    cartypefailure: null
};

const persistConfig = {
    key: 'mastercartype',
    storage: storage,
    whitelist: ['cartypesuccess']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_CAR_TYPE:
            return { ...state, loading: true }
        case GET_MASTER_CAR_TYPE_SUCCESS:
            let carType = _.map(action.payload, v => {
                return { ...v, selected: false }
            });
            return { ...state, loading: false, cartypesuccess: carType, cartypefailure: null }
        case GET_MASTER_CAR_TYPE_FAILURE:
            return { ...state, loading: false, cartypesuccess: null, cartypefailure: action.payload }
        default:
            return { ...state }
    }
})