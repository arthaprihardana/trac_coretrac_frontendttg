/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-14 16:45:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-14 17:20:28
 */
import { GET_BUS_RENTAL_PRICE, GET_BUS_RENTAL_PRICE_SUCCESS, GET_BUS_RENTAL_PRICE_FAILURE, GET_DISTANCE_BUS } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    priceBus: null,
    distanceBus: 0
}

export default ( state = INIT_STATE, action ) => {
    switch (action.type) {
        case GET_BUS_RENTAL_PRICE:
            return { ...state, loading: true }
        case GET_BUS_RENTAL_PRICE_SUCCESS:
            return { ...state, loading: false, priceBus: action.payload }
        case GET_BUS_RENTAL_PRICE_FAILURE:
            return { ...state, loading: false, priceBus: null }
        case GET_DISTANCE_BUS:
            return { ...state, distanceBus: action.payload }
        default:
            return { ...state }
    }
} 