/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-23 17:55:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-02-23 19:04:16
 */
import React, { Component } from 'react';

const GlobalContext = React.createContext();

const GlobalProvider = GlobalContext.Provider;
const GlobalCustomer = GlobalContext.Consumer;

const ProviderParent = (Children) => {
    class ProviderParent extends Component {
        state={
            values: {},
            transportType:'',
            loginType: 'email'
        }
    
        changeValues = (newValues) => {
            this.setState({values: newValues})
        }

        changeTransportType = (newValues) => {
            this.setState({transportType: newValues})
        }
        
        changeLoginType = (newValues) => {
            this.setState({loginType: newValues})
        }
    
        render(){
            return(
                <GlobalProvider value={{
                    rentContext: {
                        state: this.state,
                        handler: {
                            changeValues: this.changeValues,
                            changeTransportType: this.changeTransportType,
                            changeLoginType: this.changeLoginType
                        }
                    }
                }}>
                    <Children {...this.props}/>
                </GlobalProvider>
            )
        }
    }
    return ProviderParent;
}

export const CustomerGlobal = (Children) => {
    class CustomerGlobal extends Component {
        render(){
            return(
                <GlobalCustomer>
                    {rentContext => (
                        <Children {...this.props} {...rentContext}/>
                    )}
                </GlobalCustomer>
            )
        }
    }
    return CustomerGlobal;
} 

export default ProviderParent;
