# [TRAC To Go](https://trac.astra.co.id/)

## Description

TRAC To Go adalah sebuah platform yang menyediakan jasa Rental Mobil dan Bus yang berkualitas.

Platform ini dibuat menggunakan framework FrontEnd ReactJS.

## Dependencies

-   @babel/plugin-proposal-class-properties
-   @babel/preset-env
-   @babel/preset-react
-   @babel/register
-   auth0-js
-   axios
-   babel-plugin-dynamic-import-node
-   babel-plugin-syntax-dynamic-import
-   babel-polyfill
-   classnames
-   crypto-js
-   dexie
-   express
-   file-loader
-   formik
-   google-map-react
-   google-react-maps
-   ignore-styles
-   image-to-base64
-   lodash
-   moment
-   node-sass
-   nodemon
-   prop-types
-   rc-slider
-   react
-   react-copy-to-clipboard
-   react-datepicker
-   react-dom
-   react-dropzone
-   react-ga
-   react-helmet
-   react-icons
-   react-loadable
-   react-number-format
-   react-redux
-   react-reveal
-   react-router
-   react-router-dom
-   react-scripts
-   react-select
-   react-slick
-   react-spinners
-   redux
-   redux-devtools-extension
-   redux-persist
-   redux-saga
-   redux-thunk
-   slick-carousel
-   stylint
-   url-loader
-   uuid
-   window-or-global
-   yup

## Dev Dependencies

-   @babel/plugin-proposal-class-properties
-   @babel/preset-env
-   @babel/preset-react
-   @babel/register
-   babel-plugin-dynamic-import-node
-   babel-plugin-syntax-dynamic-import
-   babel-polyfill
-   eslint-config-airbnb
-   eslint-plugin-import
-   eslint-plugin-jsx-a11y
-   eslint-plugin-react
-   file-loader
-   nodemon
-   redux-devtools-extension
-   stylint

## Screenshot

### Home
![Screenshot-1](https://bitbucket.org/arthaprihardana/trac_coretrac_frontendttg/raw/929645b90640a0c912aec5b4808853f28a22880d/screenshots/screenshot-1.png)

### List Car
![Screenshot-2](https://bitbucket.org/arthaprihardana/trac_coretrac_frontendttg/raw/929645b90640a0c912aec5b4808853f28a22880d/screenshots/screenshot-2.png)

### Detail Car
![Screenshot-3](https://bitbucket.org/arthaprihardana/trac_coretrac_frontendttg/raw/929645b90640a0c912aec5b4808853f28a22880d/screenshots/screenshot-3.png)